<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SchoolImportCronsController extends AppController {
    
    const STATUS_NEW = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_COMPLETE = 2;
        
    public $name = 'SchoolImportCrons';
    public $uses = array('SchoolImport','School', 'Setting');

    public function beforeFilter() {
        
        parent::beforeFilter();
        $this->layout = false;
        $this->autoRender = false;
    }
    
    public function process() { 
        $imports = $this->_getPendingExcelSheets();
        if (count($imports) > 0) {
            require(WWW_ROOT . 'lib/simplexlsx.class.php');
            CakeLog::write('debug', 'There are ' . count($imports) . ' import(s) waiting for process.');
            foreach ($imports as $import) {
                CakeLog::write('debug', 'Processing ' . $import['SchoolImport']['id']);
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_PROCESSING);
                $this->_startImport($import);
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_COMPLETE);
            }
        }
    }
    
    protected function _getPendingExcelSheets() {
        $imports = $this->SchoolImport->find('all', array('conditions' => array('status' => self::STATUS_NEW)));
        return $imports;
    }
    
    protected function _startImport($import) {

        $xlsFile = WWW_ROOT . 'files/school/imports/' . $import['SchoolImport']['path'];
        $xlsx = new SimpleXLSX($xlsFile);
        if (!$xlsx || empty($xlsx)) {
            CakeLog::write('debug', 'There was an error opening xls file.');
            return false;
        }

        $error = false;
        $already = $total = 0;
        
        for ($j = 1; $j <= $xlsx->sheetsCount(); $j++) {
            foreach ($xlsx->rows($j) as $k => $r) {
                $worksheets = $xlsx->rows($j);
            }
        }
        unset($worksheets[0]); /* Avoid first line for headers */
        
        foreach ($worksheets as $index => $row) {

            $check = $this->School->findByEmail($row[2]);
            
            if(!empty($check)){
                /* Email already exist in school table */
                $already++;
            }  else {
                
                $address = (!empty($row[4]))? trim($row[4]) : '';
                $phone = (!empty($row[5]))? trim($row[5]) : '';
                $fax = (!empty($row[6]))? trim($row[6]) : '';

                $data = array('School' => array(
                    'name' => trim($row[0]),
                    'manager_name' => trim($row[1]),
                    'email' => trim($row[2]),
                    'password' => (!empty($row[3])) ? trim($row[3]) : '',
                    'address' => $address,
                    'isdeleted' => 1,
                    'issent' => 0,
                    'phone' => $phone,
                    'fax' => $fax,
                    'created' => date('YmdHis')
                ));
                
                $this->School->create();
                $this->School->save($data);
                $this->School->clear();
                //$school_id = $this->School->id;
                //$this->sendEmail($dataemail, $school_id,$row[3]);

                $total++;
                
                $this->SchoolImport->read(null, $import['SchoolImport']['id']);
                $this->SchoolImport->set(array(
                    'imported' => $total
                ));
                $this->SchoolImport->save();
                $this->SchoolImport->clear();
                
            }

            $msg = '';
            if ($total > 0)
                $msg .= __('%d school was imported!', $total);
            if ($already > 0)
                $msg .= ($total > 0 ? " " : "") . __('%d emails have already exist.', $already);
            CakeLog::write('debug', $msg);
        }
    }
    
}
?>