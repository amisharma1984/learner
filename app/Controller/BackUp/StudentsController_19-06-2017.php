<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class StudentsController extends AppController {

    public $name = 'Students';
    public $uses = array('Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer', 'Answer', 'MyExamStudentAnswer', 'ExaminationType', 'ExaminationCategory');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration() {
        $this->layout = 'home_layout';
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		  $this->set(compact('examination_types', 'examination_categories'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 
			 //student login start
			 if(!empty($this->request->data['StudentLogin']['login_registration_flag'])  && $this->request->data['StudentLogin']['login_registration_flag'] == 'student_login'){ 
			  $login = strtolower($this->request->data['StudentLogin']['login']);
            $password = $this->request->data['StudentLogin']['password'];
			 $studentdata = $this->Student->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['StudentLogin']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['StudentLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($studentdata) && !empty(trim($this->request->data['StudentLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setStudentData($studentdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			} 
			 } else {
			
			 //student login End
			 
			 //student registration
			 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['Student']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['Student']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['Student']['password'];
							$cpass = $this->request->data['Student']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['Student']['password'] != $this->request->data['Student']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['Student']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['Student']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'students', 'action' => 'registration'));
				}
				
			 }
		 }
	  }

	
	
    public function login() {
        $this->layout = false;
		$ERROR = 0;
       if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			//pr($this->request->data);die;
            $login = strtolower($this->request->data['Student']['login']);
            $password = $this->request->data['Student']['password'];
			 $studentdata = $this->Student->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['Student']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['Student']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($studentdata)){
								$ERROR = 1;
								$this->set('lnerror', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setStudentData($studentdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			} else {
				 $this->redirect(array('controller' => 'students', 'action' => 'registration'));
			}
        }  
    }
    
    public function index() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $examinations = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		 $this->set(compact('studentDetails', 'examinations'));
		 $this->render('purchase_exam');
		// echo $RandomID = $this->Session->read('RandomID'); exit;
		 
    }
	
	 public function myprofile() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$studentdata1 = $this->UserConfiguration->getStudentData();
		//pr($studentdata1);die;
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));  
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		  $this->set(compact('studentDetails', 'examination_types', 'examination_categories'));
		  	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				$this->Student->id = $student_id;
				$this->Student->save($this->request->data);
				//$this->Session->setFlash('Data has been submitted successfully.');
				  $this->Session->setFlash(__('Data has been submitted successfully.'), 'success');
				$this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			 }
		  
       }
	
	
	 public function all_exam_list() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));  
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $practiceExamCategory = $this->ExaminationCategory->find('all', array(
																										'recursive' => -1,	
																										 'conditions' => array(
																															 'ExaminationCategory.isdeleted' => 0,
																															 'ExaminationCategory.examination_type_id' => 2
																													) ));
		// pr($practiceExamCategory);die;
		  $this->set(compact('studentDetails',  'practiceExamCategory'));
		  	
       }
	
	
	
	
	
	
	
	
	//02-03-2017
		
	  public function view_answer($random_id = NULL, $student_idd = NULL) {
       // $this->UserConfiguration->isStudentLoggedIn();
		 $this->layout = 'take_exam_layout';
		  $studentdata1 = $this->UserConfiguration->getStudentData();
				 if(!empty($student_idd)){
					  //$this->Session->delete('App.studentData');
					  //my_exam_id is school_purchase_exam_id
					  $myExamStudentAnsAr = $this->MyExamStudentAnswer->find('first', array(
																				  'fields' => 'id,my_exam_id,class_name,unique_flag',	
																				  'conditions' => array(
																						  'MyExamStudentAnswer.unique_flag' => $random_id
																						  )));
					  
					  $school_purchase_exam_id = $myExamStudentAnsAr['MyExamStudentAnswer']['my_exam_id'];
					  $class_name = $myExamStudentAnsAr['MyExamStudentAnswer']['class_name'];
					  $noOfStudentInYearGroup = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
					  $noOfStudentInClass = $this->MyExamStudentAnswer->find('count', array('conditions' => array(
																								  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																								  'MyExamStudentAnswer.class_name' => $class_name, 
																								  'MyExamStudentAnswer.exam_type' => 2
																								  )));

					 $studentRank = $this->fetch_student_rank($student_idd);
					 $yearGroupTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as year_group_scores'
															)));
														
						
																		
						$sameClassTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			'MyExamStudentAnswer.class_name' => $class_name
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as class_scores'
															)));										
					 
					  $yearGroupMean = round(($yearGroupTotalScores[0]['year_group_scores']/$noOfStudentInYearGroup),2); 
					 
					  $sameClassMean = round(($sameClassTotalScores[0]['class_scores']/$noOfStudentInClass),2);
					   
						$this->set(compact('studentRank', 'yearGroupMean', 'sameClassMean', 'noOfStudentInYearGroup'));
						
					    $this->set('flagA', 'answer_view_by_school');
					    $student_id = $student_idd;
				 } else {
					   $student_id = $studentdata1['Student']['id'];
				 }
				 
				  if(!empty($random_id)){
					  //$this->Session->delete('App.studentData');
					  $this->set('flagB', 'answer_view_by_school');
				 }
		
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id) && empty($student_idd)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		if(!empty($student_idd)){
		$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.unique_flag' => $random_id
																														)));
		
		} else {
			$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.student_ans_random_id' => $RandomID
																														)));
		}
		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		if(!empty($student_idd)){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.unique_flag' => $random_id,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else {
			$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 )));
		}																															 
																																		 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		 if(!empty($student_idd)){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id
																													 )));
	 
		 } else {
			 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.random_id' => $RandomID
																													 )));
			 
		 }
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		 //pr($QuestionAns);die;
		 $this->render('view_answer_new');
    }
	 
	 
	  public function fetch_student_rank($student_id = NULL) {
        //$this->autoRender = false;
	    $this->layout = 'take_exam_layout';
		$studentRank = $this->MyExamStudentAnswer->find('first', 
															array(
																'fields' => 'id,year_group_rank,class_rank',
																'conditions' => array(
																				'MyExamStudentAnswer.student_id' => $student_id,
																				'MyExamStudentAnswer.unique_flag !=' => ' ',
																				
																)));
		return $studentRank;
	  }
	
	
	  public function view_answer_bak($random_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		 $totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array('fields' => 'id, total_set_question','conditions' => array('MyExamStudentAnswer.student_id' => $student_id, 'MyExamStudentAnswer.student_ans_random_id' => $RandomID)));
		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		 'StudentAnswer.answer_id !=' => 0
																																		 )));
		$answer_ids = '';
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
		 }
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		
		 $totalAttemptQs = count($StudentAnswerAr);
		 
		  $this->paginate = array(
							'conditions' => array('StudentAnswer.student_id' => $student_id, 'StudentAnswer.random_id' => $RandomID),
							'limit' => 1
						);
       
	   $StudentAnswerAr = $this->paginate('StudentAnswer');
    
		
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet'));
		 
		//echo '<pre>';print_r($StudentAnswerAr);
		//die;
		 
    }
	 
	
	
	
	 public function my_exams() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $this->MyExamStudentAnswer->recursive = 3;
		 
		 $this->Examination->unbindModel(array('hasMany' => 'Question'), false);
		 
		 $myExamAr___ = $this->MyExamStudentAnswer->find('all', array(
																								 'conditions' => array(
																								 'MyExamStudentAnswer.isdeleted' => 0, 
																								 'MyExamStudentAnswer.exam_type' => 1, 
																								 'MyExamStudentAnswer.student_id' => $student_id
																								 ),
																								'order' => 'MyExamStudentAnswer.created DESC',
																								'limit' => 10					
																								 ));
		//pr($myExamAr);die;
		
		 $this->paginate = array(
										'conditions' =>  array(
																 'MyExamStudentAnswer.isdeleted' => 0, 
																 'MyExamStudentAnswer.exam_type' => 1, 
																 'MyExamStudentAnswer.student_id' => $student_id
																 ),
										'limit' => 10,
										'order' => 'MyExamStudentAnswer.id DESC',
									);
			$myExamAr = $this->paginate('MyExamStudentAnswer');
		
		
		
		
		
		
		 $this->set(compact('studentDetails', 'myExamAr'));
		
    }
	
	
	   public function second_section_exam() {
         //$this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		  $this->set(compact('studentDetails'));
		 
	  }
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		 $RandomID = $this->Session->read('RandomID');
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.student_ans_random_id' => $RandomID,
																																		'MyExamStudentAnswer.my_exam_id' => $this->request->data['my_exam_id'],
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
					$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1;
					}
					
				if(empty($MyExamStudentAnswerExist)){
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				}else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + $this->Session->read('totalQuestions');
						$this->MyExamStudentAnswer->save($data);
					}
			 
			
			/*$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
			$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
			$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
			$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
			$this->MyExamStudentAnswer->save($myExamStudentAns);*/
			
			
			 //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					 $this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
				}
			}
			
			
		//data insert which answer is not given	
		$questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
							
							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			// die;
			$this->Session->setFlash('Data has been submitted successfully.');
			$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			
			/*if($this->request->data['page_action'] == 'take_exam' && $this->request->data['exam_flag'] == 'SingleExam' ){ 
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			}
			else if($this->request->data['page_action'] == 'take_exam_second_section'){
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			} else {
					$this->redirect(array('controller' => 'students', 'action' => 'second_section_exam'));
			}
			*/
			   
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1;
					}
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					//pr($myExamStudentAns);die;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
					
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				     
					 $this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
					  $this->request->data['StudentAnswer']['student_id'] = $student_id;
					   $this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
					$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
					$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];

					$questionData['StudentAnswer']['student_id'] = $student_id;
					$questionData['StudentAnswer']['question_id'] = $qid;
					$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
					$questionData['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	  
	
	 public function take_exam($examination_id = NULL, $my_exam_id = NULL,$flag = NULL,$examFlag = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		$flag = $this->data_decrypt($flag);
		$examFlag = $this->data_decrypt($examFlag);
		$this->Session->write('flag', $flag);
		//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		if($flag == 'NON-CALCULATOR'){
			$section = 'exam_section_2_name';
			$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
			$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
			$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			$examDuration = $settingsData24['Setting']['value'];
		} else if($flag == 'CALCULATOR'){
			$section = 'exam_section_1_name';
			 
			$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
			$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
			$QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$examDuration = $settingsData21['Setting']['value'];
		} else {
			$section = 'nothing'; //false
		}
		
		
		
		
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $examination_id = $this->data_decrypt($examination_id);
		 $myExamID = $this->data_decrypt($my_exam_id);
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		  $this->Session->write('my_exam_id', $myExamID);
		  
		  $this->set('my_exam_id', $myExamID);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		//$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		//$examDuration = $settingsData21['Setting']['value'];
		//$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section,  'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		
    }
	
	
//	public function take_exam($examination_id = NULL, $my_exam_id = NULL) {
	 public function take_exam_second_section($flag = NULL) {
        //Configure::write('debug', 0);
		
			$flag = $this->data_decrypt($flag);
				//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
				if($flag == 'CALCULATOR'){
					$section = 'exam_section_1_name';
				} else if($flag == 'NON-CALCULATOR'){
					$section = 'exam_section_2_name';
				} else {
					$section = 'nothing'; //false
				}
		
		
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 //pr($studentDetails);die;
		 //$examination_id = $this->data_decrypt($examination_id);
		 //$myExamID = $this->data_decrypt($my_exam_id);
		 
		// $this->Session->write('examination_id', $examination_id);
		
		$examination_id = $this->Session->read('examination_id');
		$my_exam_id = $this->Session->read('my_exam_id');
		 $randomValue = $this->random_value();
		  //$this->Session->write('RandomID', $randomValue);
		 // $this->Session->write('my_exam_id', $myExamID);
		  
		 //$this->set('my_exam_id', $myExamID);
		  $this->set('my_exam_id', $my_exam_id);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		//$examDuration = $settingsData21['Setting']['value'];
		//$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		$examDuration = $settingsData24['Setting']['value'];
		$QuestionLimit =  $no_of_qs_23['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		 $this->render('take_exam');
    }
	
	public function get_calculator_exam_count($examination_id = NULL) {
		   $this->UserConfiguration->isStudentLoggedIn();
           $this->layout = 'home_layout';
		  $CalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_1_name', //Calculator
																								'Question.isdeleted' => 0
																								)));
			return $CalculatorQuestionCount;																					
			
	}
	
	public function get_non_calculator_exam_count($examination_id = NULL) {
          $this->UserConfiguration->isStudentLoggedIn();
          $this->layout = 'home_layout';
			$NonCalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_2_name', //Non calculator
																								'Question.isdeleted' => 0
																								)));
		return $NonCalculatorQuestionCount;																						
	}
	
	
	
	 public function take_exam_list() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $this->MyExam->recursive = 2;
		 $examinations = $this->MyExam->find('all', array(
																					'order' => 'MyExam.id DESC'	,
																					 'conditions' => array(
																													 'MyExam.isdeleted' => 0, 
																													 'MyExam.payment_status' => 1, 
																													 'MyExam.student_id' => $student_id
																													 )));
		// pr($examinations);die;
		 $this->set(compact('studentDetails', 'examinations'));
		
    }
	
		 public function checkout() {
			$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
			 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'students', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_id' => $student_id)
                ),
            )

        ));	

				 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					// pr($this->data);die;
					 $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $this->request->data['Examination']['ids']
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					foreach($allExamAr as $val){
						$subTotal = $subTotal + $val['Examination']['price'];
					}
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 }
				 
		 $this->set(compact('studentDetails', 'allExamAr','subTotal','gst', 'total'));
		
    }
	
	
	 public function checkout_new($examination_id = NULL) {
			$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
			 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'students', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_id' => $student_id)
						),
					)

				));	

				     $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $examination_id
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					
						$subTotal = $allExamAr[0]['Examination']['price'];
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 
		 $this->set(compact('studentDetails', 'allExamAr','subTotal','gst', 'total'));
		 $this->render('checkout');
		
    }
	
		 public function is_exist_in_myexam($examination_id = NULL) {
			$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			$this->autoRender = false;
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			 $isMyexamPurchased = $this->MyExam->find('count', array(
														 'conditions' => array(
																'MyExam.examination_id' => $examination_id,
																'MyExam.student_id' => $student_id,
																'MyExam.payment_status' => 1,
																'MyExam.isdeleted' => 0,
																'MyExam.is_expired' => 0,
														 )));
			return $isMyexamPurchased;											 
		 }
	
	
	
	
	 public function purchase_exam($examination_category_id = NULL) {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		// echo $student_id;
		 	$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_id' => $student_id)
													),
												)

										));	
		 
		 $allExamAr = $this->Examination->find('all', array(
																						 'conditions' => array(
																										 'Examination.isdeleted' => 0,  
																										 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																										 'Examination.examination_category_id' => $examination_category_id
																							 )));
		//pr($allExamAr);die;
		 $this->set(compact('studentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student' => $studentDetails['Student']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	
	 public function exams_available($examination_category_id = NULL) {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		 //$this->ExaminationCategory->recursive = 2;
		// echo $student_id;
		 	$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_id' => $student_id)
													),
												)

										));	
		 
		 $allExamAr = $this->Examination->find('all', array(
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));			
																 
		//pr($allExamAr);die;
		 $this->set(compact('studentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student' => $studentDetails['Student']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	
	
	 /* Start Payment functions */
	 
	    public function payment_success() {
			$this->layout = false;
			if ($this->request->is('post') || $this->request->is('put')) {
				   $customString = explode('||', $this->request->data['custom']);
					$ExamIdsAr = explode('#', $customString[0]);
					$eachTotalPriceAr = explode('**', $customString[1]);
					$student_id = $customString[2];
				
				$dateTime = new DateTime('NOW');
				$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
				$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

				$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
				$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
			
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
										$i = 0;
										foreach($ExamIdsAr as $examId){
											$this->request->data['MyExam']['student_id'] = $student_id;
											$this->request->data['MyExam']['examination_id'] = $examId;
											$this->request->data['MyExam']['payment_status'] = 1;
											$this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
											$this->request->data['MyExam']['amount'] = $eachTotalPriceAr[$i];
											
											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
											
											$this->MyExam->create();
											$this->MyExam->save($this->request->data);
												$i++;
										}
							
							   $this->Session->write('Student.transactionData', $this->request->data);
                              $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_success'));
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	 
	 
	 
    public function payment_success__bak() {
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->request->is('post')) {
         //   pr($this->request->data);
           // echo 'next array';
            $payPalCustomData = array();
            $pagedata = explode('##', $this->request->data['custom']);
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
                $this->request->data['MyExam']['student_id'] = $pagedata[0];
                $this->request->data['MyExam']['examination_id'] = $pagedata[1];
                $this->request->data['MyExam']['payment_status'] = 1;
                $this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
                $this->request->data['MyExam']['amount'] = $this->request->data['payment_gross'];
				$this->MyExam->save($this->request->data);
                /* For school payment success */
               // if(!empty($payPalCustomData['student'])){
                    $this->Session->write('Student.transactionData', $this->request->data);
                    $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_success'));
               // }
                
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	 public function examinations($examination_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata = $this->UserConfiguration->getStudentData();
		 $questions = $this->Question->find('all', array('conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)));
		 $this->set(compact('studentdata', 'questions'));
		 pr($questions);
		 die;
    }
    

	
		   public function make_payment($examination_id = NULL){
					$this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'home_layout';
					 $MultipleExamPayment = $this->Session->read('MultipleExamPayment');
					$gross_total = $MultipleExamPayment['gross_total'];
					
					//$examination_id = $this->data_decrypt($examination_id);
					
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
					// $examDtls = $this->Examination->find('first', array('fields' => 'id, price,title','conditions' => array('Examination.id' => $examination_id, 'Examination.isdeleted' => 0)));
					// $amount = $examDtls['Examination']['price'];
					// $title = $examDtls['Examination']['title'];
					 //$examination_id = $examDtls['Examination']['id'];
					 $ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $studentDetails['Student']['first_name'];
						$lastName= $studentDetails['Student']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('USD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						$dateTime = new DateTime('NOW');
						$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
						$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

						$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
						$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								/*$this->request->data['MyExam']['student_id'] = $student_id;
								$this->request->data['MyExam']['examination_id'] = $examination_id;
								$this->request->data['MyExam']['payment_status'] = 1;
								$this->request->data['MyExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
								$this->request->data['MyExam']['amount'] = $amount;
								$this->MyExam->save($this->request->data);*/
								
							   $datas = $MultipleExamPayment['ids'];
									$i = 0;
									foreach($datas as $exam_id){
										$this->request->data['MyExam']['student_id'] = $student_id;
										$this->request->data['MyExam']['examination_id'] =  $exam_id;
										$this->request->data['MyExam']['payment_status'] = 1;
										$this->request->data['MyExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
										$this->request->data['MyExam']['amount'] =  $MultipleExamPayment['total_price'][$i];
										
										$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
										$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
										
										$this->MyExam->create();
										$this->MyExam->save($this->request->data);
										$i++;
									}
									
								$this->redirect(array('controller'=>'students','action'=>'purchase_exam_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							}
						}
						}
							///payment End from here
						}
						
					  $paypalFormSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student_id' => $studentDetails['Student']['id'],
            'examination_id' => @$examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
		
		 $examID = null;
		$datas = $MultipleExamPayment['ids'];
			foreach($datas as $exam_id){
				if (!empty($examID)) {
					$examID .= "";
				}
				$examID .= "#" . $exam_id;
			}
		
		 $eachTotalPrice = null;
		$total_price = $MultipleExamPayment['total_price'];
			foreach($total_price as $val){
				if (!empty($eachTotalPrice)) {
					$eachTotalPrice .= "";
				}
				$eachTotalPrice .= "**" . $val;
			}
		
		// $custom1 = trim($examID, '#').'||'.trim($noOfstudents, '@').'||'.trim($eachTotalPrice, '**').'||'.$SchoolDetails['School']['id'];
		 $custom1 = trim($examID, '#').'||'.trim($eachTotalPrice, '**').'||'.$studentDetails['Student']['id'];
		
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        //$paypalFormSettings['item_name'] = @$title; //Router::url('/', true)
        $paypalFormSettings['item_name'] = 'Purchase Practice Exam(s)';
        //$paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['custom'] = $custom1;
        //$paypalFormSettings['amount'] = @$amount; //Router::url('/', true)
        $paypalFormSettings['amount'] = $gross_total; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
		$this->set(compact('studentDetails', 'examDtls','paypalFormSettings', 'gross_total'));
		   }
    
    public function purchase_exam_success(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
        $this->set(compact('studentDetails'));
        if($this->Session->check('Student.transactionData')){
            
            $transactionData = $this->Session->read('Student.transactionData');
            $this->Session->delete('Student.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $student_id = $pagedata[0]; 
			 $student_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->Student->read(null, $payPalCustomData['student']);
            $this->MyExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
	 public function payment_error(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
        $this->set(compact('studentDetails'));
	 }
	
	
	
	
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->studentLogout();
		 $this->Session->delete('App.studentData');
        $this->Session->setFlash(__('You have logged out.'), 'success');
      //  $this->redirect(array('controller' => 'students', 'action' => 'login'));
        $this->redirect(array('controller' => 'students', 'action' => 'registration'));
    }
    
    protected function _studentLoginRedirect(){
        $isLoggedIn = $this->Session->check('App.studentData');
        if ($isLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
	//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	public function fetch_student_total_correct_answer($random_id = NULL, $student_id = NULL) {
       $this->layout = 'ajax';
            $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0
            )));
        

        $answer_ids = '';
        foreach ($StudentAnswerAr as $val) {
            //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
            $answer_ids = $answer_ids . ',' . $val['StudentAnswer']['answer_id'];
        }
        $answer_ids = trim($answer_ids, ",");
        $answer_ids_ar = explode(',', $answer_ids);
        // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $data = array();
        $data['totalCorrectAnswer']   = $correctAnswer;
        $data['totalAttempQuestions'] = count($StudentAnswerAr);
        return $data;
    }
	
	  public function login_new() {
        $this->layout = 'home_layout';
        //$this->layout = 'landing_layout';
        $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0)));
        $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0)));
        $this->set(compact('examination_types', 'examination_categories'));
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {

            //student login start
            if (!empty($this->request->data['StudentLogin']['login_registration_flag']) && $this->request->data['StudentLogin']['login_registration_flag'] == 'student_login') {
                $login = strtolower($this->request->data['StudentLogin']['login']);
                $password = $this->request->data['StudentLogin']['password'];
                $studentdata = $this->Student->getLoginData($login, $password);

                if (empty(trim($this->request->data['StudentLogin']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['StudentLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($studentdata) && !empty(trim($this->request->data['StudentLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }

                if ($ERROR == 0) {
                    $this->UserConfiguration->setStudentData($studentdata);
                    // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                    $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                    $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
                }
            } else {

                //student login End
                //student registration
                if (empty(trim($this->request->data['Student']['first_name']))) {
                    $ERROR = 1;
                    $this->set('fnerror', 'Please enter first name');
                }

                if (empty(trim($this->request->data['Student']['last_name']))) {
                    $ERROR = 1;
                    $this->set('lnerror', 'Please enter last name');
                }

                if (empty(trim($this->request->data['Student']['address']))) {
                    $ERROR = 1;
                    $this->set('adderror', 'Please enter address');
                }
                if (empty(trim($this->request->data['Student']['school_name']))) {
                    $ERROR = 1;
                    $this->set('scerror', 'Please enter school name');
                }


                if (empty(trim($this->request->data['Student']['email']))) {
                    $ERROR = 1;
                    $this->set('emerror', 'Please enter email');
                } else {
                    $email = $this->request->data['Student']['email'];
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $ERROR = 1;
                        $this->set('emerror', 'Please enter valid email format');
                    }
                }

                if (!empty(trim($this->request->data['Student']['email']))) {
                    $email = $this->request->data['Student']['email'];
                    $ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
                    if ($ExistEmail > 0) {
                        $ERROR = 1;
                        $this->set('emerror', 'This email already exist, please try another');
                    }
                }


                if (empty(trim($this->request->data['Student']['password']))) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Please enter password');
                }
                if (!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) < 6) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Password must be 6 to 20 characters');
                }
                if (!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) > 20) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Password must be 6 to 20 characters');
                }


                if (empty(trim($this->request->data['Student']['confirm_password']))) {
                    $ERROR = 1;
                    $this->set('cpwderror', 'Please enter confirm password');
                }


                $pass = $this->request->data['Student']['password'];
                $cpass = $this->request->data['Student']['confirm_password'];
                if (!empty($pass) && !empty($cpass) && $this->request->data['Student']['password'] != $this->request->data['Student']['confirm_password']) {
                    $ERROR = 1;
                    $this->set('cpwderror', 'Password and confirm password does not match');
                }

                if (empty(trim($this->request->data['Student']['captcha_txt']))) {
                    $ERROR = 1;
                    $this->set('captchaErr', 'Please enter captcha code');
                } else {
                    if (strcasecmp($this->data['Student']['captcha_txt'], $this->Session->read('captcha')) != 0) {
                        $ERROR = 1;
                        $this->set('captchaErr', 'Please enter correct captcha code ');
                    }
                }




                if ($ERROR == 0) {
                    $this->request->data['Student']['username'] = $this->request->data['Student']['email'];
                    $this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
                    $this->Student->save($this->request->data);

                    $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
                    $this->redirect(array('controller' => 'students', 'action' => 'registration'));
                   
                }
            }
        }
    }
	
}
?>