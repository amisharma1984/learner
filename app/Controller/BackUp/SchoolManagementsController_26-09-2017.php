<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CommonUtilitiesHelper', 'View/Helper');

class SchoolManagementsController extends AppController {

    public $name = 'SchoolManagements';
    public $uses = array('Setting', 'Admin', 'School','Attachment', 'SchoolInformation');
    public $components = array('FilterSchools');
    public $settingData = array();
    public $commonUtilities;
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const DEFAULTVALIDITY = 100;
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->commonUtilities = new CommonUtilitiesHelper(new View());
        $this->UserConfiguration->isAdminLoggedIn();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function index() {
        $this->layout = 'admin';
        
        $schools = $this->FilterSchools->getSchools($this);
        if ($this->request->is('requested')){
            return $schools;
        }
        $this->set(compact('schools'));
        $this->getSchoolFilterOptions();
    }
    
    public function add() {
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            $dataToSave = array();
            
            if(!empty($this->request->data['School'])){
                $message = '';
                
                $schoolData = $this->School->findByEmail($this->request->data['School']['email']);
                if(!empty($schoolData)){
                    echo json_encode(array('status' => 'Error', 'message' => 'Email already in use.'));
                    exit();
                }else{
                    $dataToSave['School'] = $this->request->data['School'];
                    $password = $this->_randomPassword(8);
                    $dataToSave['School']['password'] = $password;
                    $dataToSave['School']['isdeleted'] = 1;
                    $dataToSave['School']['created'] = date('YmdHis');               

                    $this->School->create();
                    if ($this->School->save($dataToSave)) {
                        
                        if(!empty($this->request->data['SchoolInformation'])){
                            $this->request->data['SchoolInformation']['url'] = (!empty($this->request->data['SchoolInformation']['url']))?$this->commonUtilities->getUrlWithProtocol($this->request->data['SchoolInformation']['url']):'';
                            $this->request->data['SchoolInformation']['school_id'] = $this->School->id;
                            $this->request->data['SchoolInformation']['created'] = date('YmdHis');
                            $this->request->data['SchoolInformation']['isdeleted'] = 1;
                            $dataForInformation = $this->request->data['SchoolInformation'];
                            $this->SchoolInformation->create();
                            $this->SchoolInformation->save($dataForInformation);
                        }
                        $message = 'School information saved successfully.';
                        echo json_encode(array(
                            'status' => 'Success',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen($message)
                            ));
                        exit();
                    }else{
                        $message = 'School information not saved properly.';
                        echo json_encode(array(
                            'status' => 'Error',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen($message)
                            ));
                        exit();
                    }
                }    
            }
            
        }  else {
            $this->layout = 'admin';
        }
        
    }
    
    public function view(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $schoolId = $this->request->data['schoolId'];
            $schoolDetails = $this->School->findById($schoolId);
            echo json_encode($schoolDetails);
            exit();
        }else{
            $this->layout = 'admin';
        }
        
    }
    
    public function edit($schoolId = null, $schoolInfoId = null){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            if(!empty($this->request->data)){
                $dataToUpdate = array();
                $changedEmail = $this->request->data['School']['email'];
                
                $schoolDetails = $this->School->find('count', array(
                    'conditions' => array(
                        'School.email' => $changedEmail,
                        'School.id !=' => $schoolId,
                        'School.isdeleted' => 0
                        )
                    )
                );
                
                if($schoolDetails > 0){                    
                    $message = 'Email already in use.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    $dataToUpdate['School'] = $this->request->data['School'];
                    $dataToUpdate['School']['modified'] = date('YmdHis');
                    
                    $this->School->read(null, $schoolId);
                    $this->School->set($dataToUpdate['School']);
                    $this->School->save();
                    
                    $dataForInfo = array();
                    if(!empty($schoolInfoId) && !empty($this->request->data['SchoolInformation'])){
                        $dataForInfo = $this->request->data['SchoolInformation'];
                        $dataForInfo['modified'] = date('YmdHis');
                                
                        $this->SchoolInformation->read(null, $schoolInfoId);
                        $this->SchoolInformation->set($dataForInfo);
                        $this->SchoolInformation->save();
                    }
                    
                    $message = 'School information updated successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
                                
            }else{
                $schoolDetails = $this->School->findById($schoolId);
                echo json_encode($schoolDetails);
                exit();
            }
            
        }else{
            $this->layout = 'admin';
        }
        
    }

    public function delete(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $allIds = $this->request->data['schoolIds'];
            $schoolIds = explode(',', $allIds);
            
            foreach ($schoolIds as $schoolId){
                
                $schoolData = $this->School->read(null, $schoolId);
                $this->School->set(array(
                    'isdeleted' => 1
                ));
                $this->School->save();
                $this->School->clear();
                
                $schoolInfoData = $this->SchoolInformation->read(null, $schoolData['SchoolInformation']['id']);
                $this->SchoolInformation->set(array(
                    'isdeleted' => 1
                ));
                $this->SchoolInformation->save();
                $this->SchoolInformation->clear();
            }
            echo json_encode($schoolIds);
            exit();
        }else{
            $this->layout = 'admin';
        }
        
    }
    
    public function send_login_credentials(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $schoolId = $this->request->data['schoolId'];
            
            $schoolDetails = $this->School->findById($schoolId);
            
            if(!empty($schoolDetails)){
                if(!empty($schoolDetails['School']['issent'])){
                    
                }else{
                
                    if($this->_sendSchoolRegistrationEmail($schoolDetails, $schoolDetails['School']['id'], $schoolDetails['School']['password'])){
                        
                        $this->School->read(null, $schoolId);
                        $this->School->set(array(
                            'issent' => 1,
                            'isdeleted' => 0,
                            'password' => md5($schoolDetails['School']['password'])
                        ));
                        $this->School->save();
                        $this->School->clear();
                        
                        $message = 'School credentials sent successfully.';
                        echo json_encode(array(
                            'status' => 'Success',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen($message)
                            ));
                        exit();
                        
                    }else{
                        $message = 'Can\'t send credentials email to: '.$schoolDetails['School']['email'];
                        echo json_encode(array(
                            'status' => 'Error',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen($message)
                            ));
                        exit();
                    }
                }
                
            }else{
                $message = 'School Not Found.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    ));
                exit();
            }            
        }
    }

    /**
     * Search action
     * Redirect to main action mapping the search form to a named param
     * TODO Review to make search form call index direct
     */
    public function search() {
        $this->layout = 'admin';
        $url = array('controller' => 'school_managements', 'action' => 'index');
        if (!empty($this->request->data['School']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['School']['search'])));
        $this->redirect($url);
    }

    /**
     * Return only totals of result filtered [Internal]
     */
    public function totals() {
        $options = array();
        $options['conditions'] = $this->FilterSchools->getConditions($this);
        $options['order'] = array('School.created' => 'DESC');
        $options['recursive'] = 0;
        $out = $this->School->find('count', $options);
        return $out;
    }
    
    protected function _sendSchoolRegistrationEmail($schoolData = array(), $school_id = null, $password = null) {
        
        $school = $schoolData['School'];
        
        $domainURL = Router::url('/', true).'naplan_exam'; //Configure::read('App.fullUrl');
        $app_name = Configure::read('App.appName');
        CakeLog::write('debug', "Sending email to: ".$school['email']);
        
        $email = new CakeEmail();
        $email->template('register', 'welcome');
        $email->to($school['email']);
        $email->subject(__('School registration at '.$app_name));
        $email->viewVars(array(
            'username' => $school['email'],
            'password' =>$password,
            'fullName' => $school['name'],
            'schoolId' => $school_id,
            'full_url' =>$domainURL,
            'validity' => (!empty($this->settingData['Setting']['school_registration_validity'])) ? $this->settingData['Setting']['school_registration_validity'] : self::DEFAULTVALIDITY,
            'registrationFee' => (!empty($this->settingData['Setting']['school_registration_fee'])) ? $this->settingData['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT
        ));
        //$email->replyTo($this->from_default);
        $email->from(Configure::read('App.emailFromDefault'));
        $email->domain(Configure::read('App.domainName'));
        $email->emailFormat('html');

        if($email->send()){
            CakeLog::write('debug', "Email sent to: ".$school['email']);
            return true;
        }else{
            CakeLog::write('error', "Can't send email to: ".$school['email']);
            return false;
        }
    }
        
}
?>