<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class OnlineExamsController extends AppController {

    public $name = 'OnlineExams';
    public $uses = array(
										'Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer', 
										'Answer', 'MyExamStudentAnswer', 'ExaminationType', 'ExaminationCategory',
										'SchoolPurchaseExam', 'SchoolTakeExam'
										);
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    

	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	
	
    public function login() {
         $this->layout = 'home_layout';
		 $this->_onlineExamLoginRedirect();
		$ERROR = 0;
       if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			//pr($this->request->data);die;
            $email = strtolower($this->request->data['Student']['email']);
            //$password = $this->request->data['Student']['password'];
            $exam_code = $this->request->data['Student']['exam_code'];
			 //$studentdata = $this->Student->getLoginData($login, $password);
					  $examCodeData = $this->SchoolPurchaseExam->find('first', array(
										 'conditions' => array(
																			'SchoolPurchaseExam.exam_code' => $exam_code,
														'SchoolPurchaseExam.isdeleted' => 0,
														'SchoolPurchaseExam.isexpired' => 0,
														'SchoolPurchaseExam.payment_status' => 1,
											)));
													 
				/*$examCodeData = $this->SchoolTakeExam->find('first', array(
													'conditions' => array(
																'SchoolTakeExam.exam_code' => $exam_code,
																'SchoolTakeExam.isdeleted' => 0,
																'SchoolTakeExam.isexpired' => 0,
															 )));*/																								 
															 
			      if(!empty($examCodeData)){
							 $this->Session->write('SchoolPurchaseExam', $examCodeData);
							// $this->Session->write('SchoolTakeExam', $examCodeData);
							// list($school_id, $schoolName, $examination_id, $school_take_exam_id) = explode('-', $this->request->data['Student']['exam_code']);
							 list($school_id, $schoolName, $examination_id) = explode('-', $this->request->data['Student']['exam_code']);
						 }
					
					/*$studentdata = $this->Student->find('first', array(
															'conditions' => array(
																	'Student.email' => $email,
																	'Student.isdeleted' => 0,
																	'Student.school_id' => $school_id
															 )));*/
															 
															 

						$schoolNameAr = $this->School->find('first', array(
															'fields' => 'id, name',			
															'conditions' => array(
																	'School.id' => @$school_id
															 )));
															 
								
								$NoOfStudent = $this->SchoolPurchaseExam->find('first', array(
															'fields' => 'id, no_of_student',			
															'conditions' => array(
																	'SchoolPurchaseExam.school_id' => @$school_id,
																	'SchoolPurchaseExam.examination_id' => @$examination_id,
																	'SchoolPurchaseExam.isdeleted' => 0,
																	'SchoolPurchaseExam.isexpired' => 0,
															 )));
															 
								$NoOfStudentLogin = $this->Student->find('all', array(
															'fields' => 'id',			
															'conditions' => array(
																	'Student.school_id' => @$school_id,
																	'Student.examination_id' => @$examination_id,
																	'Student.isdeleted' => 0,
															 )));
															 
								$no_of_student = @$NoOfStudent['SchoolPurchaseExam']['no_of_student'];	

								//echo 'no_of_student'.$no_of_student.'<br>';
							//	echo 'NoOfStudentLogin'.count($NoOfStudentLogin).'<br>';
														//die;	 	
							if(count($NoOfStudentLogin) >= $no_of_student && !empty($NoOfStudentLogin) && !empty($no_of_student)){
								$ERROR = 1;
								$this->set('exceedErr', 'Exceed the limit of students allowed for this exam, please check with school administrator.');
							}
								
							
						if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('firstNameErr', 'Please enter your first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lastNameErr', 'Please enter your last name');
							}
						if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('loginEmailErr', 'Please enter valid email format');
								}
							} 
							
							
							
						if(empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Please enter exam code');
							}
							
						
							
						/*if(empty($studentdata) && !empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Invalid email or exam code');
							}*/
							
						if(empty($examCodeData) && !empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Please enter valid exam code');
							}	
			
			if($ERROR == 0){
				 $this->request->data['Student']['school_id'] = $school_id;
				 $this->request->data['Student']['examination_id'] = $examination_id;
				 $this->request->data['Student']['school_name'] = $schoolNameAr['School']['name'];
				 $this->Student->save($this->request->data);
				 $lastInserStudentId = $this->Student->getInsertID();
				 $studentdata = $this->Student->find('first', array(
															'conditions' => array(
																	'Student.id' => $lastInserStudentId
															 )));
				 
				 
				 $this->UserConfiguration->setOnlineExamStudentData($studentdata);
                // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'online_exams', 'action' => 'exam_instruction',));
			} 
        }  
    }
    
     public function exam_instruction() {
         $this->layout = 'home_layout';
		 $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
	 }
	 
	   public function second_section_exam() {
         $this->layout = 'home_layout';
		 $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
	  }
	 
	   public function thank_you() {
         $this->layout = 'home_layout';
		  $this->Session->delete('App.onlineExamStudentData');
		  $this->Session->delete('SchoolPurchaseExam');
		   //$this->Session->delete('SchoolTakeExam');
		 //$this->UserConfiguration->isStudenOnlineExamtLoggedIn();
	 }
	
	
	  protected function _onlineExamLoginRedirect(){
       // $isLoggedIn = $this->Session->check('App.schoolData');
        $isOnlineExamLoggedIn = $this->Session->check('App.onlineExamStudentData');
        if ($isOnlineExamLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'online_exams', 'action' => 'exam_instruction'));
        }else{
            return;
        }
    }
	
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		 
		 $RandomID = $this->Session->read('RandomID');
		
		  $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
		  $school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		  $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		  $examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
		
	
	
		  
		  //$schoolExamData =  $this->Session->read('SchoolTakeExam');
		  //$school_id = $schoolExamData['SchoolTakeExam']['school_id'];
		  
		 //$school_take_exam_id = $schoolExamData['SchoolTakeExam']['id'];
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			 
			if($this->request->data['page_action'] == 'take_exam_second_section'){
					$section = 'exam_section_1_name';
					$QuestionLimit =  $no_of_qs_20['Setting']['value'];
			} else {
					$section = 'exam_section_2_name';
					$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			}
		  $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		  
			 
			 
			 
			 
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				if(empty($MyExamStudentAnswerExist)){
					$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					//$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
					//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2;
					//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  count($QuestionAns);
					$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] =  $unique_flag;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				}else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
			 
			  //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
						$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$this->request->data['StudentAnswer']['student_id'] = $student_id;
						$this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				}
			}
			 
			 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			// die;
			//$this->Session->setFlash('Data has been submitted successfully.');
			if($this->request->data['page_action'] == 'take_exam_second_section'){
					$this->redirect(array('controller' => 'online_exams', 'action' => 'thank_you'));
			} else {
					$this->redirect(array('controller' => 'online_exams', 'action' => 'second_section_exam'));
			}
		
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			  /*$schoolExamData =  $this->Session->read('SchoolTakeExam');
		       $school_id = $schoolExamData['SchoolTakeExam']['school_id'];
		       $school_take_exam_id = $schoolExamData['SchoolTakeExam']['id'];*/
			    $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		        $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
				  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				if($this->request->data['page_action'] == 'take_exam_second_section'){
					$section = 'exam_section_1_name';
					$QuestionLimit =  $no_of_qs_20['Setting']['value'];
				
				} else {
					$section = 'exam_section_2_name';
					$QuestionLimit =  $no_of_qs_23['Setting']['value'];
				}
		  $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		  
					$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				if(empty($MyExamStudentAnswerExist)){
						$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					   //$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
						//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
						$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id; //school purchase id
						//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] = count($QuestionAns);
						$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
						$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2; //online taken by school 
						$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
						$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] = $unique_flag;
						$this->MyExamStudentAnswer->save($myExamStudentAns);
					} else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
				
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 
			  	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
								$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
								$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
								$this->request->data['StudentAnswer']['student_id'] = $student_id;
								$this->request->data['StudentAnswer']['question_id'] = $question_id;
								$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
								$this->request->data['StudentAnswer']['random_id'] = $RandomID;
								$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
								$this->StudentAnswer->create();
								$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

						$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$questionData['StudentAnswer']['student_id'] = $student_id;
						$questionData['StudentAnswer']['question_id'] = $qid;
						$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
						$questionData['StudentAnswer']['random_id'] = $RandomID;
						$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	  
	
	 public function take_exam($flag = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'take_exam_layout';
		$flag = $this->data_decrypt($flag);
		//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		if($flag == 'NON-CALCULATOR'){
			$section = 'exam_section_2_name';
		} else if($flag == 'CALCULATOR'){
			$section = 'exam_section_1_name';
		} else {
			$section = 'nothing'; //false
		}
		
		
		
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		/* $schoolExamData =  $this->Session->read('SchoolTakeExam');
		 $examination_id =  $schoolExamData['SchoolTakeExam']['examination_id'];
		 $myExamID = $schoolExamData['SchoolTakeExam']['school_purchase_exam_id'];*/
		       $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		        $myExamID =  $schoolExamData['SchoolPurchaseExam']['id'];
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		 $this->set('my_exam_id', $myExamID);
		 
		 
		
		$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		//$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$examDuration = $settingsData21['Setting']['value'];
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
																	 
		// pr($ExamDetails);die; exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		// echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		//pr($QuestionAns);die;
		// echo $this->Session->read('totalQuestions');die;
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		 //$this->render('../Students/take_exam');
		
    }
	
	
	
	 public function take_exam_second_section($flag = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'take_exam_layout';
		$flag = $this->data_decrypt($flag);
		//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		if($flag == 'CALCULATOR'){
			$section = 'exam_section_1_name';
		} else if($flag == 'NON-CALCULATOR'){
			$section = 'exam_section_2_name';
		} else {
			$section = 'nothing'; //false
		}
		
		
		
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		/* $schoolExamData =  $this->Session->read('SchoolTakeExam');
		 $examination_id =  $schoolExamData['SchoolTakeExam']['examination_id'];
		 $myExamID = $schoolExamData['SchoolTakeExam']['school_purchase_exam_id'];*/
		       $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		        $myExamID =  $schoolExamData['SchoolPurchaseExam']['id'];
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		 $this->set('my_exam_id', $myExamID);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$examDuration = $settingsData24['Setting']['value'];
		$QuestionLimit =  $no_of_qs_23['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die; exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		 $this->render('take_exam');
		
    }
	
	
	

    public function logout() {
        $this->UserConfiguration->studentOnlineExamLogout();
		 $this->Session->delete('App.onlineExamStudentData');
		 $this->Session->delete('SchoolTakeExam');
		// $this->Session->delete('SchoolPurchaseExam');
        $this->Session->setFlash(__('You have logged out.'), 'success');
      //  $this->redirect(array('controller' => 'students', 'action' => 'login'));
        $this->redirect(array('controller' => 'online_exams', 'action' => 'login'));
    }
    
	
}
?>