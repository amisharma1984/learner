<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');

class HomesController extends AppController {

    public $name = 'Homes';
    public $uses = array('Student','ContentManagement','EmailTemplate','Examination', 'Question','ExaminationCategory');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
				public function beforeFilter() {
					parent::beforeFilter();
				 //   $this->settingData = $this->Setting->getSettingsData();
				}
    
	
    
	
	public function subscribe_news_letter() {
					$this->layout = 'landing_layout';
					$this->Session->delete('emailErr');
					$ERROR = 0;
					 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
						// pr($this->data);die;
						if(empty(trim($this->request->data['User']['email']))){
								$ERROR = 1;
								$this->Session->write('emailErr', 'Please enter email');
								$this->redirect($this->referer());
							} else {
								$email = $this->request->data['User']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->Session->write('emailErr', 'Please enter valid email format');
									$this->redirect($this->referer());
								}
							} 
							
							if($ERROR == 0){
								 $this->Session->setFlash(__('Your email has been subscribed successfully.'), 'success');
								$this->redirect($this->referer());
							}
					 }
				}
	
				public function index() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				
				
				
				
				 public function contact_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower('Contact us')));
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
					 
					  if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
						 // pr($this->data);die;
                        $mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 10)));
                        $html_content = $mailTemplateData['EmailTemplate']['content'];
                        $html = '';
                        $html .= str_replace('{{name}}', $this->request->data['name'], $html_content);
                        $html = str_replace('{{email}}', $this->request->data['email'], $html);
                        $html = str_replace('{{mobile}}', $this->request->data['mobile'], $html);
                        $html = str_replace('{{address}}', $this->request->data['address'], $html);
                        $html = str_replace('{{contact_info}}', nl2br($this->request->data['other_info']), $html);
                        $email_subject = $mailTemplateData['EmailTemplate']['title'];
						//$admin_email = 'dinesh.amstech@gmail.com';
                        $email = $this->getAdminValue('contact_email');
                        $mailFrom = $this->request->data['email'];
                        $FromName = $this->request->data['name'];

                        $mail = new PHPMailer;
                        $mail->FromName = $FromName;
                        $mail->From = $mailFrom;
                        $mail->Subject = $email_subject;
                        $mail->Body = stripslashes($html);
                        $mail->AltBody = stripslashes($html);
                        $mail->IsHTML(true);
                        $mail->AddAddress($email);
                        $mail->Send();
						 $this->Session->setFlash(__('Email has been successfully sent to site admin.'), 'success');
						
					  }
					 
					 
					 
				}
				
				
				
				
				
			
				
				 public function high_school_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 8,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																					 'Examination.examination_category_id' => array(38,39)
																		 )));
					$this->set(compact('allExamAr'));
					$this->render('parent_student_page');
				}
				
				public function primary_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 7,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																					 'Examination.examination_category_id' => array(42,43)
																		 )));
					
					 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => array(42,43)
																 )));
					
					
					
					
					//pr($allExamAr);
					$this->set(compact('allExamAr'));
					$this->render('parent_student_page');
					
				}
				
				 public function high_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 6,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 1, //for online /school
																					 'Examination.examination_category_id' => array(36,37)
																		 )));
					$this->set(compact('allExamAr'));
					
					$this->render('school_content_page');
					//$this->render('common_content_display');
				}
				
				 public function primary_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 5,'ContentManagement.isdeleted' => 0)));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 1, //for online /school
																					 'Examination.examination_category_id' => array(40,41)
																		 )));
					$this->set(compact('allExamAr','cmsData'));
					$this->render('school_content_page');
					//$this->render('common_content_display');
				}
				
				
				
				
				public function sample_questions($examination_id = NULL) {
					//$this->layout = 'landing_layout';
					 $this->layout = 'take_exam_layout';
					 $examination_id = $this->data_decrypt($examination_id);
					 $this->Examination->recursive = 0; 
					 $examDuration = 40;//minutes
					 $ExamDetails = $this->Examination->find('first', array( 
																				'fields' => 'id, title, description',	
																				 'conditions' => array('Examination.id' => $examination_id)
																				 ));
					
					 $QuestionAns = $this->Question->find('all', array( 
																				'limit' => 8,
																				'offset' => 15,
																				'order' => 'Question.question_order_front ASC',
																				 'conditions' => array(
																				 'Question.examination_id' => $examination_id,
																				 //'Question.section' => $section,  
																				 'Question.isdeleted' => 0
																				 )
																				 ));
					
					
					 $this->set(compact('QuestionAns','examDuration'));
				}
				
				
				 public function about_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
					 
					 $cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 4,'ContentManagement.isdeleted' => 0)));
					 $this->set(compact('cmsData'));
					 $this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					 $this->render('common_content_display');
				}
				
				 public function get_about_us_content() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'ajax';
					 $cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 4,'ContentManagement.isdeleted' => 0)));
					 return $cmsData;
					// pr($cmsData);
				}
				
				
				 public function company_information() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Company information');
					$this->set('page_heading', 'Company information');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 3,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				
				 public function terms_of_service() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Terms of service');
					$this->set('page_heading', 'Terms of service');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 2,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function privacy_policy() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Privacy policy');
					$this->set('page_heading', 'Privacy policy');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function we_offer($page = NULL) {
					$this->layout = 'landing_layout';
					//$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					//$this->set(compact('cmsData'));
					
					$page = str_replace('-', ' ', $page);
					$this->set('title_for_layout', 'We offer '.$page);
					$this->set('page_heading', $page);
					
					$this->render('common_content_display');
				}
				
				
				
				
	
}
?>