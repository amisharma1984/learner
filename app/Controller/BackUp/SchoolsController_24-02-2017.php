<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SchoolsController extends AppController {

    public $name = 'Schools';
    public $uses = array('Setting', 'School');
    public $components = array('FilterSchools');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function login() {
        $this->layout = false;
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['School']['login']);
            $password = $this->request->data['School']['password'];
            $userdata = $this->School->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setSchoolData($userdata);
                $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'schools', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'schools', 'action' => 'login'));
            }
        }  else {
            $this->_schoolLoginRedirect();
        }
    }
    
    public function index() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        //$this->autoRender = false;
        
        
    }
	
	public function registration() {
        $this->layout = 'home_layout';
		$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		   $schoolLevelOptions = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
        
        
        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));
       
		  $this->set(compact('examination_types', 'examination_categories','schoolLevelOptions','types'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
						
							
							if(empty(trim($this->request->data['School']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['School']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['School']['username'] = $this->request->data['School']['email'];
					$this->School->save($this->request->data['School']);
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'Schools', 'action' => 'registration'));
				}
				
			 }
	  }

	
    
    public function view(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function edit($schoolId = null){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
        
    }

    public function registration_payment(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        
        $timeStamp = time();
        $paymentAmount = (!empty($settings['Setting']['school_registration_fee'])) ? $settings['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT;
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY, true);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY));
        }      
        
        $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
            'item_name' => 'School Registration',
            'item_number' => str_pad($schoolData['School']['id'], 10, "0", STR_PAD_LEFT),
            'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'school' => $schoolData['School']['id'],
            'payment_for' => 'registration',
            'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
            'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'app/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'app/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'app/payment_cancle'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);        
    }
    
    public function checkFingerprint(){
        
        $this->layout = false;
        $this->autoRender = false;
        $flag = false;
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        
        $extraData = $this->request->data['custom'];
        $amount = $this->request->data['amount'];
        
        $payPalCustomData = array();
        $pagedata = explode('/', $extraData);
        foreach ($pagedata as $key => $value) {
            $exploaded = explode(':', $value);
            $payPalCustomData[$exploaded[0]] = $exploaded[1];
        }
        
        $oldFingerprint = urldecode($payPalCustomData['fp']);
        
        if (phpversion() >= '5.1.2') {
            $newFingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY, true);
        } else {
            $newFingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY));
        }
        
        if($oldFingerprint != $newFingerprint){
            $flag = true;
        }
        echo $flag;
        exit();
    }

    public function registration_success(){
        $this->layout = false;
        
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('/', $transactionData['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
            $endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            $fingerPrint = urldecode($payPalCustomData['fp']);
            
            $this->School->read(null, $payPalCustomData['school']);
            $this->School->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            $this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'schools', 'action' => 'login'));
        }
        
    }
    
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->schoolLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'schools', 'action' => 'login'));
    }
    
    protected function _schoolLoginRedirect(){
        $isLoggedIn = $this->Session->check('App.schoolData');
        if ($isLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'schools', 'action' => 'index'));
        }else{
            return;
        }
    }
}
?>