<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class StudentsController extends AppController {

    public $name = 'Students';
    public $uses = array('Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function login() {
        $this->layout = false;
		
        if (!empty($this->request->data)) {
			//pr($this->request->data);die;
            $login = strtolower($this->request->data['Student']['login']);
            $password = $this->request->data['Student']['password'];
            $studentdata = $this->Student->getLoginData($login, $password);
            if (!empty($studentdata)) {
                $this->UserConfiguration->setStudentData($studentdata);
                $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'index'));
              //  $this->redirect(array('controller' => 'students', 'action' => 'take_exam_list'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'students', 'action' => 'login'));
            }
        }  else {
            $this->_studentLoginRedirect();
        }
    }
    
    public function index() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $examinations = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		 $this->set(compact('studentDetails', 'examinations'));
		 $this->render('purchase_exam');
    }
	
	
	
	 
	
	
	
	 public function my_exam() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $examinations = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		 $this->set(compact('studentDetails', 'examinations'));
		
    }
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 
			// pr($this->request->data);die;
			 
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
			 }
			 
			$this->Session->setFlash('Data has been submitted successfully.');
			$this->redirect(array('controller' => 'students', 'action' => 'take_exam_list'));
			   
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
			 }
		
		 }
		 
	  }
	  
	
	 public function take_exam($examination_id = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $this->Session->write('examination_id', $examination_id);
		
		$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$examDuration = $settingsData21['Setting']['value'];
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	 'conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		
    }
	
	
	
	 public function take_exam_list() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $this->MyExam->recursive = 2;
		 $examinations = $this->MyExam->find('all', array('conditions' => array('MyExam.isdeleted' => 0)));
		// pr($examinations);die;
		 $this->set(compact('studentDetails', 'examinations'));
		
    }
	
	 public function examinations($examination_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata = $this->UserConfiguration->getStudentData();
		 $questions = $this->Question->find('all', array('conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)));
		 $this->set(compact('studentdata', 'questions'));
		 pr($questions);
		 die;
    }
    
    public function view(){
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function edit($StudentId = null){
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
        
    }

    public function registration_payment(){
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = false;
        $settings = $this->_getSettingsData();
        $studentData = $this->UserConfiguration->getStudentData();
        
        $timeStamp = time();
        $paymentAmount = (!empty($settings['Setting']['student_registration_fee'])) ? $settings['Setting']['student_registration_fee'] : self::DEFAULTPAYMENTAMOUNT;
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $studentData['Student']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY, true);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $studentData['Student']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY));
        }      
        
        $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
            'item_name' => 'Student Registration',
            'item_number' => str_pad($studentData['Student']['id'], 10, "0", STR_PAD_LEFT),
            'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student' => $studentData['School']['id'],
            'payment_for' => 'registration',
            'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
            'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'app/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'app/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'app/payment_cancle'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);        
    }
    
    public function checkFingerprint(){
        
        $this->layout = false;
        $this->autoRender = false;
        $flag = false;
        $settings = $this->_getSettingsData();
        $StudentData = $this->UserConfiguration->getStudentData();
        
        $extraData = $this->request->data['custom'];
        $amount = $this->request->data['amount'];
        
        $payPalCustomData = array();
        $pagedata = explode('/', $extraData);
        foreach ($pagedata as $key => $value) {
            $exploaded = explode(':', $value);
            $payPalCustomData[$exploaded[0]] = $exploaded[1];
        }
        
        $oldFingerprint = urldecode($payPalCustomData['fp']);
        
        if (phpversion() >= '5.1.2') {
            $newFingerprint = hash_hmac("md5", $studentData['Student']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY, true);
        } else {
            $newFingerprint = bin2hex(mhash(MHASH_MD5, $StudentData['Student']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY));
        }
        
        if($oldFingerprint != $newFingerprint){
            $flag = true;
        }
        echo $flag;
        exit();
    }

    public function registration_success(){
        $this->layout = false;
        
        if($this->Session->check('Student.transactionData')){
            
            $transactionData = $this->Session->read('Student.transactionData');
            $this->Session->delete('Student.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('/', $transactionData['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $StudentRegistrationValidity = (!empty($this->settingData['Setting']['student_registration_validity']))?$this->settingData['Setting']['student_registration_validity']:100;
            $endDate = strtotime ( '+'.$studentRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            $fingerPrint = urldecode($payPalCustomData['fp']);
            
            $this->Student->read(null, $payPalCustomData['student']);
            $this->Student->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            $this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->studentLogout();
		 $this->Session->delete('App.studentData');
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'students', 'action' => 'login'));
    }
    
    protected function _studentLoginRedirect(){
        $isLoggedIn = $this->Session->check('App.studentData');
        if ($isLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'students', 'action' => 'index'));
        }else{
            return;
        }
    }
}
?>