<?php

App::uses('AppController', 'Controller');

class AdminsController extends AppController {

    public $name = 'Admins';
    public $uses = array('Admin');
        
    public function index() {
        $this->layout = 'login';
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['Admin']['login']);
            $password = $this->request->data['Admin']['password'];
            $userdata = $this->Admin->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setAdminData($userdata);
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'index'));
            }
        }else{
            $adminData = $this->UserConfiguration->getAdminData();
            if (!empty($adminData)) {
                $this->Session->setFlash(__('You have already logged in.'), 'info');
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            }
        }
    }
    
    public function login() {
        $this->layout = 'login';       
        
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['Admin']['login']);
            $password = $this->request->data['Admin']['password'];
            $userdata = $this->Admin->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setAdminData($userdata);
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'login'));
            }
        }else{
            $adminData = $this->UserConfiguration->getAdminData();
            if (!empty($adminData)) {
                $this->Session->setFlash(__('You have already logged in.'), 'info');
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            }
        }
    }

    public function home() {
        $this->layout = 'admin';
        $admindata = $this->UserConfiguration->getAdminData('App.adminData');
        if (empty($admindata)) {
            $this->Session->setFlash(__('You are logged out!'), 'info');
            $this->redirect(array('controller' => 'admins', 'action' => 'login'));
        }
    }

    public function logout() {
        $this->UserConfiguration->adminLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'admins', 'action' => 'login'));
    }

    public function profile() {
        
        $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
        if($this->request->is('post') || $this->request->is('put')){
            
            if(empty($this->request->data['Admin']['id'])){
                $this->Session->setFlash(__('Internal error. Please try again.'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
            }
            
            $updatedPassword = '';
            if(!empty($this->request->data['Admin']['password'])){
                if ($this->request->data['Admin']['password'] != $this->request->data['Admin']['repaetpassowrd']) {
                    $this->Session->setFlash(__('Password mismatch, please enter same password in Repeat-Password field.'), 'error');
                    $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
                }else{
                    $updatedPassword = md5(trim($this->request->data['Admin']['password'])); 
                }
            }
            
            $dataToUpdate = array(
                'first_name' => trim($this->request->data['Admin']['first_name']),
                'last_name' => trim($this->request->data['Admin']['last_name']),
                'email' => trim($this->request->data['Admin']['email']),
                'mobile' => trim($this->request->data['Admin']['mobile']),
                'modified' => date('YmdHis')
            );
            
            if(!empty($updatedPassword)){
                $dataToUpdate['password'] = $updatedPassword;
            }
            
            $this->Admin->read(null, $this->request->data['Admin']['id']);
            $this->Admin->set($dataToUpdate);
            
            if($this->Admin->save()){
                
                $this->UserConfiguration->setAdminData($this->Admin->read(null, $this->Admin->id));
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
                
            }
                       
        }
            
        if(!empty($adminData)){
            unset($adminData['Admin']['password']);
            $this->request->data = $adminData;
            $this->set('admindata', $adminData);
        }else{
            $this->Session->setFlash(__('You are logged out!'), 'info');
            $this->redirect(array('controller' => 'admins', 'action' => 'login'));
        }
                
    }
    
    public function checkAdminStatus(){
        if($this->request->is('ajax')){
            
            $isLoggedIn = $this->Session->check('App.adminData');
            if (!$isLoggedIn) {        
                $message = 'You have logged out! Please login to continue.';
                echo json_encode(array(
                    'status' => 'Success', 
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();             
            }else{
                $message = 'An internal error occurred.';
                echo json_encode(array(
                    'status' => 'Error', 
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();
            }
            
        }
    }
    
    public function password(){
        
        if($this->request->is('post')){
            
            $isAdmin = $this->Admin->findByEmail(trim($this->request->data['Password']['email']));
            if(!empty($isAdmin)){
                
                $key = Configure::read('Security.salt');
                $encryptedResult = Security::encrypt($isAdmin['Admin']['id'], $key);
                $encryptedResult = urlencode($encryptedResult);
                
                $userData = array(
                    'id' => $isAdmin['Admin']['id'],
                    'email' => $isAdmin['Admin']['email'],
                    'fullName' => $isAdmin['Admin']['first_name'].' '.$isAdmin['Admin']['last_name'],
                    'resetLink' => Router::url('/', true).'admins/password_reset/'.$encryptedResult
                );
                
                if($this->sendPasswordResetEmail($userData, 'Admin')){
                    $this->Session->setFlash(__('Check your email for a link to reset your password. If it doesn\'t appear within a few minutes, check your spam folder.'), 'success');
                    $this->redirect(array('controller' => 'admins', 'action' => 'password'));
                }else{
                    $this->Session->setFlash(__('Email sending error'), 'error');
                    $this->redirect(array('controller' => 'admins', 'action' => 'password'));
                }
            }else{
                $this->Session->setFlash(__('That email address doesn\'t match any user accounts. Are you sure you\'ve registered?'), 'error');
            }
        }
        
        
    }
    
    public function password_reset($resetKey = null){
        
        $resetKey = urldecode($resetKey);
        $this->set('resetKey', urldecode($resetKey));
        if($this->request->is('post') || $this->request->is('put')){
            
            if ($this->request->data['Admin']['password'] != $this->request->data['Admin']['repeatpassowrd']) {
                $this->Session->setFlash(__('Password mismatch, please enter same password in Repeat-Password field.'), 'error');
            }else{
                $updatedPassword = md5(trim($this->request->data['Admin']['password'])); 
                $key = Configure::read('Security.salt');
                $userId = Security::decrypt($resetKey, $key);
                $this->Admin->read(null, $userId);
                $this->Admin->set('password', $updatedPassword);
                $this->Admin->save();
                $this->Session->setFlash(__('Password changed successfully.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'login'));
            }
        }
        
        if(!empty($resetKey)){
            
            $key = Configure::read('Security.salt');
            $userId = Security::decrypt($resetKey, $key);
            $userData = $this->Admin->findById($userId);
            if(!empty($userData)){
                $this->set('userData', $userData);
            }else{
                $this->Session->setFlash(__('Wrong password reset key. Please try again.'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'password'));
            }
        }else{
            $this->Session->setFlash(__('Wrong password reset key. Please try again.'), 'error');
            $this->redirect(array('controller' => 'admins', 'action' => 'password'));
        }
        
    }
}
?>