<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class QuestionManagementsController extends AppController {

    public $name = 'QuestionManagements';
    public $uses = array('Setting', 'Admin', 'School', 'Attachment', 'ExaminationCategory', 'Examination', 'Question', 'Answer', 'Subject');
    public $components = array('FilterSchools', 'FilterExamCategories', 'FilterExamination', 'FilterQuestion', 'KeyValue');
    public $settingData = array();
       
    const DEFAULTPAYMENTAMOUNT = 2;
    const DEFAULTVALIDITY = 100;
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->UserConfiguration->isAdminLoggedIn();
        $this->settingData = $this->Setting->getSettingsData();
        $this->setExaminationCategoriesTree();
        $this->layout = 'admin';
    }
    
    public function index(){
        
    }
    
    public function questions($examId = null){
        
        $this->setExaminationTypesTree();
        $this->set('settingData', $this->settingData);
        $examId = (!empty($this->request->params['named']['exam']))?$this->request->params['named']['exam']:$examId;
        
        if(!empty($examId)){
            $examDetails = $this->Examination->findById($examId);
            //pr($examDetails);exit();
            if(!empty($examDetails)){
                $this->request->data['Question']['examination_title'] = $examDetails['Examination']['title'];
                $this->request->data['Question']['examination_id'] = $examDetails['Examination']['id'];
                $this->request->data['Question']['examination_type_id'] = $examDetails['Examination']['examination_type_id'];
                $this->request->data['Question']['examination_category_id'] = $examDetails['Examination']['examination_category_id'];
            }   
        }
        
        $subjects = $this->Subject->find('list', array(
            'fields' => array('Subject.id', 'Subject.name'),
            'conditions' => array('Subject.isdeleted' => 0),
            'recursive' => -1
        ));
        $this->set(compact('subjects'));
    }
    
    public function create_question(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            $message = '';
            $examinationId = $this->request->data['Question']['examination_id'];
            unset($this->request->data['Question']['examination_title']);
            $this->request->data['Question']['created'] = date('YmdHis');
            
            $sectionName = $this->KeyValue->getValueFromSettings($this->request->data['Question']['section'], $this->settingData);
            $str = $this->request->data['Question']['section'];
            preg_match('!\d+!', $str, $matches);
            $sectionQuestionLimit = $this->KeyValue->getValueFromSettings('exam_section_'.$matches[0].'_questions', $this->settingData);
                        
            $questionCount = $this->Question->find('count', array(
                'conditions' => array(
                    'Question.examination_id' => $examinationId,
                    'Question.section' => $this->request->data['Question']['section'],
                    'Question.isdeleted' => 0
                )
            ));
            
            $questionExistingOrder = $this->Question->field(
                'question_order',
                array(
                    'examination_id' => $examinationId,
                    'section' => $this->request->data['Question']['section'],
                    'isdeleted' => 0
                ),
                'question_order DESC'
            );
            $this->request->data['Question']['question_order'] = $questionExistingOrder + 1;
                        
            //if($questionCount < $sectionQuestionLimit){
                $this->Question->create();
                if ($this->Question->save($this->request->data)) {
                    $message = 'Question data saved successfully.';
                    echo json_encode(array(
                        'status' => 'Success', 
                        'message' => $message,
                        'questionId' => $this->Question->id,
                        'examinationId' => $examinationId,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        )
                    );
                    exit();
                }else{
                    $message = 'Question data not saved successfully.';
                    echo json_encode(array(
                        'status' => 'Error', 
                        'message' => $message,
                        'questionId' => '',
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        )
                    );
                    exit();
                }
//            }else{
//                $message = 'You have reached to the maximum number of question for <b>'.ucfirst($sectionName).'</b>.';
//                echo json_encode(array(
//                    'status' => 'Error', 
//                    'message' => $message,
//                    'questionId' => '',
//                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
//                    'messageLetterCount' => strlen($message)
//                    )
//                );
//                exit();
//            }
        }
    }
    
    public function read_question(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            //pr($this->request->data);
            $questionDetails = $this->Question->findById($this->request->data['questionId']);
            echo json_encode($questionDetails);
            exit();
        }
        
    }
    
    public function update_question(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $questionId = $this->request->data['Question']['id'];
            unset($this->request->data['Question']['examination_title']);
            
            $questionDetails = $this->Question->read(null, $questionId);
            $this->Question->set($this->request->data['Question']);
            $this->Question->save();
            
            /**
             * If the question answer type differ from Multiple choice to Short descriptive or vice versa 
             * 2 -> 'Multiple correct', 3 -> 'One correct'
             */
            if(!in_array($this->request->data['Question']['answer_type'], array('2', '3'))){
                $answers = $this->Answer->find('all', array(
                    'conditions' => array(
                        'Answer.question_id' => $questionId,
                        'Answer.answer_type' => $questionDetails['Question']['answer_type'],
                        'Answer.isdeleted' => 0
                    ),
                    'recursive' => -1
                ));
                if(!empty($answers)){
                    foreach ($answers as $key => $data){
                        $this->Answer->read(null, $data['Answer']['id']);
                        $this->Answer->set(array(
                            'isdeleted' => 1
                        ));
                        $this->Answer->save();
                        $this->Answer->clear();
                    }
                }
            }

            $message = 'Question updated successfully.';
            echo json_encode(array(
                'type' => 'update',
                'status' => 'Success', 
                'message' => $message,
                'questionId' => $questionId,
                'examinationId' => $questionDetails['Question']['examination_id'],
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();
        }
    }
    
    public function delete_question(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            $questionId = $this->request->data['questionId'];
            
            $questionDetails = $this->Question->read(null, $questionId);
            $this->request->params['named']['examination_id'] = $questionDetails['Question']['examination_id'];
            
            /* Start Rearrange the question order */
            $questionOrder = $questionDetails['Question']['question_order'];
            $nextQuestions = $this->Question->find('all', array(
                'conditions' => array(
                    'Question.isdeleted' => 0,
                    'Question.question_order >' => $questionDetails['Question']['question_order']
                ),
                'recursive' => -1,
                'order' => array('Question.question_order ASC')
            ));
            if(!empty($nextQuestions)){
                foreach ($nextQuestions as $key => $value){
                    $this->Question->read(null, $value['Question']['id']);
                    $this->Question->set(array(
                        'question_order' => $questionOrder
                    ));
                    $this->Question->save();
                    $this->Question->clear();
                    $questionOrder++;
                }
            }
            /* End Rearrange the question order */
            
            $previousQuestion   = $this->FilterQuestion->getPrevious($this, $questionDetails);
            $nextQuestion       = $this->FilterQuestion->getNext($this, $questionDetails);          
            
            if(!empty($nextQuestion)){
                $questionToShow = (!empty($nextQuestion['Question']['id']))?$nextQuestion['Question']['id']:'';
            }else{
                $questionToShow = (!empty($previousQuestion['Question']['id']))?$previousQuestion['Question']['id']:'';
            }
                    
            $this->Question->updateAll(
                    array('Question.isdeleted' => 1),
                    array(
                        'Question.id' => $questionId,
                        'Question.isdeleted' => 0
                    )
            );
            
            $this->Answer->updateAll(
                    array('Answer.isdeleted' => 1),
                    array(
                        'Answer.question_id' => $questionId,
                        'Answer.isdeleted' => 0
                    )
            );

            $message = 'Question deleted successfully.';
            echo json_encode(array(
                'type' => 'update',
                'status' => 'Success', 
                'message' => $message,
                'questionId' => $questionToShow,
                'examinationId' => $questionDetails['Question']['examination_id'],
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();
        }
    }

    public function create_answer(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            $message = '';
            $questionId = $this->request->data['Answer']['question_id'];
            $questionDetails = $this->Question->read(array('examination_id', 'answer_type'), $questionId);
            $answerExistingOrder = $this->Answer->field(
                'answer_order',
                array(
                    'question_id' => $questionId,
                    'isdeleted' => 0
                ),
                'answer_order DESC'
            );
            $this->request->data['Answer']['answer_order'] = (!empty($answerExistingOrder))? $answerExistingOrder + 1 : ord('A');
            $this->request->data['Answer']['answer_type'] = $questionDetails['Question']['answer_type'];
            
            /* checking for single correct answer */
            if(!empty($this->request->data['Answer']['iscorrect'])){ 
                
                if(!empty($questionDetails) && $questionDetails['Question']['answer_type'] == 3){                
                    $answersList = $this->Answer->find('all', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.question_id' => $questionId)));
                    if(!empty($answersList)){
                        foreach ($answersList as $data){
                            $this->Answer->updateAll(
                                array('Answer.iscorrect' => 0), array('Answer.id' => $data['Answer']['id'])
                            );
                        }
                    }
                }
            }
            
            $this->Answer->create();
            if ($this->Answer->save($this->request->data)) {
                $message = 'Answer saved successfully.';
                echo json_encode(array(
                    'status' => 'Success', 
                    'message' => $message,
                    'questionId' => $questionId,
                    'examinationId' => $questionDetails['Question']['examination_id'],
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();
            }else{
                $message = 'Answer not saved successfully.';
                echo json_encode(array(
                    'status' => 'Error', 
                    'message' => $message,
                    'questionId' => '',
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();
            }
        }
    }
    
    public function read_answer(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            //pr($this->request->data);
            $answerDetails = $this->Answer->findById($this->request->data['answerId']);
            echo json_encode($answerDetails);
            exit();
        }
        
    }
    
    public function update_answer(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $answerId = $this->request->data['Answer']['id'];
            $answerDetails = $this->Answer->read(null, $answerId);
            $this->Answer->set($this->request->data['Answer']);
            $this->Answer->save();

            $examinationId = $this->Question->field(
                'examination_id',
                array(
                    'id' => $answerDetails['Answer']['question_id'],
                    'isdeleted' => 0
                )
            );
            
            $message = 'Answer updated successfully.';
            echo json_encode(array(
                'type' => 'update',
                'status' => 'Success', 
                'message' => $message,
                'questionId' => $answerDetails['Answer']['question_id'],
                'examinationId' => $examinationId,
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();
        }
    }
    
    public function delete_answer(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $answerId = $this->request->data['answerId'];
            $questionId = $this->request->data['questionId'];
            $answerDetails = $this->Answer->read(null, $answerId);
            
            /* Start Rearrange the answer order */
            $answerOrder = $answerDetails['Answer']['answer_order'];
            $nextAnswers = $this->Answer->find('all', array(
                'conditions' => array(
                    'Answer.question_id' => $answerDetails['Answer']['question_id'],
                    'Answer.isdeleted' => 0,
                    'Answer.answer_order >' => $answerDetails['Answer']['answer_order']
                ),
                'recursive' => -1,
                'order' => array('Answer.answer_order ASC')
            ));
            if(!empty($nextAnswers)){
                foreach ($nextAnswers as $key => $value){
                    $this->Answer->read(null, $value['Answer']['id']);
                    $this->Answer->set(array(
                        'answer_order' => $answerOrder
                    ));
                    $this->Answer->save();
                    $this->Answer->clear();
                    $answerOrder++;
                }
            }
            /* End Rearrange the answer order */
            
            $this->Answer->updateAll(
                    array('Answer.isdeleted' => 1),
                    array(
                        'Answer.id' => $answerId,
                        'Answer.isdeleted' => 0
                    )
            );
            
            $examinationId = $this->Question->field(
                'examination_id',
                array(
                    'id' => $questionId,
                    'isdeleted' => 0
                )
            );
            
            $message = 'Answer deleted successfully.';
            echo json_encode(array(
                'type' => 'update',
                'status' => 'Success', 
                'message' => $message,
                'questionId' => $questionId,
                'examinationId' => $examinationId,
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();
            
        }
    }
    
    public function preview_question(){
        if($this->request->is('ajax')){
            
            $this->layout = 'ajax';
            $conditions = array(
                'Question.isdeleted' => 0
            );
            
            if(!empty($this->request->data['examination_id'])){
                $conditions['Question.examination_id'] = $this->request->params['named']['examination_id'] = $this->request->data['examination_id'];
            }
            if(!empty($this->request->data['question_id'])){
                $conditions['Question.id'] = $this->request->data['question_id'];
            }else{
                $conditions['Question.question_order'] = 1;
            }
            
            $questionDetails = $this->Question->find('first', array(
                'conditions' => $conditions,
                'order' => array('Question.created ASC')
            ));
            
            if(!empty($questionDetails['Answer'])){
                $answers = $questionDetails['Answer'];
                $sortedAnswers = Set::sort($answers, '{n}.answer_order', 'asc');
                $questionDetails['Answer'] = $sortedAnswers;
            }
            
            $this->set(compact('questionDetails'));
            $this->set('next', $this->FilterQuestion->getNext($this, $questionDetails));
            $this->set('previous', $this->FilterQuestion->getPrevious($this, $questionDetails));
            
        }
    }

    public function examTypeHeadSearch(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $exams = $this->Examination->find('all', array(
                    'conditions' => array(
                        'Examination.isdeleted' => 0,
                        'Examination.title LIKE' => "%" . urldecode($this->request->data['search']) . "%"
                    ),
                    'fields' => array('Examination.id', 'Examination.title', 'Examination.examination_type_id', 'Examination.examination_category_id'),
                )
            );
            $this->Examination->clear();
            $results = Set::classicExtract($exams, '{n}.Examination');
            echo json_encode($results);
            exit();
        }
    }
}

?>