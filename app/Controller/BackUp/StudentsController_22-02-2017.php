<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class StudentsController extends AppController {

    public $name = 'Students';
    public $uses = array('Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer', 'Answer', 'MyExamStudentAnswer');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	
	
    public function login() {
        $this->layout = false;
		
        if (!empty($this->request->data)) {
			//pr($this->request->data);die;
            $login = strtolower($this->request->data['Student']['login']);
            $password = $this->request->data['Student']['password'];
            $studentdata = $this->Student->getLoginData($login, $password);
            if (!empty($studentdata)) {
                $this->UserConfiguration->setStudentData($studentdata);
                $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
              //  $this->redirect(array('controller' => 'students', 'action' => 'take_exam_list'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'students', 'action' => 'login'));
            }
        }  else {
            $this->_studentLoginRedirect();
        }
    }
    
    public function index() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $examinations = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		 $this->set(compact('studentDetails', 'examinations'));
		 $this->render('purchase_exam');
		// echo $RandomID = $this->Session->read('RandomID'); exit;
		 
    }
	
	 public function myprofile() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $this->set(compact('studentDetails'));
       }
	
	
	  public function view_answer($random_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 if(!empty($random_id)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array('StudentAnswer.student_id' => $student_id, 'StudentAnswer.random_id' => $RandomID)));
		$answer_ids = '';
		foreach($StudentAnswerAr as $val){
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
		 }
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		
		 $totalAttemptQs = count($StudentAnswerAr);
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails'));
		 
		//echo '<pre>';print_r($StudentAnswerAr);
		//die;
		 
    }
	 
	
	
	
	 public function my_exams() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $this->MyExamStudentAnswer->recursive = 3;
		 
		 $this->Examination->unbindModel(array('hasMany' => 'Question'), false);
		 
		 $myExamAr = $this->MyExamStudentAnswer->find('all', array(
																								 'conditions' => array(
																								 'MyExamStudentAnswer.isdeleted' => 0, 
																								 'MyExamStudentAnswer.student_id' => $student_id
																								 ),
																								'order' => 'MyExamStudentAnswer.created DESC' 
																								 ));
		//pr($myExamAr);die;
		 $this->set(compact('studentDetails', 'myExamAr'));
		
    }
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		 $RandomID = $this->Session->read('RandomID');
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 
			//pr($this->request->data);die;
			$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
			$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
			$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
			//pr($myExamStudentAns);die;
			$this->MyExamStudentAnswer->save($myExamStudentAns);
			 
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					 $this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
				}
			}
			 
			$this->Session->setFlash('Data has been submitted successfully.');
			$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			   
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			 $my_exam_id = $this->Session->read('my_exam_id');
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					//pr($myExamStudentAns);die;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
					
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					$this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
			 }
		
		 }
		 
	  }
	  
	
	 public function take_exam($examination_id = NULL, $my_exam_id = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 
		 $examination_id = $this->data_decrypt($examination_id);
		 $my_exam_id = $this->data_decrypt($my_exam_id);
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		  $this->Session->write('my_exam_id', $my_exam_id);
		  
		  $this->set(compact('my_exam_id'));
		 
		 
		
		$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$examDuration = $settingsData21['Setting']['value'];
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	 'conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		
    }
	
	
	
	 public function take_exam_list() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $this->MyExam->recursive = 2;
		 $examinations = $this->MyExam->find('all', array('conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1)));
		// pr($examinations);die;
		 $this->set(compact('studentDetails', 'examinations'));
		
    }
	
	 public function purchase_exam() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 //$this->Examination->recursive = 2;
		 $allExamAr = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		//pr($allExamAr);die;
		 $this->set(compact('studentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student' => $studentDetails['Student']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	 /* Start Payment functions */
    public function payment_success() {
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->request->is('post')) {
         //   pr($this->request->data);
           // echo 'next array';
            $payPalCustomData = array();
            $pagedata = explode('##', $this->request->data['custom']);
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
                $this->request->data['MyExam']['student_id'] = $pagedata[0];
                $this->request->data['MyExam']['examination_id'] = $pagedata[1];
                $this->request->data['MyExam']['payment_status'] = 1;
                $this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
                $this->request->data['MyExam']['amount'] = $this->request->data['payment_gross'];
				$this->MyExam->save($this->request->data);
                /* For school payment success */
               // if(!empty($payPalCustomData['student'])){
                    $this->Session->write('Student.transactionData', $this->request->data);
                    $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_success'));
               // }
                
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	 public function examinations($examination_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata = $this->UserConfiguration->getStudentData();
		 $questions = $this->Question->find('all', array('conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)));
		 $this->set(compact('studentdata', 'questions'));
		 pr($questions);
		 die;
    }
    

	
		   public function make_payment($examination_id = NULL){
					$this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'home_layout';
					$examination_id = $this->data_decrypt($examination_id);
					
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $examDtls = $this->Examination->find('first', array('fields' => 'id, price,title','conditions' => array('Examination.id' => $examination_id, 'Examination.isdeleted' => 0)));
					 $amount = $examDtls['Examination']['price'];
					 $title = $examDtls['Examination']['title'];
					 $examination_id = $examDtls['Examination']['id'];
					 $ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $studentDetails['Student']['first_name'];
						$lastName= $studentDetails['Student']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('USD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								$this->request->data['MyExam']['student_id'] = $student_id;
								$this->request->data['MyExam']['examination_id'] = $examination_id;
								$this->request->data['MyExam']['payment_status'] = 1;
								$this->request->data['MyExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
								$this->request->data['MyExam']['amount'] = $amount;
								
									$this->MyExam->save($this->request->data);
								
								// $this->Flash->success(__("transaction successfully completed"));
								 //$this->Session->setFlash('Transaction successfully completed');
								$this->redirect(array('controller'=>'students','action'=>'purchase_exam_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							}
						}
						}
							///payment End from here
						}
						
					  $paypalFormSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'student_id' => $studentDetails['Student']['id'],
            'examination_id' => $examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalFormSettings['item_name'] = $title; //Router::url('/', true)
        $paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['amount'] = $amount; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
		$this->set(compact('studentDetails', 'examDtls','paypalFormSettings'));
		   }
    
    public function purchase_exam_success(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
        $this->set(compact('studentDetails'));
        if($this->Session->check('Student.transactionData')){
            
            $transactionData = $this->Session->read('Student.transactionData');
            $this->Session->delete('Student.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $student_id = $pagedata[0]; 
			 $student_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->Student->read(null, $payPalCustomData['student']);
            $this->MyExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
	 public function payment_error(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
        $this->set(compact('studentDetails'));
	 }
	
	
	
	
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->studentLogout();
		 $this->Session->delete('App.studentData');
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'students', 'action' => 'login'));
    }
    
    protected function _studentLoginRedirect(){
        $isLoggedIn = $this->Session->check('App.studentData');
        if ($isLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
	//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	
	
	
	
}
?>