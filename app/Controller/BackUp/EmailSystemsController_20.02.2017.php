<?php

App::import('Console/Command', 'AppShell');
App::import('Console/Command', 'SchoolimportShell');
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class EmailSystemsController extends AppController {

    public $name = 'EmailSystems';
    public $uses = array('SchoolImport', 'School', 'Setting', 'Admin', 'Attachment', 'Email', 'EmailDelivery', 'EmailTemplate');
    public $components = array('Uploader.Uploader', 'FilterSchools', 'FilterEmailTemplate');
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->UserConfiguration->isAdminLoggedIn();
        
    }

    public function index() {
        $this->layout = 'admin';
        $this->request->params['named']['isdeleted'] = true;
        
        $schools = $this->FilterSchools->getSchools($this);
        if ($this->request->is('requested')){
            return $schools;
        }
        $this->set('school', $schools);
        $this->getSchoolFilterOptions();
        
        $emailTemplates = $this->EmailTemplate->find('list', array(
            'recursive' => -1,
            'fields' => array('EmailTemplate.id', 'EmailTemplate.title'),
            'order' => array('EmailTemplate.title ASC'),
            'group' => array('EmailTemplate.title')
        ));
        $this->set('emailTemplates', $emailTemplates);
    }
    
    /**
     * Search action
     * Redirect to main action mapping the search form to a named param
     * TODO Review to make search form call index direct
     */
    public function search() {
        $url = array('controller' => 'email_systems', 'action' => 'index');
        if (!empty($this->request->data['School']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['School']['search'])));
        $this->redirect($url);
    }

    public function mail_templates(){
        $this->layout = 'admin';
    }
    
    public function create_update_template(){
        
        $this->layout = false;
        $this->autoRender = false;
        
        if($this->request->is('ajax')){
            
            if (!empty($this->request->data['EmailTemplate'])) {
                
                $templateSlug = Inflector::slug(strtolower(trim($this->request->data['EmailTemplate']['title'])), $replacement = '_');
            
                /* For Update */
                if(!empty($this->request->data['EmailTemplate']['id'])){
                    
                    $template = $this->EmailTemplate->find('first', array(
                        'conditions' => array(
                            'EmailTemplate.slug' => $templateSlug,
                            'EmailTemplate.id !=' => $this->request->data['EmailTemplate']['id'],
                            'EmailTemplate.isdeleted' => 0
                        )
                    ));
                    
                    if(!empty($template)){
                        $message = '<b>'.$this->request->data['EmailTemplate']['title'].'</b> template already exist.';
                        echo json_encode(array(
                            'status' => 'Error',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen(strip_tags($message))
                            ));
                        exit();
                    }else{
                        $templateId = $this->request->data['EmailTemplate']['id'];
                        unset($this->request->data['EmailTemplate']['id']);
                        $this->request->data['EmailTemplate']['modified'] = date('YmdHis');
                        $this->EmailTemplate->read(null, $templateId);
                        $this->EmailTemplate->set($this->request->data['EmailTemplate']);
                        $this->EmailTemplate->save();
                        $message = 'Email template updated successfully.';
                        echo json_encode(array(
                            'status' => 'Success',
                            'message' => $message,
                            'templateId' => $templateId,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen($message)
                            ));
                        exit();
                    }
                    
                }else{  /* For Create Template */
                    
                    $template = $this->EmailTemplate->find('first', array(
                        'conditions' => array(
                            'EmailTemplate.slug' => $templateSlug,
                            'EmailTemplate.isdeleted' => 0
                        )
                    ));
                    
                    if(!empty($template)){
                        $message = '<b>'.$this->request->data['EmailTemplate']['title'].'</b> template already exist.';
                        echo json_encode(array(
                            'status' => 'Error',
                            'message' => $message,
                            'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                            'messageLetterCount' => strlen(strip_tags($message))
                            ));
                        exit();
                    }else{
                        $this->request->data['EmailTemplate']['slug'] = $templateSlug;
                        $this->request->data['EmailTemplate']['created'] = date('YmdHis');

                        $this->EmailTemplate->create();
                        if ($this->EmailTemplate->save($this->request->data)) {
                            $message = 'Email template created successfully.';
                            echo json_encode(array(
                                'status' => 'Success',
                                'message' => $message,
                                'templateId' => $this->EmailTemplate->id,
                                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                                'messageLetterCount' => strlen($message)
                                ));
                            exit();
                        }else{
                            $message = 'Email template not created properly.';
                            echo json_encode(array(
                                'status' => 'Error',
                                'message' => $message,
                                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                                'messageLetterCount' => strlen($message)
                                ));
                            exit();
                        }
                    }  
                }
            }
        }
    }
    
    public function view_mail_template($templateId = null){
        
        $this->layout = false;
        $this->autoRender = false;
        $template = $this->EmailTemplate->read(null, $templateId);
        echo json_encode($template);
        exit();
        
    }
    
    public function delete_mail_template(){
        
        if($this->request->is('ajax')){
            
            $this->layout = false;
            $this->autoRender = false;
            $templateId = $this->request->data['templateId'];
            $templateDetails = $this->EmailTemplate->read(null, $templateId);
            
            $previousTemplate   = $this->FilterEmailTemplate->getPrevious($this, $templateDetails);
            $nextTemplate       = $this->FilterEmailTemplate->getNext($this, $templateDetails);          
            
            if(!empty($nextTemplate)){
                $templateToShow = (!empty($nextTemplate['EmailTemplate']['id']))?$nextTemplate['EmailTemplate']['id']:'';
            }else{
                $templateToShow = (!empty($previousTemplate['EmailTemplate']['id']))?$previousTemplate['EmailTemplate']['id']:'';
            }
                    
            $this->EmailTemplate->updateAll(
                    array('EmailTemplate.isdeleted' => 1),
                    array(
                        'EmailTemplate.id' => $templateId,
                        'EmailTemplate.isdeleted' => 0
                    )
            );

            $message = 'Mail template deleted successfully.';
            echo json_encode(array(
                'status' => 'Success', 
                'message' => $message,
                'templateId' => $templateToShow,
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();       
        }
    }

    public function preview_mail_template(){
        
        if($this->request->is('ajax')){
            
            $this->layout = 'ajax';
            $conditions = array(
                'EmailTemplate.isdeleted' => 0
            );
            
            //$this->request->params['named']['examination_id']
            
            if(!empty($this->request->data['templateId'])){
                $conditions['EmailTemplate.id'] = $this->request->data['templateId'];
            }
            
            $templateDetails = $this->EmailTemplate->find('first', array(
                'conditions' => $conditions,
                'order' => array('EmailTemplate.created ASC')
            ));
                        
            $this->set(compact('templateDetails'));
            $this->set('next', $this->FilterEmailTemplate->getNext($this, $templateDetails));
            $this->set('previous', $this->FilterEmailTemplate->getPrevious($this, $templateDetails));
            
        }else{
            $this->layout = 'admin';
        }
        
    }
    
    public function import(){
        $this->layout = 'admin';
        if (!empty($this->request->data)) {
            
            if(!empty($this->request->data['email_systems']) && count($this->request->data['email_systems']) > 0){
                $uploaded = $notUploaded = array();
                $message = '';
                foreach ($this->request->data['email_systems'] as $key => $fileData){
                    
                    $name = md5(microtime()) . '.' . pathinfo($fileData['filename']['name'], PATHINFO_EXTENSION);
                    if (move_uploaded_file($fileData['filename']['tmp_name'], 'files/school/imports/' . $name)) {
                        $data = array(
                            'path' => $name,
                            'status' => 0,
                            'created' => date('YmdHis')
                        );
                        $this->SchoolImport->create();
                        $this->SchoolImport->save($data);
                        $this->SchoolImport->clear();
                        array_push($uploaded, '<b>'.$fileData['filename']['name'].'</b>');
                    } else {
                        array_push($notUploaded, '<b>'.$fileData['filename']['name'].'</b>');
                    }
                }
                
                if(count($uploaded) == count($this->request->data['email_systems'])){
                    $this->Session->setFlash(__('All selected excel files uploaded successfully. School data will import shortly.'), 'success');
                }
                
                if(count($notUploaded) > 0){
                    $message = (count($uploaded) > 0)?implode(', ', $uploaded).' files uploaded successfully. ':'';
                    $this->Session->setFlash(__($message.'Unable to upload '.implode(', ', $notUploaded).' excel files.'), 'error');
                }
            }
            
            //pr($this->request->data); exit();
            //$this->redirect(array('controller' => 'email_systems', 'action' => 'import'));
        }
    }
    
    public function delete_attachment(){
        $this->layout = false;
        $this->autoRender = false;
        $message = '';
        
        $attachmentId = $this->request->data['attachmentId'];
        if(!empty($attachmentId)){
            
            $attachmentDetails = $this->Attachment->findById($attachmentId);
            if(!empty($attachmentDetails)){
                $dirPath = WWW_ROOT . 'files/school/attachments/' .$attachmentDetails['Attachment']['path'];
                $dirPath = str_replace("\\", "/", $dirPath);
                $isDeleted = unlink($dirPath . $attachmentDetails['Attachment']['title'] . '.' . $attachmentDetails['Attachment']['extension']);
                if($isDeleted){
                    $flag = $this->_recursivelyDeleteEmptySubDirectories($attachmentDetails);
                    if($flag){
                        $this->Attachment->delete($attachmentId);
                        $message = 'deleted';
                    }
                    
                }
            }
            
        }
        echo $message;
        exit();
    }
    
    public function send_mail() {
        $this->layout = false;
        $this->autoRender = false;
        if (!empty($this->request->data)) {
            $message = array();
            $attachmentFiles = array();
            $toEmail = null;
            $bccEmails = array();
            $isBccEmail = false;

            if (!empty($this->request->data['Email']['toIds'])) {
                
                $toIds = explode(',', $this->request->data['Email']['toIds']);
                
                $emailDataToSave = array(
                    'subject' => trim($this->request->data['Email']['subject']), 
                    'message' => trim($this->request->data['Email']['message']),
                    'attachment_mask' => trim($this->request->data['Email']['attachmentMask']),
                    'created' => date('YmdHis')
                );
                
                if(!empty($this->request->data['Email']['email_template_id'])){
                    $emailDataToSave['email_template_id'] = $this->request->data['Email']['email_template_id'];
                }
                
                if($this->Email->save($emailDataToSave)){
                    $emailId = $this->Email->id;
                    $fromId = $this->request->data['Email']['fromId'];
                    foreach ($toIds as $index => $id){
                    
                        $dataToSave = array(
                            'from_id' => $fromId,
                            'to_id' => $id,
                            'email_id' => $emailId,
                            'created' => date('YmdHis')
                        );
                        $this->EmailDelivery->save($dataToSave);
                        $this->EmailDelivery->clear();
                        
                    }
                    
                    $this->Attachment->updateAll(
                            array('Attachment.email_id' => $emailId, 'Attachment.deleted' => 0), 
                            array('Attachment.mask' => trim($this->request->data['Email']['attachmentMask']))
                    );
                }
                
                $message = 'Email information saved successfully. It will start sending email very soon.';
                echo json_encode(array(
                    'status' => 'Success', 
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();
                                
            }
        }
    }

    public function download() {

        $this->viewClass = 'Media';
        $path1 = WWW_ROOT . 'files/school/imports/';
        // in this example $path should hold the filename but a trailing slash
        $params1 = array(
            'id' => 'Upload_Template_Structure.xlsx',
            'name' => 'Download Sample Template for School',
            'download' => true,
            'path' => $path1,
            'extension' => 'xlsx',
        );
        $this->set($params1);
    }

    /* This function only for testing purpose */
    public function run_shell() {
        $this->layout = false;
        $this->autoRender = false;
        $job = new SchoolimportShell();
        $job->dispatchMethod('process');
    }
    
    protected function _recursivelyDeleteEmptySubDirectories($data = array()) {
        $flag = false;
        $parentDirectory = WWW_ROOT . 'files/school/attachments';
        $parentDirectory = str_replace("\\", "/", $parentDirectory);
        
        $subDirectoryPath = rtrim($data['Attachment']['path'], '/');
        $subDirectories = explode('/', $subDirectoryPath);
        $subDirectoryCount = count($subDirectories) - 1;
        $loopIncrimentor = 0;
        $teml = array();
        for($i = $subDirectoryCount; $i >= 0; $i--){
            
            $directoryPathToRemove = $parentDirectory.'/'.implode('/', $subDirectories);
            array_push($teml, 'Dir Path : '.$directoryPathToRemove); /* It's only for testing purpose. */
            if(rmdir($directoryPathToRemove)){
                $deletedDirectory = array_pop($subDirectories);
                if($loopIncrimentor == $i){
                    $flag = true;
                }
                $loopIncrimentor++;
            }
            
        }
        return $flag;
    }
}
?>