<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SchoolsController extends AppController {

    public $name = 'Schools';
    public $uses = array('Setting', 'School', 'SchoolInformation');
    public $components = array('FilterSchools');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function login() {
        $this->layout = false;
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['School']['login']);
            $password = $this->request->data['School']['password'];
            $userdata = $this->School->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setSchoolData($userdata);
                $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                //$this->redirect(array('controller' => 'schools', 'action' => 'index'));
                $this->redirect(array('controller' => 'schools', 'action' => '/'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'schools', 'action' => 'login'));
            }
        }  else {
            $this->_schoolLoginRedirect();
        }
    }
	
	public function myprofile(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
        
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			    $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('nameErr', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('managerNameErr', 'Please enter manager name');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							} else {
								$manager_email = $this->request->data['School']['manager_email'];
								if (!filter_var($manager_email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('manager_emailErr', 'Please enter valid email format');
								}
							} 
							
							
							
							
							if(empty(trim($this->request->data['SchoolInformation']['level']))){
								$ERROR = 1;
								$this->set('levelErr', 'Please select level');
							}
							
							if(empty(trim($this->request->data['SchoolInformation']['street']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter street');
							}
							
								if(empty(trim($this->request->data['SchoolInformation']['district']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter district');
							}
							
								if(empty(trim($this->request->data['SchoolInformation']['zip']))){
								$ERROR = 1;
								$this->set('zipErr', 'Please enter zip');
							}
							if(empty(trim($this->request->data['SchoolInformation']['type']))){
								$ERROR = 1;
								$this->set('typeErr', 'Please select zip');
							}
							
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emailErr', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emailErr', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email, 'School.id <>' => $school_id)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->request->data['School']['email'] = $this->request->data['School']['email'];
										$this->set('emailErr', 'This email already exist');
										$this->referer();
									}
							}
							
					 
					 if($ERROR == 0){		
							$this->School->save($this->request->data['School']);
							$this->SchoolInformation->save($this->request->data['SchoolInformation']);
							$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
							$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
					 } 
					
					
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
		}				 
	
    
    public function index() {
        
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
        
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$this->School->save($this->request->data['School']);
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
        
        
    }
	
	public function registration() {
        $this->layout = 'school_layout';
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		   $schoolLevelOptions = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
        
        
        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));
       
		  $this->set(compact('examination_types', 'examination_categories','schoolLevelOptions','types'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
							
								 //student login start
			 if(!empty($this->request->data['SchoolLogin']['login_registration_flag'])  && $this->request->data['SchoolLogin']['login_registration_flag'] == 'school_login'){ 
			  $login = strtolower($this->request->data['SchoolLogin']['login']);
            $password = $this->request->data['SchoolLogin']['password'];
            $userdata = $this->School->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['SchoolLogin']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($userdata) && !empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setSchoolData($userdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
			} 
			 } else {
			
			 //student login End
							
							//school registration
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
						
							
							if(empty(trim($this->request->data['School']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['School']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['School']['username'] = $this->request->data['School']['email'];
					$this->request->data['School']['password'] =$this->_randomPassword(8);
					
					$this->School->save($this->request->data['School']);
					$this->request->data['SchoolInformation']['school_id'] = $this->School->getLastInsertID();
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'Schools', 'action' => 'registration'));
				}
				
		 }
				
			 }
	  }

	
    
    public function view(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function edit($schoolId = null){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
        
    }

	public function registration_payment_success(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
		}				 
	
    public function registration_payment(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
						 $school_id = $schoolData['School']['id'];
						//Credit card start here
						$ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $studentDetails['Student']['first_name'];
						$lastName= $studentDetails['Student']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('USD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								 $dateTime = new DateTime('NOW');
								$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
								$schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
								$endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
								$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
								
								$this->School->set(array(
									'id' => $school_id,
									'payment_status' => 1,
									'start_subscription' => $subscriptionStart,
									'end_subscription' => $subscriptionEnd,
									'modified' => $subscriptionStart
								));
								$this->School->save();
								$this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
								$this->redirect(array('controller'=>'schools','action'=>'registration_payment_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							}
						}
						}
							///Credit card payment End from here
						}
		
		
		
        $timeStamp = time();
        $paymentAmount = (!empty($settings['Setting']['school_registration_fee'])) ? $settings['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT;
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY, true);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY));
        }      
        
        $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
            'item_name' => 'School Registration',
            'item_number' => str_pad($schoolData['School']['id'], 10, "0", STR_PAD_LEFT),
            'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'school' => $schoolData['School']['id'],
            'payment_for' => 'registration',
            'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
            'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'app/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'app/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'app/payment_cancle'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		$this->set(compact('paymentAmount'));	
			$this->render('make_payment');
    }
    
    public function checkFingerprint(){
        
        $this->layout = false;
        $this->autoRender = false;
        $flag = false;
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        
        $extraData = $this->request->data['custom'];
        $amount = $this->request->data['amount'];
        
        $payPalCustomData = array();
        $pagedata = explode('/', $extraData);
        foreach ($pagedata as $key => $value) {
            $exploaded = explode(':', $value);
            $payPalCustomData[$exploaded[0]] = $exploaded[1];
        }
        
        $oldFingerprint = urldecode($payPalCustomData['fp']);
        
        if (phpversion() >= '5.1.2') {
            $newFingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY, true);
        } else {
            $newFingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY));
        }
        
        if($oldFingerprint != $newFingerprint){
            $flag = true;
        }
        echo $flag;
        exit();
    }

    public function registration_success(){
        $this->layout = false;
        
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('/', $transactionData['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
            $endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            $fingerPrint = urldecode($payPalCustomData['fp']);
            
            $this->School->read(null, $payPalCustomData['school']);
            $this->School->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            $this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'schools', 'action' => 'login'));
        }
        
    }
    
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->schoolLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'schools', 'action' => 'login'));
    }
    
    protected function _schoolLoginRedirect(){
       // $isLoggedIn = $this->Session->check('App.schoolData');
        $isSchoolLoggedIn = $this->Session->check('App.schoolData');
        if ($isSchoolLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'schools', 'action' => 'index'));
        }else{
            return;
        }
    }
	
		//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	
	
	
}
?>