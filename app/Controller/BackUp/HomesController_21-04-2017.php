<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class HomesController extends AppController {

    public $name = 'Homes';
    public $uses = array('Student','ContentManagement');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
				public function beforeFilter() {
					parent::beforeFilter();
				 //   $this->settingData = $this->Setting->getSettingsData();
				}
    
	
    
	
	public function subscribe_news_letter() {
					$this->layout = 'landing_layout';
					$this->Session->delete('emailErr');
					$ERROR = 0;
					 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
						// pr($this->data);die;
						if(empty(trim($this->request->data['User']['email']))){
								$ERROR = 1;
								$this->Session->write('emailErr', 'Please enter email');
								$this->redirect($this->referer());
							} else {
								$email = $this->request->data['User']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->Session->write('emailErr', 'Please enter valid email format');
									$this->redirect($this->referer());
								}
							} 
							
							if($ERROR == 0){
								 $this->Session->setFlash(__('Your email has been subscribed successfully.'), 'success');
								$this->redirect($this->referer());
							}
					 }
				}
	
				public function index() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				
				
				
				
				 public function contact_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower('Contact us')));
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				
				
				
				
			
				
				 public function high_school_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 8,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$this->render('common_content_display');
				}
				
				public function primary_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 7,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$this->render('common_content_display');
				}
				
				 public function high_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 6,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$this->render('common_content_display');
				}
				
				 public function primary_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 5,'ContentManagement.isdeleted' => 0)));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				
				
				 public function about_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
					 
					 $cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 4,'ContentManagement.isdeleted' => 0)));
					 $this->set(compact('cmsData'));
					 $this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					 $this->render('common_content_display');
				}
				
				
				 public function company_information() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Company information');
					$this->set('page_heading', 'Company information');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 3,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				
				 public function terms_of_service() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Terms of service');
					$this->set('page_heading', 'Terms of service');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 2,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function privacy_policy() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Privacy policy');
					$this->set('page_heading', 'Privacy policy');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function we_offer($page = NULL) {
					$this->layout = 'landing_layout';
					//$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					//$this->set(compact('cmsData'));
					
					$page = str_replace('-', ' ', $page);
					$this->set('title_for_layout', 'We offer '.$page);
					$this->set('page_heading', $page);
					
					$this->render('common_content_display');
				}
				
				
				
				
	
}
?>