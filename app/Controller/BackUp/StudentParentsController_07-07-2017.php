<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class StudentParentsController extends AppController {

    public $name = 'StudentParents';
    public $uses = array('StudentParent','Student','Examination', 'MyExam', 'ExaminationCategory','MyExamStudentAnswer');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration($no_of_child = NULL) {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		  if(!empty($no_of_child)){
			  if($no_of_child == 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Child';
			  } else if($no_of_child > 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Children';
			  } else {
				   $no_of_child = '';
				   $childTag = '';
			  }
		  } else {
			   $no_of_child = '';
			   $childTag = '';
		  }
		  
		   $this->set(compact('examination_types', 'examination_categories','no_of_child','childTag'));
		  
		  
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 //pr($this->request->data);die;
			 //Parent registration
			 	if(empty(trim($this->request->data['StudentParent']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							
							
							
							if(empty(trim($this->request->data['StudentParent']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['StudentParent']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['StudentParent']['email']))){
								$email = $this->request->data['StudentParent']['email'];
								$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['StudentParent']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['StudentParent']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['StudentParent']['password'];
							$cpass = $this->request->data['StudentParent']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['StudentParent']['password'] != $this->request->data['StudentParent']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['StudentParent']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['StudentParent']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['StudentParent']['username'] = $this->request->data['StudentParent']['email'];
					$this->request->data['StudentParent']['password'] = md5($this->request->data['StudentParent']['password']);
					$this->StudentParent->save($this->request->data);
					$lastInserId = $this->StudentParent->getLastInsertID();
					for($i =0; $i<$no_of_child; $i++){
						$studentData['Student']['student_parent_id'] = $lastInserId;
						$studentData['Student']['examination_category_id'] = $this->request->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->request->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->request->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->request->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
					}
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					 $this->redirect(array('controller' => 'parent', 'action' => 'registration',$no_of_child));
				}
				
			 }
		 }
	 

	
	
    public function login() {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
				//print_r($this->request->data);
            //student login start
                $login = strtolower($this->request->data['StudentParent']['login']);
                $password = $this->request->data['StudentParent']['password'];
                $parentData = $this->StudentParent->getParentLoginData($login, $password);
				//print_r($parentData);die;
                if (empty(trim($this->request->data['StudentParent']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($parentData) && !empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }

                if ($ERROR == 0) {
                    $this->UserConfiguration->setParentData($parentData);
                    
					$this->StudentParent->id = $parentData['StudentParent']['id'];
					$lastdate = date("Y-m-d h:i:s");
					$this->StudentParent->saveField('last_login', $lastdate);
					
                    $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                   // $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
                    $this->redirect(array('controller' => 'parent', 'action' => 'take_exam_list'));
                }
            } 
        }
    
	
	 public function family_profiles() {
				$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
				$no_of_child = count($parentData['Student']);
				 if(!empty($no_of_child) && $no_of_child != 0){
					  if($no_of_child == 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Child';
					  } else if($no_of_child > 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Children';
					  } else {
						   $no_of_child = '';
						   $childTag = '';
					  }
				  } else {
					   $no_of_child = '';
					   $childTag = '';
				  }
		  
		   $this->set(compact('no_of_child','childTag'));
				
		  	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				// pr($this->data);die;
				    
					$this->StudentParent->id = $parent_id;
					$this->StudentParent->save($this->request->data);
					$i = 0;
					foreach($this->data['Student']['id'] as $id){ 
						$studentData['Student']['id'] = $id;
						$studentData['Student']['student_parent_id'] = $parent_id; 
						$studentData['Student']['examination_category_id'] = $this->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
						$i++;
					}
					
				    $this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
				    $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
			 }
			 
			 $this->data = $this->StudentParent->read(NULL, $parent_id);
		  
       }
	
	 public function take_exam_list() {
            $this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
		 
		 $this->MyExam->recursive = 2;
		 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
		 
		
		 $this->paginate = array(
							'conditions' => array(
												 'MyExam.isdeleted' => 0, 
												 'MyExam.payment_status' => 1, 
												 'MyExam.student_parent_id' => $parent_id
							 ),
							'limit' => 8,
							'order' => 'MyExam.id DESC'	,
							'group' => 'Examination.examination_category_id'
						);
       
	    $examinations = $this->paginate('MyExam');
		
		$this->set(compact('parentDetails', 'examinations'));
		
    }
	
	
	 public function fetch_exam_paper($examination_category_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$this->autoRender = false;
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			$conditions = array(
							 'MyExam.isdeleted' => 0, 
							 'MyExam.payment_status' => 1, 
							 'MyExam.student_parent_id' => $parent_id,
							 'Examination.examination_category_id' => $examination_category_id
							);
		
		   $MyExamAr = $this->MyExam->find('all', array('conditions' => $conditions, 'order' => 'Examination.paper ASC'));
		   return $MyExamAr;
		   
	    }
	
	 public function fetch_child($examination_category_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			//$this->layout = 'parent_layout';
			$this->autoRender = false;
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			
			$conditions = array(
							 'Student.isdeleted' => 0, 
							 'Student.payment_status' => 1, 
							 'Student.student_parent_id' => $parent_id,
							 'Student.examination_category_id' => $examination_category_id
							);
		
		   $childData = $this->Student->find('all', array('conditions' => $conditions, 'fields' => 'id, first_name, last_name'));
		   return $childData;
		  /* $childName = '';
		   if(!empty($childData)){
			   foreach($childData as $val){
				   $childName = $childName.', '.$val['Student']['first_name'];
			   }
		   } else {
			   $childName = '';
		   }
		   return ltrim($childName, ', ');*/
		   
	    }
	
	
	
	 public function is_exist_in_myexam($examination_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			 $isMyexamPurchased = $this->MyExam->find('count', array(
														 'conditions' => array(
																'MyExam.examination_id' => $examination_id,
																'MyExam.student_parent_id' => $parent_id,
																'MyExam.payment_status' => 1,
																'MyExam.isdeleted' => 0,
																'MyExam.is_expired' => 0,
														 )));
			return $isMyexamPurchased;											 
		 }
	
	
	 public function exams_available($examination_category_id = NULL) {
        
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			$examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		 //$this->ExaminationCategory->recursive = 2;
		// echo $student_id; 'Examination', 'MyExam', 'ExaminationCategory'
		 	/*$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_parent_id' => $parent_id)
													),
												)

										));	*/
		 
		 $allExamAr = $this->Examination->find('all', array(
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));			
																 
		//pr($allExamAr);die;
		 $this->set(compact('parentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($parentDetails['StudentParent']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'parent' => $parentDetails['StudentParent']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'student_parents/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'student_parents/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'student_parents/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	 public function checkout() {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'student_parents', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_parent_id' => $parent_id)
                ),
            )

        ));	

				 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					// pr($this->data);die;
					 $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $this->request->data['Examination']['ids']
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					foreach($allExamAr as $val){
						$subTotal = $subTotal + $val['Examination']['price'];
					}
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 }
				 
		 $this->set(compact('parentDetails', 'allExamAr','subTotal','gst', 'total'));
		
    }
	
	
	 public function make_payment($examination_id = NULL){
					$this->UserConfiguration->isParentLoggedIn();
					$this->layout = 'parent_layout';
					$parentData = $this->UserConfiguration->getParentData();
					$parent_id = $parentData['StudentParent']['id'];
					
					 $MultipleExamPayment = $this->Session->read('MultipleExamPayment');
					$gross_total = $MultipleExamPayment['gross_total'];
					 $ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $parentData['StudentParent']['first_name'];
						$lastName= $parentData['StudentParent']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('AUD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						$dateTime = new DateTime('NOW');
						$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
						$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

						$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
						$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								/*$this->request->data['MyExam']['student_id'] = $student_id;
								$this->request->data['MyExam']['examination_id'] = $examination_id;
								$this->request->data['MyExam']['payment_status'] = 1;
								$this->request->data['MyExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
								$this->request->data['MyExam']['amount'] = $amount;
								$this->MyExam->save($this->request->data);*/
								
							   $datas = $MultipleExamPayment['ids'];
									$i = 0;
									foreach($datas as $exam_id){
										$this->request->data['MyExam']['student_id'] = 0;
										$this->request->data['MyExam']['student_parent_id'] = $parent_id;
										$this->request->data['MyExam']['examination_id'] =  $exam_id;
										$this->request->data['MyExam']['payment_status'] = 1;
										$this->request->data['MyExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
										$this->request->data['MyExam']['amount'] =  $MultipleExamPayment['total_price'][$i];
										
										$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
										$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
										
										$this->MyExam->create();
										$this->MyExam->save($this->request->data);
										$i++;
									}
									
								$this->redirect(array('controller'=>'student_parents','action'=>'purchase_exam_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
							}
						}
						}
							///payment End from here
						}
						
					  $paypalFormSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($parentData['StudentParent']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'parent_id' => $parentData['StudentParent']['id'],
            'examination_id' => @$examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
		
		 $examID = null;
		$datas = $MultipleExamPayment['ids'];
			foreach($datas as $exam_id){
				if (!empty($examID)) {
					$examID .= "";
				}
				$examID .= "#" . $exam_id;
			}
		
		 $eachTotalPrice = null;
		$total_price = $MultipleExamPayment['total_price'];
			foreach($total_price as $val){
				if (!empty($eachTotalPrice)) {
					$eachTotalPrice .= "";
				}
				$eachTotalPrice .= "**" . $val;
			}
		
		// $custom1 = trim($examID, '#').'||'.trim($noOfstudents, '@').'||'.trim($eachTotalPrice, '**').'||'.$SchoolDetails['School']['id'];
		 $custom1 = trim($examID, '#').'||'.trim($eachTotalPrice, '**').'||'.$parentData['StudentParent']['id'];
		
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        //$paypalFormSettings['item_name'] = @$title; //Router::url('/', true)
        $paypalFormSettings['item_name'] = 'Purchase Practice Exam(s)';
        //$paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['custom'] = $custom1;
        //$paypalFormSettings['amount'] = @$amount; //Router::url('/', true)
        $paypalFormSettings['amount'] = $gross_total; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'student_parents/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'student_parents/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'student_parents/payment_cancel'. '/' . $custom;
		$this->set(compact('studentDetails', 'examDtls','paypalFormSettings', 'gross_total'));
		   }
    
	
	
	
	 /* Start Payment functions by paypal*/
	 
	    public function payment_success() {
			$this->layout = false;
			if ($this->request->is('post') || $this->request->is('put')) {
				   $customString = explode('||', $this->request->data['custom']);
					$ExamIdsAr = explode('#', $customString[0]);
					$eachTotalPriceAr = explode('**', $customString[1]);
					$parent_id = $customString[2];
				
				$dateTime = new DateTime('NOW');
				$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
				$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

				$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
				$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
			
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
										$i = 0;
										foreach($ExamIdsAr as $examId){
											$this->request->data['MyExam']['student_id'] = 0;
											
											$this->request->data['MyExam']['student_parent_id'] = $parent_id;
											$this->request->data['MyExam']['examination_id'] = $examId;
											$this->request->data['MyExam']['payment_status'] = 1;
											$this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
											$this->request->data['MyExam']['amount'] = $eachTotalPriceAr[$i];
											
											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
											
											$this->MyExam->create();
											$this->MyExam->save($this->request->data);
												$i++;
										}
							
							   $this->Session->write('StudentParent.transactionData', $this->request->data);
                              $this->redirect(array('controller' => 'student_parents', 'action' => 'purchase_exam_success'));
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	 public function purchase_exam_success(){
				$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
					
        if($this->Session->check('StudentParent.transactionData')){
            
            $transactionData = $this->Session->read('StudentParent.transactionData');
            $this->Session->delete('StudentParent.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $student_id = $pagedata[0]; 
			 $student_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->Student->read(null, $payPalCustomData['student']);
            $this->MyExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
	
	
	 public function childrens_results() {
                $this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
		 
		 $this->MyExamStudentAnswer->recursive = 3;
		 
		 $this->Examination->unbindModel(array('hasMany' => 'Question'), false);
		 $this->paginate = array(
										'conditions' =>  array(
																 'MyExamStudentAnswer.isdeleted' => 0, 
																 //'MyExamStudentAnswer.exam_type' => 1, 
																 'MyExamStudentAnswer.student_parent_id' => $parent_id
																 ),
										'limit' => 10,
										'order' => 'MyExamStudentAnswer.id DESC',
									);
			$myExamAr = $this->paginate('MyExamStudentAnswer');
		
		
		
		
		
		
		 $this->set(compact('myExamAr'));
		
    }
	
	
	
	
	
	 public function logout() {
			$this->UserConfiguration->parentLogout();
			$this->Session->delete('App.studentData');
			$this->Session->setFlash(__('You have logged out.'), 'success');
			$this->redirect(array('controller' => 'parent', 'action' => 'login'));
    }
	
	
	 protected function _parentLoginRedirect(){
        $isparentLoggedIn = $this->Session->check('App.parentData');
        if ($isparentLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'parent', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
	
   
     public function captcha_match_value() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$captcha_txt = $this->request->data['captcha_txt'];
		
		if(strcasecmp($captcha_txt,$this->Session->read('captcha'))!= 0)
			{
				echo 'ERROR';
			} else {
				echo 'OK';
			}
		
	 }
	 
	 public function get_parent_email_exist() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$email = $this->request->data['parent_email'];
		$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
			if($ExistEmail > 0){
				echo 'EXIST';
			} else {
				echo 'NOT-EXIST';
			}
	
		
	 }
   
   
}
?>