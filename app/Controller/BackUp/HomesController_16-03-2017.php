<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class HomesController extends AppController {

    public $name = 'Homes';
    public $uses = array('Student');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
				public function beforeFilter() {
					parent::beforeFilter();
				 //   $this->settingData = $this->Setting->getSettingsData();
				}
    
	
    
				public function index() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				 public function about_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				 public function contact_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				
	
}
?>