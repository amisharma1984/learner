<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');

require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'configuration.php');
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'connection.php');


class SchoolsController extends AppController {

    public $name = 'Schools';
    public $uses = array(
										'Setting', 'School', 'SchoolInformation','Examination',
										'SchoolPurchaseExam','Student', 'MyExamStudentAnswer',
										'SchoolTakeExam','EmailTemplate', 'StudentAnswer', 'Answer','Question','Payment'
										);
    public $components = array('FilterSchools');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function login() {
        $this->layout = false;
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['School']['login']);
            $password = $this->request->data['School']['password'];
            $userdata = $this->School->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setSchoolData($userdata);
                $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                //$this->redirect(array('controller' => 'schools', 'action' => 'index'));
                $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'schools', 'action' => 'login'));
            }
        }  else {
            $this->_schoolLoginRedirect();
        }
    }
	
	public function myprofile(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			 $schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
        //added by dinesh on 02-05-2017
	    $schoolLevel['Primary'] = $schoolLevelOptions['Primary'];
		$schoolLevel['Secondary'] = $schoolLevelOptions['Secondary'];
		$schoolLevel['Combined'] = $schoolLevelOptions['Combined'].' (K-12)';
		//$schoolLevel['Special'] = $schoolLevelOptions['Special'];
		$schoolLevelOptions = $schoolLevel;
		
		
		
		
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			    $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('nameErr', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('managerNameErr', 'Please enter manager name');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							} else {
								$manager_email = $this->request->data['School']['manager_email'];
								if (!filter_var($manager_email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('manager_emailErr', 'Please enter valid email format');
								}
							} 
							
							
							
							
							if(empty(trim($this->request->data['SchoolInformation']['level']))){
								$ERROR = 1;
								$this->set('levelErr', 'Please select level');
							}
							
							if(empty(trim($this->request->data['SchoolInformation']['street']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter street');
							}
							
								if(empty(trim($this->request->data['SchoolInformation']['phone']))){
								$ERROR = 1;
								$this->set('phoneErr', 'Please enter phone');
							}
							if(empty(trim($this->request->data['SchoolInformation']['fax']))){
								$ERROR = 1;
								$this->set('faxErr', 'Please enter fax');
							}
							if(empty(trim($this->request->data['SchoolInformation']['zip']))){
								$ERROR = 1;
								$this->set('zipErr', 'Please enter post code');
							}
							//if(empty(trim($this->request->data['SchoolInformation']['url']))){
							//	$ERROR = 1;
							//	$this->set('urlErr', 'Please enter School Website');
							//}
							if(empty(trim($this->request->data['SchoolInformation']['type']))){
								$ERROR = 1;
								$this->set('typeErr', 'Please select zip');
							}
							
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emailErr', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emailErr', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email, 'School.id <>' => $school_id)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->request->data['School']['email'] = $this->request->data['School']['email'];
										$this->set('emailErr', 'This email already exist');
										$this->referer();
									}
							}
							
					 
					 if($ERROR == 0){		
							$this->School->save($this->request->data['School']);
							$this->SchoolInformation->save($this->request->data['SchoolInformation']);
							$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
							$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
					 } 
					
					
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
		}				 
	
    
    public function index() {
        
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
		//added by dinesh on 02-05-2017
	    $schoolLevel['Primary'] = $schoolLevelOptions['Primary'];
		$schoolLevel['Secondary'] = $schoolLevelOptions['Secondary'];
		$schoolLevel['Combined'] = $schoolLevelOptions['Combined'].' (K-12)';
		//$schoolLevel['Special'] = $schoolLevelOptions['Special'];
		$schoolLevelOptions = $schoolLevel;
		
        
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$this->School->save($this->request->data['School']);
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
        
        
    }
	
	public function registration() {
        $this->layout = 'school_layout';
		//$this->layout = 'landing_layout';
		$this->_schoolLoginRedirect();
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		   $schoolLevelOptions = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            //'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
       // print_r($schoolLevelOptions);
		
		//$schoolLevelOptions['Combined'] = $schoolLevelOptions['Combined'].' (K-12)';
		
		
		$schoolLevel['Primary'] = $schoolLevelOptions['Primary'];
		$schoolLevel['Secondary'] = $schoolLevelOptions['Secondary'];
		$schoolLevel['Combined'] = $schoolLevelOptions['Combined'].' (K-12)';
		//$schoolLevel['Special'] = $schoolLevelOptions['Special'];
		$schoolLevelOptions = $schoolLevel;
        
        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));
       
		  $this->set(compact('examination_types', 'examination_categories','schoolLevelOptions','types'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
							
								 //school login start
			 if(!empty($this->request->data['SchoolLogin']['login_registration_flag'])  && $this->request->data['SchoolLogin']['login_registration_flag'] == 'school_login'){ 
			  $login = strtolower($this->request->data['SchoolLogin']['login']);
            $password = $this->request->data['SchoolLogin']['password'];
            $userdata = $this->School->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['SchoolLogin']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($userdata) && !empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setSchoolData($userdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
			} 
			 } else {
			
			 //student login End
							
							//school registration
							if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('mNameErr', 'Please enter manager name');
							}
							
							if(empty(trim($this->request->data['SchoolInformation']['street']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter street');
							}
							
							if(empty(trim($this->request->data['SchoolInformation']['town']))){
								$ERROR = 1;
								$this->set('townErr', 'Please enter town');
							}
								if(empty(trim($this->request->data['SchoolInformation']['zip']))){
								$ERROR = 1;
								$this->set('zipErr', 'Please enter post code');
							}
							if(empty(trim($this->request->data['SchoolInformation']['state']))){
								$ERROR = 1;
								$this->set('stateErr', 'Please enter state');
							}
							if(empty(trim($this->request->data['SchoolInformation']['phone']))){
								$ERROR = 1;
								$this->set('phoneErr', 'Please enter phone');
							}
							if(empty(trim($this->request->data['SchoolInformation']['fax']))){
								$ERROR = 1;
								$this->set('faxErr', 'Please enter fax');
							}
							
							//if(empty(trim($this->request->data['SchoolInformation']['url']))){
							//	$ERROR = 1;
							//	$this->set('urlErr', 'Please enter school website');
							//}
							
							
							
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter school email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager  email');
							} else {
								$memail = $this->request->data['School']['manager_email'];
								if (!filter_var($memail, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('manager_emailErr', 'Please enter valid  email format');
								}
							} 
							
							
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
						
							
							if(empty(trim($this->request->data['SchoolInformation']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['SchoolInformation']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['School']['username'] = $this->request->data['School']['email'];
					//$this->request->data['School']['password'] =$this->_randomPassword(8);
					$this->request->data['School']['password'] = md5($this->request->data['School']['password']);
					$this->School->save($this->request->data['School']);
					$this->request->data['SchoolInformation']['school_id'] = $this->School->getLastInsertID();
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					
					$schoolEmail = $this->request->data['School']['email'];
					$full_name = $this->request->data['School']['manager_name'];
					
					////email send start//////
				if (!empty($schoolEmail)) {
						
					$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
					$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
					$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 8)));
					$html_content = $mailTemplateData['EmailTemplate']['content'];
					$validity = 100; //days will come from admin
					//$html_content = $this->_html_for_reset_password();
					
					$html = '';
                    $html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
					$html = str_replace('{{full_name}}', $full_name, $html);
					$html = str_replace('{{validity}}', $validity, $html);
					
                 //  pr($html);die;
				   $email_subject ='Welcome to Naplan Exam';
                    $email = $schoolEmail;
					$mailFrom = 'webmaster@learnerleader.com';
					$FromName = 'Learner Leader Online';
                   
				    $mail = new PHPMailer;
				    $mail->FromName = $FromName;
                    $mail->From = $mailFrom;
                    $mail->Subject = $email_subject;
                    $mail->Body = stripslashes($html);
                    $mail->AltBody = stripslashes($html);
                    $mail->IsHTML(true);
                    $mail->AddAddress($email);
                    $mail->Send();
					
					
					$email_subject1 = 'School Registration Notification';
					$mailFrom1 = $schoolEmail;
					$FromName1 = $full_name;
					$mailTemplateData1 = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 9)));
					$html_content1 = $mailTemplateData1['EmailTemplate']['content'];
					
					$html1 = '';
                    $html1.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content1);
					$html1 = str_replace('{{full_name}}', $full_name, $html1);
					//$html1 = str_replace('{{validity}}', $validity, $html1);
					$admin_email_to = $this->getAdminValue('global_email');
					$AdminEmail = new PHPMailer;
				    $AdminEmail->FromName = $FromName1;
                    $AdminEmail->From = $mailFrom1;
                    $AdminEmail->Subject = $email_subject1;
                    $AdminEmail->Body = stripslashes($html1);
                    $AdminEmail->AltBody = stripslashes($html1);
                    $AdminEmail->IsHTML(true);
                    $AdminEmail->AddAddress($admin_email_to);
                    $AdminEmail->Send();
					
					
					} 
					////email send End//////
					
					
					
					
					
					
					
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'Schools', 'action' => 'registration'));
				}
				
		 }
				
			 }
	  }

	
    
    public function view(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function edit($schoolId = null){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
        
    }

	public function registration_payment_success(){
			
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 
			//below code for re-write session for payment status
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->UserConfiguration->setSchoolData($schoolData);
			 //after payment update check login
			 $this->UserConfiguration->isSchoolLoggedIn();
		}				 
	
    public function registration_payment(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
						 $school_id = $schoolData['School']['id'];
						//Credit card start here
						$ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $studentDetails['Student']['first_name'];
						$lastName= $studentDetails['Student']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('AUD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								 $dateTime = new DateTime('NOW');
								$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
								$schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
								$endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
								$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
								
								$this->School->set(array(
									'id' => $school_id,
									'payment_status' => 1,
									'start_subscription' => $subscriptionStart,
									'end_subscription' => $subscriptionEnd,
									'modified' => $subscriptionStart
								));
								$this->School->save();
								$this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
								$this->redirect(array('controller'=>'schools','action'=>'registration_payment_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							}
						}
						}
							///Credit card payment End from here
						}
		
		
		
        $timeStamp = time();
        $paymentAmount = (!empty($settings['Setting']['school_registration_fee'])) ? $settings['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT;
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY, true);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY));
        }      
        
        $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
            'item_name' => 'School Registration',
            'item_number' => str_pad($schoolData['School']['id'], 10, "0", STR_PAD_LEFT),
            'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'school' => $schoolData['School']['id'],
            'payment_for' => 'registration',
            'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
            'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'app/payment_success';
        //$paypalSettings['return'] = Router::url('/', true) . 'app/registration_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'app/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'app/payment_cancle'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		$this->set(compact('paymentAmount'));	
			$this->render('make_payment');
    }
    
    public function checkFingerprint(){
        
        $this->layout = false;
        $this->autoRender = false;
        $flag = false;
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        
        $extraData = $this->request->data['custom'];
        $amount = $this->request->data['amount'];
        
        $payPalCustomData = array();
        $pagedata = explode('/', $extraData);
        foreach ($pagedata as $key => $value) {
            $exploaded = explode(':', $value);
            $payPalCustomData[$exploaded[0]] = $exploaded[1];
        }
        
        $oldFingerprint = urldecode($payPalCustomData['fp']);
        
        if (phpversion() >= '5.1.2') {
            $newFingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY, true);
        } else {
            $newFingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY));
        }
        
        if($oldFingerprint != $newFingerprint){
            $flag = true;
        }
        echo $flag;
        exit();
    }

    public function registration_success(){
        $this->layout = false;
        
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('/', $transactionData['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
            $endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            $fingerPrint = urldecode($payPalCustomData['fp']);
            
            $this->School->read(null, $payPalCustomData['school']);
            $this->School->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            $this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'schools', 'action' => 'login'));
        $this->redirect(array('controller' => 'schools', 'action' => 'registration_payment_success'));
		
        }
        
    }
    
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->schoolLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'school', 'action' => 'login'));
        //$this->redirect(array('controller' => 'schools', 'action' => 'registration'));
    }
    
    protected function _schoolLoginRedirect(){
       // $isLoggedIn = $this->Session->check('App.schoolData');
        $isSchoolLoggedIn = $this->Session->check('App.schoolData');
        if ($isSchoolLoggedIn) {
            $this->Session->setFlash(__('You have already logged in.'), 'info');
            $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
		//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	//School purchase exam module start
	//below method is currently not using
	 public function create_exam($id = NULL) {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		 $id = $this->data_decrypt($id);
		 $this->set('school_purchase_exam_id', $id);
		 
		 $this->SchoolPurchaseExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
		 $myPurchasedExam = $this->SchoolPurchaseExam->find('first', array(
																										 'conditions' => array(
																																		 'SchoolPurchaseExam.id' => $id, 
																																		 'SchoolPurchaseExam.isdeleted' => 0, 
																																		 'SchoolPurchaseExam.payment_status' => 1
																																		))); // examination_type_id =1 means online exam
		//pr($myPurchasedExam);die;
		
			if($this->request->is('post') || $this->request->is('put')){
				$this->request->data['SchoolTakeExam']['exam_code'] = $myPurchasedExam['SchoolPurchaseExam']['exam_code'];
				
				//pr($this->request->data);die;
				if($this->SchoolTakeExam->save($this->request->data)){
					$lastId = $this->SchoolTakeExam->getLastInsertID();
					$exam_code =  $myPurchasedExam['SchoolPurchaseExam']['exam_code'].'-'.$lastId;
					$this->SchoolTakeExam->id = $lastId;
					$this->SchoolTakeExam->saveField('exam_code', $exam_code);
					$this->Session->setFlash(__('Exam has been created successfully.'), 'success');
					$this->redirect(array('controller'=>'Schools','action'=>'take_exam_list'));
				}
			}
		 $this->set(compact('SchoolDetails', 'myPurchasedExam'));
    }
	
	
	 public function my_purchased_exam() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		 $this->SchoolPurchaseExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			/*$this->Examination->bindModel(array(
            'hasOne' => array(
                'SchoolPurchaseExam' => array(
                    'className' => 'SchoolPurchaseExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1, 'SchoolPurchaseExam.school_id' => $School_id)
                ),
            )

        ));	*/										
		 $myPurchasedExam = $this->SchoolPurchaseExam->find('all', array(
																										 'conditions' => array(
																																		 'SchoolPurchaseExam.school_id' => $School_id, 
																																		 'SchoolPurchaseExam.isdeleted' => 0, 
																																		 'SchoolPurchaseExam.payment_status' => 1
																																		))); // examination_type_id =1 means online exam
		//pr($myPurchasedExam);die;
		
		/*** jitendra kumar 14-11-2017 */
		if(!empty($myPurchasedExam)){
			foreach($myPurchasedExam as $key=>$value){
				$id = $value['SchoolPurchaseExam']['id'];
				$condi = array('MyExamStudentAnswer.my_exam_id'=>$id,'MyExamStudentAnswer.exam_type'=>2);
				$check_exam_take = $this->MyExamStudentAnswer->find('count',array('conditions'=>$condi));
				//echo $check_exam_take;
				if($check_exam_take>0){
					$myPurchasedExam[$key]['SchoolPurchaseExam']['exam_given_status'] = 'Yes';
				}else{
					$myPurchasedExam[$key]['SchoolPurchaseExam']['exam_given_status'] = 'No';
				}
			}
		}
		/** End **/
		 $this->set(compact('SchoolDetails', 'myPurchasedExam'));
    }
	
	/** jitendra 14-11-2017 */
	public function generate_review_exam_code() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		//$id = $this->request->data['id'];
		//$school_id = $this->data_decrypt($id);
		$school_id = $this->request->data['id'];
		$greview_exam_code=substr(str_shuffle('1234567890'),0,4);
		$current_date_time  = date('Y-m-d H:i:s');
		$myPurchasedExam = $this->SchoolPurchaseExam->find('first', array('conditions'=>array('SchoolPurchaseExam.id'=>$school_id)));
		//pr($myPurchasedExam);
		$review_exam_code = $myPurchasedExam['SchoolPurchaseExam']['review_exam_code'];
		$generate_date = $myPurchasedExam['SchoolPurchaseExam']['reviewexamcode_generate_datetime'];
		if(empty($review_exam_code) && $generate_date=='0000-00-00 00:00:00'){
			$data = array();
			$data['SchoolPurchaseExam']['id'] = $school_id;
			//$data['SchoolPurchaseExam']['review_exam_code'] = $school_id.'-'.$greview_exam_code;
			$data['SchoolPurchaseExam']['review_exam_code'] = $school_id.$greview_exam_code;
			$data['SchoolPurchaseExam']['reviewexamcode_generate_datetime'] = $current_date_time;
			$this->SchoolPurchaseExam->save($data);			
			
		} else {
			$seconds = strtotime($current_date_time) - strtotime($generate_date);
			$minutes = round($seconds / 60);
				if($minutes> (60*24*7)){ // 7 days
					$this->SchoolPurchaseExam->updateAll(array('SchoolPurchaseExam.review_exam_code_is_expired' => 1), array('SchoolPurchaseExam.id' => $school_id)); 
					/*$data = array();
					$data['SchoolPurchaseExam']['id'] = $school_id;
					$data['SchoolPurchaseExam']['review_exam_code'] = $school_id.$greview_exam_code;
					$data['SchoolPurchaseExam']['reviewexamcode_generate_datetime'] = $current_date_time;
					$this->SchoolPurchaseExam->save($data);*/	
				}
			}
		
			$data = $this->SchoolPurchaseExam->find('first',array(
													'fields' => 'id,review_exam_code,review_exam_code_is_expired',
													'conditions' => array(
													  'SchoolPurchaseExam.id' => $school_id
													)));
													
			if(!empty($data) && $data['SchoolPurchaseExam']['review_exam_code']!= ''){
				$exam_review_code = $data['SchoolPurchaseExam']['review_exam_code'];
			} else {
				$exam_review_code = '';
			}
			
			if($data['SchoolPurchaseExam']['review_exam_code_is_expired'] == 1){
				echo $exam_review_code.'(Expired)';
			} else {
				echo $exam_review_code;
			}
		
		//exit;

	}
	
	
	public function generate_review_exam_code_jk($id) {
		$this->layout = false;
		$id = $this->request->data['id'];
		$school_id = $this->data_decrypt($id);
		$greview_exam_code=substr(str_shuffle('1234567890'),0,4);
		$current_date_time  = date('Y-m-d H:i:s');
		$myPurchasedExam = $this->SchoolPurchaseExam->find('first', array('conditions'=>array('SchoolPurchaseExam.id'=>$school_id)));
		//pr($myPurchasedExam);
		$review_exam_code = $myPurchasedExam['SchoolPurchaseExam']['review_exam_code'];
		$generate_date = $myPurchasedExam['SchoolPurchaseExam']['reviewexamcode_generate_datetime'];
		if(empty($review_exam_code) && $generate_date=='0000-00-00 00:00:00'){
			$data = array();
			$data['SchoolPurchaseExam']['id'] = $school_id;
			$data['SchoolPurchaseExam']['review_exam_code'] = $school_id.'-'.$greview_exam_code;
			$data['SchoolPurchaseExam']['reviewexamcode_generate_datetime'] = $current_date_time;
			$this->SchoolPurchaseExam->save($data);			
			
		}else{
			$seconds = strtotime($current_date_time) - strtotime($generate_date);
			$minutes = round($seconds / 60);
			if($minutes>15){
				$data = array();
				$data['SchoolPurchaseExam']['id'] = $school_id;
				$data['SchoolPurchaseExam']['review_exam_code'] = $school_id.'-'.$greview_exam_code;
				$data['SchoolPurchaseExam']['reviewexamcode_generate_datetime'] = $current_date_time;
				$this->SchoolPurchaseExam->save($data);	
			}
		}
		$this->redirect(array('controller'=>'schools','action'=>'my_purchased_exam'));
		//echo $school_id.'|'.$review_exam_code;
		
		exit;

	}
	/** End **/
		//below method is currently not using
	 public function take_exam_list() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		$this->SchoolTakeExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
											
		// $allExamAr = $this->SchoolTakeExam->find('all', array('conditions' => array('SchoolTakeExam.isdeleted' => 0, 'SchoolTakeExam.school_id' => $School_id))); 
		
		   $this->paginate = array(
							'conditions' => array('SchoolTakeExam.school_id' => $School_id, 'SchoolTakeExam.isdeleted' => 0),
							'limit' => 10,
							'order' => 'SchoolTakeExam.id DESC'
						);
			$allExamAr = $this->paginate('SchoolTakeExam');
		    $this->set(compact('SchoolDetails', 'allExamAr'));
			//pr($allExamAr);die;
    }
	
	
	 public function checkout() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // pr($this->request->data);die;
			   $this->redirect(array('controller' => 'schools', 'action' => 'purchase_exam_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'SchoolPurchaseExam' => array(
                    'className' => 'SchoolPurchaseExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1, 'SchoolPurchaseExam.school_id' => $School_id)
                ),
            )

        ));	

				 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					// pr($this->data);die;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $this->request->data['Examination']['ids']
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
				 }
				 
		 $this->set(compact('SchoolDetails', 'allExamAr'));
		
    }
	
	
	
	
	public function purchase_exam() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'SchoolPurchaseExam' => array(
                    'className' => 'SchoolPurchaseExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1, 'SchoolPurchaseExam.school_id' => $School_id)
                ),
            )

        ));											
		 $allExamAr = $this->Examination->find('all', array(
							'order' => 'ExaminationCategory.name ASC',
							'conditions' => array(
								 'Examination.isdeleted' => 0, 
								 'Examination.examination_type_id' => 1
								 ))); // examination_type_id =1 means online exam
		//pr($allExamAr);die;
		 $this->set(compact('SchoolDetails', 'allExamAr'));
		
    }
    
    public function payment_error(){
        $this->UserConfiguration->isSchoolLoggedIn();
		$this->layout = 'school_layout';
		$Schooldata1 = $this->UserConfiguration->getSchoolData();
		$School_id = $Schooldata1['School']['id'];
		$SchoolDetails = $this->School->find('first', array('conditions' => array('School.id' => $School_id)));
					
		$this->set(compact('SchoolDetails'));
	 }

    function makePayment($DATA){

        $method = 'PUT' ;
        $customUri = "";
		if (array_key_exists("orderId", $DATA))
		  $customUri .= "/order/" . $DATA["orderId"];

		if (array_key_exists("transactionId", $DATA))
		  $customUri .= "/transaction/" . $DATA["transactionId"];

		$unsetNames = array("orderId", "transactionId", "creditsubmit","nameofcard");

		foreach ($unsetNames as $fieldName) {
		  if (array_key_exists($fieldName, $DATA))
		    unset($DATA[$fieldName]);
		}
		$merchantObj = new Merchant();
		$parserObj = new Parser($merchantObj);
		if (array_key_exists("version", $DATA)) {
		  $merchantObj->SetVersion($DATA["version"]);
		  unset($DATA["version"]);
		}
		$request = $parserObj->ParseRequest($DATA);
		if ($request == ""){
			return "not Getting any request!";
		}
		//  The below code for debugging for that enable Debug to true in configuration file
		/*if ($merchantObj->GetDebug())
        echo $request . "<br/><br/>";*/
        $requestUrl = $parserObj->FormRequestUrl($merchantObj, $customUri);
        $response = $parserObj->SendTransaction($merchantObj, $request, $method);
        return $response ;
  
     }
	
	function randomAsciiChar() {
        $randomKey = '';
        for($i=0;$i<10;$i++){
         $randomKey.= chr(rand(97,122));   
        }
       return $randomKey ; 
    }
     
    public function check3dsPayment($paymentData,$multipleExamIds,$totalPrice,$schoolId,$lastpaymentId){
        Configure::write('debug', 2);
        $responseUrl = Router::url('/',true);
        $asciiCharacter = $this->randomAsciiChar();
        $request=array('apiOperation' => 'CHECK_3DS_ENROLLMENT',
                    'order.amount' => $paymentData['order']['amount'],
                    'order.currency' => $paymentData['order']['currency'],
                    'sourceOfFunds.provided.card.number' => $paymentData['sourceOfFunds']['provided']['card']['number'],
                    'sourceOfFunds.provided.card.expiry.month' => $paymentData['sourceOfFunds']['provided']['card']['expiry']['month'],
                    'sourceOfFunds.provided.card.expiry.year' => $paymentData['sourceOfFunds']['provided']['card']['expiry']['year'],
                    '3DSecureId' => $asciiCharacter,
                    '3DSecure.authenticationRedirect.pageGenerationMode' => 'SIMPLE',
                    '3DSecure.authenticationRedirect.responseUrl' => $responseUrl.'schools/process_authentication_result?secure_id='.$asciiCharacter.'&amount='.$paymentData['order']['amount'].'&orderId='.$paymentData['orderId'].'&transactionId='.$paymentData['transactionId'].'&card_number='.$paymentData['sourceOfFunds']['provided']['card']['number'].'&securityCode='.$paymentData['sourceOfFunds']['provided']['card']['securityCode'].'&expiry_month='.$paymentData['sourceOfFunds']['provided']['card']['expiry']['month'].'&expiry_year='.$paymentData['sourceOfFunds']['provided']['card']['expiry']['year'].'&payment_id='.$lastpaymentId,
                    'merchant' => 'TESTLEALEACOM201',
                    'apiUsername' => 'merchant.TESTLEALEACOM201',
                    'apiPassword' => 'e7e62e16dda921039b854b05c2613a87'
                );
        $url = 'https://paymentgateway.commbank.com.au/api/nvp/version/45';         
        $query = http_build_query($request);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $query);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen( $query)));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
        curl_setopt($ch, CURLOPT_USERPWD, 'merchant.TESTLEALEACOM201' . ":" . 'e7e62e16dda921039b854b05c2613a87');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if (curl_error($ch))
          $response = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
        $values = array();
        $nv_strings = explode ('&', $response);
        foreach ($nv_strings as $s) {
            $nv = explode ('=', $s, 2);
            $name = urldecode ($nv[0]);
            $value = (isset ($nv[1]) ? urldecode($nv[1]) : null);
            $values[$name] = $value;
        }
        //pr($values); exit ;
        return $values ;
    }
     
    public function process_authentication_result(){
        Configure::write('debug', 2);
        $this->autoRender = false ;
        $this->layout = false ;
        
        $SecureId       = $_GET['secure_id'];
        $amount         = $_GET['amount'];
        $orderId        = $_GET['orderId'];
        $transactionId  = $_GET['transactionId'];
        $card_number    = $_GET['card_number'];
        $securityCode   = $_GET['securityCode'];
        $expiry_month   = $_GET['expiry_month'];
        $expiry_year    = $_GET['expiry_year'];
        $nameOnCard     = 'abc';
       // $multipleExamIds= rawurldecode($_GET['examids']);
       // $totalPrice = rawurldecode($_GET['totalPrice']);
        //$parent_id = rawurldecode($_GET['parent_id']);
        
        $paymentId = $_GET['payment_id'];
        
        $arPayment = $this->Payment->find('first',array('fields'=>array('id','loggedin_id','type','exam_ids','price','nu_of_student'),'conditions'=>array('id'=>$paymentId,'isdeleted'=>0)));
        $multipleExamIds= $arPayment['Payment']['exam_ids'];
        $totalPrice= $arPayment['Payment']['price'];
        $nuofstudent= $arPayment['Payment']['nu_of_student'];
        $School_id= $arPayment['Payment']['loggedin_id'];
        $paymentPostArr = array();
        
        $request = array('apiOperation' => 'PROCESS_ACS_RESULT',
                      '3DSecure.paRes' => $_POST['PaRes'],
                      '3DSecureId' => $SecureId,
                      'merchant' => 'TESTLEALEACOM201',
                      'apiUsername' => 'merchant.TESTLEALEACOM201',
                      'apiPassword' => 'e7e62e16dda921039b854b05c2613a87'
                );
        $url = 'https://paymentgateway.commbank.com.au/api/nvp/version/45';         
        $query = http_build_query($request);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $query);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen( $query)));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
        curl_setopt($ch, CURLOPT_USERPWD, 'merchant.TESTLEALEACOM201' . ":" . 'e7e62e16dda921039b854b05c2613a87');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if (curl_error($ch))
          $response = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
         
        $values = array();
        $nv_strings = explode ('&', $response);
        foreach ($nv_strings as $s) {
            $nv = explode ('=', $s, 2);
            $name = urldecode ($nv[0]);
            $value = (isset ($nv[1]) ?$nv[1] : null);
            $values[$name] = $value;
        }
        
        if(array_key_exists('response.3DSecure.gatewayCode',$values)){
            
            if($values['response.3DSecure.gatewayCode']=='AUTHENTICATION_SUCCESSFUL' || $values['response.3DSecure.gatewayCode']=='AUTHENTICATION_ATTEMPTED' || $values['response.3DSecure.gatewayCode']=='AUTHENTICATION_NOT_AVAILABLE'){
                
                $paymentPostArr['3DSecureId']= $SecureId ;
                $paymentPostArr['order']['amount'] = $amount ;
                $paymentPostArr['order']['currency'] = 'AUD' ;
                $paymentPostArr['version'] = 34 ;
                $paymentPostArr['orderId'] = $orderId ;
                $paymentPostArr['transactionId'] = $transactionId ;
                $paymentPostArr['apiOperation'] = 'PAY' ;
                $paymentPostArr['sourceOfFunds']['type'] = 'CARD' ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['number'] = $card_number ; 
                $paymentPostArr['sourceOfFunds']['provided']['card']['expiry']['month'] = $expiry_month ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['expiry']['year'] = $expiry_year ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['securityCode'] = $securityCode ;
                $paymentPostArr['nameofcard'] = $nameOnCard ;
                $paymentPostArr['creditsubmit'] = 'Submit' ;
                
                
				 $response = $this->makePayment($paymentPostArr);
                 $responseArray = json_decode($response, TRUE);
        
                         if ($responseArray == NULL){
                                        	
        	                                $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
        	                                $this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
        									$this->redirect(array('controller'=>'schools','action'=>'payment_error'));                                	
        
                        }else if(array_key_exists("result", $responseArray)){
                                            $result = $responseArray["result"];
                                            if ($result == "FAILURE") {
                                                $errorMessage = "";
                                                $tmpArray = array();
        
                                        		if (array_key_exists("reason", $responseArray)) {
        									    $tmpArray = $responseArray["reason"];
        
        									    if (array_key_exists("explanation", $tmpArray)) {
        									      $errorMessage = rawurldecode($tmpArray["explanation"]);
        									    }
        									    else if (array_key_exists("supportCode", $tmpArray)) {
        									      $errorMessage = rawurldecode($tmpArray["supportCode"]);
        									    }
        									    else {
        									      $errorMessage = "Reason unspecified.";
        									    }									   
        									    }                                	
        										$this->Session->setFlash($errorMessage);
        										$this->set('payment_error', $errorMessage);
        										$this->redirect(array('controller'=>'schools','action'=>'payment_error'));							
        
                                        	}else if($result == "ERROR"){
                                        		$errorMessage = "";
                                                $tmpArray = array();
        	                                	if (array_key_exists("error", $responseArray)) {
        										    $tmpArray = $responseArray["error"];
        										    if (array_key_exists("explanation", $tmpArray)) {
        										     $errorMessage = rawurldecode($tmpArray["explanation"]);
        										    }
        										    else if (array_key_exists("cause", $tmpArray)) {
        										     $errorMessage = rawurldecode($tmpArray["supportCode"]);
        										    }
        										    else{
        										     $errorMessage = "Reason unspecified.";
        										    }
        									    }                                	
        										$this->Session->setFlash($errorMessage);
        										$this->set('payment_error', $errorMessage);
        										$this->redirect(array('controller'=>'schools','action'=>'payment_error'));
                                        	}else if($result == "SUCCESS"){
                                        	    
                                        	    $SchoolDetails = $this->School->find('first', array('conditions' => array('School.id' => $School_id)));
					
				                            	if(!empty($SchoolDetails['School']['name'])){
                                        	    $schoolFullName = strtoupper(str_replace(',', '', $SchoolDetails['School']['name']));
                        						$words = explode(" ", $schoolFullName);
                        						$schoolShortName = "";
                        						foreach ($words as $value) {
                        							$schoolShortName .= substr($value, 0, 1); 
                        						}
                        						} else {
                        							$schoolShortName = 'NE';
                        						}
                                                                	    
                                        	    
                                        	    $MultipleExamPayment = explode('-',$multipleExamIds) ;
                                	        	$total_price = explode('-',$totalPrice) ;
                                	        	$nu_of_student = explode('-',$nuofstudent);
                                                $datas = $MultipleExamPayment;
        							            $i = 0;
        										foreach($datas as $exam_id){
        										$examPrice = $this->Examination->find('first', array('fields' => 'price',				
        																									'conditions' => array(
        																											'Examination.id' => $exam_id
        																									)));
        											$price_per_student = $examPrice['Examination']['price'];														
        											$this->request->data['SchoolPurchaseExam']['school_id'] = $School_id;
        									        $this->request->data['SchoolPurchaseExam']['exam_code'] = $School_id.'-'.$schoolShortName.'-'.$exam_id;
        											
        											$this->request->data['SchoolPurchaseExam']['examination_id'] = $exam_id;
        											$this->request->data['SchoolPurchaseExam']['payment_status'] = 1;
        											$this->request->data['SchoolPurchaseExam']['txn_id'] = $responseArray["transaction"]["id"];;
        											//$this->request->data['SchoolPurchaseExam']['amount'] = $amount;
        											$this->request->data['SchoolPurchaseExam']['order_id'] = $responseArray["order"]["id"];;
        											
        											$this->request->data['SchoolPurchaseExam']['no_of_student'] = $nu_of_student[$i];
        											$this->request->data['SchoolPurchaseExam']['amount'] = $total_price[$i];
        											$this->request->data['SchoolPurchaseExam']['gross_total'] = ($total_price[$i] + ($total_price[$i] *(10/100)));
        											$this->request->data['SchoolPurchaseExam']['price_per_student'] = $price_per_student;
        											
        											$this->SchoolPurchaseExam->create();
        											$this->SchoolPurchaseExam->save($this->request->data);
        											$i++;
        										}
        												
        										$htmlList = '<table style="width:100%;">
        										   <tr style="background: #1f3446; color:#fff;">
        										   <th style="padding:8px;">List</th>
        										   <th style="padding:8px;">Exam Name</th>
        										   <th style="padding:8px;">Category</th>
        										   <th style="padding:8px;">Exam Code</th>
        										   <th style="padding:8px;">Price per student($ AUD)</th>
        										   <th style="padding:8px;">No. of Student</th>
        										   <th style="padding:8px;">Total Paid</th>
        									   </tr>';
        										$j = 0;
        										$k = 1;
        										foreach($datas as $exam_id){
        											$examDtl = $this->Examination->find('first', array(
        																									//'fields' => 'price',				
        																									'conditions' => array(
        																											'Examination.id' => $exam_id
        																									)));
        											$category = explode(' ',$examDtl['ExaminationCategory']['name']);
        											$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
        											$inserted = array();
        											$inserted[] = 'Year '.$category[1];
        											array_splice( $examTitle, 1, 0, $inserted );
        										   $examName = implode(' ', $examTitle);														
        											$exam_code = $School_id.'-'.$schoolShortName.'-'.$exam_id;														
        											$price_per_student = $examDtl['Examination']['price'];
        											//$examName = $examDtl['Examination']['title'];
        										    $htmlList .= '<tr style="text-align:center; background:#EFEBE6">
        													   <td style="padding:8px;">'.$k.'</td>
        													   <td style="padding:8px;">'.$examName.'</td>
        													   <td style="padding:8px;">NAPLAN Style Trial</td>
        													   <td style="padding:8px;">'.$exam_code.'</td>
        													   <td style="padding:8px;">'.$price_per_student.'</td>
        													   <td style="padding:8px;">'.$nu_of_student[$j].'</td>
        													   <td style="padding:8px;">'.($total_price[$j] + ($total_price[$j] *(10/100))).'</td>
        													 </tr>';
        										$j++;
        										$k++;					
        										}
        												
        										$htmlList .= '</table>';
        														
        										$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
        										$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
        										$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 12)));
        										$html_content = $mailTemplateData['EmailTemplate']['content'];
        										$validity = 100; //days will come from admin
        										$full_name = $SchoolDetails['School']['name'];
        										$purcDate  = date('d-m-Y H:i:s') ;
        										
        										$head_teacher_name = $SchoolDetails['School']['manager_name'];
        										$school_name = $SchoolDetails['School']['name'];
        										
        										$html = '';
        										$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
        										$html = str_replace('{{full_name}}', $head_teacher_name, $html);
        										$html = str_replace('{{SCHOOL_NAME}}', $school_name, $html);
        										$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
        										$html = str_replace('{{validity}}', $validity, $html);
        										$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);
        										$email_subject = $mailTemplateData['EmailTemplate']['title'];
        					                    $email = $SchoolDetails['School']['email'];
        					                    //$email = 'dinesh.amstech@gmail.com';
        										$mailFrom = 'sales@learnerleader.com';
        										$FromName = 'Learner Leader Online';
        					                   
        									    $mail = new PHPMailer;
        									    $mail->FromName = $FromName;
        					                    $mail->From = $mailFrom;
        					                    $mail->Subject = $email_subject;
        					                    $mail->Body = stripslashes($html);
        					                    $mail->AltBody = stripslashes($html);
        					                    $mail->IsHTML(true);
        					                    $mail->AddAddress($email);
        					                    //$mail->AddAddress('amarjit.brainium@gmail.com');
        					                    $mail->Send();	
        										$this->redirect(array('controller'=>'Schools','action'=>'purchase_exam_success'));
        										
        
                                        	}
                                             
        
        
                                        }

                
                
                
                
                
            }else{
              $this->Session->setFlash("AUTHENTICATION_FAILED. Please try again");
              $this->set('payment_error', "Internal Error. Processing timeout. Please try again");
              $this->redirect(array('controller'=>'schools','action'=>'payment_error'));   
            }
            
        }else{
           $this->Session->setFlash("Internal Error. Processing timeout. Please try again");
           $this->set('payment_error', "Internal Error. Processing timeout. Please try again");
           $this->redirect(array('controller'=>'schools','action'=>'payment_error')); 
        }
         
         
     }
       
	
	public function purchase_exam_payment($examination_id = NULL){
                    Configure::write('debug', 2);
					$this->UserConfiguration->isSchoolLoggedIn();
					$this->layout = 'school_layout';
					$examination_id = $this->data_decrypt($examination_id);					
					$MultipleExamPayment = $this->Session->read('MultipleExamPayment');
					//pr($MultipleExamPayment); exit ;
					$gross_total = $MultipleExamPayment['gross_total'];	
					$multipleExamIds = implode('-',$MultipleExamPayment['ids']) ;
                    $totalPrice = implode('-',$MultipleExamPayment['total_price']) ;
                    $nuofstudent = implode('-',$MultipleExamPayment['no_of_student']) ;
				
					$Schooldata1 = $this->UserConfiguration->getSchoolData();
					$School_id = $Schooldata1['School']['id'];
					$SchoolDetails = $this->School->find('first', array('conditions' => array('School.id' => $School_id)));
					
					if(!empty($SchoolDetails['School']['name'])){
						$schoolFullName = strtoupper(str_replace(',', '', $SchoolDetails['School']['name']));
						$words = explode(" ", $schoolFullName);
						$schoolShortName = "";
						foreach ($words as $value) {
							$schoolShortName .= substr($value, 0, 1); 
						}
						} else {
							   $schoolShortName = 'NE';
						}					
					 
					    $ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['number']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])) || empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['securityCode']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['nameofcard']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year'])){
								$this->set('year','Please select year');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])){
								$this->set('exp_month', 'Please select month');
							}
							
								///payment Start from here
							if($ERROR == 0){
							    
							    $paymentOptions = $this->Payment->set(array('loggedin_id'=>$School_id,'type'=>'School','exam_ids'=>$multipleExamIds,'price'=>$totalPrice,'nu_of_student'=>$nuofstudent));
					            $this->Payment->save($paymentOptions,false);
					            $lastpaymentId = $this->Payment->getLastInsertId();
					            $is3ds = $this->check3dsPayment($_POST,$multipleExamIds,$totalPrice,$School_id,$lastpaymentId);
					            //pr($is3ds); exit ;
					             if(array_key_exists('response.3DSecure.gatewayCode',$is3ds)){
					                 if($is3ds['response.3DSecure.gatewayCode']=='CARD_ENROLLED'){
					                     
					                     echo $is3ds['3DSecure.authenticationRedirect.simple.htmlBodyContent']; exit ;
					                     
					                 }else if($is3ds['response.3DSecure.gatewayCode']=='CARD_DOES_NOT SUPPORT_3DS'){
					                      $this->Session->setFlash("The card does not support 3DS, and you cannot proceed with 3DS authentication.");
                                          $this->set('payment_error', 'The card does not support 3DS, and you cannot proceed with 3DS authentication.');
        								  $this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
					                 }
					                 else{
					                     if(!empty($is3ds['3DSecureId'])){
					                          $_POST['3DSecureID'] = $is3ds['3DSecureId'];
					                     }
					                   

                                        $response = $this->makePayment($_POST);
                                        $responseArray = json_decode($response, TRUE);
        
                                        if ($responseArray == NULL){
                                        	
        	                                $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
        	                                $this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
        									$this->redirect(array('controller'=>'schools','action'=>'payment_error'));                                	
        
                                        }else if(array_key_exists("result", $responseArray)){
                                            $result = $responseArray["result"];
                                            if ($result == "FAILURE") {
                                                $errorMessage = "";
                                                $tmpArray = array();
        
                                        		if (array_key_exists("reason", $responseArray)) {
        									    $tmpArray = $responseArray["reason"];
        
        									    if (array_key_exists("explanation", $tmpArray)) {
        									      $errorMessage = rawurldecode($tmpArray["explanation"]);
        									    }
        									    else if (array_key_exists("supportCode", $tmpArray)) {
        									      $errorMessage = rawurldecode($tmpArray["supportCode"]);
        									    }
        									    else {
        									      $errorMessage = "Reason unspecified.";
        									    }									   
        									    }                                	
        										$this->Session->setFlash($errorMessage);
        										$this->set('payment_error', $errorMessage);
        										$this->redirect(array('controller'=>'schools','action'=>'payment_error'));							
        
                                        	}else if($result == "ERROR"){
                                        		$errorMessage = "";
                                                $tmpArray = array();
        	                                	if (array_key_exists("error", $responseArray)) {
        										    $tmpArray = $responseArray["error"];
        										    if (array_key_exists("explanation", $tmpArray)) {
        										     $errorMessage = rawurldecode($tmpArray["explanation"]);
        										    }
        										    else if (array_key_exists("cause", $tmpArray)) {
        										     $errorMessage = rawurldecode($tmpArray["supportCode"]);
        										    }
        										    else{
        										     $errorMessage = "Reason unspecified.";
        										    }
        									    }                                	
        										$this->Session->setFlash($errorMessage);
        										$this->set('payment_error', $errorMessage);
        										$this->redirect(array('controller'=>'schools','action'=>'payment_error'));
                                        	}else if($result == "SUCCESS"){
                                                $datas = $MultipleExamPayment['ids'];
        							            $i = 0;
        										foreach($datas as $exam_id){
        										$examPrice = $this->Examination->find('first', array('fields' => 'price',				
        																									'conditions' => array(
        																											'Examination.id' => $exam_id
        																									)));
        											$price_per_student = $examPrice['Examination']['price'];														
        											$this->request->data['SchoolPurchaseExam']['school_id'] = $School_id;
        									        $this->request->data['SchoolPurchaseExam']['exam_code'] = $School_id.'-'.$schoolShortName.'-'.$exam_id;
        											
        											$this->request->data['SchoolPurchaseExam']['examination_id'] = $exam_id;
        											$this->request->data['SchoolPurchaseExam']['payment_status'] = 1;
        											$this->request->data['SchoolPurchaseExam']['txn_id'] = $responseArray["transaction"]["id"];;
        											//$this->request->data['SchoolPurchaseExam']['amount'] = $amount;
        											$this->request->data['SchoolPurchaseExam']['order_id'] = $responseArray["order"]["id"];;
        											
        											$this->request->data['SchoolPurchaseExam']['no_of_student'] = $MultipleExamPayment['no_of_student'][$i];
        											$this->request->data['SchoolPurchaseExam']['amount'] = $MultipleExamPayment['total_price'][$i];
        											$this->request->data['SchoolPurchaseExam']['gross_total'] = ($MultipleExamPayment['total_price'][$i] + ($MultipleExamPayment['total_price'][$i] *(10/100)));
        											$this->request->data['SchoolPurchaseExam']['price_per_student'] = $price_per_student;
        											
        											$this->SchoolPurchaseExam->create();
        											$this->SchoolPurchaseExam->save($this->request->data);
        											$i++;
        										}
        												
        										$htmlList = '<table style="width:100%;">
        										   <tr style="background: #1f3446; color:#fff;">
        										   <th style="padding:8px;">List</th>
        										   <th style="padding:8px;">Exam Name</th>
        										   <th style="padding:8px;">Category</th>
        										   <th style="padding:8px;">Exam Code</th>
        										   <th style="padding:8px;">Price per student($ AUD)</th>
        										   <th style="padding:8px;">No. of Student</th>
        										   <th style="padding:8px;">Total Paid</th>
        									   </tr>';
        										$j = 0;
        										$k = 1;
        										foreach($datas as $exam_id){
        											$examDtl = $this->Examination->find('first', array(
        																									//'fields' => 'price',				
        																									'conditions' => array(
        																											'Examination.id' => $exam_id
        																									)));
        											$category = explode(' ',$examDtl['ExaminationCategory']['name']);
        											$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
        											$inserted = array();
        											$inserted[] = 'Year '.$category[1];
        											array_splice( $examTitle, 1, 0, $inserted );
        										   $examName = implode(' ', $examTitle);														
        											$exam_code = $School_id.'-'.$schoolShortName.'-'.$exam_id;														
        											$price_per_student = $examDtl['Examination']['price'];
        											//$examName = $examDtl['Examination']['title'];
        										    $htmlList .= '<tr style="text-align:center; background:#EFEBE6">
        													   <td style="padding:8px;">'.$k.'</td>
        													   <td style="padding:8px;">'.$examName.'</td>
        													   <td style="padding:8px;">NAPLAN Style Trial</td>
        													   <td style="padding:8px;">'.$exam_code.'</td>
        													   <td style="padding:8px;">'.$price_per_student.'</td>
        													   <td style="padding:8px;">'.$MultipleExamPayment["no_of_student"][$j].'</td>
        													   <td style="padding:8px;">'.($MultipleExamPayment["total_price"][$j] + ($MultipleExamPayment["total_price"][$j] *(10/100))).'</td>
        													 </tr>';
        										$j++;
        										$k++;					
        										}
        												
        										$htmlList .= '</table>';
        														
        										$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
        										$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
        										$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 12)));
        										$html_content = $mailTemplateData['EmailTemplate']['content'];
        										$validity = 100; //days will come from admin
        										$full_name = $SchoolDetails['School']['name'];
        										$purcDate  = date('d-m-Y H:i:s') ;
        										
        										$head_teacher_name = $SchoolDetails['School']['manager_name'];
        										$school_name = $SchoolDetails['School']['name'];
        										
        										$html = '';
        										$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
        										$html = str_replace('{{full_name}}', $head_teacher_name, $html);
        										$html = str_replace('{{SCHOOL_NAME}}', $school_name, $html);
        										$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
        										$html = str_replace('{{validity}}', $validity, $html);
        										$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);
        										$email_subject = $mailTemplateData['EmailTemplate']['title'];
        					                    $email = $SchoolDetails['School']['email'];
        					                    //$email = 'dinesh.amstech@gmail.com';
        										$mailFrom = 'sales@learnerleader.com';
        										$FromName = 'Learner Leader Online';
        					                   
        									    $mail = new PHPMailer;
        									    $mail->FromName = $FromName;
        					                    $mail->From = $mailFrom;
        					                    $mail->Subject = $email_subject;
        					                    $mail->Body = stripslashes($html);
        					                    $mail->AltBody = stripslashes($html);
        					                    $mail->IsHTML(true);
        					                    $mail->AddAddress($email);
        					                    //$mail->AddAddress('amarjit.brainium@gmail.com');
        					                    $mail->Send();	
        										$this->redirect(array('controller'=>'Schools','action'=>'purchase_exam_success'));
        										
        
                                        	}
                                             
        
        
                                        }
					                 }
					             }else{
					                 $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
        	                         $this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
        							 $this->redirect(array('controller'=>'schools','action'=>'payment_error'));
					             }
						
						}
							///payment End from here
			}
						
		$paypalFormSettings = array(
            'payment_url' => 'https://www.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'sngpublishing@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'School Registration',
            'item_number' => str_pad($SchoolDetails['School']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'school_id' => $SchoolDetails['School']['id'],
            'examination_id' => $examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );        

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
		
		$examID = null;
		$datas = $MultipleExamPayment['ids'];
			foreach($datas as $exam_id){
				if (!empty($examID)) {
					$examID .= "";
				}
				$examID .= "#" . $exam_id;
			}
		
		$noOfstudents = null;
		$no_of_student = $MultipleExamPayment['no_of_student'];
			foreach($no_of_student as $val){
				if (!empty($noOfstudents)) {
					$noOfstudents .= "";
				}
				$noOfstudents .= "@" . $val;
			}
			
			 $eachTotalPrice = null;
		$total_price = $MultipleExamPayment['total_price'];
			foreach($total_price as $val){
				if (!empty($eachTotalPrice)) {
					$eachTotalPrice .= "";
				}
				$eachTotalPrice .= "**" . $val;
			}
		
		$custom1 = trim($examID, '#').'||'.trim($noOfstudents, '@').'||'.trim($eachTotalPrice, '**').'||'.$SchoolDetails['School']['id'];
		
		$transactionId = "Trans".md5(uniqid(rand(), true))  ;
		$orderId       = "Order".md5(uniqid(rand(), true))  ;
        $transOrderId = array('transactionId'=>$transactionId,'orderId'=>$orderId);
		
		
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalFormSettings['item_name'] = 'Purchase Exam(s)'; //Router::url('/', true)
        //$paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['custom'] = $custom1; //Router::url('/', true)
        $paypalFormSettings['amount'] = $gross_total; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'Schools/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'Schools/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'Schools/payment_cancel'. '/' . $custom1;
		$this->set(compact('SchoolDetails', 'examDtls','paypalFormSettings', 'gross_total','transOrderId'));
	}
    
	
	public function purchase_exam_success(){
			
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 
			//below code for re-write session for payment status
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->UserConfiguration->setSchoolData($schoolData);
			 //after payment update check login
			 $this->UserConfiguration->isSchoolLoggedIn();
			 $this->Session->setFlash(__('Payment has been successfully done.'), 'success');
			 $this->render('registration_payment_success');
		}			
	
	//paypal success
	   public function payment_success() {
        $this->layout = false;
        //$this->autoRender = false;
					$Schooldata1 = $this->UserConfiguration->getSchoolData();
					 $School_id = $Schooldata1['School']['id'];
					 $SchoolDetails = $this->School->find('first', array('conditions' => array('School.id' => $School_id)));
					
					$schoolFullName = strtoupper(str_replace(',', '', $SchoolDetails['School']['name']));
					$words = explode(" ", $schoolFullName);
					$schoolShortName = "";
					foreach ($words as $value) {
						$schoolShortName .= substr($value, 0, 1); 
					}
        
		
		
        if ($this->request->is('post') || $this->request->is('put')) {
         //   pr($this->request->data);
           // echo 'next array';
            $payPalCustomData = array();
            //$pagedata = explode('##', $this->request->data['custom']);
			
			   $customString = explode('||', $this->request->data['custom']);
				$ExamIdsAr = explode('#', $customString[0]);
				$noOfstudentsAr = explode('@', $customString[1]);
				$eachTotalPriceAr = explode('**', $customString[2]);
				$school_id = $customString[3];
				
		
			
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
                
								/*$this->request->data['SchoolPurchaseExam']['school_id'] = $pagedata[0];
								$this->request->data['SchoolPurchaseExam']['exam_code'] = $pagedata[0].'-'.$schoolShortName.'-'.$pagedata[1];
								$this->request->data['SchoolPurchaseExam']['examination_id'] = $pagedata[1];
								$this->request->data['SchoolPurchaseExam']['payment_status'] = 1;
								$this->request->data['SchoolPurchaseExam']['txn_id'] = $this->request->data['txn_id'];
								$this->request->data['SchoolPurchaseExam']['amount'] = $this->request->data['payment_gross'];*/
										$i = 0;
										foreach($ExamIdsAr as $examId){
											
											$examPrice = $this->Examination->find('first', array(
																								'fields' => 'price',				
																								'conditions' => array(
																										'Examination.id' => $examId
																								)));
										    $price_per_student = $examPrice['Examination']['price'];
											
												$this->request->data['SchoolPurchaseExam']['school_id'] = $school_id;
												$this->request->data['SchoolPurchaseExam']['exam_code'] = $school_id.'-'.$schoolShortName.'-'.$examId;
												$this->request->data['SchoolPurchaseExam']['examination_id'] = $examId;
												$this->request->data['SchoolPurchaseExam']['payment_status'] = 1;
												$this->request->data['SchoolPurchaseExam']['txn_id'] = $this->request->data['txn_id'];
												$this->request->data['SchoolPurchaseExam']['amount'] = $eachTotalPriceAr[$i];
												
												$this->request->data['SchoolPurchaseExam']['gross_total'] = ($eachTotalPriceAr[$i] + ($eachTotalPriceAr[$i] *(10/100))); //10% gst
										        $this->request->data['SchoolPurchaseExam']['price_per_student'] = $price_per_student;										
												
												
												$this->request->data['SchoolPurchaseExam']['no_of_student'] = $noOfstudentsAr[$i];
												$this->SchoolPurchaseExam->create();
												$this->SchoolPurchaseExam->save($this->request->data);
												$i++;
										}
										
									
								// $this->Flash->success(__("transaction successfully completed"));
								 //$this->Session->setFlash('Transaction successfully completed');
								  $this->Session->write('School.transactionData', $this->request->data);
								$this->redirect(array('controller'=>'schools','action'=>'purchase_exam_success'));
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	
    public function purchase_exam_success__(){
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
        $this->set(compact('SchoolDetails'));
		
		die;
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $School_id = $pagedata[0]; 
			 $School_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->School->read(null, $payPalCustomData['School']);
            $this->SchoolPurchaseExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Schools', 'action' => 'login'));
        }
        
    }
    
	
	//School purchase exam module End
	
	//add student by school manager start
	  public function add_student(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $school_purchase_exams = $this->SchoolPurchaseExam->find('all', array(
																																	 'fields' => array('SchoolPurchaseExam.id', 'Examination.id', 'Examination.title'),
																																	 'conditions' => array(
																																	 'SchoolPurchaseExam.school_id' => $school_id,
																																	 'SchoolPurchaseExam.payment_status' => 1,
																																	 'SchoolPurchaseExam.isdeleted' => 0,
																																	 'SchoolPurchaseExam.isexpired' => 0,
																																	  'Examination.isdeleted' => 0,
																																	 
																																	 )));
			$SchoolPurchaseExamOption = '';
			foreach($school_purchase_exams as $val){
				$SchoolPurchaseExamOption .= '<option value="'.$val['SchoolPurchaseExam']['id'].'" >'.$val['Examination']['title'].' </option>';
			}
			 
			 $this->set(compact('SchoolPurchaseExamOption'));
			   $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							//pr($this->data);die;
						
				if($ERROR == 0){		
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					//$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->request->data['Student']['school_id'] = $school_id;
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Student data has been added successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'student_list'));
				}						
							
							
				 }
			 

			
	  }
	  
	  
	    public function edit_student($student_id = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$student_id = $this->data_decrypt($student_id);
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $school_purchase_exams = $this->SchoolPurchaseExam->find('all', array(
																																	 'fields' => array('SchoolPurchaseExam.id', 'Examination.id', 'Examination.title'),
																																	 'conditions' => array(
																																	 'SchoolPurchaseExam.school_id' => $school_id,
																																	 'SchoolPurchaseExam.payment_status' => 1,
																																	 'SchoolPurchaseExam.isdeleted' => 0,
																																	 'SchoolPurchaseExam.isexpired' => 0,
																																	  'Examination.isdeleted' => 0,
																																	 
																																	 )));
			$SchoolPurchaseExamOption = '';
			foreach($school_purchase_exams as $val){
				$SchoolPurchaseExamOption .= '<option value="'.$val['SchoolPurchaseExam']['id'].'" >'.$val['Examination']['title'].' </option>';
			}
			 
			 $this->set(compact('SchoolPurchaseExamOption'));
			   $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email, 'Student.id !=' => $student_id)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							//pr($this->data);die;
						
				if($ERROR == 0){		
					$this->request->data['Student']['id'] = $student_id;
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					//$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->request->data['Student']['school_id'] = $school_id;
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Student data has been updated successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'student_list'));
				}						
							
							
				 }
			 
			 $this->request->data = $this->Student->read(NULL,$student_id);
			 $this->render('add_student');

			
	  }
	  
	  //fetch student percentage who gives wrong answer
		public function fetch_student_wrong_answer($school_purchase_exam_id = NULL, $question_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//pr($studentAppearAr);die;
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$wrong_answer = 0;
			$i = 1;
			$pass = 0;
			$j = 1;
			$noOfStudentCorrectAns = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																 // 'limit' => $limit, 
																																 // 'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.question_id' => $question_id, 
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 //'StudentAnswer.answer_id !=' => 0
																																			 'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																			 )));

			
			
			$answer_ids = '';
			$descriptiveWrongAnswer = 0;
			foreach($StudentAnswerAr as $val){
				if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				}
				
				 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
					 if(!empty($descriptiveAnswer)){
					 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
				
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
					 if($stAns != $corAns && !empty($stAns) && !empty($corAns)){
						 $i = 1;
						  $descriptiveWrongAnswer = $descriptiveWrongAnswer + $i;
						  $i++;
					  }
				 }
				
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  //pr($answer_ids_ar);
			  $WrongAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 0, //for wrong answer
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_ar
																												  )));
			
		
		
			  $wrong_answer = $wrong_answer + $WrongAnswer;
			
			 }
			
		
			
		    $percentageStudent = ($wrong_answer * 100) / $no_of_student_appear;
			return round($percentageStudent,2);
		
	  }
	
	
	
	
	
	public function fetch_student_correct_answer($school_purchase_exam_id = NULL, $question_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//pr($studentAppearAr);die;
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$j = 1;
			$noOfStudentCorrectAns = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																 // 'limit' => $limit, 
																																 // 'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.question_id' => $question_id, 
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 //'StudentAnswer.answer_id !=' => 0
																																'OR' => array(
																																				 'StudentAnswer.answer_id !=' => 0,
																																				'StudentAnswer.answer_text !=' => '',
																																			 ) 
																																 
																																			 )));

			
			
			$answer_ids = '';
			$descriptiveCorrectAnswer = 0;
			foreach($StudentAnswerAr as $val){
				//if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				//}
				
		 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
				
				
				
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  //pr($answer_ids_ar);
			  $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_ar
																												  )));
			
		
			  // echo 'correct'.$correctAnswer;die;
			   if($correctAnswer >= 0){
				    $noOfStudentCorrectAns = $noOfStudentCorrectAns + $j;
					$j++;
			   }
			  
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
		    $correct_answer = ($correct_answer + $descriptiveCorrectAnswer);
			
		    $percentageStudent = ($correct_answer * 100) / $no_of_student_appear;
		    //$percentageStudent = ($noOfStudentCorrectAns * 100) / $no_of_student_appear;
			$no_of_student_selecting_correct_ans = $correct_answer;
			return $no_of_student_selecting_correct_ans.'##'.round($percentageStudent,2);
			//return $correctAnswer;
		
	  }
	
	
		public function fetch_avarage_correct_ans($school_purchase_exam_id = NULL, $offset = NULL, $limit = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 
			
			
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$correct_answer = 0;
			$i = 1;
			$j = 1;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																  'limit' => $limit, 
																																  'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			// 'StudentAnswer.answer_id !=' => 0
																																			 )));

			
			
			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				}
			 }
			 
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			
			  $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_ar
																												  )));
			
		
		
			 $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
		
			
			$averageCorrectAns = ($correct_answer) / $no_of_student_appear;
			return $averageCorrectAns;
			//return $no_of_student_appear;
		
	  }
	
	
	public function fetch_standard_deviation($school_purchase_exam_id = NULL, $question_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//pr($studentAppearAr);die;
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$j = 1;
			$noOfStudentCorrectAns = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																 // 'limit' => $limit, 
																																 // 'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.question_id' => $question_id, 
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 'StudentAnswer.answer_id !=' => 0
																																			 )));

			
			
			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				}
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  //pr($answer_ids_ar);
			  $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_ar
																												  )));
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
		  $average = ($correct_answer / $no_of_student_appear);
		  $d1 = ($average - $correct_answer);
		  $daviation= ($d1*$d1);
		  if($no_of_student_appear == 1){
			    $standardDeviation = round(($daviation) / sqrt($no_of_student_appear), 2);
		  } else {
			    $standardDeviation = round(($daviation) / sqrt($no_of_student_appear -1), 2);
		  }
		
		  
			return $standardDeviation;
			
	  }
	
	
	
	
public function fetch_standard_deviation_bak_dpr($school_purchase_exam_id = NULL, $offset = NULL, $limit = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 
			
			
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$correct_answer = 0;
			$i = 1;
			$j = 1;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																  'limit' => $limit, 
																																  'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			// 'StudentAnswer.answer_id !=' => 0
																																			 )));

			
			
			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				}
			 }
			 
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			
			  $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_ar
																												  )));
			
		
		
			 $correct_answer = $correct_answer + $correctAnswer;
			 }
			
		
			
			$averageCorrectAns = ($correct_answer) / $no_of_student_appear;
			
			
			//2nd step
				$daviation = 0;
				$StudentAnswerArr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerArr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																  'limit' => $limit, 
																																  'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			// 'StudentAnswer.answer_id !=' => 0
																																			 )));

			
			
			$answer_idss = '';
			foreach($StudentAnswerArr as $val){
				if($val['StudentAnswer']['answer_id'] !=0){
				   $answer_idss = $answer_idss.','.$val['StudentAnswer']['answer_id'];
				}
			 }
			 
			  $answer_idss = trim($answer_idss,",");
			  $answer_ids_arr = explode(',', $answer_idss);
			
			  $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $answer_ids_arr
																												  )));
			
		
		
			 $correct_answer = $correct_answer + $correctAnswer;
			 
			 	$d1 = ($averageCorrectAns - $correctAnswer);
				$d2 = ($d1*$d1);
				$daviation = $daviation + $d2;
				
			 }
			
			//$standardDeviation = ($daviation) / sqrt($no_of_student_appear - 1);
			$standardDeviation = round(($daviation) / sqrt($limit -1), 2);
			
		
			
			return $standardDeviation;
			//return $no_of_student_appear;
		
	  }
	
		
	
		  public function get_student_who_gives_correct_ans_bak(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			 $school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			 $question_id = $this->request->data['question_id'];
			 
			 if(!empty($school_purchase_exam_id)){
				 $studentData = $this->MyExamStudentAnswer->find('all', array(
																										 'conditions' => array(
																																		 'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																																		 'MyExamStudentAnswer.exam_type' => 2 // 2 for school taken
																													 )));
				
				
				/*$html = '<table><tr><th>List</th><th>Student Name</th>';
				$i = 1;
				 foreach($studentData as $val){
						 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td></tr>';
						$i++;
				 }
				  $html .= '</table>';
				  echo $html;
				  */
				  
				  if(!empty($studentData)){
				$html = '<table><tr><th>List</th><th>Student Name</th><th>Option</th>';
				$i = 1;
				 foreach($studentData as $val){
					     $student_id = $val['Student']['id'];
						 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td><td><a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewEachQuestionAnswer" onclick="get_each_particular_question_ans('.$student_id.','.$question_id.','.$school_purchase_exam_id.')">View Details</a></td></tr>';
						$i++;
				 }
				  $html .= '</table>';
				  echo $html;
				} 
				  
			 } 
			 
		  }
	
	
	
	 public function get_student_who_gives_wrong_ans(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			//$school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			$school_purchase_exam_id =$this->request->data['school_purchase_exam_id'];
			$question_id = $this->request->data['question_id'];
			
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//pr($studentAppearAr);die;
		
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																 // 'limit' => $limit, 
																																 // 'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.question_id' => $question_id, 
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			// 'StudentAnswer.answer_id !=' => 0
																																			'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																			 )));

			
			
			
			//$student_ids = '';
			$descriptiveWrongAnswer = 0;
			foreach($StudentAnswerAr as $val){
				  // $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				// echo $val['StudentAnswer']['student_id'];
				/*
				 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
					 if(!empty($descriptiveAnswer)){
					 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
				
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
					 if($stAns != $corAns && !empty($stAns) && !empty($corAns)){
						 $i = 1;
						  $descriptiveWrongAnswer = $descriptiveWrongAnswer + $i;
						  $i++;
					  }
				 }
				*/
				
				
				     $correctAnswer = $this->Answer->find('first', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $val['StudentAnswer']['answer_id']
																												  )));
				   
				  // echo $correctAnswer;
				
				   if(empty($correctAnswer)){
					   @$student_ids = @$student_ids.','.$val['StudentAnswer']['student_id'];
					    //echo $val['StudentAnswer']['student_id'];
				   }
				}
			 }
			
			//die;
		
			  @$student_ids = trim(@$student_ids,",");
			  $student_ids_arr = explode(',', @$student_ids);
			   $studentData = $this->Student->find('all', array(
																										 'conditions' => array(
																																		 'Student.id' => $student_ids_arr, 
																																		 'Student.isdeleted' => 0 
																													 )));
				
				
				if(!empty($studentData)){
				$html = '<table><tr><th>List</th><th>Student Name</th><th>Option</th>';
				$i = 1;
				 foreach($studentData as $val){
					     $student_id = $val['Student']['id'];
						 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td><td><a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewEachQuestionAnswer" onclick="get_each_particular_question_ans('.$student_id.','.$question_id.','.$school_purchase_exam_id.')">View Details</a></td></tr>';
						$i++;
				 }
				  $html .= '</table>';
				  echo $html;
				} 
		
	  }
	
	
	
	 public function get_each_particular_question_ans(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			//$school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			$school_purchase_exam_id =$this->request->data['school_purchase_exam_id'];
			$question_id = $this->request->data['question_id'];
			$student_id = $this->request->data['student_id'];
			
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			// $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $MyExamStudentAnswerData = $this->MyExamStudentAnswer->find('first', array(
																													'fields' => 'id, unique_flag',	
																													 'conditions' => array(
																													 'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																													 'MyExamStudentAnswer.exam_type' => 2,
																													 'MyExamStudentAnswer.student_id' => $student_id,
																													 'MyExamStudentAnswer.school_id' => $school_id
																													 
																													 )));	
		
		
			//echo $school_purchase_exam_id.'##'.$question_id.'@@'.$student_id;
			 $unique_flag = $MyExamStudentAnswerData['MyExamStudentAnswer']['unique_flag'];
			  $this->StudentAnswer->recursive = 2;
			  $QuestionAns = $this->StudentAnswer->find('first', array(
																										'conditions' => array(
																													'StudentAnswer.question_id' => $question_id, 
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $unique_flag
																													 )));
			
			
			$qval = $QuestionAns;
			//$html = '<div><b>Q.</b> '.$qval['Question']['title'];
			$html = '<div><b>Q.</b> '.preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $qval['Question']['title']);
			$html .= '</div></br>';
			
			$html .= '<div class="big_answer">';
			$k = 'radio';
			foreach($qval['Question']['Answer'] as $ansval){
				
				if($ansval['answer_type'] == 1){
					$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
				}
				else if($ansval['answer_type'] == 2){
					if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
						$checked = 'checked';
					} else {
						$checked = ' ';
					}
					$answer_type = '<input type="checkbox" '.$checked.' disabled="disabled" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
					} else if($ansval['answer_type'] == 3){
						if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
							$checked = 'checked';
						} else {
							$checked = ' ';
						}
						$answer_type = '<input type="radio"  '.$checked.' disabled="disabled" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="'.$k.'_ans[]">';

						} else if($ansval['answer_type'] == 4){
						$answer_type = '<textarea readonly="readonly" name="ans_'.$qval['Question']['id'].'">'.$qval['StudentAnswer']['answer_text'].'</textarea>';
				}

				if($ansval['answer_type'] == 4){
							 $html .= $answer_type;
					 } else {
							 //$html .= $answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
							 $html .= $answer_type.'&nbsp;&nbsp;&nbsp;'. preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $ansval['answer_text']).'&nbsp;&nbsp;&nbsp;';
							
							// preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $string);
					}
				
			}
			
			$html .= ' <div class="qus_details" style="padding:20px 0;">Correct Answer :';
			 foreach($qval['Question']['CorrectAnswer'] as $corrans) { 
					    $html .= $corrans['answer_text'];
					  if(!empty($corrans["ans_description"])){
					     $html .='<div class="qus_details" style="padding:20px 0;"><a href="javascript:void(0);" onclick="answer_details()">View Details</a></div>';
                         $html .= '<div class="qus_details" id="details_showhide" style="display:none;padding:20px 0;">Answer Description : '.$corrans["ans_description"].'</div>';
					  }
			 }
			$html .= '</div>';
			$html .= '</div>';
			
			echo $html;
			//pr($QuestionAns);
	 }
	
	
	 public function get_student_who_gives_correct_ans(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			//$school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			$school_purchase_exam_id =$this->request->data['school_purchase_exam_id'];
			$question_id = $this->request->data['question_id'];
			
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//pr($studentAppearAr);die;
		
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																																 'order' => 'StudentAnswer.question_order ASC',
																																 // 'limit' => $limit, 
																																 // 'offset' => $offset, 
																																  'recursive' => -1,
																																'conditions' => array(
																																			 'StudentAnswer.question_id' => $question_id, 
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 //'StudentAnswer.answer_id !=' => 0
																																			 'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		       )
																																			 )));

			
			
			
			//$student_ids = '';
			$descriptiveCorrectAnswer = 0;
			foreach($StudentAnswerAr as $val){
				  // $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				 // echo $val['StudentAnswer']['student_id'];
				 
				   $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
					 if(!empty($descriptiveAnswer)){
					 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
				
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
					 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
						 $i = 1;
						  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
						  $i++;
					  }
				}
				 
				 
				 
				     $correctAnswer = $this->Answer->find('count', array(
																								
																												  'conditions' => array(
																																			  'Answer.iscorrect' => 1,
																																			  'Answer.answer_type' => 3, 
																																			  'Answer.id' => $val['StudentAnswer']['answer_id']
																												  )));
				   
				  // echo $correctAnswer;
				  
				  	$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
				  
				   if($correctAnswer > 0){
					   @$student_ids = @$student_ids.','.$val['StudentAnswer']['student_id'];
					    //echo $val['StudentAnswer']['student_id'];
				   }
				}
			 }
			
		
			  $student_ids = trim($student_ids,",");
			  $student_ids_arr = explode(',', $student_ids);
			   $studentData = $this->Student->find('all', array(
																										 'conditions' => array(
																																		 'Student.id' => $student_ids_arr, 
																																		 'Student.isdeleted' => 0 
																													 )));
				
				
				/*if(!empty($studentData)){
				$html = '<table><tr><th>List</th><th>Student Name</th>';
				$i = 1;
				 foreach($studentData as $val){
						 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td></tr>';
						$i++;
				 }
				  $html .= '</table>';
				  echo $html;
				} */
				
				  if(!empty($studentData)){
					$html = '<table><tr><th>List</th><th>Student Name</th><th>Option</th>';
					$i = 1;
					 foreach($studentData as $val){
							 $student_id = $val['Student']['id'];
							 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td><td><a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewEachQuestionAnswer" onclick="get_each_particular_question_ans('.$student_id.','.$question_id.','.$school_purchase_exam_id.')">View Details</a></td></tr>';
							$i++;
					 }
					  $html .= '</table>';
					  echo $html;
					} 
		
	  }
	
	
	
		  public function result_chart($school_purchase_exam_id = NULL, $cal_non_cal = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'result_chart_school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 $schoolPurchaseExamData = $this->SchoolPurchaseExam->find('first', array('conditions' => array('SchoolPurchaseExam.id' => $school_purchase_exam_id)));
			 $examination_id = $schoolPurchaseExamData['SchoolPurchaseExam']['examination_id'];
			 
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
					$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			// 'StudentAnswer.answer_id !=' => 0
																																			'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		       )
																																			 )));

			$answer_ids = '';
			$descriptiveCorrectAnswer = 0;
			foreach($StudentAnswerAr as $val){
				 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
				 
				  $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
					 if(!empty($descriptiveAnswer)){
					 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
				
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
					 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
						 $i = 1;
						  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
						  $i++;
					  }
				}
				 
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
			 	$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
			 
			   if($correctAnswer != 0){
				   $percentage = ($correctAnswer * 100) / $total_set_question;
				   if($percentage >= 30){ // pass marks 30%
					   $pass = $pass + $i;
					   $i++;
				   }
				   
			   }
			  
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
			$avereageCorrectAns = $correct_answer / $no_of_student_appear;
			if($no_of_student_appear > $pass){
				$fail = $no_of_student_appear - $pass;
			} else {
				$fail = 0;
			}
			
			// $totalAttemptQs = count($StudentAnswerAr);	
			//fetch question data
			 if(!empty($cal_non_cal) && $cal_non_cal == 'non-calculator'){
				 $conditions[] = array(
												  'Question.examination_id' => $examination_id, 
												  'Question.isdeleted' => 0,
												  'Question.section' => 'exam_section_2_name' //exam_section_2_name(Non calculator)/ exam_section_1_name(Calculator allowed)
												  );
			 } else  if(!empty($cal_non_cal) && $cal_non_cal == 'calculator'){
				 $conditions[] = array(
												  'Question.examination_id' => $examination_id, 
												  'Question.isdeleted' => 0,
												  'Question.section' => 'exam_section_1_name' //exam_section_2_name(Non calculator)/ exam_section_1_name(Calculator allowed)
												  );
			 } else {
				 $conditions[] = array(
												  'Question.examination_id' => $examination_id, 
												  'Question.isdeleted' => 0,
												  //'Question.section' => 'exam_section_2_name' //exam_section_2_name(Non calculator)/ exam_section_1_name(Calculator allowed)
												  );
			 }
				
												  
			  $this->paginate = array(
										'conditions' =>$conditions,
										//'limit' => 10,
										'limit' => $total_set_question,
										'order' => 'Question.question_order_front ASC'
									);
			$questionList = $this->paginate('Question');
			$groupByStudent =  $this->Student->find('all', array(
										'conditions' => array(
										'Student.examination_category_id' => $questionList[0]['Examination']['examination_category_id'],
										'Student.school_id' => $school_id
										),
            'recursive' => -1,
            //'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            //'order' => array('SchoolInformation.type ASC'),
            'group' => array('Student.teacher_name')
			));
			//pr($groupByStudent);
			$this->set(compact('questionList','no_of_student_appear', 'avereageCorrectAns','pass', 'fail','total_set_question','groupByStudent','cal_non_cal'));	
			
			//pr($questionList);die;
			

	  }
	

	
	
	  public function result_chart_bak($school_purchase_exam_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 
			
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 'StudentAnswer.answer_id !=' => 0
																																			 )));

			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
			
			   if($correctAnswer != 0){
				   $percentage = ($correctAnswer * 100) / $total_set_question;
				   if($percentage >= 30){ // pass marks 30%
					   $pass = $pass + $i;
					   $i++;
				   }
				   
			   }
			  
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
			$avereageCorrectAns = $correct_answer / $no_of_student_appear;
			if($no_of_student_appear > $pass){
				$fail = $no_of_student_appear - $pass;
			} else {
				$fail = 0;
			}
			
			// $totalAttemptQs = count($StudentAnswerAr);	
			 
			$this->set(compact('no_of_student_appear', 'avereageCorrectAns','pass', 'fail','total_set_question'));	
	  }
	
	
	  public function student_list_appeared_in_exam($school_purchase_exam_id = NULL, $cal_non_cal = NULL){
			//$school_take_exam_id = NULL, 
			$this->UserConfiguration->isSchoolLoggedIn();
			//$this->layout = 'school_layout';
			$this->layout = 'result_chart_school_layout';
			
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 
			 if(!empty($cal_non_cal) && $cal_non_cal == 'calculator'){
				 $cal_non_cal = 'calculator';
			 } else if(!empty($cal_non_cal) && $cal_non_cal == 'non-calculator'){
				 $cal_non_cal = 'non-calculator';
			 } else if(!empty($cal_non_cal) && $cal_non_cal == 'combined'){
				 $cal_non_cal = 'combined';
			 } else {
				 $cal_non_cal = '';
			 }
			 
			 $this->set(compact('cal_non_cal'));
			 
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $this->MyExamStudentAnswer->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			
			
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//added below for pass / fail student start
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
					$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 'StudentAnswer.answer_id !=' => 0
																																			 )));

			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
			
			   if($correctAnswer != 0){
				   $percentage = ($correctAnswer * 100) / $total_set_question;
				   if($percentage >= 30){ // pass marks 30%
					   $pass = $pass + $i;
					   $i++;
				   }
				   
			   }
			  
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
			$avereageCorrectAns = $correct_answer / $no_of_student_appear;
			if($no_of_student_appear > $pass){
				$fail = $no_of_student_appear - $pass;
			} else {
				$fail = 0;
			}
			
			//added below for pass / fail student End
			 
			 
			  $conditions[] = array(
												  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
												  'MyExamStudentAnswer.isdeleted' => 0,
												  'MyExamStudentAnswer.exam_type' => 2 //online exam by school
												  );
			 
			
			$studentList = $this->MyExamStudentAnswer->find('all', array('conditions' => $conditions, 'order' => 'Student.id DESC'));
			
			//pr($studentList);
			//Added by Abul Hasan(8.6.2017)
			
			$groupByStudent =  $this->Student->find('all', array(
										'conditions' => array(
											'Student.examination_category_id' => $studentList[0]['SchoolPurchaseExam']['Examination']['ExaminationCategory']['id'],
											'Student.school_id' => $school_id
											),
								'recursive' => -1,
								//'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
								//'order' => array('SchoolInformation.type ASC'),
								'group' => array('Student.teacher_name')
								));
			//pr($groupByStudent);
			
			
			
			$this->set(compact('studentList', 'no_of_student_appear', 'pass', 'fail','groupByStudent'));	
			//pr($conditions);
				//pr($studentList);
	      }
		  
		  // below method delete latter
		   public function student_list_appeared_in_exam_delete_latter($school_purchase_exam_id = NULL){
			//$school_take_exam_id = NULL, 
			$this->UserConfiguration->isSchoolLoggedIn();
			//$this->layout = 'school_layout';
			$this->layout = 'result_chart_school_layout';
			
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $this->set('school_purchase_exam_id', $school_purchase_exam_id);
			 $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
			 
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $this->MyExamStudentAnswer->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			
			
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			//added below for pass / fail student start
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
					$correct_answer = 0;
			$i = 1;
			$pass = 0;
			$StudentAnswerAr = array();
			 foreach($studentAppearAr as $v){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																			 'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'], 
																																			 'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
																																			 'StudentAnswer.answer_id !=' => 0
																																			 )));

			$answer_ids = '';
			foreach($StudentAnswerAr as $val){
				 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			 }
			  $answer_ids = trim($answer_ids,",");
			  $answer_ids_ar = explode(',', $answer_ids);
			  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
			
			   if($correctAnswer != 0){
				   $percentage = ($correctAnswer * 100) / $total_set_question;
				   if($percentage >= 30){ // pass marks 30%
					   $pass = $pass + $i;
					   $i++;
				   }
				   
			   }
			  
			  $correct_answer = $correct_answer + $correctAnswer;
			
			 }
			
			$avereageCorrectAns = $correct_answer / $no_of_student_appear;
			if($no_of_student_appear > $pass){
				$fail = $no_of_student_appear - $pass;
			} else {
				$fail = 0;
			}
			
			//added below for pass / fail student End
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			  $conditions[] = array(
												  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
												  'MyExamStudentAnswer.isdeleted' => 0,
												  'MyExamStudentAnswer.exam_type' => 2 //online exam by school
												  );
			 if($this->request->is('post') || $this->request->is('put')){
				//pr($this->data);
				$search_text = $this->request->data['search_text'];
				$search_by = $this->request->data['search_by'];
				 //$conditions[] = array('MyExamStudentAnswer.school_id' => $school_id, 'MyExamStudentAnswer.isdeleted' => 0);
				 $conditions[] = array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.isdeleted' => 0, 'MyExamStudentAnswer.exam_type' => 2);
					if($search_by == 'name' && !empty(trim($search_text))){
						$conditions[] = array(
															'OR' => array(
																				'Student.first_name LIKE' => '%'.$search_text.'%',
																				'Student.last_name LIKE' =>  '%'.$search_text.'%',
																				'MyExamStudentAnswer.name LIKE' =>  '%'.$search_text.'%',
																				)
															);
					}		

					if($search_by == 'email' && !empty(trim($search_text))){
						$conditions[] = array('Student.email LIKE' => '%'.$search_text.'%',);
					}				
												
				  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 } else {
			
			  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 }
			
			$studentList = $this->paginate('MyExamStudentAnswer');
			
			//pr($studentList);
			//Added by Abul Hasan(8.6.2017)
			
			$groupByStudent =  $this->Student->find('all', array(
										'conditions' => array(
											'Student.examination_category_id' => $studentList[0]['SchoolPurchaseExam']['Examination']['ExaminationCategory']['id'],
											'Student.school_id' => $school_id
											),
								'recursive' => -1,
								//'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
								//'order' => array('SchoolInformation.type ASC'),
								'group' => array('Student.teacher_name')
								));
			//pr($groupByStudent);
			
			
			
			$this->set(compact('studentList', 'no_of_student_appear', 'pass', 'fail','groupByStudent'));	
			//pr($conditions);
				//pr($studentList);
	      }
		  
		  
	public function fetch_number_student($examination_category_id = NULL, $teacher_name = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			
			$totalStudent =  $this->Student->find('count', array(
			'conditions' => array('Student.examination_category_id' => $examination_category_id,'Student.teacher_name'=>$teacher_name)
			));
			 
			 
		
		  
			return $totalStudent;
			
	  }
	  
	  public function fetch_number_student_by_category($examination_category_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			
			$totalStudent =  $this->Student->find('count', array(
			'conditions' => array('Student.examination_category_id' => $examination_category_id)
			));
			 
			 
		
		  
			return $totalStudent;
			
	  }
	  public function fetch_exam_category($examination_category_id = NULL){
		 $this->UserConfiguration->isSchoolLoggedIn();
		 $this->layout = 'ajax'; 
		  $categoryName =  $this->ExaminationCategory->find('first', array(
			'conditions' => array('ExaminationCategory.id' => $examination_category_id),
			'fields' => array('ExaminationCategory.name'),
			));
		return $categoryName;	
	  }
	  public function fetch_total_list_student($examination_category_id = NULL, $teacher_name = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->Student->recursive=2;
		$this->Student->bindModel(
		//if(){
                array(

                    'hasOne' => array(

                        'MyExamStudentAnswer' => array(

                            'className' => 'MyExamStudentAnswer',

                            'foreignKey' => 'student_id',

                           'fields' => array('MyExamStudentAnswer.unique_flag','MyExamStudentAnswer.student_ans_random_id')

                        )
						

                    )

                ),false

        );
			$listStudent =  $this->Student->find('all', array(
			'conditions' => array('Student.examination_category_id' => $examination_category_id,'Student.teacher_name'=>$teacher_name),
			//'fields' => array('Student.id'),
			));
			 
			 
		
		  
			return $listStudent;
			
	  }
	  
	  public function fetch_total_list_student_byexamcat($examination_category_id = NULL){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->Student->recursive=2;
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
		$this->Student->bindModel(
		//if(){
                array(

                    'hasOne' => array(

                        'MyExamStudentAnswer' => array(

                            'className' => 'MyExamStudentAnswer',

                            'foreignKey' => 'student_id',

                           'fields' => array('MyExamStudentAnswer.unique_flag','MyExamStudentAnswer.student_ans_random_id')

                        )
						

                    )

                ),false

        );
			$listStudent =  $this->Student->find('all', array(
			'conditions' => array('Student.examination_category_id' => $examination_category_id,'Student.school_id'=> $school_id),
			//'fields' => array('Student.id'),
			));
			 
			 
		
		  
			return $listStudent;
			
	  }
	  
	  
	  
	//add student by school manager End
	  public function student_list(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->Student->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			 
			 if($this->request->is('post') || $this->request->is('put')){
				//pr($this->data);
				$search_text = $this->request->data['search_text'];
				$search_by = $this->request->data['search_by'];
				 $conditions[] = array('Student.school_id' => $school_id, 'Student.isdeleted' => 0);
					if($search_by == 'name' && !empty(trim($search_text))){
						$conditions[] = array(
															'OR' => array(
																				'Student.first_name LIKE' => '%'.$search_text.'%',
																				'Student.last_name LIKE' =>  '%'.$search_text.'%',
																				'Student.name LIKE' =>  '%'.$search_text.'%',
																				)
															);
					}		

					if($search_by == 'email' && !empty(trim($search_text))){
						$conditions[] = array('Student.email LIKE' => '%'.$search_text.'%',);
					}				
												
				  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 } else {
			
			  $this->paginate = array(
							'conditions' => array('Student.school_id' => $school_id, 'Student.isdeleted' => 0),
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 }
			 
			$studentList = $this->paginate('Student');
			
			
			$this->set(compact('studentList'));	
				//pr($studentList);
	      }
	
	 public function delete_student(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			$this->layout = 'ajax';
			$student_id = $this->request->data['student_id'];
			if(!empty($student_id)){
				$this->Student->id = $student_id;
				$this->Student->saveField('isdeleted', 1);
			}
			
	 }
	 
	 
	  public function delete_take_exam($take_exam_id = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			//$this->layout = 'ajax';
			if(!empty($take_exam_id)){
				$this->SchoolTakeExam->id = $this->data_decrypt($take_exam_id);
				$this->SchoolTakeExam->saveField('isdeleted', 1);
				$this->redirect(array('controller' => 'schools', 'action' => 'take_exam_list'));
			}
			
	 }
	 
	   public function fetch_exam_taken_date($school_id = NULL, $school_purchase_exam_id = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			//$this->autoRender = false;
			$this->layout = 'ajax';
			 $myExamStudentAnsData = $this->MyExamStudentAnswer->find('first', array(
																			 'conditions' => array(
																							 'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																							 'MyExamStudentAnswer.school_id' => $school_id, 
																							 'MyExamStudentAnswer.exam_type' => 2 // online exam by school
																					)));
			
			if(!empty($myExamStudentAnsData)){
				$exam_taken_date = $myExamStudentAnsData['MyExamStudentAnswer']['created'];
			} else {
				$exam_taken_date = '';
			}
			return $exam_taken_date;
	 }
	   public function get_student_appear_in_exam($school_purchase_exam_id = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			//$this->autoRender = false;
			$this->layout = 'ajax';
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array(
																											 'conditions' => array(
																																		 'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																																		 'MyExamStudentAnswer.exam_type' => 2 // online exam by school
																											 )));	

			return $no_of_student_appear;
	 }
	 
	 public function substrands_result_chart($school_purchase_exam_id = NULL, $cal_non_cal = NULL) {

        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'result_chart_school_layout';
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $this->set('school_purchase_exam_id', $school_purchase_exam_id);
        $this->set('cal_non_cal', $cal_non_cal);
		
        $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);
        $schoolPurchaseExamData = $this->SchoolPurchaseExam->find('first', array('conditions' => array('SchoolPurchaseExam.id' => $school_purchase_exam_id)));
        $examination_id = $schoolPurchaseExamData['SchoolPurchaseExam']['examination_id'];

        $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );

        $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array(
            'conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2),
                )
        );

        $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
        $total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
        $correct_answer = 0;
        $i = 1;
        $pass = 0;
        $StudentAnswerAr = array();
        foreach ($studentAppearAr as $v) {
            $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'],
                    'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
                    'StudentAnswer.answer_id !=' => 0
            )));

            $answer_ids = '';
            foreach ($StudentAnswerAr as $val) {
                $answer_ids = $answer_ids . ',' . $val['StudentAnswer']['answer_id'];
            }
            $answer_ids = trim($answer_ids, ",");
            $answer_ids_ar = explode(',', $answer_ids);
            $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));

            if ($correctAnswer != 0) {
                $percentage = ($correctAnswer * 100) / $total_set_question;
                if ($percentage >= 30) { // pass marks 30%
                    $pass = $pass + $i;
                    $i++;
                }
            }

            $correct_answer = $correct_answer + $correctAnswer;
        }

        $avereageCorrectAns = $correct_answer / $no_of_student_appear;

        if ($no_of_student_appear > $pass) {
            $fail = $no_of_student_appear - $pass;
        } else {
            $fail = 0;
        }

        // $totalAttemptQs = count($StudentAnswerAr);	
        //fetch question data

        if(!empty($cal_non_cal) && $cal_non_cal == 'non-calculator'){
				  $conditions[] = array(
										'Question.examination_id' => $examination_id,
										'Question.isdeleted' => 0,
										//'Question.sub_strands_code is NOT NULL', //this line commented on 06-09-2017
										'Question.section' => 'exam_section_2_name' //exam_section_2_name(Non calculator)/ exam_section_1_name(Calculator allowed)

									);
			 } else  if(!empty($cal_non_cal) && $cal_non_cal == 'calculator'){
				 $conditions[] = array(
										'Question.examination_id' => $examination_id,
										'Question.isdeleted' => 0,
										//'Question.sub_strands_code is NOT NULL', //this line commented on 06-09-2017
										'Question.section' => 'exam_section_1_name' //exam_section_2_name(Non calculator)/ exam_section_1_name(Calculator allowed)

									);
				 
			 } else {
					$conditions[] = array(
										'Question.examination_id' => $examination_id,
										'Question.isdeleted' => 0,
										//'Question.sub_strands_code is NOT NULL' //this line commented on 06-09-2017
									);
			 }


	 
		
		

        $this->paginate = array(
            'conditions' => $conditions,
            //'group_by' => 'Question.sub_strands_code',//this line commented on 06-09-2017
            'limit' => $total_set_question,
           // 'order' => 'Question.question_order_front ASC'
        );
        $questionList = $this->paginate('Question');
		$groupByStudent =  $this->Student->find('all', array(
											'conditions' => array(
											'Student.examination_category_id' => $questionList[0]['Examination']['examination_category_id'],
											'Student.school_id' => $school_id
											),
							'recursive' => -1,
							//'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
							//'order' => array('SchoolInformation.type ASC'),
							'group' => array('Student.teacher_name')
							));
        $this->set(compact('questionList', 'no_of_student_appear', 'avereageCorrectAns', 'pass', 'fail', 'total_set_question','groupByStudent'));
//pr($questionList);die;
    } 
   public function get_each_particular_question(){
			
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $school_purchase_exam_id = $this->request->data['school_purchase_exam_id'];
        $question_id = $this->request->data['question_id'];
        $question_no = $this->request->data['question_no'];
       

        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );

        $MyExamStudentAnswerData = $this->MyExamStudentAnswer->find('first', array(
            'fields' => 'id, unique_flag',
            'conditions' => array(
                'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
                'MyExamStudentAnswer.exam_type' => 2,
                'MyExamStudentAnswer.school_id' => $school_id
        )));
        $unique_flag = $MyExamStudentAnswerData['MyExamStudentAnswer']['unique_flag'];
        $this->StudentAnswer->recursive = 2;
        $QuestionAns = $this->StudentAnswer->find('first', array(
            'conditions' => array(
                'StudentAnswer.question_id' => $question_id,
                'StudentAnswer.unique_flag' => $unique_flag
        )));

        $qval = $QuestionAns;
        $html = '<div><b>Q '.$question_no.'.</b> ' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $qval['Question']['title']);
        $html .= '</div></br>';

        $html .= '<div class="big_answer">';
        $k = 'radio';
        foreach ($qval['Question']['Answer'] as $ansval) {

            if ($ansval['answer_type'] == 1) {
                $answer_type = '<input type="radio" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 2) {
               if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        //$checked = 'checked';
						$checked = ' ';
                } else {
                        $checked = ' ';
                }
               
                $answer_type = '<input type="checkbox" ' . $checked . '  disabled="disabled" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 3) {
                if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        //$checked = 'checked';
						$checked = ' ';
                } else {
                        $checked = ' ';
                }
                $answer_type = '<input type="radio"  ' . $checked . ' disabled="disabled"  value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="' . $k . '_ans[]">';
            } else if ($ansval['answer_type'] == 4) {
                $answer_type = '<textarea readonly="readonly" name="ans_' . $qval['Question']['id'] . '"></textarea>';
            }

            if ($ansval['answer_type'] == 4) {
                $html .= $answer_type;
            } else {
                $html .= $answer_type . '&nbsp;&nbsp;&nbsp;' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $ansval['answer_text']) . '&nbsp;&nbsp;&nbsp;';
            }
        }

        $html .= ' <div class="qus_details" style="padding:20px 0;">Correct Answer :';
         foreach($qval['Question']['CorrectAnswer'] as $corrans) { 
                            $html .= $corrans['answer_text'];
                          if(!empty($corrans["ans_description"])){
                             $html .='<div class="qus_details" style="padding:20px 0;"><a href="javascript:void(0);" onclick="answer_details()">View Details</a></div>';
         $html .= '<div class="qus_details" id="details_showhide" style="display:none;padding:20px 0;">Answer Description : '.$corrans["ans_description"].'</div>';
                          }
         }
        $html .= '</div>';
        $html .= '</div>';
        echo $html;
        //pr($QuestionAns);
    }	
	public function get_student_each_particular_question_ans($school_purchase_exam_id, $question_id, $student_id) {

        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );
        // $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
        $MyExamStudentAnswerData = $this->MyExamStudentAnswer->find('first', array(
            'fields' => 'id, unique_flag',
            'conditions' => array(
                'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
                'MyExamStudentAnswer.exam_type' => 2,
                'MyExamStudentAnswer.student_id' => $student_id,
                'MyExamStudentAnswer.school_id' => $school_id
        )));
        $unique_flag = $MyExamStudentAnswerData['MyExamStudentAnswer']['unique_flag'];
        $this->StudentAnswer->recursive = 2;
        $QuestionAns = $this->StudentAnswer->find('first', array(
            'conditions' => array(
                'StudentAnswer.question_id' => $question_id,
                'StudentAnswer.student_id' => $student_id,
                'StudentAnswer.unique_flag' => $unique_flag
        )));
        $qval = $QuestionAns;
        $studentAnswer = 0;
        if (!empty($qval['StudentAnswer']['answer_id'])) {
            $studentAnswer = $qval['StudentAnswer']['answer_id'];
        } else {
            $studentAnswer = 0;
        }
        return $studentAnswer;
    }
	 public function student_questions_result_chart($school_purchase_exam_id = NULL) {
       
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $this->set('school_purchase_exam_id', $school_purchase_exam_id);
        $school_purchase_exam_id = $this->data_decrypt($school_purchase_exam_id);

        $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );
        $this->MyExamStudentAnswer->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';

        $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
        //added below for pass / fail student start
        $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
        $total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
        $correct_answer = 0;
        $i = 1;
        $pass = 0;
        $StudentAnswerAr = array();
        foreach ($studentAppearAr as $v) {
            $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $v['MyExamStudentAnswer']['student_id'],
                    'StudentAnswer.unique_flag' => $v['MyExamStudentAnswer']['unique_flag'],
                    'StudentAnswer.answer_id !=' => 0
            )));

            $answer_ids = '';
            foreach ($StudentAnswerAr as $val) {
                $answer_ids = $answer_ids . ',' . $val['StudentAnswer']['answer_id'];
            }
            $answer_ids = trim($answer_ids, ",");
            $answer_ids_ar = explode(',', $answer_ids);
            $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));

            if ($correctAnswer != 0) {
                $percentage = ($correctAnswer * 100) / $total_set_question;
                if ($percentage >= 30) { // pass marks 30%
                    $pass = $pass + $i;
                    $i++;
                }
            }

            $correct_answer = $correct_answer + $correctAnswer;
        }

        $avereageCorrectAns = $correct_answer / $no_of_student_appear;
        if ($no_of_student_appear > $pass) {
            $fail = $no_of_student_appear - $pass;
        } else {
            $fail = 0;
        }

        //added below for pass / fail student End
        $conditions[] = array(
            'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
            'MyExamStudentAnswer.isdeleted' => 0,
            'MyExamStudentAnswer.exam_type' => 2 //online exam by school
        );
        if ($this->request->is('post') || $this->request->is('put')) {
            //pr($this->data);
            $search_text = $this->request->data['search_text'];
            $search_by = $this->request->data['search_by'];
            //$conditions[] = array('MyExamStudentAnswer.school_id' => $school_id, 'MyExamStudentAnswer.isdeleted' => 0);
            $conditions[] = array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.isdeleted' => 0, 'MyExamStudentAnswer.exam_type' => 2);
            if ($search_by == 'name' && !empty(trim($search_text))) {
                $conditions[] = array(
                    'OR' => array(
                        'Student.first_name LIKE' => '%' . $search_text . '%',
                        'Student.last_name LIKE' => '%' . $search_text . '%',
                        'MyExamStudentAnswer.name LIKE' => '%' . $search_text . '%',
                    )
                );
            }

            if ($search_by == 'email' && !empty(trim($search_text))) {
                $conditions[] = array('Student.email LIKE' => '%' . $search_text . '%',);
            }

            $this->paginate = array(
                'conditions' => $conditions,
                'limit' => 10,
                'order' => 'Student.id DESC'
            );
        } else {

            $this->paginate = array(
                'conditions' => $conditions,
                'limit' => 10,
                'order' => 'Student.id DESC'
            );
        }

        $studentList = $this->paginate('MyExamStudentAnswer');
        $schoolPurchaseExamData = $this->SchoolPurchaseExam->find('first', array('conditions' => array('SchoolPurchaseExam.id' => $school_purchase_exam_id)));
        $examination_id = $schoolPurchaseExamData['SchoolPurchaseExam']['examination_id'];
        $questionList = $this->Question->find('all', array(
																				'limit' => $total_set_question,
																				'order' => 'Question.question_order_front ASC',
																				'conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)));

        $this->set(compact('studentList', 'questionList', 'no_of_student_appear', 'pass', 'fail', 'school_purchase_exam_id'));

    }
     public function login_new() {
        $this->layout = 'school_layout';
        //$this->layout = 'landing_layout';
        $this->_schoolLoginRedirect();
        $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0)));
        $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0)));

        $schoolLevelOptions = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
        // print_r($schoolLevelOptions);

       // $schoolLevelOptions['Combined'] = $schoolLevelOptions['Combined'] . ' (K-12)';
	   
	   //added by dinesh on 02-05-2017
	    $schoolLevel['Primary'] = $schoolLevelOptions['Primary'];
		$schoolLevel['Secondary'] = $schoolLevelOptions['Secondary'];
		$schoolLevel['Combined'] = $schoolLevelOptions['Combined'].' (K-12)';
		//$schoolLevel['Special'] = $schoolLevelOptions['Special'];
		$schoolLevelOptions = $schoolLevel;
	   


        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));

        $this->set(compact('examination_types', 'examination_categories', 'schoolLevelOptions', 'types'));
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {

            //school login start
            if (!empty($this->request->data['SchoolLogin']['login_registration_flag']) && $this->request->data['SchoolLogin']['login_registration_flag'] == 'school_login') {
                $login = strtolower($this->request->data['SchoolLogin']['login']);
                $password = $this->request->data['SchoolLogin']['password'];
                $userdata = $this->School->getLoginData($login, $password);

                if (empty(trim($this->request->data['SchoolLogin']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['SchoolLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($userdata) && !empty(trim($this->request->data['SchoolLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }

                if ($ERROR == 0) {
                    $this->UserConfiguration->setSchoolData($userdata);
					$this->School->id = $userdata['School']['id'];
					//$lastdate = date("Y-m-d h:i:s");
					$lastdate = date("Y-m-d H:i:s", strtotime('+5 hour +30 minutes'));
					$this->School->saveField('last_login', $lastdate);
                    // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                    $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                    //$this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
                    $this->redirect(array('controller' => 'schools', 'action' => 'my_purchased_exam'));
                }
            } else {

                //student login End
                //school registration
                if (empty(trim($this->request->data['School']['name']))) {
                    $ERROR = 1;
                    $this->set('fnerror', 'Please enter school name');
                }

                if (empty(trim($this->request->data['School']['manager_name']))) {
                    $ERROR = 1;
                    $this->set('mNameErr', 'Please enter manager name');
                }

                if (empty(trim($this->request->data['SchoolInformation']['street']))) {
                    $ERROR = 1;
                    $this->set('streetErr', 'Please enter street');
                }

                if (empty(trim($this->request->data['SchoolInformation']['town']))) {
                    $ERROR = 1;
                    $this->set('townErr', 'Please enter town');
                }
                if (empty(trim($this->request->data['SchoolInformation']['zip']))) {
                    $ERROR = 1;
                    $this->set('zipErr', 'Please enter post code');
                }
                if (empty(trim($this->request->data['SchoolInformation']['state']))) {
                    $ERROR = 1;
                    $this->set('stateErr', 'Please enter state');
                }
                if (empty(trim($this->request->data['SchoolInformation']['phone']))) {
                    $ERROR = 1;
                    $this->set('phoneErr', 'Please enter phone');
                }
                if (empty(trim($this->request->data['SchoolInformation']['fax']))) {
                    $ERROR = 1;
                    $this->set('faxErr', 'Please enter fax');
                }

                //if(empty(trim($this->request->data['SchoolInformation']['url']))){
                //	$ERROR = 1;
                //	$this->set('urlErr', 'Please enter school website');
                //}




                if (empty(trim($this->request->data['School']['email']))) {
                    $ERROR = 1;
                    $this->set('emerror', 'Please enter school email');
                } else {
                    $email = $this->request->data['School']['email'];
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $ERROR = 1;
                        $this->set('emerror', 'Please enter valid email format');
                    }
                }

                if (empty(trim($this->request->data['School']['manager_email']))) {
                    $ERROR = 1;
                    $this->set('manager_emailErr', 'Please enter manager  email');
                } else {
                    $memail = $this->request->data['School']['manager_email'];
                    if (!filter_var($memail, FILTER_VALIDATE_EMAIL)) {
                        $ERROR = 1;
                        $this->set('manager_emailErr', 'Please enter valid  email format');
                    }
                }



                if (!empty(trim($this->request->data['School']['email']))) {
                    $email = $this->request->data['School']['email'];
                    $ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
                    if ($ExistEmail > 0) {
                        $ERROR = 1;
                        $this->set('emerror', 'This email already exist, please try another');
                    }
                }



                if (empty(trim($this->request->data['School']['captcha_txt']))) {
                    $ERROR = 1;
                    $this->set('captchaErr', 'Please enter captcha code');
                } else {
                    if (strcasecmp($this->data['School']['captcha_txt'], $this->Session->read('captcha')) != 0) {
                        $ERROR = 1;
                        $this->set('captchaErr', 'Please enter correct captcha code ');
                    }
                }




                if ($ERROR == 0) {
                    $this->request->data['School']['username'] = $this->request->data['School']['email'];
                    $this->request->data['School']['password'] = $this->_randomPassword(8);
                    $this->School->save($this->request->data['School']);
                    $this->request->data['SchoolInformation']['school_id'] = $this->School->getLastInsertID();
                    $this->SchoolInformation->save($this->request->data['SchoolInformation']);

                    $schoolEmail = $this->request->data['School']['email'];
                    $full_name = $this->request->data['School']['manager_name'];

                    ////email send start//////
                    if (!empty($schoolEmail)) {

                        $host = 'https://' . $_SERVER['HTTP_HOST'] . $this->webroot . 'naplan_exam/';
                        $naplan_exam = '<a href="' . $host . '">Naplan exam</a>';
                        $mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 8)));
                        $html_content = $mailTemplateData['EmailTemplate']['content'];
                        $validity = 100; //days will come from admin
                        //$html_content = $this->_html_for_reset_password();

                        $html = '';
                        $html .= str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
                        $html = str_replace('{{full_name}}', $full_name, $html);
                        $html = str_replace('{{validity}}', $validity, $html);

                        //  pr($html);die;
                        $email_subject = 'Welcome to Naplan Exam';
                        $email = $schoolEmail;
                        $mailFrom = 'noReply@naplanExam.com';
                        $FromName = 'Naplan Exam';

                        $mail = new PHPMailer;
                        $mail->FromName = $FromName;
                        $mail->From = $mailFrom;
                        $mail->Subject = $email_subject;
                        $mail->Body = stripslashes($html);
                        $mail->AltBody = stripslashes($html);
                        $mail->IsHTML(true);
                        $mail->AddAddress($email);
                        $mail->Send();


                        $email_subject1 = 'School Registration Notification';
                        $mailFrom1 = $schoolEmail;
                        $FromName1 = $full_name;
                        $mailTemplateData1 = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 9)));
                        $html_content1 = $mailTemplateData1['EmailTemplate']['content'];

                        $html1 = '';
                        $html1 .= str_replace('{{naplan_exam}}', $naplan_exam, $html_content1);
                        $html1 = str_replace('{{full_name}}', $full_name, $html1);
                        //$html1 = str_replace('{{validity}}', $validity, $html1);
                        $admin_email_to = 'dinesh.brainium@gmail.com';
                        $AdminEmail = new PHPMailer;
                        $AdminEmail->FromName = $FromName1;
                        $AdminEmail->From = $mailFrom1;
                        $AdminEmail->Subject = $email_subject1;
                        $AdminEmail->Body = stripslashes($html1);
                        $AdminEmail->AltBody = stripslashes($html1);
                        $AdminEmail->IsHTML(true);
                        $AdminEmail->AddAddress($admin_email_to);
                        $AdminEmail->Send();
                    }
                    ////email send End//////








                    $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
                    $this->redirect(array('controller' => 'Schools', 'action' => 'registration'));
                }
            }
        }
    }
	
	
	//view exam
	public function view_exam($exam_id = NULL, $cal_non_cal = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			//$this->layout = 'school_layout';
			$this->layout = 'take_exam_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			$school_id = $schoolData['School']['id'];
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			$exam_id = $this->data_decrypt($exam_id);
			
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions/cal
			 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions/non-cal
			
			$QuestionLimitCal =  $no_of_qs_20['Setting']['value'];
			$QuestionLimitNonCal =  $no_of_qs_23['Setting']['value'];
			$QuestionLimitNormal = 32;
			if($cal_non_cal == 'calculator'){
				$section = 'Calculator Section';
				$QuestionAns = $this->Question->find('all', array(
													'order' => 'Question.question_order_front ASC',
													'limit' => $QuestionLimitCal,
													'conditions' => array(
																'Question.examination_id' => $exam_id,
																'Question.section' => 'exam_section_1_name',
																'Question.isdeleted' => 0,
													)));
			} else if($cal_non_cal == 'non-calculator'){
				$section = 'Non-Calculator Section';
				$QuestionAns = $this->Question->find('all', array(
													'order' => 'Question.question_order_front ASC',
													'limit' => $QuestionLimitNonCal,
													'conditions' => array(
																'Question.examination_id' => $exam_id,
																'Question.section' => 'exam_section_2_name',
																'Question.isdeleted' => 0,
													)));
			} else {
				$section = '';
				$QuestionAns = $this->Question->find('all', array(
													'order' => 'Question.question_order_front ASC',
													'limit' => $QuestionLimitNormal,
													'conditions' => array(
																'Question.examination_id' => $exam_id,
																'Question.isdeleted' => 0,
													)));
			}
			
													
													
			$this->set(compact('QuestionAns', 'totalqustionSet', 'section'));
			//pr($QuestionAns);die;

	}
	
	// added by dinesh on 08-06-2017
	
	 public function click_view_to_see__bak(){
			
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $school_purchase_exam_id = $this->request->data['school_purchase_exam_id'];
        $question_id = $this->request->data['question_id'];
		$no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
		//echo $school_purchase_exam_id;die;
		//echo $question_id;die;

        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );

        $MyExamStudentAnswerData = $this->MyExamStudentAnswer->find('first', array(
            'fields' => 'id, unique_flag',
            'conditions' => array(
                'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
                'MyExamStudentAnswer.exam_type' => 2,
                'MyExamStudentAnswer.school_id' => $school_id
        )));
        $unique_flag = $MyExamStudentAnswerData['MyExamStudentAnswer']['unique_flag'];
        $this->StudentAnswer->recursive = 2;
        $QuestionAns = $this->StudentAnswer->find('first', array(
            'conditions' => array(
                'StudentAnswer.question_id' => $question_id,
                'StudentAnswer.unique_flag' => $unique_flag
        )));

        $qval = $QuestionAns;
		//pr($qval);die;
		//echo $qval['StudentAnswer']['answer_id'];die;
        $html = '<div><b>Q.</b> ' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $qval['Question']['title']);
        $html .= '</div></br>';

        $html .= '<div class="rect_box_ans">';
        $k = 'radio';
        foreach ($qval['Question']['Answer'] as $ansval) {

            if ($ansval['answer_type'] == 1) {
                $answer_type = '<input type="radio" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 2) {
               if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        //$checked = 'checked';
						 $checked = ' ';
                } else {
                        $checked = ' ';
                }
               
                $answer_type = '<input type="checkbox" ' . $checked . '  disabled="disabled" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 3) {
                if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        //$checked = 'checked';
						 $checked = ' ';
                } else {
                        $checked = ' ';
                }
                $answer_type = '<input type="radio"  ' . $checked . ' disabled="disabled"  value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="' . $k . '_ans[]">';
            } else if ($ansval['answer_type'] == 4) {
                $answer_type = '<textarea readonly="readonly" name="ans_' . $qval['Question']['id'] . '"></textarea>';
            }

            if ($ansval['answer_type'] == 4) {
                $html .= $answer_type;
            } else {
                $html .= '<span class="rect_box_op">'.$answer_type . '&nbsp;&nbsp;&nbsp;' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $ansval['answer_text']) . '&nbsp;&nbsp;&nbsp;'.'</span>';
            }
        }
		$html .= '<div class="rect_box_full">';
		foreach ($qval['Question']['Answer'] as $ansval) {
			$noOfStdnts = $this->fetch_no_of_student_for_each_option($school_purchase_exam_id, $question_id, $ansval['id']);
			$noOfStdnts = 2;
			$percentageStudent = (($noOfStdnts * 100) / $no_of_student_appear);
			$html .= '<div class="rect_box">
					<div class="sq_text">Number of students : '.$noOfStdnts.'</div>
					<div class="sq_text">Percentage of Students :'.$percentageStudent.' </div>
					<div class="sq_text"><a href="">Click to view student list</a></div>
					</div>';
		
		}	
		$html .= '</div>';

        /*$html .= ' <div class="qus_details" style="padding:20px 0;">Correct Answer :';
         foreach($qval['Question']['CorrectAnswer'] as $corrans) { 
                            $html .= $corrans['answer_text'];
                          if(!empty($corrans["ans_description"])){
                             $html .='<div class="qus_details" style="padding:20px 0;"><a href="javascript:void(0);" onclick="answer_details()">View Details</a></div>';
         $html .= '<div class="qus_details" id="details_showhide" style="display:none;padding:20px 0;">Answer Description : '.$corrans["ans_description"].'</div>';
                          }
         }*/
        $html .= '</div>';
        $html .= '</div>';
        echo $html;
        //pr($QuestionAns);
    }
	
	
	
	
	 public function click_view_to_see(){
			
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
        $school_purchase_exam_id = $this->request->data['school_purchase_exam_id'];
        $question_id = $this->request->data['question_id'];
        $question_no = $this->request->data['question_no'];
		$no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
		//echo $school_purchase_exam_id;die;
		//echo $question_id;die;

        $this->MyExamStudentAnswer->recursive = 3;
        $this->Examination->unbindModel(
                array('hasMany' => array('Question'))
        );

        $MyExamStudentAnswerData = $this->MyExamStudentAnswer->find('first', array(
            'fields' => 'id, unique_flag',
            'conditions' => array(
                'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
                'MyExamStudentAnswer.exam_type' => 2,
                'MyExamStudentAnswer.school_id' => $school_id
        )));
        $unique_flag = $MyExamStudentAnswerData['MyExamStudentAnswer']['unique_flag'];
        $this->StudentAnswer->recursive = 2;
        $QuestionAns = $this->StudentAnswer->find('first', array(
            'conditions' => array(
                'StudentAnswer.question_id' => $question_id,
                'StudentAnswer.unique_flag' => $unique_flag
        )));
		
		$questionsArr = $this->Question->find('first', array(
            'conditions' => array(
                'Question.id' => $question_id
        )));
		//pr($questionsArr);die;
        $qval = $QuestionAns;
        $qval = $questionsArr;
		
		//echo $qval['StudentAnswer']['answer_id'];die;
        $html = '<div><b>Q '.$question_no.'.</b> ' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $qval['Question']['title']);
        $html .= '</div></br>';

        $html .= '<div class="rect_box_ans">';
        $k = 'radio';
        //foreach ($qval['Question']['Answer'] as $ansval) {
		$last_key = end(array_keys($qval['Answer']));
		//foreach ($qval['Answer'] as $ansval) { 
		foreach ($qval['Answer'] as $key => $ansval) { 
		
		$answerText = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1',$ansval['answer_text']);
		$answerText = strlen($answerText);
		 if($answerText >= 3 && $answerText <=6){
			$rect_box_op = 'rect_box_op_3_6'; //15%
		} else if($answerText >= 1 && $answerText <=2 && count($qval['Answer']) != 5){
			$rect_box_op = 'rect_box_op_1_2'; //16% or 18
		} else if($answerText >= 1 && $answerText <=2 && count($qval['Answer']) == 5){
			$rect_box_op = 'rect_box_op_1_2_5op'; //14%
		}
		else if($answerText >= 7 && $answerText <=10){
			$rect_box_op = 'rect_box_op_7_10'; //10%
		} 
		//
		else if($answerText == 22){
			$rect_box_op = 'rect_box_op_22'; //14%
		} 
		else if($answerText == 14){
			$rect_box_op = 'rect_box_op_14'; //11%
		} 
		else if($answerText == 39){
			$rect_box_op = 'rect_box_op_39'; //9%
		} 
		
		else if($answerText >= 15 && $answerText <=19){
			$rect_box_op = 'rect_box_op_15_19'; //4%
		} else if($answerText >= 11 && $answerText <=14){
			$rect_box_op = 'rect_box_op_11_14'; //15% or 7%
		} else if($answerText >= 22 && $answerText <=25){
			$rect_box_op = 'rect_box_op_22_25'; //12%
		}  else if($answerText == 41){
			$rect_box_op = 'rect_box_op_31_42'; //9% for q16
		} else if($answerText == 32){
			$rect_box_op = 'rect_box_op_31_42'; //9% for q16
		} else if($answerText == 33){
			$rect_box_op = 'rect_box_op_31_42'; //9% for q16
		}
		else if($answerText >= 30 && $answerText <=62){
			$rect_box_op = 'rect_box_op_20_27'; //2%
		} 
		
		else {
			$rect_box_op = 'rect_box_op'.' '.$answerText;
		}
		
		//$rect_box_op = $rect_box_op.'  @=>'.$answerText;
		
		if (stripos(strtolower($ansval['answer_text']), 'src="') !== false) {
			//$rect_box_op = 'rect_box_op_img';
			$str = $ansval['answer_text'];
			preg_match('/(src=["\'](.*?)["\'])/', $str, $match);  //find src="X" or src='X'
			$split = preg_split('/["\']/', $match[0]); // split by quotes
			$src = $split[1]; // X between quotes
			list($width, $height, $type, $attr) = getimagesize($src);
			 if($width >= 15 && $width <=32){
					$rect_box_op = 'rect_box_op_img_15_32'; //15%
				} else if($width >= 124 && $width <=138 ){
					$rect_box_op = 'rect_box_op_img_125_138'; //6% 
				} else if($width >= 230 && $width <=262 ){
					$rect_box_op = 'rect_box_op_img_150_w'; //img 150px 
				} else if($width >= 60 && $width <=72 ){
					$rect_box_op = 'rect_box_op_img_60_72'; //10% 
				}
				else {
					$rect_box_op = '';
				}
			
		} else {
		
		 if ($key == $last_key) { // last option's class blank for non-image content
				$rect_box_op = '';
		} else {
			  $rect_box_op = $rect_box_op;
		}
		
	}
		
		
		
		
		if($ansval['iscorrect'] == 1){
			$fontcolor = 'color:green;font-weight:800;';
		} else {
			$fontcolor = 'color:red;';
		}
		    $checked = ' ';
            if ($ansval['answer_type'] == 1) {
                $answer_type = '<input type="radio" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 2) {
              /* if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        $checked = 'checked';
						 
                } else {
                        $checked = ' ';
                }*/
               
                $answer_type = '<input type="checkbox" ' . $checked . '  disabled="disabled" value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="ans[]">';
            } else if ($ansval['answer_type'] == 3) {
               
			   /*if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
                        $checked = 'checked';
						 
                } else {
                        $checked = ' ';
                }*/
				
                $answer_type = '<input type="radio"  ' . $checked . ' disabled="disabled"  value="' . $qval['Question']['id'] . 'Q#' . $ansval['id'] . '" name="' . $k . '_ans[]">';
            } else if ($ansval['answer_type'] == 4) {
                //$answer_type = '<textarea readonly="readonly" name="ans_' . $qval['Question']['id'] . '"></textarea>';
                $answer_type = '<span class="'.$rect_box_op.'">' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', '<span style="'.$fontcolor.'">'.$ansval['answer_text'].'</span>') . '&nbsp;&nbsp;&nbsp;'.'</span>';
            }

            if ($ansval['answer_type'] == 4) {
                $html .= $answer_type;
            } else {
                $html .= '<span class="'.$rect_box_op.'">'.$answer_type . '&nbsp;&nbsp;&nbsp;' . preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', '<span style="'.$fontcolor.'">'.$ansval['answer_text'].'</span>') . '&nbsp;&nbsp;&nbsp;'.'</span>';
            }
        }
		$html .= '<div class="rect_box_full">';
		foreach ($qval['Answer'] as $ansval_1) {
			$answer_id = $ansval_1['id'];
			
			if($ansval_1['answer_type'] == 4) {
			  $noOfStdnts = $this->fetch_no_of_student_for_each_option($school_purchase_exam_id, $question_id, $answer_id,'textAreaAns');
			} else {
			  $noOfStdnts = $this->fetch_no_of_student_for_each_option($school_purchase_exam_id, $question_id, $answer_id);
			}
			$percentageStudent = (($noOfStdnts * 100) / $no_of_student_appear);
			if($noOfStdnts > 0){
				$textarea_ans = 'textarea_ans';
				if($ansval_1['answer_type'] == 4) {
				     $student_list = '<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".studentList4eachOption" onclick="get_student_list_for_each_option_new('.$school_purchase_exam_id.','.$question_id.','.$answer_id.','.$textarea_ans.')" class="student_list_ans" data-student-qid="'.$question_id.'" data-student="'.$school_purchase_exam_id.'">Click to view student list</a>';
					} else {
				     $student_list = '<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".studentList4eachOption" onclick="get_student_list_for_each_option('.$school_purchase_exam_id.','.$question_id.','.$answer_id.')" class="student_list_ans" data-student-qid="'.$question_id.'" data-student="'.$school_purchase_exam_id.'">Click to view student list</a>';
					}
				
				
			} else {
				$student_list = '<a href="javascript:void(0);" style="margin-bottom: 10px;">Click to view student list</a>';
			}
			$html .= '<div class="rect_box">
					<div class="sq_text">Number of students : '.$noOfStdnts.'</div>
					<div class="sq_text">Percentage of Students :'.round($percentageStudent, 2).' </div>
					<div class="sq_text">'.$student_list.'
					</div>
					</div>';
		
		}	
		$html .= '</div>';

        /*$html .= ' <div class="qus_details" style="padding:20px 0;">Correct Answer :';
         foreach($qval['Question']['CorrectAnswer'] as $corrans) { 
                            $html .= $corrans['answer_text'];
                          if(!empty($corrans["ans_description"])){
                             $html .='<div class="qus_details" style="padding:20px 0;"><a href="javascript:void(0);" onclick="answer_details()">View Details</a></div>';
         $html .= '<div class="qus_details" id="details_showhide" style="display:none;padding:20px 0;">Answer Description : '.$corrans["ans_description"].'</div>';
                          }
         }*/
        $html .= '</div>';
        $html .= '</div>';
        echo $html;
        //pr($QuestionAns);
    }
	
	
	
	
	
	
	 public function fetch_no_of_student_for_each_option($school_purchase_exam_id, $question_id, $answer_id, $textAreaAns){
			
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
		
		//echo $school_purchase_exam_id.'<br>';
		//echo $question_id.'<br>';
		//echo $answer_id.'<br>';die;
		if(!empty($textAreaAns)&& $answer_id != 0){
			$answerData = $this->Answer->find('first', array(
															'fields' => 'id,answer_text',	
															'conditions' => array(
															'Answer.id' => $answer_id,
																	)
															));
			$studentAnsData = $this->StudentAnswer->find('all', array(
															'fields' => 'id,answer_text',
															'conditions' => array(
																'StudentAnswer.school_purchase_exam_id' => $school_purchase_exam_id,
																'StudentAnswer.question_id' => $question_id,
																//'StudentAnswer.answer_id' => $answer_id,
																'StudentAnswer.unique_flag !=' => ''
															)));
				if(!empty($studentAnsData)){
					$noOfStudent = 0;
					foreach($studentAnsData as $stansV){
						if(strip_tags($stansV['StudentAnswer']['answer_text']) == strip_tags($answerData['Answer']['answer_text'])){
							$noOfStudent = ($noOfStudent + 1);
						}
						
					}
				}											
															
		
		    return $noOfStudent;
		} else {
		if($answer_id != 0){
		$noOfStudent = $this->StudentAnswer->find('count', array(
															'conditions' => array(
															'StudentAnswer.school_purchase_exam_id' => $school_purchase_exam_id,
															'StudentAnswer.question_id' => $question_id,
															'StudentAnswer.answer_id' => $answer_id,
															'StudentAnswer.unique_flag !=' => ''
															)));
															
		
		    return $noOfStudent;
		} else {
			
			$noOfStudent = 0;
			return $noOfStudent;
		}
	 }
	}
	
	public function fetch_student_list_for_each_option($school_purchase_exam_id, $question_id, $answer_id){
			
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'ajax';
        $this->autoRender = false;
        $schoolData = $this->UserConfiguration->getSchoolData();
        $school_id = $schoolData['School']['id'];
		
		//echo $school_purchase_exam_id.'<br>';
		//echo $question_id.'<br>';
		//echo $answer_id.'<br>';die;
		
		if($answer_id != 0){
		$studentList = $this->StudentAnswer->find('all', array(
															//'group' => 'StudentAnswer.student_id',
															'conditions' => array(
															'StudentAnswer.school_purchase_exam_id' => $school_purchase_exam_id,
															'StudentAnswer.question_id' => $question_id,
															'StudentAnswer.answer_id' => $answer_id,
															'StudentAnswer.unique_flag !=' => ''
															)));
															
		
		    return $studentList;
		} else {
			
			$noOfStudent = 0;
			return $studentList;
		}
	 }
	
	
	
	public function get_student_list_for_each_option_new(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			//$school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			$school_purchase_exam_id =$this->request->data['school_purchase_exam_id'];
			$question_id = $this->request->data['question_id'];
			$answer_id = $this->request->data['answer_id'];
			$textarea_ans = $this->request->data['textarea_ans'];
			
			if(!empty($textarea_ans) && $answer_id != 0){
			$answerData = $this->Answer->find('first', array(
															'fields' => 'id,answer_text',	
															'conditions' => array(
															'Answer.id' => $answer_id,
																	)
															));
			$StudentAnswerAr = $this->StudentAnswer->find('all', array(
															'fields' => 'id,student_id,answer_text',
															'conditions' => array(
																'StudentAnswer.school_purchase_exam_id' => $school_purchase_exam_id,
																'StudentAnswer.question_id' => $question_id,
																//'StudentAnswer.answer_id' => $answer_id,
																'StudentAnswer.unique_flag !=' => ''
															)));
				if(!empty($StudentAnswerAr)){
					foreach($StudentAnswerAr as $val){
						if(strip_tags($val['StudentAnswer']['answer_text']) == strip_tags($answerData['Answer']['answer_text'])){
				       @$student_ids = @$student_ids.','.$val['StudentAnswer']['student_id'];
						}
					}
				
				}
	       } 
			
		
			  $student_ids = trim($student_ids,",");
			  $student_ids_arr = explode(',', $student_ids);
			   $studentData = $this->Student->find('all', array(
																										 'conditions' => array(
																																		 'Student.id' => $student_ids_arr, 
																																		 'Student.isdeleted' => 0 
																													 )));
				
			
				  if(!empty($studentData)){
					$html = '<table><tr><th>List</th><th>Student Name</th>';
					$i = 1;
					 foreach($studentData as $val){
							 $student_id = $val['Student']['id'];
							 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td></tr>';
							$i++;
					 }
					  $html .= '</table>';
					  echo $html;
					} 
					//echo '<pre>';print_r($StudentAnswerAr);
		
	  }
	
	
	
	public function get_student_list_for_each_option(){
			
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			//$school_purchase_exam_id = $this->data_decrypt($this->request->data['school_purchase_exam_id']);
			$school_purchase_exam_id =$this->request->data['school_purchase_exam_id'];
			$question_id = $this->request->data['question_id'];
			$answer_id = $this->request->data['answer_id'];
			
			
			 $StudentAnswerAr = $this->StudentAnswer->find('all', array(
																		 //'order' => 'StudentAnswer.question_order ASC',
																		  'recursive' => -1,
																		'conditions' => array(
																					 'StudentAnswer.question_id' => $question_id, 
																					 'StudentAnswer.answer_id' => $answer_id, 
																					 'StudentAnswer.school_purchase_exam_id' => $school_purchase_exam_id,
																					 'StudentAnswer.unique_flag !=' => '',
																					 )));



			foreach($StudentAnswerAr as $val){
				//$student_ids = '';
				 @$student_ids = @$student_ids.','.$val['StudentAnswer']['student_id'];
			}
			
	    
			
		
			  $student_ids = trim($student_ids,",");
			  $student_ids_arr = explode(',', $student_ids);
			   $studentData = $this->Student->find('all', array(
																										 'conditions' => array(
																																		 'Student.id' => $student_ids_arr, 
																																		 'Student.isdeleted' => 0 
																													 )));
				
			
				  if(!empty($studentData)){
					$html = '<table><tr><th>List</th><th>Student Name</th>';
					$i = 1;
					 foreach($studentData as $val){
							 $student_id = $val['Student']['id'];
							 $html .= '<tr><td>'.$i.'</td><td>'.$val["Student"]["first_name"].' '.$val["Student"]["last_name"].'</td></tr>';
							$i++;
					 }
					  $html .= '</table>';
					  echo $html;
					} 
		
	  }
	
	//abul start from here
	public function update_schooldata(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			//pr($this->data);
			//$id = $this->data['id'];
			$id = $this->data['id'];
			//$this->layout = 'ajax';
			if(!empty($id)){
				$id = $this->data_decrypt($id);
			    $schoolData = $this->student_list_appeared_in_exam_ajaxdata($id);
				if($schoolData){
					$updateClassRank = $this->student_update_class_ajaxdata($id);
					if($updateClassRank){
						return true;	
					}else{
					return false;
					}
				}else{
					return false;
				}
				
			}
			
	 }
	 public function student_list_appeared_in_exam_ajaxdata($school_purchase_exam_id = NULL){
			//$school_take_exam_id = NULL, 
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			//$this->layout = 'school_layout';
			$this->layout = 'ajax';
			
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 1;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
		   
			 $this->MyExamStudentAnswer->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			
			
			 $no_of_student_appear = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2,'MyExamStudentAnswer.isdeleted' => 0)));	
			//added below for pass / fail student start
			 $studentAppearAr = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));	
			$total_set_question = $studentAppearAr[0]['MyExamStudentAnswer']['total_set_question'];
			//added below for pass / fail student End
			  $conditions[] = array(
												  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
												  'MyExamStudentAnswer.isdeleted' => 0,
												  'MyExamStudentAnswer.exam_type' => 2 //online exam by school
												  );
			 
			
			  $this->paginate = array(
							'conditions' =>$conditions,
							'order' => 'Student.id DESC'
						);
			 
			$studentList = $this->MyExamStudentAnswer->find('all', array('conditions' => $conditions,'order' => 'Student.id DESC'));
			//pr($studentList); exit;
			foreach($studentList as $key=>$studentdata){
			//pr($studentdata);
			$student_ans_random_id = $studentdata['MyExamStudentAnswer']['unique_flag'];
			$schoolData = $this->fetch_student_total_correct_answer($studentdata['MyExamStudentAnswer']['unique_flag'], $studentdata['Student']['id'],NULL);	
			$studentList[$key]['MyExamStudentAnswer']['correct_ans']=$schoolData ;
			}
			//$maindata = array();
			foreach ($studentList as $key => $row)
			{
				$studentList1[$key]['MyExamStudentAnswer']['correct_ans'] = $row['MyExamStudentAnswer']['correct_ans'];
			}
			array_multisort($studentList1, SORT_DESC, $studentList);
			if($studentList){
			//pr($studentList);
			$k =1;
			foreach($studentList as $dataList){
			$this->MyExamStudentAnswer->updateAll(array('MyExamStudentAnswer.year_group_rank'=>$k,'MyExamStudentAnswer.score'=>$dataList['MyExamStudentAnswer']['correct_ans']), array('MyExamStudentAnswer.school_id'=>$school_id,'MyExamStudentAnswer.id'=>$dataList['MyExamStudentAnswer']['id'],));	
			$k++;	
			}
			}
			return true;
	      }
		  
	public function fetch_student_total_correct_answer($random_id = NULL, $student_id = NULL, $cal_non_cal = NULL) {
       $this->layout = 'ajax';
	         if(!empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0,
                    'StudentAnswer.calculator_non_calculator' => $cal_non_cal
            )));
			} else {
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0
            )));
			}
           
        

        $answer_ids = '';
        foreach ($StudentAnswerAr as $val) {
            //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
            $answer_ids = $answer_ids . ',' . $val['StudentAnswer']['answer_id'];
        }
        $answer_ids = trim($answer_ids, ",");
        $answer_ids_ar = explode(',', $answer_ids);
        // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $data = array();
        $data['totalCorrectAnswer']   = $correctAnswer;
        $data['totalAttempQuestions'] = count($StudentAnswerAr);
        return $correctAnswer;
    } 
	public function student_update_class_ajaxdata($school_purchase_exam_id = NULL){
			//$school_take_exam_id = NULL, 
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			//$this->layout = 'school_layout';
			$this->layout = 'ajax';
			
			$settings = $this->_getSettingsData();
			 $schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			$groupByStudent =  $this->MyExamStudentAnswer->find('all', array(
			'conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,'MyExamStudentAnswer.exam_type' => 2,'MyExamStudentAnswer.isdeleted' => 0,'MyExamStudentAnswer.school_id'=>$school_id),
            'fields' => array('MyExamStudentAnswer.class_name'),
            'group' => array('MyExamStudentAnswer.class_name')
			));
			//print_r($groupByStudent); exit;
			if($groupByStudent){
			foreach($groupByStudent as $studentData){
				$listStudent = $this->MyExamStudentAnswer->find('all', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,'MyExamStudentAnswer.class_name' => $studentData['MyExamStudentAnswer']['class_name'], 'MyExamStudentAnswer.exam_type' => 2,'MyExamStudentAnswer.unique_flag !='=>' '),'order' => 'MyExamStudentAnswer.year_group_rank ASC'));
				
				//echo'<pre>';
				//print_r($listStudent); exit;
				$i=1;
				if($listStudent){
				foreach($listStudent as $data){
				$this->MyExamStudentAnswer->updateAll(array('MyExamStudentAnswer.class_rank'=>$i), array('MyExamStudentAnswer.id'=>$data['MyExamStudentAnswer']['id'],'MyExamStudentAnswer.school_id'=>$school_id,'MyExamStudentAnswer.unique_flag !='=>' ','MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,'MyExamStudentAnswer.exam_type' => 2,));	
				$i++;	
					
				}
			}
			} 
			}
			return true;
			
	      }
	
	public function get_sub_strands_description(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'ajax';
			$this->autoRender = false;
			$question_id = $this->request->data['question_id'];
			$questionData = $this->Question->find('first', array('conditions' => array('Question.id' => $question_id)));
			$sub_strands_description = $questionData['Question']['sub_strands_description'];
			return $sub_strands_description;
			
	 }
	
	
	public function registration_approve_check($email = NULL){
			
			$this->layout = 'ajax';
			//$this->autoRender = false;
			$email = $this->request->data['email'];
			$schoolData =  $this->School->find('first', array(
								'conditions' => array(
													'School.email' => $email,
													'School.isapproved'=> 0, 
													'School.isdeleted'=> 0
													)));
		  
			if(!empty($schoolData)){
				echo 'OK';
			} else {
				echo 'NOT-OK';
			}
			exit;
			
	  }

	  public function settings(){
			 $this->UserConfiguration->isSchoolLoggedIn();
			 $this->layout = 'school_layout';
			 $schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 
			 if($this->request->is('post') || $this->request->is('put')){
				 $this->School->id = $school_id;
				 $this->School->save($this->data);
				 $this->Session->setFlash('Data has been saved successfully.', 'success');
			 } else {
				 $this->data = $this->School->read(NULL, $school_id);
			 }
	  }
	
	
}
?>