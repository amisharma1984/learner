<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class StudentParentsController extends AppController {

    public $name = 'StudentParents';
    public $uses = array('StudentParent','Student','Examination', 'MyExam', 'ExaminationCategory');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration($no_of_child = NULL) {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		  if(!empty($no_of_child)){
			  if($no_of_child == 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Child';
			  } else if($no_of_child > 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Children';
			  } else {
				   $no_of_child = '';
				   $childTag = '';
			  }
		  } else {
			   $no_of_child = '';
			   $childTag = '';
		  }
		  
		   $this->set(compact('examination_types', 'examination_categories','no_of_child','childTag'));
		  
		  
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 //pr($this->request->data);die;
			 //Parent registration
			 	if(empty(trim($this->request->data['StudentParent']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							
							
							
							if(empty(trim($this->request->data['StudentParent']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['StudentParent']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['StudentParent']['email']))){
								$email = $this->request->data['StudentParent']['email'];
								$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['StudentParent']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['StudentParent']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['StudentParent']['password'];
							$cpass = $this->request->data['StudentParent']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['StudentParent']['password'] != $this->request->data['StudentParent']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['StudentParent']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['StudentParent']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['StudentParent']['username'] = $this->request->data['StudentParent']['email'];
					$this->request->data['StudentParent']['password'] = md5($this->request->data['StudentParent']['password']);
					$this->StudentParent->save($this->request->data);
					$lastInserId = $this->StudentParent->getLastInsertID();
					for($i =0; $i<$no_of_child; $i++){
						$studentData['Student']['student_parent_id'] = $lastInserId;
						$studentData['Student']['examination_category_id'] = $this->request->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->request->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->request->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->request->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
					}
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					 $this->redirect(array('controller' => 'parent', 'action' => 'registration',$no_of_child));
				}
				
			 }
		 }
	 

	
	
    public function login() {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
				//print_r($this->request->data);
            //student login start
                $login = strtolower($this->request->data['StudentParent']['login']);
                $password = $this->request->data['StudentParent']['password'];
                $parentData = $this->StudentParent->getParentLoginData($login, $password);
				//print_r($parentData);die;
                if (empty(trim($this->request->data['StudentParent']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($parentData) && !empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }

                if ($ERROR == 0) {
                    $this->UserConfiguration->setParentData($parentData);
                    
					$this->StudentParent->id = $parentData['StudentParent']['id'];
					$lastdate = date("Y-m-d h:i:s");
					$this->StudentParent->saveField('last_login', $lastdate);
					
                    $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                    $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
                    //$this->redirect(array('controller' => 'StudentParent', 'action' => 'take_exam_list'));
                }
            } 
        }
    
	
	 public function family_profiles() {
				$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
				$no_of_child = count($parentData['Student']);
				 if(!empty($no_of_child) && $no_of_child != 0){
					  if($no_of_child == 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Child';
					  } else if($no_of_child > 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Children';
					  } else {
						   $no_of_child = '';
						   $childTag = '';
					  }
				  } else {
					   $no_of_child = '';
					   $childTag = '';
				  }
		  
		   $this->set(compact('no_of_child','childTag'));
				
		  	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				// pr($this->data);die;
				    
					$this->StudentParent->id = $parent_id;
					$this->StudentParent->save($this->request->data);
					$i = 0;
					foreach($this->data['Student']['id'] as $id){ 
						$studentData['Student']['id'] = $id;
						$studentData['Student']['student_parent_id'] = $parent_id; 
						$studentData['Student']['examination_category_id'] = $this->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
						$i++;
					}
					
				    $this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
				    $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
			 }
			 
			 $this->data = $this->StudentParent->read(NULL, $parent_id);
		  
       }
	
	
	 public function exams_available($examination_category_id = NULL) {
        
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			$examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		 //$this->ExaminationCategory->recursive = 2;
		// echo $student_id; 'Examination', 'MyExam', 'ExaminationCategory'
		 	/*$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_parent_id' => $parent_id)
													),
												)

										));	*/
		 
		 $allExamAr = $this->Examination->find('all', array(
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));			
																 
		//pr($allExamAr);die;
		 $this->set(compact('parentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($parentDetails['StudentParent']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'parent' => $parentDetails['StudentParent']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'student_parents/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'student_parents/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'student_parents/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	
	
	
	
	
	
	
	 public function logout() {
			$this->UserConfiguration->parentLogout();
			$this->Session->delete('App.studentData');
			$this->Session->setFlash(__('You have logged out.'), 'success');
			$this->redirect(array('controller' => 'parent', 'action' => 'login'));
    }
	
	
	 protected function _parentLoginRedirect(){
        $isparentLoggedIn = $this->Session->check('App.parentData');
        if ($isparentLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'parent', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
	
   
     public function captcha_match_value() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$captcha_txt = $this->request->data['captcha_txt'];
		
		if(strcasecmp($captcha_txt,$this->Session->read('captcha'))!= 0)
			{
				echo 'ERROR';
			} else {
				echo 'OK';
			}
		
	 }
	 
	 public function get_parent_email_exist() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$email = $this->request->data['parent_email'];
		$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
			if($ExistEmail > 0){
				echo 'EXIST';
			} else {
				echo 'NOT-EXIST';
			}
	
		
	 }
   
   
}
?>