<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'configuration.php');
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'connection.php');

class StudentsController extends AppController {

    public $name = 'Students';
    public $uses = array('Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer', 'Answer', 'MyExamStudentAnswer', 'ExaminationType', 'ExaminationCategory','EmailTemplate','SchoolPurchaseExam');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
     
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration() {
        $this->layout = 'home_layout';
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		  $this->set(compact('examination_types', 'examination_categories'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 
			 //student login start
			 if(!empty($this->request->data['StudentLogin']['login_registration_flag'])  && $this->request->data['StudentLogin']['login_registration_flag'] == 'student_login'){ 
			  $login = strtolower($this->request->data['StudentLogin']['login']);
            $password = $this->request->data['StudentLogin']['password'];
			 $studentdata = $this->Student->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['StudentLogin']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['StudentLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($studentdata) && !empty(trim($this->request->data['StudentLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setStudentData($studentdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			} 
			 } else {
			
			 //student login End
			 
			 //student registration
			 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['Student']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['Student']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['Student']['password'];
							$cpass = $this->request->data['Student']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['Student']['password'] != $this->request->data['Student']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['Student']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['Student']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'students', 'action' => 'registration'));
				}
				
			 }
		 }
	  }

	
	
    public function login() {
        $this->layout = false;
		$ERROR = 0;
       if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			//pr($this->request->data);die;
            $login = strtolower($this->request->data['Student']['login']);
            $password = $this->request->data['Student']['password'];
			 $studentdata = $this->Student->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['Student']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['Student']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($studentdata)){
								$ERROR = 1;
								$this->set('lnerror', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setStudentData($studentdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			} else {
				 $this->redirect(array('controller' => 'students', 'action' => 'registration'));
			}
        }  
    }
    
    public function index() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $examinations = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0)));
		 $this->set(compact('studentDetails', 'examinations'));
		 $this->render('purchase_exam');
		// echo $RandomID = $this->Session->read('RandomID'); exit;
		 
    }
	
	 public function myprofile() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$studentdata1 = $this->UserConfiguration->getStudentData();
		//pr($studentdata1);die;
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));  
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		  $this->set(compact('studentDetails', 'examination_types', 'examination_categories'));
		  	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				$this->Student->id = $student_id;
				$this->Student->save($this->request->data);
				//$this->Session->setFlash('Data has been submitted successfully.');
				  $this->Session->setFlash(__('Data has been submitted successfully.'), 'success');
				$this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
			 }
		  
       }
	
	
	 public function all_exam_list() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));  
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $practiceExamCategory = $this->ExaminationCategory->find('all', array(
																										'recursive' => -1,	
																										 'conditions' => array(
																															 'ExaminationCategory.isdeleted' => 0,
																															 'ExaminationCategory.examination_type_id' => 2
																													) ));
		// pr($practiceExamCategory);die;
		  $this->set(compact('studentDetails',  'practiceExamCategory'));
		  	
       }
	
	
	
	
	
	
	
	
	//02-03-2017
		
	  public function view_answer($random_id = NULL, $student_idd = NULL, $cal_non_cal = NULL,$schoolPurchaseExam_id = NULL) {
       // $this->UserConfiguration->isStudentLoggedIn();
		 $this->layout = 'take_exam_layout';
		 if(!empty($student_idd)){
			  $view_by_student = '';
		 } else {
			 $view_by_student = 'view_by_student';
		 }
		$this->set(compact('view_by_student','cal_non_cal','schoolPurchaseExam_id'));
		 
		  $studentdata1 = $this->UserConfiguration->getStudentData();
				 if(!empty($student_idd)){
					  //$this->Session->delete('App.studentData');
					  //my_exam_id is school_purchase_exam_id
					  $myExamStudentAnsAr = $this->MyExamStudentAnswer->find('first', array(
																				  'fields' => 'id,my_exam_id,class_name,unique_flag',	
																				  'conditions' => array(
																						  'MyExamStudentAnswer.unique_flag' => $random_id
																						  )));
					  
					  $school_purchase_exam_id = $myExamStudentAnsAr['MyExamStudentAnswer']['my_exam_id'];
					  $class_name = $myExamStudentAnsAr['MyExamStudentAnswer']['class_name'];
					  $noOfStudentInYearGroup = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
					  $noOfStudentInClass = $this->MyExamStudentAnswer->find('count', array('conditions' => array(
																								  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																								  'MyExamStudentAnswer.class_name' => $class_name, 
																								  'MyExamStudentAnswer.exam_type' => 2
																								  )));

					 $studentRank = $this->fetch_student_rank($student_idd);
					 $yearGroupTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as year_group_scores'
															)));
														
						
																		
						$sameClassTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			'MyExamStudentAnswer.class_name' => $class_name
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as class_scores'
															)));										
					 
					  $yearGroupMean = round(($yearGroupTotalScores[0]['year_group_scores']/$noOfStudentInYearGroup),2); 
					 
					  $sameClassMean = round(($sameClassTotalScores[0]['class_scores']/$noOfStudentInClass),2);
					   
						$this->set(compact('studentRank', 'yearGroupMean', 'sameClassMean', 'noOfStudentInYearGroup'));
						
					    $this->set('flagA', 'answer_view_by_school');
					    $student_id = $student_idd;
				 } else {
					   $student_id = $studentdata1['Student']['id'];
				 }
				 
				  if(!empty($random_id)){
					  //$this->Session->delete('App.studentData');
					  $this->set('flagB', 'answer_view_by_school');
				 }
		
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id) && empty($student_idd)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		if(!empty($student_idd)){
		$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.unique_flag' => $random_id
																														)));
		
		} else {
			$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.student_ans_random_id' => $RandomID
																														)));
		}
		
		if(!empty($cal_non_cal) && $cal_non_cal == 'combined'){
			$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		    $totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		} else {
			$totalqustionSet = 32;
		}
		
		
		
		
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		if(!empty($student_idd) && !empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.unique_flag' => $random_id,
																																		 'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else if(!empty($student_idd) && (@$cal_non_cal == 'combined' || $cal_non_cal == '')){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.unique_flag' => $random_id,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else {
			$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 )));
		}																															 
																																		 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		 //if(!empty($student_idd) && !empty($cal_non_cal)){
		if(!empty($student_idd) && !empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 )));
	 
		 } else if(!empty($student_idd) && (@$cal_non_cal == 'combined' || $cal_non_cal == '')){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 //'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 )));
	 
		 } else {
			 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.random_id' => $RandomID
																													 )));
			 
		 }
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		
		 if($cal_non_cal == 'combined'){
			 
			 $QuestionAnsNonCalculator = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => 'non-calculator',
																													 )));
			 
			 $QuestionAnsCalculator = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => 'calculator',
																													 )));
			 
			$QuestionAnsCombined = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 //'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 ))); 
			 $this->set(compact('QuestionAnsNonCalculator', 'QuestionAnsCalculator','QuestionAnsCombined'));
			 $this->render('view_answer_combined');
		 } else {
			 $this->render('view_answer_new');
		 }
		 
		 
    }
	 
	 
	  public function fetch_student_rank($student_id = NULL) {
        //$this->autoRender = false;
	    $this->layout = 'take_exam_layout';
		$studentRank = $this->MyExamStudentAnswer->find('first', 
															array(
																'fields' => 'id,year_group_rank,class_rank',
																'conditions' => array(
																				'MyExamStudentAnswer.student_id' => $student_id,
																				'MyExamStudentAnswer.unique_flag !=' => ' ',
																				
																)));
		return $studentRank;
	  }
	
	
	  public function view_answer_bak($random_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		 $totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array('fields' => 'id, total_set_question','conditions' => array('MyExamStudentAnswer.student_id' => $student_id, 'MyExamStudentAnswer.student_ans_random_id' => $RandomID)));
		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		 'StudentAnswer.answer_id !=' => 0
																																		 )));
		$answer_ids = '';
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
		 }
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		  $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		
		 $totalAttemptQs = count($StudentAnswerAr);
		 
		  $this->paginate = array(
							'conditions' => array('StudentAnswer.student_id' => $student_id, 'StudentAnswer.random_id' => $RandomID),
							'limit' => 1
						);
       
	   $StudentAnswerAr = $this->paginate('StudentAnswer');
    
		
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet'));
		 
		//echo '<pre>';print_r($StudentAnswerAr);
		//die;
		 
    }
	 
	
	
	
	 public function my_exams() {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $this->MyExamStudentAnswer->recursive = 3;
		 
		 $this->Examination->unbindModel(array('hasMany' => 'Question'), false);
		 
		 $myExamAr___ = $this->MyExamStudentAnswer->find('all', array(
																								 'conditions' => array(
																								 'MyExamStudentAnswer.isdeleted' => 0, 
																								 'MyExamStudentAnswer.exam_type' => 1, 
																								 'MyExamStudentAnswer.student_id' => $student_id
																								 ),
																								'order' => 'MyExamStudentAnswer.created DESC',
																								'limit' => 10					
																								 ));
		//pr($myExamAr);die;
		
		 $this->paginate = array(
										'conditions' =>  array(
																 'MyExamStudentAnswer.isdeleted' => 0, 
																 'MyExamStudentAnswer.exam_type' => 1, 
																 'MyExamStudentAnswer.student_id' => $student_id
																 ),
										'limit' => 10,
										'order' => 'MyExamStudentAnswer.id DESC',
									);
			$myExamAr = $this->paginate('MyExamStudentAnswer');
		
		
		
		
		
		
		 $this->set(compact('studentDetails', 'myExamAr'));
		
    }
	
	
	   public function second_section_exam() {
         //$this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		  $this->set(compact('studentDetails'));
		 
	  }
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		 $RandomID = $this->Session->read('RandomID');
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.student_ans_random_id' => $RandomID,
																																		'MyExamStudentAnswer.my_exam_id' => $this->request->data['my_exam_id'],
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
					$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else if($flagQs != ' ' && $flagQs == 'NON-CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1; //1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 0; //0 for other
					}
					
					
					
					
				if(empty($MyExamStudentAnswerExist)){
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				}else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + $this->Session->read('totalQuestions');
						$this->MyExamStudentAnswer->save($data);
					}
			 
			
			/*$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
			$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
			$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
			$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
			$this->MyExamStudentAnswer->save($myExamStudentAns);*/
			
			
			 //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					 $this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
				}
			}
			
			
		//data insert which answer is not given	
		$questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
							
							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			// die;
			$this->Session->setFlash(__('Data has been submitted successfully.'), 'success');
			//$this->Session->setFlash('Data has been submitted successfully.');
			$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			//$this->redirect(array('controller' => 'students', 'action' => 'take_exam_list'));
			
			/*if($this->request->data['page_action'] == 'take_exam' && $this->request->data['exam_flag'] == 'SingleExam' ){ 
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			}
			else if($this->request->data['page_action'] == 'take_exam_second_section'){
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			} else {
					$this->redirect(array('controller' => 'students', 'action' => 'second_section_exam'));
			}
			*/
			   
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$flagQs = $this->Session->read('flag');
					
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else if($flagQs != ' ' && $flagQs == 'NON-CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1; //1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 0; //0 for other
					}
					
					
					
					
					
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					//pr($myExamStudentAns);die;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
					
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				     
					 $this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
					  $this->request->data['StudentAnswer']['student_id'] = $student_id;
					   $this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
					$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
					$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];

					$questionData['StudentAnswer']['student_id'] = $student_id;
					$questionData['StudentAnswer']['question_id'] = $qid;
					$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
					$questionData['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	  
	
	 public function take_exam($examination_id = NULL, $my_exam_id = NULL,$flag = NULL,$examFlag = NULL) {
        //Configure::write('debug', 0);
		$this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		$flag = $this->data_decrypt($flag);
		$examFlag = $this->data_decrypt($examFlag);
		$this->Session->write('flag', $flag);
		//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		
		 $settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		 $settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
			
		if($flag == 'NON-CALCULATOR'){
			$section = 'exam_section_2_name';
			//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
			$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
			$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			$examDuration = $settingsData24['Setting']['value'];
		} else if($flag == 'CALCULATOR'){
			$section = 'exam_section_1_name';
			 
			//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
			$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
			$QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$examDuration = $settingsData21['Setting']['value'];
		} else {
			$section = 'nothing'; //false
			$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0)));
			$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			
			if(!empty($settingsData24['Setting']['value'])){
		      $examDuration = $settingsData24['Setting']['value'];
			} else if(!empty($settingsData21['Setting']['value'])){
			  $examDuration = $settingsData21['Setting']['value'];
			} else {
			  $examDuration = 40; //minutes
			}
			
		}
		
		
		
		
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $examination_id = $this->data_decrypt($examination_id);
		 $myExamID = $this->data_decrypt($my_exam_id);
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		  $this->Session->write('my_exam_id', $myExamID);
		  
		  $this->set('my_exam_id', $myExamID);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		//$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		//$examDuration = $settingsData21['Setting']['value'];
		//$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		if($section == 'nothing'){
			$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.isdeleted' => 0)
																	 ));
		
		} else {
		$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,
																	 'Question.section' => $section,
																	 'Question.isdeleted' => 0
																	 )));
		
		}															 
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		
    }
	
	
//	public function take_exam($examination_id = NULL, $my_exam_id = NULL) {
	 public function take_exam_second_section($flag = NULL) {
        //Configure::write('debug', 0);
		
			$flag = $this->data_decrypt($flag);
				//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
				if($flag == 'CALCULATOR'){
					$section = 'exam_section_1_name';
				} else if($flag == 'NON-CALCULATOR'){
					$section = 'exam_section_2_name';
				} else {
					$section = 'nothing'; //false
				}
		
		
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'take_exam_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 //pr($studentDetails);die;
		 //$examination_id = $this->data_decrypt($examination_id);
		 //$myExamID = $this->data_decrypt($my_exam_id);
		 
		// $this->Session->write('examination_id', $examination_id);
		
		$examination_id = $this->Session->read('examination_id');
		$my_exam_id = $this->Session->read('my_exam_id');
		 $randomValue = $this->random_value();
		  //$this->Session->write('RandomID', $randomValue);
		 // $this->Session->write('my_exam_id', $myExamID);
		  
		 //$this->set('my_exam_id', $myExamID);
		  $this->set('my_exam_id', $my_exam_id);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		//$examDuration = $settingsData21['Setting']['value'];
		//$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		$examDuration = $settingsData24['Setting']['value'];
		$QuestionLimit =  $no_of_qs_23['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails'));
		 $this->render('take_exam');
    }
	
	public function get_calculator_exam_count($examination_id = NULL) {
		  // $this->UserConfiguration->isStudentLoggedIn();
           $this->layout = 'home_layout';
		  $CalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_1_name', //Calculator
																								'Question.isdeleted' => 0
																								)));
			return $CalculatorQuestionCount;																					
			
	}
	
	public function get_non_calculator_exam_count($examination_id = NULL) {
          //$this->UserConfiguration->isStudentLoggedIn();
          $this->layout = 'home_layout';
			$NonCalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_2_name', //Non calculator
																								'Question.isdeleted' => 0
																								)));
		return $NonCalculatorQuestionCount;																						
	}
	
	
	
	 public function take_exam_list() {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		$this->Session->delete('flag');
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $this->MyExam->recursive = 2;
		 $examinationsOLD = $this->MyExam->find('all', array(
																					'order' => 'MyExam.id DESC'	,
																					 'conditions' => array(
																													 'MyExam.isdeleted' => 0, 
																													 'MyExam.payment_status' => 1, 
																													 'MyExam.student_id' => $student_id
																													 )));
		// pr($examinations);die;
		
		 $this->paginate = array(
							'conditions' => array(
												 'MyExam.isdeleted' => 0, 
												 'MyExam.payment_status' => 1, 
												 'MyExam.student_id' => $student_id
							 ),
							'limit' => 8,
							'order' => 'MyExam.id DESC'	,
							//'order' => array('MyExam.id DESC', 'Examination.paper ASC')
						);
       
	   $examinations = $this->paginate('MyExam');
		
		
		
		 $this->set(compact('studentDetails', 'examinations'));
		
    }
	
		 public function checkout() {
			$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
			 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'students', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_id' => $student_id)
                ),
            )

        ));	

				 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					// pr($this->data);die;
					 $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $this->request->data['Examination']['ids']
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					foreach($allExamAr as $val){
						$subTotal = $subTotal + $val['Examination']['price'];
					}
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 }
				 
		 $this->set(compact('studentDetails', 'allExamAr','subTotal','gst', 'total'));
		
    }
	
	
	 public function checkout_new($examination_id = NULL) {
			$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
			 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'students', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_id' => $student_id)
						),
					)

				));	

				     $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $examination_id
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					
						$subTotal = $allExamAr[0]['Examination']['price'];
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 
		 $this->set(compact('studentDetails', 'allExamAr','subTotal','gst', 'total'));
		 $this->render('checkout');
		
    }
	
		 public function is_exist_in_myexam($examination_id = NULL) {
			//$this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'home_layout';
			$this->autoRender = false;
			 $studentdata1 = $this->UserConfiguration->getStudentData();
			 $student_id = $studentdata1['Student']['id'];
			 $isMyexamPurchased = $this->MyExam->find('count', array(
														 'conditions' => array(
																'MyExam.examination_id' => $examination_id,
																'MyExam.student_id' => $student_id,
																'MyExam.payment_status' => 1,
																'MyExam.isdeleted' => 0,
																'MyExam.is_expired' => 0,
														 )));
			return $isMyexamPurchased;											 
		 }
	
	
	
	
	 public function purchase_exam($examination_category_id = NULL) {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		// echo $student_id;
		 	$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_id' => $student_id)
													),
												)

										));	
		 
		 $allExamAr = $this->Examination->find('all', array(
																						 'conditions' => array(
																										 'Examination.isdeleted' => 0,  
																										 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																										 'Examination.examination_category_id' => $examination_category_id
																							 )));
		//pr($allExamAr);die;
		 $this->set(compact('studentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'student' => $studentDetails['Student']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	
	 public function exams_available($examination_category_id = NULL) {
        
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		 //$this->ExaminationCategory->recursive = 2;
		// echo $student_id;
		 	$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_id' => $student_id)
													),
												)

										));	
		 
		 $allExamAr = $this->Examination->find('all', array(
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));			
																 
		//pr($allExamAr);die;
		 $this->set(compact('studentDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'student' => $studentDetails['Student']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	
	
	 /* Start Payment functions */
	 
	    public function payment_success() {
			$this->layout = false;
			if ($this->request->is('post') || $this->request->is('put')) {
				   $customString = explode('||', $this->request->data['custom']);
					$ExamIdsAr = explode('#', $customString[0]);
					$eachTotalPriceAr = explode('**', $customString[1]);
					$student_id = $customString[2];
				
				$dateTime = new DateTime('NOW');
				$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
				$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

				$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
				$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
			
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
										$i = 0;
										foreach($ExamIdsAr as $examId){
											$this->request->data['MyExam']['student_id'] = $student_id;
											$this->request->data['MyExam']['examination_id'] = $examId;
											$this->request->data['MyExam']['payment_status'] = 1;
											$this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
											$this->request->data['MyExam']['amount'] = $eachTotalPriceAr[$i];
											
											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
											
											$this->MyExam->create();
											$this->MyExam->save($this->request->data);
												$i++;
										}
							
							   $this->Session->write('Student.transactionData', $this->request->data);
                              $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_success'));
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	 
	 
	 
    public function payment_success__bak() {
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->request->is('post')) {
         //   pr($this->request->data);
           // echo 'next array';
            $payPalCustomData = array();
            $pagedata = explode('##', $this->request->data['custom']);
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
                $this->request->data['MyExam']['student_id'] = $pagedata[0];
                $this->request->data['MyExam']['examination_id'] = $pagedata[1];
                $this->request->data['MyExam']['payment_status'] = 1;
                $this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
                $this->request->data['MyExam']['amount'] = $this->request->data['payment_gross'];
				$this->MyExam->save($this->request->data);
                /* For school payment success */
               // if(!empty($payPalCustomData['student'])){
                    $this->Session->write('Student.transactionData', $this->request->data);
                    $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_success'));
               // }
                
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	 public function examinations($examination_id = NULL) {
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata = $this->UserConfiguration->getStudentData();
		 $questions = $this->Question->find('all', array('conditions' => array('Question.examination_id' => $examination_id, 'Question.isdeleted' => 0)));
		 $this->set(compact('studentdata', 'questions'));
		 pr($questions);
		 die;
    }
    
    function makePayment($DATA){

        $method = 'PUT' ;
        $customUri = "";
		if (array_key_exists("orderId", $DATA))
		  $customUri .= "/order/" . $DATA["orderId"];

		if (array_key_exists("transactionId", $DATA))
		  $customUri .= "/transaction/" . $DATA["transactionId"];

		$unsetNames = array("orderId", "transactionId", "creditsubmit","nameofcard");

		foreach ($unsetNames as $fieldName) {
		  if (array_key_exists($fieldName, $DATA))
		    unset($DATA[$fieldName]);
		}
		$merchantObj = new Merchant();
		$parserObj = new Parser($merchantObj);
		if (array_key_exists("version", $DATA)) {
		  $merchantObj->SetVersion($DATA["version"]);
		  unset($DATA["version"]);
		}
		$request = $parserObj->ParseRequest($DATA);
		if ($request == ""){
			return "not Getting any request!";
		}
		//  The below code for debugging for that enable Debug to true in configuration file
		/*if ($merchantObj->GetDebug())
        echo $request . "<br/><br/>";*/
        $requestUrl = $parserObj->FormRequestUrl($merchantObj, $customUri);
        $response = $parserObj->SendTransaction($merchantObj, $request, $method);
        return $response ;
  
     }
     
     
    public function make_payment($examination_id = NULL){
					$this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'home_layout';
					$MultipleExamPayment = $this->Session->read('MultipleExamPayment');
					$gross_total = $MultipleExamPayment['gross_total'];				
					$studentdata1 = $this->UserConfiguration->getStudentData();
					$student_id = $studentdata1['Student']['id'];					
					$studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));					
					$ERROR = 0;
			if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['number']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])) || empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['securityCode']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['nameofcard']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year'])){
								$this->set('year','Please select year');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])){
								$this->set('exp_month', 'Please select month');
							}							
						
						if($ERROR == 0){

                            $response = $this->makePayment($_POST);
                            $responseArray = json_decode($response, TRUE);
                            //pr($responseArray); exit ;
                            if ($responseArray == NULL){
                                $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
                                $this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));                                	

                            }else if(array_key_exists("result", $responseArray)){
                                    $result = $responseArray["result"];
                                    if ($result == "FAILURE") {
                                        $errorMessage = "";
                                        $tmpArray = array();

                                		if (array_key_exists("reason", $responseArray)) {
									    $tmpArray = $responseArray["reason"];

									    if (array_key_exists("explanation", $tmpArray)) {
									      $errorMessage = rawurldecode($tmpArray["explanation"]);
									    }
									    else if (array_key_exists("supportCode", $tmpArray)) {
									      $errorMessage = rawurldecode($tmpArray["supportCode"]);
									    }
									    else {
									      $errorMessage = "Reason unspecified.";
									    }

									    // if (array_key_exists("code", $tmpArray)) {
									    //   $errorCode = "Error (" . $tmpArray["code"] . ")";
									    // }
									    // else {
									    //   $errorCode = "Error (UNSPECIFIED)";
									    // }
									    }
                                	
										$this->Session->setFlash($errorMessage);
										$this->set('payment_error', $errorMessage);
										$this->redirect(array('controller'=>'students','action'=>'payment_error'));							

                                	}else if($result == "ERROR"){
                                		$errorMessage = "";
                                        $tmpArray = array();
	                                	if (array_key_exists("error", $responseArray)) {
										    $tmpArray = $responseArray["error"];
										    if (array_key_exists("explanation", $tmpArray)) {
										     $errorMessage = rawurldecode($tmpArray["explanation"]);
										    }
										    else if (array_key_exists("cause", $tmpArray)) {
										     $errorMessage = rawurldecode($tmpArray["supportCode"]);
										    }
										    else{
										     $errorMessage = "Reason unspecified.";
										    }
									    }                                	
										$this->Session->setFlash($errorMessage);
										$this->set('payment_error', $errorMessage);
										$this->redirect(array('controller'=>'students','action'=>'payment_error'));
                                	}else if($result == "SUCCESS"){
                                		$dateTime = new DateTime('NOW');
										$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
										$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

										$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
										$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );

										$datas = $MultipleExamPayment['ids'];
										$i = 0;
										foreach($datas as $exam_id){
											$this->request->data['MyExam']['student_id'] = $student_id;
											$this->request->data['MyExam']['examination_id'] =  $exam_id;
											$this->request->data['MyExam']['payment_status'] = 1;
											$this->request->data['MyExam']['txn_id'] = $responseArray['transaction']['id'];
											
										$this->request->data['MyExam']['order_id'] = $responseArray['order']['id'];
										
											$this->request->data['MyExam']['amount'] =  $MultipleExamPayment['total_price'][$i];
											
											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
											
											$this->MyExam->create();
											$this->MyExam->save($this->request->data);
											$i++;
										}
										
										$htmlList = '<table style="width:100%;">
										   <tr style="background: #1f3446; color:#fff;">
										   <th style="padding:8px;">List</th>
										   <th style="padding:8px;">Year</th>
										   <th style="padding:8px;">Exam Name</th>
										   <th style="padding:8px;">Category</th>
										   <th style="padding:8px;">Price ($ AUD)</th>
										   <th style="padding:8px;">Total Paid</th>
									    </tr>';
										$j = 0;
										$k = 1;
					     	            foreach($datas as $exam_id){
									    $examDtl = $this->Examination->find('first', array('conditions' => array(
																									'Examination.id' => $exam_id
																							)));
										$category = explode(' ',$examDtl['ExaminationCategory']['name']);
										$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
										$inserted = array();
										$inserted[] = 'Year '.$category[1];
										array_splice( $examTitle, 1, 0, $inserted );
								        $examName = implode(' ', $examTitle);	
								        $paper = $examDtl['Examination']['paper'];					   
																							
									    $price_per_student = $examDtl['Examination']['price'];
									    //$examName = $examDtl['Examination']['title'];
								        $htmlList .= '<tr style="text-align:center; background:#EFEBE6">
											   <td style="padding:8px;">'.$k.'</td>
											   <td style="padding:8px;">'.$category[1].'</td>
											   <td style="padding:8px;">Numeracy '.$paper.'</td>
											   <td style="padding:8px;">NAPLAN Style Practice</td>
											   <td style="padding:8px;">'.$price_per_student.'</td>
											   
											   <td style="padding:8px;">'.($MultipleExamPayment["total_price"][$j] + ($MultipleExamPayment["total_price"][$j] *(10/100))).'</td>
											 </tr>';
										$j++;
										$k++;					
								       }
												
										$htmlList .= '</table>';
														
										$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
										$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
										$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 13)));
										$html_content = $mailTemplateData['EmailTemplate']['content'];
										$validity = $examValidity; //days will come from admin
										$full_name = $studentDetails['Student']['first_name'].' '.$studentDetails['Student']['last_name'];
										$html = '';
										$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
										$html = str_replace('{{full_name}}', $full_name, $html);
										$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
										$html = str_replace('{{validity}}', $validity, $html);
										$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);
										
									   //pr($html);die;
										$email_subject = $mailTemplateData['EmailTemplate']['title'];
										$email = $studentDetails['Student']['email'];
										
										
										$mailFrom = 'sales@learnerleader.com';
										$FromName = 'Learner Leader Online';
					                   
									    $mail = new PHPMailer;
									    $mail->FromName = $FromName;
					                    $mail->From = $mailFrom;
					                    $mail->Subject = $email_subject;
					                    $mail->Body = stripslashes($html);
					                    $mail->AltBody = stripslashes($html);
					                    $mail->IsHTML(true);
					                    $mail->AddAddress($email);
					                    $mail->AddAddress('dinesh.brainium@gmail.com');
					                    $mail->Send();
													
										
										
									    $this->redirect(array('controller'=>'students','action'=>'purchase_exam_success'));
                                	}

                            }


						   
						}
							
			}
						
		$paypalFormSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($studentDetails['Student']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'AUD'
        );

        $customdata = array(
            'student_id' => $studentDetails['Student']['id'],
            'examination_id' => @$examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
		
		$examID = null;
		$datas = $MultipleExamPayment['ids'];
		foreach($datas as $exam_id){
				if (!empty($examID)) {
					$examID .= "";
				}
				$examID .= "#" . $exam_id;
		}
		
		$eachTotalPrice = null;
		$total_price = $MultipleExamPayment['total_price'];
		foreach($total_price as $val){
			if (!empty($eachTotalPrice)) {
				$eachTotalPrice .= "";
			}
			$eachTotalPrice .= "**" . $val;
		}		
		$custom1 = trim($examID, '#').'||'.trim($eachTotalPrice, '**').'||'.$studentDetails['Student']['id'];
        $paypalFormSettings['item_name'] = 'Purchase Practice Exam(s)';        
        $paypalFormSettings['custom'] = $custom1;        
        $paypalFormSettings['amount'] = $gross_total; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'students/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'students/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'students/payment_cancel'. '/' . $custom;
        $transactionId = "Trans".md5(uniqid(rand(), true))  ;
	    $orderId       = "Order".md5(uniqid(rand(), true))  ;
        $transOrderId = array(
              'transactionId'=>$transactionId,
              'orderId'=>$orderId
       	);
		$this->set(compact('studentDetails', 'examDtls','paypalFormSettings', 'gross_total','transOrderId'));


	} 
	
		   
    
    public function purchase_exam_success(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
        $this->set(compact('studentDetails'));
        if($this->Session->check('Student.transactionData')){
            
            $transactionData = $this->Session->read('Student.transactionData');
            $this->Session->delete('Student.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $student_id = $pagedata[0]; 
			 $student_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->Student->read(null, $payPalCustomData['student']);
            $this->MyExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
	 public function payment_error(){
        $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
        $this->set(compact('studentDetails'));
	 }
	
	
	
	
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->studentLogout();
		 $this->Session->delete('App.studentData');
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'student', 'action' => 'login'));
        //$this->redirect(array('controller' => 'students', 'action' => 'registration'));
    }
	
     public function close_page() {
        $this->UserConfiguration->studentLogout();
		$this->Session->delete('App.studentData');
        $this->redirect(array('controller' => '/', 'action' => ''));
    }
    
    protected function _studentLoginRedirect(){
        $isLoggedIn = $this->Session->check('App.studentData');
        if ($isLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
	//added by Dinesh
	//now below method is calling from aap controller
			function PPHttpPost___($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	public function fetch_student_total_correct_answer($random_id = NULL, $student_id = NULL, $cal_non_cal = NULL) {
       $this->layout = 'ajax';
            
			 if(!empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0,
                    'StudentAnswer.calculator_non_calculator' => $cal_non_cal
            )));
			} else {
				 $StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0
            )));
			}
           
			
			
			/*$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
                    'StudentAnswer.student_id' => $student_id,
                    'StudentAnswer.unique_flag' => $random_id,
                    'StudentAnswer.answer_id !=' => 0
            )));*/
        

        $answer_ids = '';
        foreach ($StudentAnswerAr as $val) {
            //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
            $answer_ids = $answer_ids . ',' . $val['StudentAnswer']['answer_id'];
        }
        $answer_ids = trim($answer_ids, ",");
        $answer_ids_ar = explode(',', $answer_ids);
        // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1, 'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
        $data = array();
        $data['totalCorrectAnswer']   = $correctAnswer;
        $data['totalAttempQuestions'] = count($StudentAnswerAr);
        return $data;
    }
	
	  public function login_new() {
        $this->layout = 'home_layout';
        //$this->layout = 'landing_layout';
        $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0)));
        $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0)));
        $this->set(compact('examination_types', 'examination_categories'));
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {

            //student login start
            if (!empty($this->request->data['StudentLogin']['login_registration_flag']) && $this->request->data['StudentLogin']['login_registration_flag'] == 'student_login') {
                $login = strtolower($this->request->data['StudentLogin']['login']);
                $password = $this->request->data['StudentLogin']['password'];
                $studentdata = $this->Student->getLoginData($login, $password);

                if (empty(trim($this->request->data['StudentLogin']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['StudentLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($studentdata) && !empty(trim($this->request->data['StudentLogin']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }

                if ($ERROR == 0) {
                    $this->UserConfiguration->setStudentData($studentdata);
                    
					$this->Student->id = $studentdata['Student']['id'];
					//$lastdate = date("Y-m-d h:i:s");
					$lastdate = date("Y-m-d H:i:s", strtotime('+5 hour +30 minutes'));
					$this->Student->saveField('last_login', $lastdate);
					
                    $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                    //$this->redirect(array('controller' => 'students', 'action' => 'myprofile'));
                    $this->redirect(array('controller' => 'students', 'action' => 'take_exam_list'));
                }
            } else {

                //student login End
                //student registration
                if (empty(trim($this->request->data['Student']['first_name']))) {
                    $ERROR = 1;
                    $this->set('fnerror', 'Please enter first name');
                }

                if (empty(trim($this->request->data['Student']['last_name']))) {
                    $ERROR = 1;
                    $this->set('lnerror', 'Please enter last name');
                }

                if (empty(trim($this->request->data['Student']['address']))) {
                    $ERROR = 1;
                    $this->set('adderror', 'Please enter address');
                }
                if (empty(trim($this->request->data['Student']['school_name']))) {
                    $ERROR = 1;
                    $this->set('scerror', 'Please enter school name');
                }


                if (empty(trim($this->request->data['Student']['email']))) {
                    $ERROR = 1;
                    $this->set('emerror', 'Please enter email');
                } else {
                    $email = $this->request->data['Student']['email'];
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $ERROR = 1;
                        $this->set('emerror', 'Please enter valid email format');
                    }
                }

                if (!empty(trim($this->request->data['Student']['email']))) {
                    $email = $this->request->data['Student']['email'];
                    $ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
                    if ($ExistEmail > 0) {
                        $ERROR = 1;
                        $this->set('emerror', 'This email already exist, please try another');
                    }
                }


                if (empty(trim($this->request->data['Student']['password']))) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Please enter password');
                }
                if (!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) < 6) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Password must be 6 to 20 characters');
                }
                if (!empty(trim($this->request->data['Student']['password'])) && strlen($this->request->data['Student']['password']) > 20) {
                    $ERROR = 1;
                    $this->set('pwderror', 'Password must be 6 to 20 characters');
                }


                if (empty(trim($this->request->data['Student']['confirm_password']))) {
                    $ERROR = 1;
                    $this->set('cpwderror', 'Please enter confirm password');
                }


                $pass = $this->request->data['Student']['password'];
                $cpass = $this->request->data['Student']['confirm_password'];
                if (!empty($pass) && !empty($cpass) && $this->request->data['Student']['password'] != $this->request->data['Student']['confirm_password']) {
                    $ERROR = 1;
                    $this->set('cpwderror', 'Password and confirm password does not match');
                }

                if (empty(trim($this->request->data['Student']['captcha_txt']))) {
                    $ERROR = 1;
                    $this->set('captchaErr', 'Please enter captcha code');
                } else {
                    if (strcasecmp($this->data['Student']['captcha_txt'], $this->Session->read('captcha')) != 0) {
                        $ERROR = 1;
                        $this->set('captchaErr', 'Please enter correct captcha code ');
                    }
                }




                if ($ERROR == 0) {
                    $this->request->data['Student']['username'] = $this->request->data['Student']['email'];
                    $this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
                    $this->Student->save($this->request->data);

                    $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
                    $this->redirect(array('controller' => 'students', 'action' => 'registration'));
                   
                }
            }
        }
    }
	
	
	//28-08-2017 start
	 public function sample_question_save() {
       // $this->UserConfiguration->isStudentLoggedIn();
        $this->layout = 'home_layout';
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		 //$RandomID = $this->Session->read('RandomID');
		 $RandomID = 'SQ-'.$this->random_value();
		 $this->Session->write('SQ.RandomID');
		// echo 'hello==>'.$RandomID;die;
		 //$RandomID = $this->Session->read('RandomID');
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			
			$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else if($flagQs != ' ' && $flagQs == 'NON-CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1; //1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 0; //0 for other
					}
					
					
				
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = 0;
					
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = 0;
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  8;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				
			
			
			
			 //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = 0;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = 0;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
				  $this->request->data['StudentAnswer']['student_id'] = 0;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					 $this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
				}
			}
			
			
		//data insert which answer is not given	
		$questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								// 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
							
							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = 0;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
		
			$this->Session->setFlash(__('Data has been submitted successfully.'), 'success');
			$this->redirect(array('controller' => 'students', 'action' => 'sample_question_view_answer',$RandomID));
			
		 }
		 
	  }
	  
	 public function sample_question_view_answer($RandomID = NULL) {
       // $this->UserConfiguration->isStudentLoggedIn();
			$this->layout = 'take_exam_layout';
			$this->set('view_by_student','view_by_student');
			$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
												'fields' => 'id, total_set_question',
												'conditions' => array(
																'MyExamStudentAnswer.student_ans_random_id' => $RandomID
																)));

		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 8 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 //'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		))));
																																 
			//pr($StudentAnswerAr);die;																															 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													//'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.random_id' => $RandomID
																													 )));
		
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		 
    }
	
	 /** jitendra 15-11-2017 **/
	public function review_exam(){
		$this->layout = 'home_layout';

	}
	public function check_valid_student(){
		$this->layout = false;
		//pr($_POST);
		$condi = array('Student.email'=>$_POST['email_id'],'Student.password'=>'','Student.is_review_exam !='=> 1);
		$check_email = $this->Student->find('count',array('conditions'=>$condi));
		if($check_email>0){
			echo "VALLID";
		}else{
			echo "UNVALLID";
		}
		exit;
	}
	public function check_review_exam_code(){
		$this->layout = false;
		$condi = array('SchoolPurchaseExam.review_exam_code'=>$_POST['review_exam_code']);
		$check_review_code = $this->SchoolPurchaseExam->find('count',array('conditions'=>$condi));
		if($check_review_code>0){
			echo "VALLID";
		}else{
			echo "UNVALLID";
		}
		exit;
	}
	
	public function check_exam_given(){
		$this->layout = false;
		$condi = array('Student.email'=>$_POST['email_id']);
		$student_details = $this->Student->find('first',array('conditions'=>$condi));
		//pr($student_details);
		$student_id = $student_details['Student']['id'];
		$condi2 = array('SchoolPurchaseExam.review_exam_code'=>$_POST['review_exam_code']);
		
		$exam_details = $this->SchoolPurchaseExam->find('first',array('conditions'=>$condi2));
		if(!empty($exam_details)){
			$school_purchase_exam_id = $exam_details['SchoolPurchaseExam']['id'];
			//pr($exam_details);
			
			$condi3 = array('MyExamStudentAnswer.student_id'=>$student_id,'MyExamStudentAnswer.my_exam_id'=>$school_purchase_exam_id,'MyExamStudentAnswer.exam_type'=>2);
			$check_exam_given = $this->MyExamStudentAnswer->find('count',array('conditions'=>$condi3));
			if($check_exam_given>0){
				echo "VALLID";
			}else{
				echo "UNVALLID";
			}
		}else{
			echo "UNVALLID";
		}
		exit;
	}
	public function result() {
		$this->layout = 'take_exam_layout';
		$view_by_student = '';
       // $this->UserConfiguration->isStudentLoggedIn();
		$random_id 				= '';
		$student_idd 			= '';
		$cal_non_cal 			= 'combined';
		$schoolPurchaseExam_id 	= '';
		
		 if(!empty($this->request->data['Student']['school_email'])){
			  $this->Student->updateAll(array('Student.is_review_exam' => 1),array('Student.email' => $this->request->data['Student']['school_email']));
		  }
		
		//student_details
		$condi = array('Student.email'=>$this->request->data['Student']['school_email']);
		$student_details = $this->Student->find('first',array('conditions'=>$condi));
		$student_idd = $student_details['Student']['id'];
		$student_id = $student_idd;
	    //schooldetails
		$condi2 = array('SchoolPurchaseExam.review_exam_code'=>$this->request->data['Student']['review_exam_code']);
		$school_details = $this->SchoolPurchaseExam->find('first',array('conditions'=>$condi2));
		$schoolPurchaseExam_id = $school_details['SchoolPurchaseExam']['id'];
		//pr($school_details);
		///echo $student_idd.'-'.$cal_non_cal.'-'.$schoolPurchaseExam_id.'-'.base64_encode($schoolPurchaseExam_id);
		// exit;
		
		$this->set(compact('view_by_student','cal_non_cal','schoolPurchaseExam_id'));
		 
		// $studentdata1 = $this->UserConfiguration->getStudentData();
				 if(!empty($student_idd)){
					  //$this->Session->delete('App.studentData');
					  //my_exam_id is school_purchase_exam_id
					  $myExamStudentAnsAr = $this->MyExamStudentAnswer->find('first', array(
																				  'fields' => 'id,my_exam_id,class_name,unique_flag',	
																				  'conditions' => array(
																						  //'MyExamStudentAnswer.unique_flag' => $random_id
																						  'MyExamStudentAnswer.student_id' => $student_id,
																						  )));
					  
					  $school_purchase_exam_id = $myExamStudentAnsAr['MyExamStudentAnswer']['my_exam_id'];
					  $class_name = $myExamStudentAnsAr['MyExamStudentAnswer']['class_name'];
					  $noOfStudentInYearGroup = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
					  $noOfStudentInClass = $this->MyExamStudentAnswer->find('count', array('conditions' => array(
																								  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																								  'MyExamStudentAnswer.class_name' => $class_name, 
																								  'MyExamStudentAnswer.exam_type' => 2
																								  )));

					 $studentRank = $this->fetch_student_rank($student_idd);
					 $yearGroupTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as year_group_scores'
															)));
														
						
																		
						$sameClassTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			'MyExamStudentAnswer.class_name' => $class_name
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as class_scores'
															)));										
					 
					  $yearGroupMean = round(($yearGroupTotalScores[0]['year_group_scores']/$noOfStudentInYearGroup),2); 
					 
					  $sameClassMean = round(($sameClassTotalScores[0]['class_scores']/$noOfStudentInClass),2);
					   
						$this->set(compact('studentRank', 'yearGroupMean', 'sameClassMean', 'noOfStudentInYearGroup'));
						
					    $this->set('flagA', 'answer_view_by_school');
					    $student_id = $student_idd;
				 } else {
					   $student_id = $studentdata1['Student']['id'];
				 }
				 
				  if(!empty($random_id)){
					  //$this->Session->delete('App.studentData');
					  $this->set('flagB', 'answer_view_by_school');
				 }
		
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id) && empty($student_idd)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		if(!empty($student_idd)){
		$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				//'MyExamStudentAnswer.unique_flag' => $random_id
																														)));
		
		} else {
			$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				//'MyExamStudentAnswer.student_ans_random_id' => $RandomID
																														)));
		}
		
		if(!empty($cal_non_cal) && $cal_non_cal == 'combined'){
			$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		    $totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		} else {
			$totalqustionSet = 32;
		}
		
		
		
		
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		if(!empty($student_idd) && !empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 //'StudentAnswer.unique_flag' => $random_id,
																																		 'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else if(!empty($student_idd) && (@$cal_non_cal == 'combined' || $cal_non_cal == '')){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 //'StudentAnswer.unique_flag' => $random_id,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else {
			$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 //'StudentAnswer.random_id' => $RandomID,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 )));
		}																															 
																																		 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		 //if(!empty($student_idd) && !empty($cal_non_cal)){
		if(!empty($student_idd) && !empty($cal_non_cal) && ($cal_non_cal == 'calculator' || $cal_non_cal == 'non-calculator')){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 )));
	 
		 } else if(!empty($student_idd) && (@$cal_non_cal == 'combined' || $cal_non_cal == '')){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 //'StudentAnswer.unique_flag' => $random_id,
																													 //'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 )));
	 
		 } else {
			 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 //'StudentAnswer.random_id' => $RandomID
																													 )));
			 
		 }
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		  
		  if($cal_non_cal == 'combined'){
		  $QuestionAnsNonCalculator = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 //'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => 'non-calculator',
																													 )));
			 
			 $QuestionAnsCalculator = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 //'StudentAnswer.unique_flag' => $random_id,
																													 'StudentAnswer.calculator_non_calculator' => 'calculator',
																													 )));
			 
			$QuestionAnsCombined = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 //'StudentAnswer.unique_flag' => $random_id,
																													 //'StudentAnswer.calculator_non_calculator' => $cal_non_cal,
																													 ))); 
			 $this->set(compact('QuestionAnsNonCalculator', 'QuestionAnsCalculator','QuestionAnsCombined'));
		  }
			 
		 $this->render('current_view_answer_combined');
		// $this->render('view_answer_new');
    }
	
}
?>