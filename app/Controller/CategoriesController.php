<?php

App::import('Sanitize');
class CategoriesController extends AppController {

    public $name = 'Categories';
     public $components = array("FilterArticles", 'Uploader.Uploader', 'Share' => array(
		'model' => array('alias' => 'Article'),
		'notification' => array('type' => 7)
	),'FilterArchives', 'Share' => array(
            'model' => array('alias' => 'Archive'),
            'notification' => array('type' => 5)
    ),"FilterEvents", 'Uploader.Uploader', 'Share' => array(
		'model' => array('alias' => 'Content'),
		'notification' => array('type' => 8)
	),'FilterTopics', 'Share' => array(
		'model' => array('alias' => 'Topic'),
		'notification' => array('type' => 6)
	),'FilterRequestQuotations','FilterLicitations');
    public $uses = array('Topic', 'RequestQuotation','RequestQuotationsCategory','Licitation','LicitationsCategory','Category','TopicsCategory','Content', 'ContentsCategory','Article', 'ArticlesCategory', 'Archive', 'Notification', 'ArchivesCategory','Comments.Comment', 'User', 'Profile', 'Invitation', 'Notification','CommunitiesSetting');
    public function index($all=null) {
        $conditions = array('Category.community_id' => Configure::read('Community.id'));
        if (!Configure::read('User.isAdministrator'))
            $conditions['Category.private'] = 0;
        $categories = $this->Category->find('all', array('conditions' => $conditions, 'order' => array('Category.name' => 'ASC')));  
        if($all==null){
          $categories = $this->buildTree($categories);
        }
        $this->set(compact('categories'));          
    }
    public function edit ($id = null)
    {
        if (empty($id)){
            $this->Session->setFlash(__('Invalid category id'), 'error');
            $this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'index'));
        }
        if (!empty($this->request->data)){
            if(empty($this->request->data['Category']['parent_id'])){
                 $this->request->data['Category']['parent_id']=0;
            }
            $oldslug=$this->request->data['Category']['slug'];
            $this->request->data['Category']['id'] = $id;
            $this->request->data['Category']['slug'] = strtolower(Inflector::slug($this->request->data['Category']['name'], $replacement = '-'));
            if ($this->Category->save($this->request->data)){
                if (isset($this->request->data['Category']['shareCategory'])) {
                    $share_Communities = $this->request->data['Category']['shareCategory'];
                    if (!empty($share_Communities)) {
                        foreach ($share_Communities As $key => $val) {
                            //$this->request->data['Category']['community_id'] = $val;
                            
                            $this->Category->updateAll(array('Category.name'=>"'".$this->request->data['Category']['name']."'",'Category.slug'=>"'".$this->request->data['Category']['slug']."'"),array('Category.id'=>$val));        
                        }
                    }
                }
                if (isset($this->request->data['Category']['shareCommunity'])) {
                    $share_Communities = $this->request->data['Category']['shareCommunity'];
                    if (!empty($share_Communities)) {
                        foreach ($share_Communities As $key => $val) {
                            $catexistcomm=$this->Category->find('count', array(
                                    'conditions' => array('Category.slug' => $this->request->data['Category']['slug'],'Category.community_id' =>$val)
                                        ));
                            if($catexistcomm==0){
                                $this->Category->create();
	                $r = $this->Category->save(array(
	                    'name' =>$this->request->data['Category']['name'],
	                    'slug' =>$this->request->data['Category']['slug'],
                            'community_id'=>$val
	                ));
                            }
                            
                        }
                    }
                 }
                 
                $this->Session->setFlash(__('Category updated successfully'), 'success');
                $this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'index'));
            }
            
            
                 
            else
            {
                $this->Session->setFlash(__('Unable to update category'), 'error');
                $this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'index'));
            }
        }
        $currentlang=Configure::read('Config.language');
        if($currentlang=='es_ES'){
            $langvariable='Community.slug';
        }
        else{
            $langvariable='Community.name';
        }
        
        $communities = $this->Community->find('all', array(
            'conditions' => array('Community.deleted' => 0, 'Community.id != ' => Configure::read('Community.id')),
            'order' => array($langvariable => 'asc'),
                )
        );
         $eventecom=$this->Category->find('first', array(
        'conditions' => array('Category.id' => $id)
            ));
         $eventecomcount=$this->Category->find('count', array(
        'conditions' => array('Category.id' => $id)
            ));
         if($eventecomcount>0){
         $eventcombyslg=$eventecom=$this->Category->find('all', array(
        'conditions' => array('Category.slug' => $eventecom['Category']['slug'])
            ));
         }
         $this->set(compact('eventcombyslg')); 
         $this->set(compact('communities')); 
        $cat = $this->Category->read(null,$id);
        $this->request->data=$cat;
    }
    public function add($cat = null,$parent_id=null) {
        if ($cat == null) {
            if (!empty($this->request->data)) {
                
                if(empty($this->request->data['Category']['parent_id'])){
                 $this->request->data['Category']['parent_id']=0;
                }
                
                $this->request->data['Category']['slug'] = strtolower(Inflector::slug($this->request->data['Category']['name'], $replacement = '-'));
                $this->request->data['Category']['community_id'] = Configure::read('Community.id');
                //$this->request->data['Category']['name'] = Sanitize::clean($this->request->data['Category']['name'], array(' ', '@'));
                $this->Category->create();
                if ($this->Category->save($this->request->data)) {
                         if(isset($this->request->data['Category']['shareCommunity']))
                                {
                                    $community = $this->request->data['Category']['shareCommunity'];
                                }
                            if (!empty($community) ) {
                               
                                foreach ($community as $com) {
                                    
                                      
                                    if(!empty($this->request->data['Category']['parent_id'])){
                                        $getcomcat=$communityparent = $this->Category->find('first', array(
                                'conditions' => array('Category.id' => $this->request->data['Category']['parent_id'], 'Category.community_id' => Configure::read('Community.id'))
                                    ));
                                        
                                        $checkcommunitycategory = $this->Category->find('count', array(
                                'conditions' => array('Category.slug' => $getcomcat['Category']['slug'], 'Category.community_id' => $com)
                                    ));
                                 
                                    if($checkcommunitycategory==0){
                                        $communityparent = $this->Category->find('first', array(
                                'conditions' => array('Category.id' => $this->request->data['Category']['parent_id'])
                                    ));
                                         
                                        $data = array(
                                            'slug' => $communityparent['Category']['slug'],
                                            'name' =>  $communityparent['Category']['name'],
                                            'community_id' => $com,
                                            'parent_id' => 0
                                    );
                                        $this->Category->create();
                                    $this->Category->save($data);
                                    $id1 = $this->Category->getLastInsertID();
                                    
                                    if(!empty($id1)){
                                     $data1 = array(
                                            'slug' => $this->request->data['Category']['slug'],
                                            'name' =>  $this->request->data['Category']['name'],
                                            'community_id' => $com,
                                            'parent_id' => $id1                                        
                                    );
                                     $this->Category->create();
                                    $this->Category->save($data1);
                                    $id2 = $this->Category->getLastInsertID();
                                    
                                    }
                                    }
                                    else{
                                        
                                        $checkcommunitycategorydet = $this->Category->find('first', array(
                                'conditions' => array('Category.slug' => $getcomcat['Category']['slug'], 'Category.community_id' => $com)
                                    ));
                                         $this->Category->create();
                                    $r = $this->Category->save(array('Category' => array(
                                            'parent_id' => $checkcommunitycategorydet['Category']['id'],
                                            'name' => $this->request->data['Category']['name'],
                                            'slug' => $this->request->data['Category']['slug'],
                                            'community_id' => $com
                                    )));
                                    }
                                    }else{
                                    
                                    $this->Category->create();
                                    $r = $this->Category->save(array('Category' => array(
                                            'parent_id' => $this->request->data['Category']['parent_id'],
                                            'name' => $this->request->data['Category']['name'],
                                            'slug' => $this->request->data['Category']['slug'],
                                            'community_id' => $com
                                    )));
                                    }
                                }
                            }
                    $this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'index'));
                } else {
                    //$this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'add'));
                }
            }
        } else {
            
            $slug = strtolower(Inflector::slug($cat, $replacement = '-'));            
            $data = array(
                'Category' => array(
                    'slug' => $slug,
                    'name' =>  htmlspecialchars_decode($cat,ENT_QUOTES),
                    'community_id' => Configure::read('Community.id'),
                    'parent_id' => $parent_id
                )
            );
            if ($this->Category->save($data))
                $this->set('category', $this->Category->findBySlug($slug));
             
        }
         $currentlang=Configure::read('Config.language');
        if($currentlang=='es_ES'){
            $langvariable='Community.slug';
        }
        else{
            $langvariable='Community.name';
        }
        
        $communities = $this->Community->find('all', array(
            'conditions' => array('Community.deleted' => 0, 'Community.id != ' => Configure::read('Community.id')),
            'order' => array($langvariable => 'asc'),
                )
        );
        $this->set(compact('communities'));
    }

    public function delete($id) {
        $this->Category->delete($id);
        $this->Category->deleteAll(array('Category.parent_id' => $id));
        $this->redirect(array('community' => Configure::read('Community.slug'), 'controller' => 'categories', 'action' => 'index'));
    }

    public function cloud($model = 'Archive', $submodel = 'event') {
        $this->set('categories', $this->Category->getCloudCategories($model, $submodel));
        $this->set('model', $model);
    }
    public function listdata($id=null,$typ=null,$child_ids=null){ 
        
        $breadCrumbData = array();
        if(!empty($typ) && !empty($id)){
            $categoryList = Configure::read('categories_list');
            
            // For Parent Element
            if($typ == 'parent'){
                $i = 0;
                foreach ($categoryList as $category){
                    if($category['Category']['id'] == $id){
                        
                        $child_ids = '';
                        if(isset($category['children'])&& count($category['children']) > 0){
                            foreach($category['children'] as $child) { 
                                $child_ids .=$child['Category']['id'].",";
                            }
                        }
                        
                        $breadCrumbData[$i]['text'] = $category['Category']['name'];
                        $breadCrumbData[$i]['link']['community'] = Configure::read('Community.slug');
                        $breadCrumbData[$i]['link']['controller'] = 'categories';
                        $breadCrumbData[$i]['link']['action'] = 'listdata';
                        array_push($breadCrumbData[$i]['link'], $category['Category']['id'], "parent", $child_ids);
                        $i++;
                        break 1;
                    }
                }
                
            }else{
                $i = 1;
                $parentId = '';
                // For Child Element
                foreach ($categoryList as $category){
                    
                    if(!empty($category['children'])){
                        
                        foreach($category['children'] as $child) {
                            if($child['Category']['id'] == $id){
                                $parentId = $child['Category']['parent_id'];
                                $breadCrumbData[$i]['text'] = $child['Category']['name'];
                                $breadCrumbData[$i]['link']['community'] = Configure::read('Community.slug');
                                $breadCrumbData[$i]['link']['controller'] = 'categories';
                                $breadCrumbData[$i]['link']['action'] = 'listdata';
                                array_push($breadCrumbData[$i]['link'], $child['Category']['id'], "child");
                                $i--;                                
                                break 2;
                            }
                        }    
                    }    
                }
                
                // For Parent Element
                foreach ($categoryList as $category){
                    if($category['Category']['id'] == $parentId){
                        
                        $child_ids = '';
                        if(isset($category['children'])&& count($category['children']) > 0){
                            foreach($category['children'] as $child) { 
                                $child_ids .=$child['Category']['id'].",";
                            }
                        }
                        
                        $breadCrumbData[$i]['text'] = $category['Category']['name'];
                        $breadCrumbData[$i]['link']['community'] = Configure::read('Community.slug');
                        $breadCrumbData[$i]['link']['controller'] = 'categories';
                        $breadCrumbData[$i]['link']['action'] = 'listdata';
                        array_push($breadCrumbData[$i]['link'], $category['Category']['id'], "parent", $child_ids);
                        break 1;
                    }
                }
            }
        }else{
            $this->redirect(Router::url('/', true).Configure::read('Community.slug'));
        }
        
        
        $ids = $id;
        if($typ == "parent"){      
            $ids .=','.$child_ids;
        }
                      
        $this->request->params['named']['cat'] = $ids;
        $articles = $this->FilterArticles->getArticles($this);
       
	$this->set(compact('articles'));               
        $archives = $this->FilterArchives->getArchives($this);
       
        $this->set(compact('archives'));        
      
        $contents = $this->FilterEvents->getContents($this);
	$this->set(compact('contents'));      
       
    	$topics = $this->FilterTopics->getTopics($this);
        $this->set(compact('topics'));  
        
        $auctions = $this->FilterLicitations->getLicitation($this);
        $this->set(compact('auctions'));  
        
        $rfqs = $this->FilterRequestQuotations->getRequestQuotation($this);
        $this->set(compact('rfqs'));  
        
        // added dated on 29-09-2015 for product       
        $has_products = $this->CommunitiesSetting->getSingle(Configure::read('Community.id'), 'has_products');
      
       $this->Content->bindModel(array('belongsTo' => array(
                                    'ContentsCategory' => array('foreignKey' => false,
                                    'type'=>'LEFT',
                                    'conditions' =>array('Content.id = ContentsCategory.content_id')
                                    ))));
            $ids=array();
            if($child_ids !=''){$child_ids=substr(($id.",".$child_ids),0,-1);
           
              $ids=explode(",",$child_ids); 
           }else{
                  array_push($ids, $id);
                  
           }
             $current_date=date('Y-m-d H:i:s');
            $this->paginate= array(
            'conditions' => array(
		'Content.community_id' => Configure::read('Community.id'),
		'Content.deleted' => '0',
		'Content.type' => 'product',
                'DATE_ADD(Content.created, INTERVAL 60 DAY)  >=' => $current_date,
                'ContentsCategory.category_id'=>$ids),
            'group' => 'Content.id',
            'order' => array('Content.rating' => 'DESC', 'Content.created' => 'DESC'),
            'limit' =>  120,
            'maxLimit' => 120
        );
        $contents = $this->Content->find("all",$this->paginate);
       
        if ($this->request->is('requested')) return $contents;
	    $this->set(compact('contents'));  
            
            $this->set('breadCrumbData', $breadCrumbData);
    }
    public function getConditions(){
        $and = array();
        // Visibility
        if (!Configure::read('User.isAdministrator')  && !Configure::read('Community.administrator')) {
            $or2 = array();
            // Public
            $or2["Content.visibility"] = array('-1', '300');
            // Follow owner
            $or2["AND"] = array(
                "Content.visibility" => '100',
                'Content.user_id IN (SELECT MyLeader.following_id FROM users_followings as MyLeader WHERE MyLeader.user_id = "'.Configure::read('User.id').'")'
            );

            // Groups membership
            $groups_ids = $controller->getGroupsIds();
            if (count($groups_ids) > 0) {
                $groups_ids = join('","', $groups_ids);
                $or2["AND"] = array(
                    'Content.visibility IN ("'.$groups_ids.'")'
                );
            }

            // Is the owner
            $or2["Content.user_id"] = Configure::read('User.id');
            $and = array("AND" => array("OR" => $or2,
                'Content.community_id' => Configure::read('Community.id'),
                'Content.deleted' => '0',
                'Content.type' => 'product'
            ));
        } else {
    		$and = array(
                'Content.community_id' => Configure::read('Community.id'),
                'Content.deleted' => '0',
                'Content.type' => 'product'
    		);
        }
        return $and;
    }
}