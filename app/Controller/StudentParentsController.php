<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'configuration.php');
require_once(ROOT . DS . 'app' . DS .'Vendor' . DS  . 'commbank_gatway' . DS . 'connection.php');

class StudentParentsController extends AppController {

    public $name = 'StudentParents';
	public $uses = array(
						'Student','Setting', 'School','Examination','MyExam', 'Question',
						'StudentAnswer', 'Answer', 'MyExamStudentAnswer', 'ExaminationType',
						'ExaminationCategory','StudentParent','EmailTemplate','Payment'
						);

    //public $uses = array('StudentParent','Student','Examination', 'MyExam', 'ExaminationCategory','MyExamStudentAnswer');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration($no_of_child = NULL) {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		  if(!empty($no_of_child)){
			  if($no_of_child == 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Child';
			  } else if($no_of_child > 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Children';
			  } else {
				   $no_of_child = '';
				   $childTag = '';
			  }
		  } else {
			   $no_of_child = '';
			   $childTag = '';
		  }
		  
		   $this->set(compact('examination_types', 'examination_categories','no_of_child','childTag'));
		  
		  
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 //pr($this->request->data);die;
			 //Parent registration
			 	if(empty(trim($this->request->data['StudentParent']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['StudentParent']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							
							
							
							if(empty(trim($this->request->data['StudentParent']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['StudentParent']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['StudentParent']['email']))){
								$email = $this->request->data['StudentParent']['email'];
								$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['StudentParent']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['StudentParent']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['StudentParent']['password'];
							$cpass = $this->request->data['StudentParent']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['StudentParent']['password'] != $this->request->data['StudentParent']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['StudentParent']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['StudentParent']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['StudentParent']['username'] = $this->request->data['StudentParent']['email'];
					$this->request->data['StudentParent']['password'] = md5($this->request->data['StudentParent']['password']);
					
					//send verfication mail
					$dataForMail['user_type'] = "StudentParent";
					$dataForMail['email'] = trim( $this->request->data['StudentParent']['email']);
					$dataForMail['full_name'] =  trim($this->request->data['StudentParent']['first_name'])." ". trim($this->request->data['StudentParent']['last_name']);
					$this->sendVerificationEmail($dataForMail);				 
					//send verfication mail+++
					
					
					$this->StudentParent->save($this->request->data);
					$lastInserId = $this->StudentParent->getLastInsertID();
					for($i =0; $i<$no_of_child; $i++){
						$studentData['Student']['student_parent_id'] = $lastInserId;
						$studentData['Student']['examination_category_id'] = $this->request->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->request->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->request->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->request->data['Student']['last_name'][$i];
						$studentData['Student']['status'] = 'active';
						$this->Student->create();
						$this->Student->save($studentData);
					}
					
					 $this->Session->setFlash(__('Your registration has been done successfully. Please check your inbox to verify you email .'), 'success');
					 $this->redirect(array('controller' => 'parent', 'action' => 'registration',$no_of_child));
				}
				
			 }
		 }
	 

	
	
    public function login() {
        $this->layout = 'home_layout';
		$this->_parentLoginRedirect();
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
				//print_r($this->request->data);
            //student login start
                $login = strtolower($this->request->data['StudentParent']['login']);
                $password = $this->request->data['StudentParent']['password'];
                $parentData = $this->StudentParent->getParentLoginData($login, $password);
				 
                if (empty(trim($this->request->data['StudentParent']['login']))) {
                    $ERROR = 1;
                    $this->set('loginEmailErr', 'Please enter username');
                }

                if (empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Please enter password');
                }

                if (empty($parentData) && !empty(trim($this->request->data['StudentParent']['password']))) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Invalid email or password');
                }
				
				if (!empty($parentData) && $parentData['StudentParent']['status'] != "active" ) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Your account is not active, Please contact Site Administrator');
                }
				
				if (!empty($parentData) && $parentData['StudentParent']['status'] == "unverified" ) {
                    $ERROR = 1;
                    $this->set('loginPassErr', 'Your account is not verified, Please check your email inbox and verify your account ');
                }
				
                if ($ERROR == 0) {
                    $this->UserConfiguration->setParentData($parentData);
                    
					$this->StudentParent->id = $parentData['StudentParent']['id'];
					//$lastdate = date("Y-m-d h:i:s");
					$lastdate = date("Y-m-d H:i:s", strtotime('+5 hour +30 minutes'));
					$this->StudentParent->saveField('last_login', $lastdate);
					
                    $this->Session->setFlash(__('You have logged in successfully.'), 'success');
                   // $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
                    $this->redirect(array('controller' => 'parent', 'action' => 'take_exam_list'));
                }
            } 
        }
    
	
	
	
	 public function add_more_children($no_of_child = NULL) {
				$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
				 if(!empty($no_of_child)){
			  if($no_of_child == 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Child';
			  } else if($no_of_child > 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Children';
			  } else {
				   $no_of_child = '';
				   $childTag = '';
			  }
		  } else {
			   $no_of_child = '';
			   $childTag = '';
		  }
		  
		   $this->set(compact('examination_types', 'examination_categories','no_of_child','childTag'));
				
		  	 if($this->request->is('post') || $this->request->is('put')){
				// pr($this->data);die;
					
					for($i =0; $i<$no_of_child; $i++){
						
						$studentData['Student']['student_parent_id'] = $parent_id; 
						$studentData['Student']['examination_category_id'] = $this->data['Student']['examination_category_id'][$i];
						
						$studentData['Student']['school_name'] = $this->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
						
					}
					
				    $this->Session->setFlash(__('Children has been added successfully.'), 'success');
				    $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
			 }
			 
			
		  
       }
	
	 public function family_profiles() {
				$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
				$no_of_child = count($parentData['Student']);
				 if(!empty($no_of_child) && $no_of_child != 0){
					  if($no_of_child == 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Child';
					  } else if($no_of_child > 1){
						  $no_of_child = $no_of_child;
						  $childTag = 'Children';
					  } else {
						   $no_of_child = '';
						   $childTag = '';
					  }
				  } else {
					   $no_of_child = '';
					   $childTag = '';
				  }
		  
		   $this->set(compact('no_of_child','childTag'));
				
		  	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				// pr($this->data);die;
				    
					$this->StudentParent->id = $parent_id;
					$this->StudentParent->save($this->request->data);
					$i = 0;
					foreach($this->data['Student']['id'] as $id){ 
						$studentData['Student']['id'] = $id;
						$studentData['Student']['student_parent_id'] = $parent_id; 
						//$studentData['Student']['examination_category_id'] = $this->data['Student']['examination_category_id'][$i];
						unset($studentData['Student']['examination_category_id']);
						$studentData['Student']['school_name'] = $this->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->data['Student']['last_name'][$i];
						$this->Student->create();
						$this->Student->save($studentData);
						$i++;
					}
					
				    $this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
				    $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
			 }
			 
			 $this->data = $this->StudentParent->read(NULL, $parent_id);
		  
       }
	
	 public function take_exam_list() {
            $this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
		 
		 $this->MyExam->recursive = 2;
		 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
		 
		
		 $this->paginate = array(
							'conditions' => array(
												 'MyExam.isdeleted' => 0, 
												 'MyExam.payment_status' => 1, 
												 'MyExam.student_parent_id' => $parent_id
							 ),
							'limit' => 8,
							'order' => 'MyExam.id DESC'	,
							'group' => 'Examination.examination_category_id'
						);
       
	    $examinations = $this->paginate('MyExam');
		
		$this->set(compact('parentDetails', 'examinations'));
		
    }
	
	
	 public function fetch_exam_paper($examination_category_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$this->autoRender = false;
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			$conditions = array(
							 'MyExam.isdeleted' => 0, 
							 'MyExam.payment_status' => 1, 
							 'MyExam.student_parent_id' => $parent_id,
							 'Examination.examination_category_id' => $examination_category_id
							);
		
		   $MyExamAr = $this->MyExam->find('all', array('conditions' => $conditions, 'order' => 'Examination.paper ASC'));
		   return $MyExamAr;
		   
	    }
	
	 public function fetch_child($examination_category_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			//$this->layout = 'parent_layout';
			$this->autoRender = false;
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			
			$conditions = array(
							 'Student.isdeleted' => 0, 
							 'Student.payment_status' => 1, 
							 'Student.student_parent_id' => $parent_id,
							 'Student.examination_category_id' => $examination_category_id
							);
		
		   $childData = $this->Student->find('all', array('conditions' => $conditions, 'fields' => 'id, first_name, last_name'));
		   return $childData;
		  /* $childName = '';
		   if(!empty($childData)){
			   foreach($childData as $val){
				   $childName = $childName.', '.$val['Student']['first_name'];
			   }
		   } else {
			   $childName = '';
		   }
		   return ltrim($childName, ', ');*/
		   
	    }
	
	
	
	 public function is_exist_in_myexam($examination_id = NULL) {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			 $isMyexamPurchased = $this->MyExam->find('count', array(
														 'conditions' => array(
																'MyExam.examination_id' => $examination_id,
																'MyExam.student_parent_id' => $parent_id,
																'MyExam.payment_status' => 1,
																'MyExam.isdeleted' => 0,
																'MyExam.is_expired' => 0,
														 )));
			return $isMyexamPurchased;											 
		 }
	
	
	 public function exams_available($examination_category_id = NULL) {
        
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			$examination_category_id = $this->data_decrypt($examination_category_id);
		 //$this->Examination->recursive = 2;
		 //$this->ExaminationCategory->recursive = 2;
		// echo $student_id; 'Examination', 'MyExam', 'ExaminationCategory'
		 	/*$this->Examination->bindModel(array(
												'hasOne' => array(
													'MyExam' => array(
														'className' => 'MyExam',
														'foreignKey' => 'examination_id',
														 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1,'MyExam.is_expired' => 0, 'MyExam.student_parent_id' => $parent_id)
													),
												)

										));	*/
		 
		 /*$allExamAr = $this->Examination->find('all', array(
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));*/
																 
																 
																 
																 
			$ExamFlagArr38 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>38 
																 )));
					$ExamFlagArr39 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>39 
																 )));
					
					
					$ExamFlagArr42 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>42 
																 )));
					$ExamFlagArr43 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>43 
																 )));
					
					$flgAr38 = array();
					$flgAr39 = array();
					$flgAr42 = array();
					$flgAr43 = array();
					
					$examCatIds = array();
					
					foreach($ExamFlagArr38 as $key => $val){
						$flgAr38[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr39 as $key => $val){
						$flgAr39[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr42 as $key => $val){
						$flgAr42[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr43 as $key => $val){
						$flgAr43[] = $val['Examination']['exam_flag'];
					}
					
					
					if(in_array(2,$flgAr38)){
						$examCatIds[] = 38;
					} 
					
					if(in_array(2,$flgAr39)){
						$examCatIds[] = 39;
					}													 
					
					if(in_array(2,$flgAr42)){
						$examCatIds[] = 42;
					} 
					
					if(in_array(2,$flgAr43)){
						$examCatIds[] = 43;
					}	
																		 
				$allExamLiteracyArr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => $examCatIds
																 
																 )));												 
																 
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));			
																 
		//pr($allExamLiteracyArr);die;
		 $this->set(compact('parentDetails', 'allExamAr','allExamLiteracyArr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'Student Registration',
            'item_number' => str_pad($parentDetails['StudentParent']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'parent' => $parentDetails['StudentParent']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'student_parents/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'student_parents/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'student_parents/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	 public function checkout() {
			$this->UserConfiguration->isParentLoggedIn();
			$this->layout = 'parent_layout';
			$parentData = $this->UserConfiguration->getParentData();
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $parentData;
			 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'purchase_exam_payment') ){
			   $this->Session->write('MultipleExamPayment', $this->request->data);
			  // $this->redirect(array('controller' => 'students', 'action' => 'purchase_exam_payment' ));
			   $this->redirect(array('controller' => 'student_parents', 'action' => 'make_payment' ));
			   
		    }
		 
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'MyExam' => array(
                    'className' => 'MyExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1, 'MyExam.student_parent_id' => $parent_id)
                ),
            )

        ));	

				 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					// pr($this->data);die;
					 $subTotal = 0;
					 $allExamAr = $this->Examination->find('all', array(
																								 'conditions' => array(
																																	'Examination.id' => $this->request->data['Examination']['ids']
																																// 'Examination.isdeleted' => 0,
																																// 'Examination.examination_type_id' => 1
																																 ))); // examination_type_id =1 means online exam
					//pr($allExamAr);die;
					
					foreach($allExamAr as $val){
						$subTotal = $subTotal + $val['Examination']['price'];
					}
					
					$gst = (($subTotal * 10) / 100);
					$total = ($subTotal + $gst);
				 }
				 
		 $this->set(compact('parentDetails', 'allExamAr','subTotal','gst', 'total'));
		
    }
    
    public function payment_error(){
        //$this->UserConfiguration->isParentLoggedIn();
		$this->layout = 'parent_layout';
		//$parentData = $this->UserConfiguration->getParentData();
		//$parent_id = $parentData['StudentParent']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $parentDetails = $this->StudentParent->find('first', array('conditions' => array('StudentParent.id' => $parent_id)));
       // $this->set(compact('parentDetails'));
	 }
	 
	 public function purchase_exam_success(){
				//$this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				//$parentData = $this->UserConfiguration->getParentData();
				//$parent_id = $parentData['StudentParent']['id'];
			$this->Session->setFlash(__('Thank\'s for your payment.'), 'success');
        if($this->Session->check('StudentParent.transactionData')){
            
            $transactionData = $this->Session->read('StudentParent.transactionData');
            $this->Session->delete('StudentParent.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $student_id = $pagedata[0]; 
			 $student_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->Student->read(null, $payPalCustomData['student']);
            $this->MyExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->Student->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Students', 'action' => 'login'));
        }
        
    }
    
	
	function makePayment($DATA){

        $method = 'PUT' ;
        $customUri = "";
		if (array_key_exists("orderId", $DATA))
		  $customUri .= "/order/" . $DATA["orderId"];

		if (array_key_exists("transactionId", $DATA))
		  $customUri .= "/transaction/" . $DATA["transactionId"];

		$unsetNames = array("orderId", "transactionId", "creditsubmit","nameofcard");

		foreach ($unsetNames as $fieldName) {
		  if (array_key_exists($fieldName, $DATA))
		    unset($DATA[$fieldName]);
		}
		$merchantObj = new Merchant();
		$parserObj = new Parser($merchantObj);
		if (array_key_exists("version", $DATA)) {
		  $merchantObj->SetVersion($DATA["version"]);
		  unset($DATA["version"]);
		}
		$request = $parserObj->ParseRequest($DATA);
		if ($request == ""){
			return "not Getting any request!";
		}
		//  The below code for debugging for that enable Debug to true in configuration file
		/*if ($merchantObj->GetDebug())
        echo $request . "<br/><br/>";*/
        $requestUrl = $parserObj->FormRequestUrl($merchantObj, $customUri);
        $response = $parserObj->SendTransaction($merchantObj, $request, $method);
        return $response ;
  
     }
     
    function randomAsciiChar() {
        $randomKey = '';
        for($i=0;$i<7;$i++){
         $randomKey.= chr(rand(97,122));   
        }
       return $randomKey ; 
    }
     
    public function check3dsPayment($paymentData,$multipleExamIds,$totalPrice,$parentId,$lastpaymentId){
         Configure::write('debug', 2);
        //$multipleExamIds = base64_decode($multipleExamIds);
        $responseUrl = Router::url('/',true);
        $parentId = rawurlencode($parentId);
        $asciiCharacter = $this->randomAsciiChar();
        $request=array('apiOperation' => 'CHECK_3DS_ENROLLMENT',
                    'order.amount' => $paymentData['order']['amount'],
                    'order.currency' => $paymentData['order']['currency'],
                    'sourceOfFunds.provided.card.number' => $paymentData['sourceOfFunds']['provided']['card']['number'],
                    'sourceOfFunds.provided.card.expiry.month' => $paymentData['sourceOfFunds']['provided']['card']['expiry']['month'],
                    'sourceOfFunds.provided.card.expiry.year' => $paymentData['sourceOfFunds']['provided']['card']['expiry']['year'],
                    '3DSecureId' => $asciiCharacter,
                    '3DSecure.authenticationRedirect.pageGenerationMode' => 'SIMPLE',
                    '3DSecure.authenticationRedirect.responseUrl' => $responseUrl.'student_parents/process_authentication_result?secure_id='.$asciiCharacter.'&amount='.$paymentData['order']['amount'].'&orderId='.$paymentData['orderId'].'&transactionId='.$paymentData['transactionId'].'&card_number='.$paymentData['sourceOfFunds']['provided']['card']['number'].'&securityCode='.$paymentData['sourceOfFunds']['provided']['card']['securityCode'].'&expiry_month='.$paymentData['sourceOfFunds']['provided']['card']['expiry']['month'].'&expiry_year='.$paymentData['sourceOfFunds']['provided']['card']['expiry']['year'].'&payment_id='.$lastpaymentId,
                    'merchant' => 'LEALEACOM201',
                    'apiUsername' => 'merchant.LEALEACOM201',
                    'apiPassword' => 'e4df0f8cb115e0496d9bac3140a6f0aa'
                );
        $url = 'https://paymentgateway.commbank.com.au/api/nvp/version/45';         
        $query = http_build_query($request);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $query);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen( $query)));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
        curl_setopt($ch, CURLOPT_USERPWD, 'merchant.LEALEACOM201' . ":" . 'e4df0f8cb115e0496d9bac3140a6f0aa');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if (curl_error($ch))
          $response = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
        $values = array();
        $nv_strings = explode ('&', $response);
        foreach ($nv_strings as $s) {
            $nv = explode ('=', $s, 2);
            $name = urldecode ($nv[0]);
            $value = (isset ($nv[1]) ? urldecode($nv[1]) : null);
            $values[$name] = $value;
        }
        //pr($values); exit ;
        return $values ;
    }
     
    public function process_authentication_result(){
         Configure::write('debug', 2);
        $this->autoRender = false ;
        $this->layout = false ;
        
        $SecureId       = $_GET['secure_id'];
        $amount         = $_GET['amount'];
        $orderId        = $_GET['orderId'];
        $transactionId  = $_GET['transactionId'];
        $card_number    = $_GET['card_number'];
        $securityCode   = $_GET['securityCode'];
        $expiry_month   = $_GET['expiry_month'];
        $expiry_year    = $_GET['expiry_year'];
        $nameOnCard     = 'abc';
       // $multipleExamIds= rawurldecode($_GET['examids']);
       // $totalPrice = rawurldecode($_GET['totalPrice']);
        //$parent_id = rawurldecode($_GET['parent_id']);
        
        $paymentId = $_GET['payment_id'];
        
        $arPayment = $this->Payment->find('first',array('fields'=>array('id','loggedin_id','type','exam_ids','price'),'conditions'=>array('id'=>$paymentId,'isdeleted'=>0)));
        $multipleExamIds= $arPayment['Payment']['exam_ids'];
        $totalPrice= $arPayment['Payment']['price'];
        $parent_id= $arPayment['Payment']['loggedin_id'];
        $paymentPostArr = array();
        
        $request = array('apiOperation' => 'PROCESS_ACS_RESULT',
                      '3DSecure.paRes' => $_POST['PaRes'],
                      '3DSecureId' => $SecureId,
                      'merchant' => 'LEALEACOM201',
                      'apiUsername' => 'merchant.LEALEACOM201',
                      'apiPassword' => 'e4df0f8cb115e0496d9bac3140a6f0aa'
                );
        $url = 'https://paymentgateway.commbank.com.au/api/nvp/version/45';         
        $query = http_build_query($request);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  $query);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen( $query)));
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
        curl_setopt($ch, CURLOPT_USERPWD, 'merchant.LEALEACOM201' . ":" . 'e4df0f8cb115e0496d9bac3140a6f0aa');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if (curl_error($ch))
          $response = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);
         
        $values = array();
        $nv_strings = explode ('&', $response);
        foreach ($nv_strings as $s) {
            $nv = explode ('=', $s, 2);
            $name = urldecode ($nv[0]);
            $value = (isset ($nv[1]) ?$nv[1] : null);
            $values[$name] = $value;
        }
        
        if(array_key_exists('response.3DSecure.gatewayCode',$values)){
            
            if($values['response.3DSecure.gatewayCode']=='AUTHENTICATION_SUCCESSFUL' || $values['response.3DSecure.gatewayCode']=='AUTHENTICATION_ATTEMPTED' || $values['response.3DSecure.gatewayCode']=='AUTHENTICATION_NOT_AVAILABLE'){
                
                $paymentPostArr['3DSecureId']= $SecureId ;
                $paymentPostArr['order']['amount'] = $amount ;
                $paymentPostArr['order']['currency'] = 'AUD' ;
                $paymentPostArr['version'] = 34 ;
                $paymentPostArr['orderId'] = $orderId ;
                $paymentPostArr['transactionId'] = $transactionId ;
                $paymentPostArr['apiOperation'] = 'PAY' ;
                $paymentPostArr['sourceOfFunds']['type'] = 'CARD' ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['number'] = $card_number ; 
                $paymentPostArr['sourceOfFunds']['provided']['card']['expiry']['month'] = $expiry_month ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['expiry']['year'] = $expiry_year ;
                $paymentPostArr['sourceOfFunds']['provided']['card']['securityCode'] = $securityCode ;
                $paymentPostArr['nameofcard'] = $nameOnCard ;
                $paymentPostArr['creditsubmit'] = 'Submit' ;
                
                $response = $this->makePayment($paymentPostArr);	
					                    $responseArray = json_decode($response, TRUE);
                                        //pr($responseArray); exit ; 
                                        if ($responseArray == NULL){
                                            $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
                                        	$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
        									$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
                                        }else if(array_key_exists("result", $responseArray)){
                                            	$result = $responseArray["result"];
                                            	if ($result == "FAILURE") {
                                                    $errorMessage = "";
                                                    $tmpArray = array();
            
                                            		if (array_key_exists("reason", $responseArray)) {
            									    $tmpArray = $responseArray["reason"];
            
            									    if (array_key_exists("explanation", $tmpArray)) {
            									      $errorMessage = rawurldecode($tmpArray["explanation"]);
            									    }
            									    else if (array_key_exists("supportCode", $tmpArray)) {
            									      $errorMessage = rawurldecode($tmpArray["supportCode"]);
            									    }
            									    else {
            									      $errorMessage = "Reason unspecified.";
            									    }
            									    
            									  }else{
            									      $errorMessage = "Reason unspecified.";
            									  }
                                            	
            										$this->Session->setFlash($errorMessage);
            										$this->set('payment_error', $errorMessage);
            										$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));										
            
                                            	}else if($result == "ERROR"){
                                            		$errorMessage = "";
                                                    $tmpArray = array();
            	                                	if (array_key_exists("error", $responseArray)) {
            										    $tmpArray = $responseArray["error"];
            										    if (array_key_exists("explanation", $tmpArray)) {
            										     $errorMessage = rawurldecode($tmpArray["explanation"]);
            										    }
            										    else if (array_key_exists("cause", $tmpArray)) {
            										     $errorMessage = rawurldecode($tmpArray["supportCode"]);
            										    }
            										    else{
            										     $errorMessage = "Reason unspecified.";
            										    }
            									    }                                	
            										$this->Session->setFlash($errorMessage);
            										$this->set('payment_error', $errorMessage);
            										$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
                                            	}else if($result == "SUCCESS"){
                                            	    
                                					//$parentData = $this->UserConfiguration->getParentData();
                                				//	$parent_id  = $parentData['StudentParent']['id'];
                                					//echo $parent_id ; exit ;
                                					$MultipleExamPayment = explode('-',$multipleExamIds) ;
                                					$total_price = explode('-',$totalPrice) ;
                                					//$gross_total = $MultipleExamPayment['gross_total'];
                                                    //pr($MultipleExamPayment); exit ;
                                            		$dateTime = new DateTime('NOW');
            										$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            										$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;
            
            										$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            										$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            
                                            		$datas = explode('-',$multipleExamIds);
            										$i = 0;
            										foreach($datas as $exam_id){
            											$this->request->data['MyExam']['student_id'] = 0;
            											$this->request->data['MyExam']['student_parent_id'] = $parent_id;
            											$this->request->data['MyExam']['examination_id'] =  $exam_id;
            											$this->request->data['MyExam']['payment_status'] = 1;
            											$this->request->data['MyExam']['order_id'] = $responseArray["order"]["id"];
            											$this->request->data['MyExam']['txn_id'] = $responseArray["transaction"]["id"];
            											$this->request->data['MyExam']['amount'] =  $total_price[$i];
            											
            											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
            											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
            											
            											$this->MyExam->create();
            											$this->MyExam->save($this->request->data);
            											$i++;
            							         	}
            										
            										$htmlList = '<table style="width:100%;">
            										   <tr style="background: #1f3446; color:#fff;">
            										   <th style="padding:8px;">List</th>
            										   <th style="padding:8px;">Year</th>
            										   <th style="padding:8px;">Exam Name</th>
            										   <th style="padding:8px;">Category</th>
            										   <th style="padding:8px;">Price ($ AUD)</th>
            										   <th style="padding:8px;">Total Paid</th>
            									    </tr>';
            										$j = 0;
            										$k = 1;
            					     	            foreach($datas as $exam_id){
            									    $examDtl = $this->Examination->find('first', array('conditions' => array(
            																									'Examination.id' => $exam_id
            																							)));
            										$category = explode(' ',$examDtl['ExaminationCategory']['name']);
            										$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
            										$inserted = array();
            										$inserted[] = 'Year '.$category[1];
            										array_splice( $examTitle, 1, 0, $inserted );
            								        $examName = implode(' ', $examTitle);	
            								        $paper = $examDtl['Examination']['paper'];					   
            										$exam_flag = $examDtl['Examination']['exam_flag'];	
														if($exam_flag == 2){
															$exname = 'Literacy';
														} else {
															$exname = 'Numeracy';
														}	

													
            									    $price_per_student = $examDtl['Examination']['price'];
            									    //$examName = $examDtl['Examination']['title'];
            								        $htmlList .= '<tr style="text-align:center; background:#EFEBE6">
            											   <td style="padding:8px;">'.$k.'</td>
            											   <td style="padding:8px;">'.$category[1].'</td>
            											   <td style="padding:8px;">'.$exname.' '.$paper.'</td>
            											   <td style="padding:8px;">NAPLAN Style Practice</td>
            											   <td style="padding:8px;">'.$price_per_student.'</td>
            											   
            											   <td style="padding:8px;">'.($total_price[$j] + ($total_price[$j] *(10/100))).'</td>
            											 </tr>';
            										$j++;
            										$k++;					
            								       }
            												
            										$htmlList .= '</table>';
            														
            										$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
            										$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
            										$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 13)));
            										$html_content = $mailTemplateData['EmailTemplate']['content'];
            										$validity = $examValidity; //days will come from admin
            										$arStudentParent = $this->StudentParent->find('first',array('fields'=>array('StudentParent.first_name','StudentParent.last_name','StudentParent.email'),'conditions'=>array('StudentParent.id'=>$parent_id)));
            										$full_name = $arStudentParent['StudentParent']['first_name'].' '.$arStudentParent['StudentParent']['last_name'];
            					
            										$html = '';
            										$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
            										$html = str_replace('{{full_name}}', $full_name, $html);
            										$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
            										$html = str_replace('{{validity}}', $validity, $html);
            										$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);
            										
            									   //pr($html);die;
            										$email_subject = $mailTemplateData['EmailTemplate']['title'];
            										$email = $arStudentParent['StudentParent']['email'];
            					                    //$email = 'dinesh.amstech@gmail.com';
            										$mailFrom = 'sales@learnerleader.com';
            										$FromName = 'Learner Leader Online';
            					                   
            									    $mail = new PHPMailer;
            									    $mail->FromName = $FromName;
            					                    $mail->From = $mailFrom;
            					                    $mail->Subject = $email_subject;
            					                    $mail->Body = stripslashes($html);
            					                    $mail->AltBody = stripslashes($html);
            					                    $mail->IsHTML(true);
            					                    $mail->AddAddress($email);
            					                   // $mail->AddAddress('amarjit.brainium@gmail.com');
            					                    $mail->Send();
            													
            											
            											
            											
            									   $this->redirect(array('controller'=>'student_parents','action'=>'purchase_exam_success'));
            
            
            
                                            	}
                    }  
                
                
                
                
                
            }else{
              $this->Session->setFlash("AUTHENTICATION_FAILED. Please try again");
              $this->set('payment_error', "Internal Error. Processing timeout. Please try again");
              $this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));   
            }
            
        }else{
           $this->Session->setFlash("Internal Error. Processing timeout. Please try again");
           $this->set('payment_error', "Internal Error. Processing timeout. Please try again");
           $this->redirect(array('controller'=>'student_parents','action'=>'payment_error')); 
        }
         
         
     }
     
	public function make_payment($examination_id = NULL){
	                Configure::write('debug', 2);
	                $this->UserConfiguration->isParentLoggedIn();
					$this->layout = 'parent_layout';
					$parentData = $this->UserConfiguration->getParentData();
					//pr($parentData); exit;
					$parent_id = $parentData['StudentParent']['id'];
					
					$MultipleExamPayment = $this->Session->read('MultipleExamPayment');
					//echo $parent_id ;
					//pr($MultipleExamPayment); exit ;
					$gross_total = $MultipleExamPayment['gross_total'];
					$ERROR = 0;  
                    $multipleExamIds = implode('-',$MultipleExamPayment['ids']) ;
                    //$multipleExamIds = rawurlencode($multipleExamIds);
                    
                    $totalPrice = implode('-',$MultipleExamPayment['total_price']) ;
                    //$totalPrice = rawurlencode($totalPrice);
                    //pr($MultipleExamPayment); exit ;

					if($this->request->is('post') || $this->request->is('put'))
					{
							
                           // pr($this->request->data); exit;                           

							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['number']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])) || empty(trim($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['sourceOfFunds']['provided']['card']['securityCode']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['nameofcard']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['year'])){
								$this->set('year','Please select year');
							}
							
							if(!empty($this->request->data['sourceOfFunds']['provided']['card']['expiry']['month'])){
								$this->set('exp_month', 'Please select month');
							}	

							//payment Start from here

					        if($ERROR == 0)
					        {		
					            $paymentOptions = $this->Payment->set(array('loggedin_id'=>$parent_id,'type'=>'Parent','exam_ids'=>$multipleExamIds,'price'=>$totalPrice));
					            $this->Payment->save($paymentOptions,false);
					            $lastpaymentId = $this->Payment->getLastInsertId();
					            $is3ds = $this->check3dsPayment($_POST,$multipleExamIds,$totalPrice,$parent_id,$lastpaymentId);
					           
					            if(array_key_exists('response.3DSecure.gatewayCode',$is3ds)){
					                 if($is3ds['response.3DSecure.gatewayCode']=='CARD_ENROLLED'){
					                     
					                     echo $is3ds['3DSecure.authenticationRedirect.simple.htmlBodyContent']; exit ;
					                     
					                 }else if($is3ds['response.3DSecure.gatewayCode']=='CARD_DOES_NOT SUPPORT_3DS'){
					                      $this->Session->setFlash("The card does not support 3DS, and you cannot proceed with 3DS authentication.");
                                          $this->set('payment_error', 'The card does not support 3DS, and you cannot proceed with 3DS authentication.');
        								  $this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
					                 }
					                 else{
					                    if(!empty($is3ds['3DSecureId'])){
					                       $_POST['3DSecureID'] = $is3ds['3DSecureId'];
					                    } 
					                    $response = $this->makePayment($_POST);	
					                    $responseArray = json_decode($response, TRUE);
					                    
                                        if ($responseArray == NULL){
                                            $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
                                        	$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
        									$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
                                        }else if(array_key_exists("result", $responseArray)){
                                            	$result = $responseArray["result"];
                                            	if ($result == "FAILURE") {
                                                    $errorMessage = "";
                                                    $tmpArray = array();
            
                                            		if (array_key_exists("reason", $responseArray)) {
            									    $tmpArray = $responseArray["reason"];
            
            									    if (array_key_exists("explanation", $tmpArray)) {
            									      $errorMessage = rawurldecode($tmpArray["explanation"]);
            									    }
            									    else if (array_key_exists("supportCode", $tmpArray)) {
            									      $errorMessage = rawurldecode($tmpArray["supportCode"]);
            									    }
            									    else {
            									      $errorMessage = "Reason unspecified.";
            									    }
            
            									    // if (array_key_exists("code", $tmpArray)) {
            									    //   $errorCode = "Error (" . $tmpArray["code"] . ")";
            									    // }
            									    // else {
            									    //   $errorCode = "Error (UNSPECIFIED)";
            									    // }
            									  }else{
            									      $errorMessage = "Reason unspecified.";
            									  }
                                            	
            										$this->Session->setFlash($errorMessage);
            										$this->set('payment_error', $errorMessage);
            										$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));										
            
                                            	}else if($result == "ERROR"){
                                            		$errorMessage = "";
                                                    $tmpArray = array();
            	                                	if (array_key_exists("error", $responseArray)) {
            										    $tmpArray = $responseArray["error"];
            										    if (array_key_exists("explanation", $tmpArray)) {
            										     $errorMessage = rawurldecode($tmpArray["explanation"]);
            										    }
            										    else if (array_key_exists("cause", $tmpArray)) {
            										     $errorMessage = rawurldecode($tmpArray["supportCode"]);
            										    }
            										    else{
            										     $errorMessage = "Reason unspecified.";
            										    }
            									    }                                	
            										$this->Session->setFlash($errorMessage);
            										$this->set('payment_error', $errorMessage);
            										$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
                                            	}else if($result == "SUCCESS"){
            
                                            		$dateTime = new DateTime('NOW');
            										$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            										$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;
            
            										$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            										$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            
                                            		$datas = $MultipleExamPayment['ids'];
            										$i = 0;
            										foreach($datas as $exam_id){
            											$this->request->data['MyExam']['student_id'] = 0;
            											$this->request->data['MyExam']['student_parent_id'] = $parent_id;
            											$this->request->data['MyExam']['examination_id'] =  $exam_id;
            											$this->request->data['MyExam']['payment_status'] = 1;
            											$this->request->data['MyExam']['order_id'] = $responseArray["order"]["id"];
            											$this->request->data['MyExam']['txn_id'] = $responseArray["transaction"]["id"];
            											$this->request->data['MyExam']['amount'] =  $MultipleExamPayment['total_price'][$i];
            											
            											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
            											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
            											
            											$this->MyExam->create();
            											$this->MyExam->save($this->request->data);
            											$i++;
            							         	}
            										
            										$htmlList = '<table style="width:100%;">
            										   <tr style="background: #1f3446; color:#fff;">
            										   <th style="padding:8px;">List</th>
            										   <th style="padding:8px;">Year</th>
            										   <th style="padding:8px;">Exam Name</th>
            										   <th style="padding:8px;">Category</th>
            										   <th style="padding:8px;">Price ($ AUD)</th>
            										   <th style="padding:8px;">Total Paid</th>
            									    </tr>';
            										$j = 0;
            										$k = 1;
            					     	            foreach($datas as $exam_id){
													$examDtl = $this->Examination->find('first', array(
													'conditions' => array(
																		'Examination.id' => $exam_id
																)));
            										$category = explode(' ',$examDtl['ExaminationCategory']['name']);
            										$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
            										$inserted = array();
            										$inserted[] = 'Year '.$category[1];
            										array_splice( $examTitle, 1, 0, $inserted );
            								        $examName = implode(' ', $examTitle);	
            								        $paper = $examDtl['Examination']['paper'];					   
            										$exam_flag = $examDtl['Examination']['exam_flag'];	
														if($exam_flag == 2){
															$exname = 'Literacy';
														} else {
															$exname = 'Numeracy';
														}											
            									    $price_per_student = $examDtl['Examination']['price'];
            									    //$examName = $examDtl['Examination']['title'];
            								        $htmlList .= '<tr style="text-align:center; background:#EFEBE6">
            											   <td style="padding:8px;">'.$k.'</td>
            											   <td style="padding:8px;">'.$category[1].'</td>
            											   <td style="padding:8px;">'.$exname.' '.$paper.'</td>
            											   <td style="padding:8px;">NAPLAN Style Practice</td>
            											   <td style="padding:8px;">'.$price_per_student.'</td>
            											   
            											   <td style="padding:8px;">'.($MultipleExamPayment["total_price"][$j] + ($MultipleExamPayment["total_price"][$j] *(10/100))).'</td>
            											 </tr>';
            										$j++;
            										$k++;					
            								       }
            												
            										$htmlList .= '</table>';
            														
            										$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
            										$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
            										$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 13)));
            										$html_content = $mailTemplateData['EmailTemplate']['content'];
            										$validity = $examValidity; //days will come from admin
            										$full_name = $parentData['StudentParent']['first_name'].' '.$parentData['StudentParent']['last_name'];
            					
            										$html = '';
            										$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
            										$html = str_replace('{{full_name}}', $full_name, $html);
            										$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
            										$html = str_replace('{{validity}}', $validity, $html);
            										$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);
            										
            									   //pr($html);die;
            										$email_subject = $mailTemplateData['EmailTemplate']['title'];
            										$email = $parentData['StudentParent']['email'];
            					                    //$email = 'dinesh.amstech@gmail.com';
            										$mailFrom = 'sales@learnerleader.com';
            										$FromName = 'Learner Leader Online';
            					                   
            									    $mail = new PHPMailer;
            									    $mail->FromName = $FromName;
            					                    $mail->From = $mailFrom;
            					                    $mail->Subject = $email_subject;
            					                    $mail->Body = stripslashes($html);
            					                    $mail->AltBody = stripslashes($html);
            					                    $mail->IsHTML(true);
            					                    $mail->AddAddress($email);
            					                    //$mail->AddAddress('amarjit.brainium@gmail.com');
            					                    $mail->Send();
            													
            											
            											
            											
            									   $this->redirect(array('controller'=>'student_parents','action'=>'purchase_exam_success'));
            
            
            
                                            	}
                                            }   
					                 }
					            }else{
					                $this->Session->setFlash("Internal Error. Processing timeout. Please try again.");
                                	$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
									$this->redirect(array('controller'=>'student_parents','action'=>'payment_error'));
					            }
					            
					            
					           
					        	
							
				    }
							///payment End from here
				}

				   $transactionId = "Trans".md5(uniqid(rand(), true))  ;
				   $orderId       = "Order".md5(uniqid(rand(), true))  ;
                   $transOrderId = array(
                          'transactionId'=>$transactionId,
                          'orderId'=>$orderId
                   	);

						
					$paypalFormSettings = array(
				            'payment_url' => 'https://www.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
				            'cmd' => '_xclick',
				            'business' => 'sngpublishing@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
				            //'business' => 'paypal-facilitator@surfernet.com',
				           // 'item_name' => 'Student Registration',
				            'item_number' => str_pad($parentData['StudentParent']['id'], 10, "0", STR_PAD_LEFT),
				           // 'amount' => $paymentAmount,
				            'quantity' => 1,
				            'currency_code' => 'AUD'
				    );

			        $customdata = array(
			            'parent_id' => $parentData['StudentParent']['id'],
			            'examination_id' => @$examination_id,
			            'payment_for' => 'Purchase Exam',
			           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
			           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
			        );         

			        $custom = null;
			        foreach ($customdata as $key => $data) {

			            if (!empty($custom)) {
			                $custom .= "";
			            }
			            $custom .= "##" . $data;
			        }
		
					$examID = null;
		            $datas = $MultipleExamPayment['ids'];
					foreach($datas as $exam_id){
						if (!empty($examID)) {
							$examID .= "";
						}
						$examID .= "@@@" . $exam_id;
					}
		
		            $eachTotalPrice = null;
		            $total_price = $MultipleExamPayment['total_price'];
					foreach($total_price as $val){
						if (!empty($eachTotalPrice)) {
							$eachTotalPrice .= "";
						}
						$eachTotalPrice .= "**" . $val;
					}
		
		// $custom1 = trim($examID, '#').'||'.trim($noOfstudents, '@').'||'.trim($eachTotalPrice, '**').'||'.$SchoolDetails['School']['id'];
		 $custom1 = trim($examID, '@@@').'||'.trim($eachTotalPrice, '**').'||'.$parentData['StudentParent']['id'];
		
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        //$paypalFormSettings['item_name'] = @$title; //Router::url('/', true)
        $paypalFormSettings['item_name'] = 'Purchase Practice Exam(s)';
        //$paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['custom'] = $custom1;
        //$paypalFormSettings['amount'] = @$amount; //Router::url('/', true)
        $paypalFormSettings['amount'] = $gross_total; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'student_parents/payment_success/'.$custom1;
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'student_parents/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'student_parents/payment_cancel'. '/' . $custom;
		$this->set(compact('studentDetails', 'examDtls','paypalFormSettings', 'gross_total','transOrderId'));
					
     }
    
	
	 /* Start Payment functions by paypal*/
	 
	    public function payment_success($custom = NULL) {
			$this->layout = false;
			$parentData = $this->UserConfiguration->getParentData();
			//$parent_id = $parentData['StudentParent']['id'];
			//if ($this->request->is('post') || $this->request->is('put')) {
				if (!empty($custom)) {
				   //$customString = explode('||', $this->request->data['custom']);
				   $customString = explode('||', $custom);
					$ExamIdsAr = explode('@@@', $customString[0]);
					$eachTotalPriceAr = explode('**', $customString[1]);
					$parent_id = $customString[2];
				
				$dateTime = new DateTime('NOW');
				$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
				$examValidity = (!empty($this->settingData['Setting']['parent_student_exam_validity']))?$this->settingData['Setting']['parent_student_exam_validity']:30;

				$endDate = strtotime ( '+'.$examValidity.' days' , strtotime ( $subscriptionStart ) ) ;
				$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
			
            //if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
				if (!empty($custom)) {
										$i = 0;
										foreach($ExamIdsAr as $examId){
											$this->request->data['MyExam']['student_id'] = 0;
											
											$this->request->data['MyExam']['student_parent_id'] = $parent_id;
											$this->request->data['MyExam']['examination_id'] = $examId;
											$this->request->data['MyExam']['payment_status'] = 1;
											//$this->request->data['MyExam']['txn_id'] = $this->request->data['txn_id'];
											$this->request->data['MyExam']['txn_id'] = $this->random_value(17);
											$this->request->data['MyExam']['order_id'] = $this->random_value(20);;
											$this->request->data['MyExam']['amount'] = $eachTotalPriceAr[$i];
											
											$this->request->data['MyExam']['start_subscription'] = $subscriptionStart;
											$this->request->data['MyExam']['end_subscription'] = $subscriptionEnd;
											
											$this->MyExam->create();
											$this->MyExam->save($this->request->data);
												$i++;
										}
							
							   $this->Session->write('StudentParent.transactionData', $this->request->data);
                              //$this->redirect(array('controller' => 'student_parents', 'action' => 'purchase_exam_success'));
							  
								$htmlList = '<table style="width:100%;">
								   <tr style="background: #1f3446; color:#fff;">
								   <th style="padding:8px;">List</th>
								   <th style="padding:8px;">Year</th>
								   <th style="padding:8px;">Exam Name</th>
								   <th style="padding:8px;">Category</th>
								   <th style="padding:8px;">Price ($ AUD)</th>
								   <th style="padding:8px;">Total Paid</th>
								</tr>';
								$j = 0;
								$k = 1;
								foreach($ExamIdsAr as $exam_id){
								$examDtl = $this->Examination->find('first', array('conditions' => array(
																							'Examination.id' => $exam_id
																					)));
								$category = explode(' ',$examDtl['ExaminationCategory']['name']);
								$examTitle = explode(' ',ucwords(strtolower($examDtl['Examination']['title'])));
								$inserted = array();
								$inserted[] = 'Year '.$category[1];
								array_splice( $examTitle, 1, 0, $inserted );
								$examName = implode(' ', $examTitle);	
								$paper = $examDtl['Examination']['paper'];					   
								$exam_flag = $examDtl['Examination']['exam_flag'];	
								if($exam_flag == 2){
									$exname = 'Literacy';
								} else {
									$exname = 'Numeracy';
								}	
	

									
								$price_per_student = $examDtl['Examination']['price'];
								//$examName = $examDtl['Examination']['title'];
								$htmlList .= '<tr style="text-align:center; background:#EFEBE6">
									   <td style="padding:8px;">'.$k.'</td>
									   <td style="padding:8px;">'.$category[1].'</td>
									   <td style="padding:8px;">'.$exname.' '.$paper.'</td>
									   <td style="padding:8px;">NAPLAN Style Practice</td>
									   <td style="padding:8px;">'.$price_per_student.'</td>
									   
									   <td style="padding:8px;">'.($eachTotalPriceAr[$j] + ($eachTotalPriceAr[$j] *(10/100))).'</td>
									 </tr>';
								$j++;
								$k++;					
								}
										
								$htmlList .= '</table>';
												
								$host = 'https://'.$_SERVER['HTTP_HOST'].$this->webroot.'naplan_exam/';
								$naplan_exam = '<a href="'.$host.'">Naplan exam</a>';
								$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 13)));
								$html_content = $mailTemplateData['EmailTemplate']['content'];
								$validity = $examValidity; //days will come from admin
								$full_name = $parentData['StudentParent']['first_name'].' '.$parentData['StudentParent']['last_name'];

								$html = '';
								$html.=str_replace('{{naplan_exam}}', $naplan_exam, $html_content);
								$html = str_replace('{{full_name}}', $full_name, $html);
								$html = str_replace('{{DATE_OF_PURCHASE}}', date('Y/m/d'), $html);
								$html = str_replace('{{validity}}', $validity, $html);
								$html = str_replace('{{exam_purchase_list_table}}', $htmlList, $html);

								//pr($html);die;
								$email_subject = $mailTemplateData['EmailTemplate']['title'];
								$email = $parentData['StudentParent']['email'];
								//$email = 'dinesh.amstech@gmail.com';
								$mailFrom = 'sales@learnerleader.com';
								$FromName = 'Learner Leader Online';

								$mail = new PHPMailer;
								$mail->FromName = $FromName;
								$mail->From = $mailFrom;
								$mail->Subject = $email_subject;
								$mail->Body = stripslashes($html);
								$mail->AltBody = stripslashes($html);
								$mail->IsHTML(true);
								$mail->AddAddress($email);
								//$mail->AddAddress('dinesh.amstech@gmail.com');
								$mail->Send();
								$this->redirect(array('controller'=>'student_parents','action'=>'purchase_exam_success'));
													  
													  
													  
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
	
	
	 
	
	//exam start
	 public function take_exam_save() {
        $this->UserConfiguration->isParentLoggedIn();
		$this->layout = 'parent_layout';
		$parentData = $this->UserConfiguration->getParentData();
		$parent_id = $parentData['StudentParent']['id'];
		
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		 
		 $RandomID = $this->Session->read('RandomID');
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 $student_id = $this->request->data['student_id'];
			$QuestionLimit =  $this->request->data['totalQuestionSet'];
			$exam_section_id =  $this->request->data['exam_section_id'];
			
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.student_ans_random_id' => $RandomID,
																																		'MyExamStudentAnswer.my_exam_id' => $this->request->data['my_exam_id'],
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
					$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else if($flagQs != ' ' && $flagQs == 'NON-CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1; //1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 0; //0 for other
					}
					
					$exam_section_id =  $this->request->data['exam_section_id'];
					 if($exam_section_id != ' ' && $exam_section_id == 1){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 3; //3 Reading Booklet and Questions
					}  
					
					if($exam_section_id != ' ' && $exam_section_id == 2){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 4; //4 Language Conventions
					}
					
					
					
				if(empty($MyExamStudentAnswerExist)){
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_parent_id'] = $parent_id;
					$tsq = !empty($this->request->data['totalQuestionSet']) ? $this->request->data['totalQuestionSet'] :$this->Session->read('totalQuestions');
					//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $tsq;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				}else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$tsq = !empty($this->request->data['totalQuestionSet']) ? $this->request->data['totalQuestionSet'] :$this->Session->read('totalQuestions');
						//$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + $this->Session->read('totalQuestions');
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + $tsq;
						$this->MyExamStudentAnswer->save($data);
					}
			 
			
			/*$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
			$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
			$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
			$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
			$this->MyExamStudentAnswer->save($myExamStudentAns);*/
			
			
			 //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
				  $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
				  $this->request->data['StudentAnswer']['student_id'] = $student_id;
				   $this->request->data['StudentAnswer']['question_id'] = $question_id;
				    $this->request->data['StudentAnswer']['answer_id'] = $answer_id;
					 $this->request->data['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($this->request->data);
				}
			}
			
			
		//data insert which answer is not given	
		$questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
							
							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			// die;
			$this->Session->setFlash('Data has been submitted successfully.');
			//$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			$this->redirect(array('controller' => 'parent', 'action' => 'take_exam_list'));
			
			/*if($this->request->data['page_action'] == 'take_exam' && $this->request->data['exam_flag'] == 'SingleExam' ){ 
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			}
			else if($this->request->data['page_action'] == 'take_exam_second_section'){
					$this->redirect(array('controller' => 'students', 'action' => 'view_answer'));
			} else {
					$this->redirect(array('controller' => 'students', 'action' => 'second_section_exam'));
			}
			*/
			   
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$this->UserConfiguration->isParentLoggedIn();
		//$this->layout = 'parent_layout';
		$parentData = $this->UserConfiguration->getParentData();
		$parent_id = $parentData['StudentParent']['id'];
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 
			        $student_id = $this->request->data['student_id'];
					$flagQs = $this->Session->read('flag');
					if($flagQs != ' ' && $flagQs == 'CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 2; //2 for calculator, 1 for non-calculator
					} else if($flagQs != ' ' && $flagQs == 'NON-CALCULATOR'){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 1; //1 for non-calculator
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 0; //0 for other
					}
					
					$exam_section_id =  $this->request->data['exam_section_id'];
					 if($exam_section_id != ' ' && $exam_section_id == 1){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 3; //3 Reading Booklet and Questions
					}  
					
					if($exam_section_id != ' ' && $exam_section_id == 2){
						$myExamStudentAns['MyExamStudentAnswer']['question_section'] = 4; //4 Language Conventions
					}
					
					
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					$myExamStudentAns['MyExamStudentAnswer']['student_parent_id'] = $parent_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					//pr($myExamStudentAns);die;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
					
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
					   $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
				     
					 $this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
					  $this->request->data['StudentAnswer']['student_id'] = $student_id;
					   $this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
					$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));
					$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];

					$questionData['StudentAnswer']['student_id'] = $student_id;
					$questionData['StudentAnswer']['question_id'] = $qid;
					$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
					$questionData['StudentAnswer']['random_id'] = $RandomID;
					$this->StudentAnswer->create();
					$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	  
	
	 public function take_exam($examination_id = NULL, $my_exam_id = NULL,$flag = NULL,$examFlag = NULL,$studentID = NULL,$exam_section_id = NULL) {
		$this->UserConfiguration->isParentLoggedIn();
		//$this->layout = 'parent_layout';
		$parentData = $this->UserConfiguration->getParentData();
		$parent_id = $parentData['StudentParent']['id'];
		$studentid = $this->data_decrypt($studentID);
		
		//$this->layout = 'parent_exam_layout';
		$this->layout = 'parent_exam_layout_special';
		
		$flag = $this->data_decrypt($flag);
		$examFlag = $this->data_decrypt($examFlag);
		$this->Session->write('flag', $flag);
		//$this->Session->write('student_id', $studentid);
		
		//exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		
		 $settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		 $settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
			
		if($flag == 'NON-CALCULATOR'){
			$section = 'exam_section_2_name';
			//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
			$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
			$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			if($exam_section_id == 1){
						 $examDuration = 65;
					 } else if($exam_section_id == 2 || $exam_section_id == 3){
						 $examDuration = 45;
					 } else if($exam_section_id == '' && !empty($settingsData24['Setting']['value'])){
						 $examDuration = $settingsData24['Setting']['value'];
					 } else {
						 $examDuration = 40;
					 }
			
			//$examDuration = $settingsData24['Setting']['value'];
		} else if($flag == 'CALCULATOR'){
			$section = 'exam_section_1_name';
			 
			//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
			$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
			$QuestionLimit =  $no_of_qs_20['Setting']['value'];
			   if($exam_section_id == 1){
						 $examDuration = 65;
					 } else if($exam_section_id == 2 || $exam_section_id == 3){
						 $examDuration = 45;
					 } else if($exam_section_id == '' && !empty($settingsData21['Setting']['value'])){
						 $examDuration = $settingsData21['Setting']['value'];
					 } else {
						 $examDuration = 40;
					 }
			
			//$examDuration = $settingsData21['Setting']['value'];
		} else {
			$section = 'nothing'; //false
			$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0)));
			$QuestionLimit =  $no_of_qs_23['Setting']['value'];
			
			      if($exam_section_id == 1){
						 $examDuration = 65;
					 } else if($exam_section_id == 2 || $exam_section_id == 3){
						 $examDuration = 45;
					 } else if($exam_section_id == '' && !empty($settingsData24['Setting']['value'])){
						 $examDuration = $settingsData24['Setting']['value'];
					 } else {
						 $examDuration = 40;
					 }
			/*if(!empty($settingsData24['Setting']['value'])){
		      $examDuration = $settingsData24['Setting']['value'];
			} else if(!empty($settingsData21['Setting']['value'])){
			  $examDuration = $settingsData21['Setting']['value'];
			} else {
			  $examDuration = 40; //minutes
			}*/
			
		}
		
		
		
		
		 //$studentdata1 = $this->UserConfiguration->getStudentData();
		 //$student_id = $studentdata1['Student']['id'];
		
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $studentid)));
		 
		 $examination_id = $this->data_decrypt($examination_id);
		 $myExamID = $this->data_decrypt($my_exam_id);
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		  $this->Session->write('my_exam_id', $myExamID);
		  
		  $this->set('my_exam_id', $myExamID);
		  $this->set('student_id', $studentid);
		 
		 
		
		//$settingsData21 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 21,'Setting.isdeleted' => 0))); //for calculator
		//$settingsData24 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 24,'Setting.isdeleted' => 0))); // for non-calculator
		
		//$no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		//$no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		//$examDuration = $settingsData21['Setting']['value'];
		//$QuestionLimit =  $no_of_qs_20['Setting']['value'];
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		if($section == 'nothing'){
			$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.isdeleted' => 0)
																	 ));
		
		} else {
		$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,
																	 'Question.section' => $section,
																	 'Question.isdeleted' => 0
																	 )));
		
		}		


		if(!empty($exam_section_id) && $exam_section_id != 3){
			$QuestionAns = $this->Question->find('all', array( 
																	//'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC',
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,
																	 'Question.exam_section_id' => $exam_section_id,
																	 'Question.isdeleted' => 0
																	 )));
		}
		
		
		if(!empty($exam_section_id) && $exam_section_id == 3){
			$QuestionAns = $this->Question->find('all', array( 
																	'limit' => 1,
																	//'order' => 'Question.question_order_front ASC',
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,
																	 //'Question.id' => 928,
																	 'Question.exam_section_id' => $exam_section_id,
																	 'Question.isdeleted' => 0
																	 )));
		}
		
		// pr($ExamDetails);die;
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		  $totalQuestions = count($QuestionAns);

		  $send_flag = 0;
		 if($flag == 'CALCULATOR')
		    $send_flag = 1;
		elseif ($flag == 'NON-CALCULATOR')
			$send_flag = 2;
		
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails','exam_section_id','examFlag','examination_id','send_flag'));
		 if($exam_section_id == 3){
		  $this->render('sample_literacy_writing');
		 }
		
    }
	
	
	
	
	
	
	
	
	
	
	
	 public function childrens_results() {
                $this->UserConfiguration->isParentLoggedIn();
				$this->layout = 'parent_layout';
				$parentData = $this->UserConfiguration->getParentData();
				$parent_id = $parentData['StudentParent']['id'];
		 
		 $this->MyExamStudentAnswer->recursive = 3;
		 
		 $this->Examination->unbindModel(array('hasMany' => 'Question'), false);
		 $this->paginate = array(
										'conditions' =>  array(
																 'MyExamStudentAnswer.isdeleted' => 0, 
																 //'MyExamStudentAnswer.exam_type' => 1, 
																 'MyExamStudentAnswer.student_parent_id' => $parent_id
																 ),
										'limit' => 10,
										'order' => 'MyExamStudentAnswer.id DESC',
									);
			$myExamAr = $this->paginate('MyExamStudentAnswer');
		
		
		
		
		
		
		 $this->set(compact('myExamAr'));
		
    }
	
	
	 public function view_answer($random_id = NULL, $student_id = NULL) {
       // $this->UserConfiguration->isStudentLoggedIn();
		 //$this->layout = 'parent_exam_layout';
		 $this->layout = 'parent_exam_layout_special';
		  $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 $totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
															'fields' => 'id, total_set_question',
															'conditions' => array(
																					'MyExamStudentAnswer.student_id' => $student_id, 
																					'MyExamStudentAnswer.student_ans_random_id' => $random_id
															)));
		
		
		
		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		
		
			$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $random_id,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 )));
																																	 
																																		 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if(trim(strtolower($stAns)) == trim(strtolower($corAns)) && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		
		 
		 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.random_id' => $random_id
																													 )));
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		 
    }
	
	
	
	
	  public function view_answer_old($random_id = NULL, $student_idd = NULL) {
       // $this->UserConfiguration->isStudentLoggedIn();
		 $this->layout = 'take_exam_layout';
		  $studentdata1 = $this->UserConfiguration->getStudentData();
				 if(!empty($student_idd)){
					  //$this->Session->delete('App.studentData');
					  //my_exam_id is school_purchase_exam_id
					  $myExamStudentAnsAr = $this->MyExamStudentAnswer->find('first', array(
																				  'fields' => 'id,my_exam_id,class_name,unique_flag',	
																				  'conditions' => array(
																						  'MyExamStudentAnswer.unique_flag' => $random_id
																						  )));
					  
					  $school_purchase_exam_id = $myExamStudentAnsAr['MyExamStudentAnswer']['my_exam_id'];
					  $class_name = $myExamStudentAnsAr['MyExamStudentAnswer']['class_name'];
					  $noOfStudentInYearGroup = $this->MyExamStudentAnswer->find('count', array('conditions' => array('MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 'MyExamStudentAnswer.exam_type' => 2)));
					  $noOfStudentInClass = $this->MyExamStudentAnswer->find('count', array('conditions' => array(
																								  'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id, 
																								  'MyExamStudentAnswer.class_name' => $class_name, 
																								  'MyExamStudentAnswer.exam_type' => 2
																								  )));

					 $studentRank = $this->fetch_student_rank($student_idd);
					 $yearGroupTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as year_group_scores'
															)));
														
						
																		
						$sameClassTotalScores = $this->MyExamStudentAnswer->find('first', array(
																			'conditions' => array(
																			'MyExamStudentAnswer.my_exam_id' => $school_purchase_exam_id,
																			'MyExamStudentAnswer.class_name' => $class_name
																			),
																	'fields' => array('sum(MyExamStudentAnswer.score) as class_scores'
															)));										
					 
					  $yearGroupMean = round(($yearGroupTotalScores[0]['year_group_scores']/$noOfStudentInYearGroup),2); 
					 
					  $sameClassMean = round(($sameClassTotalScores[0]['class_scores']/$noOfStudentInClass),2);
					   
						$this->set(compact('studentRank', 'yearGroupMean', 'sameClassMean', 'noOfStudentInYearGroup'));
						
					    $this->set('flagA', 'answer_view_by_school');
					    $student_id = $student_idd;
				 } else {
					   $student_id = $studentdata1['Student']['id'];
				 }
				 
				  if(!empty($random_id)){
					  //$this->Session->delete('App.studentData');
					  $this->set('flagB', 'answer_view_by_school');
				 }
		
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 if(!empty($random_id) && empty($student_idd)){
			$RandomID =  $random_id;
		 } else {
			  $RandomID = $this->Session->read('RandomID'); 
		 }
		
		
		if(!empty($student_idd)){
		$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.unique_flag' => $random_id
																														)));
		
		} else {
			$totalqustionSetDtl = $this->MyExamStudentAnswer->find('first', array(
																														'fields' => 'id, total_set_question',
																														'conditions' => array(
																																				'MyExamStudentAnswer.student_id' => $student_id, 
																																				'MyExamStudentAnswer.student_ans_random_id' => $RandomID
																														)));
		}
		$totalqustionSet = @$totalqustionSetDtl['MyExamStudentAnswer']['total_set_question'];
		$totalqustionSet = !empty($totalqustionSet) ? $totalqustionSet : 32 ;
		 $this->StudentAnswer->recursive = 2;
		 
		 //all attempt questions and answer list with correct answer in this arra $StudentAnswerAr in this array
		if(!empty($student_idd)){
		$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.unique_flag' => $random_id,
																																		// 'StudentAnswer.answer_id !=' => 0,
																																		
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 
																																		 )));
																																		 
		} else {
			$StudentAnswerAr = $this->StudentAnswer->find('all', array('conditions' => array(
																																		 'StudentAnswer.student_id' => $student_id, 
																																		 'StudentAnswer.random_id' => $RandomID,
																																		// 'StudentAnswer.answer_id !=' => 0
																																		'OR' => array(
																																							 'StudentAnswer.answer_id !=' => 0,
																																							'StudentAnswer.answer_text !=' => '',
																																		)
																																		 )));
		}																															 
																																		 
		$answer_ids = '';
		$question_ids = '';
		$descriptiveCorrectAnswer = 0;
		
		foreach($StudentAnswerAr as $val){
			 //$answer_ids = $answer_ids.','.$val['StudentAnswer']['question_id'];
			 $answer_ids = $answer_ids.','.$val['StudentAnswer']['answer_id'];
			
			 $descriptiveAnswer = $this->Answer->find('first', array('conditions' => array('Answer.iscorrect' => 1,'Answer.isdeleted' => 0,'Answer.answer_type' => 4, 'Answer.question_id' => $val['StudentAnswer']['question_id'])));
		     if(!empty($descriptiveAnswer)){
			 $stAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($val['StudentAnswer']['answer_text'])); // remove p tag
		
			$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($descriptiveAnswer['Answer']['answer_text'])); // remove p tag
			 if($stAns == $corAns && !empty($stAns) && !empty($corAns)){
				 $i = 1;
				  $descriptiveCorrectAnswer = $descriptiveCorrectAnswer + $i;
				  $i++;
			  }
			 }
		}
		 
	
		 $answer_ids = trim($answer_ids,",");
		  $answer_ids_ar = explode(',', $answer_ids);
		
		  
		 // $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		 $correctAnswer = $this->Answer->find('count', array('conditions' => array('Answer.iscorrect' => 1,'Answer.answer_type' => 3, 'Answer.id' => $answer_ids_ar)));
		$correctAnswer = ($correctAnswer + $descriptiveCorrectAnswer);
		 $totalAttemptQs = count($StudentAnswerAr);
		 if(!empty($student_idd)){
		  $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.unique_flag' => $random_id
																													 )));
	 
		 } else {
			 $QuestionAns = $this->StudentAnswer->find('all', array(
																										'order' => 'StudentAnswer.question_order ASC',
																										'conditions' => array(
																													'StudentAnswer.student_id' => $student_id, 
																													 'StudentAnswer.random_id' => $RandomID
																													 )));
			 
		 }
		 
		 $this->set(compact('StudentAnswerAr', 'totalAttemptQs', 'correctAnswer','studentDetails','totalqustionSet','QuestionAns'));
		 
    }
	 
	
	
	 public function logout() {
			$this->UserConfiguration->parentLogout();
			$this->Session->delete('App.studentData');
			$this->Session->setFlash(__('You have logged out.'), 'success');
			$this->redirect(array('controller' => 'parent', 'action' => 'login'));
    }
	
	
	 protected function _parentLoginRedirect(){
        $isparentLoggedIn = $this->Session->check('App.parentData');
        if ($isparentLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'parent', 'action' => 'family-profiles'));
        }else{
            return;
        }
    }
	
	
   
     public function captcha_match_value() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$captcha_txt = $this->request->data['captcha_txt'];
		
		if(strcasecmp($captcha_txt,$this->Session->read('captcha'))!= 0)
			{
				echo 'ERROR';
			} else {
				echo 'OK';
			}
		
	 }
	 
	 public function get_parent_email_exist() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$email = $this->request->data['parent_email'];
		$ExistEmail = $this->StudentParent->find('count', array('conditions' => array('StudentParent.email' => $email)));
			if($ExistEmail > 0){
				echo 'EXIST';
			} else {
				echo 'NOT-EXIST';
			}
	
		
	 }
   
   
   //forgot password and reset password
	
	 public function forgot_password() {
        $this->layout = 'home_layout';
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put')) {
			
				$email = $this->request->data['StudentParent']['email'];
				$emailData = $this->StudentParent->find('first', array(
														'conditions' => array(
														'StudentParent.email' => $email,
														'StudentParent.isdeleted' => 0,
														)));
				//pr($emailData);die;
                if (empty(trim($this->request->data['StudentParent']['email']))) {
                    $ERROR = 1;
                    $this->set('EmailErr', 'Please enter email');
                } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $ERROR = 1;
                        $this->set('EmailErr', 'Please enter valid email format');
                } else if(!empty($email) && count($emailData) == 0){
					    $ERROR = 1;
                        $this->set('EmailErr', 'This email is not found in our database');
				}
			
               $encoded_id = $this->data_encrypt($emailData['StudentParent']['id']);
			   $encoded_email = $this->data_encrypt($email);
			   $StudentParent_id = $emailData['StudentParent']['id'];
			  
                if ($ERROR == 0) {
                   //email send start
					$host = 'https://learnerleaderonline.com/student_parents/reset_password/'.$encoded_id.'/'.$encoded_email;
					$reset_password_link = '<a href="'.$host.'">Click here</a> to reset password';
					$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 17)));

					$html_content = $mailTemplateData['EmailTemplate']['content'];

					$arStudentParent = $this->StudentParent->find('first',array(
							'fields'=>array(
									'StudentParent.first_name',
									'StudentParent.last_name',
									'StudentParent.email'),
									'conditions'=>array(
									'StudentParent.id'=>$StudentParent_id
							)));

					$name = $arStudentParent['StudentParent']['first_name'];
					$html = '';
					$html.=str_replace('{{reset_password_link}}', $reset_password_link, $html_content);
					$html = str_replace('{{name}}', $name, $html);

					//pr($html);die;
					$email_subject = $mailTemplateData['EmailTemplate']['title'];
					$email = $arStudentParent['StudentParent']['email'];


					$mailFrom = 'sales@learnerleader.com';
					$FromName = 'Learner Leader Online';

					$mail = new PHPMailer;
					$mail->FromName = $FromName;
					$mail->From = $mailFrom;
					$mail->Subject = $email_subject;
					$mail->Body = stripslashes($html);
					$mail->AltBody = stripslashes($html);
					$mail->IsHTML(true);
					$mail->AddAddress($email);
					//$mail->AddAddress('dinesh.brainium@gmail.com');
					$mail->Send();
				   
				   
                   //email send end
                    $this->Session->setFlash(__('Password reset link has been sent to your email address.'), 'success');
                    $this->redirect(array('controller' => 'student_parents', 'action' => 'forgot_password'));
                }
			}
		}
	
	
	 public function reset_password($id = NULL, $email = NULL) {
        $this->layout = 'home_layout';
        $ERROR = 0;
        if ($this->request->is('post') || $this->request->is('put')) {
                if(empty(trim($this->request->data['StudentParent']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter new password');
							}
							 if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['StudentParent']['password'])) && strlen($this->request->data['StudentParent']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['StudentParent']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['StudentParent']['password'];
							$cpass = $this->request->data['StudentParent']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['StudentParent']['password'] != $this->request->data['StudentParent']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							
							$id = $this->data_decrypt($id);
							
							$email = $this->data_decrypt($email);
							$emailData = $this->StudentParent->find('first', array(
														'conditions' => array(
														'StudentParent.email' => trim($email),
														'StudentParent.id' => $id,
														'StudentParent.isdeleted' => 0
														)));
														
							//pr($emailData);die;							
							
							if(empty($emailData) && !empty($pass) && !empty($cpass) && $pass == $cpass){
								$ERROR = 1;
								$this->set('somthingErr', 'Something Error !');
							}
							

                if ($ERROR == 0) {
					$this->StudentParent->id = $id;
                    $this->request->data['StudentParent']['password'] = md5($pass);
                    $this->StudentParent->save($this->request->data);
                    $this->Session->setFlash(__('Password reset has been done, please login with new password'), 'success');
                    $this->redirect(array('controller' => 'parent', 'action' => 'login'));
                }
			}
		}
	
   
   
   
   
   
}
?>