<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CategoryHelper', 'View/Helper');

class ContentManagementsController extends AppController {

    public $name = 'ContentManagements';
    public $uses = array('ContentManagement', 'Setting', 'Admin', 'School', 'Attachment', 'ExaminationType', 'ExaminationCategory', 'Examination', 'Question', 'Answer', 'Subject');
    public $components = array('FilterSchools', 'FilterExamTypes', 'FilterExamCategories', 'FilterExamination', 'FilterSubject');
    public $settingData = array();
    public $categoryBreadcrumbList = array();
    public $examTypeBreadcrumbList = array();
    public $categoryHelper;
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const DEFAULTVALIDITY = 100;
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->UserConfiguration->isAdminLoggedIn();
        $this->settingData = $this->Setting->getSettingsData();
        $this->setExaminationCategoriesTree();
        $this->layout = 'admin';
        $this->categoryHelper = new CategoryHelper(new View());
    }
    
    public function index(){
        $this->setExaminationTypesTree();
       $contentManagementData = $this->ContentManagement->find('all', array('order' => 'ContentManagement.id DESC', 'conditions' => array('ContentManagement.isdeleted' => 0)));
        $this->set(compact('contentManagementData'));
        
    }
    
    public function subjects(){
        
        $subjects = $this->FilterSubject->getSubjects($this);
        //pr($subjects);
        $this->set(compact('subjects'));
        
    }

    public function search_subjects(){
        $url = array('controller' => 'examination_managements', 'action' => 'subjects');
        if (!empty($this->request->data['Subject']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['Subject']['search'])));
        $this->redirect($url);
    }
    
    public function add_subject(){
        $this->layout = false;
        $this->autoRender = false;
        
        if(!empty($this->request->data['Subject'])){
            
            $subjectName = trim($this->request->data['Subject']['name']);
            $subjectSlug = Inflector::slug(strtolower($subjectName), $replacement = '_');
            $this->request->data['Subject']['slug'] = $subjectSlug;
            
            $subject = $this->Subject->find('first', array(
                'conditions' => array(
                    'Subject.slug' => $subjectSlug,
                    'Subject.isdeleted' => 0
                )
            ));
            if(!empty($subject)){
                $message = 'Subject already exist with same name.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    ));
                exit();
            }else{
                                
                $this->Subject->create();
                if ($this->Subject->save($this->request->data)) {
                    $message = 'Subject saved successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'subjectId' => $this->Subject->id,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    $message = 'Subject not saved properly.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
            }
        }
    }
    
    public function edit_subject($subjectId = null){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            if(!empty($this->request->data)){
                $dataToUpdate = array();
                
                $subjectName = trim($this->request->data['Subject']['name']);
                $subjectSlug = Inflector::slug(strtolower($subjectName), $replacement = '_');
                $this->request->data['Subject']['slug'] = $subjectSlug;
                
                $subject = $this->Subject->find('count', array(
                    'conditions' => array(
                        'Subject.slug' => $subjectSlug,
                        'Subject.id !=' => $subjectId,
                        'Subject.isdeleted' => 0
                        )
                    )
                );
                
                if($subject > 0){
                    $message = 'Subject already exist with same name.';
                    echo json_encode(array(
                        'status' => 'Error', 
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    
                    $this->Subject->read(null, $subjectId);
                    $this->Subject->set($this->request->data['Subject']);
                    $this->Subject->save();
                    $message = 'Subject updated successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
                                
            }else{
                $subjectDetails = $this->Subject->findById($subjectId);
                echo json_encode($subjectDetails);
                exit();
            }
        }
    }
    
    public function view_subject($subjectId = null){

        if($this->request->is('ajax')){
            $this->layout = 'ajax';
//            $this->layout = false;
//            $this->autoRender = false;
            $subject = $this->Subject->findById($subjectId);
            echo json_encode($subject);
            exit();
        }
    }
    
    public function delete_subjects(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $allIds = $this->request->data['subjectIds'];
            $subjectIds = explode(',', $allIds);
                        
            foreach ($subjectIds as $key => $id){
                //$this->_recursivelyDeleteExamTypes($id);
                
                $questions = $this->Question->find('all', array(
                    'conditions' => array(
                        'Question.isdeleted' => 0,
                        'Question.subject_id' => $id
                    )
                ));
                
                if(!empty($questions)){
                    foreach ($questions as $key => $question){
                        $this->Answer->updateAll(
                            array('Answer.isdeleted' => 1),
                            array(
                                'Answer.question_id' => $question['Question']['id'],
                                'Answer.isdeleted' => 0
                            )
                        );
                        $this->Answer->clear();
                        
                        /* Deleting Questions */
                        $this->Question->updateAll(
                            array('Question.isdeleted' => 1),
                            array(
                                'Question.id' => $question['Question']['id'],
                                'Question.isdeleted' => 0
                            )
                        );
                        $this->Question->clear();
                    }
                }
                
                /* Deleting Subjects */
                $this->Subject->updateAll(
                    array('Subject.isdeleted' => 1),
                    array(
                        'Subject.id' => $id,
                        'Subject.isdeleted' => 0
                    )
                );
                $this->Subject->clear();                
            }

            echo json_encode($subjectIds);
            exit();
        }
    }
    
    public function types(){
        
        $this->setExaminationTypesTree();
        $examTypesBreadcrumb = array(
            array("text" => __('Examination Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index')),
            array("text" => __('Types'), "link" => array('controller' => 'examination_managements', 'action' => 'types'))
        );
        
        if (!empty($this->request->params['named']['parent_id'])){
            $parentId = $this->request->params['named']['parent_id'];
            $parentCategoryData = $this->ExaminationType->find('first', array(
                'conditions' => array(
                    'ExaminationType.id' => $parentId,
                    'ExaminationType.isdeleted' => 0
                )
            ));
            
            if(!empty($parentCategoryData['ExaminationType']['parent_id'])){
                $returnData = $this->getExamTypeBreadcrumb($parentCategoryData['ExaminationType']['parent_id']);
                                
                if(!empty($this->examTypeBreadcrumbList)){
                    $this->examTypeBreadcrumbList[] = array(
                        "text" => __($parentCategoryData['ExaminationType']['name']), 
                        "link" => array('controller' => 'examination_managements', 'action' => 'types', 'parent_id' => $parentCategoryData['ExaminationType']['id'])
                    );
                    
                    foreach ($this->examTypeBreadcrumbList as $key => $data){
                        $examTypesBreadcrumb[] = $data;
                    }
                }
            }else{
                $examTypesBreadcrumb[] = array("text" => __($parentCategoryData['ExaminationType']['name']), "link" => array( 'controller' => 'examination_managements', 'action' => 'types', 'parent_id' => $parentCategoryData['ExaminationType']['id']));
            }
            
            $this->set('isParent', true);
            
        }

        $examinationTypes = $this->FilterExamTypes->getExamTypes($this);//pr($examinationTypes);
        if (!empty($examinationTypes)) {
            $categoryTree = (!empty($parentId))?$this->buildTree($examinationTypes, $parentId, 'ExaminationType'):$this->buildTree($examinationTypes, 0, 'ExaminationType');
            //pr($examinationCategories);
            if(!empty($categoryTree)){
                $examinationTypes = $categoryTree;
            }
        }
        $this->set(compact('examinationTypes'));
        $this->set('examTypesBreadcrumb', $examTypesBreadcrumb);
        
    }
    
    public function search_exam_types() {
        $url = array('controller' => 'examination_managements', 'action' => 'types');
        if (!empty($this->request->data['ExaminationType']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['ExaminationType']['search'])));
        $this->redirect($url);
    }
    
    public function add_exam_type(){
        $this->layout = false;
        $this->autoRender = false;
        
        if(!empty($this->request->data['ExaminationType'])){
            
            $examTypeName = trim($this->request->data['ExaminationType']['name']);
            $examTypeSlug = Inflector::slug(strtolower($examTypeName), $replacement = '_');
            $parentId = (!empty($this->request->data['ExaminationType']['parent_id']))?$this->request->data['ExaminationType']['parent_id']:0;
            
            $examType = $this->ExaminationType->find('first', array(
                'conditions' => array(
                    'ExaminationType.slug' => $examTypeSlug,
                    'ExaminationType.parent_id' => $parentId
                )
            ));
            if(!empty($examType)){
                $message = 'Examination type already exist with same parent.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    ));
                exit();
            }else{
                $dataToSave = array(
                    'ExaminationType' => array(
                        'name' => $examTypeName,
                        'slug' => $examTypeSlug,
                        'parent_id' => $parentId,
                        'created' => date('YmdHis')
                    )
                );
                
                if(!empty($this->request->data['ExaminationType']['ismultipletimes'])){
                    $dataToSave['ExaminationType']['ismultipletimes'] = 1;
                }
                
                $this->ExaminationType->create();
                if ($this->ExaminationType->save($dataToSave)) {
                    $message = 'Examination type saved successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'examTypeId' => $this->ExaminationType->id,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    $message = 'Examination type not saved properly.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
            }
        }
    }
    
    public function edit_exam_type($examTypeId = null){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            if(!empty($this->request->data)){
                $dataToUpdate = array();
                
                $examTypeName = trim($this->request->data['ExaminationType']['name']);
                $examTypeSlug = Inflector::slug(strtolower($examTypeName), $replacement = '_');
                $parentId = (!empty($this->request->data['ExaminationType']['parent_id']))?$this->request->data['ExaminationType']['parent_id']:0;
                
                $examTypeDetails = $this->ExaminationType->find('count', array(
                    'conditions' => array(
                        'ExaminationType.slug' => $examTypeSlug,
                        'ExaminationType.id !=' => $examTypeId,
                        'ExaminationType.parent_id' => $parentId,
                        'ExaminationType.isdeleted' => 0
                        )
                    )
                );
                
                if($examTypeDetails > 0){
                    $message = 'Examination type already exist with same parent.';
                    echo json_encode(array(
                        'status' => 'Error', 
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }elseif ($examTypeId == $parentId) {
                    $message = 'Examination type and parent should\'t be same.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    $dataToUpdate['ExaminationType']['name'] = $examTypeName;
                    $dataToUpdate['ExaminationType']['slug'] = $examTypeSlug;
                    $dataToUpdate['ExaminationType']['modified'] = date('YmdHis');
                    
                    if(!empty($this->request->data['ExaminationType']['ismultipletimes'])){
                        $dataToUpdate['ExaminationType']['ismultipletimes'] = 1;
                    }
                    
                    $this->ExaminationType->read(null, $examTypeId);
                    $this->ExaminationType->set($dataToUpdate['ExaminationType']);
                    $this->ExaminationType->save();
                    $message = 'Examination type updated successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
                                
            }else{
                $examTypeDetails = $this->ExaminationType->findById($examTypeId);
                echo json_encode($examTypeDetails);
                exit();
            }
        }
        
    }
    
    public function view_exam_type($examTypeId = null){

        if($this->request->is('ajax')){
            $this->layout = 'ajax';
//            $this->layout = false;
//            $this->autoRender = false;
            $examType = $this->ExaminationType->findById($examTypeId);
            echo json_encode($examType);
            exit();
        }
    }
    
    public function delete_exam_types(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $allIds = $this->request->data['examTypeIds'];
            $examTypeIds = explode(',', $allIds);
                        
            foreach ($examTypeIds as $key => $id){
                $this->_recursivelyDeleteExamTypes($id);
            }

            echo json_encode($examTypeIds);
            exit();
        }
    }
    
    public function search_exam(){
        $url = array('controller' => 'examination_managements', 'action' => 'index');
        if (!empty($this->request->data['Examination']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['Examination']['search'])));
        $this->redirect($url);
    }

    public function categories(){
        
        $this->setExaminationTypesTree();
        $categoriesBreadcrumb = array(
            array("text" => __('Examination Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index')),
            array("text" => __('Categories'), "link" => array('controller' => 'examination_managements', 'action' => 'categories'))
        );
        
        if (!empty($this->request->params['named']['type'])){
            $this->request->params['named']['examination_type_id'] = $this->request->params['named']['type'];
        }
        
        if (!empty($this->request->params['named']['parent_id'])){
            $parentId = $this->request->params['named']['parent_id'];
            $parentCategoryData = $this->ExaminationCategory->find('first', array(
                'conditions' => array(
                    'ExaminationCategory.id' => $parentId,
                    'ExaminationCategory.isdeleted' => 0
                )
            ));
            
            if(!empty($parentCategoryData['ExaminationCategory']['parent_id'])){
                $returnData = $this->getCategoryBreadcrumb($parentCategoryData['ExaminationCategory']['parent_id']);
                                
                if(!empty($this->categoryBreadcrumbList)){
                    $this->categoryBreadcrumbList[] = array(
                        "text" => __($parentCategoryData['ExaminationCategory']['name']), 
                        "link" => array('controller' => 'examination_managements', 'action' => 'categories', 'parent_id' => $parentCategoryData['ExaminationCategory']['id'])
                    );
                    
                    foreach ($this->categoryBreadcrumbList as $key => $data){
                        $categoriesBreadcrumb[] = $data;
                    }
                }
            }else{
                $categoriesBreadcrumb[] = array("text" => __($parentCategoryData['ExaminationCategory']['name']), "link" => array( 'controller' => 'examination_managements', 'action' => 'categories', 'parent_id' => $parentCategoryData['ExaminationCategory']['id']));
            }
            
            $this->set('isParent', true);
            
        }

        $examinationCategories = $this->FilterExamCategories->getCategories($this);//pr($examinationCategories);
        if (!empty($examinationCategories)) {
            $categoryTree = (!empty($parentId))?$this->buildTree($examinationCategories, $parentId):$this->buildTree($examinationCategories);
            //pr($examinationCategories);
            if(!empty($categoryTree)){
                $examinationCategories = $categoryTree;
            }
        }
        $this->set(compact('examinationCategories'));
        $this->set('categoriesBreadcrumb', $categoriesBreadcrumb);
    }
    
    public function search_categories() {
        $url = array('controller' => 'examination_managements', 'action' => 'categories');
        if (!empty($this->request->data['ExaminationCategory']['search']))
            $url = array_merge($url, array('search' => urlencode($this->request->data['ExaminationCategory']['search'])));
        $this->redirect($url);
    }
    
    public function add_category(){
        $this->layout = false;
        $this->autoRender = false;
        
        if(!empty($this->request->data['ExaminationCategory'])){
            
            $categoryName = trim($this->request->data['ExaminationCategory']['name']);
            $categorySlug = Inflector::slug(strtolower($categoryName), $replacement = '_');
            $parentId = (!empty($this->request->data['ExaminationCategory']['parent_id']))?$this->request->data['ExaminationCategory']['parent_id']:0;
            
            $category = $this->ExaminationCategory->find('first', array(
                'conditions' => array(
                    'ExaminationCategory.slug' => $categorySlug,
                    'ExaminationCategory.examination_type_id' => $this->request->data['ExaminationCategory']['examination_type_id']
                )
            ));
            if(!empty($category)){
                $message = 'Category <b>'.$categoryName.'</b> already exist of same exam type.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();
            }else{
                $dataToSave = array(
                    'ExaminationCategory' => array(
                        'name' => $categoryName,
                        'slug' => $categorySlug,
                        'parent_id' => $parentId,
                        'examination_type_id' => $this->request->data['ExaminationCategory']['examination_type_id'],
                        'created' => date('YmdHis')
                    )
                );
                
                $this->ExaminationCategory->create();
                if ($this->ExaminationCategory->save($dataToSave)) {
                    $message = 'Category information saved successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'categoryId' => $this->ExaminationCategory->id,
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }else{
                    $message = 'Category information not saved properly.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
            }
        }
    }
    
    public function edit_category($categoryId = null){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            if(!empty($this->request->data)){
                $dataToUpdate = array();
                
                $categoryName = trim($this->request->data['ExaminationCategory']['name']);
                $categorySlug = Inflector::slug(strtolower($categoryName), $replacement = '_');
                $parentId = (!empty($this->request->data['ExaminationCategory']['parent_id']))?$this->request->data['ExaminationCategory']['parent_id']:0;
                
                $categoryDetails = $this->ExaminationCategory->find('count', array(
                    'conditions' => array(
                        'ExaminationCategory.slug' => $categorySlug,
                        'ExaminationCategory.id !=' => $categoryId,
                        'ExaminationCategory.examination_type_id' => $this->request->data['ExaminationCategory']['examination_type_id'],
                        'ExaminationCategory.isdeleted' => 0
                        )
                    )
                );
                
                if($categoryDetails > 0){
                    $message = 'Category <b>'.$categoryName.'</b> already exist of same exam type.';
                    echo json_encode(array(
                        'status' => 'Error',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen(strip_tags($message))
                        ));
                    exit();
                }else{
                    $dataToUpdate['ExaminationCategory']['name'] = $categoryName;
                    $dataToUpdate['ExaminationCategory']['slug'] = $categorySlug;
                    $dataToUpdate['ExaminationCategory']['parent_id'] = $parentId;
                    $dataToUpdate['ExaminationCategory']['examination_type_id'] = $this->request->data['ExaminationCategory']['examination_type_id'];
                    $dataToUpdate['ExaminationCategory']['modified'] = date('YmdHis');
                                        
                    $this->ExaminationCategory->read(null, $categoryId);
                    $this->ExaminationCategory->set($dataToUpdate['ExaminationCategory']);
                    $this->ExaminationCategory->save();
                    
                    $message = 'Category information updated successfully.';
                    echo json_encode(array(
                        'status' => 'Success',
                        'message' => $message,
                        'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                        'messageLetterCount' => strlen($message)
                        ));
                    exit();
                }
                                
            }else{
                $examinationCategoryDetails = $this->ExaminationCategory->findById($categoryId);
                echo json_encode($examinationCategoryDetails);
                exit();
            }
            
        }
    }
    
    public function view_category($categoryId = null){

        if($this->request->is('ajax')){
            $this->layout = 'ajax';
//            $this->layout = false;
//            $this->autoRender = false;
            $category = $this->ExaminationCategory->findById($categoryId);
            echo json_encode($category);
            exit();
        }
    }
    
    public function delete_categories(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $allIds = $this->request->data['categoryIds'];
            $categoryIds = explode(',', $allIds);
                        
            foreach ($categoryIds as $key => $id){
                $this->_recursivelyDeleteCategories($id);
            }

            echo json_encode($categoryIds);
            exit();
        }
        
    }
    
    public function getExamTypeBreadcrumb($parentId = null){
        
        $parentCategoryData = $this->ExaminationType->find('first', array(
            'conditions' => array(
                'ExaminationType.id' => $parentId,
                'ExaminationType.isdeleted' => 0
            )
        ));
        
        if(!empty($parentCategoryData['ExaminationType']['name'])){
            array_unshift($this->examTypeBreadcrumbList, array(
                "text" => __($parentCategoryData['ExaminationType']['name']), 
                "link" => array('controller' => 'examination_managements', 'action' => 'types', 'parent_id' => $parentCategoryData['ExaminationType']['id'])
            ));
        }
        
        if(!empty($parentCategoryData['ExaminationType']['parent_id'])){
            $this->getExamTypeBreadcrumb($parentCategoryData['ExaminationType']['parent_id']);
        }
        
        //return $this->categoryBreadcrumbList;
    }
    
    public function getCategoryBreadcrumb($parentId = null){
        
        $parentCategoryData = $this->ExaminationCategory->find('first', array(
            'conditions' => array(
                'ExaminationCategory.id' => $parentId,
                'ExaminationCategory.isdeleted' => 0
            )
        ));
        
        if(!empty($parentCategoryData['ExaminationCategory']['name'])){
            array_unshift($this->categoryBreadcrumbList, array(
                "text" => __($parentCategoryData['ExaminationCategory']['name']), 
                "link" => array('controller' => 'examination_managements', 'action' => 'categories', 'parent_id' => $parentCategoryData['ExaminationCategory']['id'])
            ));
        }
        
        if(!empty($parentCategoryData['ExaminationCategory']['parent_id'])){
            $this->getCategoryBreadcrumb($parentCategoryData['ExaminationCategory']['parent_id']);
        }
        
        //return $this->categoryBreadcrumbList;
    }

    protected function _recursivelyDeleteExamTypes($id) {
                
        $examTypes = $this->ExaminationType->find('all', array(
                    'conditions' => array(
                        'ExaminationType.parent_id' => $id,
                        'ExaminationType.isdeleted' => 0,
                    )
                )
            );

        if (!empty($examTypes) && count($examTypes)) {
            foreach($examTypes as $key => $examType) {
                
                if(!empty($examType['ExaminationCategory']) && count($examType['ExaminationCategory']) > 0){
                    foreach($examType['ExaminationCategory'] as $key => $category) {
                        $this->_recursivelyDeleteCategories($category['id']);
                    }
                } 
                
                $this->_recursivelyDeleteExamTypes($examType['ExaminationType']['id']);
            }
        }else{
            $examinationTypeDetails = $this->ExaminationType->read(null, $id);
            if(!empty($examinationTypeDetails['ExaminationCategory']) && count($examinationTypeDetails['ExaminationCategory']) > 0){
                foreach($examinationTypeDetails['ExaminationCategory'] as $key => $category) {
                    $this->_recursivelyDeleteCategories($category['id']);
                }
            }
        }
        
        /* Delete category from examination_categories table */
        $this->ExaminationType->read(null, $id);
        $this->ExaminationType->set(array(
            'isdeleted' => 1
        ));
        $this->ExaminationType->save();
        $this->ExaminationType->clear();
        
    }
    
    protected function _recursivelyDeleteCategories($id) {
                
        $categories = $this->ExaminationCategory->find('all', array(
                    'conditions' => array(
                        'ExaminationCategory.parent_id' => $id,
                        'ExaminationCategory.isdeleted' => 0,
                    )
                )
            );

        if (!empty($categories) && count($categories)) {
             foreach($categories as $key => $category) {
                  $this->_recursivelyDeleteCategories($category['ExaminationCategory']['id']);
             }
        }
        
        /* Delete category from examination_categories table */
        $this->ExaminationCategory->read(null, $id);
        $this->ExaminationCategory->set(array(
            'isdeleted' => 1
        ));
        $this->ExaminationCategory->save();
        $this->ExaminationCategory->clear();
        
        /* deleting all examination of related category */
        $examinations = $this->Examination->find('all', array(
            'conditions' => array(
                'Examination.examination_category_id' => $id,
                'Examination.isdeleted' => 0
            ),
            'recursive' => -1,
        ));
        
        if(!empty($examinations)){
            
            foreach ($examinations as $key => $value){
                
                $questions = $this->Question->find('all', array(
                    'conditions' => array(
                        'Question.examination_id' => $value['Examination']['id'],
                        'Question.isdeleted' => 0
                    ),
                    'recursive' => -1,
                ));
                
                if(!empty($questions)){
                    foreach ($questions as $key => $question){
                        $this->Answer->updateAll(
                            array('Answer.isdeleted' => 1),
                            array(
                                'Answer.question_id' => $question['Question']['id'],
                                'Answer.isdeleted' => 0
                            )
                        );
                        $this->Answer->clear();
                    }
                }
                
                /* Deleting Questions */
                $this->Question->updateAll(
                    array('Question.isdeleted' => 1),
                    array(
                        'Question.examination_id' => $value['Examination']['id'],
                        'Question.isdeleted' => 0
                    )
                );
                $this->Question->clear();
                
            }
            /* Deleting Examinations */
            $this->Examination->updateAll(
                array('Examination.isdeleted' => 1),
                array(
                    'Examination.examination_category_id' => $id,
                    'Examination.isdeleted' => 0
                )
            );
            $this->Examination->clear();
        }
        
    }
    
	  public function add_content(){
        
        $this->layout = false;
        $this->autoRender = false;
        
        if(!empty($this->request->data['ContentManagement'])){
            
            $this->ContentManagement->create();
            if ($this->ContentManagement->save($this->request->data)) {
                $message = 'Content saved successfully.';
                echo json_encode(array(
                    'status' => 'Success',
                    'message' => $message,
                    'examId' => $this->ContentManagement->id,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();
            }else{
                $message = 'Content not saved properly.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();
            }
        }
        
    }
	
	
	
    public function add_exam(){
        
        $this->layout = false;
        $this->autoRender = false;
        
        if(!empty($this->request->data['ContentManagement'])){
            
            $this->ContentManagement->create();
            if ($this->ContentManagement->save($this->request->data)) {
                $message = 'Content saved successfully.';
                echo json_encode(array(
                    'status' => 'Success',
                    'message' => $message,
                    'examId' => $this->ContentManagement->id,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();
            }else{
                $message = 'Content not saved properly.';
                echo json_encode(array(
                    'status' => 'Error',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();
            }
        }
        
    }
    
    public function view_content($content_management_id = null){

        if($this->request->is('ajax')){
            $this->layout = 'ajax';
//            $this->layout = false;
//            $this->autoRender = false;
            $content = $this->ContentManagement->findById($content_management_id);
            echo json_encode($content);
            exit();
        }
    }
    
    public function edit_content($content_management_id = null){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            if(!empty($this->request->data)){
                $this->ContentManagement->read(null, $content_management_id);
                $this->ContentManagement->set($this->request->data['ContentManagement']);
                $this->ContentManagement->save();
                
                $message = 'Content updated successfully.';
                echo json_encode(array(
                    'status' => 'Success',
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen(strip_tags($message))
                    ));
                exit();                                   
            }else{
                $examDetails = $this->ContentManagement->findById($content_management_id);
                echo json_encode($examDetails);
                exit();
            }   
        }
    }
    
    public function delete_exam(){
        
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;

            $allIds = $this->request->data['examIds'];
            $examIds = explode(',', $allIds);

            foreach ($examIds as $examId){

                $this->Examination->read(null, $examId);
                $this->Examination->set(array(
                    'isdeleted' => 1
                ));
                $this->Examination->save();
                $this->Examination->clear();
                
                /* Needs to update questions and answers table also. a*/
            }
            echo json_encode($examIds);
            exit();
        }else{
            $this->layout = 'admin';
        }
        
    }
    
    public function getExamTypeOrCategory(){
        if($this->request->is('ajax')){
            $this->layout = false;
            $this->autoRender = false;
            
            $actionName = trim($this->request->data['actionName']);
            $selectedValue = $this->request->data['selectedValue'];
            
            $typeDetails = array();
            if(!empty($selectedValue) && ($actionName == 'Category')){
                $typeDetails = $this->ExaminationCategory->read(null, $selectedValue);
            }else {
                $conditions = array(
                    'ExaminationCategory.isdeleted' => 0
                );
                if($selectedValue != 'all'){
                    $conditions['ExaminationCategory.examination_type_id'] = $selectedValue;
                }
                
                $arrayData = $this->ExaminationCategory->find('all', array(
                        'conditions' => $conditions,
                        'recursive' => -1,
                        'fields'=>array('ExaminationCategory.id, ExaminationCategory.name, ExaminationCategory.parent_id'), 
                        'order' => array('ExaminationCategory.name' => 'ASC')
                    )
                );       
                $this->ExaminationCategory->clear();       
                $examinationCategories = $this->buildTree($arrayData);
                $typeDetails = $this->categoryHelper->getCategoryList($examinationCategories);
                
            }
            echo json_encode($typeDetails);
            exit();
        }
    }
    
}