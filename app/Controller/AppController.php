<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('CakeText', 'Utility');


App::import('Vendor', 'phpmailer');


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
 
 ini_set('memory_limit','-1');
class AppController extends Controller {

    public $components = array('Session', 'UserConfiguration');
    public $uses = array(
						'Admin','Setting', 'ExaminationType', 'ExaminationCategory',
						'SchoolInformation', 'School', 'Student', 'StudentClass', 'StudentParent', 'ExamSection','EmailTemplate'

						);
    
    public function beforeFilter() {
        //Configure::write('debug', 0);
        /* ex. Naplan Exam: Admins */
        $this->set('title_for_layout', Configure::read('App.appName').': '.Inflector::humanize($this->request->params['controller']).' - '.Inflector::humanize($this->request->params['action']));
        $this->UserConfiguration->beforeFilter($this);
		//fetch class list from classes table
	 $classOptions = $this->StudentClass->find('list', array('fields' => 'id, name','conditions' => array('StudentClass.isactive' => 1, 'StudentClass.isdeleted' => 0)));
     $this->set(compact('classOptions'));
	    $yearGroupOptionPractice = $this->ExaminationCategory->find('list', array('fields' => 'id, name',
															 'order' =>	'ExaminationCategory.name ASC',
															  'conditions' => array(
															  'ExaminationCategory.examination_type_id' => 2, 
															  'ExaminationCategory.isdeleted' => 0
															  )));
          $this->set(compact('yearGroupOptionPractice'));
	 
	 
	
			//school
			$schoolData = $this->UserConfiguration->getSchoolData();
			$lastLoginSchool = $schoolData['School']['last_login'];
			$school_id = $schoolData['School']['id'];
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			//student
			$studentData = $this->UserConfiguration->getStudentData();
			$lastLoginStudent = @$studentData['Student']['last_login'];
			$student_id = $studentData['Student']['id'];
			$studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
			//Parent
			$parentData = $this->UserConfiguration->getParentData();
			$lastLoginParent = @$parentData['StudentParent']['last_login'];
			$parent_id = $parentData['StudentParent']['id'];
			$parentDetails = $this->StudentParent->find('first', array('conditions' => array('StudentParent.id' => $parent_id)));
			//added on 15-01-2018
			$examSections = $this->ExamSection->find('list',array(
															'fields' => 'id,name',
															'conditions' => array(
															'ExamSection.isdeleted' => 0
															)));
			//pr($studentDetails);die;
			$this->set(compact('schoolData','lastLoginSchool', 'studentDetails','lastLoginStudent','lastLoginParent','parentDetails', 'examSections'));
			
			

			
			
        
    }
	
	//get admin email
	
	 public function getAdminValue($field = null) {
      $admin = $this->Admin->find('first', array(
												'fields' => $field,
												'conditions' => array(
													'id' => 1
											)));
            if (!empty($admin)) {
                return $admin['Admin'][$field];
            } else {
                return null;
            }
       
    } 
	 
	 
    /* Start Payment functions */
    public function payment_success() {
        $this->layout = false;
        $this->autoRender = false;
        
        if ($this->request->is('post')) {
            //pr($this->request->data);
            
            $payPalCustomData = array();
            $pagedata = explode('/', $this->request->data['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            //pr($payPalCustomData);
            if (!empty($this->request->data['txn_id']) && ($this->request->data['payment_status'] == 'Completed')){
                
                /* For school payment success */
                if(!empty($payPalCustomData['school'])){
                    $this->Session->write('School.transactionData', $this->request->data);
                    $this->redirect(array('controller' => 'schools', 'action' => 'registration_success'));
                }
                
            }
            
        }else{
            $this->redirect(Router::url('/', true));
        }
        
    }
    
    public function payment_notify() {
        
    }
    
    public function payment_cancle() {
        $this->layout = false;
        $this->autoRender = false;
        
        /* Checking for school only */
        $schoolKey = $this->_paramKeyExist($this, 'school');
        if(!empty($schoolKey)){
            if($this->request->params['named']['payment_for'] == 'registration'){
                $this->redirect(array('controller' => 'schools', 'action' => 'registration_payment'));
            }
        }
        
    }
    /* End Payment functions */

    protected function _randomPassword($passwordLength = 8) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%&*^';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $passwordLength; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    
    protected function _getSettingsData(){
        $settings = array();
        $results = $this->Setting->find('all');
        if(!empty($results)){
            foreach ($results as $key => $setting){
                foreach ($setting as $newKey => $data){
                    $settings[$newKey][$data['key']] = $data['value'];
                }
            }
        }
        return $settings;
    }
    
    private function _paramKeyExist($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }
    
    protected function _getRandomPath($maxlevel = 3) {
        $level = rand(1, $maxlevel);
        $path = '';

        $validCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";
        for ($i = 0; $i < $level; $i++) {
            $tempFolder = '';
            $tempFolderSize = rand(2, 5);
            for ($j = 0; $j < $tempFolderSize; $j++) {
                $index = mt_rand(0, strlen($validCharacters) - 1);
                $tempFolder .= $validCharacters[$index];
            }
            $path .= $tempFolder . '/';
        }

        return $path;
    }
    
    public function uploadEditorImageFiles(){
        $this->layout = false;
        $this->autoRender = false;
        
        /*******************************************************
        * Only these origins will be allowed to upload images *
        ******************************************************/
        $accepted_origins = array();
        $origin = Router::url('/', true);
        $origin = rtrim($origin, '/');
        array_push($accepted_origins, $origin);
        //$accepted_origins = array("http://localhost", "http://192.168.1.1", "http://example.com");

        /** *******************************************
         * Change this lines of code to set the upload folder *
         * ******************************************* */
        //pr($this->request->data);
        //pr($_FILES); exit();
        
        $imageFolder = "images/";

        reset($_FILES);
        $temp = current($_FILES);
        if (is_uploaded_file($temp['tmp_name'])) {
//            if (isset($_SERVER['HTTP_ORIGIN'])) {
//                // same-origin requests won't set an origin. If the origin is set, it must be valid.
//                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
//                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
//                } else {
//                    header("HTTP/1.0 403 Origin Denied");
//                    return;
//                }
//            }

            /*
              If your script needs to receive cookies, set images_upload_credentials : true in
              the configuration and enable the following two headers.
             */
            // header('Access-Control-Allow-Credentials: true');
            // header('P3P: CP="There is no P3P policy."');
            // Sanitize input
            if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
                header("HTTP/1.0 500 Invalid file name.");
                return;
            }

            // Verify extension
            if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
                header("HTTP/1.0 500 Invalid extension.");
                return;
            }
            
            // Common variables for creating random folder path
            $randomDirectory = $this->_getRandomPath(3);
            //$targetDir = WWW_ROOT . 'files/editor/images/' . $randomDirectory;
            $targetDir = WWW_ROOT . 'files/editor/images/';
            $pathinfo = pathinfo($temp['name']);
            $ext = $pathinfo['extension'];
            $fileName = CakeText::uuid();
            //$fileName = $pathinfo['filename'];
            $fileNameExt = $fileName . "." . $ext;
            $locationToSend = $origin.'/files/editor/images/'.$fileNameExt;
            // Create target dir
//            $oldmask = umask(0);
//            if (!file_exists($targetDir))
//                @mkdir($targetDir, 0777, true);
//            umask($oldmask);
            
            // Accept upload if there was no origin, or if it is an accepted origin
            $filetowrite = $targetDir . $fileNameExt;
            move_uploaded_file($temp['tmp_name'], $filetowrite);

            // Respond to the successful upload with JSON.
            // Use a location key to specify the path to the saved image resource.
            // { location : '/your/uploaded/image/file'}
            echo json_encode(array('location' => $locationToSend));
            exit();
        } else {
            // Notify editor that the upload failed
            //header("HTTP/1.0 500 Server Error");
            die("HTTP/1.0 500 Server Error");
            exit();
        }
    }
    
    public function sendPasswordResetEmail($user = array(), $userType = null){
        
        if(!empty($user) && !empty($userType)){
            
            CakeLog::write('debug', "Sending reset email to .".$user["email"]);
            
            $email = new CakeEmail();
            $email->template('reset_password_content', 'reset_password');
            $email->to($user["email"]);
            $email->subject(__('Please reset your password'));
            $email->viewVars(array(
                'userData' => $user,
                'userType' => $userType,
                'fullUrl' => Router::url('/', true)
            ));

            $email->from(Configure::read('App.emailFromDefault'));
            $email->domain(Configure::read('App.domainName'));
            $email->emailFormat('html');

            if ($email->send()) {
                CakeLog::write('debug', "Reset Password Email sent to: " . $user['email']);
                return true;
            } else {
                CakeLog::write('error', "Can't send reset password email to: " . $user['email']);
                return false;
            }
            
        }else{
            return false;
        }
        
    }
    
    public function setExaminationTypesTree(){
        
        $arrayData = $this->ExaminationType->find('all', array(
                'conditions' => array(
                    'ExaminationType.isdeleted' => 0
                ),
                'fields'=>array('ExaminationType.id, ExaminationType.name, ExaminationType.parent_id'), 
                'order' => array('ExaminationType.name' => 'ASC')
            )
        );       
        $this->ExaminationType->clear();       
        $examinationTypes = $this->buildTree($arrayData, 0, 'ExaminationType');
        Configure::write('App.examinationTypeList', $examinationTypes);  
    }
    
    public function getExaminationTypesTree(){
        $results = Configure::read('App.examinationTypeList'); 
        return (!empty($results))?$results:null;
    }
    
    public function setExaminationCategoriesTree(){
        
        $arrayData = $this->ExaminationCategory->find('all', array(
                'conditions' => array(
                    'ExaminationCategory.isdeleted' => 0
                ),
                'fields'=>array('ExaminationCategory.id, ExaminationCategory.name, ExaminationCategory.parent_id'), 
                'order' => array('ExaminationCategory.name' => 'ASC')
            )
        );       
        $this->ExaminationCategory->clear();       
        $examinationCategories = $this->buildTree($arrayData);
        Configure::write('App.examinationCategoryList', $examinationCategories);  
    }
    
    public function getExaminationCategoriesTree(){
        $results = Configure::read('App.examinationCategoryList'); 
        return (!empty($results))?$results:null;
    }
            
    public function buildTree(array $elements, $parentId = 0, $modelName = null) {
        $branch = array();
        $modelName = (!empty($modelName))?$modelName:'ExaminationCategory';
        foreach ($elements as $element) {
            if ($element[$modelName]['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element[$modelName]['id'], $modelName);
                if ($children) {
                    $element['children'] = $children;
                }

                $branch[] = $element;
            }
        }
        return $branch;
    }
    
    public function getSchoolFilterOptions(){
        
        /* These are only check through browser url */
        //$this->layout = false;
        //$this->autoRender = false;
        
        $defaultLevels = array(
            'Combined' => 'Combined',
            'Primary' => 'Primary',
            'Secondary' => 'Secondary',
            'Special' => 'Special'
        );
        
        $defaultTypes = array(
            'Catholic' => 'Catholic',
            'Government' => 'Government',
            'Private' => 'Private',
            'Special' => 'Special'
        );
        
        $defaultStates = array(
            'NSW' => 'NSW'
        );
        
        $levels = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
        if(!empty($levels)){
            $defaultLevels = array_merge($defaultLevels, $levels);
            asort($defaultLevels);
        }
        
        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));
        if(!empty($types)){
            $defaultTypes = array_merge($defaultTypes, $types);
            asort($defaultTypes);
        }
        
        $states = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.state', 'SchoolInformation.state'),
            'order' => array('SchoolInformation.state ASC'),
            'group' => array('SchoolInformation.state')
        ));
        if(!empty($states)){
            $defaultStates = array_merge($defaultStates, $states);
            asort($defaultStates);
        }
        
        $this->set('schoolLevelOptions', $defaultLevels);
        $this->set('schoolTypeOptions', $defaultTypes);
        $this->set('schoolStateOptions', $defaultStates);
    }

	//added by Dinesh
	 public function data_encrypt($val = null) {
        $data = base64_encode($val);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    //Value decrypt function
    public function data_decrypt($val = null) {
        $data = str_replace(array('-', '_'), array('+', '/'), $val);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
	
	
	//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
				
	
	
	
		public function sendWelcomeEmail($arr=null){		
 			
			   
			$email = $arr['email'];
			$user_type = $arr['user_type'];
			$full_name = $arr['full_name'];
			$password = $arr['password'];
			 
			
			//email send start
					//$verification_link = Router::url('/', true).'/user/verify/'.$user_type.'/'.$encoded_email;
					 
					$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => 'welcome_email' )));

					$html_content = $mailTemplateData['EmailTemplate']['content'];

					 
					$html = '';
					$html.=str_replace('{{email}}', $email, $html_content);
					$html = str_replace('{{user_type}}', $user_type, $html);
					$html = str_replace('{{full_name}}', $full_name, $html);
					$html = str_replace('{{password}}', $password, $html);

					//pr($html);die;
					$email_subject = $mailTemplateData['EmailTemplate']['title'];
					 


					$mailFrom = 'sales@learnerleader.com';
					$FromName = 'Learner Leader Online';

					$mail = new PHPMailer;
					$mail->FromName = $FromName;
					$mail->From = $mailFrom;
					$mail->Subject = $email_subject;
					$mail->Body = stripslashes($html);
					$mail->AltBody = stripslashes($html);
					$mail->IsHTML(true);
					$mail->AddAddress($email);
					$mail->AddAddress('saneerkalia@kingwayz.com');
					try{
						$mail->Send();
					}catch(Exception $e) {
					}
			
					return true;
				   
                   //email send end
		}
		
		
		public function sendVerificationEmail($arr=null){
			
			//$encoded_id = $this->data_encrypt($emailData['School']['id']);
			
			   
			$email = $arr['email'];
			$user_type = $arr['user_type'];
			$full_name = $arr['full_name'];
			$encoded_email = $this->data_encrypt( $email);
			
			//email send start
					$verification_link = Router::url('/', true).'/user/verify/'.$user_type.'/'.$encoded_email;
					$reset_password_link = '<a href="'.$verification_link.'">Click here</a> to reset password';
					$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => 'verification_email_for_new_registration' )));

					$html_content = $mailTemplateData['EmailTemplate']['content'];

					 
					$html = '';
					$html.=str_replace('{{verification_link}}', $verification_link, $html_content);
					$html = str_replace('{{full_name}}', $full_name, $html);

					//pr($html);die;
					$email_subject = $mailTemplateData['EmailTemplate']['title'];
					 


					$mailFrom = 'sales@learnerleader.com';
					$FromName = 'Learner Leader Online';

					$mail = new PHPMailer;
					$mail->FromName = $FromName;
					$mail->From = $mailFrom;
					$mail->Subject = $email_subject;
					$mail->Body = stripslashes($html);
					$mail->AltBody = stripslashes($html);
					$mail->IsHTML(true);
					$mail->AddAddress($email);
					$mail->AddAddress('saneerkalia@kingwayz.com');
					try{
						$mail->Send();
					}catch(Exception $e) {
					}
			
					return true;
				   
                   //email send end
		}
		
		
	public function sendCustomMailToSchool($arr=null){					
					
		$email = $arr['to_email'];
		$mailFrom = $arr['from_email'];
		$html_content = $arr['message'];
		$email_subject = $arr['mail_subject'];
		 
		
		//email send start

				
				$FromName = 'Learner Leader Online';

				$mail = new PHPMailer;
				$mail->FromName = $FromName;
				$mail->From = $mailFrom;
				$mail->Subject = $email_subject;
				$mail->Body = stripslashes($html_content);
				$mail->AltBody = stripslashes($html_content);
				$mail->IsHTML(true);
				$mail->AddAddress($email);
				/*$mail->AddAddress('kingwayz@yopmail.com');
				$mail->AddAddress('kingwayz@mailinator.com');
				$mail->AddAddress('amitsharma.1984@yahoo.com');
				*/
				try{
					$mail->Send();
				}catch(Exception $e) {
				}
		
				return true;
			   
			   //email send end
	}
}
