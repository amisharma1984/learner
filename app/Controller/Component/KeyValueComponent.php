<?php

class KeyValueComponent extends Component {

    public $controller;
    //public $components = array('Session', 'Settings');
    //public $uses = array('Setting', 'Admin');
    
    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    public function setValue($key, $value, $id = null) {
        
//        $this->controller->loadModel('Profile');
//        $this->controller->Profile->recursive = 0;
//        $this->controller->Profile->unbindModel(array('belongsTo' => array('User')));

        $exists = $this->controller->Setting->find('count', array(
            'conditions' => array(
                'key' => $key,
                'isdeleted' => 0
            ))) == 0 ? false : true;

        if ($exists) {
            $this->controller->Setting->updateAll(
                    array('Setting.value' => "'$value'"), 
                    array('Setting.key' => $key, 'Setting.isdeleted' => 0)
            );
        } else {
            $this->controller->Setting->create();
            $this->controller->Setting->save(array(
                'Setting' => array(
                    'key' => $key, 
                    'value' => $value
                )
            ));
        }
    }

    public function getValue($key = null, $model = null) {
        if (!empty($model)) {
            /* For any other model rather than Setting */
        } else {
//            $this->controller->loadModel('Profile');
//            $this->controller->Profile->recursive = 0;
//            $this->controller->Profile->unbindModel(array('belongsTo' => array('User')));

            $exists = $this->controller->Setting->find('first', array(
                'conditions' => array(
                    'key' => $key,
                    'isdeleted' => 0
            )));
            if (!empty($exists)) {
                return $exists['Setting']['value'];
            } else {
                return null;
            }
        }
        return null;
    }
    
    public function getValueFromSettings($key, $model) {
        //echo json_encode($model['Setting'])."<br><br>".$key;
        if (!isset($model['Setting']) || !isset($model['Setting'][$key]))
            return null;
        return $model['Setting'][$key];
    }

}

?>