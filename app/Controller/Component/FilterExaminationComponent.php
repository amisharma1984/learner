<?php

class FilterExaminationComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 10;

    public function getExaminations($controller) {

        $controller->paginate = array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'limit' => $this->getLimit($controller),
            'maxLimit' => $this->_getTotalExaminations($controller)
        );

        $examinations = $controller->paginate('Examination');
        
        return $examinations;
    }

    public function getNext($controller, $examination) {
        $conditions = $this->getConditions($controller);
        $conditions['Examination.created <'] = $examination['Examination']['created'];
        
        $examination = $controller->Examination->find('first', array(
            'conditions' => $conditions,
            'order' => array('Examination.created' => 'desc'),
            'recursive' => -1
        ));

        return $examination;
    }

    public function getPrevious($controller, $examination) {
        $conditions = $this->getConditions($controller);
        $conditions['Examination.created >'] = $examination['Examination']['created'];
        
        $examination = $controller->Examination->find('first', array(
            'conditions' => $conditions,
            'order' => array('Examination.created' => 'asc'),
            'recursive' => -1
        ));

        return $examination;
    }

    public function getConditions($controller) {

        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted Examinations
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['Examination.isdeleted'] = '0';
        }
        
        // Parent category
        if (array_key_exists('exam_type', $controller->request->params['named'])) {
            $and['Examination.examination_type_id'] = $controller->request->params['named']['exam_type'];
            $controller->set('examType', $controller->request->params['named']['exam_type']);
        } else {
            $controller->set('examType', '');
        }
        
        // Parent category
        if (array_key_exists('category_id', $controller->request->params['named'])) {
            $and['Examination.examination_category_id'] = $controller->request->params['named']['category_id'];
            $controller->set('categoryId', $controller->request->params['named']['category_id']);
        } else {
            $controller->set('categoryId', '');
        }
        
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                "OR" => array(
                    'Examination.title LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'Examination.description LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationType.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationType.slug LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationCategory.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationCategory.slug LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%"
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else {
            $controller->set('search', '');
        }
                
        $final = array_merge($and, $or);//pr($final);

        return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalExaminations($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'title';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'title': $order = 'Examination.title ' . $orderType;
                break;
            case 'price': $order = 'Examination.price ' . $orderType;
                break;
            case 'exam_type': $order = 'ExaminationType.name ' . $orderType;
                break;
            case 'category': $order = 'ExaminationCategory.name ' . $orderType;
                break;
            case 'created': $order = 'Examination.created ' . $orderType;
                break;
        }

        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalExaminations($controller) {

        $count = $controller->Examination->find('count', array(
                'conditions' => array(
                    'Examination.isdeleted' => 0
                )
            )
        );
        $controller->Examination->clear();
        return $count;
    }

}
