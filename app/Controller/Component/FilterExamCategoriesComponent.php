<?php

class FilterExamCategoriesComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 100;

    public function getCategories($controller) {

//        $controller->paginate = array(
//            'conditions' => $this->getConditions($controller),
//            'joins' => $this->getJoins($controller),
//            'order' => $this->getOrder($controller),
//            'limit' => $this->getLimit($controller),
//            'maxLimit' => $this->_getTotalCategories($controller)
//        );
//
//        $categories = $controller->paginate('ExaminationCategory');
        
        $categories = $controller->ExaminationCategory->find('all', array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'limit' => $this->getLimit($controller)
        ));
        return $categories;
    }

    public function getNext($controller, $examinationCategory) {
        $conditions = $this->getConditions($controller);
        $conditions['ExaminationCategory.created <'] = $examinationCategory['ExaminationCategory']['created'];
        
        $conditions['ExaminationCategory.parent_id'] = (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0;

        $examinationCategory = $controller->ExaminationCategory->find('first', array(
            'conditions' => $conditions,
            'order' => array('ExaminationCategory.created' => 'desc'),
            'recursive' => -1
        ));

        return $examinationCategory;
    }

    public function getPrevious($controller, $examinationCategory) {
        $conditions = $this->getConditions($controller);
        $conditions['ExaminationCategory.created >'] = $examinationCategory['ExaminationCategory']['created'];
        $conditions['ExaminationCategory.parent_id'] = (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0;
        
        $examinationCategory = $controller->ExaminationCategory->find('first', array(
            'conditions' => $conditions,
            'order' => array('ExaminationCategory.created' => 'asc'),
            'recursive' => -1
        ));

        return $examinationCategory;
    }

    public function getConditions($controller) {

        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted categories
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['ExaminationCategory.isdeleted'] = '0';
        }
        
        // Parent category
        if (array_key_exists('parent_id', $controller->request->params['named'])) {
            $and['ExaminationCategory.parent_id'] = $controller->request->params['named']['parent_id'];
        }
        
        // Examination Type
        if (array_key_exists('exam_type', $controller->request->params['named'])) {
            $and['ExaminationCategory.examination_type_id'] = $controller->request->params['named']['exam_type'];
            $controller->set('examType', $controller->request->params['named']['exam_type']);
        }else{
            $controller->set('examType', '');
        }
        
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                "OR" => array(
                    'ExaminationCategory.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationCategory.slug LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationType.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationType.slug LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%"
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else {
            $controller->set('search', '');
        }
                
        $final = array_merge($and, $or);//pr($final);

        return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalCategories($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'name';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'name': $order = 'ExaminationCategory.name ' . $orderType;
                break;
            case 'slug': $order = 'ExaminationCategory.slug ' . $orderType;
                break;
            case 'exam_type': $order = 'ExaminationType.name ' . $orderType;
                break;
            case 'created': $order = 'ExaminationCategory.created ' . $orderType;
                break;
        }

        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalCategories($controller) {

        $count = $controller->ExaminationCategory->find('count', array(
            'Conditions' => array(
                'ExaminationCategory.isdeleted' => 0,
                'ExaminationCategory.parent_id' => (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0
                )
            )
        );
        $controller->ExaminationCategory->clear();
        return $count;
    }

}
