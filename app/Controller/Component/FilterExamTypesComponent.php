<?php

class FilterExamTypesComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 100;

    public function getExamTypes($controller) {
        
        $examTypes = $controller->ExaminationType->find('all', array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'limit' => $this->getLimit($controller)
        ));
        return $examTypes;
    }

    public function getNext($controller, $examinationType) {
        $conditions = $this->getConditions($controller);
        $conditions['ExaminationType.created <'] = $examinationType['ExaminationType']['created'];
        
        $conditions['ExaminationType.parent_id'] = (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0;

        $examinationType = $controller->ExaminationType->find('first', array(
            'conditions' => $conditions,
            'order' => array('ExaminationType.created' => 'desc'),
            'recursive' => -1
        ));

        return $examinationType;
    }

    public function getPrevious($controller, $examinationType) {
        $conditions = $this->getConditions($controller);
        $conditions['ExaminationType.created >'] = $examinationType['ExaminationType']['created'];
        $conditions['ExaminationType.parent_id'] = (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0;
        
        $examinationType = $controller->ExaminationType->find('first', array(
            'conditions' => $conditions,
            'order' => array('ExaminationType.created' => 'asc'),
            'recursive' => -1
        ));

        return $examinationType;
    }

    public function getConditions($controller) {

        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted categories
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['ExaminationType.isdeleted'] = '0';
        }
        
        // Parent category
        if (array_key_exists('parent_id', $controller->request->params['named'])) {
            $and['ExaminationType.parent_id'] = $controller->request->params['named']['parent_id'];
        }
        
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                "OR" => array(
                    'ExaminationType.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'ExaminationType.slug LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%"
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else {
            $controller->set('search', '');
        }
                
        $final = array_merge($and, $or);//pr($final);

        return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalTypes($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'name';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'name': $order = 'ExaminationType.name ' . $orderType;
                break;
            case 'slug': $order = 'ExaminationType.slug ' . $orderType;
                break;
            case 'created': $order = 'ExaminationType.created ' . $orderType;
                break;
        }

        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalTypes($controller) {

        $count = $controller->ExaminationType->find('count', array(
            'Conditions' => array(
                'ExaminationType.isdeleted' => 0,
                'ExaminationType.parent_id' => (!empty($controller->request->params['named']['parent_id']))?$controller->request->params['named']['parent_id']:0
                )
            )
        );
        $controller->ExaminationType->clear();
        return $count;
    }

}
