<?php

class FilterSubjectComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 10;

    public function getSubjects($controller) {

        $controller->paginate = array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'limit' => $this->getLimit($controller),
            'maxLimit' => $this->_getTotalSubjects($controller)
        );

        $subjects = $controller->paginate('Subject');
        
        return $subjects;
    }

    public function getNext($controller, $subject) {
        if(!empty($subject)){
            $conditions = $this->getConditions($controller);
            $conditions['Subject.created >'] = $subject['Subject']['created'];

            $subject = $controller->Subject->find('first', array(
                'conditions' => $conditions,
                'order' => array('Subject.created' => 'asc'),
                //'recursive' => -1
            ));
        }

        return $subject;
    }

    public function getPrevious($controller, $subject) {
        if(!empty($subject)){
            $conditions = $this->getConditions($controller);
            $conditions['Subject.created <'] = $subject['Subject']['created'];

            $subject = $controller->Subject->find('first', array(
                'conditions' => $conditions,
                'order' => array('Subject.created' => 'desc'),
                //'recursive' => -1
            ));
        }
        return $subject;
    }

    public function getConditions($controller) {

        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted categories
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['Subject.isdeleted'] = '0';
        }
                
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                "OR" => array(
                    'Subject.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'Subject.description LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%"
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else {
            $controller->set('search', '');
        }
                
        $final = array_merge($and, $or);//pr($final);

        return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalSubjects($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'name';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'name': $order = 'Subject.name ' . $orderType;
                break;
            case 'slug': $order = 'Subject.slug ' . $orderType;
                break;
            case 'created': $order = 'Subject.created ' . $orderType;
                break;
        }
        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalSubjects($controller) {
        $conditions = $this->getConditions($controller);
        $count = $controller->Subject->find('count', array(
                'conditions' => $conditions
            )
        );
        $controller->Subject->clear();
        return $count;
    }

}