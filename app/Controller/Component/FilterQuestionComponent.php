<?php

class FilterQuestionComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 10;

    public function getQuestions($controller) {

        $controller->paginate = array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'limit' => $this->getLimit($controller),
            'maxLimit' => $this->_getTotalQuestions($controller)
        );

        $questions = $controller->paginate('Question');
        
        return $questions;
    }

    public function getNext($controller, $question) {
        if(!empty($question)){
            $conditions = $this->getConditions($controller);
            $conditions['Question.created >'] = $question['Question']['created'];

            $question = $controller->Question->find('first', array(
                'conditions' => $conditions,
                'order' => array('Question.created' => 'asc'),
                //'recursive' => -1
            ));
        }

        return $question;
    }

    public function getPrevious($controller, $question) {
        if(!empty($question)){
            $conditions = $this->getConditions($controller);
            $conditions['Question.created <'] = $question['Question']['created'];

            $question = $controller->Question->find('first', array(
                'conditions' => $conditions,
                'order' => array('Question.created' => 'desc'),
                //'recursive' => -1
            ));
        }
        return $question;
    }

    public function getConditions($controller) {

        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted categories
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['Question.isdeleted'] = '0';
        }
        
        // question related to examination
        if (array_key_exists('examination_id', $controller->request->params['named'])) {
            $and['Question.examination_id'] = $controller->request->params['named']['examination_id'];
            $controller->set('examinationId', $controller->request->params['named']['examination_id']);
        } else {
            $controller->set('examinationId', '');
        }
        
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                "OR" => array(
                    'Question.title LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'Examination.title LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%"
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else {
            $controller->set('search', '');
        }
                
        $final = array_merge($and, $or);//pr($final);

        return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalQuestions($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'title';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'title': $order = 'Question.title ' . $orderType;
                break;
            case 'exam': $order = 'Examination.title ' . $orderType;
                break;
            case 'created': $order = 'Question.created ' . $orderType;
                break;
        }

        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalQuestions($controller) {
        $conditions = $this->getConditions($controller);
        $count = $controller->Question->find('count', array(
                'conditions' => $conditions
            )
        );
        $controller->Question->clear();
        return $count;
    }

}
