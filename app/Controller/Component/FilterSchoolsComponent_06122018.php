<?php

class FilterSchoolsComponent extends Component {

    //public $components = array('Session');

    const DEFAULTLIMIT = 10;

    public function getSchools($controller) {

        $controller->paginate = array(
            'conditions' => $this->getConditions($controller),
            'joins' => $this->getJoins($controller),
            'order' => $this->getOrder($controller),
            'group' => array('School.id'),
            'limit' => $this->getLimit($controller),
            'maxLimit' => $this->_getTotalSchools($controller)
        );

        $schools = $controller->paginate('School');
        return $schools;
    }

    public function getNext($controller, $school) {
        $conditions = $this->getConditions($controller);
        $conditions['School.created <'] = $school['School']['created'];

        $school = $controller->School->find('first', array(
            'conditions' => $conditions,
            'order' => array('School.created' => 'desc'),
            'recursive' => -1
        ));

        return $school;
    }

    public function getPrevious($controller, $school) {
        $conditions = $this->getConditions($controller);
        $conditions['School.created >'] = $school['School']['created'];

        $school = $controller->School->find('first', array(
            'conditions' => $conditions,
            'order' => array('School.created' => 'asc'),
            'recursive' => -1
        ));

        return $school;
    }

    public function getConditions($controller) {
        
        
        $or = array();
        $and = array();
        $final = array();
        
        // Not deleted categories
        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
            $and['School.isdeleted'] = 0;
        }
        
        // Is previous year customer?
        if (array_key_exists('previous', $controller->request->params['named'])) {
            $and['SchoolInformation.isprevious'] = 1;
            $controller->set('isprevious', true);
        } else {
            $controller->set('isprevious', false);
        }
        
        // School level
        if (array_key_exists('level', $controller->request->params['named'])) {
            $levels = explode(',', $controller->request->params['named']['level']);
            //$and['SchoolInformation.level IN'] = "('".implode("','", $levels)."')";
            $and = array_merge($and, array('SchoolInformation.level IN ("'.implode('","', $levels).'")'));
            $controller->set('level', $controller->request->params['named']['level']);
        } else {
            $controller->set('level', '');
        }
        
        // School level
        if (array_key_exists('type', $controller->request->params['named'])) {
            $types = explode(',', $controller->request->params['named']['type']);
            $and = array_merge($and, array('SchoolInformation.type IN ("'.implode('","', $types).'")'));
            $controller->set('type', $controller->request->params['named']['type']);
        } else {
            $controller->set('type', '');
        }
        
        // School State
        if (array_key_exists('state', $controller->request->params['named'])) {
            $states = explode(',', $controller->request->params['named']['state']);
            $and = array_merge($and, array('SchoolInformation.state IN ("'.implode('","', $states).'")'));
            $controller->set('state', $controller->request->params['named']['state']);
        } else {
            $controller->set('state', '');
        }
        
        // Search
        if (array_key_exists('search', $controller->request->params['named'])) {
            $and = array_merge($and, array(
                'OR' => array(
                    'School.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'School.manager_name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'School.email LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'SchoolInformation.street LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                    'SchoolInformation.phone LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
                )
            ));
            $controller->set('search', $controller->request->params['named']['search']);
        } else if (array_key_exists('sortbyname', $controller->request->params['named'])) {			
			$and = array_merge($and, array(                
                    'School.name LIKE' =>  urldecode($controller->request->params['named']['sortbyname']) . "%"
            ));
			$controller->set('sortbyname', $controller->request->params['named']['sortbyname']);
		}else {
            $controller->set('search', '');
        }
        $andNew['School.isdeleted'] = 0;
        //$andNew['SchoolInformation.isdeleted'] = 0;
        $final = array_merge($and,$andNew, $or);

        return $final;
        
        
        
        
        
        
        
        
        

//        $or = array();
//        $and = array();
//
//        // Search
//        if (array_key_exists('search', $controller->request->params['named'])) {
//            $and = array_merge($and, array(
//                "OR" => array(
//                    'School.name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
//                    'School.manager_name LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
//                    'School.email LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
//                    'School.address LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
//                    'School.phone LIKE' => "%" . urldecode($controller->request->params['named']['search']) . "%",
//                )
//            ));
//            $controller->set('search', $controller->request->params['named']['search']);
//        } else {
//            $controller->set('search', '');
//        }
//        
//        if (!array_key_exists('isdeleted', $controller->request->params['named'])) {
//            $and = array_merge($and, array("AND" => array(
//                    'School.isdeleted' => '0'
//            )));
//        }
        
        
        
        
        
               
        //return $final;
    }

    public function getLimit($controller) {
        $limit = self::DEFAULTLIMIT;
        if (array_key_exists('limit', $controller->request->params['named']))
            $limit = $controller->request->params['named']['limit'];

        $controller->set('limit', $limit);

        if (!empty($controller->request->params['named']['limit']) && $controller->request->params['named']['limit'] == -1) {
            $limit = $controller->request->params['named']['limit'] = $this->_getTotalSchools($controller);
        }

        return $limit;
    }

    public function getOrder($controller) {
        $order = $this->_param($controller, 'order');
        $orderType = strtoupper($this->_param($controller, 'orderType'));

        if (empty($order)) {
            $order = 'name';
        }
        if (empty($orderType)) {
            $orderType = 'ASC';
        }
        $controller->set('orderType', $orderType);
        $controller->set('order', $order);
        switch ($order) {
            case 'name': $order = 'School.name ' . $orderType;
                break;
//            case 'manager_name': $order = 'School.manager_name ' . $orderType;
//                break;
            case 'email': $order = 'School.email ' . $orderType;
                break;
            case 'level': $order = 'SchoolInformation.level ' . $orderType;
                break;
            case 'type': $order = 'SchoolInformation.type ' . $orderType;
                break;
            case 'payment_status': $order = 'School.payment_status ' . $orderType;
                break;
            case 'created': $order = 'School.created ' . $orderType;
                break;
        }

        return $order;
    }

    public function getJoins($controller) {
        $out = array();

//        if (array_key_exists('cat', $controller->request->params['named']))
//            array_push($out, array('table' => 'archives_categories', 'alias' => 'ArchivesCategory', 'type' => 'LEFT', 'conditions' => array('Archive.id = ArchivesCategory.archive_id')));       

        return $out;
    }

    private function _param($controller, $key) {
        if (array_key_exists($key, $controller->request->params['named']))
            return $controller->request->params['named'][$key];
        return '';
    }

    private function _getTotalSchools($controller) {

        $count = $controller->School->find('count', array('Conditions' => array('School.isdeleted' => 0)));
        $controller->School->clear();
        return $count;
    }

}
