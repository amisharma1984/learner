<?php

App::uses('Component', 'Controller');

class UserConfigurationComponent extends Component {
    
    public $components = array('Cookie', 'Session');
    public $controller;
    
    function beforeFilter(Controller $controller) {
        $this->controller = $controller;
        
        /* set previous url */
        //$path = $this->controller->request->params;
        //$this->setPreviousUrl($path);
        
        /* For Administrator Only */
        if ($this->Session->check('App.adminData')) {
            $admindata = $this->Session->read('App.adminData');
            $this->controller->set(compact('admindata'));
        }
        
        /* For School Only */
        if ($this->Session->check('App.schoolData')) {
            $schooldata = $this->Session->read('App.schoolData');
            $this->controller->set(compact('schooldata'));
        }
    }
    
    public function setAdminData($user) {
        $this->Session->write('App.adminData', $user);
    }
    
    public function getAdminData() {
        return $this->Session->read('App.adminData');
    }
    
    public function adminLogout() {
        $this->Session->delete('App.adminData');
    }
    
    public function isAdminLoggedIn() {
        $isLoggedIn = $this->Session->check('App.adminData');
        if (!$isLoggedIn) {
            //$this->Session->setFlash(__('Your session has expired. Please log in again.'), 'warning');
            $this->controller->redirect(array('controller' => 'admins', 'action' => 'login'));
        }
    }
    
    /* Start School related functions */
    public function setSchoolData($user) {
        $this->Session->write('App.schoolData', $user);
    }
    
    public function getSchoolData() {
        return $this->Session->read('App.schoolData');
    }
    
    public function schoolLogout() {
        $this->Session->delete('App.schoolData');
    }
    
    public function isSchoolLoggedIn() {
        $isLoggedIn = $this->Session->check('App.schoolData');
        if (!$isLoggedIn) {
            //$this->Session->setFlash(__('Your session has expired. Please log in again.'), 'warning');
            $this->controller->redirect(array('controller' => 'school', 'action' => 'login'));
            //$this->controller->redirect(array('controller' => 'schools', 'action' => 'registration'));
        }else{
            $schoolData = $this->Session->read('App.schoolData');
            if(!empty($schoolData['School']['payment_status'])){
                return true;
            }else{
                $actions = array('registration_payment');
                if(!in_array($this->controller->request->params['action'], $actions)){
                    $this->Session->setFlash(__('Please make a payment to get eligibility to take the online exam for your students in <strong>'.Configure::read('App.appName').'</strong>'), 'info');
                    $this->controller->redirect(array('controller' => 'schools', 'action' => 'registration_payment'));
                }   
            }
        }
    }
    
    /* url configuration functions */
    public function setPreviousUrl($url) {
        $this->Session->write('App.previousUrl', $url);
    }
    public function setPreviousUrlGeneral($url) {
        $this->Session->write('App.previousUrlGeneral', $url);
    }
    public function getPreviousUrlGeneral() {
        return $this->Session->read('App.previousUrlGeneral');
    }
    public function getPreviousUrl() {
        return $this->Session->read('App.previousUrl');
    }
    
	
	/* Start Parent related functions */
	 public function setParentData($user) {
        $this->Session->write('App.parentData', $user);
    }
    
    public function getParentData() {
        return $this->Session->read('App.parentData');
    }
    
    public function parentLogout() {
        $this->Session->delete('App.parentData');
    }
	
	
	 public function isParentLoggedIn() {
        $isLoggedIn = $this->Session->check('App.parentData');
        if (!$isLoggedIn) {
            $this->controller->redirect(array('controller' => 'parent', 'action' => 'login'));
        }else{
             return true;
        }
    }
	
	
	
	
	
	  /* Start Student related functions */
    public function setStudentData($user) {
        $this->Session->write('App.studentData', $user);
    }
    
    public function getStudentData() {
        return $this->Session->read('App.studentData');
    }
    
    public function studentLogout() {
        $this->Session->delete('App.studentData');
    }
	
	  public function isStudentLoggedIn() {
        $isLoggedIn = $this->Session->check('App.studentData');
        if (!$isLoggedIn) {
            //$this->Session->setFlash(__('Your session has expired. Please log in again.'), 'warning');
            $this->controller->redirect(array('controller' => 'student', 'action' => 'login'));
            //$this->controller->redirect(array('controller' => 'students', 'action' => 'registration'));
        }else{
            $studentData = $this->Session->read('App.studentData');
            if(!empty($studentData['Student']['payment_status'])){
                return true;
            }else{
                $actions = array('registration_payment');
                if(!in_array($this->controller->request->params['action'], $actions)){
                    $this->Session->setFlash(__('Please make a payment to get eligibility to take the online exam for your students in <strong>'.Configure::read('App.appName').'</strong>'), 'info');
                    $this->controller->redirect(array('controller' => 'schools', 'action' => 'registration_payment'));
                }   
            }
        }
    }
	
	 /* Start online exam related functions */
    public function setOnlineExamStudentData($user) {
        $this->Session->write('App.onlineExamStudentData', $user);
    }
    
    public function getOnlineExamStudentData() {
        return $this->Session->read('App.onlineExamStudentData');
    }
    
    public function studentOnlineExamLogout() {
        $this->Session->delete('App.onlineExamStudentData');
    }
	
	  public function isStudenOnlineExamtLoggedIn() {
        $isOnlineExamStudentLoggedIn = $this->Session->check('App.onlineExamStudentData');
        if (!$isOnlineExamStudentLoggedIn) {
            $this->controller->redirect(array('controller' => 'online_exams', 'action' => 'login'));
        } else {
            return true;
        }
    }
	
	
}
?>