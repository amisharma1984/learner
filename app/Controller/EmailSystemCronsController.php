<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class EmailSystemCronsController extends AppController {
    
    const STATUS_SEND = 1;
    const STATUS_PENDING = 0;
    public $name = 'EmailSystemCronsController';
    public $uses = array('School', 'Setting', 'Admin', 'Attachment', 'Email', 'EmailDelivery');
    public $extensionWiseMimeTypes;


    public function beforeFilter(){
        
        parent::beforeFilter();
        $this->layout = false;
        $this->autoRender = false;
        $this->extensionWiseMimeTypes = array(
            "pdf" => "application/pdf",
            "txt" => "text/plain",
            "doc" => "application/msword",
            "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "xls" => "application/vnd.ms-excel",
            "ppt" => "application/vnd.ms-powerpoint",
            "pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "ppsx" => "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "rtf" => "application/rtf",
            "odt" => "application/vnd.oasis.opendocument.text",
            "odp" => "application/vnd.oasis.opendocument.presentation",
            "odg" => "application/vnd.oasis.opendocument.graphics",
            "gif" => "image/gif",
            "png" => "image/png",
            "jpeg" => "image/jpg",
            "jpg" => "image/jpg",
        );
    }
    
    public function process() {
        
        $emailDeliveryDetails = $this->_getPending();
		//pr($emailDeliveryDetails);
        
        if(!empty($emailDeliveryDetails)){
            
            $emailDetails = $this->Email->find('first', array('conditions' => array('Email.id' => $emailDeliveryDetails['EmailDelivery']['email_id'])));
			//pr($emailDeliveryDetails);die;
            if(!empty($emailDetails)){
                $flag = $this->_sendMail($emailDetails, $emailDeliveryDetails);
                
                /* Updating Email, EmailDelivery */
                if($flag){
                    $this->Email->read(null, $emailDetails['Email']['id']);
                    $this->Email->set('issended', 0);
                    $this->Email->save();
                    
                    $this->EmailDelivery->read(null, $emailDeliveryDetails['EmailDelivery']['id']);
                    $this->EmailDelivery->set('issended', 0);
                    $this->EmailDelivery->save();
                }
            }
        }
        
    }
    
    protected function _getPending() {
        $result = $this->EmailDelivery->find('first', array(
            'conditions' => array(
                'EmailDelivery.issended' => self::STATUS_PENDING,
                'EmailDelivery.isdeleted' => 0
                )
            )
        );
        return $result;
    }
    
    protected function _sendMail($emailDetails = array(), $emailDeliveryDetails = array()){
        
        $attachmentFiles = array();
        if (!empty($emailDetails['Attachment'])) {
            //array('photo.png' => '/full/some_hash.png');
            foreach ($emailDetails['Attachment'] as $key => $attachment){
                //$attachmentFiles[$attachment['title'].'.'.$attachment['extension']] = Router::url('/', true).'files/school/attachments/'.$attachment['path'].$attachment['title'].'.'.$attachment['extension'];
                //$attachmentFiles[$attachment['title'].'.'.$attachment['extension']] = WWW_ROOT . 'files/school/attachments/'.$attachment['path'].$attachment['title'].'.'.$attachment['extension'];
                //$attachmentFiles[$key] = WWW_ROOT . 'files/school/attachments/'.$attachment['path'].$attachment['title'].'.'.$attachment['extension'];
                
                
                $attachmentFiles[$attachment['title'].'.'.$attachment['extension']]['file'] = WWW_ROOT . 'files/school/attachments/'.$attachment['path'].$attachment['title'].'.'.$attachment['extension'];
                $attachmentFiles[$attachment['title'].'.'.$attachment['extension']]['mimetype'] = $this->extensionWiseMimeTypes[strtolower($attachment['extension'])];
                
            }
        }
        //pr($attachmentFiles); exit();
        $fromDetails = $this->Admin->findById($emailDeliveryDetails['EmailDelivery']['from_id']);
        $toDetails = $this->School->findById($emailDeliveryDetails['EmailDelivery']['to_id']);
        
        $domainURL = Router::url('/', true) . 'naplan_exam'; //Configure::read('App.fullUrl');
        $app_name = Configure::read('App.appName');

        CakeLog::write('debug', "Email sending to school...");
        $this->set('title_for_layout', $emailDetails['Email']['subject']);

        $email = new CakeEmail();
        
        if(!empty($emailDetails['EmailTemplate']['content'])){
            $email->template('template', 'default');
        } else {
            $email->template('school', 'welcome');
        }
        
        $email->to($toDetails['School']['email']);
        $email->subject($emailDetails['Email']['subject']);

        if (!empty($attachmentFiles)) {
            $email->attachments($attachmentFiles);
        }
        $email->viewVars(array(
            'username' => $toDetails['School']['email'],
            'fullName' => $toDetails['School']['name'],
            'schoolId' => $toDetails['School']['id'],
            'message' => $emailDetails['Email']['message'],
            'full_url' => $domainURL
        ));

        //$email->from(array('info@t.com'));
        //$email->from(Configure::read('App.emailFromDefault'));
        $email->from($emailDetails['Email']['from_email']);
        $email->domain(Configure::read('App.domainName'));
        $email->emailFormat('html');
        
        if ($email->send()) {
            CakeLog::write('debug', "Email sent to: " . $toDetails['School']['email']);
            return true;
        } else {
            CakeLog::write('error', "Can't send email to: " . $toDetails['School']['email']);
            return false; 
        }
    }
    
}

?>


