<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
 
	 
    public $uses = array('Email', 'EmailTemplate','Student','StudentParent','School','EmailDelivery');
    public $components = array('FilterEmailTemplate');

    /**
     * Displays a view
     *
     * @return void
     * @throws ForbiddenException When a directory traversal attempt.
     * @throws NotFoundException When the view file could not be found
     *   or MissingViewException in debug mode.
     */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
    
    public function get_template_content($templateId = null){
        $this->layout = false;
        $this->autoRender = false;
        $templateDetails = $this->EmailTemplate->read(null, $templateId);
        
        $view = new View($this, false);
        $view->viewPath = 'EmailSystems';
        $view->render('get_template_content');
        $this->set(compact('templateDetails'));
    }
	
	public function verify($arr=null){
			
			 
			$model= $this->request['pass'][0];
			$email= $this->request['pass'][1];
		 	
			$decoded_email = $this->data_decrypt( $email);
			//$user = $this->$model->find('first', array('conditions' => array('email' => $decoded_email  )));
			
			
			$this->$model->updateAll(array('status'=>"'active'"), array( "email" => $decoded_email) )  ;

			  

$this->layout = 'landing_layout';
			 $this->Session->setFlash(__('Your Account has been verified, Please login.'), 'success');
			 
			 

			 
			
		}
		
		
		public function mail_status($schoolID,$emailID,$emailDeliveryID){
			
			$schoolID  = base64_decode($schoolID);
			$emailID  = base64_decode($emailID);
			$emailDeliveryID  = base64_decode($emailDeliveryID);
			
			$this->EmailDelivery->updateAll(array('EmailDelivery.issended'=>"'1'"), array( "EmailDelivery.id" => $emailDeliveryID) )  ;
 		
			$this->layout = 'landing_layout';
			 $this->Session->setFlash(__('Thanks for reading out the email/message.'), 'success');
			 
			  
			
		}
		
		
		
}
