<?php

App::uses('AppController', 'Controller');

class SettingsController extends AppController {

    public $name = 'Settings';
    public $uses = array('Setting', 'Admin');
    public $components = array('KeyValue');
    public $settingData = array();
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->UserConfiguration->isAdminLoggedIn();
        $this->settingData = $this->Setting->getSettingsData();
        
    }

    public function index() {
        $this->layout = 'admin';
        if(!empty($this->settingData)){
            $this->request->data = $this->settingData;
        }
    }
    
//    public function save() {
//        $this->layout = false;
//        $this->autoRender = false;
//        
//        if($this->request->is('post')){
//            
//            if(!empty($this->request->data['Setting'])){
//                foreach ($this->request->data['Setting'] as $key => $data){
//                    
//                    $id = $this->_settingsKeyExist($this->settingData, $key);
//                    if(!empty($id)){
//                        
//                        $this->Setting->updateAll(
//                                array('Setting.value' => '\''.$data.'\''), 
//                                array('Setting.id' => $id, 'Setting.key' => $key)
//                        );
//                    }else{
//                        
//                        $dataToSave = array(
//                            'key' => $key,
//                            'value' => $data,
//                            'created' => date('YmdHis')
//                        );
//                        $user = $this->Setting->save($dataToSave);
//                        $this->Setting->clear();
//                    }
//                }
//            }
//            $this->Session->setFlash(__('Your settings have been saved.'), 'success');
//            $this->redirect(array('controller' => 'settings', 'action' => 'index'));
//        }
//    }
    
    public function save() {
        $this->layout = false;
        $this->autoRender = false;
        
        if($this->request->is('post')){
            
            //pr($this->request->data);exit();
            $settingFor = $this->request->data['Setting']['for'];
            unset($this->request->data['Setting']['for']);
            
            if($settingFor == 'Examination'){
                
                $sections = $this->KeyValue->getValue('examination_sections');
                if(!empty($sections) && ($sections > $this->request->data['Setting']['examination_sections'])){
                    for($i = 1; $i <= $sections; $i++){
                        if($i > $this->request->data['Setting']['examination_sections']){
                            
                            $this->Setting->deleteAll(array(
                                'Setting.key' => 'exam_section_'.$i.'_name',
                                'Setting.isdeleted' => 0
                                ), false);
                            
                            $this->Setting->deleteAll(array(
                                'Setting.key' => 'exam_section_'.$i.'_questions',
                                'Setting.isdeleted' => 0
                                ), false);
                            
                            $this->Setting->deleteAll(array(
                                'Setting.key' => 'exam_section_'.$i.'_durations',
                                'Setting.isdeleted' => 0
                                ), false);
                        }
                    }
                }
                
            }
            
            foreach ($this->request->data['Setting'] as $key => $value ){
                //echo 'Key : '.$key.' Value : '.$value;
                $this->KeyValue->setValue($key, $value);
            }
            $message = $settingFor.' settings saved successfully.';
            echo json_encode(array(
                'status' => 'Success', 
                'message' => $message, 
                'settingsFor' => $settingFor, 
                'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                'messageLetterCount' => strlen($message)
                )
            );
            exit();
            
        }
        
    }
        
    protected function _settingsKeyExist($array = array(), $keySearch = null) {
        //pr($array);exit();
        $settingId = '';
                
        if (array_key_exists($keySearch, $array['Setting']))
            $settingId = $array['Setting'][$keySearch];

        return $settingId;
    }
    
    public function getCurrentSettingsData(){
        $this->layout = false;
        $this->autoRender = false;
        $this->settingData = $this->Setting->getSettingsData();
        echo json_encode($this->settingData);
        exit();
    }
}
