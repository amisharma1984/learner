<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ParentController extends AppController {

    public $name = 'Parent';
    public $uses = array('Parent', 'Student');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    
	function captcha_image()
	 {
		App::import('Vendor', 'captcha/captcha');
		$captcha = new captcha();
		$captcha->show_captcha();
	 }
	
	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	  public function registration($no_of_child = NULL) {
        $this->layout = 'home_layout';
		//$this->layout = 'landing_layout';
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		  if(!empty($no_of_child)){
			  if($no_of_child == 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Child';
			  } else if($no_of_child > 1){
				  $no_of_child = $no_of_child;
				  $childTag = 'Children';
			  } else {
				   $no_of_child = '';
				   $childTag = '';
			  }
		  } else {
			   $no_of_child = '';
			   $childTag = '';
		  }
		  
		   $this->set(compact('examination_types', 'examination_categories','no_of_child','childTag'));
		  
		  
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			 //pr($this->request->data);die;
			 //Parent registration
			 	if(empty(trim($this->request->data['Parent']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Parent']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Parent']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							
							
							
							if(empty(trim($this->request->data['Parent']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Parent']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Parent']['email']))){
								$email = $this->request->data['Parent']['email'];
								$ExistEmail = $this->Parent->find('count', array('conditions' => array('Parent.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['Parent']['password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['Parent']['password'])) && strlen($this->request->data['Parent']['password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['Parent']['password'])) && strlen($this->request->data['Parent']['password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['Parent']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['Parent']['password'];
							$cpass = $this->request->data['Parent']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['Parent']['password'] != $this->request->data['Parent']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
							if(empty(trim($this->request->data['Parent']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['Parent']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['Parent']['username'] = $this->request->data['Parent']['email'];
					$this->request->data['Parent']['password'] = md5($this->request->data['Parent']['password']);
					
					//send verfication mail
					$dataForMail['user_type'] = "Parent";
					$dataForMail['email'] = trim( $this->request->data['Parent']['email']);
					$dataForMail['full_name'] =  trim($this->request->data['Parent']['first_name'])." ". trim($this->request->data['Parent']['last_name']);
					$this->sendVerificationEmail($dataForMail);				 
					//send verfication mail+++
				
				
					$this->Parent->save($this->request->data);
					$lastInserId = $this->Parent->getLastInsertID();
					for($i =0; $i<$no_of_child; $i++){
						$studentData['Student']['parent_id'] = $lastInserId;
						$studentData['Student']['examination_category_id'] = $this->request->data['Student']['examination_category_id'][$i];
						$studentData['Student']['school_name'] = $this->request->data['Student']['school_name'][$i];
						$studentData['Student']['first_name'] = $this->request->data['Student']['first_name'][$i];
						$studentData['Student']['last_name'] = $this->request->data['Student']['last_name'][$i];
						$studentData['Student']['status'] = 'active';
						$this->Student->create();
						$this->Student->save($studentData);
					}
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					 $this->redirect(array('controller' => 'Parent', 'action' => 'registration',$no_of_child));
				}
				
			 }
		 }
	 

	
	
    public function login() {
        $this->layout = false;
		$ERROR = 0;
       if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			//pr($this->request->data);die;
            $login = strtolower($this->request->data['Parent']['login']);
            $password = $this->request->data['Parent']['password'];
			 $Parentdata = $this->Parent->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['Parent']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['Parent']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($Parentdata)){
								$ERROR = 1;
								$this->set('lnerror', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setParentData($Parentdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'Parent', 'action' => 'myprofile'));
			} else {
				 $this->redirect(array('controller' => 'Parent', 'action' => 'registration'));
			}
        }  
    }
    
   
     public function captcha_match_value() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$captcha_txt = $this->request->data['captcha_txt'];
		
		if(strcasecmp($captcha_txt,$this->Session->read('captcha'))!= 0)
			{
				echo 'ERROR';
			} else {
				echo 'OK';
			}
		
	 }
	 
	 public function get_parent_email_exist() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$email = $this->request->data['parent_email'];
		$ExistEmail = $this->Parent->find('count', array('conditions' => array('Parent.email' => $email)));
			if($ExistEmail > 0){
				echo 'EXIST';
			} else {
				echo 'NOT-EXIST';
			}
	
		
	 }
   
   
}
?>