<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');
class OnlineExamsController extends AppController {

    public $name = 'OnlineExams';
    public $uses = array(
										'Student','Setting', 'School','Examination','MyExam', 'Question','StudentAnswer', 
										'Answer', 'MyExamStudentAnswer', 'ExaminationType', 'ExaminationCategory',
										'SchoolPurchaseExam', 'SchoolTakeExam','EmailTemplate'
										);
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
     //   $this->settingData = $this->Setting->getSettingsData();
    }
    

	function random_value($length = NULL) {
			if(!empty($length)){
				$length = $length;
			} else {
				$length = 50;
			}
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$pin = substr( str_shuffle( $chars ), 0, $length );
			return strtoupper($pin);
		}
	
	
	public function writing_leaflet_mail(){
		
		 $studentData = $this->UserConfiguration->getOnlineExamStudentData();
		 $SchoolPurchaseExam = $this->Session->read('SchoolPurchaseExam');
		 
		 $student_id = $studentData['Student']['id'];
		 $from_email = $studentData['Student']['email'];
		 $from_name = $studentData['Student']['first_name'].' '.$studentData['Student']['last_name'];
		 
		 $to_email = $SchoolPurchaseExam['School']['manager_email']; //manager_email treated as head teacher's email
		 $to_name = $SchoolPurchaseExam['School']['manager_name'];//manager_name treated as head teacher's name
		 
		
		
		if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
						 //pr($this->request->data);die;
                        
						$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 14)));
                       $html_content = $mailTemplateData['EmailTemplate']['content'];
                       $html = '';
					   $html .= str_replace('{{name}}', $to_name, $html_content);
                       $html = str_replace('{{email_content}}', nl2br($this->request->data['descriptive_ans']), $html);
                        $email_subject = $mailTemplateData['EmailTemplate']['title'];
						
                        $mailFrom = $from_email;
                        $FromName = $from_name;

                        $mail = new PHPMailer;
                        $mail->FromName = $FromName;
                        $mail->From = $mailFrom;
                        $mail->Subject = $email_subject;
                        $mail->Body = stripslashes($html);
                        $mail->AltBody = stripslashes($html);
                        $mail->IsHTML(true);
                        $mail->AddAddress($to_email);
                        $mail->AddAddress('dinesh.amstech@gmail.com');
                        $mail->Send();
						
						
						//save writing leaflet in db start
		   $RandomID = $this->Session->read('RandomID');
		  $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
		  $school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		  $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		  $examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
		
			$exam_section_id = $this->request->data['exam_section_id'];
			$question_id = $this->request->data['questionids'];
			 
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				
				if(empty($MyExamStudentAnswerExist)){
					$studentAr = $this->Student->find('first', array(
										'fields' => 'id, teacher_name',
										'conditions' => array(
												'Student.id' => $student_id		
										)));
					$class_name = $studentAr['Student']['teacher_name']; // teacher_name is class_name
					$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['class_name'] = $class_name;
					$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2;
					//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  count($QuestionAns);
					$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] =  $unique_flag;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				} 
			 
			  
				// descriptive answer save
			if(!empty($this->request->data['questionids'])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'];
							$this->request->data['StudentAnswer']['writing_leaflet_mail'] = $html;
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->request->data['StudentAnswer']['calculator_non_calculator'] = $exam_section_id;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					
				
			}
			
						//save writing leaflet in db End
						
						
						
						
						
						
						
						
						
						
						
						
						$this->redirect(array('controller' => 'online_exams', 'action' => 'mail_sent'));
						 //$this->Session->setFlash(__('Email has been successfully sent to yor email address.'), 'success');
						
					  }
					 
	}
	
	
	public function mail_sent(){
		$this->layout = 'sample_qs_mail_sent';
		 $this->UserConfiguration->studentOnlineExamLogout();
		 $this->Session->delete('App.onlineExamStudentData');
		 $this->Session->delete('SchoolTakeExam');
		 $this->Session->delete('SchoolPurchaseExam');
		$this->render('mail_sent_thank_you');
	}
	
	
	
	
    public function login() {
         //$this->layout = 'home_layout';
         $this->layout = 'home_layout_special';
		 
		 $this->_onlineExamLoginRedirect();
		 
		  $yearGroupOption = $this->ExaminationCategory->find('list', array('fields' => 'id, name',
															 'order' =>	'ExaminationCategory.name ASC',
															  'conditions' => array(
															  'ExaminationCategory.examination_type_id' => 1, 
															  'ExaminationCategory.isdeleted' => 0
															  )));
          $this->set(compact('yearGroupOption'));
		 
		 
		$ERROR = 0;
       if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			//pr($this->request->data);die;
            $email = strtolower($this->request->data['Student']['email']);
            //$password = $this->request->data['Student']['password'];
            $exam_code = $this->request->data['Student']['exam_code'];
			 //$studentdata = $this->Student->getLoginData($login, $password);
					  $examCodeData = $this->SchoolPurchaseExam->find('first', array(
										 'conditions' => array(
																			'SchoolPurchaseExam.exam_code' => $exam_code,
														'SchoolPurchaseExam.isdeleted' => 0,
														'SchoolPurchaseExam.isexpired' => 0,
														'SchoolPurchaseExam.payment_status' => 1,
											)));
													 
				/*$examCodeData = $this->SchoolTakeExam->find('first', array(
													'conditions' => array(
																'SchoolTakeExam.exam_code' => $exam_code,
																'SchoolTakeExam.isdeleted' => 0,
																'SchoolTakeExam.isexpired' => 0,
															 )));*/																								 
															 
			      if(!empty($examCodeData)){
							 $this->Session->write('SchoolPurchaseExam', $examCodeData);
							// $this->Session->write('SchoolTakeExam', $examCodeData);
							// list($school_id, $schoolName, $examination_id, $school_take_exam_id) = explode('-', $this->request->data['Student']['exam_code']);
							 list($school_id, $schoolName, $examination_id) = explode('-', $this->request->data['Student']['exam_code']);
						 }
					
					/*$studentdata = $this->Student->find('first', array(
															'conditions' => array(
																	'Student.email' => $email,
																	'Student.isdeleted' => 0,
																	'Student.school_id' => $school_id
															 )));*/
															 
															 

						$schoolNameAr = $this->School->find('first', array(
															'fields' => 'id, name',			
															'conditions' => array(
																	'School.id' => @$school_id
															 )));
															 
								
								$NoOfStudent = $this->SchoolPurchaseExam->find('first', array(
															'fields' => 'id, no_of_student',			
															'conditions' => array(
																	'SchoolPurchaseExam.school_id' => @$school_id,
																	'SchoolPurchaseExam.examination_id' => @$examination_id,
																	'SchoolPurchaseExam.isdeleted' => 0,
																	'SchoolPurchaseExam.isexpired' => 0,
															 )));
															 
								$NoOfStudentLogin = $this->Student->find('all', array(
															'fields' => 'id',			
															'conditions' => array(
																	'Student.school_id' => @$school_id,
																	'Student.examination_id' => @$examination_id,
																	'Student.isdeleted' => 0,
															 )));
															 
								$no_of_student = @$NoOfStudent['SchoolPurchaseExam']['no_of_student'];	

								//echo 'no_of_student'.$no_of_student.'<br>';
								//echo 'NoOfStudentLogin'.count($NoOfStudentLogin).'<br>';
								//die;	 	
							if(count($NoOfStudentLogin) >= @$no_of_student && !empty($no_of_student)){
								$ERROR = 1;
								//$this->set('exceedErr', 'Exceed the limit of students allowed for this exam, please check with school administrator.');
								$this->set('exceedErr', 'The number of students allowed to attempt this exam has been exceeded. Please check with your Head Teacher.');
							}
								
							
						if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('firstNameErr', 'Please enter your first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lastNameErr', 'Please enter your last name');
							}
							
							if(!empty(trim($this->request->data['Student']['school_name'])) && !empty(trim($this->request->data['Student']['exam_code']))){
								
								$examCode = trim($this->request->data['Student']['exam_code']);
								$schoolName = trim($this->request->data['Student']['school_name']);
								$data = explode('-',$examCode);
								$schoolID = $data[0];
								$schoolDtls = $this->School->find('first',array('fields' => 'id,name','conditions' => array('School.id' => $schoolID)));
								//pr($schoolDtls);die;
								if(strtolower($schoolDtls['School']['name']) != strtolower($schoolName)){
									$ERROR = 1;
								    $this->set('schoolNameErr', 'The school name is incorrect.');
								}
								
							}
							
							
						if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('loginEmailErr', 'Please enter valid email format');
								} else if(!empty($this->request->data['Student']['exam_code'])){
								list($school_id, $schName, $exam_id) = explode('-', $this->request->data['Student']['exam_code']);
									$schooldata = $this->School->find('first', array('fields' => 'id, student_common_email_part','conditions' => array('School.id' => $school_id)));
									$existEmail = $this->Student->find('count', array(
																						'conditions' => array(
																						'Student.email' => $email,
																						'Student.password ' => '',
																						'Student.attemt_qs_1' => 1,
																						'Student.attemt_qs_2' => 2,
																						'Student.attemt_qs_3' => 3
																						
																						)));

									
									$scemail = $schooldata['School']['student_common_email_part'];
									if($scemail != '')
									if(strpos($scemail, '@') !== false){
										$scemail = $scemail;
									} else {
										$scemail = '@'.$scemail;
									}
									
									list($a, $email2ndPart) = explode('@', $email);
									
									if($scemail != '' && $scemail != '@'.$email2ndPart){
										$ERROR = 1;
									    $this->set('loginEmailErr', 'This email is wrong, check with your Head Teacher');
									} else if($existEmail >=1){
										$ERROR = 1;
									    $this->set('loginEmailErr', 'This email has been used, check with your Head Teacher');
									} else {
										//$ERROR = 0;
									}
															
								}
								
								/*if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('loginEmailErr', 'Please enter valid email format');
								}*/
							} 
							
							
							
							
						if(empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Please enter exam code');
							}
							
						
							
						/*if(empty($studentdata) && !empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Invalid email or exam code');
							}*/
							
						if(empty($examCodeData) && !empty(trim($this->request->data['Student']['exam_code']))){
								$ERROR = 1;
								$this->set('examCodeErr', 'Please enter valid exam code');
							}
							
					
				if(empty($this->request->data['Student']['examination_category_id'])){
						$ERROR = 1;
						$this->set('yearGrpErr','Please select year group');
					} else if(!empty($this->request->data['Student']['examination_category_id']) && !empty($this->request->data['Student']['exam_code'])){	
					$exam_code = $this->request->data['Student']['exam_code'];
					$year_group = $this->request->data['Student']['examination_category_id'];
					list($school_id, $schoolName, $examination_id) = explode('-', $exam_code);
					$examData = $this->SchoolPurchaseExam->find('first', array(
																	'fields' => array(	 
																						 'SchoolPurchaseExam.id',
																						 'SchoolPurchaseExam.examination_id',
																						 'SchoolPurchaseExam.isdeleted',
																						 'SchoolPurchaseExam.isexpired',
																						 'SchoolPurchaseExam.exam_code',
																						 'Examination.id',
																						 'Examination.examination_type_id',
																						 'Examination.examination_category_id',
																						 'Examination.title'
																					 ),
																	'conditions' => array(
																						'SchoolPurchaseExam.exam_code' => $exam_code,
																						'SchoolPurchaseExam.isdeleted' => 0,
																						'SchoolPurchaseExam.isexpired' => 0
																	)));
					
					
					if($year_group != $examData['Examination']['examination_category_id']){
						$ERROR = 1;
						$this->set('yearGrpErr', 'The exam code you have used, does not correspond to your year. Check with your Head Teacher');
					} 	
							
				}		
							

							
			//echo $ERROR;die;
			//pr($this->data);die;
			if($ERROR == 0){
				$getStudent = $this->Student->find('first', array(
														'conditions' => array(
																'Student.email' => $this->request->data['Student']['email'],
																'Student.examination_id' => $examination_id, //this line added on 29-03-2018
																'Student.password' => ''
														 )));
				
				//pr($getStudent);die;
				if(!empty($getStudent)){
					 $this->Student->id = $getStudent['Student']['id'];
				 }
				 
				 
				 $this->request->data['Student']['school_id'] = $school_id;
				 $this->request->data['Student']['examination_id'] = $examination_id;
				 $this->request->data['Student']['school_name'] = !empty($this->request->data['Student']['school_name']) ? $this->request->data['Student']['school_name'] : $schoolNameAr['School']['name'];
				 $this->Student->save($this->request->data);
				 
				  if(!empty($getStudent)){
					 $lastInserStudentId = $getStudent['Student']['id'];
				 } else {
					 $lastInserStudentId = $this->Student->getInsertID();
				 }
				 
				 
				 
				 $studentData = $this->Student->find('first', array(
															'conditions' => array(
																	'Student.id' => $lastInserStudentId
															 )));
				 
				 
				 $this->UserConfiguration->setOnlineExamStudentData($studentData);
				 $this->Session->write('OnlineStd', $studentData);
				
                // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'online_exams', 'action' => 'exam_instruction',));
			} 
        }  
    }
    
	
	
     public function exam_instruction_literacy() {
			//$this->layout = 'home_layout';
			$this->layout = 'home_layout_special';
	 }
	
	
     public function exam_instruction() {
			//$this->layout = 'home_layout';
			$this->layout = 'home_layout_special';
			$this->UserConfiguration->isStudenOnlineExamtLoggedIn();
		    $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
			$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
			
			
			$studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		    $student_id = $studentdata1['Student']['id'];
		    $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		    //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
			
			
			$examinationAr = $this->Examination->find('first', 
															array(
																'conditions' => array('Examination.id' => $examination_id)
															));
			
			$CalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_1_name', //Calculator
																								'Question.isdeleted' => 0
																								)));
																								
			$NonCalculatorQuestionCount = $this->Question->find('count', array(
																								'conditions' => array(
																								'Question.examination_id' => $examination_id,
																								'Question.section' => 'exam_section_2_name', //Non calculator
																								'Question.isdeleted' => 0
																								)));
																								
		 $this->set(compact('CalculatorQuestionCount', 'NonCalculatorQuestionCount','studentDetails'));
		 if($examinationAr['Examination']['exam_flag'] == 2){
			 $this->render('exam_instruction_literacy');
		 }
	 }
	 
	   public function second_section_exam() {
         //$this->layout = 'home_layout';
		 $this->layout = 'home_layout_special';
		 $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
	  }
	 
	   public function thank_you() {
         //$this->layout = 'home_layout';
		  $this->layout = 'home_layout_special';
		  $this->Session->delete('App.onlineExamStudentData');
		  $this->Session->delete('SchoolPurchaseExam');
		   //$this->Session->delete('SchoolTakeExam');
		 //$this->UserConfiguration->isStudenOnlineExamtLoggedIn();
	 }
	
	
	  protected function _onlineExamLoginRedirect(){
       // $isLoggedIn = $this->Session->check('App.schoolData');
        $isOnlineExamLoggedIn = $this->Session->check('App.onlineExamStudentData');
        if ($isOnlineExamLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'online_exams', 'action' => 'exam_instruction'));
        }else{
            return;
        }
    }
	
	
	
	 public function take_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		 $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		 
		 $RandomID = $this->Session->read('RandomID');
		
		  $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
		  $school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		  $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		  $examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
		
	
	
		  
		  //$schoolExamData =  $this->Session->read('SchoolTakeExam');
		  //$school_id = $schoolExamData['SchoolTakeExam']['school_id'];
		  
		 //$school_take_exam_id = $schoolExamData['SchoolTakeExam']['id'];
		 //$my_exam_id = $this->Session->read('my_exam_id');
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			
			 
			$this->Examination->recursive = 0;
		    $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 
			
			if($this->request->data['page_action'] == 'take_exam_second_section'){
					$section = 'exam_section_1_name';
					$calculator_non_calculator = 'calculator';
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
			} else {
					$section = 'exam_section_2_name';
					$calculator_non_calculator = 'non-calculator';
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					
					
			}
			
			if($this->request->data['exam_flag'] == 'NoneOfBothExam'){
				$QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 32;
				$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,//'Question.section' => $section, 
																	 'Question.isdeleted' => 0
																	 
																	 )
																	 ));
			} else {
			
			
		    $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		  
			 
			}
			 
			 
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				
				if(empty($MyExamStudentAnswerExist)){
					$studentAr = $this->Student->find('first', array(
										'fields' => 'id, teacher_name',
										'conditions' => array(
												'Student.id' => $student_id		
										)));
					$class_name = $studentAr['Student']['teacher_name']; // teacher_name is class_name
					
					$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					//$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
					//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['class_name'] = $class_name;
					$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2;
					//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					if($calculator_non_calculator == 'non-calculator'){
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question_lc'] =  count($QuestionAns);
					} else {
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question_rbq'] =  count($QuestionAns);
					}
					
					
					
					
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  count($QuestionAns);
					
					$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] =  $unique_flag;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				}else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						if($calculator_non_calculator == 'non-calculator'){
							$data['MyExamStudentAnswer']['total_set_question_lc'] =  count($QuestionAns);
						} else {
							$data['MyExamStudentAnswer']['total_set_question_rbq'] =  count($QuestionAns);
						}
						
						
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
			 
			  //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						$ansRadio['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
						$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$this->request->data['StudentAnswer']['student_id'] = $student_id;
						$this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;

						$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
						$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				}
			}
			 
			 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							
							$questionData['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
							$questionData['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			// die;
			//$this->Session->setFlash('Data has been submitted successfully.');
			if($this->request->data['page_action'] == 'take_exam' && ($this->request->data['exam_flag'] == 'SingleExam' || $this->request->data['exam_flag'] == 'NoneOfBothExam') ){ 
					$this->redirect(array('controller' => 'online_exams', 'action' => 'thank_you'));
			}
			else if($this->request->data['page_action'] == 'take_exam_second_section'){
					$this->redirect(array('controller' => 'online_exams', 'action' => 'thank_you'));
			} else {
					$this->redirect(array('controller' => 'online_exams', 'action' => 'second_section_exam'));
			}
		
			// pr($this->request->data);die;
		 }
		 
	  }
	  
	  
	   public function take_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));;
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			  /*$schoolExamData =  $this->Session->read('SchoolTakeExam');
		       $school_id = $schoolExamData['SchoolTakeExam']['school_id'];
		       $school_take_exam_id = $schoolExamData['SchoolTakeExam']['id'];*/
			    $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		        $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		        $examination_id =  $schoolExamData['SchoolPurchaseExam']['examination_id'];
				  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				
				/*if($this->request->data['page_action'] == 'take_exam_second_section'){
					$section = 'exam_section_1_name';
					$QuestionLimit =  $no_of_qs_20['Setting']['value'];
					$calculator_non_calculator = 'calculator';
				
				} else {
					$section = 'exam_section_2_name';
					$QuestionLimit =  $no_of_qs_23['Setting']['value'];
					$calculator_non_calculator = 'non-calculator';
				}
				*/
			$this->Examination->recursive = 0;
		    $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 
			
			if($this->request->data['page_action'] == 'take_exam_second_section'){
					$section = 'exam_section_1_name';
					$calculator_non_calculator = 'calculator';
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
			} else {
					$section = 'exam_section_2_name';
					$calculator_non_calculator = 'non-calculator';
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					
					
			}	
			
			if($this->request->data['exam_flag'] == 'NoneOfBothExam'){
				$QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 32;
				$QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array(
																	 'Question.examination_id' => $examination_id,//'Question.section' => $section, 
																	 'Question.isdeleted' => 0
																	 
																	 )
																	 ));
			} else {
				
		     $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
			}
					
					
					$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				if(empty($MyExamStudentAnswerExist)){
						$studentAr = $this->Student->find('first', array(
										'fields' => 'id, teacher_name',
										'conditions' => array(
												'Student.id' => $student_id		
										)));
						$class_name = $studentAr['Student']['teacher_name']; // teacher_name is class_name				
						$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					   //$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
						//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
						$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id; //school purchase id
						//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
						
						if($calculator_non_calculator == 'non-calculator'){
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question_lc'] =  count($QuestionAns);
						} else {
							$myExamStudentAns['MyExamStudentAnswer']['total_set_question_rbq'] =  count($QuestionAns);
						}
					
						
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] = count($QuestionAns);
						$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
						$myExamStudentAns['MyExamStudentAnswer']['class_name'] = $class_name;
						
						$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2; //online taken by school 
						$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
						$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] = $unique_flag;
						$this->MyExamStudentAnswer->save($myExamStudentAns);
					} else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
				
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						
						$ansRadio['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						$ansRadio['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 
			  	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
								$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
								$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
								$this->request->data['StudentAnswer']['student_id'] = $student_id;
								$this->request->data['StudentAnswer']['question_id'] = $question_id;
								$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
								$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
								
								
								$this->request->data['StudentAnswer']['random_id'] = $RandomID;
								$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
								$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
								$this->StudentAnswer->create();
								$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

						$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$questionData['StudentAnswer']['student_id'] = $student_id;
						$questionData['StudentAnswer']['question_id'] = $qid;
						$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
						$questionData['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id; 
						
						$questionData['StudentAnswer']['random_id'] = $RandomID;
						$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
						$questionData['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	  
	
	 public function take_exam($flag = NULL,$examFlag = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        //$this->layout = 'take_exam_layout';
        $this->layout = 'take_exam_layout_special';
		$flag = $this->data_decrypt($flag);
		$examFlag = $this->data_decrypt($examFlag);
		
		
		
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		/* $schoolExamData =  $this->Session->read('SchoolTakeExam');
		 $examination_id =  $schoolExamData['SchoolTakeExam']['examination_id'];
		 $myExamID = $schoolExamData['SchoolTakeExam']['school_purchase_exam_id'];*/
		       $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		        $myExamID =  $schoolExamData['SchoolPurchaseExam']['id'];
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		 $this->set('my_exam_id', $myExamID);
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
																	 
			     //exam_section_1_name/calculator, exam_section_2_name/ non-calculator
				 if($flag == 'NON-CALCULATOR'){
					$section = 'exam_section_2_name';
					$examDuration = !empty($ExamDetails['Examination']['exam_duration_non_cal_lc']) ? $ExamDetails['Examination']['exam_duration_non_cal_lc'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					
				} else if($flag == 'CALCULATOR'){
					$section = 'exam_section_1_name';
					 $examDuration = !empty($ExamDetails['Examination']['exam_duration_cal_rb']) ? $ExamDetails['Examination']['exam_duration_cal_rb'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
				} else {
					$section = 'nothing'; //false
					$examDuration = !empty($ExamDetails['Examination']['exam_duration_normal_wl']) ? $ExamDetails['Examination']['exam_duration_normal_wl'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 32;
				}

																 
																	 
		if($section == 'nothing'){
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array(
																		 'Question.examination_id' => $examination_id,//'Question.section' => $section, 
																		 'Question.isdeleted' => 0
																		 )
																	 ));
																	 
		} else {
			 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array(
																			 'Question.examination_id' => $examination_id,'Question.section' => $section, 
																			 'Question.isdeleted' => 0
																			 )
																	 ));
		}														 
																	 
		// pr($ExamDetails);die; exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		// echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		//pr($QuestionAns);die;
		// echo $this->Session->read('totalQuestions');die;
		 $step = 1;
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails', 'examFlag','examination_id','step'));
		 //$this->render('../Students/take_exam');
		
    }
	
	
	
	 public function take_exam_second_section($flag = NULL) {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        //$this->layout = 'take_exam_layout';
        $this->layout = 'take_exam_layout_special';
		$flag = $this->data_decrypt($flag);
		
		
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		 
		/* $schoolExamData =  $this->Session->read('SchoolTakeExam');
		 $examination_id =  $schoolExamData['SchoolTakeExam']['examination_id'];
		 $myExamID = $schoolExamData['SchoolTakeExam']['school_purchase_exam_id'];*/
		       $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		        $myExamID =  $schoolExamData['SchoolPurchaseExam']['id'];
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		 $this->set('my_exam_id', $myExamID);
		
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		 //exam_section_1_name/calculator, exam_section_2_name/ non-calculator
				 if($flag == 'NON-CALCULATOR'){
					$section = 'exam_section_2_name';
					$examDuration = !empty($ExamDetails['Examination']['exam_duration_non_cal_lc']) ? $ExamDetails['Examination']['exam_duration_non_cal_lc'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					
				} else if($flag == 'CALCULATOR'){
					$section = 'exam_section_1_name';
					 $examDuration = !empty($ExamDetails['Examination']['exam_duration_cal_rb']) ? $ExamDetails['Examination']['exam_duration_cal_rb'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
				} else {
					$section = 'nothing'; //false
					$examDuration = !empty($ExamDetails['Examination']['exam_duration_normal_wl']) ? $ExamDetails['Examination']['exam_duration_normal_wl'] : 45;
						 
					$QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 32;
				}
		
		
		    $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		// pr($ExamDetails);die; exam_section_1_name/calculator, exam_section_2_name/ non-calculator
		 //echo count($QuestionAns);die;
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $step = 2;
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails','examination_id','step'));
		 $this->render('take_exam');
		
    }
	
	
	//date 22-01-2018 start
	
	 public function take_literacy_exam($exam_section_id = NULL) {

	 	$examSection = 0;
        	if(!is_null($exam_section_id))
        		$examSection = $this->data_decrypt($exam_section_id);
        
      //  $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
       
        $this->layout = 'take_exam_layout_special';
		$exam_section_id = $this->data_decrypt($exam_section_id); //1,2,3
		
		//$examFlag = $this->data_decrypt($examFlag);
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 
		 if(!empty($exam_section_id) && !empty($student_id)){
			 $this->Student->id = $student_id;
			 echo $exam_section_id;
			  if($exam_section_id == 1){
				  $this->Student->saveField('attemt_qs_1',1);
			  }
			  
			  if($exam_section_id == 2){
				  $this->Student->saveField('attemt_qs_2',2);
			  }
			  
			  if($exam_section_id == 3){
				  $this->Student->saveField('attemt_qs_3',3);
			  }
			 
		 }
		 
		 
		
		$studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		$schoolExamData =  $this->Session->read('SchoolPurchaseExam');
		$examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		$myExamID =  $schoolExamData['SchoolPurchaseExam']['id'];
		 
		 $this->Session->write('examination_id', $examination_id);
		 $randomValue = $this->random_value();
		  $this->Session->write('RandomID', $randomValue);
		 $this->set('my_exam_id', $myExamID);
		
		        
		
		$this->Examination->recursive = 0;
		 $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
		
		
		           if($exam_section_id == 1){
						 $examDuration = !empty($ExamDetails['Examination']['exam_duration_cal_rb']) ? $ExamDetails['Examination']['exam_duration_cal_rb'] : 65;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
					 } 
					 
					 if($exam_section_id == 2){
						 $examDuration = !empty($ExamDetails['Examination']['exam_duration_non_cal_lc']) ? $ExamDetails['Examination']['exam_duration_non_cal_lc'] : 45;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					 } 
		
		            if($exam_section_id == 3){
						 $examDuration = !empty($ExamDetails['Examination']['exam_duration_normal_wl']) ? $ExamDetails['Examination']['exam_duration_normal_wl'] : 45;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 1;
					 } 
		
		
		
		 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.exam_section_id' => $exam_section_id, 'Question.isdeleted' => 0)
																	 ));
																	 
																	 
			if(!empty($exam_section_id) && $exam_section_id == 3){
				$QuestionAns = $this->Question->find('all', array( 
																		'limit' => 1,
																		 'conditions' => array(
																		 'Question.examination_id' => $examination_id,
																		 //'Question.id' => 928,
																		 'Question.exam_section_id' => $exam_section_id,
																		 'Question.isdeleted' => 0
																		 )));
			}															 
																	 
		
		 $this->Session->write('totalQuestions', count($QuestionAns));
		 $this->set(compact('studentDetails', 'QuestionAns', 'examDuration', 'totalQuestions', 'ExamDetails', 'examFlag','exam_section_id','examination_id','examSection'));
		 
		 if($exam_section_id == 3){
		  $this->render('sample_literacy_writing');
		 }
		
		
    }
	
	 public function take_literacy_exam_save() {
        //Configure::write('debug', 0);
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'home_layout';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		// $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));;
		 $RandomID = $this->Session->read('RandomID');
		
		  $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
		  $school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		  $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		  $examination_id = $schoolExamData['SchoolPurchaseExam']['examination_id'];
		  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
		  $this->Examination->recursive = 0;
		  $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
	
	
	
	     
		
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
			$exam_section_id = !empty($this->request->data['exam_section_id']) ? $this->request->data['exam_section_id'] :'';
			 
			    if($exam_section_id == 1){
						 //$examDuration = !empty($ExamDetails['Examination']['exam_duration_cal_rb']) ? $ExamDetails['Examination']['exam_duration_cal_rb'] : 65;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
					 } 
					 
					 if($exam_section_id == 2){
						 //$examDuration = !empty($ExamDetails['Examination']['exam_duration_non_cal_lc']) ? $ExamDetails['Examination']['exam_duration_non_cal_lc'] : 45;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					 } 
		
		            if($exam_section_id == 3){
						 //$examDuration = !empty($ExamDetails['Examination']['exam_duration_normal_wl']) ? $ExamDetails['Examination']['exam_duration_normal_wl'] : 45;
						 
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 1;
					 } 
			
		    $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.exam_section_id' => $exam_section_id, 'Question.isdeleted' => 0)
																	 ));
		  
			 
			 
			 $QuestionLimit =  count($QuestionAns);
			 
			$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				
				if(empty($MyExamStudentAnswerExist)){
					$studentAr = $this->Student->find('first', array(
										'fields' => 'id, teacher_name',
										'conditions' => array(
												'Student.id' => $student_id		
										)));
					$class_name = $studentAr['Student']['teacher_name']; // teacher_name is class_name
					
					$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					//$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
					//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
					$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id;
					$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
					$myExamStudentAns['MyExamStudentAnswer']['class_name'] = $class_name;
					$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2;
					//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
					
					if($exam_section_id == 1){
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question_rbq'] =  count($QuestionAns);
					}  
					
					if($exam_section_id == 2){
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question_lc'] =  count($QuestionAns);
					} 
					
					
					$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  count($QuestionAns);
					
					$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] =  $unique_flag;
					$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
					$this->MyExamStudentAnswer->save($myExamStudentAns);
				} else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						if($exam_section_id == 1){
							$data['MyExamStudentAnswer']['total_set_question_rbq'] =  count($QuestionAns);
						}  
						
						if($exam_section_id == 2){
							$data['MyExamStudentAnswer']['total_set_question_lc'] =  count($QuestionAns);
						} 
					
						
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
			 
			  //radio type answer save
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						$ansRadio['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						//$ansRadio['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$ansRadio['StudentAnswer']['calculator_non_calculator'] = $exam_section_id;
						
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			
				// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							//$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$this->request->data['StudentAnswer']['calculator_non_calculator'] = $exam_section_id;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			
			
			//check box answer save
			if(!empty($this->request->data['ans'])){
			 foreach($this->request->data['ans'] as $val){
				 $this->request->data = array();
				 list($question_id, $answer_id) = explode('Q#', $val);
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
						$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$this->request->data['StudentAnswer']['student_id'] = $student_id;
						$this->request->data['StudentAnswer']['question_id'] = $question_id;
						$this->request->data['StudentAnswer']['answer_id'] = $answer_id;

						$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						$this->request->data['StudentAnswer']['random_id'] = $RandomID;
						$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
						//$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->request->data['StudentAnswer']['calculator_non_calculator'] = $exam_section_id;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($this->request->data);
				}
			}
			 
			 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

							$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$questionData['StudentAnswer']['student_id'] = $student_id;
							$questionData['StudentAnswer']['question_id'] = $qid;
							$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
							
							$questionData['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							$questionData['StudentAnswer']['random_id'] = $RandomID;
							$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
							//$questionData['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$questionData['StudentAnswer']['calculator_non_calculator'] = $exam_section_id;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
			 
			$this->redirect(array('controller' => 'online_exams', 'action' => 'thank_you'));
		
		 }
		 
	  }
	  
	  
	 public function take_literacy_exam_save_ajax() {
        $this->autoRender = false;
        $this->UserConfiguration->isStudenOnlineExamtLoggedIn();
        $this->layout = 'ajax';
		 $studentdata1 = $this->UserConfiguration->getOnlineExamStudentData();
		 $student_id = $studentdata1['Student']['id'];
		 //$studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
		 $studentDetails = $this->Student->find('first', array('conditions' => array('Student.id' => $student_id)));
		
			 $no_of_qs_20 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 20,'Setting.isdeleted' => 0))); //for exam_section_1_questions
		    $no_of_qs_23 = $this->Setting->find('first', array('fields' => 'id, key, value','conditions' => array('Setting.id' => 23,'Setting.isdeleted' => 0))); // for exam_section_2_questions
		
		   $QuestionLimit =  $no_of_qs_20['Setting']['value'];
			$RandomID = $this->Session->read('RandomID');
			
			  /*$schoolExamData =  $this->Session->read('SchoolTakeExam');
		       $school_id = $schoolExamData['SchoolTakeExam']['school_id'];
		       $school_take_exam_id = $schoolExamData['SchoolTakeExam']['id'];*/
			    $schoolExamData =  $this->Session->read('SchoolPurchaseExam');
				$school_id = $schoolExamData['SchoolPurchaseExam']['school_id'];
		        $my_exam_id =  $schoolExamData['SchoolPurchaseExam']['id'];
		        $examination_id =  $schoolExamData['SchoolPurchaseExam']['examination_id'];
				  $unique_flag = $schoolExamData['SchoolPurchaseExam']['exam_code'].'-'.$student_id;
				  
			  $this->Examination->recursive = 0;
		      $ExamDetails = $this->Examination->find('first', array( 
																	//'fields' => 'id, title, description',	
																	 'conditions' => array('Examination.id' => $examination_id)
																	 ));
	  
				  
			 
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
				$exam_section_id = !empty($this->request->data['exam_section_id']) ? $this->request->data['exam_section_id'] :'';
			 
			    if($exam_section_id == 1){
						 $calculator_non_calculator = 1;
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_cal_rb']) ? $ExamDetails['Examination']['limit_cal_rb'] : 32;
					 } 
					 
					 if($exam_section_id == 2){
						$calculator_non_calculator = 2;
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_non_cal_lc']) ? $ExamDetails['Examination']['limit_non_cal_lc'] : 32;
					 } 
		
		            if($exam_section_id == 3){
						 $calculator_non_calculator = 3;
						 $QuestionLimit = !empty($ExamDetails['Examination']['limit_normal_wl']) ? $ExamDetails['Examination']['limit_normal_wl'] : 1;
					 } 
				
				
			
				
				
			 $QuestionAns = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.exam_section_id' => $exam_section_id, 'Question.isdeleted' => 0)
																	 ));	
				
		  $QuestionAns___ = $this->Question->find('all', array( 
																	'limit' => $QuestionLimit,
																	'order' => 'Question.question_order_front ASC', 
																	 'conditions' => array('Question.examination_id' => $examination_id,'Question.section' => $section, 'Question.isdeleted' => 0)
																	 ));
		  
					$MyExamStudentAnswerExist = $this->MyExamStudentAnswer->find('first', array(
																																		'conditions' => array(
																																		'MyExamStudentAnswer.school_id' => $school_id,
																																		'MyExamStudentAnswer.student_id' => $student_id
																																		)));
			
				if(empty($MyExamStudentAnswerExist)){
						$studentAr = $this->Student->find('first', array(
										'fields' => 'id, teacher_name',
										'conditions' => array(
												'Student.id' => $student_id		
										)));
						$class_name = $studentAr['Student']['teacher_name']; // teacher_name is class_name				
						$myExamStudentAns['MyExamStudentAnswer']['school_id'] = $school_id;
					   //$myExamStudentAns['MyExamStudentAnswer']['school_take_exam_id'] = $school_take_exam_id;
						//$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $this->request->data['my_exam_id'];
						$myExamStudentAns['MyExamStudentAnswer']['my_exam_id'] = $my_exam_id; //school purchase id
						//$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] =  $this->Session->read('totalQuestions');
						$myExamStudentAns['MyExamStudentAnswer']['total_set_question'] = count($QuestionAns);
						$myExamStudentAns['MyExamStudentAnswer']['student_id'] = $student_id;
						$myExamStudentAns['MyExamStudentAnswer']['class_name'] = $class_name;
						
						$myExamStudentAns['MyExamStudentAnswer']['exam_type'] = 2; //online taken by school 
						$myExamStudentAns['MyExamStudentAnswer']['student_ans_random_id'] = $RandomID;
						$myExamStudentAns['MyExamStudentAnswer']['unique_flag'] = $unique_flag;
						$this->MyExamStudentAnswer->save($myExamStudentAns);
					} else {
						$this->MyExamStudentAnswer->id = $MyExamStudentAnswerExist['MyExamStudentAnswer']['id'];
						$data['MyExamStudentAnswer']['total_set_question'] =  $MyExamStudentAnswerExist['MyExamStudentAnswer']['total_set_question'] + count($QuestionAns);
						$this->MyExamStudentAnswer->save($data);
					}
				
			 for($i = 1; $i <= $QuestionLimit; $i++){
				 if(!empty($this->request->data[$i.'_ans'][0])){
					  list($question_id, $answer_id) = explode('Q#', $this->request->data[$i.'_ans'][0]);
					    $question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
					   $ansRadio['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$ansRadio['StudentAnswer']['student_id'] = $student_id;
						$ansRadio['StudentAnswer']['question_id'] = $question_id;
						$ansRadio['StudentAnswer']['answer_id'] = $answer_id;
						
						$ansRadio['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
						
						$ansRadio['StudentAnswer']['random_id'] = $RandomID;
						$ansRadio['StudentAnswer']['unique_flag'] = $unique_flag;
						$ansRadio['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($ansRadio);
				 }
			 }
			 
			 
			  	// descriptive answer save
			if(!empty($this->request->data['HiddenTextQid'])){
				$j = 0;
			 foreach($this->request->data['HiddenTextQid'] as $val){
				$question_id = $val;
				if(!empty($this->request->data['descriptive_ans'][$j])){
							$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
							$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
							$this->request->data['StudentAnswer']['student_id'] = $student_id;
							$this->request->data['StudentAnswer']['question_id'] = $question_id;
							$this->request->data['StudentAnswer']['answer_id'] = 0;
							$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
							
							$this->request->data['StudentAnswer']['answer_text'] = $this->request->data['descriptive_ans'][$j];
							$this->request->data['StudentAnswer']['random_id'] = $RandomID;
							$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
							$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
							$this->StudentAnswer->create();
							$this->StudentAnswer->save($this->request->data);
					}
					$j++;
					
				}
			}
			 
			 
			
			if(!empty($this->request->data['ans'])){
				 foreach($this->request->data['ans'] as $val){
					 $this->request->data = array();
					 list($question_id, $answer_id) = explode('Q#', $val);
								$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $question_id)));
								$this->request->data['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
								$this->request->data['StudentAnswer']['student_id'] = $student_id;
								$this->request->data['StudentAnswer']['question_id'] = $question_id;
								$this->request->data['StudentAnswer']['answer_id'] = $answer_id;
								$this->request->data['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id;
								
								
								$this->request->data['StudentAnswer']['random_id'] = $RandomID;
								$this->request->data['StudentAnswer']['unique_flag'] = $unique_flag;
								$this->request->data['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
								$this->StudentAnswer->create();
								$this->StudentAnswer->save($this->request->data);
				 }
			 }
		
		 $questionData = array();
		if(!empty($this->request->data['questionids'])){
			 foreach($this->request->data['questionids'] as $qid){  //pr($qid);
				 //$questionData = array();
				 $checkData = $this->StudentAnswer->find('count', array(
																								 'conditions' => array(
																								 'StudentAnswer.question_id' => $qid, 
																								 'StudentAnswer.student_id' => $student_id, 
																								 'StudentAnswer.random_id' => $RandomID
																								 )));
				
				if($checkData == 0){
						$question_order = $this->Question->find('first', array('fields' => 'id, question_order_front','conditions' => array('Question.id' => $qid)));

						$questionData['StudentAnswer']['question_order'] = $question_order['Question']['question_order_front'];
						$questionData['StudentAnswer']['student_id'] = $student_id;
						$questionData['StudentAnswer']['question_id'] = $qid;
						$questionData['StudentAnswer']['answer_id'] = 0; //means not attempt question
						$questionData['StudentAnswer']['school_purchase_exam_id'] = $my_exam_id; 
						
						$questionData['StudentAnswer']['random_id'] = $RandomID;
						$questionData['StudentAnswer']['unique_flag'] = $unique_flag;
						$questionData['StudentAnswer']['calculator_non_calculator'] = $calculator_non_calculator;
						$this->StudentAnswer->create();
						$this->StudentAnswer->save($questionData);
				 }
					
				}
			}
		
		
		 }
		 
	  }
	    
	  
	  
	  
	//date 22-01-2018 End
	
	
	
	
	
	

    public function logout() {
        $this->UserConfiguration->studentOnlineExamLogout();
		 $this->Session->delete('App.onlineExamStudentData');
		 $this->Session->delete('SchoolTakeExam');
		// $this->Session->delete('SchoolPurchaseExam');
        $this->Session->setFlash(__('You have logged out.'), 'success');
      //  $this->redirect(array('controller' => 'students', 'action' => 'login'));
        $this->redirect(array('controller' => 'online_exams', 'action' => 'login'));
    }
    
	function db_update(){
		$this->StudentAnswer->recursive = 0;
		$this->Question->recursive = 0;
		$student_answers = $this->StudentAnswer->find('all');
		//pr($student_answers);
		foreach($student_answers as $val){
			$ques = $this->Question->find('first',array(
						'fields' => 'id,section',
						'conditions' => array(
						'Question.id'=> $val['StudentAnswer']['question_id']
						)));
					$question_id = $val['StudentAnswer']['question_id'];
				if($ques['Question']['section'] == 'exam_section_1_name'){
					//$this->StudentAnswer->updateAll(array('StudentAnswer.calculator_non_calculator' => "calculator"),array('StudentAnswer.question_id' => $val['StudentAnswer']['question_id']));		
			     $this->StudentAnswer->Query("UPDATE `student_answers` SET `calculator_non_calculator` = 'calculator' WHERE `student_answers`.`question_id` = '".$question_id."'");
				} else if($ques['Question']['section'] == 'exam_section_2_name'){
					//$this->StudentAnswer->updateAll(array('StudentAnswer.calculator_non_calculator' => "non-calculator"),array('StudentAnswer.question_id' => $val['StudentAnswer']['question_id']));		
				$this->StudentAnswer->Query("UPDATE `student_answers` SET `calculator_non_calculator` = 'non-calculator' WHERE `student_answers`.`question_id` = '".$question_id."'");

				} else {
					//
				}	
				
		}
		
		die;
	}
	
	
	 public function check_email_exist_online_exam() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$email = $this->request->data['email'];
		list($school_id, $schName, $exam_id) = explode('-', $this->request->data['exam_code']);
		$schooldata = $this->School->find('first', array('fields' => 'id, student_common_email_part','conditions' => array('School.id' => $school_id)));
		$existEmail = $this->Student->find('count', array(
															'conditions' => array(
															'Student.email' => $email,
															'Student.password ' => '',
															'Student.attemt_qs_1' => 1,
															'Student.attemt_qs_2' => 2,
															'Student.attemt_qs_3' => 3
															
															)));

		//pr($schooldata['School']['student_common_email_part']);
		$scemail = $schooldata['School']['student_common_email_part'];
		if($scemail != '')
		if(strpos($scemail, '@') !== false){
			$scemail = $scemail;
		} else {
			$scemail = '@'.$scemail;
		}
		
		list($a, $email2ndPart) = explode('@', $email);
		
		if($scemail != '' && $scemail != '@'.$email2ndPart){
			$flag = 1;
		} else if($existEmail >=1){
			$flag = 2;
		} else {
			$flag = 0;
		}
		
		echo $flag;
	 }
	 
	 public function check_exam_code_vallid() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$exam_code = $this->request->data['exam_code'];
		$examCodeDataCount = $this->SchoolPurchaseExam->find('count', array(
										 'conditions' => array(
														'SchoolPurchaseExam.exam_code' => $exam_code,
														'SchoolPurchaseExam.isdeleted' => 0,
														'SchoolPurchaseExam.isexpired' => 0,
														'SchoolPurchaseExam.payment_status' => 1,
											)));
		echo $examCodeDataCount;
	 }
	
	 public function check_year_group_vallid() {
        $this->autoRender = false;
        $this->layout = 'ajax';
		$exam_code = $this->request->data['exam_code'];
		$year_group = $this->request->data['year_group'];
		list($school_id, $schoolName, $examination_id) = explode('-', $exam_code);
		$examData = $this->SchoolPurchaseExam->find('first', array(
														'fields' => array(	 
																			 'SchoolPurchaseExam.id',
																			 'SchoolPurchaseExam.examination_id',
																			 'SchoolPurchaseExam.isdeleted',
																			 'SchoolPurchaseExam.isexpired',
																			 'SchoolPurchaseExam.exam_code',
																			 'Examination.id',
																			 'Examination.examination_type_id',
																			 'Examination.examination_category_id',
																			 'Examination.title'
																		 ),
														'conditions' => array(
																			'SchoolPurchaseExam.exam_code' => $exam_code,
																			'SchoolPurchaseExam.isdeleted' => 0,
																			'SchoolPurchaseExam.isexpired' => 0
														)));
		
		
		if($year_group != $examData['Examination']['examination_category_id']){
			echo "UNVALLID";
		} else {
			echo "VALLID";
		}
	 }
	
	
	
}
?>