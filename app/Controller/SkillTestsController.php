<?php
App::uses('AppController', 'Controller');

class SkillTestsController extends AppController {

    public $name = 'SkillTests';
    public $uses = array('Setting', 'ExaminationType', 'ExaminationCategory', 'Examination', 'Question', 'Answer');
    public $components = array('FilterSchools', 'FilterExamTypes', 'FilterExamCategories', 'FilterExamination');
    public $settingData = array();
            
    public function beforeFilter() {
        parent::beforeFilter();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function question(){
        $this->layout = 'skill_tests';
        
        if(!empty($this->request->params['named']['exam'])){
            $exam = urldecode($this->request->params['named']['exam']);
            $key = Configure::read('Security.salt');
            $examId = Security::decrypt(base64_decode($exam), $key);
            
            
            $questions = $this->Question->find('all', array(
                'conditions' => array(
                    'Question.examination_id' => $examId,
                    'Question.isdeleted' => 0
                ),
                'order' => array('Question.question_order ASC'),
                'group' => array('Question.section')
            ));
            //$this->set('questions', $questions);
            
        } else {
            $this->redirect( Router::url( $this->referer(), true ) );
        }
        
    }
}

