<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor', 'phpmailer');

class HomesController extends AppController {

    public $name = 'Homes';
    public $uses = array('Student','ContentManagement','EmailTemplate','Examination', 'Question','ExaminationCategory', 'SiteVideo');
    public $components = array('FilterSchools','Session');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
				public function beforeFilter() {
					parent::beforeFilter();
				 //   $this->settingData = $this->Setting->getSettingsData();
				}
    
	
    public function mail_sent(){
		//$this->layout = 'take_exam_layout';
		$this->layout = 'sample_qs_mail_sent';
		$this->render('mail_sent_thank_you');
		
		
	}
	
	public function writing_leaflet_mail(){
		
		$studentData = $this->UserConfiguration->getStudentData();
		$parentData = $this->UserConfiguration->getParentData();
		$schoolData = $this->UserConfiguration->getSchoolData();
		
		if(!empty($studentData)){
			$email = $studentData['Student']['email'];
			$name = $studentData['Student']['first_name'];
		} else if(!empty($parentData)){
			$email = $parentData['Parent']['email'];
			$name = $parentData['Parent']['first_name'];
		} else if(!empty($schoolData)){
			$email = $schoolData['School']['email'];
			$name = $studentData['School']['manager_name'];
		} else {
			$email = 'dinesh.amstech@gmail.com';
		}
		
		
		if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
						  //pr($this->data);die;
                        
						$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 14)));
                        $html_content = $mailTemplateData['EmailTemplate']['content'];
                        $html = '';
                        /*$html .= str_replace('{{name}}', $this->request->data['name'], $html_content);
                        $html = str_replace('{{email}}', $this->request->data['email'], $html);
                        $html = str_replace('{{mobile}}', $this->request->data['mobile'], $html);*/
					   $html .= str_replace('{{name}}', $name, $html_content);
                       $html = str_replace('{{email_content}}', nl2br($this->request->data['descriptive_ans']), $html);
						
						
                        $email_subject = $mailTemplateData['EmailTemplate']['title'];
						//$admin_email = 'dinesh.amstech@gmail.com';
                       // $email = $this->getAdminValue('contact_email');
                        //$email = 'dinesh.amstech@gmail.com';
                        $mailFrom = $this->getAdminValue('contact_email');
                        $FromName = '';

                        $mail = new PHPMailer;
                        $mail->FromName = $FromName;
                        $mail->From = $mailFrom;
                        $mail->Subject = $email_subject;
                        $mail->Body = stripslashes($html);
                        $mail->AltBody = stripslashes($html);
                        $mail->IsHTML(true);
                        $mail->AddAddress($email);
                        $mail->Send();
						$this->redirect(array('controller' => 'homes', 'action' => 'mail_sent'));
						 //$this->Session->setFlash(__('Email has been successfully sent to yor email address.'), 'success');
						
					  }
					 
	}
	
	
	public function subscribe_news_letter() {
					$this->layout = 'landing_layout';
					$this->Session->delete('emailErr');
					$ERROR = 0;
					 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
						// pr($this->data);die;
						if(empty(trim($this->request->data['User']['email']))){
								$ERROR = 1;
								$this->Session->write('emailErr', 'Please enter email');
								$this->redirect($this->referer());
							} else {
								$email = $this->request->data['User']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->Session->write('emailErr', 'Please enter valid email format');
									$this->redirect($this->referer());
								}
							} 
							
							if($ERROR == 0){
								 $this->Session->setFlash(__('Your email has been subscribed successfully.'), 'success');
								$this->redirect($this->referer());
							}
					 }
				}
	
				public function index() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
				}
				
				
				
				
				
				 public function contact_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower('Contact us')));
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
					 
					  if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
						 // pr($this->data);die;
                        $mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 10)));
                        $html_content = $mailTemplateData['EmailTemplate']['content'];
                        $html = '';
                        $html .= str_replace('{{name}}', $this->request->data['name'], $html_content);
                        $html = str_replace('{{email}}', $this->request->data['email'], $html);
                        $html = str_replace('{{mobile}}', $this->request->data['mobile'], $html);
                        $html = str_replace('{{address}}', $this->request->data['address'], $html);
                        $html = str_replace('{{contact_info}}', nl2br($this->request->data['other_info']), $html);
                        $email_subject = $mailTemplateData['EmailTemplate']['title'];
						//$admin_email = 'dinesh.amstech@gmail.com';
                        $email = $this->getAdminValue('contact_email');
                        $mailFrom = $this->request->data['email'];
                        $FromName = $this->request->data['name'];

                        $mail = new PHPMailer;
                        $mail->FromName = $FromName;
                        $mail->From = $mailFrom;
                        $mail->Subject = $email_subject;
                        $mail->Body = stripslashes($html);
                        $mail->AltBody = stripslashes($html);
                        $mail->IsHTML(true);
                        $mail->AddAddress($email);
                        $mail->Send();
						 $this->Session->setFlash(__('Email has been successfully sent to site admin.'), 'success');
						
					  }
					 
					 
					 
				}
				
				
				
				
				
			
				
				 public function high_school_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 8,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$this->ExaminationCategory->recursive = 1;
					/*$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																					 'Examination.examination_category_id' => array(38,39)
																		 )));*/
																		 
																		 
					
					//added on 08-01-2018 start
					$this->Examination->recursive = -1;
					
					$ExamFlagArr38 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>38 
																 )));
					$ExamFlagArr39 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>39 
																 )));
					
					$flgAr38 = array();
					$flgAr39 = array();
					$examCatIds = array();
					
					foreach($ExamFlagArr38 as $key => $val){
						$flgAr38[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr39 as $key => $val){
						$flgAr39[] = $val['Examination']['exam_flag'];
					}
					
					if(in_array(2,$flgAr38)){
						$examCatIds[] = 38;
					} 
					
					if(in_array(2,$flgAr39)){
						$examCatIds[] = 39;
					}													 
																		 
																		 
																		 
																		 
																		 
					
					
					
					 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => array(38,39)
																 
																 )));
					
					
					$allExamLiteracyArr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => $examCatIds
																 
																 )));
					
					
					$SiteVideo = $this->SiteVideo->find('first', array(
															 'conditions' => array(
																			 'SiteVideo.isdeleted' => 0,
																 )));
					
					$this->set(compact('allExamAr', 'allExamLiteracyArr', 'SiteVideo'));
					$this->render('parent_student_page');
					
					
					//added on 08-01-2018 End
					
					//pr($allExamLiteracyArr);
				}
				
				public function primary_student() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 7,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					/*$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																					 'Examination.examination_category_id' => array(42,43)
																		 )));*/
					
					 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => array(42,43)
																 )));
					
					
					 $SiteVideo = $this->SiteVideo->find('first', array(
															 'conditions' => array(
																			 'SiteVideo.isdeleted' => 0,
																 )));
					
					
					$this->set(compact('allExamAr','SiteVideo'));
					$this->render('parent_student_page');
					
				}
				
				 public function high_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 6,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 1, //for online /school
																					 'Examination.examination_category_id' => array(36,37)
																		 )));
					
					
					$SiteVideo = $this->SiteVideo->find('first', array(
															 'conditions' => array(
																			 'SiteVideo.isdeleted' => 0,
																 )));
					
					$this->set(compact('allExamAr','SiteVideo'));
					
					$this->render('school_content_page');
					//$this->render('common_content_display');
				}
				
				 public function primary_school() {
					$this->layout = 'landing_layout';
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 5,'ContentManagement.isdeleted' => 0)));
					$this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					$allExamAr = $this->Examination->find('all', array(
																	 'order' => 'Examination.examination_category_id ASC',
																	 'conditions' => array(
																					 'Examination.isdeleted' => 0,  
																					 'Examination.examination_type_id' => 1, //for online /school
																					 'Examination.examination_category_id' => array(40,41)
																		 )));
					
					
					 $SiteVideo = $this->SiteVideo->find('first', array(
															 'conditions' => array(
																			 'SiteVideo.isdeleted' => 0,
																 )));
					
					
					
					
					$this->set(compact('allExamAr','cmsData','SiteVideo'));
					$this->render('school_content_page');
					//$this->render('common_content_display');
				}
				
				
				
				
				public function sample_questions($examination_id = NULL, $exam_section_id = NULL, $flag = NULL) {
					
					 //$this->layout = 'take_exam_layout';
					 $this->layout = 'take_exam_layout_special';
					 $examination_id = $this->data_decrypt($examination_id);
					 $exam_section_id = $this->data_decrypt($exam_section_id);
					 $this->set(compact('exam_section_id'));
					 $this->Examination->recursive = 2; 
					 $flag = $this->data_decrypt($flag); //this flag for literacy writing(2) or normal(1)
					 $ExamDetails = $this->Examination->find('first', array( 
																				//'fields' => 'id, title, description',	
																				 'conditions' => array('Examination.id' => $examination_id)
																				 ));
					
					 $QuestionAns = $this->Question->find('all', array( 
																		'limit' => 8,
																		'offset' => 18,
																		'order' => 'Question.question_order_front ASC',
																		 'conditions' => array(
																		 'Question.examination_id' => $examination_id,
																		 'Question.exam_section_id' => $exam_section_id,
																		 //'Question.section' => $section,  
																		 'Question.isdeleted' => 0
																		 )
																		 ));
					
					
					
					
					 //$examDuration = !empty($ExamDetails['Examination']['exam_duration']) ? $ExamDetails['Examination']['exam_duration'] : 40;//minutes
					 if($exam_section_id == 1){
						 $examDuration = 65;
					 } else if($exam_section_id == 2 || $exam_section_id == 3){
						 $examDuration = 45;
					 } else if($exam_section_id == '' && !empty($ExamDetails['Examination']['exam_duration'])){
						 $examDuration = $ExamDetails['Examination']['exam_duration'];
					 } else {
						 $examDuration = 40;
					 }
					 
					 $this->set(compact('QuestionAns','examDuration','ExamDetails'));
					 
					 if(!empty($flag) && $flag == 2){
						  $QuestionAns = $this->Question->find('all', array( 
																		'limit' => 1,
																		//'offset' => 18,
																		'order' => 'Question.question_order_front ASC',
																		 'conditions' => array(
																		 //'Question.examination_id' => $examination_id,
																		 'Question.id' => 964, //for sampal literacy 
																		 'Question.exam_section_id' => $exam_section_id,  
																		 'Question.isdeleted' => 0
																		 )
																		 ));
						  $this->set(compact('QuestionAns','examDuration','ExamDetails'));
						 $this->render('sample_literacy_writing');
					 }
					 //pr($ExamDetails);die;
				}
				
				
				 public function about_us() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 $studentdata1 = $this->UserConfiguration->getStudentData();
					 $student_id = $studentdata1['Student']['id'];
					 $studentDetails = $this->Student->find('first', array('Student.id' => $student_id));
					 $this->set(compact('studentDetails'));
					 
					 $cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 4,'ContentManagement.isdeleted' => 0)));
					 $this->set(compact('cmsData'));
					 $this->set('title_for_layout', 'Learner Leader : '.ucwords(strtolower($cmsData['ContentManagement']['title'])));
					 $this->render('common_content_display');
				}
				
				 public function get_about_us_content() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'ajax';
					 $cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 4,'ContentManagement.isdeleted' => 0)));
					 return $cmsData;
					// pr($cmsData);
				}
				
				
				 public function company_information() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Company information');
					$this->set('page_heading', 'Company information');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 3,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				
				 public function terms_of_service() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Terms of service');
					$this->set('page_heading', 'Terms of service');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 2,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function privacy_policy() {
					$this->layout = 'landing_layout';
					$this->set('title_for_layout', 'Privacy policy');
					$this->set('page_heading', 'Privacy policy');
					$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					$this->set(compact('cmsData'));
					$this->render('common_content_display');
				}
				
				 public function we_offer($page = NULL) {
					$this->layout = 'landing_layout';
					//$cmsData = $this->ContentManagement->find('first', array('conditions' => array('ContentManagement.id' => 1,'ContentManagement.isdeleted' => 0)));
					//$this->set(compact('cmsData'));
					
					$page = str_replace('-', ' ', $page);
					$this->set('title_for_layout', 'We offer '.$page);
					$this->set('page_heading', $page);
					
					$this->render('common_content_display');
				}
				
				
				public function send_mail() {
				   // $this->UserConfiguration->isStudentLoggedIn();
					$this->layout = 'landing_layout';
					 
					  if ($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)) {
						  //print_r($this->request->data);die;
                        //$mailTemplateData = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.id' => 10)));
                        //print_r($mailTemplateData);die;
                        $html_content = trim($this->request->data['Email']['message']);
                        $html = '';
                        $html .= $html_content;
                        $email_subject = trim($this->request->data['Email']['subject']);
                        $email_subject = trim($this->request->data['Email']['subject']);
                        $attachment = 0;
                        if(trim($this->request->data['Email']['attachment']['name']))
                        {
                        	$file = trim($this->request->data['Email']['attachment']['name']);
                        	$randomDirectory = time().$file;
                        	$uploadfile = WWW_ROOT . 'files/school/attachments/' . $randomDirectory;
									if (move_uploaded_file($this->request->data['Email']['attachment']['tmp_name'], $uploadfile)) {
                					$attachment = 1;
            					} else {
                					$attachment = 0;
           						}
                        }
                        $email = trim($this->request->data['Email']['sendmailid']);
                        $mailFrom = $this->getAdminValue('contact_email');
                        $FromName = $this->getAdminValue('contact_email');

                        $mail1 = new PHPMailer;
                        $mail1->FromName = $FromName;
                        $mail1->From = $mailFrom;
                        $mail1->Subject = $email_subject;
                        $mail1->Body = stripslashes($html);
                        if($attachment == 1){
                        	$mail1->AddAttachment($uploadfile);
                        }
                        $mail1->AltBody = stripslashes($html);
                        $mail1->IsHTML(true);
                        $mail1->AddAddress($email);
                        $mail1->Send();
						      $this->Session->setFlash(__('Email has been successfully sent.'), 'success');
								$this->redirect('/contact-us/');
					 }
					 
					 
					 
				}
				
	
}
?>