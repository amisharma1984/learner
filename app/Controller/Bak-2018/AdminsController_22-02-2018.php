<?php

App::uses('AppController', 'Controller');

class AdminsController extends AppController {

    public $name = 'Admins';
    public $uses = array('Admin','Student','MyExam','StudentAnswer','MyExamStudentAnswer','Examination');
        
    public function index() {
        $this->layout = 'login';
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['Admin']['login']);
            $password = $this->request->data['Admin']['password'];
            $userdata = $this->Admin->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setAdminData($userdata);
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'index'));
            }
        }else{
            $adminData = $this->UserConfiguration->getAdminData();
            if (!empty($adminData)) {
                $this->Session->setFlash(__('You have already logged in.'), 'info');
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            }
        }
    }
    
    public function login() {
        $this->layout = 'login';       
        
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['Admin']['login']);
            $password = $this->request->data['Admin']['password'];
            $userdata = $this->Admin->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setAdminData($userdata);
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'login'));
            }
        }else{
            $adminData = $this->UserConfiguration->getAdminData();
            if (!empty($adminData)) {
                $this->Session->setFlash(__('You have already logged in.'), 'info');
                $this->redirect(array('controller' => 'admins', 'action' => 'home'));
            }
        }
    }

    public function home() {
        $this->layout = 'admin';
        $admindata = $this->UserConfiguration->getAdminData('App.adminData');
        if (empty($admindata)) {
            $this->Session->setFlash(__('You are logged out!'), 'info');
            $this->redirect(array('controller' => 'admins', 'action' => 'login'));
        }
    }

    public function logout() {
        $this->UserConfiguration->adminLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        $this->redirect(array('controller' => 'admins', 'action' => 'login'));
    }

	
	public function delete_test_user($id = NULL) {
			$this->layout = 'admin';
			if($this->Student->delete($id)){
				
				$this->MyExam->query("DELETE FROM `my_exams` WHERE `my_exams`.`student_id` = '".$id."'");
				$this->StudentAnswer->query("DELETE FROM `student_answers` WHERE `student_answers`.`student_id` = '".$id."'");
				$this->MyExamStudentAnswer->query("DELETE FROM `my_exam_student_answers` WHERE `my_exam_student_answers`.`student_id` = '".$id."'");
				
				$this->Session->setFlash(__('Test user deleted successfully.'), 'success');
			    $this->redirect(array('controller' => 'admins', 'action' => 'test_user_list'));
			}
			
			
		}
		
		public function delete_test_school($id = NULL) {
			$this->layout = 'admin';
			if($this->School->delete($id)){
				$this->Session->setFlash(__('Test school deleted successfully.'), 'success');
			    $this->redirect(array('controller' => 'admins', 'action' => 'test_school_list'));
			}
			
			
		}
	
	
	
	 public function management_access() {
        
        $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
		$testUserListAr = $this->Student->find('all',array(
											'order' => 'Student.id DESC',
											'conditions' => array(
															'Student.account_type' => 'test',
															'Student.isdeleted' => 0
															))); 
		$this->set(compact('testUserListAr','adminData'));
	}
    public function test_user_list() {
        
        $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
		$testUserListAr = $this->Student->find('all',array(
											'order' => 'Student.id DESC',
											'conditions' => array(
															'Student.account_type' => 'test',
															'Student.isdeleted' => 0
															))); 
		$this->set(compact('testUserListAr','adminData'));
	}
	
	 public function select_student_exams() {
	  $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
		 	
										
			//added on 08-01-2018 start
					//$this->Examination->recursive = -1;
					
					$ExamFlagArr38 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>38 
																 )));
					$ExamFlagArr39 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>39 
																 )));
					
					
					$ExamFlagArr42 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>42 
																 )));
					$ExamFlagArr43 = $this->Examination->find('all', array(
															 'order' => 'Examination.examination_category_id ASC',
															 'conditions' => array(
																			 'Examination.isdeleted' => 0,  
																			 'Examination.examination_type_id' => 2, //for practice exam/student/paraents
																			 'Examination.examination_category_id' =>43 
																 )));
					
					$flgAr38 = array();
					$flgAr39 = array();
					$flgAr42 = array();
					$flgAr43 = array();
					
					$examCatIds = array();
					
					foreach($ExamFlagArr38 as $key => $val){
						$flgAr38[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr39 as $key => $val){
						$flgAr39[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr42 as $key => $val){
						$flgAr42[] = $val['Examination']['exam_flag'];
					}
					
					foreach($ExamFlagArr43 as $key => $val){
						$flgAr43[] = $val['Examination']['exam_flag'];
					}
					
					
					if(in_array(2,$flgAr38)){
						$examCatIds[] = 38;
					} 
					
					if(in_array(2,$flgAr39)){
						$examCatIds[] = 39;
					}													 
					
					if(in_array(2,$flgAr42)){
						$examCatIds[] = 42;
					} 
					
					if(in_array(2,$flgAr43)){
						$examCatIds[] = 43;
					}	
					
		 
		 
			//examination_categories	
			 $allExamAr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 //'Examination.examination_category_id' => $examination_category_id
																 )));	



			$allExamLiteracyArr = $this->ExaminationCategory->find('all', array(
															 'conditions' => array(
																			 'ExaminationCategory.isdeleted' => 0,  
																			 'ExaminationCategory.examination_type_id' => 2, //for practice exam/student/paraents
																			 'ExaminationCategory.id' => $examCatIds
																 
																 )));													 
		
		 $this->set(compact('studentDetails', 'allExamAr', 'allExamLiteracyArr'));
		 
		
    }
	
	
	public function edit_test_user($id = NULL) {
        
        $this->layout = 'admin';
		$this->set(compact('id'));
        if($this->request->is('post') || $this->request->is('put')){
			//pr($this->data);die;
			$ERROR = 0;
            $updatedPassword = '';
            if(!empty($this->request->data['Student']['n_password'])){
                $updatedPassword = md5(trim($this->request->data['Student']['n_password'])); 
            }
			
            if(!empty($updatedPassword)){
                $this->request->data['Student']['password'] = $updatedPassword;
                $this->request->data['Student']['normal_password'] = $this->request->data['Student']['n_password'];
            }
            
			        if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							
							
							
							if(empty(trim($this->request->data['Student']['n_password'])) && !empty(trim($this->request->data['Student']['confirm_password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							
							 if(!empty(trim($this->request->data['Student']['n_password'])) && strlen($this->request->data['Student']['n_password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['Student']['n_password'])) && strlen($this->request->data['Student']['n_password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['Student']['confirm_password'])) && !empty(trim($this->request->data['Student']['n_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['Student']['n_password'];
							$cpass = $this->request->data['Student']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['Student']['n_password'] != $this->request->data['Student']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
			
			
         
             $this->Student->id = $id;
			 $this->request->data['Student']['account_type'] = 'Test';
			 if($ERROR == 0){
                $this->Student->save($this->request->data);
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'test_user_list'));
                
            }
			
			
                       
        }
            
        if(!empty($id)){
            //unset($adminData['Admin']['password']);
            $this->request->data = $this->Student->read(null, $id);
        }
                
    }
    
	
	public function add_test_user() {
        
        $this->layout = 'admin';
        if($this->request->is('post') || $this->request->is('put')){
			//pr($this->data);die;
			$ERROR = 0;
            $updatedPassword = '';
            if(!empty($this->request->data['Student']['n_password'])){
                $updatedPassword = md5(trim($this->request->data['Student']['n_password'])); 
            }
			
            if(!empty($updatedPassword)){
                $this->request->data['Student']['password'] = $updatedPassword;
                $this->request->data['Student']['normal_password'] = $this->request->data['Student']['n_password'];
            }
            
			        if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							
						
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['Student']['n_password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['Student']['n_password'])) && strlen($this->request->data['Student']['n_password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['Student']['n_password'])) && strlen($this->request->data['Student']['n_password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['Student']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['Student']['n_password'];
							$cpass = $this->request->data['Student']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['Student']['n_password'] != $this->request->data['Student']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
			 $this->request->data['Student']['account_type'] = 'Test';
			 if($ERROR == 0){
                $this->Student->save($this->request->data);
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'test_user_list'));
                
            }
			
			
           
                
        }
             
    }
    
	//test school section
	
	 public function test_school_list() {
        
        $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
		$testUserListAr = $this->School->find('all',array(
											'order' => 'School.id DESC',
											'conditions' => array(
															'School.account_type' => 'test',
															'School.isdeleted' => 0
															))); 
		$this->set(compact('testUserListAr', 'adminData'));
	}
	
	
	public function select_school_exams($id = NULL) {
        
        $this->layout = 'admin';
		$this->set(compact('id'));
		$this->Examination->unbindModel(
										array('hasMany' => array('Question'))
										);
														
			
		
		 $allExamAr = $this->Examination->find('all', array(
							'order' => 'ExaminationCategory.name ASC',
							'conditions' => array(
								 'Examination.isdeleted' => 0, 
								 'Examination.examination_type_id' => 1
								 ))); // examination_type_id =1 means online exam
		//pr($allExamAr);die;
		 $this->set(compact('allExamAr'));
		
		
	}
	
	
	public function edit_test_school($id = NULL) {
        
        $this->layout = 'admin';
		$this->set(compact('id'));
        if($this->request->is('post') || $this->request->is('put')){
			//pr($this->data);die;
			$ERROR = 0;
            $updatedPassword = '';
            if(!empty($this->request->data['School']['n_password'])){
                $updatedPassword = md5(trim($this->request->data['School']['n_password'])); 
            }
			
            if(!empty($updatedPassword)){
                $this->request->data['School']['password'] = $updatedPassword;
                $this->request->data['School']['normal_password'] = $this->request->data['School']['n_password'];
            }
            
			         if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please head teacher name');
							}
							
							
							
							
							if(empty(trim($this->request->data['School']['n_password'])) && !empty(trim($this->request->data['School']['confirm_password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							
							 if(!empty(trim($this->request->data['School']['n_password'])) && strlen($this->request->data['School']['n_password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['School']['n_password'])) && strlen($this->request->data['School']['n_password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['School']['confirm_password'])) && !empty(trim($this->request->data['School']['n_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['School']['n_password'];
							$cpass = $this->request->data['School']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['School']['n_password'] != $this->request->data['School']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
			
			
         
             $this->School->id = $id;
			 $this->request->data['School']['account_type'] = 'Test';
			 if($ERROR == 0){
                $this->School->save($this->request->data);
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'test_school_list'));
                
            }
			
			
                       
        }
            
        if(!empty($id)){
            //unset($adminData['Admin']['password']);
            $this->request->data = $this->School->read(null, $id);
        }
                
    }
    
	
	
	public function add_school() {
        
        $this->layout = 'admin';
		
		 if((($this->request->is('post') || $this->request->is('put')) && @$this->request->data['page_name'] == 'checkout') ){
					$exam_ids = '';
					 foreach($this->request->data['Examination']['ids'] as $val){
						$exam_ids .= '#'.$val;
					 }
					 
					 $exam_ids =  ltrim($exam_ids, '#');
					
					 $this->Session->write('exam_ids',$exam_ids);
					 $exam_ids = $this->Session->read('exam_ids');
					  $this->set(compact('exam_ids'));
					 /*$allExamAr = $this->Examination->find('all', array(
													 'conditions' => array(
													 'Examination.id' => $this->request->data['Examination']['ids']
													 ))); */
			}
			$this->render('add_test_school');
	
	}
	
	
	public function add_test_school() {
        
        $this->layout = 'admin';
		
		
					 $exam_ids = $this->Session->read('exam_ids');
					 $this->set(compact('exam_ids'));
		
        if($this->request->is('post') || $this->request->is('put') && @$this->request->data['page_name'] == 'add_test_school'){
			//pr($this->data);die;
			$ERROR = 0;
            $updatedPassword = '';
            if(!empty($this->request->data['School']['n_password'])){
                $updatedPassword = md5(trim($this->request->data['School']['n_password'])); 
            }
			
            if(!empty($updatedPassword)){
                $this->request->data['School']['password'] = $updatedPassword;
                $this->request->data['School']['normal_password'] = $this->request->data['School']['n_password'];
            }
            
			        if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please head teacher name');
							}
							
							
						
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							if(empty(trim($this->request->data['School']['n_password']))){
								$ERROR = 1;
								$this->set('pwderror', 'Please enter password');
							}
							 if(!empty(trim($this->request->data['School']['n_password'])) && strlen($this->request->data['School']['n_password']) < 6){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							if(!empty(trim($this->request->data['School']['n_password'])) && strlen($this->request->data['School']['n_password']) > 20){
								$ERROR = 1;
								$this->set('pwderror', 'Password must be 6 to 20 characters');
							} 
							
							
							if(empty(trim($this->request->data['School']['confirm_password']))){
								$ERROR = 1;
								$this->set('cpwderror', 'Please enter confirm password');
							}
							
							
							$pass = $this->request->data['School']['n_password'];
							$cpass = $this->request->data['School']['confirm_password'];
							if(!empty($pass) && !empty($cpass) && $this->request->data['School']['n_password'] != $this->request->data['School']['confirm_password']){
								$ERROR = 1;
								$this->set('cpwderror', 'Password and confirm password does not match');
							}
							
			 $this->request->data['School']['account_type'] = 'Test';
			 if($ERROR == 0){
                $this->School->save($this->request->data);
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'test_school_list'));
                
            }
			
			
           
                
        }
             
    }
    
	
	
	
	
    public function profile() {
        
        $this->layout = 'admin';
        $adminData = $this->UserConfiguration->getAdminData();
        if($this->request->is('post') || $this->request->is('put')){
            
            if(empty($this->request->data['Admin']['id'])){
                $this->Session->setFlash(__('Internal error. Please try again.'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
            }
            
            $updatedPassword = '';
            if(!empty($this->request->data['Admin']['password'])){
                if ($this->request->data['Admin']['password'] != $this->request->data['Admin']['repaetpassowrd']) {
                    $this->Session->setFlash(__('Password mismatch, please enter same password in Repeat-Password field.'), 'error');
                    $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
                }else{
                    $updatedPassword = md5(trim($this->request->data['Admin']['password'])); 
                }
            }
            
            $dataToUpdate = array(
                'first_name' => trim($this->request->data['Admin']['first_name']),
                'last_name' => trim($this->request->data['Admin']['last_name']),
                'email' => trim($this->request->data['Admin']['email']),
                'mobile' => trim($this->request->data['Admin']['mobile']),
                'contact_email' => trim($this->request->data['Admin']['contact_email']),
                'global_email' => trim($this->request->data['Admin']['global_email']),
                'modified' => date('YmdHis')
            );
            
            if(!empty($updatedPassword)){
                $dataToUpdate['password'] = $updatedPassword;
            }
            
            $this->Admin->read(null, $this->request->data['Admin']['id']);
            $this->Admin->set($dataToUpdate);
            
            if($this->Admin->save()){
                
                $this->UserConfiguration->setAdminData($this->Admin->read(null, $this->Admin->id));
                $this->Session->setFlash(__('Profile information updated.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'profile'));
                
            }
                       
        }
            
        if(!empty($adminData)){
            unset($adminData['Admin']['password']);
            $this->request->data = $adminData;
            $this->set('admindata', $adminData);
        }else{
            $this->Session->setFlash(__('You are logged out!'), 'info');
            $this->redirect(array('controller' => 'admins', 'action' => 'login'));
        }
                
    }
    
    public function checkAdminStatus(){
        if($this->request->is('ajax')){
            
            $isLoggedIn = $this->Session->check('App.adminData');
            if (!$isLoggedIn) {        
                $message = 'You have logged out! Please login to continue.';
                echo json_encode(array(
                    'status' => 'Success', 
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();             
            }else{
                $message = 'An internal error occurred.';
                echo json_encode(array(
                    'status' => 'Error', 
                    'message' => $message,
                    'messageTimeOut' => Configure::read('App.Static.notification_messages_timeout_by_letter'),
                    'messageLetterCount' => strlen($message)
                    )
                );
                exit();
            }
            
        }
    }
    
    public function password(){
        
        if($this->request->is('post')){
            
            $isAdmin = $this->Admin->findByEmail(trim($this->request->data['Password']['email']));
            if(!empty($isAdmin)){
                
                $key = Configure::read('Security.salt');
                $encryptedResult = Security::encrypt($isAdmin['Admin']['id'], $key);
                $encryptedResult = urlencode($encryptedResult);
                
                $userData = array(
                    'id' => $isAdmin['Admin']['id'],
                    'email' => $isAdmin['Admin']['email'],
                    'fullName' => $isAdmin['Admin']['first_name'].' '.$isAdmin['Admin']['last_name'],
                    'resetLink' => Router::url('/', true).'admins/password_reset/'.$encryptedResult
                );
                
                if($this->sendPasswordResetEmail($userData, 'Admin')){
                    $this->Session->setFlash(__('Check your email for a link to reset your password. If it doesn\'t appear within a few minutes, check your spam folder.'), 'success');
                    $this->redirect(array('controller' => 'admins', 'action' => 'password'));
                }else{
                    $this->Session->setFlash(__('Email sending error'), 'error');
                    $this->redirect(array('controller' => 'admins', 'action' => 'password'));
                }
            }else{
                $this->Session->setFlash(__('That email address doesn\'t match any user accounts. Are you sure you\'ve registered?'), 'error');
            }
        }
        
        
    }
    
    public function password_reset($resetKey = null){
        
        $resetKey = urldecode($resetKey);
        $this->set('resetKey', urldecode($resetKey));
        if($this->request->is('post') || $this->request->is('put')){
            
            if ($this->request->data['Admin']['password'] != $this->request->data['Admin']['repeatpassowrd']) {
                $this->Session->setFlash(__('Password mismatch, please enter same password in Repeat-Password field.'), 'error');
            }else{
                $updatedPassword = md5(trim($this->request->data['Admin']['password'])); 
                $key = Configure::read('Security.salt');
                $userId = Security::decrypt($resetKey, $key);
                $this->Admin->read(null, $userId);
                $this->Admin->set('password', $updatedPassword);
                $this->Admin->save();
                $this->Session->setFlash(__('Password changed successfully.'), 'success');
                $this->redirect(array('controller' => 'admins', 'action' => 'login'));
            }
        }
        
        if(!empty($resetKey)){
            
            $key = Configure::read('Security.salt');
            $userId = Security::decrypt($resetKey, $key);
            $userData = $this->Admin->findById($userId);
            if(!empty($userData)){
                $this->set('userData', $userData);
            }else{
                $this->Session->setFlash(__('Wrong password reset key. Please try again.'), 'error');
                $this->redirect(array('controller' => 'admins', 'action' => 'password'));
            }
        }else{
            $this->Session->setFlash(__('Wrong password reset key. Please try again.'), 'error');
            $this->redirect(array('controller' => 'admins', 'action' => 'password'));
        }
        
    }
}
?>