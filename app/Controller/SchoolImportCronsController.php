<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SchoolImportCronsController extends AppController {
    
    const STATUS_NEW = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_COMPLETE = 2;
        
    public $name = 'SchoolImportCrons';
    public $uses = array('SchoolImport','School', 'Setting', 'SchoolInformation');
    public $helpers = array('Form', 'Html');
    public $commonUtilities;

    public function beforeFilter() {
        
        parent::beforeFilter();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function process() {
        
        //echo "GD: ", extension_loaded('gd') ? 'OK' : 'MISSING', '<br>';
        //echo "XML: ", extension_loaded('xml') ? 'OK' : 'MISSING', '<br>';
        //echo "zip: ", extension_loaded('zip') ? 'OK' : 'MISSING', '<br>';
        //echo phpinfo();
        //exit();
        
        App::uses('CommonUtilitiesHelper', 'View/Helper');
        $this->commonUtilities = new CommonUtilitiesHelper(new View());
        
        $imports = $this->_getPendingExcelSheets();
        if (count($imports) > 0) {
            
            App::import('Vendor', 'IOFactory', array('file' => 'PHPExcel-1.8' . DS . 'Classes' . DS . 'PHPExcel' . DS . 
'IOFactory.php'));
            
            CakeLog::write('debug', 'There are ' . count($imports) . ' import(s) waiting for process.');
            
            foreach ($imports as $import) {
                CakeLog::write('debug', 'Processing File Id :' . $import['SchoolImport']['id']);
                
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_PROCESSING);
                $this->_startImport($import);
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_COMPLETE);
                
            }
        }
    }
    
    protected function _getPendingExcelSheets() {
        $imports = $this->SchoolImport->find('all', array('conditions' => array('status' => self::STATUS_NEW)));
        return $imports;
    }
    
    protected function _startImport($import) {
        
        
        $inputFileName = WWW_ROOT . 'files/school/imports/' . $import['SchoolImport']['path'];
        // $pathinfo = pathinfo($inputFileName);
        // pr($pathinfo);
        // exit();

        // $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
        $objReader->setReadDataOnly(true);

        $objPHPExcel = $objReader->load($inputFileName);
        $objWorksheet = $objPHPExcel->getActiveSheet();
        

        // echo '<table border="1" cellpadding="0" cellspacing="0">' . "\n";
        $rowCount = 2;
        $total = $already = 1;
        foreach ($objWorksheet->getRowIterator($rowCount) as $row) {
            if(!$this->isRowEmpty($row)){

            // echo '<tr>' . "\n";

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,
                // even if it is not set.
                // By default, only cells
                // that are set will be
                // iterated.
                
                //$cellValue = $objWorksheet->getCell($cell->getColumn().$cell->getRow())->getValue();
                
                $email = $objWorksheet->getCell('P'.$rowCount)->getValue();
                
                $check = $this->School->findByEmail(trim($email));
            
                if (!empty($check)) {
                    /* Email already exist in school table */
                    $already++;
                } else {

                    /* Inser Data To School Model */
                    $dataForSchool = array(
                        'School' => array(
                            'name' => $objWorksheet->getCell('A'.$rowCount)->getValue(),
                            //'manager_name' => $objWorksheet->getCell('AI'.$rowCount)->getValue(),
                            'email' => $email,
                            'password' => $this->_randomPassword(8),
                            'isdeleted' => 1,
                            'created' => date('YmdHis')
                        )
                    );
//                    $this->School->create();
//                    $this->School->save($dataForSchool);
//                    $schoolId = $this->School->id;
//                    $this->School->clear();


                    $address = (!empty($objWorksheet->getCell('C'.$rowCount)->getValue()))?$objWorksheet->getCell('C'.$rowCount)->getValue():$objWorksheet->getCell('G'.$rowCount)->getValue();
                    $town = (!empty($objWorksheet->getCell('D'.$rowCount)->getValue()))?$objWorksheet->getCell('D'.$rowCount)->getValue():$objWorksheet->getCell('H'.$rowCount)->getValue();
                    $zip =  (!empty($objWorksheet->getCell('F'.$rowCount)->getValue()))?$objWorksheet->getCell('F'.$rowCount)->getValue():$objWorksheet->getCell('J'.$rowCount)->getValue();
                    $state = (!empty($objWorksheet->getCell('E'.$rowCount)->getValue()))?$objWorksheet->getCell('E'.$rowCount)->getValue():$objWorksheet->getCell('I'.$rowCount)->getValue();
                    $levelValue = (!empty($objWorksheet->getCell('M'.$rowCount)->getValue()))?ucwords(strtolower($objWorksheet->getCell('M'.$rowCount)->getValue())):'';
                    $typeValue = (!empty($objWorksheet->getCell('N'.$rowCount)->getValue()))?ucwords(strtolower($objWorksheet->getCell('N'.$rowCount)->getValue())):'';
                    
                    $url = (!empty($objWorksheet->getCell('Q'.$rowCount)->getValue()))?$this->commonUtilities->getUrlWithProtocol($objWorksheet->getCell('Q'.$rowCount)->getValue()):'';
                    //CommonUtilities
                    
                    /* Insert Data for SchoolInformation Model */
                    $dataForSchoolInformation = array(
                        'SchoolInformation' => array(
    //                        'school_id' => $schoolId,
                            'level' => $levelValue,
                            'type' => $typeValue,
                            'street' => $address,
                            'town' => $town,
                            'zip' => $zip,
                            'district' => $objWorksheet->getCell('AA'.$rowCount)->getValue(),
                            'state' => $state,
                            'phone' => $objWorksheet->getCell('K'.$rowCount)->getValue(),
                            'fax' => $objWorksheet->getCell('L'.$rowCount)->getValue(),
                            'isprevious' => 0,
                            'url' => $url,
                            'isdeleted' => 1,
                            'created' => date('YmdHis')
                        )
                    );
//                    $this->SchoolInformation->create();
//                    $this->SchoolInformation->save($dataForSchoolInformation);
//                    $schoolInformationId = $this->SchoolInformation->id;
//                    $this->SchoolInformation->clear();
                    $finalData = $dataForSchool + $dataForSchoolInformation;
                    
                    if (!empty($finalData)) {
                        // Use the following to avoid validation errors:
                        unset($this->School->SchoolInformation->validate['school_id']);
                        $this->School->saveAssociated($finalData);
                    }
                    
                    $total++;
                }
                
                //foreach ($cellIterator as $cell) {
                    
                    //if (!is_null($cell)) {
                        
                        // echo '<td>';
                        // echo 'ROW: '. $cell->getRow().'<br>';
                        // echo 'COLUMN: '. $cell->getColumn().'<br>';
                        // echo 'COORDINATE: '. $cell->getCoordinate().'<br>';
                        // echo 'RAW VALUE: '. $cell->getValue().'<br>';
                            
                    //}
                    // echo '</td>';

                    //echo '<td>' . $cell->getValue() . '</td>' . "\n";
                //}
                // echo '</tr>' . "\n";
            }
            $rowCount++;
        }
        //echo '</table>' . "\n";
        
        $this->SchoolImport->read(null, $import['SchoolImport']['id']);
        $this->SchoolImport->set(array(
            'imported' => $total
        ));
        $this->SchoolImport->save();
        $this->SchoolImport->clear();
        
        $msg = '';
        if ($total > 0)
            $msg .= __('%d school was imported!', $total);
        if ($already > 0)
            $msg .= ($total > 0 ? " | " : "") . __('%d emails have already exist.', $already);
        
        $msg = 'From '.$import['SchoolImport']['path'].' '.$msg;
        CakeLog::write('debug', $msg);
        CakeLog::write('debug', '-------------------------------------------------------------\n\n');
    }    
    
    public static function isRowEmpty($row) {
        foreach ($row->getCellIterator() as $cell) {
            if ($cell->getValue()) {
                return false;
            }
        }

        return true;
    }

}
?>