$(function () {
 
    $('#addSchoolForm').validate({
        rules: {
            "data[School][name]":{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            "data[School][email]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
            },
			"data[School][password]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
               
            },
			
			"data[School][confirm_password]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
			
            "data[School][manager_email]": {
                email: true
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#newSchoolModal').find('.modal-footer').find('#saveSchoolData').prop('disabled', true);
            
            var options = {
                success: saveSchoolData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }

    });
    
    function saveSchoolData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#newSchoolModal').find('.modal-footer').find('#saveSchoolData').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#addSchoolSuccess').html(htmlString).fadeIn(500);
            if(response.status != 'Error'){
                $form.resetForm();
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                    $('#newSchoolModal').modal('hide');
                    $('#addSchoolSuccess').css('display', 'none');
                    window.location = location.href;
                }, timeOutValue);
            }
            
        }
        
    }
    
    $('#saveSchoolData').click(function(event){
        $('#addSchoolForm').submit();
    });
    
    $('.view-school').click(function(){
        
        var schoolId = $(this).data('school');
        
    });
    
    $('#schoolViewModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('school') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        
        var jqxhrViewSchool = $.ajax({
            type: "POST",
            url: BASEURL + "school_managements/view",
            data: {
                schoolId: recipient
            },
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    
                    var result = JSON.parse(response);
                    modal.find('.modal-title').text(result.School.name);
                    _.templateSettings.variable = "data";
                                        
                    var template = _.template(
                        $('#schoolViewTemplate').html()
                    );
                    
                    modal.find('.modal-body').html(template(result));                    
                }

            }
        });
        
    });
    
    $('#newSchoolModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-school');
        var schoolInfoId = button.attr('data-schoolInfo');
        var modal = $(this);
        
        $('#addSchoolSuccess').css('display', 'none');
        modal.find('#addSchoolForm').resetForm();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update School Informations');
            modal.find('#addSchoolForm').attr('action', BASEURL + 'school_managements/edit/'+recipient + '/' +schoolInfoId);
            
            var jqxhrViewSchool = $.ajax({
                type: "POST",
                url: modal.find('#addSchoolForm').attr('action'),
                success: function (response) {
                    //console.log(response);
                    if(response != ''){

                        var result = JSON.parse(response);
                        modal.find('#SchoolName').val(result.School.name);
                        modal.find('#SchoolEmail').val(result.School.email);
                        modal.find('#SchoolManagerName').val(result.School.manager_name);
                        modal.find('#SchoolManagerEmail').val(result.School.manager_email);
                        
                        modal.find('#SchoolInformationStreet').val(result.SchoolInformation.street);
                        modal.find('#SchoolInformationLevel').val(result.SchoolInformation.level);
                        modal.find('#SchoolInformationType').val(result.SchoolInformation.type);
                        modal.find('#SchoolInformationTown').val(result.SchoolInformation.town);
                        modal.find('#SchoolInformationDistrict').val(result.SchoolInformation.district);
                        modal.find('#SchoolInformationState').val(result.SchoolInformation.state);
                        modal.find('#SchoolInformationZip').val(result.SchoolInformation.zip);
                        modal.find('#SchoolInformationPhone').val(result.SchoolInformation.phone);
                        modal.find('#SchoolInformationFax').val(result.SchoolInformation.fax);
                        modal.find('#SchoolInformationUrl').val(result.SchoolInformation.url);
                        if(result.SchoolInformation.isprevious){
                            modal.find('#SchoolInformationIsPrevious').prop('checked', true);
                        }   
                    }

                }
            });
            
        }else{
            modal.find('.modal-title').text('Add New School');
            modal.find('#addSchoolForm').attr('action', BASEURL + 'school_managements/add');
        }
        
    });
    
    //schoolDeleteConfirm
    $('#deleteSchoolModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.data('school-name');
        var recipientId = button.data('school');
        var modal = $(this);
        
        var schoolCount = $('.dataTable').find('.select-school:checked').length;
        
        if(schoolCount > 0 && recipientId === undefined){
            modal.find('.modal-footer').find('.schoolDeleteConfirm').attr('data-isChecked', 'true');
            if(schoolCount > 1){
                modal.find('.modal-title').text('Delete schools');
                modal.find('.modal-body').html('<p>Are you sure you want to delete all selected schools?.</p>');
            }else{
                modal.find('.modal-title').text('Delete School');
                modal.find('.modal-body').html('<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            }
            
        }else if(recipientId !== undefined && $('#schoolName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.schoolDeleteConfirm').attr('data-isChecked', 'true');
            modal.find('.modal-title').text('Delete school');
            //modal.find('.modal-body').html('<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-body').html('<p>Are you sure you want to delete the selected schools ?</p>');
            modal.find('.modal-footer').find('.schoolDeleteConfirm').attr('data-schoolId', recipientId);
            
        }else{
            modal.find('.modal-title').text('Delete school');
            modal.find('.modal-body').html('<p>Before continue please, select a school.</p>');
            modal.find('.modal-footer').find('.schoolDeleteConfirm').attr('data-isChecked', 'false');
            modal.find('.modal-footer').find('.schoolDeleteConfirm').attr('data-schoolId', '');
        }            
        
    });
    
    /* Login credentials */
    $('#loginCredentialsModal').on('show.bs.modal', function (event) {
        
        var modal = $(this);
        modal.find('.modal-footer').find('.schoolCredentialsConfirm').attr('data-school', '');
        modal.find('.modal-footer').find('.schoolCredentialsConfirm').prop('disabled', true);
        
        var button = $(event.relatedTarget);
        var recipient = button.data('school-name');
        var recipientId = button.data('school');
        var modalType = button.data('modal-type'); /* data-modal-type="login-credentials" disabled */
        
        var schoolCount = $('.dataTable').find('.select-school:checked').length;
        
        if($('#schoolName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.schoolCredentialsConfirm').prop('disabled', false);
            modal.find('.modal-footer').find('.schoolCredentialsConfirm').attr('data-school', recipientId);
            modal.find('.modal-title').text('School Login Credentials');
           // modal.find('.modal-body').html('<p>Are you sure you want to send longin credentials to <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-body').html('<p>Are you sure you want to activate longin credentials to <strong>'+recipient+'</strong>?</p>');
        }else{
            //modal.find('.modal-title').text('School Login Credentials');
            modal.find('.modal-title').text('School Login Activate');
            modal.find('.modal-body').html('<p>Before continue please, select a school.</p>');
        }            
        
    });
    
    $('.dataTable').find('.select-school').click(function(){
        
        var total = $('.dataTable').find('.select-school').length;
        var checkedCount = $('.dataTable').find('.select-school:checked').length;
        
        if(total != checkedCount){
            $('.selectAllSchoolButton').val('Select All');
            $('.selectAllSchoolButton').attr('data-action', 'checkedAll');
        }else{
            $('.selectAllSchoolButton').val('Deselect All');
            $('.selectAllSchoolButton').attr('data-action', 'unCheckedAll');
        }
        
    });
    
    $('.selectAllSchoolButton').click(function(event){
        var checked = false;
        if($(this).attr('data-action') != 'unCheckedAll'){
            checked = true;
            $(this).attr('data-action', 'unCheckedAll');
            $(this).val('Deselect All');
        }else{
            $(this).attr('data-action', 'checkedAll');
            $(this).val('Select All');
        }
        
        $('.dataTable').find('.select-school').each(function(index, element){
            $(element).prop('checked', checked);
        });
        
    });
    
    $('.schoolDeleteConfirm').click(function(event){
        
        var parentScope = $(this);
        
        if($(this).attr('data-isChecked') == 'true'){
            
            var schoolId = $(this).attr('data-schoolId');
            parentScope.prop('disabled', true);
            var schoolIds = [];
            $('.dataTable').find('.select-school:checked').each(function(index, element){
                schoolIds.push($(element).val());
            });
            
            var selectedId = (schoolId !== undefined && schoolId != '')?schoolId:schoolIds.toString();
            selectedId = schoolIds;
			
            var jqxhrDeleteSchool = $.ajax({
                type: "POST",
                url: BASEURL + 'school_managements/delete',
                data:{
                    schoolIds: selectedId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var results = JSON.parse(response);
                        var message;
                        if(results.length > 1){
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> All the selected schools have been deleted.'; 
                            message += '</div>';
                        }else{
                            parentScope.attr('data-schoolId', '');
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> Your School has been deleted.'; 
                            message += '</div>';
                        }
                        
                        $('#deleteSchoolModal').find('.modal-body').html(message);
                        
                        setTimeout(function(){
                            
                            if(results.length == $('.dataTable').find('.select-school').length){
                                window.location = BASEURL + 'school_managements';
                            }else{
                                window.location = location.href;
                            }
                            
                        }, 3000);
                        
                    }

                }
            });
            
        }else{
            $('#deleteSchoolModal').modal('hide');
        }
        
    });
    
    $('.schoolCredentialsConfirm').click(function(event){
        var parentScop = $(this);
        var schoolId = $(this).attr('data-school');
        $(this).prop('disabled', true);
        
        if(schoolId !== undefined && schoolId != ''){
            
            var jqxhrDeleteSchool = $.ajax({
                type: "POST",
                url: BASEURL + 'school_managements/send_login_credentials',
                data:{
                    schoolId: schoolId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        parentScop.prop('disabled', false);
                        var results = JSON.parse(response);
                        var className = (results.status == 'Error')?'alert-danger':'alert-success';
                        var statusString = (results.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';

                        var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
                        htmlString += '<div class="alert '+className+'" role="alert">';
                        htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                        htmlString += '<strong>'+statusString+'</strong> '+results.message; 
                        htmlString += '</div>';
                        htmlString += '</div>';
                        $('#loginCredentialsModal').find('.modal-body').html(htmlString).fadeIn(500);
                        
                        if(results.status != 'Error'){
                            $('.dataTable').find('#schoolName' + schoolId).prop('checked', false);
                            var timeOutValue = parseInt(results.messageLetterCount) * parseInt(results.messageTimeOut);
                            setTimeout(function (){
                                $('#loginCredentialsModal').find('.modal-footer').find('.schoolCredentialsConfirm').attr('data-school', '');
                                $('#loginCredentialsModal').modal('hide');
                            }, timeOutValue);
                        }  
                    }
                }
            });
            
        }else{
            $('#loginCredentialsModal').modal('hide');
        }
        
    });
    
});
