/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Referrence : http://stackoverflow.com/questions/2086287/how-to-clear-jquery-validation-error-messages
 * use like this way :  $("#formId").clearValidation();
 */

$.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};

