$(function () {
    
    getEmailTemplatePreview({});
    
    tinymce.init({
        selector: '.textEditor',
        valid_elements : '*[*]',
        valid_children : '+body[style], +body[script]',
        autoresize_min_height: 250,
        autoresize_max_height: 550,
        theme: 'modern',
        element_format : 'html',
        plugins: [
            'autoresize advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: BASEURL + 'app/uploadEditorImageFiles',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
    
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];
      
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);
      
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
            };
    
            input.click();
        },
        relative_urls: false,
        convert_urls: false,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
            BASEURL + 'css/custom-editor-style.css'
        ]
    });
    
    $('#emailTemplateCreateUpdateModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-email-template');
        var modal = $(this);
        
        modal.find('#emailTemplateSuccess').css('display', 'none');
        modal.find('#emailTemplateForm').resetForm();
        modal.find("#emailTemplateForm").clearValidation();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update Template');
            
            var jqxhrEmailTemplate = $.ajax({
                type: "POST",
                url: BASEURL + 'email_systems/view_mail_template/'+recipient,
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var result = JSON.parse(response);
                        modal.find('#EmailTemplateTitle').val(result.EmailTemplate.title);
                        modal.find('#EmailTemplateId').val(result.EmailTemplate.id);
                        tinymce.get('EmailTemplateContent').setContent(result.EmailTemplate.content);
                    }

                }
            });
            
        }else{
            modal.find('.modal-title').text('Create Template');
        }
    });
    
    $('#saveTemplateButton').click(function(event){
        $('#emailTemplateForm').submit();
    });
    
    $('#emailTemplateForm').validate({
        rules: {
            'data[EmailTemplate][title]': {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            }
        },
        submitHandler: function (form) {
            
            var editorContent = tinymce.get('EmailTemplateContent').getContent();
            if(editorContent) {
                $('#EmailTemplateContent').val(editorContent);
            }
            var options = {
                success: saveEmailTemplateData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
            
        }
    });
    
    function saveEmailTemplateData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
                htmlString += '<div class="alert '+className+'" role="alert">';
                htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                htmlString += '<strong>'+statusString+'</strong> '+response.message; 
                htmlString += '</div>';
                htmlString += '</div>';

            $('#emailTemplateSuccess').html(htmlString).slideToggle();
            if(response.status != 'Error'){
                $form.resetForm();
                tinymce.get('EmailTemplateContent').setContent('');
                                
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                    $('#emailTemplateSuccess').slideToggle();
                    $('#emailTemplateCreateUpdateModal').modal('hide');
                    getEmailTemplatePreview(response);
                }, timeOutValue);
            }        
        }
    }
        
    $('#deleteEmailTemplateModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var templateId = button.attr('data-email-template');
        var modal = $(this);
                
        var data = button.data();
        var keys = $.map(data , function(value, key) { 
            return key; 
        });//console.log(keys);
        for(var i = 0; i < keys.length; i++) {
            if($.inArray(keys[i], ['previous', 'next']) !== -1){
                modal.find('.modal-footer').find('.templateDeleteConfirm').attr('data-' + keys[i], button.attr('data-' + keys[i]));
            }
        }
        modal.find('.modal-footer').find('.templateDeleteConfirm').attr('data-templateId', templateId);
        modal.find('.modal-title').text('Delete Template');
        modal.find('.modal-body').html('<p>Are you sure you want to delete this mail template?</p>');       
        
    });
    
    $('.templateDeleteConfirm').click(function(event){
        var parentScope = $(this),
            templateId  = $(this).attr('data-templateId'),
            ajaxUrl, dataToSend = {};
            
        ajaxUrl = BASEURL + 'email_systems/delete_mail_template';
        dataToSend['templateId'] = parentScope.attr('data-templateId');
        dataToSend['previousId'] = (parentScope.attr('data-previous'))?parentScope.attr('data-previous'):'';
        dataToSend['nextId'] = (parentScope.attr('data-next'))?parentScope.attr('data-next'):'';
         
        var jqxhrDeleteMailTemplate = $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: dataToSend,
            success: function (response) {
                //console.log(response);
                if(response != ''){

                    var results = JSON.parse(response);console.log(results);
                    var className = (results.status == 'Error')?'alert-danger':'alert-success';
               
                    var statusString = (results.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';

                    var htmlString = '<div class="row"><div class="col-md-12 col-xs-12 col-sm-12 text-center">';
                    htmlString += '<div class="alert '+className+'" role="alert">';
                    htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                    htmlString += '<strong>'+statusString+'</strong> '+results.message; 
                    htmlString += '</div>';
                    htmlString += '</div></div>';

                    $('#deleteEmailTemplateModal').find('.modal-body').html(htmlString);
                    if(results.status != 'Error'){
                        
                        var timeOutValue = parseInt(results.messageLetterCount) * parseInt(results.messageTimeOut);
                        setTimeout(function (){
                            
                            removeAllData('#deleteEmailTemplateModal');
                            getEmailTemplatePreview(results);
                            
                        }, timeOutValue);
                    }
                }
            }
        });

    });
    
});

$('#emailtemplatID').change(function(){
	event.preventDefault();
    var templateId = $(this).val();
    
    var queryData = {
        templateId: templateId
    };
    getEmailTemplatePreview(queryData);
});


$(document).on('click', '.preNextButton', function(event){
    event.preventDefault();
    
    var templateId = $(this).attr('data-template');
        
    var queryData = {
        templateId: templateId
    };
    getEmailTemplatePreview(queryData);
});

function removeAllData(modalSelector){
    
    var modal = $(modalSelector);
    var data = modal.find('.modal-footer').find('.templateDeleteConfirm').data();
    var keys = $.map(data , function(value, key) { 
        return key; 
    });
    for(var i = 0; i < keys.length; i++) {
        if($.inArray(keys[i], ['previous', 'next', 'templateId']) !== -1){
            modal.find('.modal-footer').find('.templateDeleteConfirm').removeAttr('data-' + keys[i]);
        }
    }
    $(modalSelector).modal('hide');
    return true;
}

function setEnailTemplateIframeContent(){
  
    var iframe = document.getElementById('emailTemplateFrame'),
        iframedoc = iframe.contentWindow.document || iframe.contentDocument;

    iframedoc.body.innerHTML = $('#templateHiddenContent').html();
    $('#templateHiddenContent').remove();
    
}

function getEmailTemplatePreview(params){
        
    var queryData = {};
    
    if(params.templateId){
        queryData['templateId'] = params.templateId;
    }else{
        queryData['templateId'] = '';
    }
    
    var jqxhrPreViewTemplate = $.ajax({
        type: "POST",
        url: BASEURL + "email_systems/preview_mail_template",
        data: queryData,
        success: function (response) {
            //console.log(response);
            if(response != ''){
                $('#templatePreviewArea').html(response);
                if(!$('#templatePreviewArea').is(':visible')){
                    $('#templatePreviewArea').fadeIn(1000);
                }
            }else{
                $('#templatePreviewArea').html('');
            }

        }
    });
    
}