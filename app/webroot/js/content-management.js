$(function(){
   
    $('#contentAddEditForm').validate({
        rules: {
            "data[ContentManagement][title]":{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            "data[ContentManagement][page_name]":{
                required:true
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#contentAddEditModal').find('.modal-footer').find('#saveExamData').prop('disabled', true);
            $('#ContentManagementDescription').val(tinymce.activeEditor.getContent());
            
            var options = {
                success: saveExaminationData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    function saveExaminationData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#contentAddEditModal').find('.modal-footer').find('#saveExamData').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#examAddEditMessage').html(htmlString).fadeIn(500);
            
            if(response.status != 'Error'){
                $form.resetForm();
                $('#ContentManagementDescription').val('');
                tinymce.activeEditor.setContent('');
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function (){
                    $('#contentAddEditModal').modal('hide');
                    $('#examAddEditMessage').css('display', 'none');
                    if(response.examId){
                        //window.location = BASEURL + 'question_managements/questions/exam:'+response.examId;
                        window.location = BASEURL + 'content_managements/index/';
                    }else{
                        window.location = location.href;
                    }
                }, timeOutValue);
            }
            
        }
    }
    
    $('#contentAddEditModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.data('exam');
        var modal = $(this);
		//alert(modal);
        
        $('#examAddEditMessage').css('display', 'none');
        modal.find('#contentAddEditForm').resetForm();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update Content');
            modal.find('#contentAddEditForm').attr('action', BASEURL + 'content_managements/edit_content/'+recipient);
            
            var jqxhrViewExam = $.ajax({
                type: "POST",
                url: BASEURL + 'content_managements/view_content/'+recipient,
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var result = JSON.parse(response);
                        modal.find('#ContentManagementPageName').val(result.ContentManagement.page_name);
                        modal.find('#ContentManagementTitle').val(result.ContentManagement.title);
                       // modal.find('#ExaminationExaminationTypeId').val(result.ContentManagement.examination_type_id);
                        //modal.find('#ExaminationExaminationCategoryId').val(result.ContentManagement.examination_category_id);
                        tinymce.activeEditor.setContent(result.ContentManagement.description);
                    }

                }
            });
            
        }else{
            modal.find('.modal-title').text('Add Content');
            modal.find('#contentAddEditForm').attr('action', BASEURL + 'content_managements/add_content');
        }
    });
    
    $('#saveExamData').click(function(event){
        $('#contentAddEditForm').submit();
    });
    
    $('.sub-categoryFinder').click(function(event){
        
        event.preventDefault();
        var examId = $(this).data('exam');
        var examName = $(this).data('exam-name');
               
        
    });
    
    $('.set-question-handler').click(function(event){
        event.preventDefault();
        var examId = $(this).data('exam');
        var examName = $(this).data('exam-name');
        window.location = BASEURL + 'question_managements/questions/exam:' + examId;         
    });
    
    $('.dataTable').find('.select-exam').click(function(){
        
        var total = $('.dataTable').find('.select-exam').length;
        var checkedCount = $('.dataTable').find('.select-exam:checked').length;
        
        if(total != checkedCount){
            $('.selectAllExamButton').val('Select All');
            $('.selectAllExamButton').attr('data-action', 'checkedAll');
        }else{
            $('.selectAllExamButton').val('Deselect All');
            $('.selectAllExamButton').attr('data-action', 'unCheckedAll');
        }
        
    });
    
    $('.selectAllExamButton').click(function(event){
        var checked = false;
        if($(this).attr('data-action') != 'unCheckedAll'){
            checked = true;
            $(this).attr('data-action', 'unCheckedAll');
            $(this).val('Deselect All');
        }else{
            $(this).attr('data-action', 'checkedAll');
            $(this).val('Select All');
        }
        
        $('.dataTable').find('.select-exam').each(function(index, element){
            $(element).prop('checked', checked);
        });
        
    });
    
    $('#deleteExamModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.data('exam-name');
        var recipientId = button.data('exam');
        var modal = $(this);
        
        var schoolCount = $('.dataTable').find('.select-exam:checked').length;
        var deleteNote = '<p><strong>Please note</strong> that all related questions and answers will be deleted.</p>';
        if(schoolCount > 0 && recipientId === undefined){
            modal.find('.modal-footer').find('.examDeleteConfirm').attr('data-isChecked', 'true');
            if(schoolCount > 1){
                modal.find('.modal-title').text('Delete Examinations');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete all selected examinations?</p>');
            }else{
                modal.find('.modal-title').text('Delete Examination');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            }
            
        }else if(recipientId !== undefined && $('#examName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.examDeleteConfirm').attr('data-isChecked', 'true');
            modal.find('.modal-title').text('Delete Examination');
            modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-footer').find('.examDeleteConfirm').attr('data-examId', recipientId);
            
        }else{
            modal.find('.modal-title').text('Delete Examination');
            modal.find('.modal-body').html('<p>Before continue please, select a examination.</p>');
            modal.find('.modal-footer').find('.examDeleteConfirm').attr('data-isChecked', 'false');
            modal.find('.modal-footer').find('.examDeleteConfirm').attr('data-examId', '');
        }            
        
    });
    
    $('.examDeleteConfirm').click(function(event){
        var parentScope = $(this);
        if($(this).attr('data-isChecked') == 'true'){
            
            $('#deleteExamModal').find('.modal-body').html('<p>Please be patient. Examinations deleting...</p>');
            
            var examId = $(this).attr('data-examId');
            $(this).prop('disabled', true);
            var examIds = [];
            $('.dataTable').find('.select-exam:checked').each(function(index, element){
                examIds.push($(element).val());
            });
            
            var selectedId = (examId !== undefined && examId != '')?examId:examIds.toString();
            
            var jqxhrDeleteExam = $.ajax({
                type: "POST",
                url: BASEURL + 'content_managements/delete_exam',
                data:{
                    examIds: selectedId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        
                        var results = JSON.parse(response);
                        var message;
                        if(results.length > 1){
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> All the selected examinations have been deleted.'; 
                            message += '</div>';
                        }else{
                            parentScope.attr('data-examId', '');
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> Selected examination has been deleted.'; 
                            message += '</div>';
                        }
                        
                        $('#deleteExamModal').find('.modal-body').html(message);
                        
                        setTimeout(function(){
                            if(results.length == $('.dataTable').find('.select-exam').length){
                                window.location = BASEURL + 'content_managements';
                            }else{
                                window.location = location.href;
                            }
                        }, 3000);
                        
                    }

                }
            });
            
        }else{
            $('#deleteExamModal').modal('hide');
        }
        
    });
    
    $(document).on('change', '.examTypeCategory', function(event){
        var actionName = $(this).attr('data-action-name');
            
        var $parentScope = $(this);
        var selectedValue = $(this).val();
            selectedValue = ((actionName == 'Type') && (selectedValue == ''))?'all':selectedValue;
        
        if(selectedValue){
            var jqxhrAvailability = $.ajax({
                type: "POST",
                url: BASEURL + 'content_managements/getExamTypeOrCategory',
                data:{actionName: actionName, selectedValue: selectedValue},
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var results = JSON.parse(response);
                        if(results.ExaminationType !== undefined && results.ExaminationType.id){
                            $('#contentAddEditForm').find('#ExaminationExaminationTypeId').val(results.ExaminationType.id);
                        }else{
                            var htmlStr = '<option value="">- Select Exam Category -</option>';
                            for(var key in results){
                                htmlStr += '<option value="'+ key +'">'+ results[key] +'</option>';
                            }
                            $('#contentAddEditForm').find('#ExaminationExaminationCategoryId').html(htmlStr);
                        }
                    }
                }
            });
        }else{
            $('#contentAddEditForm').find('.examTypeCategory').each(function(index, element){
                $(element).val('');
            });
        }
        
    });
    
    $(document).on('click', '.previewExamination', function(event){
        event.preventDefault();
        
        var examId = $(this).attr('data-exam');
        var redirectTo = BASEURL + 'skill_tests/question/exam:'+examId;
        var windowOpen = window.open(redirectTo, '_blank');
            windowOpen.focus();
        
    });
    
});