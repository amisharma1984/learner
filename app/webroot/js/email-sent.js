$('#schoolViewModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('school') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		alert(recipient);
        var modal = $(this);
        
        var jqxhrViewSchool = $.ajax({
            type: "POST",
            url: BASEURL + "school_managements/emailcontect",
            data: {
                emailId: recipient
            },
            success: function (response) {
                console.log(response);
                if(response != ''){
                    
                    var result = JSON.parse(response);
                    modal.find('.modal-title').text(result.School.name);
                    _.templateSettings.variable = "data";
                                        
                    var template = _.template(
                        $('#schoolViewTemplate').html()
                    );
                    
                    modal.find('.modal-body').html(template(result));                    
                }

            }
        });
        
    });