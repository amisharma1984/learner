$(function(){
   
    $('#subjectAddEditForm').validate({
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#subjectAddEditModal').find('.modal-footer').find('#saveSubjectData').prop('disabled', true);
            
            var options = {
                success: saveSubjectData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    function saveSubjectData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#subjectAddEditModal').find('.modal-footer').find('#saveSubjectData').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#subjectAddEditMessage').html(htmlString).fadeIn(500);
            if(response.status != 'Error'){
                $form.resetForm();
                
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function (){
                    $('#subjectAddEditModal').modal('hide');
                    $('#subjectAddEditMessage').css('display', 'none');
                    window.location = location.href;
                }, timeOutValue);
            }
        }
    }
    
    $('#subjectAddEditModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-subject');
        var modal = $(this);
        
        $('#subjectAddEditMessage').css('display', 'none');
        modal.find('#subjectAddEditForm').resetForm();
        $("#subjectAddEditForm").clearValidation();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update Subject');
            modal.find('#subjectAddEditForm').attr('action', BASEURL + 'examination_managements/edit_subject/'+recipient);
            
            var jqxhrViewSubject = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/view_subject/'+recipient,
                success: function (response) {
                    
                    if(response != ''){
                        var result = JSON.parse(response);
                        modal.find('#SubjectName').val(result.Subject.name);
                        modal.find('#SubjectDescription').val(result.Subject.description);
                    }
                }
            });
            
        }else{
            modal.find('.modal-title').text('Add Subject');
            modal.find('#subjectAddEditForm').attr('action', BASEURL + 'examination_managements/add_subject');
        }
    });
    
    $('#saveSubjectData').click(function(event){
        $('#subjectAddEditForm').submit();
    });
    
    $('.sub-subjectFinder').click(function(event){
        
        event.preventDefault();
        var subjectId = $(this).attr('data-subject');
        var subjectName = $(this).attr('data-subject-name');
        if(subjectId !== undefined && subjectId != ''){
            window.location = BASEURL + 'examination_managements/types/parent_id:'+subjectId;
        }else{
            alert(subjectName + ' does\'t have any sub-examination type.')
        }        
        
    });
    
    $('.dataTable').find('.select-subject').click(function(){
        
        var total = $('.dataTable').find('.select-subject').length;
        var checkedCount = $('.dataTable').find('.select-subject:checked').length;
        
        if(total != checkedCount){
            $('.selectAllSubjectButton').val('Select All');
            $('.selectAllSubjectButton').attr('data-action', 'checkedAll');
        }else{
            $('.selectAllSubjectButton').val('Deselect All');
            $('.selectAllSubjectButton').attr('data-action', 'unCheckedAll');
        }
        
    });
    
    $('.selectAllSubjectButton').click(function(event){
        var checked = false;
        if($(this).attr('data-action') != 'unCheckedAll'){
            checked = true;
            $(this).attr('data-action', 'unCheckedAll');
            $(this).val('Deselect All');
        }else{
            $(this).attr('data-action', 'checkedAll');
            $(this).val('Select All');
        }
        
        $('.dataTable').find('.select-subject').each(function(index, element){
            $(element).prop('checked', checked);
        });
        
    });
    
    $('#deleteSubjectModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-subject-name');
        var recipientId = button.attr('data-subject');
        var modal = $(this);
        
        var subjectCount = $('.dataTable').find('.select-subject:checked').length;
        var deleteNote = '<p><strong>Please note</strong> that all related questions and answers will be deleted.</p>';
        if(subjectCount > 0 && recipientId === undefined){
            modal.find('.modal-footer').find('.subjectDeleteConfirm').attr('data-isChecked', 'true');
            if(subjectCount > 1){
                modal.find('.modal-title').text('Delete Subjects');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete all selected subjects?</p>');
            }else{
                modal.find('.modal-title').text('Delete Subject');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            }
            
        }else if(recipientId !== undefined && $('#subjectName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.subjectDeleteConfirm').attr('data-isChecked', 'true');
            modal.find('.modal-title').text('Delete Subject');
            modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-footer').find('.subjectDeleteConfirm').attr('data-subjectId', recipientId);
            
        }else{
            modal.find('.modal-title').text('Delete Subject');
            modal.find('.modal-body').html('<p>Before continue please, select a subject.</p>');
            modal.find('.modal-footer').find('.subjectDeleteConfirm').attr('data-isChecked', 'false');
            modal.find('.modal-footer').find('.subjectDeleteConfirm').attr('data-subjectId', '');
        }            
        
    });
    
    $('.subjectDeleteConfirm').click(function(event){
        var parentScope = $(this);
        if($(this).attr('data-isChecked') == 'true'){
            
            $('#deleteSubjectModal').find('.modal-body').html('<p>Please be patient. Exam types are deleting...</p>');
            
            var subjectId = $(this).attr('data-subjectId');
            $(this).prop('disabled', true);
            var subjectIds = [];
            $('.dataTable').find('.select-subject:checked').each(function(index, element){
                subjectIds.push($(element).val());
            });
            
            var selectedId = (subjectId !== undefined && subjectId != '')?subjectId:subjectIds.toString();
            
            var jqxhrDeleteSubject = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/delete_subjects',
                data:{
                    subjectIds: selectedId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        
                        var results = JSON.parse(response);
                        var message;
                        if(results.length > 1){
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> All the selected types have been deleted.'; 
                            message += '</div>';
                        }else{
                            parentScope.attr('data-subjectId', '');
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> Selected exam type has been deleted.'; 
                            message += '</div>';
                        }
                        
                        $('#deleteSubjectModal').find('.modal-body').html(message);
                        
                        setTimeout(function(){
                            
                            if(results.length == $('.dataTable').find('.select-subject').length){
                                window.location = BASEURL + 'examination_managements/types';
                            }else{
                                window.location = location.href;
                            }
                            
                        }, 3000);
                        
                    }

                }
            });
            
        }else{
            $('#deleteSubjectModal').modal('hide');
        }
        
    });
    
});