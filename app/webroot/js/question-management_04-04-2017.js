$(function () {
    
    var EXAM_ID = $('#QuestionExaminationId').val();
    if(EXAM_ID){
        getQuestionPreview({examinationId:EXAM_ID});
    }
    
    tinymce.init({
        selector: '.textEditor',
        autoresize_min_height: 200,
        autoresize_max_height: 350,
        theme: 'modern',
        plugins: [
            'autoresize advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: BASEURL + 'app/uploadEditorImageFiles',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
    
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];
      
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);
      
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
            };
    
            input.click();
        },
        relative_urls: false,
        convert_urls: false,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
            BASEURL + 'css/custom-editor-style.css'
        ]
    });
    
    
//    $.validator.addMethod("validateEditor", function (value, element, params) {
//        alert('Hello');
//        var editorContent = tinymce.activeEditor.getContent();
//        
//        return this.optional(element) || value == editorContent;
//    }, $.validator.format("Please enter a question."));
    
    $('#questionAddEditForm').validate({
        rules: {
            'data[Question][examination_title]': {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            }
        },
        submitHandler: function (form) {
            
            var editorContent = tinymce.get('QuestionTitle').getContent();
            
            if(editorContent) {
                $('#QuestionTitle').val(editorContent);
                var options = {
                    success: saveQuestionData  // post-submit callback 
                };
                $(form).ajaxSubmit(options);
            }else{
                alert('Please enter a question.');
            }
        }
    });
    
    function saveQuestionData(responseText, statusText, xhr, $form) {
        
        scrollTopForSpecificArea('#questionSectionDivId');
        
        if (responseText != '') {
            
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';

            $('#questionAddEditMessage').html(htmlString).slideToggle();
            if(response.status != 'Error'){
                //$form.resetForm();
                
                if($('#questionAddEditForm').find('#QuestionId').length){
                    $('#questionAddEditForm').attr('action', BASEURL + 'question_managements/create_question');
                    $('#QuestionId').remove();
                    $('#QuestionSection').val('');
                    $('#QuestionAnswerType').val('');
                }
                
                $('#questionAddEditForm').find('#QuestionSubjectId').val('');
                tinymce.get('QuestionTitle').setContent('');
                $('#questionAddEditForm').find('#QuestionCorrectMarks').val('');
                $('#questionAddEditForm').find('#QuestionWrongMarks').val('');
                
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                    $('#questionAddEditMessage').slideToggle();
                    scrollTopForSpecificArea('#answerSectionDivId');
                    getQuestionPreview(response);
                }, timeOutValue);
            }
                        
        }
        
    }
    
    $('#answerAddEditForm').validate({
        submitHandler: function (form) {
            
            var editorContent = tinymce.get('AnswerAnswerText').getContent();
            var editorContentDes = tinymce.get('AnswerAnsDescription').getContent();
            if(editorContent) {
                $('#AnswerAnswerText').val(editorContent);
                $('#AnswerAnsDescription').val(editorContentDes);
                var options = {
                    success: saveAnswerData  // post-submit callback 
                };
                $(form).ajaxSubmit(options);
            }else{
                alert('Please enter a answer.');
            }
        }
    });
    
    function saveAnswerData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';

            $('#answerAddEditMessage').html(htmlString).slideToggle();
            if(response.status != 'Error'){
                //$form.resetForm();
                if($('#answerAddEditForm').find('#AnswerId').length){
                    $('#answerAddEditForm').attr('action', BASEURL + 'question_managements/create_answer');
                    $('#AnswerId').remove();
                }
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function (){
                    $('#answerAddEditMessage').slideToggle();
                    tinymce.get('AnswerAnswerText').setContent('');
                    tinymce.get('AnswerAnsDescription').setContent('');
                    $('#AnswerAnswerText').val('');
                    $('#AnswerAnsDescription').val('');
                    $('#AnswerIscorrect').prop('checked', false);
                    
                    getQuestionPreview(response);
                }, timeOutValue);
            }            
        }
    }
    
    /* For auto complete */
    $('#QuestionExaminationTitle').typeahead({
        ajax: {
            url: BASEURL + 'question_managements/examTypeHeadSearch',
            method: 'post',
            triggerLength: 2,
            displayField: 'title',
            valueField: 'id',
            loadingClass: "loading-circle",
            preDispatch: function (query) {
                //showLoadingMask(true);
                return {
                    search: query
                }
            },
            preProcess: function (data) {           
                var dataList = [];
                if(data){
                    for(var i = 0; i < data.length; i++){
                        var temp = {
                            id : data[i].id + '-' + data[i].examination_type_id + '-' + data[i].examination_category_id,
                            title : data[i].title
                        };
                        dataList.push(temp);
                    }
                }else{
                    $('#QuestionExaminationTitle').val('');
                }
                return dataList;
            }
        },
        onSelect: function(item) {
            //console.log(item);
            //QuestionExaminationId
            $('#QuestionExaminationTypeId').prop('disabled', false);
            $('#QuestionExaminationCategoryId').prop('disabled', false);
            var dataSplit = item.value.split('-'),
                id = dataSplit[0],
                typeId = dataSplit[1],
                categoryId = dataSplit[2];
            
            $('#QuestionExaminationId').val(id);
            $('#QuestionExaminationTitle').removeClass('error').addClass('valid');
            $('#QuestionExaminationTitle-error').css('display', 'none');
            
            $('#QuestionExaminationTypeId').val(typeId).removeClass('error').addClass('valid');
            $('#QuestionExaminationTypeId-error').css('display', 'none');
            $('#QuestionExaminationTypeId').prop('disabled', true);
            
            $('#QuestionExaminationCategoryId').val(categoryId).removeClass('error').addClass('valid');
            $('#QuestionExaminationCategoryId-error').css('display', 'none');
            $('#QuestionExaminationCategoryId').prop('disabled', true);
            getQuestionPreview({examinationId:id});
        }
        
    });
    
    $('#QuestionExaminationTitle').on('change keyup', function(){
        if($(this).val() == ''){
            $('#QuestionExaminationTypeId').prop('disabled', false);
            $('#QuestionExaminationTypeId').val('');
            
            $('#QuestionExaminationCategoryId').prop('disabled', false);
            $('#QuestionExaminationCategoryId').val('');
        }
    });
    
    $('.checkInteger').on('change keyup', function(event){
        var inputValue = $(this).val();
            inputValue = inputValue.trim();
        if(isNaN(inputValue)){
            $(this).val('');
        }else{
            inputValue = Math.abs(inputValue);
            /* For checking Integer value */
            if(inputValue % 1 !== 0){
                $(this).val('');
            }else{
                /* For setting min value for wrong marks */
                var minValue;
                if(event.target.id == 'QuestionWrongMarks'){
                    minValue = Math.abs($('#QuestionCorrectMarks').val());
                    $('#QuestionWrongMarks').attr('min', '-' + minValue);
                }else{
                    $('#QuestionWrongMarks').attr('min', '-' + inputValue);
                }
                
            }
        }
    });
    
    $(document).on('click', '.preNextButton', function(event){
        event.preventDefault();
        var examId = $(this).data('exam');
        var questionId = $(this).data('question');
            $('#AnswerQuestionId').val(questionId);
        var queryData = {
            examinationId: examId,
            questionId: questionId
        };
        getQuestionPreview(queryData);
    });
    
    $(document).on('click', '.editQuestionButton', function(event){
        event.preventDefault();
        var questionId = $(this).data('question');
        
        var jqxhrViewQuestion = $.ajax({
            type: "POST",
            url: BASEURL + 'question_managements/read_question',
            data:{questionId:questionId},
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    var result = JSON.parse(response);
                    $('#questionAddEditForm').attr('action', BASEURL + 'question_managements/update_question');
                    $('#QuestionExaminationTitle').val(result.Examination.title);
                    $('#QuestionExaminationId').val(result.Examination.id);
                    $('#QuestionExaminationTypeId').val(result.Examination.examination_type_id);
                    $('#QuestionExaminationCategoryId').val(result.Examination.examination_category_id);
                    $('#QuestionSubjectId').val(result.Question.subject_id);
                    $('#QuestionSection').val(result.Question.section);
                    $('#QuestionAnswerType').val(result.Question.answer_type);
                    tinymce.get('QuestionTitle').setContent(result.Question.title);
                    $('#QuestionCorrectMarks').val(result.Question.correct_marks);
                    $('#QuestionWrongMarks').val(result.Question.wrong_marks);
                    
                    if($('#questionAddEditForm').find('#QuestionId').length){
                        $('#questionAddEditForm').find('#QuestionId').val(result.Question.id);
                    }else{
                        $('#questionAddEditForm').prepend('<input type="hidden" name="data[Question][id]" value="' + result.Question.id + '" id="QuestionId">');
                    }
                    scrollTopForSpecificArea('#questionSectionDivId');
                }
            }
        });
        
    });
    
    $(document).on('click', '.editAnswerButton', function(event){
        event.preventDefault();
        var questionId = $(this).attr('data-question');
        var answerId = $(this).attr('data-answer');
        
        var jqxhrViewAnswer = $.ajax({
            type: "POST",
            url: BASEURL + 'question_managements/read_answer',
            data:{answerId:answerId},
            success: function (response) {
                
                if(response != ''){
                    var result = JSON.parse(response);
                    $('#answerAddEditForm').attr('action', BASEURL + 'question_managements/update_answer');
                    
                    tinymce.get('AnswerAnswerText').setContent(result.Answer.answer_text);
                    tinymce.get('AnswerAnsDescription').setContent(result.Answer.ans_description);
                    if(result.Answer.iscorrect){
                        $('#answerAddEditForm').find('#AnswerIscorrect').prop('checked', true);
                    }

                    if($('#answerAddEditForm').find('#AnswerId').length){
                        $('#answerAddEditForm').find('#AnswerId').val(result.Answer.id);
                    }else{
                        $('#answerAddEditForm').prepend('<input type="hidden" name="data[Answer][id]" value="' + result.Answer.id + '" id="AnswerId">');
                    }
                    scrollTopForSpecificArea('#answerEditSectionDivId');
                }

            }
        });
        
    });
    
    $('#deleteQuestionAnswerModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var actionName = button.attr('data-action');
        var modal = $(this);
        
        //questionAnswerDeleteConfirm
        
        var data = button.data();
        var keys = $.map(data , function(value, key) { 
            return key; 
        });
        for(var i = 0; i < keys.length; i++) {
            if($.inArray(keys[i], ['toggle', 'target']) === -1){
                modal.find('.modal-footer').find('.questionAnswerDeleteConfirm').attr('data-' + keys[i], button.attr('data-' + keys[i]));
            }
        }
        
        if(actionName != 'answer'){
            var deleteNote = '<p><strong>Please note</strong> that all related answers will be deleted.</p>';
//            var questionId = button.attr('data-question'),
//                previousId = button.attr('data-previous'),
//                nextId     = button.attr('data-next'); 
            
            modal.find('.modal-title').text('Delete Question');
            modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete this question?</p>');
        }else{
            modal.find('.modal-title').text('Delete Answer');
            modal.find('.modal-body').html('<p>Are you sure you want to delete this answer?</p>');
        }          
        
    });
    
    $('#deleteQuestionAnswerModal').on('hidden.bs.modal', function (e) {
        
        var modal = $(this);
        var data = modal.find('.modal-footer').find('.questionAnswerDeleteConfirm').data();
        var keys = $.map(data , function(value, key) { 
            return key; 
        });
        for(var i = 0; i < keys.length; i++) {
            if($.inArray(keys[i], ['toggle', 'target']) === -1){
                modal.find('.modal-footer').find('.questionAnswerDeleteConfirm').removeAttr('data-' + keys[i]);
            }
        }
        
    });
    
    $('.questionAnswerDeleteConfirm').click(function(event){
        var parentScope = $(this),
            actionName  = $(this).attr('data-action'),
            ajaxUrl, dataToSend = {};
            
        if(actionName != 'answer'){
            ajaxUrl = BASEURL + 'question_managements/delete_question';
            dataToSend['questionId'] = parentScope.attr('data-question');
            dataToSend['previousId'] = (parentScope.attr('data-previous'))?parentScope.attr('data-previous'):'';
            dataToSend['nextId'] = (parentScope.attr('data-next'))?parentScope.attr('data-next'):'';
        }else{
            ajaxUrl = BASEURL + 'question_managements/delete_answer';
            dataToSend['answerId'] = parentScope.attr('data-answer');
            dataToSend['questionId'] = parentScope.attr('data-question');
        }
         
        var jqxhrDeleteSchool = $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: dataToSend,
            success: function (response) {
                //console.log(response);
                if(response != ''){

                    var results = JSON.parse(response);
                    var className = (results.status == 'Error')?'alert-danger':'alert-success';
               
                    var statusString = (results.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';

                    var htmlString = '<div class="row"><div class="col-md-12 col-xs-12 col-sm-12 text-center">';
                    htmlString += '<div class="alert '+className+'" role="alert">';
                    htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                    htmlString += '<strong>'+statusString+'</strong> '+results.message; 
                    htmlString += '</div>';
                    htmlString += '</div></div>';

                    $('#deleteQuestionAnswerModal').find('.modal-body').html(htmlString);
                    if(results.status != 'Error'){
                        
                        var timeOutValue = parseInt(results.messageLetterCount) * parseInt(results.messageTimeOut);
                        setTimeout(function (){
                            
                            removeAllData('#deleteQuestionAnswerModal');
                            getQuestionPreview(results);
                            
                        }, timeOutValue);
                    }   
                }
            }
        });
    });
    
    $(document).on('change', '.examTypeCategory', function(event){
        var actionName = $(this).attr('data-action-name');
        var $parentScope = $(this);
        var selectedValue = $(this).val();
            selectedValue = ((actionName == 'Type') && (selectedValue == ''))?'all':selectedValue;
        
        if(selectedValue){
            var jqxhrAvailability = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/getExamTypeOrCategory',
                data:{actionName: actionName, selectedValue: selectedValue},
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var results = JSON.parse(response);
                        if(results.ExaminationType !== undefined && results.ExaminationType.id){
                            $('#questionAddEditForm').find('#QuestionExaminationTypeId').val(results.ExaminationType.id);
                        }else{
                            var htmlStr = '<option value="">- Select Category -</option>';
                            for(var key in results){
                                htmlStr += '<option value="'+ key +'">'+ results[key] +'</option>';
                            }
                            $('#questionAddEditForm').find('#QuestionExaminationCategoryId').html(htmlStr);
                        }
                    }
                }
            });
        }else{
            $('#questionAddEditForm').find('.examTypeCategory').each(function(index, element){
                $(element).val('');
            });
        }
    });
    
});

function removeAllData(modalSelector){
    
    var modal = $(modalSelector);
    var data = modal.find('.modal-footer').find('.questionAnswerDeleteConfirm').data();
    var keys = $.map(data , function(value, key) { 
        return key; 
    });
    for(var i = 0; i < keys.length; i++) {
        if($.inArray(keys[i], ['toggle', 'target']) === -1){
            modal.find('.modal-footer').find('.questionAnswerDeleteConfirm').removeAttr('data-' + keys[i]);
        }
    }
    $('#deleteQuestionAnswerModal').modal('hide');
    return;
}

function getQuestionPreview(params){
        
    var queryData = {};
    
    if(params.examinationId){
        queryData['examination_id'] = params.examinationId;
    }
    
    if(params.questionId){
        queryData['question_id'] = params.questionId;
    }
    
    var jqxhrViewSchool = $.ajax({
        type: "POST",
        url: BASEURL + "question_managements/preview_question",
        data: queryData,
        success: function (response) {
            //console.log(response);
            if(response != ''){
                $('#questioAndAnswerArea').html(response);
                if(!$('#questioAndAnswerArea').is(':visible')){
                    $('#questioAndAnswerArea').fadeIn(1000);
                }
                $('#AnswerQuestionId').val($('#viewingQuestionId').val());
            }else{
                $('#questioAndAnswerArea').html('');
            }

        }
    });
}

function scrollTopForSpecificArea(elementSelector) {
    $('html,body').animate({
        scrollTop: $(elementSelector).offset().top}, 950);
}