$(function(){ 
    
    $('#schoolEmailModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('school') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        $('#EmailToIds').val('');
        $('#EmailAttachmentMask').val('');
        var favorite = [];

        $.each($(".dataTable input:checked"), function () {
            favorite.push($(this).val());
        });

        var school_ids = favorite.join(",");

        if (school_ids == '') {
            alert("Please, select at least one school to send email.");
            return false;
        } else {
            //$('#filelist').html('');
            //$('#schoolEmailModal').modal('show');
            $('#EmailToIds').val(school_ids);
        }
        
    });
    
	
	//start compose email to any one
	 $('#composeEmailForm').validate({
        rules: {
            "data[Email][subject]":{
                required: true
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#ComposeEmailModal').find('.modal-footer').find('#composeEmailsButton').prop('disabled', true);
            
            //$('#EmailMessage').val(tinymce.activeEditor.getContent());
            $('#EmailContent').val(tinymce.activeEditor.getContent());
            
            var options = {
                success: saveFromEmailData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }

    });
	
	function saveFromEmailData(responseText, statusText, xhr, $form) {
		
		//alert(responseText);
                
        if (responseText != '') {
            $('#ComposeEmailModal').find('.modal-footer').find('#composeEmailsButton').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
                        
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#sendComposeEmailSuccess').html(htmlString).fadeIn(500);
            if(response.status != 'Error'){
                $form.resetForm();
                
                $('#ComposeEmailModal').find('.modal-body').find('#EmailAttachmentMask').val('');
                var toIds = $('#ComposeEmailModal').find('.modal-body').find('#EmailToIds').val();
                $('#ComposeEmailModal').find('.modal-body').find('#EmailAttachmentMask1').val('');
                $('#ComposeEmailModal').find('.modal-body').find('#EmailToIds').val('');
                $('#ComposeEmailModal').find('.modal-body').find('#fileAttachments1').html('');
                $('#EmailMessage').val('');
                tinymce.activeEditor.setContent('');
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                     
					$('#ComposeEmailModal').modal('hide');
                    $('#sendComposeEmailSuccess').css('display', 'none');
                   
                   /* var toIdsArray = toIds.split(',');
                    for(var index in toIdsArray){
                        $('.dataTable').find('#schoolName' + toIdsArray[index]).prop('checked', false);
                    }*/
                    
                }, timeOutValue);
            }
        }
        
    }
	
	
	
	$('#composeEmailsButton').click(function(event){
		
        $('#composeEmailForm').submit();
    });
	
	
	$(document).on('change', '#EmailFromEmailId', function(){
   
    var templateId = $(this).val();
	
    if(templateId){
        var jqGetFromEmailTemplate = $.ajax({
            type: "POST",
            url: BASEURL + 'email_systems/view_mail_from_template/'+templateId,
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    var result = JSON.parse(response);
                    tinymce.get('EmailContent').setContent(result.FromEmail.content);
                }else{
                    tinymce.get('EmailContent').setContent('');
                }
            }
        });
    }else{
        tinymce.get('EmailContent').setContent('');
    }
    
});
	
	
	
	
	//End compose email to any one
	
	
	
	
    $('#sendEmailsForm').validate({
        rules: {
            "data[Email][subject]":{
                required: true
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#schoolEmailModal').find('.modal-footer').find('#sendEmailsButton').prop('disabled', true);
            
            $('#EmailMessage').val(tinymce.activeEditor.getContent());
            
            var options = {
                success: saveEmailData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }

    });
    
    function saveEmailData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#schoolEmailModal').find('.modal-footer').find('#sendEmailsButton').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
                        
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#sendEmailSuccess').html(htmlString).fadeIn(500);
            if(response.status != 'Error'){
                $form.resetForm();
                
                $('#schoolEmailModal').find('.modal-body').find('#EmailAttachmentMask').val('');
                var toIds = $('#schoolEmailModal').find('.modal-body').find('#EmailToIds').val();
                $('#schoolEmailModal').find('.modal-body').find('#EmailAttachmentMask').val('');
                $('#schoolEmailModal').find('.modal-body').find('#EmailToIds').val('');
                $('#schoolEmailModal').find('.modal-body').find('#fileAttachments').html('');
                $('#EmailMessage').val('');
                tinymce.activeEditor.setContent('');
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                    $('#schoolEmailModal').modal('hide');
                    $('#sendEmailSuccess').css('display', 'none');
                    
                    var toIdsArray = toIds.split(',');
                    for(var index in toIdsArray){
                        $('.dataTable').find('#schoolName' + toIdsArray[index]).prop('checked', false);
                    }
                    
                }, timeOutValue);
            }
        }
        
    }
    
    $('#sendEmailsButton').click(function(event){
        $('#sendEmailsForm').submit();
    });
    
    
});

$(document).on('change', '#EmailEmailTemplateId', function(){
    
    var templateId = $(this).val();
    if(templateId){
        var jqxhrGetTemplate = $.ajax({
            type: "POST",
            url: BASEURL + 'email_systems/view_mail_template/'+templateId,
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    var result = JSON.parse(response);
                    tinymce.get('EmailMessage').setContent(result.EmailTemplate.content);
                }else{
                    tinymce.get('EmailMessage').setContent('');
                }
            }
        });
    }else{
        tinymce.get('EmailMessage').setContent('');
    }
    
});


$(document).on('change', '#EmailEmailTemplateId', function(){
    
    var templateId = $(this).val();
    if(templateId){
        var jqxhrGetTemplate = $.ajax({
            type: "POST",
            url: BASEURL + 'email_systems/view_mail_template/'+templateId,
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    var result = JSON.parse(response);
                    tinymce.get('EmailContent').setContent(result.EmailTemplate.content);
                }else{
                    tinymce.get('EmailContent').setContent('');
                }
            }
        });
    }else{
        tinymce.get('EmailContent').setContent('');
    }
    
});



$(document).on('change', '#EmailFromemailId', function(){
   
    var templateId = $(this).val();
	
    if(templateId){
        var jqGetFromEmailTemplate = $.ajax({
            type: "POST",
            url: BASEURL + 'email_systems/view_mail_from_template/'+templateId,
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    var result = JSON.parse(response);
                    tinymce.get('EmailMessage').setContent(result.FromEmail.content);
                }else{
                    tinymce.get('EmailMessage').setContent('');
                }
            }
        });
    }else{
        tinymce.get('EmailMessage').setContent('');
    }
    
});
	
	
	
	$(document).on('click', '.close', function(){
        tinymce.get('EmailMessage').setContent('');
	    tinymce.get('EmailContent').setContent('');
        $("#EmailEmailTemplateId option[value='']").attr('selected', true);


    
   });
	