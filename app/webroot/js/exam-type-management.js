$(function(){
   
    $('#examTypeAddEditForm').validate({
        rules: {
            "data[School][name]":{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#examTypeAddEditModal').find('.modal-footer').find('#saveExamTypeData').prop('disabled', true);
            
            var options = {
                success: saveExaminationTypeData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    function saveExaminationTypeData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#examTypeAddEditModal').find('.modal-footer').find('#saveExamTypeData').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#examTypeAddEditMessage').html(htmlString).fadeIn(500);
            if(response.status != 'Error'){
                $form.resetForm();
                
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function (){
                    $('#examTypeAddEditModal').modal('hide');
                    $('#examTypeAddEditMessage').css('display', 'none');
                    if(response.examTypeId){
                        window.location = BASEURL + 'examination_managements/categories/type:'+response.examTypeId;
                    }else{
                        window.location = location.href;
                    }
                }, timeOutValue);
            }
        }
    }
    
    $('#examTypeAddEditModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-exam-type');
        var modal = $(this);
        
        $('#examTypeAddEditMessage').css('display', 'none');
        modal.find('#examTypeAddEditForm').resetForm();
        $("#examTypeAddEditForm").clearValidation();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update Examination Type');
            modal.find('#examTypeAddEditForm').attr('action', BASEURL + 'examination_managements/edit_exam_type/'+recipient);
            
            var jqxhrViewCategory = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/view_exam_type/'+recipient,
                success: function (response) {
                    
                    if(response != ''){
                        var result = JSON.parse(response);
                        modal.find('#ExaminationTypeName').val(result.ExaminationType.name);
                        modal.find('#ExaminationTypeParentId').val((result.ExaminationType.parent_id != '0')?result.ExaminationType.parent_id:'');
                        if(result.ExaminationType.ismultipletimes){
                            modal.find('#ExaminationTypeIsmultipletimes').prop('checked', true);
                        }
                    }
                }
            });
            
        }else{
            modal.find('.modal-title').text('Add Examination Type');
            modal.find('#examTypeAddEditForm').attr('action', BASEURL + 'examination_managements/add_exam_type');
        }
    });
    
    $('#saveExamTypeData').click(function(event){
        $('#examTypeAddEditForm').submit();
    });
    
    $('.sub-examTypeFinder').click(function(event){
        
        event.preventDefault();
        var examTypeId = $(this).attr('data-exam-type');
        var examTypeName = $(this).attr('data-exam-type-name');
        if(examTypeId !== undefined && examTypeId != ''){
            window.location = BASEURL + 'examination_managements/types/parent_id:'+examTypeId;
        }else{
            alert(examTypeName + ' does\'t have any sub-examination type.')
        }        
        
    });
    
    $('.dataTable').find('.select-examType').click(function(){
        
        var total = $('.dataTable').find('.select-examType').length;
        var checkedCount = $('.dataTable').find('.select-examType:checked').length;
        
        if(total != checkedCount){
            $('.selectAllExamTypeButton').val('Select All');
            $('.selectAllExamTypeButton').attr('data-action', 'checkedAll');
        }else{
            $('.selectAllExamTypeButton').val('Deselect All');
            $('.selectAllExamTypeButton').attr('data-action', 'unCheckedAll');
        }
        
    });
    
    $('.selectAllExamTypeButton').click(function(event){
        var checked = false;
        if($(this).attr('data-action') != 'unCheckedAll'){
            checked = true;
            $(this).attr('data-action', 'unCheckedAll');
            $(this).val('Deselect All');
        }else{
            $(this).attr('data-action', 'checkedAll');
            $(this).val('Select All');
        }
        
        $('.dataTable').find('.select-examType').each(function(index, element){
            $(element).prop('checked', checked);
        });
        
    });
    
    $('#deleteExamTypeModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-exam-type-name');
        var recipientId = button.attr('data-exam-type');
        var modal = $(this);
        
        var examTypeCount = $('.dataTable').find('.select-examType:checked').length;
        var deleteNote = '<p><strong>Please note</strong> that all related sub-examination types, categories and examinations will be deleted.</p>';
        if(examTypeCount > 0 && recipientId === undefined){
            modal.find('.modal-footer').find('.examTypeDeleteConfirm').attr('data-isChecked', 'true');
            if(examTypeCount > 1){
                modal.find('.modal-title').text('Delete Types');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete all selected types?</p>');
            }else{
                modal.find('.modal-title').text('Delete Type');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            }
            
        }else if(recipientId !== undefined && $('#examTypeName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.examTypeDeleteConfirm').attr('data-isChecked', 'true');
            modal.find('.modal-title').text('Delete Type');
            modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-footer').find('.examTypeDeleteConfirm').attr('data-examTypeId', recipientId);
            
        }else{
            modal.find('.modal-title').text('Delete Type');
            modal.find('.modal-body').html('<p>Before continue please, select a exam type.</p>');
            modal.find('.modal-footer').find('.examTypeDeleteConfirm').attr('data-isChecked', 'false');
            modal.find('.modal-footer').find('.examTypeDeleteConfirm').attr('data-examTypeId', '');
        }            
        
    });
    
    $('.examTypeDeleteConfirm').click(function(event){
        var parentScope = $(this);
        if($(this).attr('data-isChecked') == 'true'){
            
            $('#deleteExamTypeModal').find('.modal-body').html('<p>Please be patient. Exam types are deleting...</p>');
            
            var examTypeId = $(this).attr('data-examTypeId');
            $(this).prop('disabled', true);
            var examTypeIds = [];
            $('.dataTable').find('.select-examType:checked').each(function(index, element){
                examTypeIds.push($(element).val());
            });
            
            var selectedId = (examTypeId !== undefined && examTypeId != '')?examTypeId:examTypeIds.toString();
            
            var jqxhrDeleteExamType = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/delete_exam_types',
                data:{
                    examTypeIds: selectedId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        
                        var results = JSON.parse(response);
                        var message;
                        if(results.length > 1){
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> All the selected types have been deleted.'; 
                            message += '</div>';
                        }else{
                            parentScope.attr('data-examTypeId', '');
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> Selected exam type has been deleted.'; 
                            message += '</div>';
                        }
                        
                        $('#deleteExamTypeModal').find('.modal-body').html(message);
                        
                        setTimeout(function(){
                            
                            if(results.length == $('.dataTable').find('.select-exam-type').length){
                                window.location = BASEURL + 'examination_managements/types';
                            }else{
                                window.location = location.href;
                            }
                            
                        }, 3000);
                        
                    }

                }
            });
            
        }else{
            $('#deleteExamTypeModal').modal('hide');
        }
        
    });
    
});