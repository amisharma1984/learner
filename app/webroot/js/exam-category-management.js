$(function(){
   
    $('#categoryAddEditForm').validate({
        rules: {
            "data[School][name]":{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            }
        },
        submitHandler: function (form) {
            
            /* Disable submit button */
            $('#categoryAddEditModal').find('.modal-footer').find('#saveCategoryData').prop('disabled', true);
            
            var options = {
                success: saveExaminationCategoryData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    function saveExaminationCategoryData(responseText, statusText, xhr, $form) {
                
        if (responseText != '') {
            $('#categoryAddEditModal').find('.modal-footer').find('#saveCategoryData').prop('disabled', false);
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';
            $('#categoryAddEditMessage').html(htmlString).fadeIn(500);
            
            if(response.status != 'Error'){
                $form.resetForm();
                
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function (){
                    $('#categoryAddEditModal').modal('hide');
                    $('#categoryAddEditMessage').css('display', 'none');
                    if(response.categoryId){
                        window.location = BASEURL + 'examination_managements/index/category:'+response.categoryId;
                    }else{
                        window.location = location.href;
                    }
                }, timeOutValue);
            }
        }
    }
    
    $('#categoryAddEditModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.attr('data-category');
        var modal = $(this);
        
        $('#categoryAddEditMessage').css('display', 'none');
        modal.find('#categoryAddEditForm').resetForm();
        $("#categoryAddEditForm").clearValidation();
        
        if(recipient !== undefined){
            modal.find('.modal-title').text('Update Examination Category');
            modal.find('#categoryAddEditForm').attr('action', BASEURL + 'examination_managements/edit_category/'+recipient);
            
            var jqxhrViewCategory = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/view_category/'+recipient,
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        var result = JSON.parse(response);
                        modal.find('#ExaminationCategoryName').val(result.ExaminationCategory.name);
                        modal.find('#ExaminationCategoryParentId').val((result.ExaminationCategory.parent_id != '0')?result.ExaminationCategory.parent_id:'');
                        modal.find('#ExaminationCategoryExaminationTypeId').val(result.ExaminationCategory.examination_type_id);
                    }

                }
            });
            
        }else{
            modal.find('.modal-title').text('Add Examination Category');
            modal.find('#categoryAddEditForm').attr('action', BASEURL + 'examination_managements/add_category');
        }
    });
    
    $('#saveCategoryData').click(function(event){
        $('#categoryAddEditForm').submit();
    });
    
    $('.sub-categoryFinder').click(function(event){
        
        event.preventDefault();
        var categoryId = $(this).data('category');
        var categoryName = $(this).data('category-name');
        if(categoryId !== undefined && categoryId != ''){
            window.location = BASEURL + 'examination_managements/categories/parent_id:'+categoryId;
        }else{
            alert(categoryName + ' does\'t have any sub-category.')
        }        
        
    });
    
    $('.dataTable').find('.select-category').click(function(){
        
        var total = $('.dataTable').find('.select-category').length;
        var checkedCount = $('.dataTable').find('.select-category:checked').length;
        
        if(total != checkedCount){
            $('.selectAllCategoryButton').val('Select All');
            $('.selectAllCategoryButton').attr('data-action', 'checkedAll');
        }else{
            $('.selectAllCategoryButton').val('Deselect All');
            $('.selectAllCategoryButton').attr('data-action', 'unCheckedAll');
        }
        
    });
    
    $('.selectAllCategoryButton').click(function(event){
        var checked = false;
        if($(this).attr('data-action') != 'unCheckedAll'){
            checked = true;
            $(this).attr('data-action', 'unCheckedAll');
            $(this).val('Deselect All');
        }else{
            $(this).attr('data-action', 'checkedAll');
            $(this).val('Select All');
        }
        
        $('.dataTable').find('.select-category').each(function(index, element){
            $(element).prop('checked', checked);
        });
        
    });
    
    $('#deleteCategoryModal').on('show.bs.modal', function (event) {
        
        var button = $(event.relatedTarget);
        var recipient = button.data('category-name');
        var recipientId = button.data('category');
        var modal = $(this);
        
        var schoolCount = $('.dataTable').find('.select-category:checked').length;
        var deleteNote = '<p><strong>Please note</strong> that all related sub-categories and examinations will be deleted.</p>';
        if(schoolCount > 0 && recipientId === undefined){
            modal.find('.modal-footer').find('.categoryDeleteConfirm').attr('data-isChecked', 'true');
            if(schoolCount > 1){
                modal.find('.modal-title').text('Delete Categories');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete all selected categories?</p>');
            }else{
                modal.find('.modal-title').text('Delete Category');
                modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            }
            
        }else if(recipientId !== undefined && $('#categoryName'+recipientId).is(':checked')){
            modal.find('.modal-footer').find('.categoryDeleteConfirm').attr('data-isChecked', 'true');
            modal.find('.modal-title').text('Delete Category');
            modal.find('.modal-body').html(deleteNote + '<p>Are you sure you want to delete <strong>'+recipient+'</strong>?</p>');
            modal.find('.modal-footer').find('.categoryDeleteConfirm').attr('data-categoryId', recipientId);
            
        }else{
            modal.find('.modal-title').text('Delete Category');
            modal.find('.modal-body').html('<p>Before continue please, select a category.</p>');
            modal.find('.modal-footer').find('.categoryDeleteConfirm').attr('data-isChecked', 'false');
            modal.find('.modal-footer').find('.categoryDeleteConfirm').attr('data-schoolId', '');
        }            
        
    });
    
    $('.categoryDeleteConfirm').click(function(event){
        var parentScope = $(this);
        if($(this).attr('data-isChecked') == 'true'){
            
            $('#deleteCategoryModal').find('.modal-body').html('<p>Please be patient. Categories deleting...</p>');
            
            var categoryId = $(this).attr('data-categoryId');
            $(this).prop('disabled', true);
            var categoryIds = [];
            $('.dataTable').find('.select-category:checked').each(function(index, element){
                categoryIds.push($(element).val());
            });
            
            var selectedId = (categoryId !== undefined && categoryId != '')?categoryId:categoryIds.toString();
            
            var jqxhrDeleteSchool = $.ajax({
                type: "POST",
                url: BASEURL + 'examination_managements/delete_categories',
                data:{
                    categoryIds: selectedId
                },
                success: function (response) {
                    //console.log(response);
                    if(response != ''){
                        
                        var results = JSON.parse(response);
                        var message;
                        if(results.length > 1){
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> All the selected categories have been deleted.'; 
                            message += '</div>';
                        }else{
                            parentScope.attr('data-categoryId', '');
                            message = '<div class="alert alert-success" role="alert">';
                            message += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                            message += '<strong><span class="glyphicon glyphicon-ok-sign"></span></strong> Selected category has been deleted.'; 
                            message += '</div>';
                        }
                        
                        $('#deleteCategoryModal').find('.modal-body').html(message);
                        
                        setTimeout(function(){
                            
                            if(results.length == $('.dataTable').find('.select-category').length){
                                window.location = BASEURL + 'examination_managements/categories';
                            }else{
                                window.location = location.href;
                            }
                            
                        }, 3000);
                        
                    }

                }
            });
            
        }else{
            $('#deleteCategoryModal').modal('hide');
        }
        
    });
    
});