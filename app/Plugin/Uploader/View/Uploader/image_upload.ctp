<style>
.loader { float:left; width: 28px; height: 28px; }
</style>

<?php // Step 1: Upload Form ------------------------------------------------ ?>

<style>
#droparea { width: 100%; height: 440px; border: 3px dashed grey; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; }
#droparea.hover { background: #fff; }
#upload-container input[type=file] {
	font-size: 28px !important;
	cursor: pointer !important;
}
#upload-container input {
	border: none;
}
</style>
<div id="step1">
	<div id="upload-container">
		<div id="droparea">
			<div style="text-align: center; margin-top: 75px;">
				<p><?php echo __("Upload your image, jpg, gif or png (2MB max)") ?></p>
				<span id="text-to-remove">
				    <?php echo __('Drop files here') ?>
	                <br />
	                <p><?php echo __('or') ?></p>
				</span>
				<a id="pickfiles" class="button" style="width: auto; text-align:center;" href="javascript:void(0);"><?php echo __('Select') ?></a>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var temp_image = '';
var uploader;
$(function() {
    
    var dndsupport = false;
    var iOS = !!navigator.userAgent.match('iPhone OS') || !!navigator.userAgent.match('iPad');
    if (Modernizr.draganddrop && !iOS) {
        dndsupport = true;
    } else if (Modernizr.draganddrop && iOS) {
        dndsupport = true;
    } else {
        dndsupport = false;
    }
    
    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
    	dndsupport = false;
    }
    
    if (navigator.userAgent.toLowerCase().indexOf('safari') > 0 &&
    	navigator.userAgent.toLowerCase().indexOf('chrome') == -1) {
    	
    	dndsupport = false;
    }
    
    if (!dndsupport) {
        $('#droparea').removeAttr('id');
        $('#text-to-remove').parent('div').css('margin-top', '0');
        $('#pickfiles').html('<?php echo __('Select files') ?>');
        $('#text-to-remove').remove();
    }
    
	uploader = new plupload.Uploader({
		runtimes : 'html5,flash',
		browse_button : 'pickfiles',
		container : 'upload-container',
		chunk_size : '1mb',
		drop_element : "droparea",
		max_file_size : '2M',
		multi_selection: false,
		url : '<?php echo $this->Html->url(array('community' => Configure::read('Community.slug'), 'plugin' => '', 'controller' => 'members', 'action' => 'uploading_avatar'), true); ?>',
		flash_swf_url : '<?php echo $this->Html->url('/js/plupload/plupload.flash.swf', true); ?>',
        silverlight_xap_url : '<?php echo $this->Html->url('/js/plupload/plupload.silverlight.xap', true); ?>',
		filters : [
			{ title : "<?php echo __("Image files") ?>", extensions : "jpg,jpeg,png,gif" }
		],
		preInit: {
			Init: function(up, params) {
		    	if (params.runtime == 'flash') {
		    		$('#droparea').removeAttr('id');
			        $('#text-to-remove').parent('div').css('margin-top', '0');
			        $('#pickfiles').html('<?php echo __('Select files') ?>');
			        $('#text-to-remove').remove();
		    	}
		    	//console.log(params.runtime);
        	}
		}, 
		init: {
			FilesAdded: function(up, files) {
				$('.progressbar').progressbar({ value: 0 }); $('.progress').html('0%');
				if (up.runtime != 'flash') $('#step1').delay(100).fadeOut('fast');
				$('#step2').delay(100).fadeIn('fast');
				up.start();
				up.refresh(); // Reposition Flash/Silverlight
			},
			UploadProgress: function(up, file) {
				$('.progressbar').progressbar('value', file.percent);
				$('.progress').html(file.percent + '%');
			},
			Error: function(up, err) {
			    if (err.code == -600) { // Size
		    		alert("<?php echo __("You have selected files that exceed the %d MB size limit", ini_get("upload_max_filesize")) ?>");
		    	} else if (err.code == -601) { // Extension
		    		alert("<?php echo __("You have selected file types that are not supported", ini_get("upload_max_filesize")) ?>");
		    	} else {
		    		console.log(err.code);
		    		console.log(err.message);
		    	}
				up.refresh(); // Reposition Flash/Silverlight
			},
			FileUploaded: function(up, file, response) {
				var obj = $.parseJSON(response.response);
				//console.log(obj);
				$('.progressbar').progressbar('value', 100); $('.progress').html('100%');
				$('#preview').attr('src', '<?php echo $this->Html->url('/files/users/avatar/') ?>'+obj.result.filename);
				$('#target').attr('src', '<?php echo $this->Html->url('/files/users/avatar/') ?>'+obj.result.filename).Jcrop({
					bgColor:     'black',
		            bgOpacity:   .4,
		            setSelect:   [0, 0, 100, 100],
		            aspectRatio: 1,
		            onChange: showPreview,
					onSelect: showPreview,
					minSize: [50, 50]
				});
				if (up.runtime == 'flash') $('#step1').delay(100).fadeOut('fast');
				$('#step2').delay(100).fadeOut('fast'); $('#step3').delay(100).fadeIn('fast');
				temp_image = obj.result.filename;
			}
		}
	});

	uploader.init();
	
	function showPreview(coords) {
		x = coords.x; y = coords.y;
		w = coords.w; h = coords.h;
		
		var rx = 69 / coords.w;
		var ry = 69 / coords.h;

		$('#preview').css({
			width: Math.round(rx * $('#target').width()) + 'px',
			height: Math.round(ry * $('#target').height()) + 'px',
			marginLeft: '-' + Math.round(rx * coords.x) + 'px',
			marginTop: '-' + Math.round(ry * coords.y) + 'px'
		});
	}
	
	$('#droparea')
		.bind('dragover dragenter', function(e) { $(this).addClass('hover'); e.preventDefault(); })
        .bind('dragexit dragleave drop dragend', function(e) { $(this).removeClass('hover'); e.preventDefault(); });
});
</script>

<?php // Step 2: Uploading -------------------------------------------------- ?>

<style>
#step2 { text-align: center; }
.progress { float:right; display:inline-block; margin-left: 10px; }
.progressarea { width:49%; display:inline-block; position: absolute; margin-top: 220px; margin-left: 200px; }
.progressbar { margin-top: 3px; }
.ui-progressbar { height: 10px; }
</style>
<div id="step2" style="display:none;">
	<table>
		<tr>
			<td valign="center" align="center" style="text-align: center;">
				<div class="progressarea">
					<div class="progress"></div>
					<div class="progressbar"></div>
				</div>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
$(function () {
	$('.progressarea').css('top', '0%');
	$(".progressbar").progressbar({ value: 0 });
	$(".progress").html("0%");
});
</script>

<?php // Step 3: Cropping --------------------------------------------------- ?>

<style>
#cropping { width: 650px; height: 442px; border: 1px solid grey; overflow: scroll;}
#cropping-sidebar { float:right; width: 190px; text-align: left; }
</style>
<div id="step3" style="display:none;">
	<div id="cropping-sidebar">
		<h3><?php echo __('Preview') ?></h3>
		<div style="width:69px;height:69px;overflow:hidden;">
			<img id="preview" src="" align="center" />
		</div>
		<div style="height: 20px;"></div>
		<a href="javascript:void(0);" onclick="crop_image();" class="button"><?php echo __('Save Avatar') ?></a>
	</div>
	<div id="cropping">
		<img id="target" src="" />
	</div>
</div>
<script type="text/javascript">
var x, y, w, h;
function crop_image() {
	var uri = "<?php echo $this->Html->url(array('community' => Configure::read('Community.slug'), 'plugin' => '', 'controller' => 'members', 'action' => 'crop_avatar')) ?>";
	$.post(uri, { image: temp_image, x: x, y: y, w: w, h: h }, function (data) {
		$('.avatar').attr('src', $('.avatar').attr('src')+'?no-cache');
		$(".ui-dialog-content").dialog("close");
	});
	
}
</script>