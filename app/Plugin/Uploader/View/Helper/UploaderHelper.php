<?php
App::uses('AppHelper', 'View/Helper');
class UploaderHelper extends AppHelper {
    
    public $helpers = array('Html');
	
	public function image($options = array()) {
		$options = array_merge(array(
			'label' => __('Thumbnail')
		), $options);
		
		$this->Html->css(array(
			'/js/plupload/jquery.ui.plupload/css/jquery.ui.plupload',
			'jcrop/jquery.Jcrop.min'
		), null, array('inline' => false));
		$this->Html->script(array(
			'jquery.Jcrop.min',
			'modernizr.custom.14453',
			'plupload/plupload.full'
		), array('inline' => false, 'once' => true));
		
		$html = '<div id="upload-image" style="display:none;">'.$this->Html->image('ajax-loader.gif').'</div>';
		$html .= '<a href="javascript: void(0);" onClick="uploadImage();">'.$options['label'].'</a>';
		
		$script = "
			$(function () {
				$('#upload-image').dialog({
					modal: true,
				    title: '".__('Upload').' '.$options['label']."',
				    width: 920,
				    height: 500,
				    autoOpen: false,
				    resizable: false,
				    open: function () {
				    	$('#upload-image').load('".$this->Html->url(array('plugin' => 'uploader', 'controller' => 'uploader', 'action' => 'image_upload'), true)."', function () {
				    		$('#upload-container > div.plupload').css('z-index','99999');
				    	});
				    },
				    close: function () {
				    	$('#upload-image').html('".$this->Html->image('ajax-loader.gif')."');
				    }
				});
			});
			function uploadImage() {
				$('#upload-image').dialog('open');
			}
		";
		
		$html .= $this->Html->scriptBlock($script);
		return $html;
	}
}