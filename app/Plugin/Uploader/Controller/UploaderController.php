<?php

App::import('Console/Command', 'AppShell');
App::uses('CakeText', 'Utility');

class UploaderController extends UploaderAppController {

    public $name = 'Uploader';
    public $uses = array("Attachment");

    // AJAX
    function upload() {
        $this->layout = 'ajax';

        // Headers
        $this->response->header('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
        $this->response->header('Last-Modified', gmdate("D, d M Y H:i:s") . " GMT");
        $this->response->header('Cache-Control', 'no-store, no-cache, must-revalidate');
        $this->response->header('Cache-Control', 'post-check=0, pre-check=0');
        $this->response->header('Pragma', 'no-cache');

        // 5 minutes execution time
        @set_time_limit(5 * 60);

        // Chunk case
        if (isset($this->request->data['chunk']) &&
                !empty($this->request->data['chunks']) &&
                !empty($this->request->data['name'])) {

            // Make sure a file was sent in the post.
            $fileName = isset($this->request->data['name']) ? $this->request->data['name'] : '';
            $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

            // Chunks data
            $chunk = (int) $this->request->data['chunk'];
            $numChunks = $this->request->data['chunks'];
            $tmpFilename = TMP . 'file_chunks/' . $fileName;

            // Move the chunk to the tmp/file_chunks/ directory.
            file_put_contents($tmpFilename, file_get_contents($this->request->params['form']['file']['tmp_name']), FILE_APPEND);

            // When finish of ensambling
            if ($chunk >= ($numChunks - 1)) {

                // Common vars
                $randomDirectory = $this->randomPath(3);
                $targetDir = WWW_ROOT . 'files/school/attachments/' . $randomDirectory;
                $pathinfo = pathinfo($this->request->data['name']);
                $ext = $pathinfo['extension'];
                //$fileName = String::uuid();
                $fileName = $pathinfo['filename'];
                $fileNameExt = $fileName . "." . $ext;

                // Create target dir
                $oldmask = umask(0);
                if (!file_exists($targetDir))
                    @mkdir($targetDir, 0777, true);
                umask($oldmask);

                // Size of the complete file
                $size = filesize($tmpFilename);
                // Copy file to destination
                if (!copy($tmpFilename, $targetDir . $fileNameExt))
                    die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Unable to mv ' . $tmpFilename . ' to destination ' . $targetDir . $fileNameExt . '."}, "id" : "id"}');
                // Remove from origin
                if (!unlink($tmpFilename))
                    die('{"jsonrpc" : "2.0", "error" : {"code": 104, "message": "Unable to remove origin."}, "id" : "id"}');

                $oldmask = umask(0);
                chmod($targetDir . $fileNameExt, 0777);
                umask($oldmask);

                if (!empty($this->request->data['attachmentMask'])) {
                    $maskId = $this->request->data['attachmentMask'];
                } else {
                    $maskId = CakeText::uuid();
                }

                // Save the file
                $this->saveAttachment($pathinfo['filename'], $ext, $size, $randomDirectory, $maskId);
            }

            die('{ "jsonrpc" : "2.0", "result" : "", "id" : "id"}');

            // Normal case
        } else {
            // Common vars
            $randomDirectory = $this->randomPath(3);
            $targetDir = WWW_ROOT . 'files/school/attachments/' . $randomDirectory;
            $pathinfo = pathinfo($_FILES['file']['name']);
            $ext = $pathinfo['extension'];
            //$fileName = String::uuid();
            $fileName = $pathinfo['filename'];
            $fileNameExt = $fileName . "." . $ext;

            // Create target dir
            $oldmask = umask(0);
            if (!file_exists($targetDir))
                @mkdir($targetDir, 0777, true);
            umask($oldmask);

            // Look for the content type header
            if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
                $contentType = $_SERVER["HTTP_CONTENT_TYPE"];
            if (isset($_SERVER["CONTENT_TYPE"]))
                $contentType = $_SERVER["CONTENT_TYPE"];
            // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
            if (strpos($contentType, "multipart") !== false) {
                if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                    $out = @fopen($targetDir . $fileNameExt, $chunk == 0 ? "wb" : "ab"); // Open temp file
                    if ($out) {
                        $in = fopen($_FILES['file']['tmp_name'], "rb"); // Read binary input stream and append it to temp file
                        if ($in) {
                            while ($buff = fread($in, 4096))
                                fwrite($out, $buff);
                        } else
                            die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                        fclose($in);
                        fclose($out);
                        @unlink($_FILES['file']['tmp_name']);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            } else {
                $out = fopen($targetDir . $fileNameExt, $chunk == 0 ? "wb" : "ab"); // Open temp file
                if ($out) {
                    $in = fopen("php://input", "rb"); // Read binary input stream and append it to temp file
                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    fclose($in);
                    fclose($out);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            }

            if (!empty($this->request->data['attachmentMask'])) {
                $maskId = $this->request->data['attachmentMask'];
            } else {
                $maskId = CakeText::uuid();
            }
            $this->saveAttachment($pathinfo['filename'], $ext, $_FILES['file']['size'], $randomDirectory, $maskId);
        }

        die('{"jsonrpc" : "2.0", "error" : {"code": 110, "message": "Failed before anything"}, "id" : "id"}');
    }

    function saveAttachment($filename, $ext, $size, $directory, $mask) {


        $data = array(
            'Attachment' => array(
                'title' => $filename,
                'extension' => $ext,
                'size' => $size,
                'path' => $directory,
                'mask' => $mask,
                'deleted' => 1,
                'created' => date('YmdHis')
            )
        );

        if (!$this->Attachment->save($data))
            die('{"jsonrpc" : "2.0", "error" : {"code": 150, "message": "Failed to save on database."}, "id" : "id"}');



        die('{ "jsonrpc" : "2.0", "result" : ' . json_encode(array(
                    "id" => $this->Attachment->id,
                    'maskId' => $mask
                )) . ' , "id" : "id"}');
    }

    function widget($width, $height) {
        $this->set(compact('width', 'height'));
    }

    function randomPath($maxlevel = 3) {
        $level = rand(1, $maxlevel);
        $path = '';

        $validCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";
        for ($i = 0; $i < $level; $i++) {
            $tempFolder = '';
            $tempFolderSize = rand(2, 5);
            for ($j = 0; $j < $tempFolderSize; $j++) {
                $index = mt_rand(0, strlen($validCharacters) - 1);
                $tempFolder .= $validCharacters[$index];
            }
            $path .= $tempFolder . '/';
        }

        return $path;
    }

    /*     * ************************************************* */

    // For load as view
    function image_upload() {
        
    }

    function uploading() {
        $this->Uploader->upload($this, array(
            'time_limit' => 2 * 60,
            'target_dir' => WWW_ROOT . "files/users/avatar",
            'filename' => "_" . md5($this->Settings->getUserId()),
        ));
    }

    /**
     * Upload file server script
     * Params by Post
     * @param file All params of the file
     */
    function upload_ok() {
        if (!empty($this->request->data) && is_uploaded_file($_POST['File']['tmp_name'])) {
            $file = new File($_POST['File']['tmp_name']);

            $pathinfo = pathinfo($this->request->data['Avatar']['File']['name']);
            $ext = $pathinfo['extension'];
            $filename = "_" . md5($this->Settings->getUserId()) . "." . $ext;
            $path = WWW_ROOT . "files/users/avatar/";

            move_uploaded_file($this->request->data['Avatar']['File']['tmp_name'], $path . $filename);

            $this->User->id = $this->Settings->getUserId();
            $this->User->saveField('avatar', $filename);
        }
        $this->redirect($this->referer());
    }

    /**
     * Params passed by POST
     * @param image Image path with extension
     * @param width Target width
     * @param height Target height
     * @param x Origin x
     * @param y Origin y
     * @param w Origin width
     * @param h Origin height
     */
    function crop() {
        $image = new File($_POST['image']);
        $width = $height = 150;
        if (isset($_POST['width']))
            $width = $_POST['width'];
        if (isset($_POST['height']))
            $height = $_POST['height'];

        switch (strtolower($image->ext)) {
            case 'jpeg':
            case 'jpg': $src = imagecreatefromjpeg($image->path);
                break;
            case 'png': $src = imagecreatefrompng($image->path);
                break;
            case 'gif': $src = imagecreatefromgif($image->path);
                break;
            default: die('{"jsonrpc" : "2.0", "error" : {"code": 151, "message": "The file type is not compatible, use: jpg, jpeg, png, gif."}, "id" : "id"}');
        }

        $dst = ImageCreateTrueColor($width, $height);
        imagecopyresampled($dst, $src, 0, 0, $_POST['x'], $_POST['y'], $width, $height, $_POST['w'], $_POST['h']);

        if (!imagepng($dst, $image->path, 9))
            die('{"jsonrpc" : "2.0", "error" : {"code": 150, "message": "Failed to save file cropped."}, "id" : "id"}');

        die('{ "jsonrpc" : "2.0", "result" : "ok", "id" : "id"}');
    }

    function removeThimThumbCache($filePath) {
        // Fake the query string
        $_SERVER['QUERY_STRING'] = 'src=' . $filePath . "&zc=1&w=497";
        parse_str($_SERVER['QUERY_STRING'], $_GET);

        // When instantiated, timthumb will generate some properties: 
        // salt, directories, cachefile, etc.
        $t = new timthumb();

        // Get the cache file name
        $f = $t->getCacheFile();

        if (file_exists($f)) {
            unlink($f);
        }
    }

    /**
     * Params passed by POST
     * @param image Image path with extension
     * @param angle Angle of rotation
     */
    function rotate() {
        $this->removeThimThumbCache($_POST['image']);

        if ((env('REMOTE_ADDR') != '127.0.0.1') && (env('REMOTE_ADDR') != '::1')) {
            //servidor produccion 
            $filePath = WWW_ROOT . str_replace("/app/webroot/", "", $_POST['image']);
        } else {
//                  servidor local
            $filePath = WWW_ROOT . str_replace("lifeandevents/app/webroot/", "", $_POST['image']);
            $filePath = str_replace("/", "\\", $filePath);
            $filePath = str_replace("\\\\", "\\", $filePath);
        }
        $pathinfo = pathinfo($filePath);
        $ext = $pathinfo['extension'];
        $angle = $_POST['angle'];
        switch (strtolower($ext)) {
            case 'jpeg':
            case 'jpg':$src = imagecreatefromjpeg($filePath);
                if ($src)
                    break;
            case 'png': $src = imagecreatefrompng($filePath);
                if ($src)
                    break;
            case 'gif': $src = imagecreatefromgif($filePath);
                if ($src)
                    break;
            default: die('{"jsonrpc" : "2.0", "error" : {"code": 151, "message": "The file type is not compatible, use: jpg, jpeg, png, gif."}, "id" : "id"}');
        }
        if (!$src) {
            die('{ "jsonrpc" : "2.0", "result" : "error1", "id" : "id", "path":"' . Router::url('/', true) . "timthumb.php?src=" . $_POST['image'] . '"}');
        }
        $bgd_color = imagecolorallocatealpha($src, 0, 0, 0, 127);
        $white = imagecolorallocate($src, 255, 255, 255);
        $rotar = imagerotate($src, $angle, $bgd_color);
        imagecolortransparent($rotar, $white);
        imagealphablending($rotar, TRUE);
        imagesavealpha($rotar, TRUE);
        //$rotar=$src;
        switch (strtolower($ext)) {
            case 'jpeg':
            case 'jpg': $src = imagejpeg($rotar, $filePath);
                break;
            case 'png': $src = imagepng($rotar, $filePath);
                break;
            case 'gif': $src = imagegif($rotar, $filePath);
                break;
            default: die('{"jsonrpc" : "2.0", "error" : {"code": 152, "message": "The file type is not compatible, use: jpg, jpeg, png, gif."}, "id" : "id"}');
        }
        if (!$src) {
            die('{ "jsonrpc" : "2.0", "result" : "error2", "id" : "id", "path":"' . Router::url('/', true) . "timthumb.php?src=" . $_POST['image'] . '"}');
        }
        die('{ "jsonrpc" : "2.0", "result" : "ok", "id" : "id", "path":"' . Router::url('/', true) . "timthumb.php?src=" . $_POST['image'] . '"}');
    }

}
