<?php
class UploaderComponent extends Component {
	
	public $components = array();
	public $default = array();
	
	public function init() {
		$this->default = array(
			'time_limit' => 5 * 60,
			'target_dir' => WWW_ROOT."img".DS."avatar".DS,
			'filename' => '',
			
		);
	}
	
	public function upload($controller = null, $options = array()) {
		$this->init();
		$options = array_merge($this->default, $options);
		
		// Headers
		$controller->response->header('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
        $controller->response->header('Last-Modified', gmdate("D, d M Y H:i:s") . " GMT");
        $controller->response->header('Cache-Control', 'no-store, no-cache, must-revalidate');
        $controller->response->header('Cache-Control', 'post-check=0, pre-check=0');
        $controller->response->header('Pragma', 'no-cache');
		
		// 5 minutes execution time
        @set_time_limit($options['time_limit']);
		
		// Get parameters
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		//$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
		
		//$fileName = isset($controller->request->data['name']) ? $controller->request->data['name'] : '';
		//$fileName = preg_replace('/[^\w\._]+/', '_', $fileName);
		$pathinfo = pathinfo($controller->request->data['name']); $ext = $pathinfo['extension'];	// Get extension
		$fileName = $options['filename'] . "." . $ext;
		
		$targetDir = $options['target_dir'];
		$filePath = $targetDir . DS . $fileName;
		
		if (!file_exists($options['target_dir'])) @mkdir($options['target_dir'], 0777, true);
		
		// Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"])) $contentType = $_SERVER["HTTP_CONTENT_TYPE"];
        if (isset($_SERVER["CONTENT_TYPE"])) $contentType = $_SERVER["CONTENT_TYPE"];
		
		// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
		if (strpos($contentType, "multipart") !== false) {
			if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
				// Open temp file
				$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
				if ($out) {
					// Read binary input stream and append it to temp file
					$in = fopen($_FILES['file']['tmp_name'], "rb");
		
					if ($in) {
						while ($buff = fread($in, 4096))
							fwrite($out, $buff);
					} else
						die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
					fclose($in);
					fclose($out);
					@unlink($_FILES['file']['tmp_name']);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
		} else {
			// Open temp file
			$out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
			if ($out) {
				// Read binary input stream and append it to temp file
				$in = fopen("php://input", "rb");
		
				if ($in) {
					while ($buff = fread($in, 4096))
						fwrite($out, $buff);
				} else
					die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
		
				fclose($in);
				fclose($out);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off 
			rename("{$filePath}.part", $filePath);
		}
		
		// Return JSON-RPC response
		//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
		die('{ "jsonrpc" : "2.0", "result" : '.json_encode(array(
			"filename" => $fileName
		)).' , "id" : "id"}');
	}
} 
?>