<?php

App::uses('Model', 'Model');

class Answer extends AppModel {

    public $name = 'Answer';

//    public $hasMany = array(
//        'Answer' => array(
//            'className' => 'Answer',
//            'conditions' => array('Answer.isdeleted' => 0),
//            'order' => 'Answer.created DESC'
//        )
//    );
    
    public $belongsTo = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => array('Question.isdeleted' => 0)
        )
    );

}
