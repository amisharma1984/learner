<?php

App::uses('Model', 'Model');

class EmailDelivery extends AppModel {

    public $name = 'EmailDelivery';
	
	public $belongsTo = array(
        'School' => array(
            'className' => 'School',
            'foreignKey' => 'to_id',
			'fields' => array('name','email','manager_email'),
			//'conditions' => array('School.isdeleted' => 0),
        ),
		
		'Admin' => array(
            'className' => 'Admin',
            'foreignKey' => 'from_id',
			'fields' => array('id','email','first_name')
        ),
		
		'Email' => array(
            'className' => 'Email',
            'foreignKey' => 'email_id',
        )
		
    );

}
