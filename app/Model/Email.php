<?php

App::uses('Model', 'Model');

class Email extends AppModel {

    public $name = 'Email';

    public $hasMany = array(
        'Attachment' => array(
            'className' => 'Attachment',
            'conditions' => array('Attachment.deleted' => 0),
            'order' => 'Attachment.created DESC'
        )
    );
    
    public $belongsTo = array(
        'EmailTemplate' => array(
            'className' => 'EmailTemplate',
            'foreignKey' => 'email_template_id'
        )
    );

}
