<?php

App::uses('Model', 'Model');

class Setting extends AppModel {

    public $name = 'Setting';

    public function getSettingsData() {
        
        $settings = $this->find('all');
        $result = array();
        if(!empty($settings)){
            foreach ($settings as $key => $setting){
                foreach ($setting as $newKey => $data){
                    $result[$newKey][$data['key']] = $data['value'];
                }
            }
        }
        return $result;
    }

}