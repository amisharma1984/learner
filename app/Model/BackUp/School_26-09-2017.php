<?php

App::uses('Model', 'Model');

class School extends AppModel {

    public $name = 'School';
    
    public $hasOne = array(
        'SchoolInformation' => array(
            'className' => 'SchoolInformation',
			 'foreignKey' => 'school_id',
//            'conditions' => array('SchoolInformation.isdeleted' => 0),
            'dependent' => true
        )
    );

    public function getLoginData($login = null, $password = null) {
        
        $login = trim($login);
        $hashed = md5(trim($password));

        $data = $this->find('first', array('conditions' => array('School.email' => $login, 'School.password' => $hashed, 'School.isdeleted' => 0), 'recursive' => 1));
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

}
