<?php

App::uses('Model', 'Model');

class ExaminationCategory extends AppModel {

    public $name = 'ExaminationCategory';
    
    public $belongsTo = array(
        'ExaminationType' => array(
            'className' => 'ExaminationType',
            'foreignKey' => 'Examination_type_id'
        )
    );

}