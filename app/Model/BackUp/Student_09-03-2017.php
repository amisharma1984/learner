<?php

App::uses('Model', 'Model');

class Student extends AppModel {

    public $name = 'Student';
	
	 public $belongsTo = array(
        'StudentClass' => array(
            'className' => 'StudentClass',
            'foreignKey' => 'student_class_id',
			 'dependent' => true
        ),
    );

    public function getLoginData($login = null, $password = null) {
        
        $login = trim($login);
        $hashed = md5(trim($password));

        $data = $this->find('first', array('conditions' => array('Student.email' => $login, 'Student.password' => $hashed, 'Student.isdeleted' => 0), 'recursive' => 1));
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

}
