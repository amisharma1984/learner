<?php

App::uses('Model', 'Model');

class Examination extends AppModel {

    public $name = 'Examination';

    public $hasMany = array(
        'Question' => array(
            'className' => 'Question',
            'conditions' => array('Question.isdeleted' => 0),
            'order' => 'Question.created DESC'
        )
    );
    
    public $belongsTo = array(
        'ExaminationType' => array(
            'className' => 'ExaminationType',
            'foreignKey' => 'examination_type_id',
            'conditions' => array('ExaminationType.isdeleted' => 0)
        ),
        'ExaminationCategory' => array(
            'className' => 'ExaminationCategory',
            'foreignKey' => 'examination_category_id',
            'conditions' => array('ExaminationCategory.isdeleted' => 0)
        )
    );

}
