<?php

App::uses('Model', 'Model');

class Examination extends AppModel {

    public $name = 'Examination';

    public $hasMany = array(
        'Question' => array(
            'className' => 'Question',
            'conditions' => array('Question.isdeleted' => 0),
            'order' => 'Question.created DESC'
        )
    );
	
	 public $hasOne = array(
       //commented below on 07-04-2017
	   /*'MyExam' => array(
            'className' => 'MyExam',
            'conditions' => array('MyExam.isdeleted' => 0, 'MyExam.payment_status' => 1)
        ),*/
		
		 'SchoolPurchaseExam' => array(
            'className' => 'SchoolPurchaseExam',
            'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1)
        )
    );
    
    public $belongsTo = array(
        'ExaminationType' => array(
            'className' => 'ExaminationType',
            'foreignKey' => 'examination_type_id',
            'conditions' => array('ExaminationType.isdeleted' => 0)
        ),
        'ExaminationCategory' => array(
            'className' => 'ExaminationCategory',
            'foreignKey' => 'examination_category_id',
            'conditions' => array('ExaminationCategory.isdeleted' => 0)
        )
    );

}
