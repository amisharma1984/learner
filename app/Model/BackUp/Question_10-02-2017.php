<?php

App::uses('Model', 'Model');

class Question extends AppModel {

    public $name = 'Question';

    public $hasMany = array(
        'Answer' => array(
            'className' => 'Answer',
            'conditions' => array('Answer.isdeleted' => 0),
            'order' => 'Answer.created DESC'
        )
    );
    
    public $belongsTo = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_id'
        ),
        'Subject' => array(
            'className' => 'Subject',
            'foreignKey' => 'subject_id'
        )
    );

}
