<?php

App::uses('Model', 'Model');

class MyExamStudentAnswer extends AppModel {

    public $name = 'MyExamStudentAnswer';
	
    public $belongsTo = array(
        'MyExam' => array(
            'className' => 'MyExam',
            'foreignKey' => 'my_exam_id',
            'conditions' => array('MyExam.isdeleted' => 0)
        )
       
    );

}
