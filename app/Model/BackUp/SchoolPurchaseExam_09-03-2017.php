<?php

App::uses('Model', 'Model');

class SchoolPurchaseExam extends AppModel {

    public $name = 'SchoolPurchaseExam';
    public $belongsTo = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_id',
        ),
        'School' => array(
            'className' => 'School',
            'foreignKey' => 'school_id'
        )
    );

}
