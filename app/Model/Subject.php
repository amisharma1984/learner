<?php

App::uses('Model', 'Model');

class Subject extends AppModel {

    public $name = 'Subject';

    public $hasMany = array(
        'Question' => array(
            'className' => 'Question',
            'conditions' => array('Question.isdeleted' => 0)
        )
    );

}
