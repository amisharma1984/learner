<?php

App::uses('Model', 'Model');

class Question extends AppModel {

    public $name = 'Question';

    public $hasMany = array(
        'Answer' => array(
            'className' => 'Answer',
            'conditions' => array('Answer.isdeleted' => 0),
            //'order' => 'Answer.created DESC'
           // 'order' => 'Answer.id ASC'
        ),
		
		 'CorrectAnswer' => array(
            'className' => 'Answer',
			 'foreignKey' => 'question_id',
            'conditions' => array('CorrectAnswer.iscorrect' => 1)
        )
    );
    
    public $belongsTo = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_id'
        ),
        'Subject' => array(
            'className' => 'Subject',
            'foreignKey' => 'subject_id'
        ),
		'ExamSection' => array(
            'className' => 'ExamSection',
            'foreignKey' => 'exam_section_id'
        )
    );

}
