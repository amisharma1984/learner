<?php

App::uses('Model', 'Model');

class StudentAnswer extends AppModel {

    public $name = 'StudentAnswer';
    
    public $belongsTo = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id'
        ),
       
    );

}
