<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class SchoolsController extends AppController {

    public $name = 'Schools';
    public $uses = array('Setting', 'School', 'SchoolInformation','Examination', 'SchoolPurchaseExam','Student', 'MyExamStudentAnswer', 'SchoolTakeExam');
    public $components = array('FilterSchools');
    public $settingData = array();
    
    const DEFAULTPAYMENTAMOUNT = 2;
    const FINGERPRINTKEY = '7XV7562x3Lhyk7Sb6';
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->settingData = $this->Setting->getSettingsData();
    }
    
    public function login() {
        $this->layout = false;
        if (!empty($this->request->data)) {
            $login = strtolower($this->request->data['School']['login']);
            $password = $this->request->data['School']['password'];
            $userdata = $this->School->getLoginData($login, $password);
            if (!empty($userdata)) {
                $this->UserConfiguration->setSchoolData($userdata);
                $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                //$this->redirect(array('controller' => 'schools', 'action' => 'index'));
                $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
            } else {
                $this->Session->setFlash(__('Invalid email or password!'), 'error');
                $this->redirect(array('controller' => 'schools', 'action' => 'login'));
            }
        }  else {
            $this->_schoolLoginRedirect();
        }
    }
	
	public function myprofile(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
        
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			    $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['School']['name']))){
								$ERROR = 1;
								$this->set('nameErr', 'Please enter school name');
							}
							
							if(empty(trim($this->request->data['School']['manager_name']))){
								$ERROR = 1;
								$this->set('managerNameErr', 'Please enter manager name');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							}
							
								if(empty(trim($this->request->data['School']['manager_email']))){
								$ERROR = 1;
								$this->set('manager_emailErr', 'Please enter manager email');
							} else {
								$manager_email = $this->request->data['School']['manager_email'];
								if (!filter_var($manager_email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('manager_emailErr', 'Please enter valid email format');
								}
							} 
							
							
							
							
							if(empty(trim($this->request->data['SchoolInformation']['level']))){
								$ERROR = 1;
								$this->set('levelErr', 'Please select level');
							}
							
							if(empty(trim($this->request->data['SchoolInformation']['street']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter street');
							}
							
								if(empty(trim($this->request->data['SchoolInformation']['district']))){
								$ERROR = 1;
								$this->set('streetErr', 'Please enter district');
							}
							
								if(empty(trim($this->request->data['SchoolInformation']['zip']))){
								$ERROR = 1;
								$this->set('zipErr', 'Please enter zip');
							}
							if(empty(trim($this->request->data['SchoolInformation']['type']))){
								$ERROR = 1;
								$this->set('typeErr', 'Please select zip');
							}
							
							
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emailErr', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emailErr', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email, 'School.id <>' => $school_id)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->request->data['School']['email'] = $this->request->data['School']['email'];
										$this->set('emailErr', 'This email already exist');
										$this->referer();
									}
							}
							
					 
					 if($ERROR == 0){		
							$this->School->save($this->request->data['School']);
							$this->SchoolInformation->save($this->request->data['SchoolInformation']);
							$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
							$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
					 } 
					
					
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
		}				 
	
    
    public function index() {
        
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			
			$schoolLevelOptions = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
					'order' => array('SchoolInformation.level ASC'),
					'group' => array('SchoolInformation.level')
				));
        
        
        $types = $this->SchoolInformation->find('list', array(
					'recursive' => -1,
					'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
					'order' => array('SchoolInformation.type ASC'),
					'group' => array('SchoolInformation.type')
				));
       
		  $this->set(compact('schoolLevelOptions','types'));
			//  pr($schoolAr);die;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					$this->School->save($this->request->data['School']);
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					$this->Session->setFlash(__('Profile has been updated successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'myprofile' ));
				 }
				 
			  $this->data = $schoolData;
			 $this->render('myprofile');
        
        
    }
	
	public function registration() {
        $this->layout = 'school_layout';
		//$this->layout = 'landing_layout';
		$this->_schoolLoginRedirect();
		 $examination_types = $this->ExaminationType->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationType.isdeleted' => 0) ));
		 $examination_categories = $this->ExaminationCategory->find('list', array('fields' => 'id, name', 'conditions' => array('ExaminationCategory.isdeleted' => 0) ));
		 
		   $schoolLevelOptions = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.level', 'SchoolInformation.level'),
            'order' => array('SchoolInformation.level ASC'),
            'group' => array('SchoolInformation.level')
        ));
        
        
        $types = $this->SchoolInformation->find('list', array(
            'recursive' => -1,
            'fields' => array('SchoolInformation.type', 'SchoolInformation.type'),
            'order' => array('SchoolInformation.type ASC'),
            'group' => array('SchoolInformation.type')
        ));
       
		  $this->set(compact('examination_types', 'examination_categories','schoolLevelOptions','types'));
			$ERROR = 0;
		 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
							
								 //school login start
			 if(!empty($this->request->data['SchoolLogin']['login_registration_flag'])  && $this->request->data['SchoolLogin']['login_registration_flag'] == 'school_login'){ 
			  $login = strtolower($this->request->data['SchoolLogin']['login']);
            $password = $this->request->data['SchoolLogin']['password'];
            $userdata = $this->School->getLoginData($login, $password);
			 
						if(empty(trim($this->request->data['SchoolLogin']['login']))){
								$ERROR = 1;
								$this->set('loginEmailErr', 'Please enter username');
							}
							
							if(empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Please enter password');
							}
							
						if(empty($userdata) && !empty(trim($this->request->data['SchoolLogin']['password']))){
								$ERROR = 1;
								$this->set('loginPassErr', 'Invalid email or password');
							}
			
			if($ERROR == 0){
				 $this->UserConfiguration->setSchoolData($userdata);
               // $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                 $this->Session->setFlash(__('You have loggedin successfully.'), 'success');
                $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
			} 
			 } else {
			
			 //student login End
							
							//school registration
							if(empty(trim($this->request->data['School']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['School']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['School']['email']))){
								$email = $this->request->data['School']['email'];
								$ExistEmail = $this->School->find('count', array('conditions' => array('School.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
						
							
							if(empty(trim($this->request->data['School']['captcha_txt']))){
								$ERROR = 1;
								$this->set('captchaErr', 'Please enter captcha code');
							} else {
								if(strcasecmp($this->data['School']['captcha_txt'],$this->Session->read('captcha'))!= 0)
								{
									$ERROR = 1;
									$this->set('captchaErr','Please enter correct captcha code ');
								} 
							}
							
							
							
							
				if($ERROR == 0){		
					$this->request->data['School']['username'] = $this->request->data['School']['email'];
					$this->request->data['School']['password'] =$this->_randomPassword(8);
					
					$this->School->save($this->request->data['School']);
					$this->request->data['SchoolInformation']['school_id'] = $this->School->getLastInsertID();
					$this->SchoolInformation->save($this->request->data['SchoolInformation']);
					
					 $this->Session->setFlash(__('Your registration has been done successfully.'), 'success');
					$this->redirect(array('controller' => 'Schools', 'action' => 'registration'));
				}
				
		 }
				
			 }
	  }

	
    
    public function view(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
    }
    
    public function edit($schoolId = null){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = false;
        $this->autoRender = false;
        
        
    }

	public function registration_payment_success(){
			
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 
			//below code for re-write session for payment status
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->UserConfiguration->setSchoolData($schoolData);
			 //after payment update check login
			 $this->UserConfiguration->isSchoolLoggedIn();
		}				 
	
    public function registration_payment(){
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
						 $school_id = $schoolData['School']['id'];
						//Credit card start here
						$ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $studentDetails['Student']['first_name'];
						$lastName= $studentDetails['Student']['last_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('USD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//pr($httpParsedResponseAr);die;
						
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								 $dateTime = new DateTime('NOW');
								$subscriptionStart = $dateTime->format('Y-m-d H:i:s');
								$schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
								$endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
								$subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
								
								$this->School->set(array(
									'id' => $school_id,
									'payment_status' => 1,
									'start_subscription' => $subscriptionStart,
									'end_subscription' => $subscriptionEnd,
									'modified' => $subscriptionStart
								));
								$this->School->save();
								$this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
								$this->redirect(array('controller'=>'schools','action'=>'registration_payment_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'students','action'=>'payment_error'));
							}
						}
						}
							///Credit card payment End from here
						}
		
		
		
        $timeStamp = time();
        $paymentAmount = (!empty($settings['Setting']['school_registration_fee'])) ? $settings['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT;
        if (phpversion() >= '5.1.2') {
            $fingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY, true);
        } else {
            $fingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $timeStamp . "^" . $paymentAmount . "^", self::FINGERPRINTKEY));
        }      
        
        $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
            'item_name' => 'School Registration',
            'item_number' => str_pad($schoolData['School']['id'], 10, "0", STR_PAD_LEFT),
            'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'school' => $schoolData['School']['id'],
            'payment_for' => 'registration',
            'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
            'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'app/payment_success';
        //$paypalSettings['return'] = Router::url('/', true) . 'app/registration_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'app/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'app/payment_cancle'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		$this->set(compact('paymentAmount'));	
			$this->render('make_payment');
    }
    
    public function checkFingerprint(){
        
        $this->layout = false;
        $this->autoRender = false;
        $flag = false;
        $settings = $this->_getSettingsData();
        $schoolData = $this->UserConfiguration->getSchoolData();
        
        $extraData = $this->request->data['custom'];
        $amount = $this->request->data['amount'];
        
        $payPalCustomData = array();
        $pagedata = explode('/', $extraData);
        foreach ($pagedata as $key => $value) {
            $exploaded = explode(':', $value);
            $payPalCustomData[$exploaded[0]] = $exploaded[1];
        }
        
        $oldFingerprint = urldecode($payPalCustomData['fp']);
        
        if (phpversion() >= '5.1.2') {
            $newFingerprint = hash_hmac("md5", $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY, true);
        } else {
            $newFingerprint = bin2hex(mhash(MHASH_MD5, $schoolData['School']['id'] . "^" . $payPalCustomData['tms'] . "^" . $amount . "^", self::FINGERPRINTKEY));
        }
        
        if($oldFingerprint != $newFingerprint){
            $flag = true;
        }
        echo $flag;
        exit();
    }

    public function registration_success(){
        $this->layout = false;
        
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('/', $transactionData['custom']);
            foreach ($pagedata as $key => $value) {
                $exploaded = explode(':', $value);
                $payPalCustomData[$exploaded[0]] = $exploaded[1];
            }
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $schoolRegistrationValidity = (!empty($this->settingData['Setting']['school_registration_validity']))?$this->settingData['Setting']['school_registration_validity']:100;
            $endDate = strtotime ( '+'.$schoolRegistrationValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            $fingerPrint = urldecode($payPalCustomData['fp']);
            
            $this->School->read(null, $payPalCustomData['school']);
            $this->School->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            $this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'schools', 'action' => 'login'));
        $this->redirect(array('controller' => 'schools', 'action' => 'registration_payment_success'));
		
        }
        
    }
    
    public function registration_success_ipn(){
        $this->layout = false;
        
    }
    
    public function logout() {
        $this->UserConfiguration->schoolLogout();
        $this->Session->setFlash(__('You have logged out.'), 'success');
        //$this->redirect(array('controller' => 'schools', 'action' => 'login'));
        $this->redirect(array('controller' => 'schools', 'action' => 'registration'));
    }
    
    protected function _schoolLoginRedirect(){
       // $isLoggedIn = $this->Session->check('App.schoolData');
        $isSchoolLoggedIn = $this->Session->check('App.schoolData');
        if ($isSchoolLoggedIn) {
            $this->Session->setFlash(__('You have already loggedin.'), 'info');
            $this->redirect(array('controller' => 'schools', 'action' => 'myprofile'));
        }else{
            return;
        }
    }
	
		//added by Dinesh
	
			function PPHttpPost($methodName_, $nvpStr_) {
				   //global $environment;
				   //test
					$environment = 'sandbox';
					//Live
					//$environment = 'live';
					// Set up your API credentials, PayPal end point, and API version.
					//$API_UserName = urlencode('my_api_username');
					//test
					//$API_UserName = urlencode('arinda_1348831794_biz_api1.gmail.com');
					$API_UserName = urlencode('pranay.pandey_api1.amstech.co.in');
					//Live
					//$API_UserName = urlencode('dateagentleman_api1.yahoo.com');
					//$API_Password = urlencode('my_api_password');
					//test
					//$API_Password = urlencode('1348831846');
					$API_Password = urlencode('2J8Z2BR2T354TSQ2');
					
					//Live
					//$API_Password = urlencode('HL6RBRMB8RWDE2HD');
					
					//$API_Signature = urlencode('my_api_signature');
					//test
					//$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31A-JuZdO-h7REihAVpasobL.3Sf.H');
					$API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AX-32VNNgIsHF2yLSrKHLyrY1bxd');
					
					
					//Live
					//$API_Signature = urlencode('AC5vWJiGkEYMFWxaVkBm-SaT0WPUAuL7VVo7gIhnjULYM-yWXOtxkUIn');
					
					
					
					
					$API_Endpoint = "https://api-3t.paypal.com/nvp";
					if("sandbox" === $environment || "beta-sandbox" === $environment) {
						$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
					}
					$version = urlencode('51.0');
			
					// Set the curl parameters.
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
			
					// Turn off the server and peer verification (TrustManager Concept).
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
			
					// Set the API operation, version, and API signature in the request.
					$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
					// Set the request as a POST FIELD for curl.
					curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
					// Get response from the server.
					$httpResponse = curl_exec($ch);
			
					if(!$httpResponse) {
						exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
					}
			
					// Extract the response details.
					$httpResponseAr = explode("&", $httpResponse);
			
					$httpParsedResponseAr = array();
					foreach ($httpResponseAr as $i => $value) {
						$tmpAr = explode("=", $value);
						if(sizeof($tmpAr) > 1) {
							$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
						}
					}
			
					if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
						exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
					}
					return $httpParsedResponseAr;
				}
					  
	
	//School purchase exam module start
	
	 public function create_exam($id = NULL) {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		 $id = $this->data_decrypt($id);
		 $this->set('school_purchase_exam_id', $id);
		 
		 $this->SchoolPurchaseExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
		 $myPurchasedExam = $this->SchoolPurchaseExam->find('first', array(
																										 'conditions' => array(
																																		 'SchoolPurchaseExam.id' => $id, 
																																		 'SchoolPurchaseExam.isdeleted' => 0, 
																																		 'SchoolPurchaseExam.payment_status' => 1
																																		))); // examination_type_id =1 means online exam
		//pr($myPurchasedExam);die;
		
			if($this->request->is('post') || $this->request->is('put')){
				$this->request->data['SchoolTakeExam']['exam_code'] = $myPurchasedExam['SchoolPurchaseExam']['exam_code'];
				
				//pr($this->request->data);die;
				if($this->SchoolTakeExam->save($this->request->data)){
					$lastId = $this->SchoolTakeExam->getLastInsertID();
					$exam_code =  $myPurchasedExam['SchoolPurchaseExam']['exam_code'].'-'.$lastId;
					$this->SchoolTakeExam->id = $lastId;
					$this->SchoolTakeExam->saveField('exam_code', $exam_code);
					$this->Session->setFlash(__('Exam has been created successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'take_exam_list'));
				}
			}
		 $this->set(compact('SchoolDetails', 'myPurchasedExam'));
    }
	
	
	 public function my_purchased_exam() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		 $this->SchoolPurchaseExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			/*$this->Examination->bindModel(array(
            'hasOne' => array(
                'SchoolPurchaseExam' => array(
                    'className' => 'SchoolPurchaseExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1, 'SchoolPurchaseExam.school_id' => $School_id)
                ),
            )

        ));	*/										
		 $myPurchasedExam = $this->SchoolPurchaseExam->find('all', array(
																										 'conditions' => array(
																																		 'SchoolPurchaseExam.school_id' => $School_id, 
																																		 'SchoolPurchaseExam.isdeleted' => 0, 
																																		 'SchoolPurchaseExam.payment_status' => 1
																																		))); // examination_type_id =1 means online exam
		//pr($myPurchasedExam);die;
		 $this->set(compact('SchoolDetails', 'myPurchasedExam'));
    }
	
	
	 public function take_exam_list() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		$this->SchoolTakeExam->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
											
		 $allExamAr = $this->SchoolTakeExam->find('all', array('conditions' => array('SchoolTakeExam.isdeleted' => 0, 'SchoolTakeExam.school_id' => $School_id))); 
		//pr($allExamAr);die;
		 $this->set(compact('SchoolDetails', 'allExamAr'));
    }
	
	
	
	
	 public function purchase_exam() {
        
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
		// $this->Examination->recursive = 2;
			$this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
														
			$this->Examination->bindModel(array(
            'hasOne' => array(
                'SchoolPurchaseExam' => array(
                    'className' => 'SchoolPurchaseExam',
                    'foreignKey' => 'examination_id',
					 'conditions' => array('SchoolPurchaseExam.isdeleted' => 0, 'SchoolPurchaseExam.payment_status' => 1, 'SchoolPurchaseExam.school_id' => $School_id)
                ),
            )

        ));											
		 $allExamAr = $this->Examination->find('all', array('conditions' => array('Examination.isdeleted' => 0, 'Examination.examination_type_id' => 1))); // examination_type_id =1 means online exam
		//pr($allExamAr);die;
		 $this->set(compact('SchoolDetails', 'allExamAr'));
		    $paypalSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'School Registration',
            'item_number' => str_pad($SchoolDetails['School']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'school' => $SchoolDetails['School']['id'],
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "/";
            }
            $custom .= $key . ":" . $data;
        }
        //$custom = implode('|', $customedata);echo $custom.'<br>';
       // $paypalSettings['custom'] = $custom; //Router::url('/', true)
        $paypalSettings['return'] = Router::url('/', true) . 'Schools/payment_success';
        $paypalSettings['notify_url'] = Router::url('/', true) . 'Schools/payment_notify';
        $paypalSettings['cancel_return'] = Router::url('/', true) . 'Schools/payment_cancel'. '/' . $custom;
        $this->set('paypalFormSettings', $paypalSettings);  
		 
		 
		
    }
	
	  public function purchase_exam_payment($examination_id = NULL){
					$this->UserConfiguration->isSchoolLoggedIn();
					$this->layout = 'school_layout';
					$examination_id = $this->data_decrypt($examination_id);
					
					 $Schooldata1 = $this->UserConfiguration->getSchoolData();
					 $School_id = $Schooldata1['School']['id'];
					 $SchoolDetails = $this->School->find('first', array('conditions' => array('School.id' => $School_id)));
					
					$schoolFullName = strtoupper(str_replace(',', '', $SchoolDetails['School']['name']));
					$words = explode(" ", $schoolFullName);
					$schoolShortName = "";
					foreach ($words as $value) {
						$schoolShortName .= substr($value, 0, 1); 
					}
					 
					 $examDtls = $this->Examination->find('first', array('fields' => 'id, price,title','conditions' => array('Examination.id' => $examination_id, 'Examination.isdeleted' => 0)));
					 $amount = $examDtls['Examination']['price'];
					 $title = $examDtls['Examination']['title'];
					 $examination_id = $examDtls['Examination']['id'];
					 $ERROR = 0;
						if($this->request->is('post') || $this->request->is('put')){
							
							if(empty(trim($this->request->data['Payment']['credit_card_no']))){
								$ERROR = 1;
								$this->set('card_err', 'Please enter card number');
							}
							if(empty(trim($this->request->data['Payment']['exp_month'])) || empty(trim($this->request->data['Payment']['exp_year']))){
								$ERROR = 1;
								$this->set('exp_month_err', 'Please select month and year');
							}
							
							if(empty(trim($this->request->data['Payment']['security_code']))){
								$ERROR = 1;
								$this->set('security_err', 'Please enter security code');
							}
							
							if(empty(trim($this->request->data['Payment']['name_on_card']))){
								$ERROR = 1;
								$this->set('name_err', 'Please enter card holder name');
							}
							
							if(!empty($this->request->data['Payment']['exp_year'])){
								$this->set('year', $this->request->data['Payment']['exp_year']);
							}
							
							if(!empty($this->request->data['Payment']['exp_month'])){
								$this->set('exp_month', $this->request->data['Payment']['exp_month']);
							}
							
								///payment Start from here
						if($ERROR == 0){		
						$firstName= $SchoolDetails['School']['name'];
						$lastName= $SchoolDetails['School']['manager_name'];			
						$address1= 'Sector V';
						$country=44;
						//$state=$county;
						$state='India';
						$city= 'Kolkata';
						$zip='700050';
						$paymentType = urlencode('Authorization');
						$amount= $this->request->data['Payment']['amount'];
						$ccType = $this->request->data['Payment']['payment_type'];
						$creditCardType = urlencode($ccType);
						$creditCardNumber=$this->request->data['Payment']['credit_card_no'];
						$expDateMonth =$this->data['Payment']['exp_month'];
						// Month must be padded with leading zero
						$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));
						$expDateYear=urlencode($this->data['Payment']['exp_year']);
						$cvv2Number=urlencode($this->data['Payment']['security_code']);			
						$currencyID=urlencode('USD');
						
						$nvpStr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
                       "&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
                       "&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
					   //pr($nvpStr);exit;
						$httpParsedResponseAr = $this->PPHttpPost('DoDirectPayment', $nvpStr);
						//echo 'hello=><pre>';print_r($httpParsedResponseAr);die;
						
						
							if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
							{
								$this->request->data['SchoolPurchaseExam']['school_id'] = $School_id;
								$this->request->data['SchoolPurchaseExam']['exam_code'] = $School_id.'-'.$schoolShortName.'-'.$examination_id;
								
								$this->request->data['SchoolPurchaseExam']['examination_id'] = $examination_id;
								$this->request->data['SchoolPurchaseExam']['payment_status'] = 1;
								$this->request->data['SchoolPurchaseExam']['txn_id'] = $httpParsedResponseAr["TRANSACTIONID"];
								$this->request->data['SchoolPurchaseExam']['amount'] = $amount;
								
									$this->SchoolPurchaseExam->save($this->request->data);
								
								// $this->Flash->success(__("transaction successfully completed"));
								 //$this->Session->setFlash('Transaction successfully completed');
								$this->redirect(array('controller'=>'Schools','action'=>'purchase_exam_success'));
								
						
							
						}
						else
						{
							//echo "<br>";
							//print_r($httpParsedResponseAr);
							
							//exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
							//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
						   // $error_mesage = str_replace('%20',' ',$httpParsedResponseAr['L_LONGMESSAGE0']);
						   $error_mesage = $httpParsedResponseAr['L_LONGMESSAGE0'];
							if($error_mesage == 'Internal Error') {
								//$this->Session->setFlash('Internal Error. Processing timeout. Please try again.');
								$this->set('payment_error', 'Internal Error. Processing timeout. Please try again.');
								$this->redirect(array('controller'=>'Schools','action'=>'payment_error'));
							} else {
								$error_mesage = str_replace('%20',' ', $error_mesage);
								$error_mesage = str_replace('%2e','.', $error_mesage);
								
								$this->Session->setFlash($error_mesage);
								
								$this->set('payment_error', $error_mesage);
								$this->redirect(array('controller'=>'Schools','action'=>'payment_error'));
							}
						}
						}
							///payment End from here
						}
						
					  $paypalFormSettings = array(
            'payment_url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', // Fro Live https://www.paypal.com/cgi-bin/webscr ,  For Sand box https://www.sandbox.paypal.com/cgi-bin/webscr
            'cmd' => '_xclick',
            'business' => 'dpdineshray@gmail.com', // For SandBox 'paypal-facilitator@surfernet.com' // for live 'paypal@surfernet.com',
           // 'item_name' => 'School Registration',
            'item_number' => str_pad($SchoolDetails['School']['id'], 10, "0", STR_PAD_LEFT),
           // 'amount' => $paymentAmount,
            'quantity' => 1,
            'currency_code' => 'USD'
        );

        $customdata = array(
            'school_id' => $SchoolDetails['School']['id'],
            'examination_id' => $examination_id,
            'payment_for' => 'Purchase Exam',
           // 'fp' => urlencode($fingerprint),   /* fp -> stands for fingerPrint */
           // 'tms' => $timeStamp     /* tms -> stands for fingerPrint */
        );         

        $custom = null;
        foreach ($customdata as $key => $data) {

            if (!empty($custom)) {
                $custom .= "";
            }
            $custom .= "##" . $data;
        }
       // $custom = implode('|', $customedata);echo $custom.'<br>';
        $paypalFormSettings['item_name'] = $title; //Router::url('/', true)
        $paypalFormSettings['custom'] = trim($custom, '##'); //Router::url('/', true)
        $paypalFormSettings['amount'] = $amount; //Router::url('/', true)
        $paypalFormSettings['return'] = Router::url('/', true) . 'Schools/payment_success';
        $paypalFormSettings['notify_url'] = Router::url('/', true) . 'Schools/payment_notify';
        $paypalFormSettings['cancel_return'] = Router::url('/', true) . 'Schools/payment_cancel'. '/' . $custom;
		$this->set(compact('SchoolDetails', 'examDtls','paypalFormSettings'));
		   }
    
	
	public function purchase_exam_success(){
			
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 
			//below code for re-write session for payment status
			$schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->UserConfiguration->setSchoolData($schoolData);
			 //after payment update check login
			 $this->UserConfiguration->isSchoolLoggedIn();
			 $this->Session->setFlash(__('Payment has been successfully done.'), 'success');
			 $this->render('registration_payment_success');
		}			
	
	
	
	
	
    public function purchase_exam_success__(){
        $this->UserConfiguration->isSchoolLoggedIn();
        $this->layout = 'school_layout';
		 $Schooldata1 = $this->UserConfiguration->getSchoolData();
		 $School_id = $Schooldata1['School']['id'];
		 $SchoolDetails = $this->School->find('first', array('School.id' => $School_id));
        $this->set(compact('SchoolDetails'));
		
		die;
        if($this->Session->check('School.transactionData')){
            
            $transactionData = $this->Session->read('School.transactionData');
            $this->Session->delete('School.transactionData');
            
            $payPalCustomData = array();
            $pagedata = explode('##', $transactionData['custom']);
             $School_id = $pagedata[0]; 
			 $School_id = $pagedata[1]; 
			  
			  
            $dateTime = new DateTime('NOW');
            $subscriptionStart = $dateTime->format('Y-m-d H:i:s');
            $ExamPurchageValidity = 30;
            $endDate = strtotime ( '+'.$ExamPurchageValidity.' days' , strtotime ( $subscriptionStart ) ) ;
            $subscriptionEnd = date ( 'Y-m-d H:i:s' , $endDate );
            //$fingerPrint = urldecode($payPalCustomData['fp']);
            
           // $this->School->read(null, $payPalCustomData['School']);
            $this->SchoolPurchaseExam->set(array(
                'payment_status' => 1,
                'start_subscription' => $subscriptionStart,
                'end_subscription' => $subscriptionEnd,
                'modified' => $subscriptionStart
            ));
            //$this->School->save();
            $this->Session->setFlash(__('Thank\'s for your payment. Your subscription valid till <strong>'.date ( 'jS F, Y' , $endDate ).'</strong>.'), 'success');
        //$this->redirect(array('controller' => 'Schools', 'action' => 'login'));
        }
        
    }
    
	
	//School purchase exam module End
	
	//add student by school manager start
	  public function add_student(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $school_purchase_exams = $this->SchoolPurchaseExam->find('all', array(
																																	 'fields' => array('SchoolPurchaseExam.id', 'Examination.id', 'Examination.title'),
																																	 'conditions' => array(
																																	 'SchoolPurchaseExam.school_id' => $school_id,
																																	 'SchoolPurchaseExam.payment_status' => 1,
																																	 'SchoolPurchaseExam.isdeleted' => 0,
																																	 'SchoolPurchaseExam.isexpired' => 0,
																																	  'Examination.isdeleted' => 0,
																																	 
																																	 )));
			$SchoolPurchaseExamOption = '';
			foreach($school_purchase_exams as $val){
				$SchoolPurchaseExamOption .= '<option value="'.$val['SchoolPurchaseExam']['id'].'" >'.$val['Examination']['title'].' </option>';
			}
			 
			 $this->set(compact('SchoolPurchaseExamOption'));
			   $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							//pr($this->data);die;
						
				if($ERROR == 0){		
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					//$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->request->data['Student']['school_id'] = $school_id;
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Student data has been added successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'student_list'));
				}						
							
							
				 }
			 

			
	  }
	  
	  
	    public function edit_student($student_id = NULL){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$student_id = $this->data_decrypt($student_id);
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $school_purchase_exams = $this->SchoolPurchaseExam->find('all', array(
																																	 'fields' => array('SchoolPurchaseExam.id', 'Examination.id', 'Examination.title'),
																																	 'conditions' => array(
																																	 'SchoolPurchaseExam.school_id' => $school_id,
																																	 'SchoolPurchaseExam.payment_status' => 1,
																																	 'SchoolPurchaseExam.isdeleted' => 0,
																																	 'SchoolPurchaseExam.isexpired' => 0,
																																	  'Examination.isdeleted' => 0,
																																	 
																																	 )));
			$SchoolPurchaseExamOption = '';
			foreach($school_purchase_exams as $val){
				$SchoolPurchaseExamOption .= '<option value="'.$val['SchoolPurchaseExam']['id'].'" >'.$val['Examination']['title'].' </option>';
			}
			 
			 $this->set(compact('SchoolPurchaseExamOption'));
			   $ERROR = 0;
			 	 if($this->request->is('post') || $this->request->is('put') || !empty($this->request->data)){
					 
					 	if(empty(trim($this->request->data['Student']['first_name']))){
								$ERROR = 1;
								$this->set('fnerror', 'Please enter first name');
							}
							
							if(empty(trim($this->request->data['Student']['last_name']))){
								$ERROR = 1;
								$this->set('lnerror', 'Please enter last name');
							}
							
							if(empty(trim($this->request->data['Student']['address']))){
								$ERROR = 1;
								$this->set('adderror', 'Please enter address');
							}
							if(empty(trim($this->request->data['Student']['school_name']))){
								$ERROR = 1;
								$this->set('scerror', 'Please enter school name');
							}
							
							
							if(empty(trim($this->request->data['Student']['email']))){
								$ERROR = 1;
								$this->set('emerror', 'Please enter email');
							} else {
								$email = $this->request->data['Student']['email'];
								if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$ERROR = 1;
									$this->set('emerror', 'Please enter valid email format');
								}
							} 
							
							if(!empty(trim($this->request->data['Student']['email']))){
								$email = $this->request->data['Student']['email'];
								$ExistEmail = $this->Student->find('count', array('conditions' => array('Student.email' => $email, 'Student.id !=' => $student_id)));
									if($ExistEmail > 0){
										$ERROR = 1;
										$this->set('emerror', 'This email already exist, please try another');
									}
							}
							
							
							//pr($this->data);die;
						
				if($ERROR == 0){		
					$this->request->data['Student']['id'] = $student_id;
					$this->request->data['Student']['username'] = $this->request->data['Student']['email'];
					//$this->request->data['Student']['password'] = md5($this->request->data['Student']['password']);
					$this->request->data['Student']['school_id'] = $school_id;
					$this->Student->save($this->request->data);
					
					 $this->Session->setFlash(__('Student data has been updated successfully.'), 'success');
					$this->redirect(array('controller' => 'schools', 'action' => 'student_list'));
				}						
							
							
				 }
			 
			 $this->request->data = $this->Student->read(NULL,$student_id);
			 $this->render('add_student');

			
	  }
	  
	
	
	  public function student_list_appeared_in_exam(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->MyExamStudentAnswer->recursive = 3;
			 $this->Examination->unbindModel(
														array('hasMany' => array('Question'))
														);
			 $this->MyExamStudentAnswer->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			
			// $MyExamStudentAnswer = $this->MyExamStudentAnswer->find('all');
			 //pr($MyExamStudentAnswer);die;
			 
			  $conditions[] = array(
												  'MyExamStudentAnswer.school_id' => $school_id, 
												  'MyExamStudentAnswer.isdeleted' => 0,
												  'MyExamStudentAnswer.exam_type' => 2 //online exam by school
												  );
			 if($this->request->is('post') || $this->request->is('put')){
				//pr($this->data);
				$search_text = $this->request->data['search_text'];
				$search_by = $this->request->data['search_by'];
				 $conditions[] = array('MyExamStudentAnswer.school_id' => $school_id, 'MyExamStudentAnswer.isdeleted' => 0);
					if($search_by == 'name' && !empty(trim($search_text))){
						$conditions[] = array(
															'OR' => array(
																				'Student.first_name LIKE' => '%'.$search_text.'%',
																				'Student.last_name LIKE' =>  '%'.$search_text.'%',
																				'MyExamStudentAnswer.name LIKE' =>  '%'.$search_text.'%',
																				)
															);
					}		

					if($search_by == 'email' && !empty(trim($search_text))){
						$conditions[] = array('Student.email LIKE' => '%'.$search_text.'%',);
					}				
												
				  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 } else {
			
			  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 }
			 
			$studentList = $this->paginate('MyExamStudentAnswer');
			
			
			$this->set(compact('studentList'));	
			//pr($conditions);
				//pr($studentList);
	      }
	
	//add student by school manager End
	  public function student_list(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->layout = 'school_layout';
			$settings = $this->_getSettingsData();
			$schoolData = $this->UserConfiguration->getSchoolData();
			 $school_id = $schoolData['School']['id'];
			 $schoolData = $this->School->find('first', array('conditions' => array('School.id' => $school_id)));
			 $this->Student->virtualFields['name'] = 'CONCAT(Student.first_name, " ",Student.last_name)';
			 
			 if($this->request->is('post') || $this->request->is('put')){
				//pr($this->data);
				$search_text = $this->request->data['search_text'];
				$search_by = $this->request->data['search_by'];
				 $conditions[] = array('Student.school_id' => $school_id, 'Student.isdeleted' => 0);
					if($search_by == 'name' && !empty(trim($search_text))){
						$conditions[] = array(
															'OR' => array(
																				'Student.first_name LIKE' => '%'.$search_text.'%',
																				'Student.last_name LIKE' =>  '%'.$search_text.'%',
																				'Student.name LIKE' =>  '%'.$search_text.'%',
																				)
															);
					}		

					if($search_by == 'email' && !empty(trim($search_text))){
						$conditions[] = array('Student.email LIKE' => '%'.$search_text.'%',);
					}				
												
				  $this->paginate = array(
							'conditions' =>$conditions,
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 } else {
			
			  $this->paginate = array(
							'conditions' => array('Student.school_id' => $school_id, 'Student.isdeleted' => 0),
							'limit' => 10,
							'order' => 'Student.id DESC'
						);
			 }
			 
			$studentList = $this->paginate('Student');
			
			
			$this->set(compact('studentList'));	
				//pr($studentList);
	      }
	
	 public function delete_student(){
			$this->UserConfiguration->isSchoolLoggedIn();
			$this->autoRender = false;
			$this->layout = 'ajax';
			$student_id = $this->request->data['student_id'];
			if(!empty($student_id)){
				$this->Student->id = $student_id;
				$this->Student->saveField('isdeleted', 1);
			}
			
	 }
	
	
}
?>