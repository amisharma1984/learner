<?php

App::uses('Model', 'Model');

class ExaminationType extends AppModel {

    public $name = 'ExaminationType';
    
    public $hasMany = array(
        'ExaminationCategory' => array(
            'className' => 'ExaminationCategory',
            'conditions' => array('ExaminationCategory.isdeleted' => 0)
        )
    );
}