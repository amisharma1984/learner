<?php

App::uses('Model', 'Model');

class StudentParent extends AppModel {

    public $name = 'StudentParent';
	
	 public $hasMany = array(
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'student_parent_id',
			 'dependent' => true
        ),
    );

    public function getParentLoginData($login = null, $password = null) {
        
        $login = trim($login);
        $hashed = md5(trim($password));

        $data = $this->find('first', array('conditions' => array('StudentParent.email' => $login, 'StudentParent.password' => $hashed, 'StudentParent.isdeleted' => 0), 'recursive' => 1));
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

}
