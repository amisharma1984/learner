<?php

App::uses('Model', 'Model');

class MyExam extends AppModel {

    public $name = 'MyExam';
    public $belongsTo = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_id',
        ),
        'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'student_id'
        )
    );

}
