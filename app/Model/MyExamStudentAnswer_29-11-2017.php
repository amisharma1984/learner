<?php

App::uses('Model', 'Model');

class MyExamStudentAnswer extends AppModel {

    public $name = 'MyExamStudentAnswer';
	
    public $belongsTo = array(
        'MyExam' => array(
            'className' => 'MyExam',
            'foreignKey' => 'my_exam_id',
            'conditions' => array('MyExam.isdeleted' => 0)
        ),
		 'SchoolPurchaseExam' => array(
            'className' => 'SchoolPurchaseExam',
            'foreignKey' => 'my_exam_id',
            'conditions' => array('SchoolPurchaseExam.isdeleted' => 0)
        ),
		 'Student' => array(
            'className' => 'Student',
            'foreignKey' => 'student_id',
            'conditions' => array('Student.isdeleted' => 0)
        )
       
    );

}
