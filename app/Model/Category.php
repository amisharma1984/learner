<?php

class Category extends AppModel {

    public $name = 'Category';
    public $hasAndBelongsToMany = array(
        'Archive' => array(
            'className' => 'Archive',
            'joinTable' => 'archives_categories',
        ),
        'Article' => array(
            'className' => 'Article',
            'joinTable' => 'articles_categories',
        ),
         'Topic' => array(
            'className' => 'Topic',
            'joinTable' => 'topics_categories',
        ),
        'Content' => array(
            'className' => 'Content',
            'joinTable' => 'contents_categories',
        ),
        'RequestQuotation' => array(
            'className' => 'RequestQuotation',
            'joinTable' => 'request_quotations_categories',
        ),
        'Licitation' => array(
            'className' => 'Licitation',
            'joinTable' => 'licitations_categories',
        ),
    );
//    public $validate = array(
//        'name' => array(
//            'required' => true,
//            'message' => 'You must fill this field.'
//        )
//    );

    public function afterFind($results, $primary = false) {
//        foreach ($results as $key => $val) {
//            $val['Category']['name']= Sanitize::clean($val['Category']['name'], array(' ', '@'));
//            $results[$key] = $val;
//        }
        return $results;
    }

    public function getCloudCategories($model = 'Archive', $submodel = 'event') {
        $this->unbindModel(array('hasAndBelongsToMany' => array($model)));
        $this->bindModel(array('hasAndBelongsToMany' => array($model => array(
                    'className' => $model,
                    'joinTable' => Inflector::underscore(Inflector::pluralize($model)) . '_categories'
        ))));

        $conditions = array('Category.community_id' => Configure::read('Community.id'));
        
        $joins = array();
        if ($model == 'Content') {
            $conditions[$model . '.type'] = $submodel;
            array_push($joins, array(
                'table' => 'contents_categories',
                'alias' => 'ContentsCategory',
                'type' => 'LEFT',
                'conditions' => array(
                    'Category.id = ContentsCategory.category_id',
                )
            ));
            array_push($joins, array(
                'table' => 'contents',
                'alias' => 'Content',
                'type' => 'LEFT',
                'conditions' => array(
                    'Content.id = ContentsCategory.content_id',
                )
            ));
        }
        if (!Configure::read('User.isAdministrator'))
            $conditions['Category.private'] = 0;
        $cats = $this->find('all', array('conditions' => $conditions, 'joins' => $joins, 'group' => array('Category.id')));
        $r = array();

        foreach ($cats as $cat) {
            if (isset($cat[$model])) {
                foreach ($cat[$model] as $key => $value)
                {
                    if ($value['deleted'] == 1)
                    {
                        unset($cat[$model][$key]);
                    }else if ($submodel=='product' && $value['units']<=$value['units_reserved']+$value['units_sold'])
                    {
                        unset($cat[$model][$key]);
                    }
                    
                }
                   

                if (count($cat[$model]) > 0) {
                    array_push($r, array(
                        'id' => $cat['Category']['id'],
                        'name' => $cat['Category']['name'],
                        'slug' => $cat['Category']['slug'],
                        'weight' => count($cat[$model])
                    ));
                }
            }
        }
        return $r;
    }

}