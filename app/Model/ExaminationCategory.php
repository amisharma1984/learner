<?php

App::uses('Model', 'Model');

class ExaminationCategory extends AppModel {

    public $name = 'ExaminationCategory';
    
    public $belongsTo = array(
        'ExaminationType' => array(
            'className' => 'ExaminationType',
            'foreignKey' => 'examination_type_id'
        )
    );
	
	//added by Dinesh on 03-05-2017
	 public $hasMany = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_category_id',
			'conditions' => array('Examination.isdeleted' => 0)
        )
    );

}