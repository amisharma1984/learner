<?php

App::uses('Model', 'Model');

class SchoolInformation extends AppModel {

    public $name = 'SchoolInformation';
    
    public $belongsTo = array(
        'School' => array(
            'className' => 'School',
            'foreignKey' => 'school_id'
        )
    );

}
