<?php

App::uses('Model', 'Model');

class SchoolTakeExam extends AppModel {

    public $name = 'SchoolTakeExam';
	
	    public $hasMany = array(
        'MyExamStudentAnswer' => array(
            'className' => 'MyExamStudentAnswer',
			 'foreignKey' => 'school_take_exam_id',
             // 'conditions' => array('MyExamStudentAnswer.isdeleted' => 0),
            'dependent' => true
        )
    );
	
	
    public $belongsTo = array(
        'Examination' => array(
            'className' => 'Examination',
            'foreignKey' => 'examination_id',
            'conditions' => array('Examination.isdeleted' => 0)
        ),
        'SchoolPurchaseExam' => array(
            'className' => 'SchoolPurchaseExam',
            'foreignKey' => 'school_purchase_exam_id',
            'conditions' => array('SchoolPurchaseExam.isdeleted' => 0)
        )
    );

}
