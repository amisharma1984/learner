<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
		
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/user/verify/*', array('controller' => 'pages', 'action' => 'verify'));
	Router::connect('/email/status/*', array('controller' => 'pages', 'action' => 'mail_status'));
	
	Router::connect('/', array('controller' => 'homes', 'action' => 'index'));
	
	Router::connect('/schools-access/online-testing/primary', array('controller' => 'homes', 'action' => 'primary_school'));
	Router::connect('/schools-access/online-testing/high-school', array('controller' => 'homes', 'action' => 'high_school'));
	
	Router::connect('/schools-access/online-testing/primary/sample-questions/*', array('controller' => 'homes', 'action' => 'sample_questions'));
	Router::connect('/schools-access/online-testing/high-school/sample-questions/*', array('controller' => 'homes', 'action' => 'sample_questions'));
	Router::connect('/school/*', array('controller' => 'schools', 'action' => 'login_new'));
	Router::connect('/school/login', array('controller' => 'schools', 'action' => 'login_new'));
	
	
	
	Router::connect('/students-access/online-practice/primary', array('controller' => 'homes', 'action' => 'primary_student'));
	Router::connect('/students-access/online-practice/high-school', array('controller' => 'homes', 'action' => 'high_school_student'));
	//Router::connect('/parent/registration/*', array('controller' => 'students', 'action' => 'registration'));
	
	
	Router::connect('/students-access/online-naplan-practice/primary/sample-questions/*', array('controller' => 'homes', 'action' => 'sample_questions'));
	Router::connect('/students-access/online-naplan-practice/high-school/sample-questions/*', array('controller' => 'homes', 'action' => 'sample_questions'));
	Router::connect('/student/*', array('controller' => 'students', 'action' => 'login_new'));
	
	Router::connect('/about-us', array('controller' => 'homes', 'action' => 'about_us'));
	
	Router::connect('/company-information', array('controller' => 'homes', 'action' => 'company_information'));
	Router::connect('/terms-of-service', array('controller' => 'homes', 'action' => 'terms_of_service'));
	Router::connect('/privacy-policy', array('controller' => 'homes', 'action' => 'privacy_policy'));
	Router::connect('/we-offer/*', array('controller' => 'homes', 'action' => 'we_offer'));
	
	
	Router::connect('/contact-us', array('controller' => 'homes', 'action' => 'contact_us'));
	
	Router::connect('/parent/registration/*', array('controller' => 'student_parents', 'action' => 'registration'));
	Router::connect('/parent/login', array('controller' => 'student_parents', 'action' => 'login'));
	Router::connect('/parent/family-profiles', array('controller' => 'student_parents', 'action' => 'family_profiles'));
	Router::connect('/parent/exams_available', array('controller' => 'student_parents', 'action' => 'exams_available'));
	Router::connect('/parent/take_exam_list', array('controller' => 'student_parents', 'action' => 'take_exam_list'));
	Router::connect('/parent/childrens_results', array('controller' => 'student_parents', 'action' => 'childrens_results'));
	Router::connect('/parent/*', array('controller' => 'student_parents', 'action' => 'login'));
	
	

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
