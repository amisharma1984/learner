<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('School Management'), "link" => array( 'controller' => 'school_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-cog"></i> School Management</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mb-sm">
                        <input type="button" class="btn btn-primary btn-sm" value="Add School" style="margin-bottom: 10px;" data-toggle="modal" data-target="#newSchoolModal">
                        <input type="button" class="btn btn-primary btn-sm selectAllSchoolButton" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" class="btn btn-primary btn-sm" value="Delete All" style="margin-bottom: 10px;" data-toggle="modal" data-target="#deleteSchoolModal">
                    </div>
                </div>
                <div class="row">    
                    <?php
                    $selectedLevel = (!empty($level))?explode(',', $level):'';
                    $selectedType = (!empty($type))?explode(',', $type):'';
                    ?>
                    <?php echo $this->Form->create('School', array('url' => array('controller' => 'school_managements', 'action' => 'search'), 'class' => 'form-inline filters', 'role' => 'form' )); ?>
                    <?php
                        echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                        echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                    ?>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm">                            
                        <?php echo $this->Form->input('search', array('label' => __('Search: '), 'div' => false, 'value' => urldecode($search), 'class' => 'form-control', 'style' => 'width:100%;')); ?>
                    </div>
                    
                    <div class="form-group col-md-3 col-sm-3 col-xs-12 pr-none mb-sm">
                        <?php echo $this->Form->input('SchoolInformation.level', array('type' => 'select', 'label' => 'Level: ', 'div' => false, 'data-placeholder' => '- Select Level -', 'selected' => $selectedLevel, 'class' => 'form-control chosen', 'style' => 'width:100%;', 'options' => $schoolLevelOptions, 'multiple' => true)); ?>
                    </div>
                    <div class="form-group col-md-3 col-sm-3 col-xs-12 pr-none mb-sm">
                        <?php echo $this->Form->input('SchoolInformation.type', array('type' => 'select', 'label' => 'Type: ', 'div' => false, 'data-placeholder' => '- Select Type -', 'selected' => $selectedType, 'class' => 'form-control chosen', 'style' => 'width:100%;', 'options' => $schoolTypeOptions, 'multiple' => true)); ?>
                    </div>
                    <div class="form-group col-md-2 col-sm-2 col-xs-12 mb-sm">
                        <?php
                            echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'class' => 'chosen form-control', 'style' => 'width:100%;', 'label' => __('Show: '), 'div' => false));
                        ?>
                    </div>                    
                    <?php echo $this->Form->end(); ?>
                        
                    
                </div>
                <div class="table-responsive">                
                <table class="table table-striped table-bordered bootstrap-datatable dataTable" id="schoolListData">
                    <thead>
                        <tr>
                            <th class="table-heading <?php echo ($order == 'name')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="name" data-order-type="<?php echo ($order == 'name')?strtolower($orderType):'asc'; ?>">School name</th>
<!--                            <th class="table-heading <?php //echo ($order == 'manager_name')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="manager_name" data-order-type="<?php //echo ($order == 'manager_name')?strtolower($orderType):'asc'; ?>">Manager name</th>-->
                            <th class="table-heading <?php echo ($order == 'email')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="email" data-order-type="<?php echo ($order == 'email')?strtolower($orderType):'asc'; ?>">Email</th>
                            
                            <th class="table-heading <?php echo ($order == 'level')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="level" data-order-type="<?php echo ($order == 'level')?strtolower($orderType):'asc'; ?>">Level</th>
                            <th class="table-heading <?php echo ($order == 'type')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="type" data-order-type="<?php echo ($order == 'type')?strtolower($orderType):'asc'; ?>">Type</th>
                            
                            <th class="table-heading <?php echo ($order == 'created')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="created" data-order-type="<?php echo ($order == 'created')?strtolower($orderType):'asc'; ?>">Date registered</th>
                            <th class="table-heading <?php echo ($order == 'payment_status')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="payment_status" data-order-type="<?php echo ($order == 'payment_status')?strtolower($orderType):'asc'; ?>">Payment Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($schools)){
                            $schoolCount = 1;
                            $rowClass = 'even';
                            foreach ($schools as $key => $school){
                                $rowClass = (($schoolCount % 2) != 0)?'odd':'even';
                        ?>
                        <tr>
                            <td>
                                <div class="checkbox" style="margin-top:0;"> <label for="schoolName<?php echo $school['School']['id']; ?>"> <input class="select-school" type="checkbox" id="schoolName<?php echo $school['School']['id']; ?>" value="<?php echo $school['School']['id']; ?>"> <?php echo $school['School']['name']; ?> </label> </div>
                            </td>
<!--                            <td><?php echo $school['School']['manager_name']; ?></td>-->
                            <td><?php echo $school['School']['email']; ?></td>
                            <td><?php echo $school['SchoolInformation']['level']; ?></td>
                            <td><?php echo $school['SchoolInformation']['type']; ?></td>
                            <td class="center"><?php echo date("jS F, Y", strtotime($school['School']['created'])); ?></td>

                            <td class="center">

                                <?php if ($school['School']['payment_status'] == 0) { ?>
                                    <span class="label-warning label label-default">
                                        Pending
                                    </span>
                                <?php } ?>
                                <?php if ($school['School']['payment_status'] == 1) { ?>
                                    <span class="label-success label label-default">
                                        Active</span>
                                <?php }  ?>
                                <?php if ($school['School']['isdeleted'] == 1) { ?>
                                    <span class="label-default label label-danger">
                                        Inactive</span>
                                <?php } ?>

                            </td>
                            <td class="center">
                                <a class="btn-success view-school btn-sm" title="View" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#schoolViewModal" data-school="<?php echo $school['School']['id']; ?>">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span> 
                                </a>
                                <a class="btn-info btn-sm" title="Edit" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#newSchoolModal" data-school="<?php echo $school['School']['id']; ?>" data-schoolInfo="<?php echo $school['SchoolInformation']['id']; ?>">
                                    <span class="glyphicon glyphicon-edit icon-white"></span>
                                </a>
                                <a class="btn-danger btn-sm" title="Delete" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#deleteSchoolModal" data-school-name="<?php echo $school['School']['name']; ?>" data-school="<?php echo $school['School']['id']; ?>">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                <a class="btn-warning btn-sm" title="Activate Account" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-modal-type="login-credentials" data-toggle="modal" data-target="#loginCredentialsModal" data-school-name="<?php echo $school['School']['name']; ?>" data-school="<?php echo $school['School']['id']; ?>">
                                    <span class="glyphicon glyphicon-share-alt icon-white"></span>&nbsp;<span class="glyphicon glyphicon-envelope icon-white"></span>
                                </a>
                            </td>
                        </tr>
                        <?php
                                $schoolCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No record found</td></tr>
                        <?php } ?>
                    </tbody>
                </table>
                    
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->    
                    
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- Start Add and edit School Modal-->
<?php echo $this->element('school/modal/add-edit', array('schoolLevelOptions' => $schoolLevelOptions, 'schoolTypeOptions' => $schoolTypeOptions)); ?>
<!-- End Add and edit School Modal-->

<!-- Start School View Modal-->
<div class="modal fade" id="schoolViewModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School View Modal -->

<!-- Start School delete Modal-->
<div class="modal fade" id="deleteSchoolModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete school</h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary schoolDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School delete Modal -->

<!-- Start School Login Credentials Modal-->
<div class="modal fade" id="loginCredentialsModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary schoolCredentialsConfirm" data-isChecked="false">Yes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School Login Credentials Modal -->

<!-- Start school View Template -->
<?php echo $this->element('school/template/view'); ?>
<!-- End school View Template -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/school-management.js"></script>
<script type="text/javascript">
    $(function () {
        
        if($('.chosen').length){
            $('.chosen').chosen();
        }
        
        $('select#SchoolLimit, select#SchoolInformationLevel, select#SchoolInformationType, input#SchoolSearch, input#SchoolOrder', '.filters').bind("change", changePaginator); // orange
        
        $('table.dataTable thead .table-heading').click(function(){
            var parentScop = this;
            var orderName = $(this).attr('data-order');
            var orderType = ($(this).attr('data-order-type') == 'asc')?'desc':'asc';
            
            $('#SchoolOrder').val(orderName);
            $('#SchoolOrderType').val(orderType);
            
            $('table.dataTable thead .table-heading').each(function(index, element){
                $(element).removeClass('sorting_'+$(parentScop).attr('data-order-type')).addClass('sorting');
            });
            $(this).removeClass('sorting').addClass('sorting_'+orderType);
            $(this).attr('data-order-type', orderType);
            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#SchoolSearch', '.filters').val('');
        $('select#SchoolLimit', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var communitySlug = '<?php echo Configure::read('Community.slug') ?>/';
        if (communitySlug == '/') communitySlug = '';

        var newurl = base+'school_managements/index';
        
//        if (passed.page !== undefined)
//            newurl += '/page:'+passed.page;
        var searchData = $('input#SchoolSearch', '.filters').val();
//        if (passed.search !== undefined){
//            newurl += '/search:'+passed.search;
//        }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }
        
        var level = $('select#SchoolInformationLevel', '.filters').val();
        if (level)
            newurl += '/level:'+$('select#SchoolInformationLevel', '.filters').val();
        
        if ($('select#SchoolInformationType', '.filters').val() != null && $('select#SchoolInformationType', '.filters').val() != '')
            newurl += '/type:'+$('select#SchoolInformationType', '.filters').val();

        if ($('select#SchoolLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#SchoolLimit', '.filters').val();
        if ($('input#SchoolOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#SchoolOrder', '.filters').val();
        if ($('input#SchoolOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#SchoolOrderType', '.filters').val();
        //console.log(newurl);
        window.location = newurl;
    }
</script>