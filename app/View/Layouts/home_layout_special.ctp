<!DOCTYPE html>
<html lang="en">
  <head>
  <!-- below function for prevent back page -->
<script type = "text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>

  
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN  = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */  
        </script>
	
   <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/bootstrap-select.css">
<link href="<?php echo $this->webroot;?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
<link href="<?php echo $this->webroot;?>css/responsive.css" rel="stylesheet">
  </head>
  <!-- below in body for prevent right click,copy,paste,cut, -->
 <body oncontextmenu="return false" oncopy="return false" oncut="return false" onpaste="return false">
   <div class="wrapper">
    <?php echo $this->element("header"); ?>
	<div class="container">
	</div>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element("footer"); ?>

</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<!---------------------------Custom Jquery--------------------------------------> 
<script src="<?php echo $this->webroot;?>js/front-end/custom.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo $this->webroot;?>js/front-end/bootstrap.min.js"></script>
<script src="<?php echo $this->webroot;?>js/front-end/bootstrap-select.js"></script>
<script>
document.onkeydown = function(){
	//stop page refresh working in chrome
  switch (event.keyCode){
        case 116 : //F5 button
            event.returnValue = false;
            event.keyCode = 0;
            return false;
        case 82 : //R button
            if (event.ctrlKey){ 
                event.returnValue = false;
                event.keyCode = 0;
                return false;
            }
    }
}
</script>

</body>
</html>