<!doctype html>
<html class="easy-sidebar-active">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
 <title>leanerleader.com</title>	
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/modality.css">
<link href="<?php echo $this->webroot;?>css/front-end/easy-sidebar.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-style.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-responsive.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/LIB/jquery-2.0.3.js"></script>
<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
</head>

<body>
<?php //echo $this->element("header_take_exam"); ?>

<?php echo $this->fetch('content'); ?> 
</body>
</html>
