<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

       <meta charset="utf-8">
	<title><?php echo $title_for_layout; ?></title>
	<meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

        <?php //echo $this->Html->meta('icon'); ?>
	<?php //echo $this->fetch('meta'); ?>
        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo Router::url('/', true); ?>img/favicon.ico" type="image/x-icon">
	<link  href="<?php echo Router::url('/', true); ?>css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo $this->webroot; ?>css/charisma-app.css" rel="stylesheet">
    <link href='<?php echo $this->webroot; ?>bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo $this->webroot; ?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo $this->webroot; ?>css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="<?php echo $this->webroot; ?>js/jquery.validate.min.js"></script>
<!--    <style type="text/css">
        #flashMessage{
            color:red;
            font-weight: bold;
        }
          
    </style>-->
</head>
   
    <body>
	<?php echo $this->fetch('content'); ?>
    <?php echo $this->element("Login/footer"); ?>
</body>
</html>


