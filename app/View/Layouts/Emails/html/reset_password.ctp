<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title><?php echo $title_for_layout; ?></title>
    </head>
    <body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="font-family: Helvetica, Arial, sans-serif;text-align:center;color:#3E3E3E;">
        <style>a.img { border:0px; }</style>
        <table width="800" border="0" cellpadding="0" cellspacing="0" align="center" style="border: 0px solid #ffffff;">

            <tr>
                <td style="padding: 0 100px 0 60px; font-size: 16px; text-align: left;">
                    <?php echo $content_for_layout; ?>
                </td>
            </tr>

        </table>
    </body>
</html>