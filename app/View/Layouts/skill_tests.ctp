<!doctype html>
<html class="easy-sidebar-active">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width">
        <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */
        </script>
        <link rel="stylesheet" href="<?php echo Router::url('/', true); ?>css/front-end/bootstrap.css">
        <link rel="stylesheet" href="<?php echo Router::url('/', true); ?>css/front-end/modality.css">
        <link href="<?php echo Router::url('/', true); ?>css/front-end/easy-sidebar.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Router::url('/', true); ?>css/front-end/dashboard-style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo Router::url('/', true); ?>css/front-end/dashboard-responsive.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
        <!--[if lt IE 9]>
            <link rel="stylesheet" href="legacy-easy-sidebar.css">
        <![endif]-->
        
        <!-- jQuery -->
        <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo Router::url('/', true); ?>img/favicon.ico" type="image/x-icon">
        <style type="text/css">
            .ll-hide-all{display: none;}
        </style>
    </head>

    <body class="toggled">
        
        <!-- Test page Header -->
        <?php echo $this->element('skill_tests/header'); ?>
        
        <!-- Question Sequence Left Side bar -->
        <?php echo $this->element('skill_tests/question-sequence', array('questions' => $questions)); ?>
        
        <div class="container-fluid">

            <div class="col-lg-9 col-md-7 col-sm-12">
                <div class="content"> 
                    <div class="content_main">
                        
                        <!-- Main content area start -->
                        <?php echo $this->fetch('content'); ?>
                        <!-- Main content area End -->
                        
                    </div>
                    <div class="clearfix"></div>
                    <!-- message content end--> 
                </div>
            </div>
        </div>
        <div class="right_bar">
            <!-- Right Side bar Timer -->
            <?php echo $this->element('skill_tests/timer'); ?>

            <!-- Right Side bar Question Completed -->
            <?php echo $this->element('skill_tests/question-completed'); ?>
        </div>

        <!-- Footer -->
        <?php echo $this->element('skill_tests/footer'); ?>
        
        <!-- exam-completed-modal-->
        <?php echo $this->element('skill_tests/exam-completed-modal'); ?> 
        
    </body>
</html>
