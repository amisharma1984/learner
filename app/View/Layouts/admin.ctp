<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no"/>
        <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN  = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */  
        </script>

        <!-- The styles -->
        <link href="<?php echo Router::url('/', true); ?>css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo Router::url('/', true); ?>css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/fullcalendar/dist/fullcalendar.print.css" rel="stylesheet" media="print">
        <link href="<?php echo Router::url('/', true); ?>bower_components/chosen/chosen.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/jquery.noty.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/noty_theme_default.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/elfinder.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/elfinder.theme.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/jquery.iphone.toggle.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/uploadify.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/animate.min.css" rel="stylesheet">

        <!-- jQuery -->
        <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo Router::url('/', true); ?>img/favicon.ico" type="image/x-icon">

    </head>

    <body>
        <!-- top navigation bar starts -->
        <?php echo $this->element('admin_menus/top-nav-bar'); ?>
        <!-- top navigation bar ends -->
        
        <div class="ch-container">
            <div class="row">

                <!-- left side-bar menu starts -->
                <div class="col-sm-2 col-lg-2">
                    <?php echo $this->element('admin_menus/left-side-bar'); ?>
                </div>
                <!-- left side-bar menu ends -->

                <noscript>
                <div class="alert alert-block col-md-12">
                    <h4 class="alert-heading">Warning!</h4>

                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                        enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="col-lg-10 col-sm-10">
                    <!-- content starts -->
                    <?php echo $this->fetch('content'); ?>
                    <!-- content ends -->
                </div><!--/#content.col-md-0-->
            </div><!--/fluid-row-->

            <hr>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">

                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h3>Settings</h3>
                        </div>
                        <div class="modal-body">
                            <p>Here settings can be configured...</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                            <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer Start -->
            <?php echo $this->element('admin-footer'); ?>
            <!-- Footer End -->

        </div><!--/.fluid-container-->
        
        <!-- Start Admin Login Status Modal-->
        <div class="modal fade" tabindex="-1" id="adminStatusModal" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Account Status</h4>
                    </div>
                    <div class="modal-body">
                        <div id="flashMessage" class="alert alert-info">
                            <strong><span class="glyphicon glyphicon-info-sign"></span></strong> Your session has expired. Please log in again.
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="redirectToAdminLogin();">Ok</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End Admin Login Status Modal-->

    </body>
</html>
