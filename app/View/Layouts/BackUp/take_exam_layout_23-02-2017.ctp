<!doctype html>
<html class="easy-sidebar-active">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Question</title>
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/modality.css">
<link href="<?php echo $this->webroot;?>css/front-end/easy-sidebar.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-style.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-responsive.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/LIB/jquery-2.0.3.js"></script>
<!--[if lt IE 9]>
    <link rel="stylesheet" href="legacy-easy-sidebar.css">
<![endif]-->
</head>

<body class="toggled">
<?php echo $this->element("header_take_exam"); ?>
<?php 
		$action = $this->params['action'];
		if($action == 'take_exam'){
			//echo $this->element("left_panel_take_exam"); 
		} else {
			echo $this->element("student_left_panel"); 
		}


?>
<?php echo $this->fetch('content'); ?>
<?php echo $this->element("right_panel"); ?>


<div class="dashboard-footer clearfix">
<div class="footer_main">
<div class="footer_left">

    <!--<a href="#yourModalId" aria-label="open">Previous</a>
    
    <a href="#" class="save_next">Save & next</a>-->
</div>
<div class="footer_right">
    <!--<a href="#">Submit test</a>-->
</div>
</div>
</div>
 
<div id="yourModalId" class="yourModalClass" role="dialog" aria-labelledby="yourModalHeading" style="display:none;">

        <div class="modal_content">
             <h2>Submit the test</h2>
        <p>Once closed, you can no longer view or modify this test. Are you sure you are done, and want to close 
the test?</p>
       <div class="modal_button">
           <a href="#">Yes, submit the test</a>
           <a href="#" class="donot">No, do not submit</a>
       </div>
        </div>
       

        <!-- This closes your modal -->
        <div class="modal_close_b"><a href="#yourModalId" aria-label="close"><img src="images/cross_modal.png" alt=""/></a></div>

    </div>
	<!-- below jquery file comment by Dinesh because of count down timer was not working-->
<!--<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script> -->
<script>
	$(document).ready(function(){
		$('.shtm').click(function(){
			$('.topbarmenu').slideToggle();
			});
		
		});
	$(window).resize(function(){
          if ( $(window).width() < 768 ) {
        $(".topbarmenu").hide();
          }
          else {
              $(".topbarmenu").show();
          }
 });	
</script>

<!--<script src="<?php echo $this->webroot;?>js/bootstrap.js"></script> 
<script src="<?php echo $this->webroot;?>js/jquery.detect_swipe.js"></script>
<script src="<?php echo $this->webroot;?>js/modality.js"></script> -->

<script>
$('.easy-sidebar-toggle').click(function(e) {
    e.preventDefault();
    $('body').toggleClass('toggled');
    $('.navbar.easy-sidebar').removeClass('toggled');
});
$('html').on('swiperight', function(){
    $('body').addClass('toggled');
});
$('html').on('swipeleft', function(){
    $('body').removeClass('toggled');
});
</script>

</body>
</html>
