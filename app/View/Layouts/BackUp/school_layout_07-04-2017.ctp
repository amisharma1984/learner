<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN  = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */  
        </script>
	
   <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/bootstrap-select.css">
<link href="<?php echo $this->webroot;?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
<link href="<?php echo $this->webroot;?>css/responsive.css" rel="stylesheet">
  </head>
  <body>
   <div class="wrapper">
    <?php echo $this->element("school_header"); ?>
	<div class="container">
	</div>
	<?php echo $this->fetch('content'); ?>
	<?php echo $this->element("footer"); ?>

</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<!---------------------------Custom Jquery--------------------------------------> 
<script src="<?php echo $this->webroot;?>js/front-end/custom.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo $this->webroot;?>js/front-end/bootstrap.min.js"></script>
<script src="<?php echo $this->webroot;?>js/front-end/bootstrap-select.js"></script>
</body>
</html>