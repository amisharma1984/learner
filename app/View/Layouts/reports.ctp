<!doctype html>
<html class="easy-sidebar-active">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
 <title><?php //echo $title_for_layout; ?>leanerleader.com</title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN  = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */  
        </script>
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/modality.css">
<link href="<?php echo $this->webroot;?>css/front-end/easy-sidebar.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-style.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-responsive.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/LIB/jquery-2.0.3.js"></script>


<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
<!--[if lt IE 9]>
    <link rel="stylesheet" href="legacy-easy-sidebar.css">
<![endif]-->
</head>
<body >
<?php echo $this->fetch('content'); ?> 
</body>
</html>
