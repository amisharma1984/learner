<!doctype html>
<html class="easy-sidebar-active">
<head>
<!-- below function for prevent back page -->
<script type = "text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>


<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
 <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
            var BASEURL = '<?php echo Router::url('/', true); ?>';
            var APP_CN = '<?php echo $this->request->params['controller']; ?>'; /* pageControllerName */
            var APP_AN  = '<?php echo $this->request->params['action']; ?>'; /* pageActionName */  
        </script>
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->webroot;?>css/front-end/modality.css">
<link href="<?php echo $this->webroot;?>css/front-end/easy-sidebar.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-style.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->webroot;?>css/front-end/dashboard-responsive.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> 
<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/LIB/jquery-2.0.3.js"></script>


<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
<!--[if lt IE 9]>
    <link rel="stylesheet" href="legacy-easy-sidebar.css">
<![endif]-->
</head>

<!-- below in body for prevent right click,copy,paste,cut, -->
<body class="toggled" oncontextmenu="return false" oncopy="return false" oncut="return false" onpaste="return false">
<?php echo $this->element("header_take_exam"); ?>
<?php 
		$action = $this->params['action'];
		if($action == 'take_exam' || $action == 'view_individual_report'){
			//echo $this->element("left_panel_take_exam"); 
		} else {
			echo $this->element("student_left_panel"); 
		}


?>
<?php echo $this->fetch('content'); ?> 
<?php 
     if($action != 'view_individual_report'){
		 if($action == 'take_exam' || $action == 'take_exam_second_section' || $action == 'sample_questions' || $action == 'take_literacy_exam'){
				echo $this->element("right_panel"); 
			} else {
				echo $this->element("view_answer_right_panel"); 
			}
	 }
?>

<?php if($action != 'result'){?>
<div class="dashboard-footer clearfix">
<div class="footer_main">
<div class="footer_left">

    <!--<a href="#yourModalId" aria-label="open">Previous</a>
    
    <a href="#" class="save_next">Save & next</a>-->
</div>
<div class="footer_right">
    <!--<a href="#">Submit test</a>-->
</div>
</div>
</div>
 <?php }?>
<div id="yourModalId" class="yourModalClass" role="dialog" aria-labelledby="yourModalHeading" style="display:none;">

        <div class="modal_content">
             <h2>Submit the test</h2>
        <p>Once closed, you can no longer view or modify this test. Are you sure you are done, and want to close 
the test?</p>
       <div class="modal_button">
           <a href="#">Yes, submit the test</a>
           <a href="#" class="donot">No, do not submit</a>
       </div>
        </div>
       

        <!-- This closes your modal -->
        <div class="modal_close_b"><a href="#yourModalId" aria-label="close"><img src="images/cross_modal.png" alt=""/></a></div>

    </div>
	<!-- below jquery file comment by Dinesh because of count down timer was not working-->
<!--<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script> -->
<script>
	$(document).ready(function(){
		$('.shtm').click(function(){
			$('.topbarmenu').slideToggle();
			});
		
		});
	$(window).resize(function(){
          if ( $(window).width() < 768 ) {
        $(".topbarmenu").hide();
          }
          else {
              $(".topbarmenu").show();
          }
 });	
</script>

<!--<script src="<?php echo $this->webroot;?>js/bootstrap.js"></script> 
<script src="<?php echo $this->webroot;?>js/jquery.detect_swipe.js"></script>
<script src="<?php echo $this->webroot;?>js/modality.js"></script> -->

<script>
$('.easy-sidebar-toggle').click(function(e) {
    e.preventDefault();
    $('body').toggleClass('toggled');
    $('.navbar.easy-sidebar').removeClass('toggled');
});
$('html').on('swiperight', function(){
    $('body').addClass('toggled');
});
$('html').on('swipeleft', function(){
    $('body').removeClass('toggled');
});
</script>


<script>
document.onkeydown = function(){
	//stop page refresh working in chrome
  switch (event.keyCode){
        case 116 : //F5 button
            event.returnValue = false;
            event.keyCode = 0;
            return false;
        case 82 : //R button
            if (event.ctrlKey){ 
                event.returnValue = false;
                event.keyCode = 0;
                return false;
            }
    }
}
</script>
</body>
</html>
