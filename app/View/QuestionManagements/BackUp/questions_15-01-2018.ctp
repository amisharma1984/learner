<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Examination Management'), "link" => array('controller' => 'examination_managements', 'action' => 'index')),
    array("text" => __('Questions & Answers'), "link" => array('controller' => 'question_managements', 'action' => 'questions'))
        ), true);
?>
<?php echo $this->Session->flash(); ?>
<?php
$examTypeId = (!empty($this->request->data['Question']['examination_type_id']))?$this->request->data['Question']['examination_type_id']:'';
$examCategoryId = (!empty($this->request->data['Question']['examination_category_id']))?$this->request->data['Question']['examination_category_id']:'';
?>
<div class="row">
    <div class="box col-md-12 col-sm-12 col-xs-12" id="questionSectionDivId">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><img style="height:22px; width: 22px;" src="<?php echo Router::url('/', true); ?>img/q_and_a.png" alt="Question & Answers"> 
                    Questions
                </h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <div class="row" id="questionAddEditMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                <?php //pr($this->request->data); ?>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'question_managements', 'action' => 'create_question'),
                    'id' => 'questionAddEditForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                echo $this->Form->input('Question.examination_id', array('type' => 'hidden'));
                ?>
                <div class="form-group mb-xs" >
                    <label for="questionFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionExaminationTitle" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Examination</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.examination_title', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Search exam\'s', 'required' => true)); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionSubjectId" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Subject</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                    //$examTypeOptions = $this->ExamType->getExamTypeList(Configure::read('App.examinationTypeList'));
                                ?>
                                <select name="data[Question][subject_id]" class="form-control" style="width:100%" id="QuestionSubjectId" required="required" aria-required="true">
                                    <option value="">- Select Subject -</option>
                                    <?php foreach ($subjects as $key => $data){ ?>
                                    <option value="<?php echo $key; ?>" ><?php echo $data; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Exam Type</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                    $examTypeOptions = $this->ExamType->getExamTypeList(Configure::read('App.examinationTypeList'));
                                ?>

                                <select name="data[Question][examination_type_id]" data-action-name="Type" class="form-control examTypeCategory" style="width:100%" id="QuestionExaminationTypeId" <?php echo (!empty($examTypeId))?'disabled="disabled"':'required="required" aria-required="true"'; ?> >
                                    <option value="">- Select Type -</option>
                                    <?php foreach ($examTypeOptions as $key => $data){ ?>
                                    <option value="<?php echo $key; ?>" <?php echo (!empty($examTypeId) && ($examTypeId == $key))?'selected':''; ?> ><?php echo $data; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Exam Category</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php //echo $this->Form->input('Setting.exam_section_' . $i . '_questions', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control check-integer', 'autocomplete' => 'off', 'required' => true)); ?>
                                <?php
                                    $categoryOptions = $this->Category->getCategoryList(Configure::read('App.examinationCategoryList'));
                                    //echo $this->Form->input('Question.examination_category_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Category -', 'class' => 'form-control', 'required' => true, 'options' => $categoryOptions));
                                ?>

                                <select name="data[Question][examination_category_id]" data-action-name="Category" class="form-control examTypeCategory" style="width:100%" id="QuestionExaminationCategoryId" <?php echo (!empty($examCategoryId))?'disabled="disabled"':'required="required" aria-required="true"'; ?> >
                                    <option value="">- Select Category -</option>
                                    <?php foreach ($categoryOptions as $key => $data){ ?>
                                    <option value="<?php echo $key; ?>" <?php echo (!empty($examCategoryId) && ($examCategoryId == $key))?'selected':''; ?> ><?php echo $data; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                            $sections = $this->KeyValue->getValue('examination_sections', $settingData);
                            $sectionOptions = array();
                            if(!empty($sections)){
                                for($i = 1; $i <= $sections; $i++){
                                    $value = $this->KeyValue->getValue('exam_section_'.$i.'_name', $settingData);
                                    if(!empty($value)){
                                        $sectionOptions['exam_section_'.$i.'_name'] = $value;
                                    }
                                }
                            }
                            ?>
                            <label for="QuestionSection" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Question section</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.section', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Section -', 'class' => 'form-control', 'required' => false, 'options' => $sectionOptions)); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionAnswerType" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Answer type</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php 
                                $ansTypeOptions = array(
                                    'Multiple choice' => array(
                                        '2' => 'Multiple correct',
                                        '3' => 'One correct'
                                    ),
                                    '4' => 'Short descriptive'
                                );//Question.answer_type
                                echo $this->Form->input('Question.answer_type', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Answer Type -', 'class' => 'form-control', 'required' => true, 'options' => $ansTypeOptions));
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="QuestionTitle" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Question</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.title', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off', 'required' => true)); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionCorrectMarks" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Correct marks</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.correct_marks', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control checkInteger', 'autocomplete' => 'off','value' => 2, 'min' => 1, 'step' => 1, 'required' => true)); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionWrongMarks" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Wrong marks</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.wrong_marks', array('type' => 'number', 'label' => false, 'div' => false,'value' => 0, 'class' => 'form-control checkInteger', 'autocomplete' => 'off', 'max' => 0, 'step' => 1, 'required' => true)); ?>
                            </div>
                        </div>
						
						   <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionCorrectMarks" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;"></sup>Sub Strands Code</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.sub_strands_code', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => false)); ?>
                            </div>
                        </div>
						
						   <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="QuestionCorrectMarks" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;"></sup>Sub Strands Description</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.sub_strands_description', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => false)); ?>
                            </div>
                        </div>
						
						
						 <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="QuestionReadingBookContent" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;"></sup>Reading Book Content</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Question.reading_book_content', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off', 'required' => false)); ?>
                            </div>
                        </div>
						
						
					
						
						

                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>     
                
            </div>
        </div>
    </div>
    
    <div class="box col-md-12 col-sm-12 col-xs-12 mt-sm" id="answerSectionDivId">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><img style="height:22px; width: 22px;" src="<?php echo Router::url('/', true); ?>img/q_and_a.png" alt="Question & Answers"> 
                    Answers
                </h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <div class="row mb-md" id="questioAndAnswerArea" style="display:none;">
                    <!-- Question preview goes here.  -->
                </div>
                
                <div class="row" id="answerAddEditMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                
                <?php //pr($this->request->data); ?>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'question_managements', 'action' => 'create_answer'),
                    'id' => 'answerAddEditForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                echo $this->Form->input('Answer.question_id', array('type' => 'hidden'));
                ?>
                <div class="form-group mb-xs" id="answerEditSectionDivId">
                    <label for="answerFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <div class="row">
                                                
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="QuestionTitle" class="col-md-12 col-sm-12 col-xs-12 control-label text-left mb-sm"><sup style="color:red;">*</sup>Answer</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Answer.answer_text', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off', 'required' => true)); ?>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="checkbox" style="margin-top:0;"> 
                                    <label for="AnswerIscorrect"> 
                                        <?php echo $this->Form->checkbox('Answer.iscorrect', array('hiddenField' => false)); ?>Is it Correct answer?
                                    </label> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
				
				 <div class="form-group">
                    <div class="row">
                                                
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="AnswerAnsDescription" class="col-md-12 col-sm-12 col-xs-12 control-label text-left mb-sm"><sup style="color:red;">*</sup>Answer Description</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Answer.ans_description', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off')); ?>
                                <?php // echo $this->Form->input('Answer.ans_description', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
				
                
                
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>     
                
            </div>
        </div>
    </div>
</div><!--/row-->

<!-- Start Question Answer delete Modal-->
<div class="modal fade" id="deleteQuestionAnswerModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete school</h4>
            </div>
            <div class="modal-body">
                <p></p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary questionAnswerDeleteConfirm" >Ok</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Question Answer delete Modal -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/tinymce/tinymce.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/bootstrap-ajax-typeahead/bootstrap-typeahead.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/question-management.js"></script>
<script type="text/javascript">
    
</script>