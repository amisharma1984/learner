<?php
    /*pr($questionDetails);
    pr($previous);
    pr($next);*/
    $ansTypeOptions = array(
            '2' => 'Multiple correct',
            '3' => 'One correct',
            '4' => 'Short descriptive'
        );
?>
<?php if(!empty($previous) || !empty($next)){ ?>
<div class="col-md-12 col-xs-12 col-sm-12">
    <?php if (!empty($previous)) echo $this->Html->link(__('&laquo; Previous'), array('controller' => 'question_managements', 'action' => 'preview_question', $previous['Question']['id']), array('class' => 'btn btn-sm btn-primary preNextButton', 'style' => 'float:left;', 'escape' => false, 'data-exam' => $previous['Question']['examination_id'], 'data-question' => $previous['Question']['id'])); ?>
    <?php if (!empty($next))  echo $this->Html->link(__('Next &raquo;'), array('controller' => 'question_managements', 'action' => 'preview_question', $next['Question']['id']), array('class' => 'btn btn-sm btn-primary preNextButton', 'style' => 'float:right;', 'escape' => false, 'data-exam' => $next['Question']['examination_id'], 'data-question' => $next['Question']['id'])); ?>
</div>
<?php } ?>

<?php if(!empty($questionDetails['Question'])){ ?>
<div class="col-md-12 col-xs-12 col-sm-12">
    <input type="hidden" id="viewingQuestionId" value="<?php echo $questionDetails['Question']['id']; ?>">
    <div class="question" >
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-10 dsp-in-block" >
                <?php echo 'Q'.$questionDetails['Question']['question_order'].'.'.'&nbsp;'.nl2br($questionDetails['Question']['title']); ?>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 text-right" >
                <?php if(!empty($admindata['Admin'])){ ?>
                <a class="btn-info btn-sm editQuestionButton" title="Edit" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-question="<?php echo $questionDetails['Question']['id']; ?>">
                    <span class="glyphicon glyphicon-edit icon-white"></span>
                </a>
                <a class="btn-danger btn-sm deleteQuestionButton" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#deleteQuestionAnswerModal" data-action="question" data-question="<?php echo $questionDetails['Question']['id']; ?>" data-previous="<?php echo (!empty($previous['Question']['id']))?$previous['Question']['id']:''; ?>" data-next="<?php echo (!empty($next['Question']['id']))?$next['Question']['id']:''; ?>">
                    <span class="glyphicon glyphicon-trash icon-white"></span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if(!empty($questionDetails['Answer'])){ ?>
<div class="col-md-12 col-xs-12 col-sm-12"> 
    <div class="answers">
        <?php foreach ($questionDetails['Answer'] as $key => $data){ ?>
        <div class="row">
            <div class="col-md-9 col-sm-9 col-xs-8 dsp-in-block" ><?php echo chr($data['answer_order']).'.&nbsp;'.$data['answer_text']; ?></div>
            <div class="col-md-3 col-sm-3 col-xs-4 text-right">
                <?php if(!empty($admindata['Admin'])){ ?>
                <?php if(!empty($data['iscorrect'])){ ?>
                <a class="btn-success btn-sm" title="Correct" style="margin:0 0px 5px 0; text-decoration: none; display: inline-block;" >
                    <span class="glyphicon glyphicon-ok icon-white"></span>
                </a>
                <?php } ?>
                
                <a class="btn-info btn-sm editAnswerButton" title="Edit" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-answer="<?php echo $data['id']; ?>" data-question="<?php echo $data['question_id']; ?>">
                    <span class="glyphicon glyphicon-edit icon-white"></span>
                </a>
                <a class="btn-danger btn-sm deleteAnswerButton" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#deleteQuestionAnswerModal" data-action="answer" data-answer="<?php echo $data['id']; ?>" data-question="<?php echo $data['question_id']; ?>">
                    <span class="glyphicon glyphicon-trash icon-white"></span>
                </a>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php } ?>