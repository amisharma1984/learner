<?php
echo $this->Breadcrumb->show(array(), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-home"></i> Dashboard</h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <h2 class="text-center">Welcome to <?php echo Configure::read('App.appName'); ?> Admin Panel.</h2>
                
            </div>
        </div>
    </div>
</div>
<!--/span-->

<?php echo $this->element('JS/external-scripts'); ?>
