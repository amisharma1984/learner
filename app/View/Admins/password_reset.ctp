<div class="ch-container">
    
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Change password for <?php echo $userData['Admin']['email']; ?></h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-default">
                Enter your new password to reset.
            </div>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'password_reset', $resetKey), true), 'id' => 'AdminResetPasswordForm')); ?>
            <fieldset>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                    <?php echo $this->Form->input('Admin.password', array('label' => false, 'type' => 'password', 'div' => false, 'required' => true, 'tabindex' => 2, 'placeholder' => __('Password'), 'class' => array('form-control'))); ?>
                </div>
                <div class="clearfix"></div><br>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                    <?php echo $this->Form->input('Admin.repeatpassowrd', array('label' => false, 'type' => 'password', 'div' => false, 'required' => true, 'tabindex' => 2, 'placeholder' => __('Confirm password'), 'class' => array('form-control'))); ?>
                </div>
                <div class="clearfix"></div><br>

                <div class="center col-md-8 col-sm-8 col-xs-12">
                    <button type="submit" class="btn btn-primary">Change password</button>
                </div>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->
<script type="text/javascript">
    $("#AdminResetPasswordForm").validate({
        rules: {
            "data[Admin][password]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            "data[Admin][repeatpassowrd]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            }
        }
    });
</script>