<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Profile'), "link" => array('controller' => 'admins', 'action' => 'profile', $admindata['Admin']['id']))
        ), true);
?>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Edit Test User</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'edit_test_user',$id), true), 'class' => 'form-horizontal', 'role' => 'form')); ?>
                <?php echo $this->Form->input('Student.id', array('type' => 'hidden', 'label' => false, 'div' => false)); ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="posterFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Student.first_name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
						<label class="error" for="StudentFirstName"><?php echo @$fnerror;?></label>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Last name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php //echo $this->Form->input('Student.last_name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
						<label class="error"><?php //echo @$lnerror;?></label>
                    </div>
                </div> -->
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Email</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Student.email', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off','readonly' => 'readonly')); ?>
						<label class="error"><?php //echo @$emerror;?></label>
                    </div>
                </div>
              
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Student.n_password', array('type' => 'password', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
						<label class="error"><?php echo @$pwderror;?></label>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Confirm Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php //echo $this->Form->input('Student.confirm_password', array('type' => 'password', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
						<label class="error"><?php echo @$cpwderror;?></label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Address</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php //echo $this->Form->input('Student.address', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;"></sup>School Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php //echo $this->Form->input('Student.school_name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div> -->
				
				
            
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input class="btn btn-primary" value="Update" type="submit">
                        <a class="btn btn-default" href="<?php echo $this->Html->url(array('controller' => 'admins', 'action' => 'test_user_list')); ?>">Cancel</a>
                    </div>
                </div>

               
            <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</div>
<!--/span-->

<?php echo $this->element('JS/external-scripts'); ?>
<script type="text/javascript">
    $("#AdminEditTestUserForm").validate({
        rules: {
            "data[Student][first_name]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            "data[Student][last_name]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            "data[Student][email]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
            },
			 /*"data[Student][contact_email]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
            },
			 "data[Student][global_email]": {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email: true
            }*/
        },
        messages: {
            "data[Student][email]": {
                email: "Please enter a valid email address.",
            }
        },
    });
</script>

