
<div class="ch-container">
            
    <div class="row">
        <div class="col-md-12 center login-header" style="height: auto;">
<!--            <h2>Welcome to <?php //echo Configure::read('App.appName'); ?></h2>-->
            <?php echo $this->Html->image('leader-learner-online-logo-v1.jpg', array('alt' => Configure::read('App.appName'), 'class' => 'img-responsive', 'style' => 'height:130px; margin:0 auto 10px;')); ?>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-default">
                Please login with your Email and Password.
            </div>
            <?php echo $this->Session->flash(); ?>
           <?php echo $this->Form->create(null, array('url'=> $this->Html->url(array('controller' => 'admins', 'action'=>'login'), true)));?>
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                      <?php echo $this->Form->input('login', array('label'=> false, 'div' => false,  'tabindex' => 1, 'required'=>'required', 'placeholder' => __('Email'),'class'=>array('form-control'))); ?>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <?php echo $this->Form->input('password', array('label'=> false, 'div' => false, 'required'=>'required', 'tabindex' => 2, 'placeholder' => __('Password'),'class'=>array('form-control'))); ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-6 col-sm-6 col-xs-12 text-left pl-none">
                        <div class="checkbox">
                            <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 text-right pr-none">
                        <div class="checkbox">
                            <?php echo $this->Html->link('Forgot your password?', array('controller' => 'admins', 'action' => 'password')); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </p>
                </fieldset>
            </form>
        </div>
        <!--/span-->
    </div><!--/row-->

</div><!--/.fluid-container-->

<script type="text/javascript">
    $("#AdminIndexForm").validate({
                    rules: {
                        "data[Admin][login]": {
                            required: true,
                            email: true
                        },
                        
                    },
                    messages: {
                        "data[Admin][login]": {
                            email: "Please enter a valid email address.",
                        }
                    },
                   
                });
    </script>