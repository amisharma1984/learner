<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>-->
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
	<script type="text/javascript" language="javascript" class="init">
	/*	$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );*/
	</script>
	
	<script>
	/*$(document).ready(function() {
			$('#example').dataTable( {
		  "columnDefs": [
			{ "visible": false, "targets": 4 } // for hide 5th column
		  ]
		} );
	} );*/
	</script>

	
	<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable( {
			//"order": [[ 2, 'asc' ]],// order by column 3 asc (i.e order by Percentage of students selecting the correct answer)
			//"ordering": false,
            "columns": [
                null,
                null,
				null,
				null,
				null,
				null,
				null,
				
               { "orderable": false }, 
            ]
        } );
    } );
</script>
<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Test User Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Test School List</h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
			<br>
		<div class="clear"></div>
		  
          <div class="col-sm-8 col-md-12">
		   <input class="btn btn-primary btn-md" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/select_school_exams'?>')" value="Create School Testing Account" style="margin-bottom: 10px;" data-toggle="modal" type="button">
		  <!--<input class="btn btn-primary btn-md" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/add_test_user'?>')" value="Add Test Student" style="margin-bottom: 10px;" data-toggle="modal" type="button">
		  
		   <input class="btn btn-primary btn-md" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/add_test_school'?>')" value="Add Test School" style="margin-bottom: 10px;" data-toggle="modal" type="button">
		   
		    <input class="btn btn-primary btn-md" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/test_user_list'?>')" value="Test Student List" style="margin-bottom: 10px;" data-toggle="modal" type="button">-->
		   
	<div class="prof_details prof_form">
	<table id="example" class="display student-list-apr " border="1" bordercolor="#fff" cellspacing="0"  width="100%">
				<thead>
					<tr>
			<th>S.No.</th>
			<th>Name</th>
			<!-- <th>Head Teacher Name</th> -->
			<th>Email</th>
			<th>Password</th>
			<th>Reported Problems</th>
			<!--<th>User Type</th>-->
			<th>Created On</th>
			<th>Action</th>
			</tr>
			</thead>
				<tbody>
					
					 <?php
						if(!empty($testUserListAr)){ //pr($testUserListAr);
						$i = 1;
							foreach($testUserListAr as $val){
					?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $val['School']['name'];?></td>
						<!-- <td><?php //echo $val['School']['manager_name'];?></td> -->
						<td><?php echo $val['School']['manager_email'];?></td>
						<td><?php echo $val['School']['normal_password'];?></td>
						<td>
						<?php if(count($val['SchoolReportedProblem']) > 0){?>
						<a target="_blank" style="color:red;" href="<?php echo $this->webroot.'admins/school_reported_problem_list/'.$val['School']['id'];?>">Click for reported problems(<?php echo count($val['SchoolReportedProblem']);?>)</a>
						<?php } else {?>
						<a href="javascript:void(0);">Click for reported problems</a>
						<?php }?>
						
						</td>
						
						<!--<td><?php //echo Test School;?></td>-->
						<td><?php echo date('Y-m-d', strtotime($val['School']['created']));?></td>
						<td class="center">
                                
                                <a href="<?php echo $this->webroot.'admins/edit_test_school/'.$val['School']['id'];?>" class="btn-info btn-sm" title="Edit" data-subject="2" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;">
                                    <span class="glyphicon glyphicon-edit icon-white"></span>
                                </a>
                                <a href="<?php echo $this->webroot.'admins/delete_test_school/'.$val['School']['id'];?>" class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" onclick="return confirm('Are you sure to delete this record?')">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                <a href="<?php echo $this->webroot.'admins/select_school_exams/'.$val['School']['id'];?>" target="_blank" class="btn-info btn-sm" title="Take Exam" data-subject="2" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;">
                                    Exams
                                </a>
                            </td>
						
				   </tr>
			<?php
			   $i++;
				}
			}
			?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>
	<?php echo $this->element('JS/external-scripts'); ?>