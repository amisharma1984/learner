<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Profile'), "link" => array('controller' => 'admins', 'action' => 'profile', $admindata['Admin']['id']))
        ), true);
?>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> View Reported Problem</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'add_test_user'), true), 'class' => 'form-horizontal', 'role' => 'form')); ?>
                
				
           
                <?php 
           // pr($reportedProblemData);die;
            foreach ($reportedProblemData as $key=>  $value) { 
                ?>
			   
			   
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Exam Title</label>
                    <div class="col-md-9 col-md-3 col-xs-12">
					<?php echo $reportedProblemData['SchoolReportedProblem']['issue_type'];?>
                        
                    </div>
                </div>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Issue Details</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo $reportedProblemData['SchoolReportedProblem']['description'];?>
                        
                    </div>
                </div>
               <?php $imgSrc = WWW_ROOT . 'img\problems\schools\\'.$reportedProblemData['SchoolReportedProblem']['image'];  
                if ($imgSrc != '') {?>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Attached Image:</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
                        <img src="<?php echo $imgSrc;  ?>">
                    </div>
                </div>
              <?php } 
          }?>
              
              
              

              
                
              
				
				
				
            
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       
                        <a class="btn btn-default btn-primary" onclick="window.history.go(-1); return false;" href="<?php //echo $this->Html->url(array('controller' => 'admins', 'action' => 'management_access')); ?>">Cancel</a>
                    </div>
                </div>

               <?php echo $this->Form->end();?>
          

            </div>
        </div>
    </div>
</div>
<!--/span-->

<?php echo $this->element('JS/external-scripts'); ?>


