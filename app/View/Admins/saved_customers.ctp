<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('School Management'), "link" => array( 'controller' => 'school_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-cog"></i> Sent E-Mail List</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mb-sm">
                       
					   <!--<input type="button" class="btn btn-primary btn-sm" value="Add School" style="margin-bottom: 10px;" data-toggle="modal" data-target="#newSchoolModal">-->
						
                        <input type="button" class="btn btn-primary btn-sm check" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" id="DelButtonId" class="btn btn-primary btn-sm" value="Delete All" style="margin-bottom: 10px;" data-toggle="modal" data-target="#deleteSentMailModal">
                    </div>
                </div>
				
               <?php /*?> <div class="row">    
                    
                    <?php echo $this->Form->create('Email', array('url' => array('controller' => 'admins', 'action' => 'search'), 'class' => 'form-inline filters', 'role' => 'form' )); ?>
                    <?php
                        echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                        echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                    ?>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm">                            
                        <?php echo $this->Form->input('search', array('label' => __('Search: '), 'div' => false, 'value' => urldecode($search), 'class' => 'form-control', 'style' => 'width:100%;')); ?>
                    </div>
                   
                    <div class="form-group col-md-2 col-sm-2 col-xs-12 mb-sm">
                        <?php
                            echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'class' => 'chosen form-control', 'style' => 'width:100%;', 'label' => __('Show: '), 'div' => false));
                        ?>
                    </div>                    
                    <?php echo $this->Form->end(); ?>
                </div><?php */?>
				
                <div class="table-responsive savedCustomerTh">                
                <table class="table table-striped table-bordered bootstrap-datatable dataTable" id="YourTableId">
                    <thead>
                        <tr>
                            <th><?php echo $this->Paginator->sort('Email.from_email', 'From Email');?></th>
                            <th><?php echo $this->Paginator->sort('Email.to_email', 'To Email');?></th>
                            <th><?php echo $this->Paginator->sort('Email.subject', 'Subject');?></th>
                            <th><?php echo $this->Paginator->sort('Email.created', 'Sent Date');?></th>
                            <th><?php echo $this->Paginator->sort('Email.issended', 'Status');?></th>
                            <th style="width:100px;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php echo $this->Form->create('Email', array('url' => array('controller' => 'admins', 'action' => 'delete_sent_maill_all'))); ?>
                        <?php
                        if(!empty($EmailDeliveryAr)){
                            $schoolCount = 1;
                            $rowClass = 'even';
                            foreach ($EmailDeliveryAr as $key => $val){
								 
                                $rowClass = (($schoolCount % 2) != 0)?'odd':'even';
                        ?>
						 
                        <tr class="<?php echo $rowClass;?>">
                            <td class="<?php echo $rowClass;?>">
                                <div class="checkbox" style="margin-top:0;"> <label> <input class="select-school__" type="checkbox" name="EmailDeliveryIds[]" value="<?php echo $val['EmailDelivery']['id']; ?>"> <?php echo !empty($val['Email']['from_email']) ? $val['Email']['from_email'] : $val['Admin']['email'];?> </label> </div>
                            </td>
 
                            
                            <td class="<?php echo $rowClass;?>"><?php echo !empty($val['Email']['to_email']) ? $val['Email']['to_email'] : $val['School']['email'];?></td>
                            <td class="<?php echo $rowClass;?>"><?php echo $val['Email']['subject'];?></td>
                            <td class="center <?php echo $rowClass;?>">
							<?php 
							    //echo date("jS F, Y", strtotime($val['Email']['created'])); 
							   //echo $val['Email']['created'];
							   echo date("d/m/Y H:i:s", strtotime($val['Email']['created']))
							?>
							</td>
							
                            <td class="center <?php echo $rowClass;?>">
							
							<?php 
							#echo $val['EmailDelivery']['id'].'<br/>';
							   $isread=$val['EmailDelivery']['issended'];
							   if ($isread == 1) {

								echo '<span class="label-success label label-default">
                                        Read</span>' ;
							   } else{
									echo '<span class="label-warning label label-default">
                                        Delivered
                                    </span>';
							   }
								
							?>
							
							</td>
                           
                            <td class="center <?php echo $rowClass;?>">
							
							 <a href="<?php echo $this->webroot.'admins/view_saved_customer/'.$val['EmailDelivery']['id'];?>" class="btn-success view-school btn-sm" title="View" data-subject="2" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span>
                                </a>
                              
							   <a href="<?php echo $this->webroot.'admins/delete_saved_customer/'.$val['EmailDelivery']['id'];?>" class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" onclick="return confirm('Are you sure to delete this record?')">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                            </td>
                        </tr>
						  
                        <?php
                                $schoolCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No record found</td></tr>
                        <?php } ?>
						<?php echo $this->Form->end(); ?>
                    </tbody>
                </table>
                    
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->    
                    
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- Start Add and edit School Modal-->
<?php echo $this->element('school/modal/add-edit', array('schoolLevelOptions' => $schoolLevelOptions, 'schoolTypeOptions' => $schoolTypeOptions)); ?>
<!-- End Add and edit School Modal-->

<!-- Start School View Modal-->
<div class="modal fade" id="schoolViewModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School View Modal -->

<!-- Start School delete Modal-->
<div class="modal fade" id="deleteSentMailModal___" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Sent Email</h4>
      </div>
      <div class="modal-body">
          <p>Are you sure to delete selected records?</p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary sentMailDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School delete Modal -->

<!-- Start School Login Credentials Modal-->
<div class="modal fade" id="loginCredentialsModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary schoolCredentialsConfirm" data-isChecked="false">Yes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School Login Credentials Modal -->

<!-- Start school View Template -->
<?php echo $this->element('school/template/view'); ?>
<!-- End school View Template -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/school-management.js"></script>
<script type="text/javascript">
    $(function () {
        
        if($('.chosen').length){
            $('.chosen').chosen();
        }
        
        $('select#SchoolLimit, select#SchoolInformationLevel, select#SchoolInformationType, input#SchoolSearch, input#SchoolOrder', '.filters').bind("change", changePaginator); // orange
        
        $('table.dataTable thead .table-heading').click(function(){
            var parentScop = this;
            var orderName = $(this).attr('data-order');
            var orderType = ($(this).attr('data-order-type') == 'asc')?'desc':'asc';
            
            $('#SchoolOrder').val(orderName);
            $('#SchoolOrderType').val(orderType);
            
            $('table.dataTable thead .table-heading').each(function(index, element){
                $(element).removeClass('sorting_'+$(parentScop).attr('data-order-type')).addClass('sorting');
            });
            $(this).removeClass('sorting').addClass('sorting_'+orderType);
            $(this).attr('data-order-type', orderType);
            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#SchoolSearch', '.filters').val('');
        $('select#SchoolLimit', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var communitySlug = '<?php echo Configure::read('Community.slug') ?>/';
        if (communitySlug == '/') communitySlug = '';

        var newurl = base+'school_managements/index';
        
//        if (passed.page !== undefined)
//            newurl += '/page:'+passed.page;
        var searchData = $('input#SchoolSearch', '.filters').val();
//        if (passed.search !== undefined){
//            newurl += '/search:'+passed.search;
//        }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }
        
        var level = $('select#SchoolInformationLevel', '.filters').val();
        if (level)
            newurl += '/level:'+$('select#SchoolInformationLevel', '.filters').val();
        
        if ($('select#SchoolInformationType', '.filters').val() != null && $('select#SchoolInformationType', '.filters').val() != '')
            newurl += '/type:'+$('select#SchoolInformationType', '.filters').val();

        if ($('select#SchoolLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#SchoolLimit', '.filters').val();
        if ($('input#SchoolOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#SchoolOrder', '.filters').val();
        if ($('input#SchoolOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#SchoolOrderType', '.filters').val();
        //console.log(newurl);
        window.location = newurl;
    }
</script>


<SCRIPT language="javascript">
	$(document).ready(function(){
		$("#DelButtonId").click(function(){
				if($('#YourTableId').find('input[type=checkbox]:checked').length == 0)
				{
					alert('Please select atleast one checkbox');
					return false;
				} else {
					confirm('Are you sure to delete the selected records?');
					document.getElementById("EmailDeleteSentMaillAllForm").submit();
					return true;
				}
		});
		
			  $('.check:button').click(function(){
					  var checked = !$(this).data('checked');
					  $('input:checkbox').prop('checked', checked);
					  $(this).val(checked ? 'Uncheck All' : 'Check All' )
					  $(this).data('checked', checked);
			});
		
		
	
	});
	

</SCRIPT>



<style>
.odd {
	background-color:#f1e8e8 !important;
}

.even {
	background-color:#ccd4cc;
}

.savedCustomerTh table th {
    background: #1f3446;
    padding: 10px;
    color: #fff;
}
</style>