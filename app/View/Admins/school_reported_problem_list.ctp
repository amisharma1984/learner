<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>-->
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
	<script type="text/javascript" language="javascript" class="init">
	/*	$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );*/
	</script>
	
	<script>
	/*$(document).ready(function() {
			$('#example').dataTable( {
		  "columnDefs": [
			{ "visible": false, "targets": 4 } // for hide 5th column
		  ]
		} );
	} );*/
	</script>

	
	<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable( {
			//"order": [[ 2, 'asc' ]],// order by column 3 asc (i.e order by Percentage of students selecting the correct answer)
			//"ordering": false,
            "columns": [
                null,
                null,
				null,
				null,
               { "orderable": false }, 
            ]
        } );
    } );
</script>
<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Test User Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> School Reported Problem List</h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
			<br>
		<div class="clear"></div>
		  
          <div class="col-sm-8 col-md-12">
		   
	<div class="prof_details prof_form">
	<table id="example" class="display student-list-apr "  bordercolor="#fff" cellspacing="0"  width="100%">
				<thead>
					<tr>
			<th>S.No.</th>
			<th>Exam Title</th>
			<th>Issue Details</th>
			<th>Image</th>
			<th>Posted On</th>
			<th style="width:75px;">Action</th>
			</tr>
			</thead>
				<tbody>
					
					 <?php
						if(!empty($reportedProblemList)){ //pr($testUserListAr);
						$i = 1;
							foreach($reportedProblemList as $val){
					?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $val['SchoolReportedProblem']['issue_type'];?></td>
						<td><?php echo $val['SchoolReportedProblem']['description'];?></td>
						<?php $imgSrc = WWW_ROOT . 'img\problems\schools\\'.$val['SchoolReportedProblem']['image'];  ?>
						<td><img src="<?php echo $imgSrc;  ?>"><?php echo $val['SchoolReportedProblem']['image'] ;?></td>
						<td><?php echo $val['SchoolReportedProblem']['created'];?></td>
						<td class="center">
                                
                                <a href="<?php echo $this->webroot.'admins/view_school_reported_problem/'.$val['SchoolReportedProblem']['issue_type'];?>" class="btn-info btn-sm" title="View" data-subject="2" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;">
                                <i class="fa fa-eye-slash" style="font-color:blue;"></i>
                                </a>
                                <a href="#<?php echo $this->webroot.'admins/delete_reported_problem/'.$val['SchoolReportedProblem']['id'];?>" class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" onclick="return confirm('Are you sure to delete this record?')">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                
                            </td>
						
				   </tr>
			<?php
			   $i++;
				}
			}
			?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>
	<?php echo $this->element('JS/external-scripts'); ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">  