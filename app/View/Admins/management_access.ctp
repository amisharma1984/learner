<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Management Access'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Management Access</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
			<br>
		<div class="clear"></div>
		  
          <div class="col-sm-8 col-md-12">
		  <input class="btn btn-primary btn-md" data-toggle="modal" data-target="#SchoolTestingModal"  value="Testing Accounts" style="margin-bottom: 10px;" data-toggle="modal" type="button">
		  
		   <input class="btn btn-primary btn-md" data-toggle="modal" data-target="#StudentFreeModal" value="Free Accounts" style="margin-bottom: 10px;" data-toggle="modal" type="button">
		   
		    <input class="btn btn-primary btn-md" data-toggle="modal" data-target="#ManagerAccountModal" value="Manager Account" style="margin-bottom: 10px;" data-toggle="modal" type="button">
			
			
			
	
          </div>
        </div>
      </div>
    </div>
	
<div class="modal fade" id="SchoolTestingModal" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Testing Account</h4>
            </div> 
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/select_school_exams'?>')">Create School Testing Account </button>
                  
				  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/test_school_list'?>')">School Testing Account List</button>
                  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/select_student_exams'?>')">Create Student Testing Account </button>
                  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="javascript:window.location.assign('<?php echo $this->webroot.'admins/test_user_list'?>')">Student Testing Account List</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="StudentFreeModal" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Free Accounts</h4>
            </div> 
            <div class="modal-body">
                <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-target = "#accountOptions">Create an Account </button>
                  
                <button type="button" onclick="openUrl('<?php echo $this->webroot.'admins/free_accounts_list';?>')" class="btn btn-secondary btn-lg btn-block">View Created Accounts</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="accountOptions" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Create Accounts For:</h4>
            </div> 
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'admins/select_student_exams';?>')">Student</button>
                 <button type="button" class="btn btn-secondary btn-lg btn-block" >Parent</button>
                  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'admins/select_school_exams';?>')" >Head Teacher</button>
            
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<?php 
 $accesTypeStudentParent = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'accesTypeStudentParent'));
 $accesTypeSchool = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'accesTypeSchool'));
 $adminEmail = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $adminData['Admin']['email']));
?>

<div class="modal fade" id="ManagerAccountModal" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> MANAGER ACCOUNT</h4>
            </div> 
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'admins/manager_account/'.$accesTypeStudentParent.'/'.$adminEmail;?>')">Student/Parent </button>
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'admins/manager_account/'.$accesTypeSchool.'/'.$adminEmail;?>')">School </button>
                  
            
			</div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function openUrl(url){
            //var win = window.open(url, '_blank');
            var win = window.open(url);
    }
	
	</script>

<?php echo $this->element('JS/external-scripts'); ?>