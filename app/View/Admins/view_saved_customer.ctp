<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Profile'), "link" => array('controller' => 'admins', 'action' => 'profile', $admindata['Admin']['id']))
        ), true);
		
		$restAllMails = $this->commonUtilities->getDetailOfSentEmail($savedCustomersDtls['EmailDelivery']['to_id'], $savedCustomersDtls['EmailDelivery']['id']);
		#echo $r;die;
?>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Sent E-mail Details</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
			 <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <a class="btn btn-default btn-primary" onclick="window.history.go(-1); return false;">&laquo; Back</a>
                    </div>
                </div>

			
			
			
			

                <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'add_test_user'), true), 'class' => 'form-horizontal', 'role' => 'form')); ?>
                
				
           
                
			   
			   <div class="form-group" style="margin-bottom: 4px;">
                    <label for="posterFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                
				<div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">From Email</label>
                    <div class="col-md-9 col-md-3 col-xs-12">
					<?php echo !empty($savedCustomersDtls['Email']['from_email']) ? $savedCustomersDtls['Email']['from_email'] : $savedCustomersDtls['Admin']['email'];?>
					
					<?php //echo $savedCustomersDtls['Email']['from_email'];?>
                        
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">To Email</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo !empty($savedCustomersDtls['Email']['to_email']) ? $savedCustomersDtls['Email']['to_email'] : $savedCustomersDtls['School']['email'];?>
					<?php //echo $savedCustomersDtls['Email']['to_email'];?>
                    
					 
					
                    </div>
                </div>
				<?php if( !empty($restAllMails) && count($restAllMails) > 0 ){?>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label"><?php echo count($restAllMails);?> more emails sent to</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo !empty($savedCustomersDtls['Email']['to_email']) ? $savedCustomersDtls['Email']['to_email'] : $savedCustomersDtls['School']['email'];?>
					
                    
					<a class="btn btn-default btn-primary" onclick="$('.restAllMails').slideToggle();">View All Sent EMails Detail</a>
					
                    </div>
                </div>
				<?php }?>
			   
			    <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Subject</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo $savedCustomersDtls['Email']['subject'];?>
                        
                    </div>
                </div>
              
			  
			   <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Message</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo $savedCustomersDtls['Email']['message'];?>
                        
                    </div>
                </div>
				
				
             <?php if(!empty($attachmentFiles)){?> 
			 
			 
          <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Attachment</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					
					<?php  
					
					foreach($attachmentFiles as $attach){
						    $file = explode('webroot',$attach['file']);
							$fileName = explode('/', $file[1]);
					?>
					<a target="_blank" href="<?php echo $this->webroot.ltrim($file[1], '/');?>"><?php echo end($fileName);?></a>
					<?php 
						echo '<br>';
					}
					?>
                        
                    </div>
                </div>
				
              <?php }?>
                
              
				
				<!--All Similar Mails-->
				
				<?php
				if( !empty($restAllMails) && count($restAllMails) > 0 ){
				?>
				<div style='float:left;width:100%;display:none' class='restAllMails box-content' >
				<?php
					foreach($restAllMails as $k=>$mailsDetail){
				?>
				<hr/>
				
				 
                
				<div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">From Email</label>
                    <div class="col-md-9 col-md-3 col-xs-12">
					<?php echo !empty($mailsDetail['Email']['from_email']) ? $mailsDetail['Email']['from_email'] : $mailsDetail['Admin']['email'];?>
					
					
                        
                    </div>
                </div>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">To Email</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo !empty($mailsDetail['Email']['to_email']) ? $mailsDetail['Email']['to_email'] : $mailsDetail['School']['email'];?>
					<?php //echo $mailsDetail['Email']['to_email'];?>
                        
                    </div>
                </div>
               
			   
			    <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Subject</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo $mailsDetail['Email']['subject'];?>
                        
                    </div>
                </div>
              
				<div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Status</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
						
						<?php 
							#echo $val['EmailDelivery']['id'].'<br/>';
							   $isread=$mailsDetail['EmailDelivery']['issended'];
							   $created=$mailsDetail['EmailDelivery']['created'];
							   $created = ( !empty($created))? date('j M Y g:i A', strtotime($created) ) :"";
							   $modified=$mailsDetail['EmailDelivery']['modified'];
							   $modified = ( !empty($modified))? date('j M Y g:i A', strtotime($modified) ) :"";
							   if ($isread == 1) {

								echo '<span class="label-success label label-default">
                                        Read</span>'. '  <span class="label label-default"> on '.$modified.'</span>' ;
							   } else{
									echo '<span class="label-warning label label-default">
                                        Delivered
                                    </span>'.'  <span class="label label-default"> on '.$created.'</span>' ;
							   }
								
							?>
							
                        
                    </div>
                </div>
				
			   <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Message</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					<?php echo $mailsDetail['Email']['message'];?>
                        
                    </div>
                </div>
				
				
             <?php if(!empty($attachmentFiles)){?> 
          <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-3 col-sm-3 col-xs-12 control-label">Attachment</label>
                    <div class="col-md-9 col-md-6 col-xs-12">
					
					<?php  
					
					foreach($attachmentFiles as $attach){
						    $file = explode('webroot',$attach['file']);
							$fileName = explode('/', $file[1]);
					?>
					<a target="_blank" href="<?php echo $this->webroot.ltrim($file[1], '/');?>"><?php echo end($fileName);?></a>
					<?php 
						echo '<br>';
					}
					?>
                        
                    </div>
                </div>
				
              <?php } }?>
				</div>
					
				<?php 
				}
				?>
				<!--All Similar Mails+++-->
				
				
            
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                       
                        <a class="btn btn-default btn-primary" onclick="window.history.go(-1); return false;">&laquo; Back</a>
                    </div>
                </div>

               
          

            </div>
        </div>
    </div>
</div>
<!--/span-->

<?php echo $this->element('JS/external-scripts'); ?>


