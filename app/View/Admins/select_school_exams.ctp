<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<!--<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>-->
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<link href="<?php echo $this->webroot;?>css/style.css" rel="stylesheet">
	
<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>



<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php //echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="box-inner">
				 <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> SELECT EXAMS</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
			  
                 
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 

  <?php  //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'checkout'), true))); ?>
  <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'add_school'), true))); ?>

<table id="example" class="display student-list-apr " border="1" bordercolor="#fff" cellspacing="0"  width="100%">
  <tr>
    <th>List</th>
	
	<th>Year</th>
	<th>Exam Category</th>
	<th>Exam Name</th>
	<th style="text-align:center;">Price per student<br>($ AUD)</th>
	
	<th>Click to purchase Exams<!--<br> <input type="button" class="check chk-unchk-btn" value="Check All" />--></th>
  </tr>

  <?php
  if(!empty($allExamAr)){
	  $i = 1;
	  foreach($allExamAr as $val){ 
	       $category = explode(' ',$val['ExaminationCategory']['name']);
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
			

		  ?>
		  <tr>
    <td><?php echo $i?></td>
	 <td><?php echo $category[1]; ?></td>
	 <td><?php echo 'NAPLAN Style Trial'; ?></td>
	 <td><?php echo  ucwords(strtolower($val['Examination']['title']));?></td>
	  <td align="center"><?php echo $val['Examination']['price'];?></td>
	  <td width="170">
	 <input type="checkbox" class="case" value="<?php echo $val['Examination']['id'];?>" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo $val['Examination']['paper'];?>
	  
	  </td>
  </tr>
<?php
$i++;
	
	  }
  }
?>
		
		
 
</table>
</div>        <input type="hidden" value="checkout" name="page_name">
				<div class="clear_fix"></div>
				<div class="exam_chkout" style="float:left;">
					<input type="submit" id="YourbuttonId" class="btn btn-info def_btn" value="Next">
					
					<!--<input type="button" class="btn btn-info def_btn2" onclick="javascript:window.location.assign('<?php //echo $this->webroot.'admins/management_access'?>')" value="Cancel">-->
					
					<input type="button" class="btn btn-info def_btn2" onclick="javascript:window.history.back()" value="Cancel">
					<?php echo $this->Form->end(); ?>
				</div>
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
	<SCRIPT language="javascript">
	$(document).ready(function(){

		$('#example').dataTable( {
			//"order": [[ 2, 'asc' ]],// order by column 3 asc (i.e order by Percentage of students selecting the correct answer)
			//"ordering": false,
            "columns": [
                null,
                null,
				null,
				null,
				null,
				null,
               { "orderable": false }, 
            ]
        } );
		$("#YourbuttonId").click(function(){
				if($('#example').find('input[type=checkbox]:checked').length == 0)
				{
					alert('Please select atleast one checkbox');
					return false;
				}
		});
		
	  	$('.check:button').click(function(){
			  var checked = !$(this).data('checked');
			  $('input:checkbox').prop('checked', checked);
			  $(this).val(checked ? 'Uncheck All' : 'Check All' )
			  $(this).data('checked', checked);
	});

		
	
	});
	

</SCRIPT>

<?php //echo $this->element('JS/external-scripts'); ?>