<div class="ch-container">
    
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Reset your password</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-default">
                Enter your email address and we will send you a link to reset your password.
            </div>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'admins', 'action' => 'password'), true))); ?>
            <fieldset>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                    <?php echo $this->Form->input('Password.email', array('label' => false, 'div' => false, 'tabindex' => 1, 'required' => true, 'placeholder' => __('Email'), 'class' => array('form-control'))); ?>
                </div>
                <div class="clearfix"></div><br>

                <div class="center col-md-8 col-sm-8 col-xs-12">
                    <button type="submit" class="btn btn-primary">Send password reset email</button>
                </div>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
        <!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->
<script type="text/javascript">
    $("#AdminPasswordForm").validate({
        rules: {
            "data[Password][email]": {
                required: true,
                email: true
            },
        },
        messages: {
            "data[Admin][login]": {
                email: "Please enter a valid email address.",
            }
        },
    });
</script>