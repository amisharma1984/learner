<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Examination Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index'))
), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Examinations</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mb-sm">
                        <input type="button" class="btn btn-primary btn-md" value="Add Exam" style="margin-bottom: 10px;" data-toggle="modal" data-target="#examAddEditModal">
                        <input type="button" class="btn btn-primary btn-md selectAllExamButton" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" class="btn btn-primary btn-md" value="Delete All" style="margin-bottom: 10px;" data-toggle="modal" data-target="#deleteExamModal">
                    </div>
                </div>
                <div class="row filters">
                    
                    <?php echo $this->Form->create('Examination', array('url' => array('controller' => 'examination_managements', 'action' => 'search_exam'), 'class' => 'form-inline', 'role' => 'form' )); ?>
                    <?php
                        echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                        echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                    ?>
                    <div class="form-group col-md-3 col-sm-3 col-xs-12 pr-none mb-sm">
                        <label for="ExaminationExaminationTypeId">Exam Type:&nbsp;</label>
                        <?php
                        $examTypeOptions = $this->ExamType->getExamTypeList(Configure::read('App.examinationTypeList'));
                        ?>
                        <select name="data[Examination][examination_type_id]" class="form-control" style="width:100%;" id="ExaminationExaminationTypeId">
                            <option value="">- Select Exam Type -</option>
                            <?php foreach ($examTypeOptions as $key => $data){ ?>
                            <option value="<?php echo $key; ?>" <?php echo (!empty($examType) && ($examType == $key))?'selected':''; ?> ><?php echo $data; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="form-group col-md-3 col-sm-3 col-xs-12 pr-none mb-sm">
                        <label for="ExaminationExaminationCategoryId">Exam Category:&nbsp;</label>
                        <?php
                        $categoryOptions = $this->Category->getCategoryList(Configure::read('App.examinationCategoryList'));
                        //echo $this->Form->input('Examination.examination_category_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Category -', 'selected' => $categoryId, 'class' => 'form-control', 'style' => '', 'options' => $categoryOptions));
                        ?>
                        <select name="data[Examination][examination_category_id]" class="form-control" style="width:100%;" id="ExaminationExaminationCategoryId">
                            <option value="">- Select Exam Category -</option>
                            <?php foreach ($categoryOptions as $key => $data){ ?>
                            <option value="<?php echo $key; ?>" <?php echo (!empty($categoryId) && ($categoryId == $key))?'selected':''; ?> ><?php echo $data; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="form-group col-md-2 col-sm-2 col-xs-12 pr-none mb-sm">
                        <label for="ExaminationLimit">Show:&nbsp;</label>
                        <?php
                            echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'empty' => '- Select Limit -', 'style' => 'width:100%;', 'class' => 'chosen form-control',
                                'label' => false, 'div' => false));
                        ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 mb-sm">
                        <label for="ExaminationSearch">Search:&nbsp;</label>
                        <?php echo $this->Form->input('search', array('label' => false, 'div' => false, 'value' => urldecode($search), 'class' => 'form-control', 'style' => 'width:100%;')); ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                        
                </div>
                
                <div class="table-responsive">                
                  
                <table class="table table-striped table-bordered bootstrap-datatable dataTable" id="schoolListData">
                    <thead>
                        <tr>
                            <th class="table-heading <?php echo ($order == 'title')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="title" data-order-type="<?php echo ($order == 'title')?strtolower($orderType):'asc'; ?>">Name</th>
                            <th class="table-heading <?php echo ($order == 'paper')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="paper" data-order-type="<?php echo ($order == 'paper')?strtolower($orderType):'asc'; ?>">Paper</th>
                            <th class="table-heading <?php echo ($order == 'price')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="price" data-order-type="<?php echo ($order == 'price')?strtolower($orderType):'asc'; ?>">Price</th>
                            <th class="table-heading <?php echo ($order == 'exam_type')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="exam_type" data-order-type="<?php echo ($order == 'exam_type')?strtolower($orderType):'asc'; ?>">Type</th>
                            <th class="table-heading <?php echo ($order == 'category')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="category" data-order-type="<?php echo ($order == 'category')?strtolower($orderType):'asc'; ?>">Category</th>
                            <th class="table-heading <?php echo ($order == 'created')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="created" data-order-type="<?php echo ($order == 'created')?strtolower($orderType):'asc'; ?>">Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($examinations)){ 
                            $examCount = 1;
                            $rowClass = 'even';
                            $securityKey = Configure::read('Security.salt');
                            foreach ($examinations as $key => $examination){
                                $rowClass = (($examCount % 2) != 0)?'odd':'even';
                                
                                $encryptedResult = Security::encrypt($examination['Examination']['id'], $securityKey);
                                $encryptedResult = urlencode(base64_encode($encryptedResult));
                                
                        ?>
                        <tr>
                            <td>
                                <div class="checkbox" style="margin-top:0;"> <label for="examName<?php echo $examination['Examination']['id']; ?>"> <input class="select-exam" type="checkbox" id="examName<?php echo $examination['Examination']['id']; ?>" value="<?php echo $examination['Examination']['id']; ?>"> <?php echo $examination['Examination']['title']; ?> </label> </div>
                            </td>
                            <td><?php echo $examination['Examination']['paper']; ?></td>
                            <td><?php echo $examination['Examination']['price']; ?></td>
                            <td><?php echo $examination['ExaminationType']['name']; ?></td>
                            <td><?php echo $examination['ExaminationCategory']['name']; ?></td>
                            <td class="center"><?php echo date("jS F, Y", strtotime($examination['Examination']['created'])); ?></td>
                            <td class="center">
<!--                                <a class="btn-success previewExamination btn-sm" title="Preview Exam" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-exam="<?php echo $encryptedResult; ?>">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span> 
                                </a>-->
                                <a class="btn-info btn-sm" title="Edit" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#examAddEditModal" data-exam="<?php echo $examination['Examination']['id']; ?>">
                                    <span class="glyphicon glyphicon-edit icon-white"></span>
                                </a>
                                <a class="btn-danger btn-sm" title="Delete" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#deleteExamModal" data-exam-name="<?php echo $examination['Examination']['title']; ?>" data-exam="<?php echo $examination['Examination']['id']; ?>">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                <a class="btn-warning btn-sm set-question-handler" title="Set Questions" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-exam-name="<?php echo $examination['Examination']['title']; ?>" data-exam="<?php echo $examination['Examination']['id']; ?>">
                                    <span class="glyphicon glyphicon-share-alt icon-white"></span>&nbsp;<span class="glyphicon glyphicon-question-sign icon-white"></span>
                                </a>
                            </td>
                        </tr>
                        <?php
                                $examCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd">
                            <td valign="top" colspan="6" class="dataTables_empty">
                                <?php
                                    echo 'No exam found';
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->
                    
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- Start Add And edit Exam Modal-->
<div class="modal fade" id="examAddEditModal" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Examination</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="examAddEditMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'examination_managements', 'action' => 'add_exam'),
                    'id' => 'examAddEditForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="examFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
               
			   <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="ExaminationTitle" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Title</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.title', array('type' => 'text', 'label' => false, 'div' => false, 'required' => true, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
				
				 <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="ExaminationTitle" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Paper</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.paper', array('type' => 'text', 'label' => false,'placeholder' => 'eg. Test 1', 'div' => false, 'required' => true, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="ExaminationPrice" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;">*</sup>Price</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.price', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">Exam Duration<br>(<span style="font-size:11px; color:#6b6bc3;">in minutes for calculator/reading booklet)</span> </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.exam_duration_cal_rb', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left">Exam Duration<br>(<span style="font-size:11px; color:#6b6bc3;">for non-calculator/language convention</span>)</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.exam_duration_non_cal_lc', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div> 
				
				
				<div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">Exam Duration(<span style="font-size:11px; color:#6b6bc3;">for normal/writing leaflet</span>) </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.exam_duration_normal_wl', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left">Exam Duration(<span style="font-size:11px; color:#6b6bc3;">for sample test</span>)</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.exam_duration', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div> 
				
				
				
				
				<div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left"><sup style="color:red;"></sup>Questions Limit<br>(<span style="font-size:11px; color:#6b6bc3;">for calculator/reading booklet</span>) </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.limit_cal_rb', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left"><sup style="color:red;"></sup>Questions Limit<br>(<span style="font-size:11px; color:#6b6bc3;">for non-calculator/language convention</span>)</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.limit_non_cal_lc', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div> 
				
				
				<div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left"><sup style="color:red;"></sup>Questions Limit(<span style="font-size:11px; color:#6b6bc3;">for normal/writing leaflet</span>) </label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.limit_normal_wl', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left"><sup style="color:red;"></sup>Questions Limit(<span style="font-size:11px; color:#6b6bc3;">for sample test</span>)</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('Examination.limit_sample', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div> 
				
				
				
				
				
				
				
				 <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="ExaminationPrice" class="col-md-12 col-sm-12 col-xs-12 control-label text-left"><sup style="color:red;"></sup>Literacy Writing or Normal</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <select name="data[Examination][exam_flag]" data-action-name="ExamFlag" class="form-control ExamFlag" style="width:100%" id="ExamFlag" aria-required="true">
                                    <option value="">- Select -</option>
                                    <option value="1">Normal</option>
                                    <option value="2">Literacy Writing</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationTypeId" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left"><sup style="color:red;">*</sup>Exam Type</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <select name="data[Examination][examination_type_id]" data-action-name="Type" class="form-control examTypeCategory" style="width:100%" id="ExaminationExaminationTypeId" required="required" aria-required="true">
                                    <option value="">- Select Exam Type -</option>
                                    <?php foreach ($examTypeOptions as $key => $data){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $data; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="ExaminationExaminationCategoryId" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left"><sup style="color:red;">*</sup>Exam Category</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <select name="data[Examination][examination_category_id]" data-action-name="Category" class="form-control examTypeCategory" style="width:100%" id="ExaminationExaminationCategoryId" required="required" aria-required="true">
                                    <option value="">- Select Exam Category -</option>
                                    <?php foreach ($categoryOptions as $key => $data){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $data; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>   



				
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="ExaminationDescription" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Description</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">                        
                                <?php echo $this->Form->input('Examination.description', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
                
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="button" class="btn btn-primary" id="saveExamData" value="Save changes" style="" >
            </div>
        </div>
    </div>
</div>
<!-- Start Add And edit Exam Modal-->

<!-- Start Exam delete Modal-->
<div class="modal fade" id="deleteExamModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Examination</h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary examDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Exam delete Modal -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/tinymce/tinymce.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/exam-management.js"></script>
<script type="text/javascript">
    $(function () {
        
        $('select#ExaminationExaminationTypeId, select#ExaminationLimit, input#ExaminationSearch, input#ExaminationOrder', '.filters').bind("change", changePaginator); // orange
        
        $('select#ExaminationExaminationCategoryId', '.filters').bind("change", function(){
                        
            var actionName = 'Category';
            var selectedValue = $(this).val();
                selectedValue = (selectedValue)?selectedValue:'';
                
            if(selectedValue){
                var jqxhrAvailability = $.ajax({
                    type: "POST",
                    url: BASEURL + 'examination_managements/getExamTypeOrCategory',
                    data:{actionName: actionName, selectedValue: selectedValue},
                    success: function (response) {
                        //console.log(response);
                        if(response != ''){
                            var results = JSON.parse(response);
                            if(results.ExaminationType !== undefined && results.ExaminationType.id){
                                $('select#ExaminationExaminationTypeId', '.filters').val(results.ExaminationType.id);
                                changePaginator();
                            }
                        }
                    }
                });
            }else{
                changePaginator();
            }
        });
        
        $('table.dataTable thead .table-heading').click(function(){
            var parentScop = this;
            var orderName = $(this).data('order');
            var orderType = ($(this).data('order-type') == 'asc')?'desc':'asc';
            
            $('#ExaminationOrder').val(orderName);
            $('#ExaminationOrderType').val(orderType);
            
            $(this).each(function(index, element){
                $(element).removeClass('sorting_'+$(parentScop).data('order-type')).addClass('sorting');
            });
            $(this).removeClass('sorting').addClass('sorting_'+orderType);
            $(this).data('order-type', orderType);
            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#ExaminationSearch', '.filters').val('');
        $('select#ExaminationLimit', '.filters').val('');
        $('select#ExaminationExaminationCategoryId', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var newurl = base+'examination_managements/index';
        
//        if (passed.parent_id !== undefined)
//            newurl += '/parent_id:'+passed.parent_id;category_id
        
        var searchData = $('input#ExaminationSearch', '.filters').val();
//        if (passed.search !== undefined){
//            newurl += '/search:'+passed.search;
//        }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }
        
        var typeId = $('select#ExaminationExaminationTypeId', '.filters').val();
        if (typeId)
            newurl += '/exam_type:'+$('select#ExaminationExaminationTypeId', '.filters').val();
        
        if ($('select#ExaminationExaminationCategoryId', '.filters').val() != '')
            newurl += '/category_id:'+$('select#ExaminationExaminationCategoryId', '.filters').val();
        if ($('select#ExaminationLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#ExaminationLimit', '.filters').val();
        if ($('input#ExaminationOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#ExaminationOrder', '.filters').val();
        if ($('input#ExaminationOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#ExaminationOrderType', '.filters').val();

        window.location = newurl;
    }
    
    tinymce.init({
        selector: '.textEditor',
        autoresize_min_height: 200,
        autoresize_max_height: 400,
        theme: 'modern',
        plugins: [
            'autoresize advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: BASEURL + 'app/uploadEditorImageFiles',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
    
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];
      
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);
      
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
            };
    
            input.click();
        },
        relative_urls: false,
        convert_urls: false,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
            BASEURL + 'css/custom-editor-style.css'
        ]
    });
</script>
