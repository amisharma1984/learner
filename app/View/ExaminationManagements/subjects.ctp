<?php

echo $this->Breadcrumb->show(array(
    array("text" => __('Examination Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index')),
    array("text" => __('Subjects'), "link" => array('controller' => 'examination_managements', 'action' => 'subjects'))
), true);

?>
<?php echo $this->Session->flash(); ?>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 
                    <?php
                    //if(!empty($parentCategoryName)){
                        //echo 'Examination Sub-categories of '.$parentCategoryName;
                    //}else{
                        echo 'Subjects';
                    //}
                    ?>
                </h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="button" class="btn btn-primary" value="Add Subject" style="margin-bottom: 10px;" data-toggle="modal" data-target="#subjectAddEditModal">
                        <input type="button" class="btn btn-primary selectAllSubjectButton" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" class="btn btn-primary" value="Delete All" style="margin-bottom: 10px;" data-toggle="modal" data-target="#deleteSubjectModal">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 filters">
                        
                        <?php echo $this->Form->create('Subject', array('url' => array('controller' => 'examination_managements', 'action' => 'search_subjects'), 'class' => 'form-inline', 'role' => 'form' )); ?>
                        <?php
                            echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                            echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                        ?>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12 pl-none">
                            <?php
                                echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'empty' => '- Select Limit -', 'class' => 'chosen form-control',
                                    'label' => __('Show: '), 'div' => false));
                            ?>
                        </div>
                        
                        <div class="form-group col-md-6 col-sm-6 col-xs-12 pr-none pl-none">                            
                            <?php echo $this->Form->input('search', array('label' => __('Search: '), 'div' => false, 'value' => urldecode($search), 'class' => 'form-control')); ?>
                        </div>
                        
                        <?php echo $this->Form->end(); ?>
                        
                    </div>
                </div>
                <div class="table-responsive">                
                  <?php //pr($examinationTypes); ?>
                <!-- examinationCategories -->  
                <table class="table table-striped table-bordered bootstrap-datatable dataTable">
                    <thead>
                        <tr>
                            <th class="table-heading <?php echo ($order == 'name')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="name" data-order-type="<?php echo ($order == 'name')?strtolower($orderType):'asc'; ?>">Name</th>
                            <th class="table-heading <?php echo ($order == 'slug')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="slug" data-order-type="<?php echo ($order == 'slug')?strtolower($orderType):'asc'; ?>">Slug</th>
                            <th class="table-heading <?php echo ($order == 'created')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="created" data-order-type="<?php echo ($order == 'created')?strtolower($orderType):'asc'; ?>">Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($subjects)){
                            $subjectCount = 1;
                            $rowClass = 'even';
                            foreach ($subjects as $key => $subject){
                                $rowClass = (($subjectCount % 2) != 0)?'odd':'even';
                        ?>
                        <tr>
                            <td>
                                <div class="checkbox" style="margin-top:0;"> <label for="subjectName<?php echo $subject['Subject']['id']; ?>"> <input class="select-subject" type="checkbox" id="subjectName<?php echo $subject['Subject']['id']; ?>" value="<?php echo $subject['Subject']['id']; ?>"> <?php echo $subject['Subject']['name']; ?> </label> </div>
                            </td>
                            <td><?php echo $subject['Subject']['slug']; ?></td>
                            <td class="center"><?php echo date("jS F, Y", strtotime($subject['Subject']['created'])); ?></td>
                            <td class="center">
<!--                                <a class="btn-success view-school btn-sm" title="View" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#categoryViewModal" data-category="<?php //echo $subject['Subject']['id']; ?>">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span> 
                                </a>-->
                                <a class="btn-info btn-sm" title="Edit" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#subjectAddEditModal" data-subject="<?php echo $subject['Subject']['id']; ?>">
                                    <span class="glyphicon glyphicon-edit icon-white"></span>
                                </a>
                                <a class="btn-danger btn-sm" title="Delete" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#deleteSubjectModal" data-subject-name="<?php echo $subject['Subject']['name']; ?>" data-subject="<?php echo $subject['Subject']['id']; ?>">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                
                            </td>
                        </tr>
                        <?php
                                $subjectCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd">
                            <td valign="top" colspan="4" class="dataTables_empty">
                                <?php
                                
                                    echo 'No subject found';
                                
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                    
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->
                                    
                
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- Start Add And edit Exam Modal-->
<div class="modal fade" id="subjectAddEditModal" tabindex="-1" role="dialog" aria-labelledby="subjectAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Subject</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="subjectAddEditMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> Exam type saved successfully. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'examination_managements', 'action' => 'add_subject'),
                    'id' => 'subjectAddEditForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="subjectFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <label for="SubjectName" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Subject.name', array('type' => 'text', 'label' => false, 'div' => false, 'required' => true, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="SubjectDescription" class=" col-md-3 col-sm-3 col-xs-12 control-label">Description</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">                        
                        <?php echo $this->Form->input('Subject.description', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                
                <?php echo $this->Form->end(); ?>
                
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="button" class="btn btn-primary" id="saveSubjectData" value="Save changes" style="" >
            </div>
        </div>
    </div>
</div>
<!-- Start Add School Modal-->

<!-- Start Category delete Modal-->
<div class="modal fade" id="deleteSubjectModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete Subject</h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary subjectDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Category delete Modal -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/exam-subject.js"></script>
<script type="text/javascript">
    $(function () {
        
        $('select#SubjectLimit, input#SubjectSearch, input#SubjectOrder', '.filters').bind("change", changePaginator); // orange
        
        $('table.dataTable thead .table-heading').click(function(){
            var parentScop = this;
            var orderName = $(this).attr('data-order');
            var orderType = ($(this).attr('data-order-type') == 'asc')?'desc':'asc';
            
            $('#SubjectOrder').val(orderName);
            $('#SubjectOrderType').val(orderType);
            
            $('table.dataTable thead .table-heading').each(function(index, element){
                $(element).removeClass('sorting_'+$(parentScop).attr('data-order-type')).addClass('sorting');
            });
            $(this).removeClass('sorting').addClass('sorting_'+orderType);
            $(this).attr('data-order-type', orderType);
            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#SubjectSearch', '.filters').val('');
        $('select#SubjectLimit', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var newurl = base+'examination_managements/subjects';
                
        var searchData = $('input#SubjectSearch', '.filters').val();
        // if (passed.search !== undefined){
        //     newurl += '/search:'+passed.search;
        // }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }

        if ($('select#SubjectLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#SubjectLimit', '.filters').val();
        if ($('input#SubjectOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#SubjectOrder', '.filters').val();
        if ($('input#SubjectOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#SubjectOrderType', '.filters').val();

        window.location = newurl;
    }
    
</script>