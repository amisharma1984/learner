<style type="text/css">
    .filters .form-control{margin-left: 0;}
</style>
<?php
if(!empty($categoriesBreadcrumb)){
    echo $this->Breadcrumb->show($categoriesBreadcrumb, true);
}else{
    echo $this->Breadcrumb->show(array(
        array("text" => __('Examination Management'), "link" => array( 'controller' => 'examination_managements', 'action' => 'index')),
        array("text" => __('Categories'), "link" => array('controller' => 'examination_managements', 'action' => 'categories'))
    ), true);
}
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> 
                    <?php
                    //if(!empty($parentCategoryName)){
                        //echo 'Examination Sub-categories of '.$parentCategoryName;
                    //}else{
                        echo 'Examination Categories';
                    //}
                    ?>
                </h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mb-sm">
                        <input type="button" class="btn btn-primary" value="Add Category" style="margin-bottom: 10px;" data-toggle="modal" data-target="#categoryAddEditModal">
                        <input type="button" class="btn btn-primary selectAllCategoryButton" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" class="btn btn-primary" value="Delete All" style="margin-bottom: 10px;" data-toggle="modal" data-target="#deleteCategoryModal">
                    </div>
                </div>
                <div class="row filters">
                    
                        
                    <?php echo $this->Form->create('ExaminationCategory', array('url' => array('controller' => 'examination_managements', 'action' => 'search_categories'), 'class' => 'form-inline', 'role' => 'form' )); ?>
                    <?php
                        echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                        echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                    ?>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm">
                        <label for="ExaminationCategoryExaminationTypeId">Exam Type:&nbsp;</label>
                        <?php
                        $examTypeOptions = $this->ExamType->getExamTypeList(Configure::read('App.examinationTypeList'));
                        ?>
                        <select name="data[ExaminationCategory][examination_type_id]" class="form-control" style="width:100%;" id="ExaminationCategoryExaminationTypeId">
                            <option value="">- Select Exam Type -</option>
                            <?php foreach ($examTypeOptions as $key => $data){ ?>
                            <option value="<?php echo $key; ?>" <?php echo (!empty($examType) && ($examType == $key))?'selected':''; ?> ><?php echo $data; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="form-group col-md-2 col-sm-2 col-xs-12 pr-none mb-sm">
                        <label for="ExaminationCategoryLimit">Show:&nbsp;</label>
                        <?php
                            echo $this->Form->input('ExaminationCategory.limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'empty' => '- Select Limit -', 'style' => 'width:100%;', 'class' => 'chosen form-control',
                                'label' => false, 'div' => false));
                        ?>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 mb-sm">
                        <label for="ExaminationSearch">Search:&nbsp;</label>
                        <?php echo $this->Form->input('search', array('label' => false, 'div' => false, 'value' => urldecode($search), 'class' => 'form-control', 'style' => 'width:100%;')); ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                     
                </div>
                <div class="table-responsive">                
                  <?php //pr($examinationCategories); ?>
                <!-- examinationCategories -->  
                <table class="table table-striped table-bordered bootstrap-datatable dataTable" id="schoolListData">
                    <thead>
                        <tr>
                            <th class="table-heading <?php echo ($order == 'name')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="name" data-order-type="<?php echo ($order == 'name')?strtolower($orderType):'asc'; ?>">Category name</th>
                            <th class="table-heading <?php echo ($order == 'slug')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="slug" data-order-type="<?php echo ($order == 'slug')?strtolower($orderType):'asc'; ?>">Category slug</th>
                            <th class="table-heading <?php echo ($order == 'exam_type')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="exam_type" data-order-type="<?php echo ($order == 'exam_type')?strtolower($orderType):'asc'; ?>">Exam type</th>
                            <th class="table-heading <?php echo ($order == 'created')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="created" data-order-type="<?php echo ($order == 'created')?strtolower($orderType):'asc'; ?>">Created</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($examinationCategories)){//pr($examinationCategories);exit();
                            $categoryCount = 1;
                            $rowClass = 'even';
                            foreach ($examinationCategories as $key => $category){
                                $rowClass = (($categoryCount % 2) != 0)?'odd':'even';
                        ?>
                        <tr>
                            <td>
                                <div class="checkbox" style="margin-top:0;"> <label for="categoryName<?php echo $category['ExaminationCategory']['id']; ?>"> <input class="select-category" type="checkbox" id="categoryName<?php echo $category['ExaminationCategory']['id']; ?>" value="<?php echo $category['ExaminationCategory']['id']; ?>"> <?php echo $category['ExaminationCategory']['name']; ?> </label> </div>
                            </td>
                            <td><?php echo $category['ExaminationCategory']['slug']; ?></td>
                            <td><?php echo $category['ExaminationType']['name']; ?></td>
                            <td class="center"><?php echo date("jS F, Y", strtotime($category['ExaminationCategory']['created'])); ?></td>
                            <td class="center">
<!--                                <a class="btn-success view-school btn-sm" title="View" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#categoryViewModal" data-category="<?php //echo $category['ExaminationCategory']['id']; ?>">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span> 
                                </a>-->
                                <a class="btn-info btn-sm" title="Edit" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#categoryAddEditModal" data-category="<?php echo $category['ExaminationCategory']['id']; ?>">
                                    <span class="glyphicon glyphicon-edit icon-white"></span>
                                </a>
                                <a class="btn-danger btn-sm" title="Delete" data-toggle="modal" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-target="#deleteCategoryModal" data-category-name="<?php echo $category['ExaminationCategory']['name']; ?>" data-category="<?php echo $category['ExaminationCategory']['id']; ?>">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                <?php if(!empty($category['children']) && count($category['children']) > 0){ ?>
                                <a class="btn-warning btn-sm sub-categoryFinder" title="View sub-categories" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-category-name="<?php echo $category['ExaminationCategory']['name']; ?>" data-category="<?php echo $category['ExaminationCategory']['id']; ?>">
<!--                                    <span class="glyphicon glyphicon-share-alt icon-white"></span>&nbsp;<span class="glyphicon glyphicon-envelope icon-white"></span>-->
                                    <img style="height:16px; width: 16px;" src="<?php echo Router::url('/', true); ?>img/sub_category.png" alt="Sub Categories">
                                </a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                                $categoryCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd">
                            <td valign="top" colspan="5" class="dataTables_empty">
                                <?php
                                if(!empty($isParent)){
                                    echo 'No sub-category found';
                                }else{
                                    echo 'No category found';
                                }
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                    
                <!-- Start Pagination -->
                <?php //echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->
                                    
                
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!-- Start Add And edit Category Modal-->
<div class="modal fade" id="categoryAddEditModal" tabindex="-1" role="dialog" aria-labelledby="categoryAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Examination Category</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="categoryAddEditMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'examination_managements', 'action' => 'add_category'),
                    'id' => 'categoryAddEditForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="categoryFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <label for="ExaminationCategoryName" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('ExaminationCategory.name', array('type' => 'text', 'label' => false, 'div' => false, 'required' => true, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ExaminationCategoryParentId" class=" col-md-3 col-sm-3 col-xs-12 control-label">Parent</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        
                        <?php 
                        $categoryOptions = $this->Category->getCategoryList(Configure::read('App.examinationCategoryList'));
                        //echo $this->Form->input('ExaminationCategory.parent_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Category -', 'class' => 'form-control', 'style' => 'width:100%', 'options' => $categoryOptions)); 
                        ?>
                        <select name="data[ExaminationCategory][parent_id]" class="form-control" style="width:100%" id="ExaminationCategoryParentId" >
                            <option value="">- Select Category -</option>
                            <?php foreach ($categoryOptions as $key => $data){ ?>
                            <option value="<?php echo $key; ?>"><?php echo $data; ?></option>
                            <?php } ?>
                        </select>
                        
                    </div>
                </div>
                <div class="form-group">
                    <label for="ExaminationCategoryExaminationTypeId" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Exam type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        
                        <?php 
                        $examTypeOptions = $this->ExamType->getExamTypeList(Configure::read('App.examinationTypeList'));
                        ?>
                        <select name="data[ExaminationCategory][examination_type_id]" class="form-control" style="width:100%" id="ExaminationCategoryExaminationTypeId" onchange="return setAvailability(this.value);" required="required">
                            <option value="">- Select Category -</option>
                            <?php foreach ($examTypeOptions as $key => $data){ ?>
                            <option value="<?php echo $key; ?>"><?php echo $data; ?></option>
                            <?php } ?>
                        </select>
                        
                    </div>
                </div>
                
<!--                <div class="form-group">
                    <label for="ExaminationCategoryAvailablity" class="col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Examination Category Availability</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">                     

                        <?php
//                        echo $this->Form->input('ExaminationCategory.availability', array(
//                            'before' => '<div class="radio"><label for="ExaminationCategoryAvailabilitySchool">',
//                            'separator' => '</label></div><div class="radio"><label for="ExaminationCategoryAvailabilityPractice">',
//                            'after' => '</label></div>',
//                            'options' => array('school' => 'For School', 'practice' => 'For Practice Exam'),
//                            'type' => 'radio',
//                            'label' => false,
//                            'div' => false,
//                            'required' => true,
//                            'legend' => false,
//                            'hiddenField' => false
//                        ));
                        ?>
                         Output HTML Start 
                        <div class="radio">
                            <label for="">
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                            </label>
                        </div>
                         Output HTML End 
                        
                    </div>
                </div>-->
<!--                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="ExaminationCategoryInfo" class="col-md-12 col-sm-12 col-xs-12 control-label text-left" style="font-size: 13px; font-weight: normal;">
                        <span class="glyphicon glyphicon-info-sign" ></span>&nbsp;Note:&nbsp;
                        <small>Availability option indicates, all the examinations of this category will only show either for school online Exam or Practice Exam.</small>
                    </label>
                </div>-->
                <?php echo $this->Form->end(); ?>
                
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="button" class="btn btn-primary" id="saveCategoryData" value="Save changes" style="" >
            </div>
        </div>
    </div>
</div>
<!-- End Add category Modal-->

<!-- Start Category delete Modal-->
<div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete school</h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary categoryDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Category delete Modal -->

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/exam-category-management.js"></script>
<script type="text/javascript">
    $(function () {
        
        $('select#ExaminationCategoryLimit, select#ExaminationCategoryExaminationTypeId, input#ExaminationCategorySearch, input#ExaminationCategoryOrder', '.filters').bind("change", changePaginator); // orange
        
        $('table.dataTable thead .table-heading').click(function(){
            var parentScop = this;
            var orderName = $(this).attr('data-order');
            var orderType = ($(this).attr('data-order-type') == 'asc')?'desc':'asc';
            
            $('#ExaminationCategoryOrder').val(orderName);
            $('#ExaminationCategoryOrderType').val(orderType);
            
            $('table.dataTable thead .table-heading').each(function(index, element){
                $(element).removeClass('sorting_'+$(parentScop).attr('data-order-type')).addClass('sorting');
            });
            $(this).removeClass('sorting').addClass('sorting_'+orderType);
            $(this).attr('data-order-type', orderType);
            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#ExaminationCategorySearch', '.filters').val('');
        $('select#ExaminationCategoryLimit', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var newurl = base+'examination_managements/categories';
        
        if (passed.parent_id !== undefined)
            newurl += '/parent_id:'+passed.parent_id;
        
        var searchData = $('input#ExaminationCategorySearch', '.filters').val();
        // if (passed.search !== undefined){
        //     newurl += '/search:'+passed.search;
        // }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }
        
        var typeId = $('select#ExaminationCategoryExaminationTypeId', '.filters').val();
        if (typeId)
            newurl += '/exam_type:'+$('select#ExaminationCategoryExaminationTypeId', '.filters').val();
        
        if ($('select#ExaminationCategoryLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#ExaminationCategoryLimit', '.filters').val();
        if ($('input#ExaminationCategoryOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#ExaminationCategoryOrder', '.filters').val();
        if ($('input#ExaminationCategoryOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#ExaminationCategoryOrderType', '.filters').val();

        window.location = newurl;
    }
    
    function setAvailability(categoryId){
        console.log(categoryId);
        
//        if(categoryId){
//            var jqxhrAvailability = $.ajax({
//                type: "POST",
//                url: BASEURL + 'examination_managements/getAvailability',
//                data:{categoryId:categoryId},
//                success: function (response) {
//                    //console.log(response);
//                    if(response != ''){
//                        $('input:radio[value='+ response +']:nth(0)').prop('checked', true);
//                    }else{
//                        $('input:radio:checked').prop('checked', false);
//                    }
//                }
//            });
//        }else{
//            $('input:radio:checked').prop('checked', false);
//        }
    }
    
</script>