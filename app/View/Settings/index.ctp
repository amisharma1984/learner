<?php
$settingsData = (!empty($this->request->data['Setting']))?$this->request->data:array(); 
?>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Settings'), "link" => array('community' => Configure::read('Community.slug'), 'controller' => 'settings', 'action' => 'index'))
        ), true);
?>
<?php //echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-6 col-sm-12 col-xs-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-cog"></i>School Settings</h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <div class="row" id="SchoolSettingsMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                <?php //pr($this->request->data); ?>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'settings', 'action' => 'save'),
                    'id' => 'saveSchoolSettingsForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                
                echo $this->Form->input('Setting.for', array('type' => 'hidden', 'value' => 'School', 'id' => 'SettingForSchool'));
                ?>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationFee" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left">School registration fee</label>
                    <div class="col-md-12 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Setting.school_registration_fee', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="SettingSchoolRegistrationValidity" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left">School registration validity</label>
                    <div class="col-md-12 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Setting.school_registration_validity', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <small>Validity should be in number of day's.</small>
                    </div>
                    
                </div>
                <div class="form-group">
                    <label for="SettingSchoolEntryLimit" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left">School entry limit</label>
                    <div class="col-md-12 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Setting.school_entry_limit', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <small>'0' indicates no limit.</small>
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <!--/span-->
    
    <div class="box col-md-6 col-sm-12 col-xs-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-cog"></i>Examination Settings</h2>

                <div class="box-icon">
                   <!-- <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <div class="row" id="ExaminationSettingsMessage" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'settings', 'action' => 'save'),
                    'id' => 'saveExaminationSettingsForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                echo $this->Form->input('Setting.for', array('type' => 'hidden', 'value' => 'Examination', 'id' => 'SettingForExamination'));
                ?>
                <div class="form-group">
                    <label for="SettingExaminationSections" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left">Number of sections</label>
                    <div class="col-md-12 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Setting.examination_sections', array('type' => 'number', 'label' => false, 'div' => false, 'class' => 'form-control check-integer', 'autocomplete' => 'off', 'min' => 1, 'step' => 1, 'required' => true)); ?>
                        <small>Minimum one section required.</small>
                    </div>
                </div>
                <div id="examSectionDetailsDiv">
                    <?php 
                    $examinationSections = $this->KeyValue->getValue('examination_sections', $this->request->data);

                    if(!empty($examinationSections)){
                        for($i = 1; $i <= $examinationSections; $i++){
                    ?>
                    <div class="form-group mb-xs" id="formGroup<?php echo $i; ?>">
                        
                        <label for="ExamSectionSettingDetails" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left" ><?php echo 'Section '.$this->CommonUtilities->convertNumberToWord($i); ?></label>
                        <div class="col-md-12 col-sm-9 col-xs-12">
                            <hr style="border-bottom: 1px solid #ccc; margin: 4px 0;">
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="SettingExamSection<?php echo $i; ?>Name" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Name</label>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo $this->Form->input('Setting.exam_section_'.$i.'_name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="SettingExamSection<?php echo $i; ?>Questions" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">No. of questions</label>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo $this->Form->input('Setting.exam_section_'.$i.'_questions', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control check-integer', 'autocomplete' => 'off', 'required' => true)); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="SettingExamSection<?php echo $i; ?>Durations" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Durations</label>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo $this->Form->input('Setting.exam_section_'.$i.'_durations', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control check-integer', 'autocomplete' => 'off', 'required' => true)); ?>
                                    <small>Duration should be in minutes.</small>
                                </div>
                            </div>
                        </div>


                    </div>

                    <?php
                        }
                    }
                    ?>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Save" class="btn btn-primary">
                    </div>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->

<!--For Template-->
<script type="text/template" id="examSettingTemplate">
    <% for(var i = 1; i <= data.SettingLocal.examination_sections; i++){ %>
        <div class="form-group mb-xs" id="formGroup<%- i %>">
            <label for="ExamSectionSettingDetails" class=" col-md-12 col-sm-3 col-xs-12 control-label text-left">Section  <%- numberInWords(i) %> </label>
            <div class="col-md-12 col-sm-9 col-xs-12">
                <hr style="border-bottom: 1px solid #ccc; margin: 4px 0;">
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label for="SettingExamSection<%- i %>Name" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Name</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input name="data[Setting][exam_section_<%- i %>_name]" class="form-control" autocomplete="off" required="required" type="text" id="SettingExamSection<%- i %>Name" value="<%- data['Setting']['exam_section_' + i + '_name'] %>" >
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="SettingExamSection<%- i %>Questions" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">No. of questions</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input name="data[Setting][exam_section_<%- i %>_questions]" class="form-control check-integer" autocomplete="off" required="required" type="text" id="SettingExamSection<%- i %>Questions" value="<%- data['Setting']['exam_section_' + i + '_questions'] %>">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <label for="SettingExamSection<%- i %>Durations" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Durations</label>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input name="data[Setting][exam_section_<%- i %>_durations]" class="form-control check-integer" autocomplete="off" required="required" type="text" id="SettingExamSection<%- i %>Durations" value="<%- data['Setting']['exam_section_' + i + '_durations'] %>">
                        <small>Duration should be in minutes.</small>
                    </div>
                </div>
            </div>
        </div>
    <% } %>
</script>

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script type="text/javascript">
    var settingsData = {};
    $('#saveSchoolSettingsForm').validate({
        rules: {
            'data[Setting][school_registration_fee]': {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                number: true
            },
            'data[Setting][school_registration_validity]': {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                number: true
            },
            'data[Setting][school_entry_limit]': {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                number: true
            }
        },
        submitHandler: function (form) {
                        
            var options = {
                success: saveSettingsData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    $('#saveExaminationSettingsForm').validate({
//        rules: {
//            'data[Setting][examination_sections]': {
//                required: {
//                    depends:function(){
//                        $(this).val($.trim($(this).val()));
//                        return true;
//                    }
//                },
//                number: true
//            }
//        },
        submitHandler: function (form) {        
            var options = {
                success: saveSettingsData  // post-submit callback 
            };
            $(form).ajaxSubmit(options);
        }
    });
    
    $('#SettingExaminationSections').on('change keyup', function(){
        
        var inputValue = $(this).val();
        if(isNaN(inputValue)){
            $(this).val('');
        }else{
            /* For checking Integer value */
            if(inputValue % 1 !== 0){
                $(this).val('');
            } else if(inputValue < 1){
                $(this).val('');
            }else{
                createSectionDetails();
            }
        }
        
    });
    
    $('.check-integer').on('change keyup', function(){
        var inputValue = $(this).val();
            inputValue = inputValue.trim();
        if(isNaN(inputValue)){
            $(this).val('');
        }else{
            /* For checking Integer value */
            if(inputValue % 1 !== 0){
                $(this).val('');
            }
        }
    });
    
    function saveSettingsData(responseText, statusText, xhr, $form) {
        
        if (responseText != '') {
            
            var response = JSON.parse(responseText);
            var className = (response.status == 'Error')?'alert-danger':'alert-success';
               
            var statusString = (response.status != 'Error')?'<span class="glyphicon glyphicon-ok-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';
            
            var htmlString = '<div class="col-md-12 col-xs-12 col-sm-12 text-center">';
            htmlString += '<div class="alert '+className+'" role="alert">';
            htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
            htmlString += '<strong>'+statusString+'</strong> '+response.message; 
            htmlString += '</div>';
            htmlString += '</div>';

            $('#' + response.settingsFor + 'SettingsMessage').html(htmlString).slideToggle();
            if(response.status != 'Error'){
                //$form.resetForm();
                var timeOutValue = parseInt(response.messageLetterCount) * parseInt(response.messageTimeOut);
                setTimeout(function () {
                    $('#' + response.settingsFor + 'SettingsMessage').slideToggle();
                }, timeOutValue);
            }
            
            createSettingsDataObject();
            
        }
        
    }
    //alert(numberInWords(125).capitalizeFirstLetter());
    
    function createSectionDetails(){
        var inputValue = $('#SettingExaminationSections').val();
        <?php if(!empty($settingsData)){ ?>
        //var jsonData = '<?php //echo json_encode($settingsData); ?>';        
        //var result = JSON.parse(jsonData);
        _.templateSettings.variable = "data";

        var template = _.template(
            $('#examSettingTemplate').html()
        );

        $('#examSectionDetailsDiv').html(template($.extend(settingsData, {"SettingLocal":{"examination_sections":inputValue}})));
        
        <?php } ?>
    }
    
    function createSettingsDataObject(){
        
        var jqxhrViewSchool = $.ajax({
            type: "POST",
            url: BASEURL + "settings/getCurrentSettingsData",
            success: function (response) {
                //console.log(response);
                if(response != ''){
                    settingsData = JSON.parse(response);                    
                }

            }
        });
        
    }
    createSettingsDataObject();
</script>