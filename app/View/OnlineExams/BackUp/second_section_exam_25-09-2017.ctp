<?php $calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'CALCULATOR'));?>
<div class="container">
	<div class="inner_field regi_main">
			<div class="prof_details">
					
				<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form_info">
											<div class="exam_instruction">
												<p style="color:#119549; font-weight:600;">Your first section exam has been done successfully. Please click below link to start second section exam.</p>
												<div class="clear_fix"></div>
											</div>
								
								<div class="row">
								<div class="col-md-6">
											<h2>Second Section Exam (Calculator allowed)</h2>
								</div>
								<div class="col-md-12">
											<div class="form-group start_exam">
										<a href="<?php echo $this->webroot.'online_exams/take_exam_second_section/'.$calculator;?>" class="btn btn-info def_btn"><span class="blink">Click here to start 2nd section exam</span></a>
											
												<div class="clear_fix"></div>
											</div>
								</div>
								
								</div>
					
				
									
									<div class="exam_instruction">
									Following rule must be followed by the student : 
									<ul class="exam_ins">
									<li>Students are to complete this test in 40 minutes.</li>
									<li>Each section is 32 questions with 40 minutes allowed for each section.</li>
									<li>If once you start exam there will be a timer displayed, counting down the 40 minutes,</li>
									<li>When there is 20 minutes left and again when there is only 10 minutes remaining. Students will be able to go back and change their answers in the section they are currently attempting.</li>
									<li>However once the 40 minutes is finished the section will be automatically closed and the students answer choices will be saved in the system and they cannot go back to it. </li>
									
									
									
									<li> Make sure you have a good internet  connection</li>
									<li>If you are taking the exam late in the day,  it  is recommended  that  you  reboot your  computer before beginning  to free up memory resources  from  other  programs  on   your  computer.</li>
									<li>Shut down all Instant Messaging tools (Skype,  AIM,  MSN  Messenger) and Email programs as they can conflict  with  Blackboard.</li>
									<li> Maximize your browser window before  starting the  test.</li>
									<li> Minimizing the browser window during the exam can prevent the submission of your exam. </li>
									</ul>
								
								
									<br>
									<br>
									</div>
									
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group start_exam">
										<a href="<?php echo $this->webroot.'online_exams/take_exam_second_section/'.$calculator?>" class="btn btn-info def_btn"><span class="blink">Click here to start 2nd section exam</span></a>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
				</div>
			</div>
	</div>
</div>