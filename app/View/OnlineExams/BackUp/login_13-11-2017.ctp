<div class="container">
	<div class="inner_field regi_main">
			<?php echo $this->Session->flash();?>
			<div class="prof_details online_exm_lg">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); 
							 // echo $this->Form->create('Student'); 
							  echo $this->Form->create('Student', array('onsubmit' => 'return validation()')); 
							 ?>
							
							 
							<h2>Sign in for school online exam </h2>
							<div class="form_cont">
							 <?php if(!empty($exceedErr)){?>
							  <div class="exceedErr">
							  <span  class="rgerror"><img src="<?php echo $this->webroot.'img/error_icon.png';?>">
							  <?php echo @$exceedErr;?> </span>
							  </div>
							<?php }?>
							
							<div class="form-group">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<?php echo $this->Form->input('first_name', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'Student First Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="fnerror"><?php echo @$firstNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<?php echo $this->Form->input('last_name', array('label' => false, 'autocomplete' => 'off',  'placeholder' => 'Student Last Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="lnerror"><?php echo @$lastNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								
								<?php echo $this->Form->input('school_name', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'School Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="scerror"><?php echo @$schoolNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<?php echo $this->Form->input('examination_category_id', array('label' => false, 'div' => false, 'empty' => 'Select Year group',  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $yearGroupOption))?>
								<span  class="rgerror" id="yrgrpErr"><?php //echo @$schoolNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								
								<?php echo $this->Form->input('teacher_name', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'Class or Teacher Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="tearcherErr"><?php echo @$teacherNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							
							<div class="form-group">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<?php echo $this->Form->input('email', array('label' => false,'type' => 'text', 'autocomplete' => 'off',  'placeholder' => 'Student School Email', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="emerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('exam_code', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'School Exam Code', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="examCodeErr"><?php echo @$examCodeErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
				
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Login</button>
							</div>
							</div>
						
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

<script>
function validation(){ 
	var result=true;
	var exam_code  = $.trim($('#StudentExamCode').val());
	var year_group = $.trim($('#StudentExaminationCategoryId').val());
	
	if($.trim($('#StudentFirstName').val()) == ""){
		 $('#fnerror').html('Please enter first name');
		 result=false;
	} else {
		 $('#fnerror').html(''); 
	}
	if($.trim($('#StudentLastName').val()) == ""){
		 $('#lnerror').html('Please enter last name');
		 result=false;
	} else {
		 $('#lnerror').html('');
	}
	
	if($.trim($('#StudentSchoolName').val()) == ""){
		 $('#scerror').html('Please enter school name');
		 result=false;
	} else {
		 $('#scerror').html('');
	}
	
	if($.trim($('#StudentExaminationCategoryId').val()) == ""){
		 $('#yrgrpErr').html('Please select year group');
		 result=false;
	} else if(year_group != "" && exam_code != ""){
			$.ajax({
				url : '<?php echo $this->webroot;?>/online_exams/check_year_group_vallid',
				type : 'POST',
				async: false,
				data : {exam_code : exam_code, year_group : year_group},
				success : function(responce){
					
					if(responce == 'UNVALLID'){
						$('#yrgrpErr').html('The exam code you have used, does not correspond to your year. Check with your Head Teacher');
						result=false;
					} else {
						$('#yrgrpErr').html('');
					}
				}
				
			});
		 
		} else {
			$('#yrgrpErr').html('');
		}
	
	if($.trim($('#StudentTeacherName').val()) == ""){
		 $('#tearcherErr').html('Please enter class or teacher name');
		 result=false;
	} else {
		 $('#tearcherErr').html('');
	}
	
	var email = $.trim($('#StudentEmail').val());
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	
	if($.trim($('#StudentEmail').val()) == ""){
		 $('#emerror').html('Please enter email');
		 result=false;
	} else if(email != '' && !pattern.test(email)){
		 $('#emerror').html('Please enter valid email format');
		 result=false;
	} else if(email != '' && pattern.test(email)){
		$.ajax({
			url : '<?php echo $this->webroot;?>/online_exams/check_email_exist_online_exam',
			type : 'POST',
			async: false,
			data : {email : email},
			success : function(responce){
				if(responce >=1){
					$('#emerror').html('This email has been used, check with your Head Teacher');
					result=false;
				} else {
					$('#emerror').html('');
				}
			}
			
		});
		 
	} else {
		 $('#emerror').html('');
	}
	
	
	if($.trim($('#StudentExamCode').val()) == ""){
		 $('#examCodeErr').html('Please enter school exam code');
		 result=false;
	} else if($.trim($('#StudentExamCode').val()) != ""){
			$.ajax({
				url : '<?php echo $this->webroot;?>/online_exams/check_exam_code_vallid',
				type : 'POST',
				async: false,
				data : {exam_code : exam_code},
				success : function(responce){
					if(responce <=0){
						$('#examCodeErr').html('Please enter valid exam code');
						result=false;
					} else {
						$('#examCodeErr').html('');
					}
				}
				
			});
		 
		} else {
		 $('#examCodeErr').html('');
		}
	
	return result;
	
 }
</script>
