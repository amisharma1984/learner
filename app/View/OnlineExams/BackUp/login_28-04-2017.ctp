<div class="container">
	<div class="inner_field regi_main">
			<?php echo $this->Session->flash();?>
			<div class="prof_details online_exm_lg">
				<div class="row">
					<div class="col-md-5 col-md-offset-3">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); 
							  echo $this->Form->create('Student'); 
							 ?>
							
							 
							<h2>Sign in for school online exam </h2>
							 <?php if(!empty($exceedErr)){?>
							  <div class="exceedErr">
							  <span  class="rgerror"><img src="<?php echo $this->webroot.'img/error_icon.png';?>">
							  <?php echo @$exceedErr;?> </span>
							  </div>
							<?php }?>
							
							<div class="form-group">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<?php echo $this->Form->input('first_name', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'First Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$firstNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-graduation-cap" aria-hidden="true"></i>
								<?php echo $this->Form->input('last_name', array('label' => false, 'autocomplete' => 'off',  'placeholder' => 'Last Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$lastNameErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<?php echo $this->Form->input('email', array('label' => false, 'autocomplete' => 'off',  'placeholder' => 'Student Email/ Student ID', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('exam_code', array('label' => false, 'autocomplete' => 'off', 'placeholder' => 'Exam Code ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$examCodeErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
				
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Login</button>
							</div>
						
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
