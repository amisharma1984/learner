<div class="container">
	<div class="inner_field regi_main">
			<div class="prof_details">
				<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form_info">
								
									<h2 style="background:white; color:#000">Exam Completed</h2>
								
									<div class="row">
									<div class="exam_instruction">
									<p style="margin:0 0 33px 60px;">
									Thank you for your participation in this exam. Your result will be given to you by your School Head Teacher.
									</p>
										<div class="clear_fix"></div>
									</div>
									</div>
									
								</div>
							</div>
				</div>
			</div>
	</div>
</div>