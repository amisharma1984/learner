<?php  
if($CalculatorQuestionCount > 0 && $NonCalculatorQuestionCount > 0){
	$content = 'Calculator not allowed';
	$examFlag = 'BothExam';
	$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'NON-CALCULATOR'));
} else if($CalculatorQuestionCount == 0 && $NonCalculatorQuestionCount > 0){
	$content = 'Calculator not allowed';
	$examFlag = 'SingleExam';
	$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'NON-CALCULATOR'));
} else if($CalculatorQuestionCount > 0 && $NonCalculatorQuestionCount == 0){
	$content = 'Calculator allowed';
	$examFlag = 'SingleExam';
	$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'CALCULATOR'));
}  else if($CalculatorQuestionCount == 0 && $NonCalculatorQuestionCount == 0){
	$content = '';
	//$examFlag = 'SingleExam';
	$examFlag = 'NoneOfBothExam';
	$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'nothing'));
} else {
	//nothing
}


$examFlag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $examFlag));

?>

<div class="container">
	<div class="inner_field regi_main">
			<div class="prof_details">
				<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form_info">
								
								
								<div class="row">
								<div class="col-md-12">
											<h2>Rules of the Exam <?php echo !empty($content) ? '('.$content.')' : '';?></h2>
								</div>
								<!--<div class="col-md-6">
											<div class="form-group start_exam">
										<a href="<?php //echo $this->webroot.'online_exams/take_exam/'.$calculator_non_calculator.'/'.$examFlag;?>" class="btn btn-info def_btn"><span class="blink">Click here to start exam</span></a>
											
												<div class="clear_fix"></div>
											</div>
								</div>-->
								
								</div>
					
				
									
									<div class="exam_instruction">
									 
									<ul class="exam_ins">
									<li>Students are to complete this test in the allocated time, displayed in the timer.</li>
									<li>Students are able to change their answers at any time during exam. </li>
									<li>When the exam time is finished, the section will automatically be closed and student answer choices will be saved in system and they cannot go back to it </li>
									<!--<li>When there is 20 minutes left and again when there is only 10 minutes remaining. Students will be able to go back and change their answers in the section they are currently attempting.</li>-->
									</ul>
									<br>
									</div>
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group start_exam">
										<a class="btn btn-info def_btn" style="text-transform:none; cursor:text;"><span class="blink___" >Head Teacher/ Supervisor Guidelines</span></a>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									
									
									<div class="exam_instruction">
									<ul class="exam_ins">
									<li> Make sure you have a good internet  connection.</li>
									<li>If you are taking the exam late in the day, it is recommended to the computer before beginning to free up memory resources from other programs on your computer.</li>
									<li>Shut down all Instant Messaging tools (Skype,  AIM,  MSN  Messenger) and Email programs as they can conflict with Blackboard.</li>
									<li>Ensure student maximize the browser window before starting the test.</li>
									<li> Minimizing the browser window during the exam can prevent submission of student's exam. </li>
									</ul>
								
								
									<br>
									<br>
									</div>
									
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group start_exam">
										<a href="<?php echo $this->webroot.'online_exams/take_exam/'.$calculator_non_calculator.'/'.$examFlag;?>" class="btn btn-info def_btn"><span class="blink___">Click here to start exam</span></a>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
				</div>
			</div>
	</div>
</div>