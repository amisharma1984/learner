<?php  
$exam_section_id1 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 1)); //Reading booklet questions

$exam_section_id2 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 2)); // Language Conventions

$exam_section_id3 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 3));  //Writing Leaflet


?>

<div class="container">
	<div class="inner_field regi_main">
			<div class="prof_details">
				<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form_info">
								
								
								<div class="row">
								<div class="col-md-12">
											<h2>Rules of the Exam</h2>
								</div>
								</div>
					
				
									
									<div class="exam_instruction">
									 
									<ul class="exam_ins">
									<li>Students are to complete this test in the allocated time, displayed in the timer.</li>
									<li>Students are able to change their answers at any time during exam. </li>
									<li>When the exam time is finished, the section will automatically be closed and student answer choices will be saved in system and they cannot go back to it </li>
									
									<!--<li>When there is 20 minutes left and again when there is only 10 minutes remaining. Students will be able to go back and change their answers in the section they are currently attempting.</li>-->
									
									</ul>
									<br>
									</div>
									
										<div class="row">
										<div class="col-md-12">
											<div class="form-group start_exam">
										<a class="btn btn-info def_btn" style="text-transform:none; cursor:text;"><span class="blink___" >Head Teacher/ Supervisor Guidelines</span></a>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									
								<div class="exam_instruction">
									<ul class="exam_ins">
									<li> Make sure you have a good internet  connection.</li>
									<li>If you are taking the exam late in the day, it is recommended to the computer before beginning to free up memory resources from other programs on your computer.</li>
									<li>Shut down all Instant Messaging tools (Skype,  AIM,  MSN  Messenger) and Email programs as they can conflict with Blackboard.</li>
									<li>Ensure student maximize the browser window before starting the test.</li>
									<li> Minimizing the browser window during the exam can prevent submission of student's exam. </li>
									</ul>
								
								
									<br>
									<br>
									</div>
									
									
									<div class="row">
										<div class="col-md-12">
											<div class="form-group start_exam">
									<a href="#" data-toggle="modal" class="btn btn-info def_btn" data-target="#OnlineLiteracyExamPopUp"><span class="blink___">Click here to start exam</span></a>		
											
											
										<!--<a href="<?php echo $this->webroot.'online_exams/take_exam/'.$calculator_non_calculator.'/'.$examFlag;?>" class="btn btn-info def_btn"><span class="blink___">Click here to start exam</span></a>-->
										
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
				</div>
			</div>
	</div>
</div>



<div class="modal fade" id="OnlineLiteracyExamPopUp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Literacy Online Trial Exam:</h4>
            </div> 
            <div class="modal-body">
			
			<?php if(empty($studentDetails['Student']['attemt_qs_1']) || $studentDetails['Student']['attemt_qs_1'] != 1){?>
			<a href="#" target="_blank" onclick="javascript:window.location.assign('<?php echo $this->webroot.'online_exams/take_literacy_exam/'.$exam_section_id1;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Reading booklet questions</a>
			<?php }?>
			
			<?php if(empty($studentDetails['Student']['attemt_qs_2']) || $studentDetails['Student']['attemt_qs_2'] != 2){?>
			<a href="#"  target="_blank" onclick="javascript:window.location.assign('<?php echo $this->webroot.'online_exams/take_literacy_exam/'.$exam_section_id2;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Language Conventions</a>
			
			<?php }?>
			
			
			<?php if(empty($studentDetails['Student']['attemt_qs_3']) || $studentDetails['Student']['attemt_qs_3'] != 3){?>
			<a href="#"  target="_blank" onclick="javascript:window.location.assign('<?php echo $this->webroot.'online_exams/take_literacy_exam/'.$exam_section_id3;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Writing Leaflet</a>
           	<?php }?>
			
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>	
	