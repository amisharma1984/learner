<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('parent_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
				<h2 style="text-align:center; color:#119548;"><?php echo @$parentDetails['StudentParent']['first_name'];?></h2>
                <h2>Purchase Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
     <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'student_parents', 'action' => 'checkout'), true))); ?>            
<table class="student-list-apr">
  <tr>
   <th>List</th>
   <th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
    <th>Paper</th>
	<th style="text-align:right;">Price($ AUD)</th>
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  $i=1;
	  foreach($allExamAr as $val){
		    $category = explode(' ',$val['ExaminationCategory']['name']);
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		<tr>
			<td><?php echo $i?></td>
			<td><?php echo $category[1]; ?></td>
			<td>NAPLAN Style Practice<?php //echo $val['ExaminationCategory']['name'];?></td>
			<td><?php echo $val['Examination']['title'];?></td>
			<td><?php echo $val['Examination']['paper'];?></td>
			<input type="hidden" value="<?php echo $val['Examination']['id'];?>" name="ids[]">

			<td style="text-align:right;">
			<input id="total_price_<?php echo $val['Examination']['id'];?>" value="<?php echo $val['Examination']['price'];?>" class="total_price"  type="text" size="8" name="total_price[]" readonly>
			</td>
		</tr>
<?php
$i++;
	  }
  }
?>
  
</table>
               <input type="hidden" value="purchase_exam_payment" name="page_name">
				<div class="exam_pay_div">
				<div class="gross_total">
					<label style="line-height:30px">Sub Total ($ AUD) :</label> <input id="gross_total" name="sub_total"  value="<?php echo $subTotal;?>"  readonly type="text" size="10"></br>
					<label style="margin-left:1px;line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GST (10%) :</label> <input id="gst_10" name="gross_total" value="<?php echo $gst;?>" readonly type="text" size="10"></br>
					<label style="line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total ($ AUD) :</label> <input id="total_with_gst" name="gross_total" value="<?php echo $total;?>" readonly type="text" size="10">
				</div>
			
				<div class="exam_chkout">
					<input type="submit" class="btn btn-info def_btn"  value="Make Payment">
				</div>
				
				
			</div>
			
			<?php echo $this->Form->end(); ?>

				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>