<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
  <style>

fieldset.scheduler-border {
    border: 1px groove #8fbda3 !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 16px !important;
    font-weight: bold !important;
    text-align: left !important;
	display: inline-block!important;
width: auto!important;
padding: 9px!important;
margin-bottom: 4px !important;
border-bottom: none !important;
font-family: "Roboto Slab",serif;
color: #1f7d47;
	
}
</style>
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('parent_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Family Profiles</h2>
                  <h3> <?php echo ucwords($parentDetails['StudentParent']['first_name']).' '.ucwords($parentDetails['StudentParent']['last_name']); ?></h3>
                 <!-- <span>year 9</span>-->
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'StudentParents', 'action' => 'myprofile'), true))); 
				// echo $this->Form->create('StudentParent'); 
				 echo $this->Form->create('StudentParent', array('name' => 'StudentParentUpdate', 'onsubmit' => 'return validation()')); 
				 ?>
				 
				 <div class="row">
					 <div style="float:right;color:#f69d25;margin:0 18px 20px 0">Last Logged In : 
						 <?php 
						 if(!empty($lastLoginParent)){
							  echo date("D, jS \of F Y, h:i:s A", strtotime($lastLoginParent));
						 }
						 ?>
					 </div>
				 </div>
				 
				 
				  <div class="row">
					 <div style="float:right;color:#f69d25;margin:0 18px 20px 0">
						 <button type="button" class="btn btn-secondary btn-lg btn-block"  data-toggle="modal" data-target="#AddMoreChildren">Add more children</button>
					 </div>
				 </div>
				 
				 
				 
				
							<div class="col-md-offset-1___ col-md-12">
								<div class="form_info">
									<div class="form_cont__">
									
								<fieldset class="scheduler-border">
								<legend class="scheduler-border">Parent Details</legend>
						
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label >Parent's First name</label>
													<?php echo $this->Form->input('first_name', array('label' => false, 'required' => 'required',  'placeholder' => "Parent's First name", 'class' => 'form-control inp_text'))?>
                                                    <span  class="prof_err" id="fnerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<label >Parent's Last name</label>
													<?php echo $this->Form->input('last_name', array('label' => false, 'required' => 'required',  'placeholder' => "Parent's Last name",  'class' => 'form-control inp_text'))?>
												<span  class="prof_err" id="lnerror"><?php echo @$lnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<label >Address</label>
												<?php echo $this->Form->input('address', array('type' => 'text','label' => false,'required' => 'required','autocomplete' => 'off', 'class' => 'form-control inp_text', 'placeholder' => 'Address'))?>
                                              <span  class="prof_err" id="adderror"><?php echo @$adderror;?> </span>
												
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<label >Email</label>
												<?php echo $this->Form->input('email', array('label' => false, 'type' => 'text','autocomplete' => 'off',  'readonly' => 'readonly', 'class' => 'form-control inp_text'))?>
												<span  class="prof_err" id="emerror"><?php echo @$emerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
							</fieldset>		
									
									
									
									
									
	<?php 
	//for($i =1; $i<= $no_of_child; $i++){
		if(!empty($this->data['Student'])){
			$i = 1;
			foreach($this->data['Student'] as $val){
	?>	
	 <?php echo $this->Form->input('Student.id', array('label' => false,'type' => 'hidden','name' => 'data[Student][id][]', 'value' => $val['id'], 'div' => false))?>

	
	<fieldset class="scheduler-border">
    <legend class="scheduler-border">Child <?php echo $i;?></legend>							
<div class="row">
	<div class="col-md-6">
		<div class="form-group">  
		<label><?php echo 'Child '.$i.' Year';?></label>
		  <?php echo $this->Form->input('Student.examination_category_id', array('label' => false,'disabled' => 'disabled','id' => 'childYear_'.$i,'name' => 'data[Student][examination_category_id][]', 'value' => $val['examination_category_id'], 'div' => false, 'empty' => 'Child '.$i.' Year',  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $yearGroupOptionPractice))?>

			<?php //echo $this->Form->input('class', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Class -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
			<span  class="prof_err"  id="childYrErr_<?php echo $i;?>"><?php //echo @$fnerror;?> </span>
			<div class="clear_fix"></div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		<label><?php echo 'Child '.$i.' School name';?></label>
			<?php echo $this->Form->input('Student.school_name', array('label' => false,'required' => 'required','id' => 'childSchoolNane_'.$i,'name' => 'data[Student][school_name][]', 'value' => $val['school_name'], 'class' => 'form-control inp_text'))?>
			<span  class="prof_err" id="childSNERR_<?php echo $i;?>"><?php //echo @$scerror;?> </span>
			<div class="clear_fix"></div>
		</div>
	</div>
</div>						
								
								
<div class="row">
		<div class="col-md-6">
			<div class="form-group"> 
			<label><?php echo 'Child '.$i.' First Name';?></label>
				<?php echo $this->Form->input('Student.first_name', array('type' => 'text','id' => 'childFirstNane_'.$i, 'name' => 'data[Student][first_name][]','label' => false,'required' => 'required','autocomplete' => 'off', 'class' => 'form-control inp_text', 'value' => $val['first_name']))?>
			  <span  class="prof_err" id="childFNERR_<?php echo $i;?>"><?php //echo @$adderror;?> </span>
				
				<div class="clear_fix"></div>
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="form-group">
			<label><?php echo 'Child '.$i.' Last Name';?></label>
				<?php echo $this->Form->input('Student.last_name', array('label' => false,'required' => 'required','id' => 'childLastNane_'.$i,'name' => 'data[Student][last_name][]', 'type' => 'text','autocomplete' => 'off', 'value' => $val['last_name'], 'class' => 'form-control inp_text'))?>
				<span  class="prof_err" id="childLNERR_<?php echo $i;?>"><?php //echo @$emerror;?> </span>
				<div class="clear_fix"></div>
			</div>
			
		</div>
	</div>
	</fieldset>
	<?php $i++;
			}
		}
	?>					
									

									
<div class="row">
	<div class="col-md-6">
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<input type="submit" class="btn btn-info def_btn" value="Submit">
			<div class="clear_fix"></div>
		</div>
	</div>
</div>
									
			</div>
		</div>
	</div>
	

<?php echo $this->Form->end(); ?>
						



						
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	
	<div class="modal fade" id="AddMoreChildren" tabindex="-1" role="dialog" aria-labelledby="AddMoreChildren" aria-hidden="true">
    <div class="modal-dialog" style="width:500px !important;;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Add More Children</h4>
            </div> 
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block">How many children do you want to add more? </button>
                 <div class="childreen-cls" style="margin-left:135px !important;;">
				  <select class="selectpicker show-tick" id="select_children">
				  <option value="">Select</option>
				  <option value="1">1 child</option>
				  <option value="2">2 children</option>
				  <option value="3">3 children</option>
				  <option value="4">4 children</option>
				  <option value="5">5 children</option>
				  <option value="6">6 children</option>
				  </select>
				  <a id="put_chaildren_val" href="" class="btn closebut btn-default">GO</a>
				 </div>
                  
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
	
	
	
	
	
	<script>
function validation(){ 
	var result=true;
	if($.trim($('#StudentParentFirstName').val()) == ""){
		 $('#fnerror').html('Please enter first name');
		 result=false;
	} else {
		 $('#fnerror').html('');
	}
	if($.trim($('#StudentParentLastName').val()) == ""){
		 $('#lnerror').html('Please enter last name');
		 result=false;
	} else {
		 $('#lnerror').html('');
	}
	
	
	
	return result;
	
 }
 
 
 $('select').on('change', function() {
		  var nos = $(this).val();
		  
		  var url = '<?php echo $this->webroot.'student_parents/add_more_children/';?>'+ nos;
		  $('#put_chaildren_val').attr('href', url);
	})
 
 
</script>
