<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); 
							  echo $this->Form->create('StudentParent'); 
							 
							 ?>
                       
							<h2>Parent Sign In</h2>
							<div class="form_cont">
							<div class="form-group">
								<i class="fa fa-user" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentParent.login', array('label' => false,  'placeholder' => 'Username', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentParent.password', array('label' => false,  'placeholder' => 'Password ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginPassErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="checkbox form-group">
								<label><?php //echo $this->Form->input('StudentParent.remember', array('type' => 'checkbox', 'label' => false, 'div' => false, 'id' => 'remember')); ?> <!--Remember me--></label>
								<a href="<?php echo $this->Html->url(array('controller'=>'student_parents','action' => 'forgot_password')) ?>" class="formbtn forget">Forgot Password?</a><br>
							</div>
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn" >Login</button>
							</div>
							<div class="form-group">
								Don’t have an account? <a href="#" data-toggle="modal" data-target="#studentParentsLoginPop"> Sign Up!</a>
								
							</div>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
                                  
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>