	<script>
function expressPaypal(){
		$('#expressPaypal').show();
		$('.card_but').addClass('active');
			
		$('#normalPaypal').hide().removeClass('active');
		$('.pay_but').removeClass('active');
}
function normalPaypal(){
		$('#normalPaypal').show();
		$('.pay_but').addClass('active');
	
		$('#expressPaypal').hide();
		$('.card_but').removeClass('active');
}
</script>

<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('parent_dashboard_left');?>
		 
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Payment Information</h2>
                  <h3> <?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span><a class="btn_paypal btn def_btn active card_but"   role="button" onclick="expressPaypal();">Credit Card</a> 
				  <a class="btn_paypal btn def_btn pay_but"   role="button" onclick="normalPaypal();">PayPal</a></span>
                </div>
				
				
                <div class="prof_details prof_form" id="expressPaypal">
                    
                   <form action="" method="post" id="payment_info_validate" >
	                  <div class="row">		  
                       <div class="col-md-8 form_info">

		                       <!-- <div class="form-group">
		                        <label >Version :</label>
		                        						
								<div class="clear_fix"></div>
		                       </div> -->
		                       <div class="form-group">
		                         <label >Payable amount ($ AUD)</label>
		                         <input type="text" readonly="readonly" name="order[amount]" value="<?php echo $gross_total; ?>" size="8" maxlength="13" class="form-control inp_text"/>					
								 <div class="clear_fix"></div>

		                       </div>
		                       <input type="hidden" readonly="readonly" name="version" value="34" size="8" maxlength="80" class="form-control inp_text" />
		                        <input type="hidden" name="order[currency]" value="AUD"  size="8" maxlength="3" class="form-control inp_text"/>
                                <input type="hidden" name="orderId" value="<?php echo $transOrderId['orderId'] ?>" size="20" maxlength="60" class="form-control inp_text" />	
                                <input type="hidden" name="transactionId" value="<?php echo $transOrderId['transactionId'] ?>" size="20" maxlength="60" class="form-control inp_text"/>
                                 <input type="hidden" readonly="readonly" name="apiOperation" value="PAY" size="20" maxlength="80" class="form-control inp_text"/>
                                 <input type="hidden" name="sourceOfFunds[type]" value="CARD" size="19" maxlength="80" class="form-control inp_text" />	
		                        <!-- <div class="form-group">
		                         <label >Method</label>
		                         <input type="text" readonly="readonly" name="method" value="PUT" size="20" maxlength="80" class="form-control inp_text"/>					
								 <div class="clear_fix"></div>
		                        </div> -->
		                        <div class="form-group">
		                         <label >Credit Card Number*</label>
		                         <input type="text" name="sourceOfFunds[provided][card][number]" value="" size="19" maxlength="80" class="form-control inp_text" required="required"/>					
								 <div class="clear_fix"></div>
								  <span  class="error"><?php echo @$card_err ;?> </span>
		                        </div>
                                
					  
		                      <div class="form-group">
		                        <label class="exp_lbl">Expiry Date * :</label>
								
						    
					        <div class="cal_month">					
								<select class="selectpicker___ show-tick form-control inp_text" name="sourceOfFunds[provided][card][expiry][month]" required="required">
								<option value="">Month</option>
								<option  value="01" <?php echo (!empty($exp_month) && $exp_month == 1) ? 'selected' : ' ';?> >January</option>
								<option  value="02" <?php echo (!empty($exp_month) && $exp_month == 2) ? 'selected' : ' ';?>>February</option>
								<option  value="03" <?php echo (!empty($exp_month) && $exp_month == 3) ? 'selected' : ' ';?>>March</option>
								<option  value="04" <?php echo (!empty($exp_month) && $exp_month == 4) ? 'selected' : ' ';?>>April</option>
								<option  value="05" <?php echo (!empty($exp_month) && $exp_month == 5) ? 'selected' : ' ';?>>May</option>
								<option  value="06" <?php echo (!empty($exp_month) && $exp_month == 6) ? 'selected' : ' ';?>>June</option>
								<option  value="07" <?php echo (!empty($exp_month) && $exp_month == 7) ? 'selected' : ' ';?>>July</option>
								<option  value="08" <?php echo (!empty($exp_month) && $exp_month == 8) ? 'selected' : ' ';?>>August</option>
								<option  value="09" <?php echo (!empty($exp_month) && $exp_month == 9) ? 'selected' : ' ';?>>September</option>
								<option  value="10" <?php echo (!empty($exp_month) && $exp_month == 10) ? 'selected' : ' ';?>>October</option>
								<option  value="11" <?php echo (!empty($exp_month) && $exp_month == 11) ? 'selected' : ' ';?>>November</option>
								<option  value="12" <?php echo (!empty($exp_month) && $exp_month == 12) ? 'selected' : ' ';?>>December</option>
								</select>	
					        </div>
					    <div class="cal_year">
				      	<select class="selectpicker__ show-tick form-control inp_text" name="sourceOfFunds[provided][card][expiry][year]" required="required">
						<!--<select class="form-control yrmnth" name="data[Payment][exp_year]">-->
							<option value="">Year</option>
							<?php
							$currYr = date('Y')+20;
							$initialYr = date('Y');
							for($i = $currYr; $i>= $initialYr; $i--){
							?>
							<option <?php if(!empty($year) && $year == $i){ echo 'selected';}?> value="<?php echo substr($i,2,4);?>"><?php echo $i;?></option>
							<?php }?>
							 
							</select>
				     	</div>
					
                        <div class="clear_fix"></div>
						 <span  class="error"><?php echo @$exp_month_err;?> </span>
						
                      </div>
                                <!-- <div class="form-group">
		                         <label >card.expiry.month </label>
		                         <input type="text" name="sourceOfFunds[provided][card][expiry][month]" value="" size="1" maxlength="2" class="form-control inp_text"/>					
								 <div class="clear_fix"></div>
		                        </div>

		                        <div class="form-group">
		                         <label >card.expiry.year </label>
		                         <input type="text" name="sourceOfFunds[provided][card][expiry][year]" value="" size="1" maxlength="2" class="form-control inp_text"/>			
								 <div class="clear_fix"></div>
		                        </div> -->

		                        <div class="form-group">
		                         <label >Security Code *</label>
		                         <input type="password" name="sourceOfFunds[provided][card][securityCode]" value="" size="8" maxlength="4" class="form-control inp_text" required="required" />
								 <div class="clear_fix"></div>
								  <span  class="error"><?php echo @$security_err;?> </span>
		                        </div>	

		                        <div class="form-group">
		                         <label >Name on Card* </label>
		                         <input type="text" name="nameofcard" value="" class="form-control inp_text" required="required" />
								 <div class="clear_fix"></div>
								  <span  class="error"><?php echo @$name_err;?> </span>
		                        </div>	                      
		                        
		                        <div class="form-group">
		                        <label > &nbsp;</label> <input type="submit"  name="creditsubmit" class="btn def_btn" value="Submit">
		                        <div class="clear_fix"></div>
                                </div>

                        </div>
                      </div>
	                </form>  
                
                </div>
				
				
  <div class="" id="normalPaypal" style="display:none;">
 <?php
        if(!empty($paypalFormSettings)){
			//$paypalFormSettings['amount'] = $val['Examination']['price'];
			//$paypalFormSettings['custom'] = $studentDetails['Student']['id'].'##'.$val['Examination']['id'];
			
			//$paypalFormSettings['item_name'] = $val['Examination']['title'].'('.$val['ExaminationType']['name'].'/'.$val['ExaminationCategory']['name'].')';
            $formAction = $paypalFormSettings['payment_url'];
            //unset($paypalFormSettings['payment_url']);
            
            echo $this->Form->create(null, array('url' => $formAction, 'id' => 'payPalPaymentForm'));
            
            foreach ($paypalFormSettings as $key => $value){
                echo '<input type="hidden" name="'.$key.'" value="'.$value.'" >';
            }
			?>
			<input type="image" width="250" src="http://<?php  echo $_SERVER['HTTP_HOST'].''.$this->webroot;?>img/paypalexpress.png" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
			<?php
            echo $this->Form->end();
        }
        ?>
  </div>
				
				
				
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>