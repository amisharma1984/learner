<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>

    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>NEXT &#187;</a>");
			  } else {
				    $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function save_next(){
			
			
				
			  var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
					$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
				 /*var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
										$('#attempted_qs').text(numberOfCheckedRadio);
										if(percentageBar == 100){
											 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
										} else {
											  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
										}
				                       
										
									   
										$('#qs_attempted_prcessing').css({"width": ""});
										$('#qs_attempted_prcessing').css({"width": percentageBar+"%"}); */
				 
			});	
		});
		
		
		function answer_details(ans_id){
			$('#details_showhide_' + ans_id).toggle();
		}
		
    </script>


<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
       <div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){
			
			 if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				} else if(!empty($qval['StudentAnswer']['answer_text']) && $qval['StudentAnswer']['answer_id'] == 0){
					$stans = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['StudentAnswer']['answer_text'])); // remove p tag
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['Question']['CorrectAnswer'][0]['answer_text'])); // remove p tag
					
					if($stans== $corAns){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				}
				
				else {
					$qstab_color = 'unattempt_qa';
				}
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			?>
    	   </ul>
    </div>

	
	<div class="attampt_flag">
          <div class="res_point"><span class="correct_answer" ></span>Correct Answer
			<div class="square">
			<div class="sq_text"><?php echo $correctAnswer;?></div>
			</div>
		  </div>
		  
         <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer
		 <div class="square">
			<div class="sq_text_wrng"><?php echo ($totalAttemptQs - $correctAnswer);?></div>
			</div>
		 </div>
       <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted
	   <div class="square">
			<div class="sq_text_unattmpt"><?php echo ($totalqustionSet - $totalAttemptQs);?></div>
			</div>
	   </div>
		
        </div>
       
	   
	
	
	<!--<div class="question_symbol">
	<ul>
          <li>Correct Answer</li>
		   <li>Wrong Answer</li>
        <li>Not attempted</li>
		</ul>
        </div>->
	
  <!--<div class="attamp_check">
          <label>Attempted</label>
        <label>Not attempted</label>
		
        </div>
        <div class="view_all_qu">
        <a href="javascript:void(0);">View All Questions</a>
        </div>-->
</nav>
</div>



<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
	  
	  <div class="bread_crumb"><span><a href="<?php echo $this->webroot.'students/my_exams'?>">My Exam</a> /</span> View Answer</div>
	  
      <div class="content_main view_qsd">
	   <h2><?php echo @$StudentAnswerAr[0]['Question']['Examination']['title'];?></h2>
				<p><?php echo @$StudentAnswerAr[0]['Question']['Examination']['description'];?></p>
	 
	
	 
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="qusdetails" class="tabs">
  <?php
	  if(!empty($QuestionAns)){ 
		  $i = 1;
		  $k = 1;
		  foreach($QuestionAns as $qval){ 
		  if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qs_color = 'corrct_ans_qs';
					} else {
						$qs_color = 'wrong_ans_qs';
					}
				} else {
					$qs_color = 'notattempt_ans_qs';
				}
	?>
	
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
        	       <div class="qus <?php //echo $qs_color;?>"><span class="qs_num"><?php echo 'Q '.$i;?>. </span> 
				   <?php 
						if(!empty($flagA)){
							echo str_replace('../../', '../../../',$qval['Question']['title']);
						} else if(empty($flagA) && empty($flagB)){
							echo str_replace('../../', '../',$qval['Question']['title']);
						} else {
							echo $qval['Question']['title'];
						}
				  
				   ?>
				  
				   </div>  
				  
<ul class="ans_field">
 <?php 
  $j = 'a';

  foreach($qval['Question']['Answer'] as $ansval) { 
  
  if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
				$checked = 'checked';
			} else {
				$checked = ' ';
			}
		$answer_type = '<input type="checkbox" '.$checked.' value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
			if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
				$checked = 'checked';
			} else {
				$checked = ' ';
			}
		$answer_type = '<input type="radio"  '.$checked.' disabled="disabled" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="'.$k.'_ans[]">';
		
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'" readonly>'.$qval['StudentAnswer']['answer_text'].'</textarea>';
	}
	
	//echo strlen($ansval['answer_text']);
	if(strlen(strip_tags($ansval['answer_text'])) >= 20  && strpos($ansval['answer_text'], 'src="') == false) {
		$answerclass = 'big_answer';
	} else if(strpos($ansval['answer_text'], 'src="') !== false){
		$answerclass = 'big_answer';
	} else 	if(strlen(strip_tags($ansval['answer_text'])) <= 20  && strpos($ansval['answer_text'], 'src="') == false && count($qval['Question']['Answer']) >=5) {
		$answerclass = 'ans5_options';
	}
	else {
		$answerclass = 'ans_options';
	}
	
  
  
  ?>	

<li class="<?php echo ($ansval['answer_type'] == 4) ? 'ans_options_txt_area' : $answerclass;?> ">
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
		 //  echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		 	if(!empty($flagA)){
				echo $answer_type.'&nbsp;&nbsp;&nbsp;'.str_replace('../../', '../../../',$ansval['answer_text']);
			} else if(empty($flagA) && empty($flagB)){
				echo $answer_type.'&nbsp;&nbsp;&nbsp;'.str_replace('../../', '../',$ansval['answer_text']);
			} else {
				 echo $answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
			}
		   
		 }
		 ?>
		 </li>
		 <?php
		 
		 $j++;
  } 
	?> 
</ul>	   

 <?php
  foreach($qval['Question']['CorrectAnswer'] as $corrans) { //pr($corrans);
  ?>
  <div class="qus_details">Correct Answer : 
		  <?php 
						if(!empty($flagA)){
							echo str_replace('../../', '../../../',$corrans['answer_text']);
						} else if(empty($flagA) && empty($flagB)){
							echo str_replace('../../', '../',$corrans['answer_text']);
						} else {
							 echo $corrans['answer_text'];
						}
		 
		  ?>
  </div>
  
  <?php if(!empty($corrans['ans_description'])){?>
  <div class="qus_details"><a href="javascript:void(0);" onclick="answer_details(<?php echo $corrans['id'];?>)">View Details</a></div>
  <div class="qus_details" id="details_showhide_<?php echo $corrans['id'];?>" style="display:none;">Answer Description : <?php echo $corrans['ans_description'];?></div>
  
  
  <?php
	  //echo 'Correct Answer : '.$corrans['answer_text'].'</br>';
	  //echo '<div class="qus_details">Correct Answer : '.str_replace('../../', '../../../',$corrans['answer_text']).'</p>';
	 // echo '<div class="qus_details">Correct Answer : '.$corrans['answer_text'].'</div>';
		}
	  }
  
	?> 
   </div>
   
   
	
	  <?php 
	  
					if($qval['Question']['answer_type']){
						$k++;
					}
				 $i++;
				}
			}
	  ?>
	   
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<style>
.square {
    background:#fff;
    width: 35px;
    height: 25px;
	margin: -19px 0px 0px 132px
}
.sq_text {
    padding: 4px 0px 0px 9px;
   color:green!important;
}
.sq_text_wrng {
   padding: 4px 0px 0px 9px;
   color:red!important;
}

.sq_text_unattmpt {
   padding: 4px 0px 0px 9px;
   color:orange!important;
}
</style>