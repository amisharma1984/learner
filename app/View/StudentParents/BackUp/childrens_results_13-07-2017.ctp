<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
       <?php echo $this->element('parent_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Children's Results</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
               
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table class="student-list-apr"  style="border: 1px solid pink;">
  <tr>
	<th>Child's Name</th>
	 <th>Child's Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
    <th>Paper</th>
    <th>Question Section</th>
	<th>Date Completed</th>
	<th>Results</th>
  </tr>
  
  <?php 
  if(!empty($myExamAr)){ //pr($myExamAr);
  $i = 1;
	  foreach($myExamAr as $val){
		$random_id = $val['MyExamStudentAnswer']['student_ans_random_id'];
		$examination_category_id = $val['MyExam']['Examination']['examination_category_id'];
		$childData = $this->requestAction(array('controller' => 'student_parents', 'action' => 'fetch_child',$examination_category_id));
		
	   $student_id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $childData[0]['Student']['id']));
		  if(!empty($childData)){
			   $childName = '';
			   foreach($childData as $chval){
				   $childName = $childName.', '.$chval['Student']['first_name'];
			   }
		   } else {
			   $childName = '';
		   }
		
		
		 if(!empty($val['MyExam']['Examination']['title'])){
			  $category = explode(' ',$val['MyExam']['Examination']['ExaminationCategory']['name']);
		 ?>
		  
		  <tr>
		<td><?php echo ltrim($childName, ', ');?></td>
		<td><?php echo $category[1];?></td>
		<td><?php echo 'NAPLAN Style Practice';?></td>
		
		<td><?php echo $val['MyExam']['Examination']['title'];?></td>
		<td><?php echo $val['MyExam']['Examination']['paper'];?></td>
	
    <td><?php echo (@$val['MyExamStudentAnswer']['question_section'] == 1)? 'Non-calculator' : 'Calculator';?></td>
	 <td><?php echo date('d-m-y', strtotime($val['MyExamStudentAnswer']['created']));?></td>
	  <td>
	  <a href="<?php echo $this->webroot.'student_parents/view_answer/'.$random_id.'/'.$childData[0]['Student']['id'];?>">View Results</a></td>
  </tr>
  
<?php
		$i++;
		 } 
	  }
  
?>

				 <?php } else {?>
					 <tr><td colspan="5">	<div class="not_found">
								<span>You have not taken any exam yet.</span>
						</div>  </td></tr>
				<?php }?>
				 </table>
                </div>
				
				
<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
		
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>