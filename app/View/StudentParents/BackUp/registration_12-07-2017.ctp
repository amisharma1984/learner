<style>

fieldset.scheduler-border {
    border: 1px groove #8fbda3 !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 16px !important;
    font-weight: bold !important;
    text-align: left !important;
	display: inline-block!important;
width: auto!important;
padding: 9px!important;
margin-bottom: 4px !important;
border-bottom: none !important;
font-family: "Roboto Slab",serif;
color: #1f7d47;
	
}
</style>
<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			
		
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-12">
				<?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => StudentParent, 'action' => 'myprofile'), true))); 
				echo $this->Form->create('StudentParent', array('name' => 'ParentRegistration', 'onsubmit' => 'return validation("'.$no_of_child.'")')); 
				?>


						<div class="row">
							<div class="col-md-offset-1 col-md-10">
								<div class="form_info">
									<h2>Sign Up for <?php echo $no_of_child.' '.$childTag;?></h2>
									
									<div class="form_cont">
									
								<fieldset class="scheduler-border">
								<legend class="scheduler-border">Parent Details</legend>
						
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
													<?php echo $this->Form->input('first_name', array('label' => false, 'required' => '',  'placeholder' => "Parent's First name", 'class' => 'form-control inp_text'))?>
                                                    <span  class="rgerror" id="fnerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-graduation-cap" aria-hidden="true"></i>
													<?php echo $this->Form->input('last_name', array('label' => false, 'required' => '',  'placeholder' => "Parent's Last name",  'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="lnerror"><?php echo @$lnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									<!--<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											  <?php echo $this->Form->input('parent_class_id', array('label' => false, 'div' => false, 'empty' => '- Class -',  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $classOptions))?>
				
												<?php //echo $this->Form->input('class', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Class -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('school_name', array('label' => false, 'required' => '',  'placeholder' => 'School name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="scerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>-->
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('address', array('type' => 'text','label' => false,'autocomplete' => 'off', 'class' => 'form-control inp_text', 'placeholder' => 'Address'))?>
                                              <span  class="rgerror" id="adderror"><?php echo @$adderror;?> </span>
												
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('email', array('label' => false, 'type' => 'text','autocomplete' => 'off',  'placeholder' => 'Email', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="emerror"><?php echo @$emerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									
											
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-lock" aria-hidden="true"></i>
											<?php echo $this->Form->input('password', array('type' => 'password', 'label' => false,'autocomplete' => 'off', 'class' => 'form-control inp_text', 'placeholder' => 'Password (e.g: abcd@#!123)'))?>
                                               <span  class="rgerror" id="pwderror"><?php echo @$pwderror;?> </span>
												
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-lock" aria-hidden="true"></i>
												<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'autocomplete' => 'off', 'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Confirm password'))?>
					                             <span  class="rgerror" id="cpwderror"><?php echo @$cpwderror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									
							</fieldset>		
									
									
									
									
									
<?php for($i =1; $i<= $no_of_child; $i++){?>	
	<fieldset class="scheduler-border">
    <legend class="scheduler-border">Child <?php echo $i;?></legend>							
<div class="row">
	<div class="col-md-6">
		<div class="form-group">  
		  <?php echo $this->Form->input('examination_category_id', array('label' => false,'id' => 'childYear_'.$i,'name' => 'data[Student][examination_category_id][]', 'div' => false, 'empty' => 'Child '.$i.' Year',  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $yearGroupOptionPractice))?>

			<?php //echo $this->Form->input('class', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Class -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
			<span  class="rgerror"  id="childYrErr_<?php echo $i;?>"><?php //echo @$fnerror;?> </span>
			<div class="clear_fix"></div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php echo $this->Form->input('school_name', array('label' => false,'id' => 'childSchoolNane_'.$i,'name' => 'data[Student][school_name][]', 'required' => '',  'placeholder' => 'Child '.$i.' School name', 'class' => 'form-control inp_text'))?>
			<span  class="rgerror" id="childSNERR_<?php echo $i;?>"><?php //echo @$scerror;?> </span>
			<div class="clear_fix"></div>
		</div>
	</div>
</div>						
								
								
<div class="row">
		<div class="col-md-6">
			<div class="form-group"> 
			<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				<?php echo $this->Form->input('first_name', array('type' => 'text','id' => 'childFirstNane_'.$i, 'name' => 'data[Student][first_name][]','label' => false,'autocomplete' => 'off', 'class' => 'form-control inp_text', 'placeholder' => 'Child '.$i.' First Name'))?>
			  <span  class="rgerror" id="childFNERR_<?php echo $i;?>"><?php //echo @$adderror;?> </span>
				
				<div class="clear_fix"></div>
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<i class="fa fa-graduation-cap" aria-hidden="true"></i>
				<?php echo $this->Form->input('last_name', array('label' => false,'id' => 'childLastNane_'.$i,'name' => 'data[Student][last_name][]', 'type' => 'text','autocomplete' => 'off',  'placeholder' => 'Child '.$i.' Last Name', 'class' => 'form-control inp_text'))?>
				<span  class="rgerror" id="childLNERR_<?php echo $i;?>"><?php //echo @$emerror;?> </span>
				<div class="clear_fix"></div>
			</div>
			
		</div>
	</div>
	</fieldset>
<?php }?>
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		
		<div class="row">
			<div class="col-sm-7">

			<?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','placeholder' => 'Type the captcha text here','type'=>'text', 'value' => @$captcha_txt));?>
			<span  class="rgerror" id="captchaErr"><?php echo @$captchaErr;?> </span>
			<div class="clear_fix"></div>
			</div>
			<div class="col-sm-5 led_wid">
				<label >
						<img id="captcha" src="<?php echo $this->webroot.'StudentParents/captcha_image';?>" alt="" />
						<a href="javascript:void(0);"
							onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'StudentParents/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
						<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
					</label>
				</div>
	</div>
		
	
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<?php //echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); ?><!--Is previous year customer? -->
			
			<div class="clear_fix"></div>
		</div>
	</div>
</div>

									
									<div class="row">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="submit" class="btn btn-info def_btn" value="Submit">
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									</div>
								</div>
							</div>
						</div>

						<?php echo $this->Form->end(); ?>
					</div>
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>
<script>
 function validation(no_of_student){ //alert(no_of_student);
	var result=true;
	
	for(var i = 1; i <= no_of_student; i++){
		if($.trim($('#childFirstNane_'+ i).val()) == ""){
			 $('#childFNERR_' + i).html('Please enter child '+ i +' first name');
			 result=false;
		} else {
			 $('#childFNERR_' + i).html('');
		}
		
		if($.trim($('#childLastNane_'+ i).val()) == ""){
			 $('#childLNERR_' + i).html('Please enter child '+ i +' last name');
			 result=false;
		} else {
			 $('#childLNERR_' + i).html('');
		}
		
		if($.trim($('#childSchoolNane_'+ i).val()) == ""){
			 $('#childSNERR_' + i).html('Please enter child '+ i +' school name');
			 result=false;
		} else {
			 $('#childSNERR_' + i).html('');
		}
		
		if($.trim($('#childYear_'+ i).val()) == ""){
			 $('#childYrErr_' + i).html('Please select year group for child '+ i);
			 result=false;
		} else {
			 $('#childYrErr_' + i).html('');
		}
		
		
	}
	
	
	
	if($.trim($('#StudentParentFirstName').val()) == ""){
		 $('#fnerror').html('Please enter first name');
		 result=false;
	} else {
		 $('#fnerror').html('');
	}
	
	if($.trim($('#StudentParentLastName').val()) == ""){
		 $('#lnerror').html('Please enter last name');
		 result=false;
	} else {
		 $('#lnerror').html('');
	}
	

	
	if($.trim($('#StudentParentAddress').val()) == ""){
		 $('#adderror').html('Please enter address');
		 result=false;
	} else {
		 $('#adderror').html('');
	}
	
	var email = $.trim($('#StudentParentEmail').val());
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	
	if(email == ""){
		 $('#emerror').html('Please enter email');
		 result=false;
	} else if(email != '' && !pattern.test(email)){
		 $('#emerror').html('Please enter valid email format');
		 result=false;
	} else if(email != "" && pattern.test(email)){
		var parent_email = $('#StudentParentEmail').val();
		
		$.ajax({
			url : '<?php echo $this->webroot.'StudentParents/get_parent_email_exist'?>',
			type : 'POST',
			async: true,
			data : {parent_email : parent_email},
			success : function(msg){ 
				if(msg == 'EXIST'){
					$('#emerror').html('This email already exist, please try another');
						result=false;
				} else {
					$('#emerror').html('');
				}
			}
			 
		});
		
		
	} else {
		 $('#emerror').html('');
	}
	
	
	
	
	var cnf_pass = $.trim($('#StudentParentConfirmPassword').val());
	if(cnf_pass == ""){
		 $('#cpwderror').html('Please enter confirm password');
		 result=false;
	} else if(cnf_pass != "" && $.trim($('#StudentParentPassword').val()) != $.trim($('#StudentParentConfirmPassword').val())){
		 $('#cpwderror').html('Password and confirm password does not match');
		 result=false;
	} else {
		 $('#cpwderror').html('');
	}
	
	
	
	
	if($.trim($('#StudentParentCaptchaTxt').val()) == ""){
		 $('#captchaErr').html('Please enter captcha code');
		 result=false;
	} else if($.trim($('#StudentParentCaptchaTxt').val()) != ""){ 
		var captcha_txt = $('#StudentParentCaptchaTxt').val();
		
		$.ajax({
			url : '<?php echo $this->webroot.'StudentParents/captcha_match_value'?>',
			type : 'POST',
			async: false,
			data : {captcha_txt : captcha_txt},
			success : function(msg){
				if(msg == 'ERROR'){
					 $('#captchaErr').html('Please enter correct captcha code');
						result=false;
				} else {
					$('#captchaErr').html('');
				}
			}
			 
		});
		
		
	} else {
		 $('#captchaErr').html('');
	}
	
	//var passPatern = /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i;
	var passPatern =  new RegExp(/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/);
	var pass = $.trim($('#StudentParentPassword').val());
	
	
	if($.trim($('#StudentParentPassword').val()) == ""){
		 $('#pwderror').html('Please enter password');
		  result=false;
	} else if(pass.length < 6){
		 $('#pwderror').html('Password must be at least 6 characters');
		  result=false;
	} else if(pass != '' && !passPatern.test(pass)){
		 $('#pwderror').html('Password should contain at least one number,one letter, and one special character');
		 result=false;
	}
	else {
		 $('#pwderror').html('');
	}
	
	
	
	return result;
	
 }
</script>
