<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('parent_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Exams Available</h2>
                </div>
				
 <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'student_parents', 'action' => 'checkout'), true))); ?>
<div class="prof_details prof_form">
<table id="YourTableId" class="student-list-apr">
  <tr>
  <th>List</th>
   <th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
   
	 <th>Price($ AUD)</th>
    <th>Click to purchase Exams</th>
   
	
  </tr>
  
  <?php
  if(!empty($allExamAr)){ 
	  $j = 1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		  ?>
		  <tr>
	<td><?php echo $j;?></td>
	 <td><?php echo $category[1]; ?></td>
    <td>NAPLAN Style Practice</td>
    <td><?php echo $val['Examination'][0]['title']; ?></td>
	<td>
		<?php 
		 foreach($val['Examination'] as $vp){ 
			echo $vp['price'];
			echo '<br>';
		 }
	    ?>
	</td>
    
	<td style="text-align:center;">
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'student_parents', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span> <?php echo $v['paper']//$i;?> (Already purchased)</span>
	<?php } else {?>
	<input type="checkbox" class="case" value="<?php echo $v['id'];?>" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo $v['paper'];?>
	
	<!--<a href="<?php //echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['paper'];?></a>-->
	
	<?php
		 }
		 echo '<br>';
		 $i++;
	 }
	?>
	
	</td>
	
	
	
	
	 
	<?php /*?> <td><?php echo @$val['Examination']['price'];?></td>
	  <td>
	  <?php
	  if(!empty($val['MyExam']['payment_status']) && $val['MyExam']['payment_status'] == 1){ 
	  ?>
	  <a href="<?php echo $this->webroot.'students/take_exam/'.@$val['Examination']['id'];?>">Purchased</a>
	  <?php } else {?>
	 <a href="<?php echo $this->webroot.'students/make_payment/'.@$val['Examination']['id'];?>">Make Payment</a>
	  <?php }?>
	  </td><?php */?>
	  
  </tr>
<?php
	$j++;
	  }
  }
?>
</table>
				 
				 
                </div>
				
					<input type="hidden" value="checkout" name="page_name">
					<div class="clear_fix"></div>
					<div class="exam_chkout">
					<input type="submit" id="YourbuttonId" class="btn btn-info def_btn" value="Checkout">

					<?php echo $this->Form->end(); ?>
					</div>
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<style>
table td a {
    color: #A0760A;
    font-weight: normal;
    text-decoration: none;
}
table td span {
    color: red;
    font-weight: normal;
    text-decoration: none;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
	<SCRIPT language="javascript">
	$(document).ready(function(){
		$("#YourbuttonId").click(function(){
				if($('#YourTableId').find('input[type=checkbox]:checked').length == 0)
				{
					alert('Please select atleast one checkbox');
					return false;
				}
		});
		
			  $('.check:button').click(function(){
					  var checked = !$(this).data('checked');
					  $('input:checkbox').prop('checked', checked);
					  $(this).val(checked ? 'Uncheck All' : 'Check All' )
					  $(this).data('checked', checked);
			});
		
		
	
	});
	

</SCRIPT>
