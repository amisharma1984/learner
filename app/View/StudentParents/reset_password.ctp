<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'StudentParents', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'StudentParentLoginForm')); 
							  echo $this->Form->create('StudentParent'); 
							 ?>
                       
							<h2>Reset Password</h2>
							<div class="form_cont">
							<?php if(!empty($somthingErr)) {?>
							<div class="form-group">
							<span  class="rgerror"><?php echo @$somthingErr;?> </span>
							<div class="clear_fix"></div>
							</div>
							<?php }?>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentParent.password', array('label' => false,'type' => 'password',  'placeholder' => 'Enter new password',  'maxlength' => 50, 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$pwderror;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentParent.confirm_password', array('label' => false,'type' => 'password',  'placeholder' => 'Enter confirm new password', 'maxlength' => 50, 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$cpwderror;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn" >Submit</button>
							</div>
							<div class="form-group">
								Back to login? <a href="<?php echo $this->Html->url(array('controller'=>'parent','action' => 'login')) ?>" > Login!</a>
								
							</div>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
                                  
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>