<?php
/* /app/View/Helper/CommunityRoleHelper.php */
App::uses('HtmlHelper', 'View/Helper');
//App::import("Model", "CommunitiesRole");

class ExamTypeHelper extends HtmlHelper {
    
    public $treeOptions = array(); 

    public function getExamTypeList($examinationTypes = array()){
        
        $this->dumpTree($examinationTypes);
        return $this->treeOptions;
        
    }
        
    public function dumpTree($examinationTypes = array(), $parent = 0, $level = 0) {
        $spacing = '';
        foreach ($examinationTypes as $key => $examinationType) {
            
            $indentString = $spacing . str_repeat("&nbsp;", $level * 2);
            if(!empty($examinationType['ExaminationType']['parent_id'])){
                $this->treeOptions[$examinationType['ExaminationType']['id']] = $indentString . $examinationType['ExaminationType']['name'];
            }else{
                $this->treeOptions[$examinationType['ExaminationType']['id']] = '<b>'.$indentString . $examinationType['ExaminationType']['name'].'</b>';
            }
            
            if (array_key_exists('children', $examinationType) && count($examinationType['children']) > 0) { /* check if this Type that we just listed has a child */
                $this->dumpTree($examinationType['children'], $examinationType['ExaminationType']['id'], $level + 1); /* If the type appears to be a parent, the function calls itself passing the current type id as a parent and incrementing the depth of the tree */
            }
        }
    }

}
