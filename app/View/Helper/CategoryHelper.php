<?php
/* /app/View/Helper/CommunityRoleHelper.php */
App::uses('HtmlHelper', 'View/Helper');
//App::import("Model", "CommunitiesRole");

class CategoryHelper extends HtmlHelper {
    
    public $treeOptions = array(); 


    public function getCategoryList($categories = array()){
        
        $this->dumpTree($categories);
        return $this->treeOptions;
        
    }
        
    public function dumpTree($categories = array(), $parent = 0, $level = 0) {
        $spacing = '';
        foreach ($categories as $key => $category) {
            
            $indentString = $spacing . str_repeat("&nbsp;", $level * 2);
            if(!empty($category['ExaminationCategory']['parent_id'])){
                $this->treeOptions[$category['ExaminationCategory']['id']] = $indentString . $category['ExaminationCategory']['name'];
            }else{
                $this->treeOptions[$category['ExaminationCategory']['id']] = '<b>'.$indentString . $category['ExaminationCategory']['name'].'</b>';
            }
            
            if (array_key_exists('children', $category) && count($category['children']) > 0) { /* check if this category that we just listed has a child */
                $this->dumpTree($category['children'], $category['ExaminationCategory']['id'], $level + 1); /* If the category appears to be a parent, the function calls itself passing the current category id as a parent and incrementing the depth of the tree */
            }
        }
    }

}
