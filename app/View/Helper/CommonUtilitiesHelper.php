<?php
App::uses('FormHelper', 'View/Helper');
class CommonUtilitiesHelper extends FormHelper {
    
    /**
     * This function will express any given number in words
     * If number is 1432 then this function will return "One thousand four hundred thirty two".
     */
    public function convertNumberToWord($num = false) {

        $num = str_replace(array(',', ' '), '', trim($num));
        if (!$num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ( $hundreds == 1 ? '' : 's' ) . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int) ($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && (int) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }
    
    /**
     * Recognizes ftp://, ftps://, http:// and https:// in a case insensitive way.
     * @param string $url
     * @return string
     */
    public function getUrlWithProtocol($url = null) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
	
	
	public function getDetailOfSentEmail($to_id,$EmailDeliveryId){
	//9022,3743
		App::import("Model", "EmailDelivery");  
		App::import("Model", "Attachment");  
		
		$EmailDeliveryModel = new EmailDelivery();  
		$AttachmentModel = new Attachment();  
		//$savedCustomersDtls = $EmailDeliveryModel->find("list");
		
		$EmailDeliveryModel->recursive = 2;
		$savedCustomersDtls = $EmailDeliveryModel->find('all',array(
											'order' => 'EmailDelivery.id DESC',
											'fields' => 'id,from_id,to_id,email_id,issended,created,modified',
											'conditions' => array(
															'EmailDelivery.id <>' => $EmailDeliveryId,
															'EmailDelivery.to_id' => $to_id,
															)));
			
			
		
		if( !empty($savedCustomersDtls) && count($savedCustomersDtls) >0 ){
			
			foreach($savedCustomersDtls as $k=>$savedCustomersDtl){
				#pr($savedCustomersDtl);die;
			   $attachment_mask = $savedCustomersDtl['Email']['attachment_mask']; 
				
			   $emailDetails = $AttachmentModel->find('all', array('conditions' => array('Attachment.mask' => $attachment_mask)));
			   $attachmentFiles = array();
				if (!empty($emailDetails)) {
					//array('photo.png' => '/full/some_hash.png');
					foreach ($emailDetails as $key => $attachment){
						 $attachmentFiles[$attachment['Attachment']['title'].'.'.$attachment['Attachment']['extension']]['file'] = WWW_ROOT . 'files/school/attachments/'.$attachment['Attachment']['path'].$attachment['Attachment']['title'].'.'.$attachment['Attachment']['extension'];
						$attachmentFiles[$attachment['Attachment']['title'].'.'.$attachment['Attachment']['extension']]['mimetype'] = $this->extensionWiseMimeTypes[strtolower($attachment['Attachment']['extension'])];
						
					}
				}
				$savedCustomersDtls[$k]["attachmentFiles"] = $attachmentFiles;
			
			}
			
			
		}	
		#pr($savedCustomersDtls);die;
		return $savedCustomersDtls;
	}
}
?>