<?php

class KeyValueHelper extends FormHelper {

    public function getValue($key, $model) {
        //echo json_encode($model['Setting'])."<br><br>".$key;
        if (!isset($model['Setting']) || !isset($model['Setting'][$key]))
            return null;
        return $model['Setting'][$key];
    }

    public function isEmpty($key, $model) {
        if (!isset($model['Setting']) || !isset($model['Setting'][$key]))
            return null;
        return empty($model['Setting'][$key]);
    }

    public function canShow($key, $model) {
        if ($this->getValue('show_' . $key, $model) && $this->getValue($key, $model) != '')
            return true;
        return false;
    }

}

?>