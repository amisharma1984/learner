<?php

/* /app/View/Helper/CommunityRoleHelper.php */
App::uses('HtmlHelper', 'View/Helper');

//App::import("Model", "CommunitiesRole");
//App::import("Model", "CommunitiesUser");
class BreadcrumbHelper extends HtmlHelper {

    /**
     * Link function map of HtmlHelper::link() for use community by default
     * @param $title Text for the link
     * @param $url The url for the link (here add the community by default)
     * @param $options Options for the link
     * @param $confirmMessage The confirmation message
     * @see HtmlHelper::link()	The funtion encapsulated
     */
    public function link($title, $url = NULL, $options = array(), $confirmMessage = false) {
        if (is_array($url)) {
            if (!isset($url['action']))
                $url['action'] = 'index';

            $controller = 'home';
            if (isset($this->request->params['controller']))
                $controller = $this->request->params['controller'];
            if (!isset($url['controller']))
                $url['controller'] = $controller;

            $plugin = '';
            if (isset($this->request->params['plugin']))
                $plugin = $this->request->params['plugin'];
            if (!isset($url['plugin']))
                $url['plugin'] = $plugin;

            if (!isset($url['community']))
                $url['community'] = Configure::read('Community.slug');
        }

        return parent::link($title, $url, $options, $confirmMessage);
    }

    /**
     * Url function map of HtmlHelper::url() for use community by default
     * @param $url The url (here add the community by default)
     * @param $full If the url is absolute use true, fals in other case
     * @see HtmlHelper::url()
     */
    public function url($url = NULL, $full = false) {

        if (is_array($url)) {
            if (!isset($url['action']))
                $url['action'] = 'index';

            $controller = 'home';
            if (isset($this->request->params['controller']))
                $controller = $this->request->params['controller'];
            if (!isset($url['controller']))
                $url['controller'] = $controller;

            $plugin = '';
            if (isset($this->request->params['plugin']))
                $plugin = $this->request->params['plugin'];
            if (!isset($url['plugin']))
                $url['plugin'] = $plugin;

            if (!isset($url['community']))
                $url['community'] = Configure::read('Community.slug');
        }
        return parent::url($url, $full);
    }

    /**
     * Make a Breadcrumb
     * Each element must have the next elements:
     * - title: The title of the element
     * - link: The link of the element
     * e.x.: array('title' => 'Test', 'link' => array('controller' => 'tests', 'action' => 'index'))
     * The last element will not show the link.
     * Allways is a Home element at the init of breadcrumb.
     * @param $elements Array of elements
     */
    function breadCrumb($elements = array(), $inAdmin = false) {
        
        if($inAdmin){
            $homeUrl = array('controller' => 'admins', 'action' => 'home');
        }else{
            $homeUrl = array('controller' => 'pages', 'action' => 'index');
        }
        
        $out = '<ul class="breadcrumb">';
        $out .= '<li>' . $this->link('Home', $homeUrl) . '</li>';
        for ($i = 0; $i < count($elements); $i++) {
            if ($i == count($elements) - 1)
                $out .= '<li>' . $elements[$i]['text'] . '</li>';
            else
                $out .= '<li>' . $this->link($elements[$i]['text'], $elements[$i]['link']) . '</li>';
        }
        $out .= '</ul>';
        return $out;
    }

    /**
     * @param $elements Array of elements
     * @see LEHtml::breadCrumb()
     */
    public function show($elements = array(), $inAdmin = false) {
        $out = '<div>';
        $out .= $this->breadCrumb($elements, $inAdmin);
        $out .= '</div>';
        return $out;
    }

    /**
     * Return a back link
     * TODO: Move from here to element
     */
    public function backLE() {
        $out = '<div style="float:right; margin-top: 14px;"><a href="javascript:void(0);" onclick="window.history.back()" style="text-decoration:none;">' . __('&laquo; Atrás') . '</a></div>';
        return $out;
    }

}
