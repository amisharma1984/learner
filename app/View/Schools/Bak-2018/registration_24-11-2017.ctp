<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					
					<div class="col-md-12">
			<?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				
				echo $this->Form->create('School', array('name' => 'SchoolRegistration', 'onsubmit' => 'return validation()')); 

				 ?>
						<div class="row">
							<div class="col-md-offset-1 col-md-10">
								<div class="form_info">
									<h2>Sign up for school account</h2>
									
									<div class="form_cont">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
												<?php echo $this->Form->input('name', array('label' => false,  'placeholder' => 'School Name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="schoolNameErr"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('email', array('type' => 'text',  'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'School Email '))?>
												<span  class="rgerror" id="emerror"><?php echo @$emerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-address-book" aria-hidden="true"></i>

												<?php echo $this->Form->input('manager_name', array('label' => false,  'placeholder' => 'Head Teacher Name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="htNameErr"><?php echo @$mNameErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('manager_email', array('label' => false, 'type' => 'text',  'placeholder' => 'Head Teacher Email', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="manager_emailErr"><?php echo @$manager_emailErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									
									
								<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-lock" aria-hidden="true"></i>
												<?php echo $this->Form->input('password', array('type' => 'password', 'label' => false,'autocomplete' => 'off', 'class' => 'form-control inp_text', 'placeholder' => 'Password (e.g: abcd@#!123)'))?>
                                               <span  class="rgerror" id="pwderror"><?php echo @$pwderror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-lock" aria-hidden="true"></i>
												<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'autocomplete' => 'off', 'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Confirm password'))?>
					                             <span  class="rgerror" id="cpwderror"><?php echo @$cpwderror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>	
									
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => 'Select Level',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select','empty'=>'Select Type', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('SchoolInformation.street', array('label' => false,'type' => 'text',  'placeholder' => 'Street', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="streetErr"><?php echo @$streetErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-location-arrow" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'type' => 'text',  'placeholder' => 'City / Town ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="townErr"><?php echo @$townErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.state', array('label' => false,  'placeholder' => 'State ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="stateErr"><?php echo @$stateErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-codepen" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false,  'placeholder' => 'Postcode ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="zipErr"><?php echo @$zipErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-phone" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false,  'placeholder' => 'Phone', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="phoneErr"><?php echo @$phoneErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-fax" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false,  'placeholder' => 'Fax', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="faxErr"><?php echo @$faxErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-link" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.url', array('label' => false,  'placeholder' => 'School website address', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror" id="urlErr"><?php //echo @$urlErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											
											<div class="row">
												<div class="col-sm-7">

												<?php echo $this->Form->input('SchoolInformation.captcha_txt', array('label' => false,'class'=>'form-control inp_text','placeholder' => 'Type the captcha text here','type'=>'text', 'value' => @$captcha_txt));?>
												<span  class="rgerror" id="ScCaptchaErr"><?php echo @$captchaErr;?> </span>
												<div class="clear_fix"></div>
												</div>
												<div class="col-sm-5 led_wid">
													<label >
															<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
															<a href="javascript:void(0);"
																onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
															<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
														</label>
													</div>
										</div>
											
										
											</div>
										</div>
									</div>
									
									
								<div class="row">
										
										<div class="col-md-6">
											<div class="form-group tcp_chk" style="padding:15px 0 0 0;">
											<span style="float:left; margin-right:7px;" >Has your school been a customer in previous years?</span>
												
												<?php
												$option = array(0 => 'No',1 => 'Yes');
												
												echo $this->Form->input('SchoolInformation.isprevious' ,array('options' => $option,'class' => 'mr-xs','hiddenField' => false,'label'=>false, 'id' => 'SchoolInformationIsPrevious','default'=>0) );

												//echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); 
												?>
												
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group tcp_chk">
												<?php echo $this->Form->checkbox('SchoolInformation.tcpp', array('value' => 0, 'class' => 'mr-xs', 'id' => 'tcpp')); ?>By clicking SUBMIT, you confirm that you have read and agree to our <a target="_blank" href="<?php echo $this->webroot.'terms-of-service';?>">Terms and Conditions</a> and our <a target="_blank" href="<?php echo $this->webroot.'privacy-policy';?>">Privacy Policy</a>. 
												</br><span  class="rgerror" id="tcppErr"></span>
												
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="submit" class="btn btn-info def_btn" value="Submit">
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									
									
									
								</div>
							</div>
							</div>
						</div>

						<?php echo $this->Form->end(); ?>
					</div>
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
<script>
function validation(){ 
	var result=true;
	
	if($.trim($('#SchoolName').val()) == ""){
		 $('#schoolNameErr').html('Please enter school name');
		 result=false;
	} else {
		 $('#schoolNameErr').html('');
	}
	
	if($.trim($('#SchoolManagerName').val()) == ""){
		 $('#htNameErr').html('Please enter head teacher name');
		 result=false;
	} else {
		 $('#htNameErr').html('');
	}
	
	if($.trim($('#SchoolInformationStreet').val()) == ""){
		 $('#streetErr').html('Please enter street');
		 result=false;
	} else {
		 $('#streetErr').html('');
	}
	
	if($.trim($('#SchoolInformationTown').val()) == ""){
		 $('#townErr').html('Please enter city/town');
		 result=false;
	} else {
		 $('#townErr').html('');
	}
	
	
	if($.trim($('#SchoolInformationState').val()) == ""){
		 $('#stateErr').html('Please enter state');
		 result=false;
	} else {
		 $('#stateErr').html('');
	}
	
	if($.trim($('#SchoolInformationZip').val()) == ""){
		 $('#zipErr').html('Please enter postcode');
		 result=false;
	} else {
		 $('#zipErr').html('');
	}
	
	if($.trim($('#SchoolInformationPhone').val()) == ""){
		 $('#phoneErr').html('Please enter phone number');
		 result=false;
	} else {
		 $('#phoneErr').html('');
	}
	
	if($.trim($('#SchoolInformationFax').val()) == ""){
		 $('#faxErr').html('Please enter fax');
		 result=false;
	} else {
		 $('#faxErr').html('');
	}
	
	var schoolemail = $.trim($('#SchoolEmail').val());
	var headTeacherEmail = $.trim($('#SchoolManagerEmail').val());
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	
	if($.trim($('#SchoolEmail').val()) == ""){
		 $('#emerror').html('Please enter school email');
		 result=false;
	} else if(schoolemail != '' && !pattern.test(schoolemail)){
		 $('#emerror').html('Please enter valid email format');
		 result=false;
	} else {
		 $('#emerror').html('');
	}
	
	if($.trim($('#SchoolManagerEmail').val()) == ""){
		 $('#manager_emailErr').html('Please enter head teacher email');
		 result=false;
	} else if(headTeacherEmail != '' && !pattern.test(headTeacherEmail)){
		 $('#manager_emailErr').html('Please enter valid email format');
		 result=false;
	} else {
		 $('#manager_emailErr').html('');
	}
	
	
	var url = $.trim($('#SchoolInformationUrl').val());
	var urlPattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
	
	 if(url != '' && !urlPattern.test(url)){
		 $('#urlErr').html('Please enter valid school website address');
		 result=false;
	} else {
		 $('#urlErr').html('');
	}
	
	
	
	
	
	var cnf_pass = $.trim($('#SchoolConfirmPassword').val());
	
	if(cnf_pass == ""){
		 $('#cpwderror').html('Please enter confirm password');
		 result=false;
	} else if(cnf_pass != "" && $.trim($('#SchoolPassword').val()) != $.trim($('#SchoolConfirmPassword').val())){
		 $('#cpwderror').html('Password and confirm password does not match');
		 result=false;
	} else {
		 $('#cpwderror').html('');
	}
	
	if($.trim($('#SchoolInformationCaptchaTxt').val()) == ""){
		 $('#ScCaptchaErr').html('Please enter captcha code');
		 result=false;
		} else {
			 $('#ScCaptchaErr').html('');
		}
		
	if($('#tcpp').is(':checked')){
			$('#tcppErr').html('');
	} else {
			$('#tcppErr').html('Please tick this box to proceed');
		 result=false;
	}		
		
	//var passPatern = /^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i;
	var passPatern = new RegExp(/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/);
	var pass = $.trim($('#SchoolPassword').val());
	
	
	if($.trim($('#SchoolPassword').val()) == ""){
		 $('#pwderror').html('Please enter password');
		 return false;
	} else if(pass.length < 6){
		 $('#pwderror').html('Password must be at least 6 characters');
		 return false;
	} else if(pass != '' && !passPatern.test(pass)){
		 $('#pwderror').html('Password should contain at least one number,one letter, and one special character');
		 result=false;
	}
	else {
		 $('#pwderror').html('');
	}
	
	
		
	
	return result;
	
 }
</script>