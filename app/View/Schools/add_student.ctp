<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   <style>
   .addstudentErr{
    color: red;
    font-size: 13px;
    margin-left: 45%;
}
   </style>
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Add Student</h2>
                  <h3> <?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                 
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('Student'); 
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                     

					 <div class="form-group">
                        <label >First Name :</label>
						<?php echo $this->Form->input('first_name', array('label' => false ,'required' => '',  'class' => 'form-control inp_text'))?>
						  <span  class="addstudentErr"><?php echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					  
					  
                      <div class="form-group">
                        <label >Class :</label>
						  <?php echo $this->Form->input('student_class_id', array('label' => false, 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $classOptions))?>
				<span  class="addstudentErr"><?php echo @$lnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					  
					  
					  
                        <?php /*?>
					  <div class="form-group">
                        <label >Exam Type :</label>
                       <?php echo $this->Form->input('examination_type_id', array('label' => false, 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $examination_types))?>
					 <div class="clear_fix"></div>
                      </div>  <?php */?>
					  
					<?php /*?>  
				 <div class="form-group">
                        <label >Exam Name :</label>
                       <select class="selectpicker show-tick form-control inp_text" name="data[Student][school_purchase_exam_id]">
                          <option value="">Select Exam</option>
                           <?php echo $SchoolPurchaseExamOption;?>
                        </select>
					 <div class="clear_fix"></div>
                      </div><?php */?>
					  
					  
					  
					 <div class="form-group">
                        <label >Email :</label>
						<?php echo $this->Form->input('email', array('label' => false ,'type' => 'text' ,'required' => '',  'class' => 'form-control inp_text'))?>
							<span  class="addstudentErr"><?php echo @$emerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					  <?php /*?>
					  	 <div class="form-group">
                        <label >Confirm Password :</label>
						<?php echo $this->Form->input('confirm_password', array('type' => 'password' ,'required' => '', 'label' => false,  'class' => 'form-control inp_text'))?>
						   <span  class="addstudentErr"><?php echo @$cpwderror;?> </span>
                        <div class="clear_fix"></div>
                      </div><?php */?>
				
                    </div>
					
					
					
					
					
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Last Name :</label>
						<?php echo $this->Form->input('last_name', array('label' => false ,'required' => '', 'class' => 'form-control inp_text'))?>
							<span  class="addstudentErr"><?php echo @$lnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >School  :</label>
							<?php echo $this->Form->input('school_name', array('label' => false,'required' => '', 'value' => @$studentDetails['Student']['school_name'], 'class' => 'form-control inp_text'))?>
                        <span  class="addstudentErr"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  <?php /*?>
                      <div class="form-group">
                        <label >Exam Category :</label>
						<?php echo $this->Form->input('examination_category_id', array('label' => false, 'div' => false,  'value' => @$studentDetails['Student']['examination_category_id'], 'class' => 'selectpicker show-tick form-control inp_text', 'options' => $examination_categories))?>
					  <div class="clear_fix"></div>
                      </div><?php */?>
					  
					  
                      <div class="form-group">
                        <label >Address :</label>
						<?php echo $this->Form->input('address', array('type' => 'textarea','required' => '', 'label' => false, 'class' => 'form-control inp_text', 'placeholder' => ''))?>
					    <span  class="addstudentErr"><?php echo @$adderror;?> </span>
					  <div class="clear_fix"></div>
                      </div>
					  <?php /*?>
					   <div class="form-group">
                        <label >Password :</label>
						<?php echo $this->Form->input('password', array('type' => 'password','required' => '', 'label' => false,  'class' => 'form-control inp_text'))?>
						 <span  class="addstudentErr"><?php echo @$pwderror;?> </span>
                        <div class="clear_fix"></div>
                      </div><?php */?>
					  
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn" value="Submit">
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>