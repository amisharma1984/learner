<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
		$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );
	</script>


<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Resault Chart</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
					<div class="prof_details prof_form">
						<div class="attempt_details">
							<div class="attempt_qus">
								<ul>
									<li><span>Total Student(s) : </span> <strong><?php echo $no_of_student_appear;?> </strong></li>
									<li><span>Pass Student(s) : </span> <strong> <?php echo $pass ?></strong></li>
									<li><span>Fail Student(s) : </span> <strong> <?php echo $fail;?> </strong></li>
									<!--<li><span>Average Correct Answer : </span> <strong><?php //echo @$avereageCorrectAns;?></strong></li>-->
									
								</ul>
							</div>
						</div>
					</div>      
				<div class="clear_fix"></div>
				
				
              </div>
            </div>
			

			
			
	<div class="prof_details prof_form">
	<table id="example" class="display" border="1" bordercolor="#fff" cellspacing="0"  width="100%">
				<thead>
					<tr>
			<th>Question Number</th>
			<th>Students(%) who gives correct answer</th>
			<th>Students(%) who gives wrong answer</th>
			<!--<th>Average Correct Answer</th>-->
			<th>Standard Deviation</th>
			<th><!--Action--></th>
					</tr>
				</thead>
				<tbody>
					
					 <?php
	        	if(!empty($questionList)){ 
					$i = 1;
					foreach($questionList as $val){
						$question_id = $val['Question']['id'];
						//echo $school_purchase_exam_id;die;
					    $percentageStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_student_correct_answer',$school_purchase_exam_id, $question_id));
					    $percentageWrongStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_student_wrong_answer',$school_purchase_exam_id, $question_id));
					    $school_pid = $this->requestAction(array('controller' => 'App', 'action' => 'data_decrypt',$school_purchase_exam_id));
	 ?>
		<tr>
			<td>
			<?php  echo $i;//echo $val['Question']['question_order'];?>
			</td>
			<td><?php echo @$percentageStudent;?>
			<?php if($percentageStudent >0) {?>
			<span style="float:right; color:#f69d25; font-size:14px;">
			  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewStudentCorrectAns" onclick="get_student_who_gives_correct_ans(<?php echo $question_id;?>,<?php echo $school_pid;?>)" class="student_list_ans" data-student-qid="<?php echo $question_id;?>" data-student="<?php echo $school_purchase_exam_id;?>">View Student</a>
			</span>
			<?php }?>
			</td>
			<!--<td><?php //echo @$averageCorrectAnswer;?></td>-->
			<td>
			<?php echo @$percentageWrongStudent;?>
				<?php if($percentageWrongStudent >0) {?>
			<span style="float:right; color:#f69d25; font-size:14px;">
			  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewStudentCorrectAns" onclick="get_student_who_gives_wrong_ans(<?php echo $question_id;?>,<?php echo $school_pid;?>)"  class="student_list_ans" data-student="<?php echo $school_purchase_exam_id;?>">View Student</a>
			</span>
			<?php }?>
			</td>
			<td><?php echo @$standardDeviation;?></td>
			<td><?php //echo 'View';?></td>
		</tr>
	<?php 
				$i++;
				}
			}
	?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>

	
<!-- Start Add And edit Exam Modal-->
<div class="modal fade" id="viewStudentCorrectAns" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<script>

function get_student_who_gives_correct_ans(question_id, school_purchase_exam_id){
			 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_correct_ans';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
						$('#viewStudentCorrectAns').find('.modal-title').html('<font color="green">Student list which gives correct answer</font>');
					  }
					  
				});
}

function get_student_who_gives_wrong_ans(question_id, school_purchase_exam_id){
			 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_wrong_ans';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
						$('#viewStudentCorrectAns').find('.modal-title').html('<font color="red">Student list which gives wrong answer</font>');
					  }
					  
				});
}




	/*$(document).ready(function(){
			 $('.student_list_ans').click(function(event){
				var school_purchase_exam_id = $(this).attr('data-student');
				var question_id = $(this).attr('data-student-qid');
				 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_correct_ans';
				
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
					  }
					  
				});
			
			});
	});
	*/
</script>
