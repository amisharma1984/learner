<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
	/*	$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );*/
	</script>
	
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable( {
            "columns": [
                null,
                null,
				null,
                null,
				null,
				null,
                null,
				null,
				null,
                { "orderable": false }, // last column orderable/up down arrow hide i.e. false
            ]
        } );
    } );
</script>

<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                 <div class="main_headding">
				<?php 
					$category = explode(' ',$studentList[0]['SchoolPurchaseExam']['Examination']['ExaminationCategory']['name']);
				?>
                <h2>YEAR <?php echo $category[1];?> STUDENT RESULT LIST<?php echo !empty($cal_non_cal) ? ': '.strtoupper($cal_non_cal) : '';?></h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
<table style="width:55%" border="1">
	<tbody>
		<tr>
		<th>&nbsp;</th>
		<th>Mean</th>
		<th>Standard Deviation</th>
		</tr>
		
		<tr>
		<td><?php echo 'Year '.$category[1];?> Group</td>
		<?php
		$yearGroupMean = 0;
		$groupmean=0;
		$standardDeviation1 =0;
		$μt = 0;
		$resultval = 0;
		if($groupByStudent){
			$countStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student_by_category',$groupByStudent[0]['Student']['examination_category_id']));
		foreach($groupByStudent as $studentData){
		
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student_byexamcat',$studentData['Student']['examination_category_id']));
		
		//pr($listStudent);
		$list_student_correct = array();
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id'],$cal_non_cal));
			 array_push($list_student_correct, $quetsionsAnswersData['totalCorrectAnswer']);
		}
		
		 
		?>
		<?php 
			}
			$totnumKeys = count($list_student_correct);
			$totalmean= 0; 
			$totalvalue =0;
			$μt = 0;
			$totalresultval = 0;
			if($totnumKeys){
			for($l=0;$l<$totnumKeys ;$l++){
			$totalvalue  =$totalvalue + $list_student_correct[$l];	
			}
			}
			$μt = round($totalvalue/$totnumKeys,2);
			
			$z1= 0;
			$t1=0;
			$x1 =0;
			if($totnumKeys){
				for($n=0;$n<$totnumKeys ;$n++){
				$z1  = $list_student_correct[$n] -  $μt;
				$t1 = $z1*$z1;
				
				$x1 = $x1+$t1;
					
				}
			}
		    $totalresultval = round(sqrt($x1/$totnumKeys),2);
			
		}
		?>
		
		<td><?php echo $μt; ?></td>
		<td><?php echo $totalresultval; ?></td>
		</tr>
		<?php
		$yearGroupMean = 0;
		$μ = 0;
		$resultval = 0;
		if($groupByStudent){
		foreach($groupByStudent as $studentData){ 
		$noOfStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		$totalnumberofans = 0;
		$user_id_worker = array();
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id']));
			$totalnumberofans = $totalnumberofans + $quetsionsAnswersData['totalCorrectAnswer'];
			 array_push($user_id_worker, $quetsionsAnswersData['totalCorrectAnswer']);
			 
		}
		
		$numKeys = count($user_id_worker);
		$mean= 0; 
		$totalval =0;
		$μ = 0;
		$resultval = 0;
		
		if($numKeys){
			for($j=0;$j<$numKeys ;$j++){
			$totalval  =$totalval + $user_id_worker[$j];	
			}
		}
		$μ = round($totalval/$numKeys,2);
		$z= 0;
		$t=0;
		$x =0;
		if($numKeys){
			for($k=0;$k<$numKeys ;$k++){
			$z  = $user_id_worker[$k] -  $μ;
			$t = $z*$z;
			
			$x = $x+$t;
				
			}
		}
		$resultval = round(sqrt($x/$numKeys),2);
		
		
		
		?>
		<tr>
		<td><?php echo $studentData['Student']['teacher_name']; ?></td>
		<td><?php echo $μ ; ?></td>
		<td><?php echo $resultval; ?></td>
		</tr>
		<?php }}?>
			
		
	</tbody>
</table></br>
				  
				  
				  
                </div>
					<div class="prof_details prof_form">
						<div class="attempt_details">
							<div class="attempt_qus">
								<ul>
									<li><span>Total Student(s) : </span> <strong><?php echo $no_of_student_appear;?> </strong></li>
									<!--<li><span>Pass Student(s) : </span> <strong> <?php //echo $pass ?></strong></li>
									<li><span>Fail Student(s) : </span> <strong> <?php //echo $fail;?> </strong></li>-->
									<!--<li><span>Average Correct Answer : </span> <strong><?php //echo @$avereageCorrectAns;?></strong></li>-->
									
								</ul>
							</div>
						</div>
					</div>      
				<div class="clear_fix"></div>
				
				
              </div>
            </div>
			

			
			
	<div class="table-responsive">
	<table id="example" class="display student-list-apr" border="1" bordercolor="#fff" cellspacing="0" style="width:99%"  width="99%">
				<thead>
					<tr>
			<th style="padding:10px 12px;" id="arrow_hide">List</th>
			<th style="padding:10px 12px;">Student Name</th>
			<!--<th style="padding:10px 12px;">Student Class</th>-->
			<th style="padding:10px 12px;">Student's Class</th>
			<th style="padding:10px 12px;">Student Email/Student ID</th>
			<th style="padding:10px 12px;">Student's Score</th>
			<th style="padding:10px 12px;">Score as %</th>
			<th style="padding:10px 12px;">Rank in year group</th>
			<th style="padding:10px 12px;">Rank in class</th>
			<th style="padding:10px 12px;">No of questions attempted</th>
			<th style="padding:10px 12px;">View student's individual report</th>
			
		
					</tr>
				</thead>
				<tbody>
					
				<?php
	        	if(!empty($studentList)){ //pr($studentList);
					$i = 1;
					foreach($studentList as $val){
						 $student_ans_random_id = $val['MyExamStudentAnswer']['unique_flag'];
				?>
		<tr>
			
			<td><?php echo $i;?></td>
			<td><?php echo $val['Student']['first_name'].' '.$val['Student']['last_name'];?></td>
			<!--<td><?php //echo @$averageCorrectAnswer;?></td>-->
			<td><?php echo $val['Student']['teacher_name'];?></td>
			<td><?php echo $val['Student']['email'];?></td>
			 <?php 
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $val['Student']['id']));
        
			?>
			
			<td><?php  echo $quetsionsAnswersData['totalCorrectAnswer'];?></td>
			<td>
			<?php
				 if ($quetsionsAnswersData['totalCorrectAnswer'] != 0) {
					$percentage = ($quetsionsAnswersData['totalCorrectAnswer'] * 100) / $val['MyExamStudentAnswer']['total_set_question'];
					echo round($percentage,2);
				}
			?>
			
			</td>
			<td><?php echo @$val['MyExamStudentAnswer']['year_group_rank'];?></td>
			<td><?php echo @$val['MyExamStudentAnswer']['class_rank'];;?></td>
			<td><?php echo $quetsionsAnswersData['totalAttempQuestions'];?></td>
			<td>
			
			 <?php if($category[1] != 3 && $category[1] != 5){ // 3,5 means below lines not for 3 and 5 years?>
            
			<!--<a href="javascript:void(0);" class="" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewResultsIndividual<?php echo $i;?>">View Result</a>-->
			<a target="_blank" title="View Result" href="<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'].'/'.$cal_non_cal.'/'.$school_purchase_exam_id;?>">View Result</a>

			 <?php } else {?>	
			<!--<a target="_blank" title="View Result" href="<?php //echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'].'/'.$school_purchase_exam_id;?>">View Result</a>-->
			<a target="_blank" title="View Result" href="<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'];?>">View Result</a>
			 <?php }?>
			
			
			
			
			</td>
		</tr>
		
		
		
		 <!--3rd modal -->
<div class="modal fade viewResultsIndividual<?php echo $i;?>"  tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Individual Student Results</h4>
            </div>
            <div class="modal-body">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'].'/non-calculator/'.$school_purchase_exam_id;?>')">Non-Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'].'/calculator/'.$school_purchase_exam_id;?>')">Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'].'/combined/'.$school_purchase_exam_id;?>')">Combined Results</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
		
		
		
		
		
		
	<?php 
				$i++;
				}
			}
	?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>

	
<!-- Start Add And edit Exam Modal-->

<div class="modal fade" id="viewStudentCorrectAns" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!--2nd modal -->
<div class="modal fade" id="viewEachQuestionAnswer" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--3rd modal -->
<div class="modal fade" id="viewEachQuestion" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
	<script>
	function delete_student(student_id){
		var r = confirm('Are you sure to delete this record?');
		if(r == true){
			$('#strow_' + student_id).hide();
			$.ajax({
				  url : '<?php echo $this->webroot.'schools/delete_student'?>',
				  type : 'POST',
				  data : {'student_id' : student_id},
				  success : function(responce){
					  //alert(responce);
				  }
			});
		}
		
	}
	</script>
	<style>
	#arrow_hide {
		background-image: none !important;
		pointer-events: none;
		cursor: default;
	}
	#example_filter {
		margin-right:0.4%;
	}
	</style>