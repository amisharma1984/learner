<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
		/*$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );*/
	</script>
<script>
	$(document).ready(function() {
			$('#example').dataTable( {
			"order": [[ 3, 'asc' ]],// order by column 3 asc (i.e order by Percentage of students selecting the correct answer)
		  "columnDefs": [
			{ "visible": false, "targets": 5 }, // for hide 6th column
			{ "orderable": false, "targets": 4 } // 4th column orderable/up down arrow hide i.e false
		  ]
		} );
	} );
	</script>

<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Full analysis of Student Results and Results by Syllabus Reference</h2>
                   <?php 
				    $examCategory = $this->requestAction(array('controller' => 'schools','action' => 'fetch_exam_category',$questionList[0]['Examination']['examination_category_id']));
					
					$category = explode(' ',$examCategory['ExaminationCategory']['name']);
				  ?>
               
				
				<table style="width:55%" border="1">
	<tbody>
		<tr>
		<th>&nbsp;</th>
		<th>Mean</th>
		<th>Standard Deviation</th>
		</tr>
		
		<tr>
		<td><?php echo 'Year '.$category[1];?> Group</td>
		<?php
		$yearGroupMean = 0;
		$groupmean=0;
		$standardDeviation1 =0;
		$μt = 0;
		$resultval = 0;
		if($groupByStudent){
			$countStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student_by_category',$groupByStudent[0]['Student']['examination_category_id']));
		foreach($groupByStudent as $studentData){
		
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student_byexamcat',$studentData['Student']['examination_category_id']));
		
		//pr($listStudent);
		$list_student_correct = array();
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id']));
			 array_push($list_student_correct, $quetsionsAnswersData['totalCorrectAnswer']);
		}
		
		 
		?>
		<?php 
			}
			$totnumKeys = count($list_student_correct);
			$totalmean= 0; 
			$totalvalue =0;
			$μt = 0;
			$totalresultval = 0;
			if($totnumKeys){
			for($l=0;$l<$totnumKeys ;$l++){
			$totalvalue  =$totalvalue + $list_student_correct[$l];	
			}
			}
			$μt = round($totalvalue/$totnumKeys,2);
			
			$z1= 0;
			$t1=0;
			$x1 =0;
			if($totnumKeys){
				for($n=0;$n<$totnumKeys ;$n++){
				$z1  = $list_student_correct[$n] -  $μt;
				$t1 = $z1*$z1;
				
				$x1 = $x1+$t1;
					
				}
			}
		    $totalresultval = round(sqrt($x1/$totnumKeys),2);
			
		}
		?>
		
		<td><?php echo $μt; ?></td>
		<td><?php echo $totalresultval; ?></td>
		</tr>
		<?php
		$yearGroupMean = 0;
		$μ = 0;
		$resultval = 0;
		if($groupByStudent){
		foreach($groupByStudent as $studentData){ 
		$noOfStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		$totalnumberofans = 0;
		$user_id_worker = array();
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id']));
			$totalnumberofans = $totalnumberofans + $quetsionsAnswersData['totalCorrectAnswer'];
			 array_push($user_id_worker, $quetsionsAnswersData['totalCorrectAnswer']);
			 
		}
		
		$numKeys = count($user_id_worker);
		$mean= 0; 
		$totalval =0;
		$μ = 0;
		$resultval = 0;
		
		if($numKeys){
			for($j=0;$j<$numKeys ;$j++){
			$totalval  =$totalval + $user_id_worker[$j];	
			}
		}
		$μ = round($totalval/$numKeys,2);
		$z= 0;
		$t=0;
		$x =0;
		if($numKeys){
			for($k=0;$k<$numKeys ;$k++){
			$z  = $user_id_worker[$k] -  $μ;
			$t = $z*$z;
			
			$x = $x+$t;
				
			}
		}
		$resultval = round(sqrt($x/$numKeys),2);
		
		
		
		?>
		<tr>
		<td><?php echo $studentData['Student']['teacher_name']; ?></td>
		<td><?php echo $μ ; ?></td>
		<td><?php echo $resultval; ?></td>
		</tr>
		<?php }}?>
			
		
	</tbody>
</table></br>
                  
                </div>
					<div class="prof_details prof_form">
						<div class="attempt_details">
							<div class="attempt_qus">
								<ul>
									<li><span>Total Student(s) : </span> <strong><?php echo $no_of_student_appear;?> </strong></li>
									<!--<li><span>Pass Student(s) : </span> <strong> <?php echo $pass ?></strong></li>
									<li><span>Fail Student(s) : </span> <strong> <?php echo $fail;?> </strong></li>-->
									<!--<li><span>Average Correct Answer : </span> <strong><?php //echo @$avereageCorrectAns;?></strong></li>-->
									
								</ul>
							</div>
						</div>
					</div>      
				<div class="clear_fix"></div>
				
				
              </div>
            </div>
			

			
			
	<div class="prof_details prof_form">
	<table id="example" class="display student-list-apr" style="width:99.8%;" border="1" bordercolor="#fff" cellspacing="0">
				<thead>
					<tr>
			<th>Syllabus Reference code for this question <br><font color="green">click view to see reference details</font></th>
			<th>Question Number<br><font color="green">click on the number to see question details</font></th>
			<th>Number of students who chose the correct option</th>
			<th>Percentage of students who chose the correct option</th>
			<th>Click <font color="green">View</font> in this column to see the number, percentage and student list for each option</th>
			<!--<th>Students(%) who gives correct answer</th>
			<th>Students(%) who gives wrong answer</th>-->
			
			    <!--<th>Standard Deviation</th>-->
                        <th>&nbsp;</th>
			
					</tr>
				</thead>
				<tbody>
					
					 <?php
	        	if(!empty($questionList)){ 
					$i = 1;
					foreach($questionList as $val){
						 $question_id = $val['Question']['id'];
						//echo $school_purchase_exam_id;die;
					    $fetch_student_correct_answer = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_student_correct_answer',$school_purchase_exam_id, $question_id));
                        list($no_of_student_selecting_correct_ans, $percentageStudent) = explode('##', $fetch_student_correct_answer);                    
					    $percentageWrongStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_student_wrong_answer',$school_purchase_exam_id, $question_id));
						$standardDeviation = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_standard_deviation',$school_purchase_exam_id, $question_id));                                            
					    $school_pid = $this->requestAction(array('controller' => 'App', 'action' => 'data_decrypt',$school_purchase_exam_id));
	 ?>
		<tr>
			<td>
			<?php echo $val['Question']['sub_strands_code'];?><span style="float:right; color:#f69d25; font-size:14px;"> <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".view_sub_strands_description" onclick="get_sub_strands_description(<?php echo $question_id;?>)">View</a></span>
			</td>
			<td><a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewEachQuestion" onclick="get_each_particular_question(<?php echo $question_id;?>,<?php echo $school_pid;?>,<?php echo $i;?>)"><?php echo $i;?></a></td>
			<td><?php echo @$no_of_student_selecting_correct_ans; //@$percentageStudent;?>
			<?php /*if($percentageStudent >0) {?>
			<span style="float:right; color:#f69d25; font-size:14px;">
			  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewStudentCorrectAns" onclick="get_student_who_gives_correct_ans(<?php echo $question_id;?>,<?php echo $school_pid;?>)" class="student_list_ans" data-student-qid="<?php echo $question_id;?>" data-student="<?php echo $school_purchase_exam_id;?>">View Student</a>
			</span>
			<?php } */?>
			</td>
			<td><?php echo @$percentageStudent;?></td>
			
			<!--<td><?php //echo @$averageCorrectAnswer;?></td>-->
			<!--<td>
			<?php //echo @$percentageWrongStudent;?>
				<?php //if($percentageWrongStudent >0) {?>
			<span style="float:right; color:#f69d25; font-size:14px;">
			  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewStudentCorrectAns" onclick="get_student_who_gives_wrong_ans(<?php echo $question_id;?>,<?php echo $school_pid;?>)"  class="student_list_ans" data-student="<?php echo $school_purchase_exam_id;?>">View Student</a>
			</span>
			<?php //}?>
			</td>
                      
			<td><?php //echo @$standardDeviation;?></td>-->
			<td style="text-align:center">
			<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".clickViewToSee" onclick="click_view_to_see(<?php echo $question_id;?>,<?php echo $school_pid;?>, <?php echo $i;?>)" title="Click to view question">View</a>
		
			<?php //echo @$standardDeviation;?>
			</td>
			
			 <td>&nbsp;</td>
		</tr>
	<?php 
				$i++;
				}
			}
	?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>

	
<!-- Start Add And edit Exam Modal-->
<div class="modal fade" id="viewStudentCorrectAns" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!--2nd modal -->
<div class="modal fade" id="viewEachQuestionAnswer" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--3rd modal -->
<div class="modal fade" id="viewEachQuestion" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--4th modal -->
<div class="modal fade clickViewToSee" id="clickViewToSee___" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog rect_box_modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><img src="<?php echo $this->webroot.'img/loading_bar.gif'?>"></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!--5th modal -->

<div class="modal fade studentList4eachOption" id="" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--6th modal -->

<div class="modal fade view_sub_strands_description" id="" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<script>

function get_student_who_gives_correct_ans(question_id, school_purchase_exam_id){
			 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_correct_ans';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
						$('#viewStudentCorrectAns').find('.modal-title').html('<font color="green">Student list which gives correct answer</font>');
					  }
					  
				});
}

function get_student_who_gives_wrong_ans(question_id, school_purchase_exam_id){
			 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_wrong_ans';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
						$('#viewStudentCorrectAns').find('.modal-title').html('<font color="red">Student list which gives wrong answer</font>');
					  }
					  
				});
}

function get_each_particular_question_ans(student_id, question_id, school_purchase_exam_id){
			 var url = '<?php echo $this->webroot;?>schools/get_each_particular_question_ans';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id, student_id : student_id},
					  success : function(data){
						$('#viewEachQuestionAnswer').find('.modal-body').html(data);
						$('#viewEachQuestionAnswer').find('.modal-title').html('<font color="green">Each question answer details</font>');
					  }
					  
				});
}



	/*$(document).ready(function(){
			 $('.student_list_ans').click(function(event){
				var school_purchase_exam_id = $(this).attr('data-student');
				var question_id = $(this).attr('data-student-qid');
				 var url = '<?php echo $this->webroot;?>schools/get_student_who_gives_correct_ans';
				
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
					  success : function(data){
						$('#viewStudentCorrectAns').find('.modal-body').html(data);
					  }
					  
				});
			
			});
	});
	*/
           function answer_details(){
			$('#details_showhide').toggle();
		}
      function get_each_particular_question(question_id, school_purchase_exam_id, question_no){
            var url = '<?php echo $this->webroot;?>schools/get_each_particular_question';
                   $.ajax({
                             url : url,
                             type : 'POST',
                             data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id, question_no:question_no},
                             success : function(data){
                                   $('#viewEachQuestion').find('.modal-body').html(data);
                                   //$('#viewEachQuestion').find('.modal-title').html('<font color="green">Each question details</font>');
                                   $('#viewEachQuestion').find('.modal-title').html(' ');
                             }

           });
        }
		
		function click_view_to_see(question_id, school_purchase_exam_id, question_no){
			 $('.clickViewToSee').find('.modal-body').html('<img src="<?php echo $this->webroot.'img/loading_bar.gif'?>">');
            var url = '<?php echo $this->webroot;?>schools/click_view_to_see';
                   $.ajax({
                             url : url,
                             type : 'POST',
                             data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id, question_no : question_no},
                             success : function(data){
                                   $('.clickViewToSee').find('.modal-body').html(data);
                                   //$('.clickViewToSee').find('.modal-title').html('<font color="green">Each question details</font>');
                                   $('.clickViewToSee').find('.modal-title').html('');
                             }

           });
        }
		
	function get_student_list_for_each_option(school_purchase_exam_id, question_id, answer_id){ 
			 var url = '<?php echo $this->webroot;?>schools/get_student_list_for_each_option';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id, answer_id : answer_id},
					  success : function(data){ 
						$('.studentList4eachOption').find('.modal-body').html(data);
						$('.studentList4eachOption').find('.modal-title').html('<font color="green">Student list</font>');
					  }
					  
				});
}
	
	
	function get_sub_strands_description(question_id){ 
			 var url = '<?php echo $this->webroot;?>schools/get_sub_strands_description';
				$.ajax({
					  url : url,
					  type : 'POST',
					  data : {question_id : question_id},
					  success : function(data){ 
						$('.view_sub_strands_description').find('.modal-body').html(data);
						$('.view_sub_strands_description').find('.modal-title').html('<font color="green">Syllabus Reference</font>');
					  }
					  
				});
}
	
		
</script>
