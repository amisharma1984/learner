<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
  <style>
.rgerror {
    color: red;
    font-size: 13px;
    margin-left: 44.5%;
}

</style>  
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2><!--School Profile--></h2>
				 <h2 style="text-align:center; color:#119548;"><?php echo @$schoolData['School']['name'];?></h2>
                  <h3> <?php //echo ucwords($this->data['School']['manager_name']); ?></h3>
              
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('School'); 
				 echo $this->Form->input('id', array('label' => false, 'type' => 'hidden'));
				 echo $this->Form->input('SchoolInformation.id', array('label' => false, 'type' => 'hidden'))
				 ?>
				 <div class="row">
				 <div style="float:right;color:#f69d25;margin:0 18px 20px 0">Last Login : 
				 <?php 
				 if(!empty($lastLoginSchool)){
					  echo date("D, jS \of F Y, h:i:s A", strtotime($lastLoginSchool));
				 }
				
				 
				 ?>
				 </div>
				 </div>
                  <div class="row">
                    <div class="col-md-6 form_info">
                     
					 <div class="form-group">
                        <label >School Name :</label>
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control inp_text'))?>
						 <?php if(!empty($nameErr)){?>
						 <span  class="rgerror"><?php echo @$nameErr;?> </span>
						 <?php }?>
                        <div class="clear_fix"></div>
                      </div>
					  
                    <div class="form-group">
                        <label >Head Teacher Name :</label>
						<?php echo $this->Form->input('manager_name', array('label' => false, 'class' => 'form-control inp_text'))?>
						  <?php if(!empty($managerNameErr)){?>
						 <span  class="rgerror"><?php echo @$managerNameErr;?> </span>
						 <?php }?>
                        <div class="clear_fix"></div>
                      </div>
					 
					    <div class="form-group">
                        <label >Level :</label>
                       <?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false, 'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
					   <?php if(!empty($levelErr)){?>
					   <span  class="rgerror"><?php echo @$levelErr;?> </span>
					   <?php }?>
                        <div class="clear_fix"></div>
                      </div>
                      
					  
                      <div class="form-group">
                        <label >Street :</label>
						  <?php echo $this->Form->input('SchoolInformation.street', array('label' => false,'type' => 'text', 'div' => false,  'class' => 'form-control inp_text'))?>
						  <?php if(!empty($streetErr)){?>
						  <span  class="rgerror"><?php echo @$streetErr;?> </span>
						  <?php }?>
                        <div class="clear_fix"></div>
                      </div>
					  
					 <!--    <div class="form-group">
                        <label >District  :</label>
						<?php //echo $this->Form->input('SchoolInformation.district', array('label' => false,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php //echo @$districtErr;?> </span>
						<div class="clear_fix"></div>
                      </div>-->
					  
					    <div class="form-group">
                        <label>Postcode  :</label>
						<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <?php if(!empty($zipErr)){?>
						 <span  class="rgerror"><?php echo @$zipErr;?> </span>
						 <?php }?>
						<div class="clear_fix"></div>
                      </div>
					  
					     <div class="form-group">
                        <label >Fax  :</label>
						<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                         
						<div class="clear_fix"></div>
                      </div>
					  
					  
					    <div class="form-group">
                        <label >School website address:</label>
							<?php echo $this->Form->input('SchoolInformation.url', array('label' => false,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                       
						<div class="clear_fix"></div>
                      </div>
					  
					  
					  
					  
                    </div>
					
					
					
					
					
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >School Email:</label>
						<?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control inp_text'))?>
							 <?php if(!empty($emailErr)){?>
							 <span  class="rgerror"><?php echo @$emailErr;?> </span>
							 <?php }?>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Head Teacher Email  :</label>
							<?php echo $this->Form->input('manager_email', array('label' => false,  'class' => 'form-control inp_text'))?>
							 <?php if(!empty($manager_emailErr)){?>
							 <span  class="rgerror"><?php echo @$manager_emailErr;?> </span>
							 <?php }?>
                        <div class="clear_fix"></div>
                      </div>
                 
				   <div class="form-group">
                        <label > Type :</label>
					  <?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
					  <?php if(!empty($typeErr)){?>
					  <span  class="rgerror"><?php echo @$typeErr;?> </span>
					  <?php }?>
                        <div class="clear_fix"></div>
                      </div>
				 
                      <div class="form-group">
                        <label >City / Town :</label>
							<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'type' => 'text',  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
							
						  <div class="clear_fix"></div>
                      </div>
					  
					    <div class="form-group">
                        <label > State  :</label>
						<?php echo $this->Form->input('SchoolInformation.state', array('label' => false,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <?php if(!empty($scerror)){?>
					    <span  class="rgerror"><?php echo @$scerror;?> </span>
					   <?php }?>
						<div class="clear_fix"></div>
                      </div>
					  
					  <div class="form-group">
                        <label > Phone  :</label>
							<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                       
						<div class="clear_fix"></div>
                      </div>
					  
					
					  
					  
					  
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn" value="Submit">
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>