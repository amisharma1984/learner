<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Purchased Exam List</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table class="student-list-apr">
  <tr>
  <th>List</th>
    <th>Exam Name</th>
    <!--<th>Type</th>-->
    <th>Category</th>
	<th>Price per subject<br> per student <br>($ AUD)</th>
	<th>No. of<br> Student</th>
	<th>Exam Code</th>
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($myPurchasedExam)){
	  $i = 1;
	  foreach($myPurchasedExam as $val){
		    $id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolPurchaseExam']['id']));
		    //$examination_id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolPurchaseExam']['examination_id']));
		    $no_of_student_appear = $this->requestAction(array('controller' => 'schools', 'action' => 'get_student_appear_in_exam', $val['SchoolPurchaseExam']['id']));
			
				//if($val['Examination']['ExaminationCategory']['name'] != 'Years 3 Exam' && $val['Examination']['ExaminationCategory']['name'] != 'Years 5 Exam'){ // this line code need to remove  latter
			
		  ?>
		  <tr>
		<td><?php echo $i?></td>
    <td><?php echo ucwords(strtolower($val['Examination']['title']));?></td>
  <!--  <td><?php //echo $val['Examination']['ExaminationType']['name'];?></td>-->
    <td><?php echo $val['Examination']['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['Examination']['price'];?></td>
	 <td><?php echo @$val['SchoolPurchaseExam']['no_of_student'];?></td>
	 <td width="150"><?php echo @$val['SchoolPurchaseExam']['exam_code'];?></td>
	  <td width="135">
	  <?php if($no_of_student_appear > 0){?>
	  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewReport"   class="student_list_ans" >Reports</a>
	  <!-- &nbsp;|&nbsp;
	   <a href="<?php //echo $this->webroot.'schools/result_chart/'.$id;?>"><span style="color:orange;">Report</span></a>-->
	  <?php } else {?>
	     <a href="javascript:void(0);" style="color:orange; text-decoration:none; cursor:text;">Exam not taken</a>
	  <?php }?>
	 <!--<a href="<?php //echo $this->webroot.'schools/create_exam/'.$id;?>">Create Exam</a>-->
	  </td>
  </tr>
<?php
$i++;
	 // }
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--1st modal -->
<div class="modal fade" id="viewReport" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> Reports</h4>
            </div>
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id;?>')">Student Wise Result</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$id;?>')">Question Wise Report</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id;?>')">Sub-strands Wise Report</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_questions_result_chart/'.$id;?>')">Student Questions Wise Report</button>
      
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function openUrl(url){
            var win = window.open(url, '_blank');
    }
    </script>