<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			
			<!-- <div class="contact_head">
				<h2>
				<span>School Registration</span>
				<strong>Form</strong>
				</h2>
				
			</div> -->
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<?php /* ?><div class="col-md-4">
				       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				  echo $this->Form->create('SchoolLogin'); 
				 echo $this->Form->input('SchoolLogin.login_registration_flag', array('type' => 'hidden', 'value' => 'school_login'));
				 ?>
						<div class="form_info">
							<h2>Sign in your account</h2>
							<div class="form-group">
								<i class="fa fa-user" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.login', array('label' => false,  'placeholder' => 'Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.password', array('label' => false,  'placeholder' => 'Password ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginPassErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="checkbox form-group">
								<label><input type="checkbox"> Remember me</label>
								<!--<a href="" class="formbtn forget"> Forget Password?</a>--><br>
							</div>
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Submit</button>
							</div>
							<div class="form-group adjpad">
								Don’t have an account? <a href="" > Sign Up!</a>
								
							</div>
							
							
							
							
							
							
							
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div><?php */ ?>
					<div class="col-md-12">
       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('School'); 
				 ?>


						<div class="row">
							<div class="col-md-offset-1 col-md-10">
								<div class="form_info">
									<h2>Sign up for school account</h2>
									
									<div class="form_cont">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
												<?php echo $this->Form->input('name', array('label' => false,  'placeholder' => 'School Name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('email', array('type' => 'text',  'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'School Email '))?>
												<span  class="rgerror"><?php echo @$emerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-address-book" aria-hidden="true"></i>

												<?php echo $this->Form->input('manager_name', array('label' => false,  'placeholder' => 'Head Teacher Name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$mNameErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('manager_email', array('label' => false, 'type' => 'text',  'placeholder' => 'Head Teacher Email', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$manager_emailErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => 'Select Level',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select','empty'=>'Select Type', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('SchoolInformation.street', array('label' => false,'type' => 'text',  'placeholder' => 'Street', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$streetErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-location-arrow" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'type' => 'text',  'placeholder' => 'City / Town ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$townErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>


												<?php //echo $this->Form->input('SchoolInformation.district', array('label' => false,  'placeholder' => 'District', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>-->
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.state', array('label' => false,  'placeholder' => 'State ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$stateErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-codepen" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false,  'placeholder' => 'Post Code ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$zipErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-phone" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false,  'placeholder' => 'Phone', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$phoneErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-fax" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false,  'placeholder' => 'Fax', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$faxErr;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-link" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.url', array('label' => false,  'placeholder' => 'School website address', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php //echo @$urlErr;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											
											<div class="row">
												<div class="col-sm-7">

												<?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','placeholder' => 'Type the captcha text here','type'=>'text', 'value' => @$captcha_txt, 'id'=>'captcha_txt'));?>
												<span  class="rgerror"><?php echo @$captchaErr;?> </span>
												<div class="clear_fix"></div>
												</div>
												<div class="col-sm-5 led_wid">
													<label >
															<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
															<a href="javascript:void(0);"
																onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
															<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
														</label>
													</div>
										</div>
											
										
											</div>
										</div>
									</div>
									
									
								<div class="row">
										
										<div class="col-md-6">
											<div class="form-group" style="padding:15px 0 0 0;">
											<span style="float:left; margin-right:7px;" >Has your school been a customer in previous years?</span>
												
												<?php
												$option = array(0 => 'No',1 => 'Yes');
												
												echo $this->Form->input('SchoolInformation.isprevious' ,array('options' => $option,'class' => 'mr-xs','hiddenField' => false,'label'=>false, 'id' => 'SchoolInformationIsPrevious','default'=>0) );

												//echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); 
												?>
												
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="submit" class="btn btn-info def_btn" value="Submit">
												<div class="clear_fix"></div>
											</div>
										</div>
										
										
									</div>
									
									
									
								</div>
							</div>
							</div>
						</div>

						<?php echo $this->Form->end(); ?>
					</div>
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>