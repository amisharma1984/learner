<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
				<h2 style="text-align:center; color:#119548;"><?php echo @$schoolData['School']['name'];?></h2>
                <h2>List of Exams Purchased</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                   
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table class="student-list-apr" style="border: 1px solid pink;">
  <tr>
  <th>List</th>
   
    <!--<th>Type</th>-->
    <!--<th>Year</th>-->
	<th>Exam Name</th>
    <th>Category</th>
	 
	<th>Price per student <br>($ AUD)</th>
	<th>No. of<br> Student</th>
	<th>Total Paid</th>
	<th>Exam Code</th>
	<th>Date of Exam taken</th>
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($myPurchasedExam)){ //pr($myPurchasedExam);
	  $i = 1;
	  foreach($myPurchasedExam as $val){
		   $category = explode(' ',$val['Examination']['ExaminationCategory']['name']);
		    
			$examTitle = explode(' ',ucwords(strtolower($val['Examination']['title'])));
			$inserted = array();
			$inserted[] = 'Year '.$category[1];
			array_splice( $examTitle, 1, 0, $inserted );
		   $examName = implode(' ', $examTitle);
		 
		   
		    $id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolPurchaseExam']['id']));
		    $examination_id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolPurchaseExam']['examination_id']));
		    $no_of_student_appear = $this->requestAction(array('controller' => 'schools', 'action' => 'get_student_appear_in_exam', $val['SchoolPurchaseExam']['id']));
		    $exam_taken_date = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_exam_taken_date', $val['SchoolPurchaseExam']['school_id'],$val['SchoolPurchaseExam']['id']));
			
		  ?>
		  <tr>
		<td><?php echo $i?></td>
   
	 <!--<td><?php //echo $category[1]; ?></td>-->
  <!--  <td><?php //echo $val['Examination']['ExaminationType']['name'];?></td>-->
    <td><?php echo $examName;?></br></br>
	
	<?php if($category[1] != 3 && $category[1] != 5){ // 3,5 means below lines not for 3 and 5 years?>
	<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewExam<?php echo $id;?>"  class="student_list_ans" >View Exam</a>
	 <?php } else {?>
	<a target="_blank" href="<?php echo $this->webroot.'schools/view_exam/'.$examination_id;?>">View Exam</a>
	 <?php }?>
	 
	</td>
	<td><?php echo 'NAPLAN Style Trial';?></td>
	 
	 <td><?php echo '$'.$val['Examination']['price'];?></td>
	 <td><?php echo @$val['SchoolPurchaseExam']['no_of_student'];?></td>
	 <td><?php echo '$'.@$val['SchoolPurchaseExam']['gross_total'];?></br>
	  <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewInvoice<?php echo $id;?>"   class="student_list_ans" >View tax invoice</a>
	 
	
	 
	 </td>
	 <td><?php echo @$val['SchoolPurchaseExam']['exam_code'];?></td>
	 <td><?php echo !empty($exam_taken_date) ? date('d/m/Y', strtotime($exam_taken_date)) : 'N/A';?></td>
	  <td width="135">
	  <?php if($no_of_student_appear > 0){?>
	   <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewReport<?php echo $id;?>" onclick="onClickEvent('<?php echo $id;?>')"  class="student_list_ans" >View exam results</a>
	  <!-- &nbsp;|&nbsp;
	   <a href="<?php //echo $this->webroot.'schools/result_chart/'.$id;?>"><span style="color:orange;">Report</span></a>-->
	  <?php } else {?>
	     <a href="javascript:void(0);" style="color:orange; text-decoration:none; cursor:text;">Exam not taken</a>
	  <?php }?>
	 <!--<a href="<?php //echo $this->webroot.'schools/create_exam/'.$id;?>">Create Exam</a>-->
	  </td>
  </tr>
      <!--1st modal -->
<div class="modal fade viewReport<?php echo $id;?>" id="viewReport____" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Reports</h4>
            </div>
            <div class="modal-body">
					 <?php if($category[1] != 3 && $category[1] != 5){ // 3,5 means below lines not for 3 and 5 years?>
                    <!--<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id;?>')">Individual Student Results</button>-->
                    <button type="button" class="btn btn-secondary btn-lg btn-block" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewResultsByIndividualStudent<?php echo $id;?>">Individual Student Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewReportByType<?php echo $id;?>">Results by Question</button>
					<button type="button" class="btn btn-secondary btn-lg btn-block" style="margin-bottom: 10px;" data-toggle="modal" data-target=".viewReportBySyllabusRef<?php echo $id;?>">Full analysis of Student Results and Results by Syllabus Reference</button>
					
	               <?php } else {?>
					<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id;?>')">Individual Student Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$id;?>')">Results by Question</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id;?>')">Full analysis of Student Results and Results by Syllabus Reference</button>
                    
					<?php }?>
					
					
					<!--<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php //echo $this->webroot.'schools/result_chart/'.$id;?>')">Results by Question</button>-->
                    <!--<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id;?>')">Full analysis of Student Results and Results by Syllabus Reference</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_questions_result_chart/'.$id;?>')">Student Questions Wise Report</button>-->
      
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
 <!--2nd modal -->
<div class="modal fade viewReportByType<?php echo $id;?>"  tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Results by Question</h4>
            </div>
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$id.'/non-calculator';?>')">Non-Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$id.'/calculator';?>')">Calculator Results</button>
                    <!--<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php //echo $this->webroot.'schools/result_chart/'.$id;?>')">Combined Results</button>-->
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

 <!--3rd modal -->
<div class="modal fade viewReportBySyllabusRef<?php echo $id;?>"  tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Full analysis of Student Results and Results by Syllabus Reference</h4>
            </div>
            <div class="modal-body">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id.'/non-calculator';?>')">Non-Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id.'/calculator';?>')">Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$id.'/combined';?>')">Combined Results</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>


 <!--4th modal -->
<div class="modal fade viewExam<?php echo $id;?>"  tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">View Exam :</h4>
            </div>
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/view_exam/'.$examination_id.'/non-calculator';?>')">Non-Calculator</button>
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/view_exam/'.$examination_id.'/calculator';?>')">Calculator</button>
                  
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

 <!--5th modal -->
<div class="modal fade viewResultsByIndividualStudent<?php echo $id;?>"  tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;">Individual Student Results</h4>
            </div>
            <div class="modal-body">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id.'/non-calculator';?>')">Non-Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id.'/calculator';?>')">Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$id.'/combined';?>')">Combined Results</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>


<!-- view invoice paopup start -->
<div class="modal fade viewInvoice<?php echo $id;?>" id="viewReport____" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff; text-align:center;"> Tax Invoice</h4>
            </div>
            <div class="modal-body">
					<span class="btn btn-secondary btn-lg btn-block">Price per student($ AUD) : <?php echo '$'.$val['Examination']['price'];?></span>
					<span class="btn btn-secondary btn-lg btn-block">No. of Student : <?php echo @$val['SchoolPurchaseExam']['no_of_student'];?></span>
					<span class="btn btn-secondary btn-lg btn-block">Sub Total ($ AUD) : <?php echo @$val['SchoolPurchaseExam']['amount'];?></span>
					<span class="btn btn-secondary btn-lg btn-block" style="border-bottom: 1px dashed #a1a1a1;">GST (10%) :<?php echo ($val['SchoolPurchaseExam']['amount'] * (10/100));?></span>
					<span class="btn btn-secondary btn-lg btn-block"><strong>Total Paid ($ AUD) : <?php echo @$val['SchoolPurchaseExam']['gross_total'];?></strong></span>
                    

            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!-- view invoice paopup End -->
  
  
<?php
$i++;
	
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<script type="text/javascript">
    function openUrl(url){
            var win = window.open(url, '_blank');
    }
	function onClickEvent(id){
		$.ajax({
            type: "POST",
            url: "<?php echo $this->webroot; ?>schools/update_schooldata",
            data: {'id':id},
            cache: false,
            async: true,
            success: function(response)
            {
				console.log(response);
			}

        });
	}
    </script>