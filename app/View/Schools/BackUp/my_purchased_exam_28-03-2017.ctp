<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Purchase Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
  <th>S.No.</th>
    <th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Price per subject<br> per student per year<br>($ AUD)</th>
	<!--<th>Exam Code</th>-->
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($myPurchasedExam)){
	  $i = 1;
	  foreach($myPurchasedExam as $val){
		    $id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolPurchaseExam']['id']));
		  ?>
		  <tr>
		<td><?php echo $i?></td>
    <td><?php echo $val['Examination']['title'];?></td>
    <td><?php echo $val['Examination']['ExaminationType']['name'];?></td>
    <td><?php echo $val['Examination']['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['Examination']['price'];?></td>
	 <!-- <td><?php //echo @$val['SchoolPurchaseExam']['exam_code'];?></td>-->
	  <td>
	 <a href="<?php echo $this->webroot.'schools/create_exam/'.$id;?>">Create Exam</a>
	  </td>
  </tr>
<?php
$i++;
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
