<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Student List</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
    <th> Name</th>
    <th>Email</th>
    <th>School Name</th>
	<th>Class</th>
	
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($studentList)){
	  foreach($studentList as $val){
		    $studentId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Student']['id']));
		  ?>
		  <tr id="strow_<?php echo $val['Student']['id']?>">
    <td><?php echo $val['Student']['first_name'].' '.$val['Student']['last_name'];?></td>
    <td><?php echo $val['Student']['email'];?></td>
    <td><?php echo $val['Student']['school_name'];?></td>
	 <td><?php echo $val['StudentClass']['name'];?></td>
	 
	  <td>
	 <a title="Edit" href="<?php echo $this->webroot.'schools/edit_student/'.$studentId;?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
	 <a title="Delete" href="javascript:void(0);" onclick="return delete_student(<?php echo $val['Student']['id'];?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
	  </td>
  </tr>
<?php
	  }
  }
?>
</table>
				 
				 
                </div>
				
				<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
	function delete_student(student_id){
		var r = confirm('Are you sure to delete this record?');
		if(r == true){
			$('#strow_' + student_id).hide();
			$.ajax({
				  url : '<?php echo $this->webroot.'schools/delete_student'?>',
				  type : 'POST',
				  data : {'student_id' : student_id},
				  success : function(responce){
					  //alert(responce);
				  }
			});
		}
		
	}
	</script>
