
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Student with questions List Appear in Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                   <!--<h3>Total No. of Student appear : <?php //echo @$no_of_student_appear; ?></h3>-->
                </div>
					<div class="prof_details prof_form">
						<div class="attempt_details">
							<div class="attempt_qus">
								<ul>
									<li><span>Total Student(s) : </span> <strong><?php echo $no_of_student_appear;?> </strong></li>
									<li><span>Pass Student(s) : </span> <strong> <?php echo $pass ?></strong></li>
									<li><span>Fail Student(s) : </span> <strong> <?php echo $fail;?> </strong></li>
									<!--<li><span>Average Correct Answer : </span> <strong><?php //echo @$avereageCorrectAns;?></strong></li>-->
									
								</ul>
							</div>
						</div>
					</div>      
				<div class="clear_fix"></div>
				
				
                <div class="prof_details prof_form">
                   <!-- content Start--> 
<table>
 
	<tr>
	 <?php  echo $this->Form->create('Student3'); ?>
		<td> Search By &nbsp;
		<select name="search_by">
		<option value="name">Name</option>
			<option value="email">Email</option>
		</select> 
		<input type="text" name="search_text">&nbsp;<input type="submit" value="Search">&nbsp;
		<input type="button" onclick="javascript:window.location.assign('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$school_purchase_exam_id;?>')" value="Show All"></td>
		<td>&nbsp;</td> 
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		  <?php echo $this->Form->end(); ?>
	</tr>
	
</table>
<div class="table-responsive">
<table class="table table-bordered">
  <tr>
      <th style="width:10% " >List</th>
  <th style="width:90%;">Questions
      <table class="table table-bordered">
          
          <tr>
             <?php  if(!empty($questionList)){ 
		$i = 1;
		foreach($questionList as $val){
                    $question_id = $val['Question']['id'];
                   
              ?>
                
              <th style="width:98px;" nowrap><?php  echo $i;?><span style="color:#f69d25; font-size:14px;"> <a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#viewEachQuestion" onclick="get_each_particular_question(<?php echo $question_id;?>,<?php echo $school_purchase_exam_id;?>)">View</a></span></th>
              <?php 
		$i++;
		}
		}
	       ?>
              
          </tr>
             
           <tr>
                 <?php  if(!empty($questionList)){ 
		
		foreach($questionList as $val){
                   
                   
              ?>
               <th><table class="table table-bordered">
                    
                      <tr>
                          
                          <th>Answer Given</th>
                      
                          <th>Answer Is:</th>
                      </tr>
             
              

      </table>
  </th>
    <?php 
		
		}
		}
	       ?>
  </tr>
      </table>
  </th>
  </tr>
  
  
  <?php
  if(!empty($studentList)){  //pr($studentList);

	  foreach($studentList as $val){ 
		   // $studentId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Student']['id']));
		  // $student_ans_random_id = $val['MyExamStudentAnswer']['student_ans_random_id'];
		   $student_ans_random_id = $val['MyExamStudentAnswer']['unique_flag'];
		  
                  $student_id = $val['Student']['id'];
		  ?>
		  <tr id="strow_<?php echo $val['Student']['id']?>">
		  
    <td style="width:10%"><?php echo $val['Student']['first_name'].' '.$val['Student']['last_name'];?></td>
    <td style="width:90%;" > <table class="table table-bordered"  >
          <tr>
             <?php if(!empty($questionList)){ 
		$i = 1;
		foreach($questionList as $val){
                     $question_id1 = $val['Question']['id'];
                     $correct_amser_id =  @$val['CorrectAnswer'][0]['id'];
                     $ans_text = @$val['CorrectAnswer'][0]['answer_text'];
              ?>
                
              <td style="width:100px;" nowrap>
                  <table class="table table-bordered">
                    
                      <tr>
                  <?php  
               
          
               $studentAnsofQuestion = $this->requestAction(array('controller' => 'schools', 'action' => 'get_student_each_particular_question_ans',$school_purchase_exam_id, $question_id1,$student_id));    
              if($studentAnsofQuestion==0){ ?>
                          <td><?php echo $i;?> : No </td> <td><?php echo $ans_text;?> </td> 
               <?php }else{
                    if($correct_amser_id == $studentAnsofQuestion){?>
                          <td><?php echo $i;?> : Yes </td><td> <?php echo $ans_text;?> </td> 
                 <?php  }else{?>
                       <td><?php echo $i;?> : Yes </td> <td><?php echo $ans_text;?> </td> 
                 <?php  }
                }
              ?>
                      </tr></table></td>
              <?php 
		$i++;
		}
		}
	       ?>
          </tr>
           
           
      </table></td>
  </tr>

<?php 
	  }
  }
?>
</table>
				 
</div>		 
                </div>
				
				<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<!--1st modal -->
<div class="modal fade" id="viewEachQuestion" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
	<script>
	function delete_student(student_id){
		var r = confirm('Are you sure to delete this record?');
		if(r == true){
			$('#strow_' + student_id).hide();
			$.ajax({
				  url : '<?php echo $this->webroot.'schools/delete_student'?>',
				  type : 'POST',
				  data : {'student_id' : student_id},
				  success : function(responce){
					  //alert(responce);
				  }
			});
		}
		
	}
        function get_each_particular_question(question_id, school_purchase_exam_id){

            var url = '<?php echo $this->webroot;?>schools/get_each_particular_question';
                   $.ajax({
                             url : url,
                             type : 'POST',
                             data : {school_purchase_exam_id : school_purchase_exam_id, question_id : question_id},
                             success : function(data){
                                   $('#viewEachQuestion').find('.modal-body').html(data);
                                   $('#viewEachQuestion').find('.modal-title').html('<font color="green">Each question details</font>');
                             }

           });
        }
        function answer_details(){
			$('#details_showhide').toggle();
		}
	</script>
