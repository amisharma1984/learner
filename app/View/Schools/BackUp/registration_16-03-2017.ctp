<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			
			<!-- <div class="contact_head">
				<h2>
				<span>School Registration</span>
				<strong>Form</strong>
				</h2>
				
			</div> -->
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-4">
				       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				  echo $this->Form->create('SchoolLogin'); 
				 echo $this->Form->input('SchoolLogin.login_registration_flag', array('type' => 'hidden', 'value' => 'school_login'));
				 ?>
						<div class="form_info">
							<h2>Sign in your account</h2>
							<div class="form-group">
								<i class="fa fa-user" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.login', array('label' => false,  'placeholder' => 'Name', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.password', array('label' => false,  'placeholder' => 'Password ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginPassErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="checkbox form-group">
								<label><input type="checkbox"> Remember me</label>
								<a href="" class="formbtn forget"> Forget Password?</a>
							</div>
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Submit</button>
							</div>
							<div class="form-group adjpad">
								Don’t have an account? <a href="" > Sign Up!</a>
								
							</div>
							
							
							
							
							
							
							
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
					<div class="col-md-8">
       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('School'); 
				 ?>


						<div class="row">
							<div class="col-md-12">
								<div class="form_info">
									<h2>Sign up your account</h2>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
												<?php echo $this->Form->input('name', array('label' => false, 'required' => 'required' ,  'placeholder' => 'School Name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('email', array('type' => 'email', 'required' => 'required email' , 'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Email '))?>
												<span  class="rgerror"><?php echo @$lnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-address-book" aria-hidden="true"></i>

												<?php echo $this->Form->input('manager_name', array('label' => false, 'required' => 'required' ,  'placeholder' => 'Manager name ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('manager_email', array('label' => false, 'type' => 'email','required' => 'required email' ,  'placeholder' => 'Manager Email', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Select Level -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select','emty'=>'Type', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('SchoolInformation.street', array('label' => false,'type' => 'text', 'required' => 'required' ,  'placeholder' => 'Street', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-location-arrow" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'required' => 'required' , 'type' => 'text',  'placeholder' => 'City / Town ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>


												<?php echo $this->Form->input('SchoolInformation.district', array('label' => false, 'required' => 'required' ,  'placeholder' => 'District', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.state', array('label' => false, 'required' => 'required' ,  'placeholder' => 'State / Province ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-codepen" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false, 'required' => 'required' ,  'placeholder' => ' Zip Code ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-phone" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false, 'required' => 'required' ,  'placeholder' => 'Phone', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-fax" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false, 'required' => 'required' ,  'placeholder' => 'Fax', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-link" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.url', array('label' => false, 'required' => 'required' ,  'placeholder' => 'URL', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											
											<div class="row">
												<div class="col-sm-7">

												<?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','placeholder' => 'Type the captcha text here','type'=>'text', 'value' => @$captcha_txt, 'id'=>'captcha_txt'));?>
												<span  class="rgerror"><?php echo @$captchaErr;?> </span>
												<div class="clear_fix"></div>
												</div>
												<div class="col-sm-5 led_wid">
													<label >
															<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
															<a href="javascript:void(0);"
																onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
															<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
														</label>
													</div>
										</div>
											
										
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); ?>Is previous year customer?
												
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									
									<div class="row">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="submit" class="btn btn-info def_btn" value="Submit">
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
						</div>

						<?php echo $this->Form->end(); ?>
					</div>
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>