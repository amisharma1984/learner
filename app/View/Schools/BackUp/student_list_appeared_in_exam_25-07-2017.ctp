<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>DataTables/resources/demo.css">
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $this->webroot;?>DataTables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" class="init">
	/*	$(document).ready(function() {
			$('#example').DataTable( {
				columnDefs: [ {
					targets: [ 0 ],
					orderData: [ 0, 1 ]
				}, {
					targets: [ 1 ],
					orderData: [ 1, 0 ]
				}, {
					targets: [ 4 ],
					orderData: [ 4, 0 ]
				} ]
			} );
		} );*/
	</script>
	
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable( {
            "columns": [
                null,
                null,
				null,
                null,
				null,
				null,
                null,
				null,
                { "orderable": false }, // last column orderable/up down arrow hide i.e. false
            ]
        } );
    } );
</script>

<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                 <div class="main_headding">
				<?php 
					$category = explode(' ',$studentList[0]['SchoolPurchaseExam']['Examination']['ExaminationCategory']['name']);
				?>
                <h2>YEAR <?php echo $category[1];?> STUDENT RESULT LIST</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
<table style="width:55%" border="1">
	<tbody>
		<tr>
		<th>&nbsp;</th>
		<th>Mean</th>
		<th>Standard Deviation</th>
		</tr>
		
		<tr>
		<td><?php echo 'Year '.$category[1];?> Group</td>
		<?php
		$yearGroupMean = 0;
		$groupmean=0;
		$standardDeviation1 =0;
		if($groupByStudent){
			$countStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student_by_category',$groupByStudent[0]['Student']['examination_category_id']));
		foreach($groupByStudent as $studentData){
		
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		$totalnumberofans = 0;
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id']));
			$totalnumberofans = $totalnumberofans + $quetsionsAnswersData['totalCorrectAnswer'];
		}
		 $yearGroupMean = $yearGroupMean +$totalnumberofans;
		 
		?>
		<?php 
			}
			$groupmean= round($yearGroupMean/$countStudent,2);
		 $d2 = ($groupmean - $yearGroupMean);
		  $daviation1= ($d2*$d2);
		  if($countStudent == 1){
			    $standardDeviation1 = round(($daviation1) / sqrt($countStudent), 2);
		  } else {
			    //$standardDeviation = round(($daviation) / sqrt($noOfStudent -1), 2);
			    $standardDeviation1 = round(($daviation1) / sqrt($countStudent), 2);
		  }
		}
		?>
		
		<td><?php echo $groupmean; ?></td>
		<td><?php echo $standardDeviation1; ?></td>
		</tr>
		<?php
		$yearGroupMean = 0;
		if($groupByStudent){
		foreach($groupByStudent as $studentData){ 
		$noOfStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_number_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		
		$listStudent = $this->requestAction(array('controller' => 'schools', 'action' => 'fetch_total_list_student',$studentData['Student']['examination_category_id'], $studentData['Student']['teacher_name']));
		$totalnumberofans = 0;
		foreach($listStudent as $listData){
			$student_ans_random_id = $listData['MyExamStudentAnswer']['unique_flag'];
			$listData['Student']['id'];
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $listData['Student']['id']));
			$totalnumberofans = $totalnumberofans + $quetsionsAnswersData['totalCorrectAnswer'];
		}
		
		$mean= 0; 
		
		if($totalnumberofans && $noOfStudent){
		$mean =	round($totalnumberofans/$noOfStudent,2);
		
		
		 $average = ($totalnumberofans / $noOfStudent);
		 $d1 = ($average - $totalnumberofans);
		  $daviation= ($d1*$d1);
		  if($noOfStudent == 1){
			    $standardDeviation = round(($daviation) / sqrt($noOfStudent), 2);
		  } else {
			    //$standardDeviation = round(($daviation) / sqrt($noOfStudent -1), 2);
			    $standardDeviation = round(($daviation) / sqrt($noOfStudent), 2);
		  }
		
		}
		?>
		<tr>
		<td><?php echo $studentData['Student']['teacher_name']; ?></td>
		<td><?php echo $mean; ?></td>
		<td><?php echo $standardDeviation; ?></td>
		</tr>
		<?php }}?>
			
		
	</tbody>
</table></br>
				  
				  
				  
                </div>
					<div class="prof_details prof_form">
						<div class="attempt_details">
							<div class="attempt_qus">
								<ul>
									<li><span>Total Student(s) : </span> <strong><?php echo $no_of_student_appear;?> </strong></li>
									<!--<li><span>Pass Student(s) : </span> <strong> <?php //echo $pass ?></strong></li>
									<li><span>Fail Student(s) : </span> <strong> <?php //echo $fail;?> </strong></li>-->
									<!--<li><span>Average Correct Answer : </span> <strong><?php //echo @$avereageCorrectAns;?></strong></li>-->
									
								</ul>
							</div>
						</div>
					</div>      
				<div class="clear_fix"></div>
				
				
              </div>
            </div>
			

			
			
	<div class="table-responsive">
	<table id="example" class="display student-list-apr" border="1" bordercolor="#fff" cellspacing="0" style="width:99%"  width="99%">
				<thead>
					<tr>
			<th style="padding:10px 12px;" id="arrow_hide">List</th>
			<th style="padding:10px 12px;">Student Name</th>
			<!--<th style="padding:10px 12px;">Student Class</th>-->
			<th style="padding:10px 12px;">Student Email/Student ID</th>
			<th style="padding:10px 12px;">Student's Score</th>
			<th style="padding:10px 12px;">Score as %</th>
			<th style="padding:10px 12px;">Rank in year group</th>
			<th style="padding:10px 12px;">Rank in class</th>
			<th style="padding:10px 12px;">No of questions attempted</th>
			<th style="padding:10px 12px;">View student's individual report</th>
			
		
					</tr>
				</thead>
				<tbody>
					
				<?php
	        	if(!empty($studentList)){ //pr($studentList);
					$i = 1;
					foreach($studentList as $val){
						 $student_ans_random_id = $val['MyExamStudentAnswer']['unique_flag'];
				?>
		<tr>
			
			<td><?php echo $i;?></td>
			<td><?php echo $val['Student']['first_name'].' '.$val['Student']['last_name'];?></td>
			<!--<td><?php //echo @$averageCorrectAnswer;?></td>-->
			<!--<td><?php //echo $val['Student']['teacher_name'];?></td>-->
			<td><?php echo $val['Student']['email'];?></td>
			 <?php 
			$quetsionsAnswersData = $this->requestAction(array('controller' => 'students', 'action' => 'fetch_student_total_correct_answer',$student_ans_random_id, $val['Student']['id']));
        
			?>
			
			<td><?php  echo $quetsionsAnswersData['totalCorrectAnswer'];?></td>
			<td>
			<?php
				 if ($quetsionsAnswersData['totalCorrectAnswer'] != 0) {
					$percentage = ($quetsionsAnswersData['totalCorrectAnswer'] * 100) / $val['MyExamStudentAnswer']['total_set_question'];
					echo round($percentage,2);
				}
			?>
			
			</td>
			<td><?php echo @$val['MyExamStudentAnswer']['year_group_rank'];?></td>
			<td><?php echo @$val['MyExamStudentAnswer']['class_rank'];;?></td>
			<td><?php echo $quetsionsAnswersData['totalAttempQuestions'];?></td>
			<td>
			<a target="_blank" title="View Result" href="<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'];?>">View Result</a>
			</td>
		</tr>
	<?php 
				$i++;
				}
			}
	?>
		
					
				</tbody>
			</table>
			</div>
          </div>
        </div>
      </div>
    </div>

	
<!-- Start Add And edit Exam Modal-->

<div class="modal fade" id="viewStudentCorrectAns" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><!-- heading/title is coming here--></h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>

<!--2nd modal -->
<div class="modal fade" id="viewEachQuestionAnswer" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--3rd modal -->
<div class="modal fade" id="viewEachQuestion" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> heading/title is coming here</h4>
            </div>
            <div class="modal-body"><!-- data will come here--> </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
	<script>
	function delete_student(student_id){
		var r = confirm('Are you sure to delete this record?');
		if(r == true){
			$('#strow_' + student_id).hide();
			$.ajax({
				  url : '<?php echo $this->webroot.'schools/delete_student'?>',
				  type : 'POST',
				  data : {'student_id' : student_id},
				  success : function(responce){
					  //alert(responce);
				  }
			});
		}
		
	}
	</script>
	<style>
	#arrow_hide {
		background-image: none !important;
		pointer-events: none;
		cursor: default;
	}
	#example_filter {
		margin-right:0.4%;
	}
	</style>