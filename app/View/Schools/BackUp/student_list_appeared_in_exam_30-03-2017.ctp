<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Student List Appear in Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                   <h3>Total No. of Student appear : <?php echo @$no_of_student_appear; ?></h3>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 
<table>
 
	<tr>
	 <?php  echo $this->Form->create('Student3'); ?>
		<td> Search By &nbsp;
		<select name="search_by">
		<option value="name">Name</option>
			<option value="email">Email</option>
		</select> 
		<input type="text" name="search_text">&nbsp;<input type="submit" value="Search">&nbsp;
		<input type="button" onclick="javascript:window.location.assign('<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$school_purchase_exam_id;?>')" value="Show All"></td>
		<td>&nbsp;</td> 
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		  <?php echo $this->Form->end(); ?>
	</tr>
	
</table>

<table>
  <tr>
  <th> No.</th>
    <th> <?php echo $this->Paginator->sort('first_name', 'Name'); ?></th>
    <th><?php echo $this->Paginator->sort('email', 'Student Email/ Student ID'); ?></th>
    <th>Category<?php //echo $this->Paginator->sort('school_name', 'Category'); ?></th>
	<!--<th><?php //echo $this->Paginator->sort('student_class_id', 'Class'); ?></th>-->
	
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($studentList)){  //pr($studentList);
  $i = 1;
	  foreach($studentList as $val){
		   // $studentId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Student']['id']));
		   $student_ans_random_id = $val['MyExamStudentAnswer']['student_ans_random_id']
		  ?>
		  <tr id="strow_<?php echo $val['Student']['id']?>">
		  <td><?php echo $i;?></td>
    <td><?php echo $val['Student']['first_name'].' '.$val['Student']['last_name'];?></td>
    <td><?php echo $val['Student']['email'];?></td>
    <td><?php echo $val['SchoolPurchaseExam']['Examination']['ExaminationCategory']['name'];?></td>
	 <!--<td><?php //echo @$val['Student']['StudentClass']['name'];?></td>-->
	 
	  <td>
	 <a target="_blank" title="View Result" href="<?php echo $this->webroot.'students/view_answer/'.$student_ans_random_id.'/'.$val['Student']['id'];?>">View Result</a>
	
	  </td>
  </tr>
<?php $i++;
	  }
  }
?>
</table>
				 
				 
                </div>
				
				<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
	function delete_student(student_id){
		var r = confirm('Are you sure to delete this record?');
		if(r == true){
			$('#strow_' + student_id).hide();
			$.ajax({
				  url : '<?php echo $this->webroot.'schools/delete_student'?>',
				  type : 'POST',
				  data : {'student_id' : student_id},
				  success : function(responce){
					  //alert(responce);
				  }
			});
		}
		
	}
	</script>
