<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Purchase Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 

  <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'checkout'), true))); ?>
<table>
  <tr>
    <th>S.No.</th>
	  <th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Price per subject<br> per student<br> per year ($ AUD)</th>
	<!--<th>Exam Code</th>-->
	<th>Action</th>
  </tr>

  <?php
  if(!empty($allExamAr)){
	  $i = 1;
	  foreach($allExamAr as $val){
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
    <td><?php echo $i?></td>
    <td><?php echo  ucwords(strtolower($val['Examination']['title']));?></td>
    <td><?php echo $val['ExaminationType']['name'];?></td>
    <td><?php echo $val['ExaminationCategory']['name'];?></td>
	 <td align="center"><?php echo $val['Examination']['price'];?></td>
	 <!-- <td><?php //echo @$val['SchoolPurchaseExam']['exam_code'];?></td>-->
	  <td width="170">
	  <?php  if(!empty($val['SchoolPurchaseExam']['payment_status']) && $val['SchoolPurchaseExam']['payment_status'] == 1){ ?>
			 <a href="javascript:void(0);" style="color:green; text-decoration:none; cursor:text;">Already purchased</a>
	  <?php } else {?>
	  <input type="checkbox"  value="<?php echo $val['Examination']['id'];?>" name="data[Examination][ids][]">
	  <?php }?>
	  <?php /*?>
	  <?php
	  if(!empty($val['SchoolPurchaseExam']['payment_status']) && $val['SchoolPurchaseExam']['payment_status'] == 1){ 
	  ?>
	  <a href="javascript:void(0);" style="color:red; text-decoration:none;">Purchased</a>
	  <?php } else {?>
	 <a href="<?php echo $this->webroot.'schools/purchase_exam_payment/'.$ExaminationId;?>">Make Payment</a>
	  <?php }?>
	  <?php */?>
	  
	  </td>
  </tr>
<?php
$i++;
	  }
  }
?>
		
		
 
</table>
</div>        <input type="hidden" value="checkout" name="page_name">
				<div class="clear_fix"></div>
				<div class="exam_chkout">
					<input type="submit" class="btn btn-info def_btn" value="Checkout">
					<?php echo $this->Form->end(); ?>
				</div>
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<style>
	.exam_chkout {
		float:right;
		margin-top:20px
	}
	</style>
