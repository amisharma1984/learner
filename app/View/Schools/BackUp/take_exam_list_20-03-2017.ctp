<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Take Exam LIst</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
				  <?php 
							if($_SERVER['HTTP_HOST'] == 'localhost'){
								$student_login =  'http://'.$_SERVER['HTTP_HOST'].'/naplan_exam/online_exams/login';
							} else if($_SERVER['HTTP_HOST'] == 'mydevfactory.com'){
								$student_login =  'http://'.$_SERVER['HTTP_HOST'].'/~pranay/pranay.com/arif/naplan_exam/online_exams/login';
							} else {
								$student_login =  'http://'.$_SERVER['HTTP_HOST'].'/online_exams/login';
							}
				  ?>
				  <p>Student Exam Login URL : <span style="color:#a0760a;"><?php echo $student_login;?></span></p>
                  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
    <th><?php echo $this->Paginator->sort('Examination.title', 'Exam Name'); ?></th>
    <!--<th>Type</th>-->
    <th><?php echo $this->Paginator->sort('Examination.examination_category_id', 'Category'); ?></th>
	<th><?php echo $this->Paginator->sort('SchoolTakeExam.exam_code', 'Exam Code'); ?></th>
	<th><?php echo $this->Paginator->sort('SchoolTakeExam.exam_date_time', 'Exam Date'); ?></th>
	
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  foreach($allExamAr as $val){
		    $SchoolTakeExamId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolTakeExam']['id']));
		    $school_take_exam_id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['SchoolTakeExam']['id']));
			$no_of_student_appear = count($val['MyExamStudentAnswer']);
		  ?>
		  <tr>
    <td><?php echo $val['Examination']['title'];?></td>
    <!--<td><?php //echo $val['Examination']['ExaminationType']['name'];?></td>-->
    <td><?php echo $val['Examination']['ExaminationCategory']['name'];?></td>
	  <td><?php echo @$val['SchoolTakeExam']['exam_code'];?></td>
	 
	   <td><?php echo $val['SchoolTakeExam']['exam_date_time'];?></td>
	  <td>
	 <a href="<?php echo $this->webroot.'schools/student_list_appeared_in_exam/'.$SchoolTakeExamId;?>">Appeared Students(<?php echo $no_of_student_appear;?>)</a>&nbsp;|&nbsp;
	  <a title="Delete" href="<?php echo $this->webroot.'schools/delete_take_exam/'.$school_take_exam_id;?>" onclick="javascript: if(confirm('Are you sure to delete this record?')) { return true} else { return false}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
	  </td>
  </tr>
<?php
	  }
  }
?>
</table>
				 
				 
                </div>
				
<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
			
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
