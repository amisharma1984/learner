<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3> <?php echo ucwords($schoolData['School']['manager_name']); ?></h3>
              
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('School'); 
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                     
					 <div class="form-group">
                        <label >School Name :</label>
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
					  
                    <div class="form-group">
                        <label >Manager Name :</label>
						<?php echo $this->Form->input('manager_name', array('label' => false, 'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
					  
                      <div class="form-group">
                        <label > Level  :</label>
                       <?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Select Level -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
					
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Street :</label>
						 <?php echo $this->Form->input('SchoolInformation.street', array('label' => false, 'div' => false,  'class' => 'form-control inp_text'))?>
					
                     
                        <div class="clear_fix"></div>
                      </div>
                    </div>
					
					
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Email:</label>
						<?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control inp_text'))?>

                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Manager Email  :</label>
							<?php echo $this->Form->input('manager_email', array('label' => false,  'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Type : :</label>
						<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
					
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >City / Town :</label>
							<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
						  <div class="clear_fix"></div>
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn adj_btn" value="Submit">
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>