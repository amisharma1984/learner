<style>
.regi_main{
	margin-left:1.5%;
}
.rigis_right {
	width:48.9% !important;
}

.rgerror {
    color: red;
    font-size: 13px;
    margin-left: 44.5%;
}

</style>  

  <div class="container">
      <div class="inner_field regi_main">
        <div class="row">
		
			<div class="contact_head">
					<h2>
						<span>School Registration</span>
						<strong>Form</strong>
					</h2>
				
			</div>
          <?php echo $this->Session->flash();?>
            <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('School'); 
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >School Name :</label>
						<?php echo $this->Form->input('name', array('label' => false, 'required' => 'required' ,  'placeholder' => '', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  <div class="form-group">
                        <label >Manager name :</label>
						<?php echo $this->Form->input('manager_name', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  
					    <div class="form-group">
                        <label >Level :</label>
                       <?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Select Level -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
					  <span  class="rgerror"><?php //echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
                      
					  
					    <div class="form-group">
                        <label >Street  :</label>
						<?php echo $this->Form->input('SchoolInformation.street', array('label' => false, 'required' => 'required' ,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					     <div class="form-group">
                        <label >District  :</label>
						<?php echo $this->Form->input('SchoolInformation.district', array('label' => false, 'required' => 'required' ,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					   <div class="form-group">
                        <label >Zip Code  :</label>
						<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false, 'required' => 'required' ,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  
					   <div class="form-group">
                        <label >Fax  :</label>
						<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false, 'required' => 'required' ,  'placeholder' => '  ', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  
					  
					   <div class="form-group">
					
                        <label >Type the captcha text here :</label>
                     	    <?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','type'=>'text', 'value' => @$captcha_txt, 'id'=>'captcha_txt'));?>
						<span  class="rgerror"><?php echo @$captchaErr;?> </span>
						<div class="clear_fix"></div>
                      </div>
					   
					  
                    </div>
                    <div class="col-md-6 form_info rigis_right">
                      <div class="form-group">
                        <label >Email :</label>
						<?php echo $this->Form->input('email', array('type' => 'email', 'required' => 'required email' , 'label' => false,'class' => 'form-control inp_text', 'placeholder' => ' '))?>
						<span  class="rgerror"><?php echo @$lnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					   
                      <div class="form-group">
                        <label >Manager Email  :</label>
							<?php echo $this->Form->input('manager_email', array('label' => false, 'type' => 'email','required' => 'required email' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					   
					  
                      <div class="form-group">
                        <label > Type :</label>
						<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
					  <span  class="rgerror"><?php //echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					  
					   <div class="form-group">
                        <label > City / Town  :</label>
							<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					     <div class="form-group">
                        <label > State / Province  :</label>
							<?php echo $this->Form->input('SchoolInformation.state', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  <div class="form-group">
                        <label > Phone  :</label>
							<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  <div class="form-group">
                        <label > URL  :</label>
							<?php echo $this->Form->input('SchoolInformation.url', array('label' => false, 'required' => 'required' ,  'placeholder' => ' ', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
                   
					   
					
					   
					   <div class="form-group">
                        <label >
						<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
						<a href="javascript:void(0);"
						onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
						<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
						</label>
						 <?php echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); ?>Is previous year customer?
							
					 <div class="clear_fix"></div>
                      </div>
					  
					  
					    <div class="form-group">
                        <label >&nbsp;</label>
								 <div class="col-md-4">
                                     <input type="submit" class="btn btn-info def_btn" value="Submit">
                                 </div>
					  <div class="clear_fix"></div>
                      </div>
					   
					  
					  
                    </div>
                  </div>
                   
                   <?php echo $this->Form->end(); ?>
                </div>
				
       
		  
		  
        </div>
      </div>
    </div>