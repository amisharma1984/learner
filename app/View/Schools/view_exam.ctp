<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>

    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>NEXT &#187;</a>");
			  } else {
				    $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function save_next(){
			
			
				
			  var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
					$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
				 /*var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
										$('#attempted_qs').text(numberOfCheckedRadio);
										if(percentageBar == 100){
											 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
										} else {
											  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
										}
				                       
										
									   
										$('#qs_attempted_prcessing').css({"width": ""});
										$('#qs_attempted_prcessing').css({"width": percentageBar+"%"}); */
				 
			});	
		});
		
		
		function answer_details(ans_id){
			$('#details_showhide_' + ans_id).toggle();
		}
		
    </script>



<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
       <div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){
			//$qstab_color = 'unattempt_qa';
			$qstab_color = '';
			
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			
			?>
    	   </ul>
    </div>

	
		<!--<div class="attampt_flag">
          <div class="res_point"><span class="correct_answer" ></span>Correct Answer</div>
         <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer</div>
         <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted</div>
        </div>-->
</nav>
</div>

<?php //pr($QuestionAns);?>

<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
	  
	  <div class="bread_crumb"><span><a href="<?php echo $this->webroot.'schools/my_purchased_exam'?>">List of Exams Purchased</a> /</span> View Exam</div>
	  
      <div class="content_main view_qsd">
	   <!--<h2><?php //echo @$QuestionAns[0]['Examination']['title'];?></h2>-->
				<h2>
				<?php 
					 if($exam_section_id == 1){
						  echo $ExamDetails['ExaminationCategory']['name'].' NAPLAN Online Trial Literacy Reading Booklet and Questions';
					 } else  if($exam_section_id == 2){
						  echo $ExamDetails['ExaminationCategory']['name'].' NAPLAN Online Trial Literacy Language Conventions';
					 } else {
						 echo strip_tags(@$QuestionAns[0]['Examination']['description']).': '.@$section;
					 }
				   ?>
				<?php //echo strip_tags(@$QuestionAns[0]['Examination']['description']).': '.@$section;?>
				</h2>
	  
	
	 
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="qusdetails" class="tabs">
  <?php
	  if(!empty($QuestionAns)){ 
		  $i = 1;
		  $k = 1;
		  foreach($QuestionAns as $qval){  //pr($qval);
		   $qs_color = 'notattempt_ans_qs';
	?>
	
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
	   
	    <?php if(!empty($qval['Question']['reading_book_content'])){?>
	    <div class="qus book_reading">
		<?php echo $qval['Question']['reading_book_content'];?>
		</div>
	   <?php }?>
        	       <div class="qus <?php //echo $qs_color;?>"><span class="qs_num"><?php echo 'Q '.$i;?>. </span> 
				   <?php 
						echo $qval['Question']['title'];
				  
				   ?>
				  
				   </div>  
				  
<ul class="ans_field">
 <?php 
  
  $j = 'a';
  foreach($qval['Answer'] as $ansval) { //pr($ansval);
  
   if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio"  name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio"  disabled="disabled"  name="ans[]">';
		
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_"></textarea>';
	}
	
	if(strlen(strip_tags($ansval['answer_text'])) >= 20  && strpos($ansval['answer_text'], 'src="') == false) {
		$answerclass = 'big_answer';
	} else if(strpos($ansval['answer_text'], 'src="') !== false){
		$answerclass = 'big_answer';
	}  else 	if(strlen(strip_tags($ansval['answer_text'])) <= 20  && strpos($ansval['answer_text'], 'src="') == false && count($qval['Answer']) >=5) {
		$answerclass = 'ans5_options';
	}
	else {
		$answerclass = 'ans_options';
	}
	
  
	
	
  ?>
  
  <?php if($ansval['answer_type'] == 4 && !empty($ansval['answer_unit']) && trim($ansval['answer_unit']) == '$'){?>
		   <div class="ans_unit_pre"><?php echo $ansval['answer_unit'];?></div>
 <?php }?>
  <li class="<?php echo ($ansval['answer_type'] == 4) ? 'ans_options_txt_area' : $answerclass;?> ">
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
			echo $answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		 }
		 ?>
 </li>
<?php if($ansval['answer_type'] == 4 && !empty($ansval['answer_unit']) && trim($ansval['answer_unit']) != '$'){?>
		   <div class="ans_unit_post"><?php echo $ansval['answer_unit'];?></div>
 <?php }?>
 
		 <?php
		 
		 $j++;
  } 
	?> 
</ul>

   </div>
	  <?php 
				$i++;
				}
			} 
	  ?>
	   
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>