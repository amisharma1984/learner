<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');
         $selectedExamTitle = '';
         	if (!empty($this->Session->read('examTitle'))){
         		$selectedExamTitle = $this->Session->read('examTitle'); 
         	} ?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Report Problems</h2>
                  
                <h3>Report an Issue</h3>
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				 echo $this->Form->create('SchoolReportedProblem', array('name' => 'StudentUpdate', 'enctype'=>'multipart/form-data', 'onsubmit' => 'return validation()')); 
				
				 $examTitle = array();
				 foreach($title as  $val) {
				 	$examTitle[$val] = $val;
				 }
				?>
				 
				
				 <div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Exam Title :</label>
									<?php echo $this->Form->input('issue_type', array('label' => false,'empty' => '--Select Exam Title--', 'selected' => $selectedExamTitle, 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $examTitle))?>
										<span  class="prof_err" id="issueTypeErr"><?php //echo @$fnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
							
						</div>
				 
						<div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Please Describe the issue :</label>
									<?php echo $this->Form->input('description', array('type' => 'textarea','label' => false, 'class' => 'form-control inp_text'))?>			
									<span  class="prof_err" id="issueDetailsErr"><?php //echo @$fnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Add ScreenShot(If Any) :</label>
									<?php echo $this->Form->input('image', array('type' => 'file','label' => false, 'class' => 'form-control inp_text'))?>			
									<span  class="prof_err" id="issueDetailsErr"><?php //echo @$fnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
						</div>
						


						
						
							<div class="row">
								<div class="col-md-6"></div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="submit" class="btn btn-info def_btn" value="Submit">
											<div class="clear_fix"></div>
										</div>
									</div>
							</div>		  
                   
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
function validation(){ 
	var result=true;
	if($.trim($('#SchoolReportedProblemIssueType').val()) == ""){
		 $('#issueTypeErr').html('Please select issue type');
		 result=false;
	} else {
		 $('#issueTypeErr').html('');
	}
	if($.trim($('#SchoolReportedProblemDescription').val()) == ""){
		 $('#issueDetailsErr').html('Please enter issue details');
		 result=false;
	} else {
		 $('#issueDetailsErr').html('');
	}
	
	
	
	
	return result;
	
 }
</script>
