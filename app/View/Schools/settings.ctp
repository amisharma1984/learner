<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
  <style>
.rgerror {
    color: red;
    font-size: 13px;
    margin-left: 44.5%;
}

</style>  
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2><!--School Profile--></h2>
				 <h2 style="text-align:center; color:#119548;">Student Email Setting</h2>
                  <h3> <?php //echo ucwords($this->data['School']['manager_name']); ?></h3>
              
				  <?php echo $this->Session->flash();?>
                </div>
				<div class="clear_fix" style="margin-top:40px;"></div>
				
                <div class="prof_details prof_form">
                 <?php 
				 echo $this->Session->flash();
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'myprofile'), true))); 
				echo $this->Form->create('School', array('onsubmit' => 'return validation()')); 
				 ?>
				 
                  <div class="row">
                    <div class="col-md-8 form_info">
					 <div class="form-group">
                        <label >Student Comman Email Part :</label>
						<?php echo $this->Form->input('student_common_email_part', array('label' => false,'maxlength' => 60,'placeholder' => 'Ex.: @abc.nsw.edu.au', 'class' => 'form-control inp_text'))?>
                        <span class="rgerror" id="studentEmailErr"></span>
						<span style="font-size:14px; color:#8181cf;">(Example : @abcd.com)</span>
						
						
                        <div class="clear_fix"></div>
                      </div>
                    </div>
				    <div class="col-md-6" style="float:right;">
					<input type="submit" class="btn btn-info def_btn" value="Submit">
					</div>
                  </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
<script>
function validation(){ 
	var result=true;
	
	if($.trim($('#SchoolStudentCommonEmailPart').val()) == ""){
		 $('#studentEmailErr').html('This field is required');
		 result=false;
	} else if($.trim($('#SchoolStudentCommonEmailPart').val()) != "" && $.trim($('#SchoolStudentCommonEmailPart').val()).length <=4){
		 $('#studentEmailErr').html('Value is too short');
		 result=false;
	} else {
		 $('#studentEmailErr').html('');
	}
	
	
	
	return result;
	
 }
</script>