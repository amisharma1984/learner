	<script>
function expressPaypal(){
		$('#expressPaypal').show();
		$('.card_but').addClass('active');
			
		$('#normalPaypal').hide().removeClass('active');
		$('.pay_but').removeClass('active');
}
function normalPaypal(){
		$('#normalPaypal').show();
		$('.pay_but').addClass('active');
	
		$('#expressPaypal').hide();
		$('.card_but').removeClass('active');
}
</script>

 <script type="text/javascript">
        $(function(){
            
            /* PayPal form Validation */            
            $("#payPalPaymentForm").validate({
                submitHandler: function(form) {
                
                    var amount = $("#payPalPaymentForm").find('input[name=amount]').val();
                    var custom = $("#payPalPaymentForm").find('input[name=custom]').val();
                    
                    var jqxhrPayPalForm = $.ajax({
                        type: "POST",
                        url: "<?php echo Router::url('/', true); ?>schools/checkFingerprint",
                        data: {
                            amount: amount,
                            custom: custom
                        },
                        success: function (response) {
                            console.log(response);
                            if(response != ''){
                                $('.help-block').fadeIn();
                            }else{
                                $('.help-block').fadeOut();
                                form.submit();
                            }

                        }
                    });
                }
            });
            
            
        });   
        </script>

<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
		 
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
				 <p>Welcome to <strong><?php echo Configure::read('App.appName'); ?>
                <h2>Payment Information</h2>
				  <?php echo $this->Session->flash(); ?>
                  <h3> <?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span><a class="btn_paypal btn def_btn active card_but"   role="button" onclick="expressPaypal();">Credit Card</a> 
				  <a class="btn_paypal btn def_btn pay_but"   role="button" onclick="normalPaypal();">PayPal</a></span>
                </div>
				
				
                <div class="prof_details prof_form" id="expressPaypal">
                <?php echo $this->Form->create('Payment', array('id' => 'payment_info_validate'));?>
                  <div class="row">
				  
				  
                    <div class="col-md-8 form_info">
                      <div class="form-group">
                        <label > Payable amount ($ USD) :</label>
						<?php  echo $this->Form->input('amount', array('type' => 'text', 'label' => false, 'value' => $paymentAmount,'class' => 'form-control inp_text', 'div' => false, 'readonly' => 'readonly'));?>
					<div class="clear_fix"></div>
                      </div>
					  
                      <div class="form-group">
                        <label >Payment Type * :</label>
						<?php echo $this->Form->input('payment_type', array('type' => 'select', 'label' => false, 'div' => false, 'options' => array('Visa' => 'Visa', 'MasterCard' => 'Master Card'), 'class' => 'selectpicker show-tick form-control inp_text'));?>
						<div class="clear_fix"></div>
                      </div>
					  
                      <div class="form-group">
                        <label >Credit Card Number *</label>
                     <?php echo $this->Form->input('credit_card_no', array('type' => 'text', 'label' => false,  'maxlength' => 16, 'div' => false, 'autocomplete' => 'off', 'placeholder' => 'Enter Credit Card Number', 'class' => 'form-control inp_text required number'));?>
						  <span  class="error"><?php echo @$card_err;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					  
                      <div class="form-group">
                        <label class="exp_lbl">Expiry Date * :</label>
						
						    
					      <div class="cal_month">
					
					<select class="selectpicker__ show-tick form-control inp_text" name="data[Payment][exp_month]">
					<option value="">Month</option>
					<option  value="1" <?php echo (!empty($exp_month) && $exp_month == 1) ? 'selected' : ' ';?> >January</option>
					<option  value="2" <?php echo (!empty($exp_month) && $exp_month == 2) ? 'selected' : ' ';?>>February</option>
					<option  value="3" <?php echo (!empty($exp_month) && $exp_month == 3) ? 'selected' : ' ';?>>March</option>
					<option  value="4" <?php echo (!empty($exp_month) && $exp_month == 4) ? 'selected' : ' ';?>>April</option>
					<option  value="5" <?php echo (!empty($exp_month) && $exp_month == 5) ? 'selected' : ' ';?>>May</option>
					<option  value="6" <?php echo (!empty($exp_month) && $exp_month == 6) ? 'selected' : ' ';?>>June</option>
					<option  value="7" <?php echo (!empty($exp_month) && $exp_month == 7) ? 'selected' : ' ';?>>July</option>
					<option  value="8" <?php echo (!empty($exp_month) && $exp_month == 8) ? 'selected' : ' ';?>>August</option>
					<option  value="9" <?php echo (!empty($exp_month) && $exp_month == 9) ? 'selected' : ' ';?>>September</option>
					<option  value="10" <?php echo (!empty($exp_month) && $exp_month == 10) ? 'selected' : ' ';?>>October</option>
					<option  value="11" <?php echo (!empty($exp_month) && $exp_month == 11) ? 'selected' : ' ';?>>November</option>
					<option  value="12" <?php echo (!empty($exp_month) && $exp_month == 12) ? 'selected' : ' ';?>>December</option>
					</select>
					</div>
					  <div class="cal_year">
					<select class="selectpicker show-tick form-control inp_text" name="data[Payment][exp_year]">
						<!--<select class="form-control yrmnth" name="data[Payment][exp_year]">-->
					<option value="">Year</option>
					<?php
					$currYr = date('Y')+30;
					$initialYr = date('Y');
					for($i = $currYr; $i>= $initialYr; $i--){
					?>
					<option <?php if(!empty($year) && $year == $i){ echo 'selected';}?> value="<?php echo $i;?>"><?php echo $i;?></option>
					<?php }?>
					 
					</select>
					</div>
					
                        <div class="clear_fix"></div>
						 <span  class="error"><?php echo @$exp_month_err;?> </span>
						
                      </div>
					  
					    <div class="form-group">
                        <label > Security Code * :</label>
						<?php echo $this->Form->input('security_code', array('type' => 'password', 'label' => false,  'minlength' => 3, 'maxlength' => 3, 'div' => false, 'placeholder' => 'Enter  Security Code', 'class' => 'form-control inp_text required'));?>
						<span  class="error"><?php echo @$security_err;?> </span>
					<div class="clear_fix"></div>
                      </div>
					  
					    <div class="form-group">
                        <label > Name on Card* :</label>
						<?php echo $this->Form->input('name_on_card', array('type' => 'text', 'label' => false, 'autocomplete' => 'off', 'div' => false, 'placeholder' => 'Enter Name on Card', 'class' => 'form-control inp_text required'));?>
						 <span  class="error"><?php echo @$name_err;?> </span>
					<div class="clear_fix"></div>
                      </div>
					  
					  
					  
					  
					   <div class="form-group">
                        <label > &nbsp;</label> <input type="submit"  name="creditsubmit" class="btn def_btn" value="Submit">
                        <div class="clear_fix"></div>
                      </div>
					  
					  
                    </div>
					
					
                  <!--  <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Last Name :</label>
                        <input type="text" class="form-control inp_text " placeholder="Fernando">
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >school  :</label>
                        <input type="text" class="form-control inp_text " placeholder="St. John’s Diocesan School">
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Exam Category :</label>
                        <select class="selectpicker show-tick form-control inp_text">
                          <option selected>Outside Experience</option>
                          <option>Outside Experience 2</option>
                        </select>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >History :</label>
                        <textarea class="form-control inp_text" placeholder="Type about you..."></textarea>
                        <div class="clear_fix"></div>
                      </div>
                    </div>-->
					
					
					
					
                  </div>
                   <!-- <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn adj_btn" value="Submit Button">
                      </div>
                    </div>-->
                  </form>
                </div>
				
				
  <div class="" id="normalPaypal" style="display:none;">
  
    <?php
        if(!empty($paypalFormSettings)){
            $formAction = $paypalFormSettings['payment_url'];
            unset($paypalFormSettings['payment_url']);
            
            echo $this->Form->create(null, array('url' => $formAction, 'id' => 'payPalPaymentForm'));
            
            foreach ($paypalFormSettings as $key => $value){
                echo '<input type="hidden" name="'.$key.'" value="'.$value.'" >';
            }
            ?>
			<input type="image" width="250" src="http://<?php  echo $_SERVER['HTTP_HOST'].''.$this->webroot;?>img/paypalexpress.png" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
			<?php
            echo $this->Form->end();
        }
        ?>
  </div>
				
				
				
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>