<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');
         $selectedExamTitle = '';
         	if (!empty($this->Session->read('examTitle'))){
         		$selectedExamTitle = $this->Session->read('examTitle'); 
         	} ?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Reported Problems</h2>
                  
                
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                <table id="example" class="display student-list-apr "  bordercolor="#fff" cellspacing="0"  width="100%">
				<thead>
					<tr>
			<th>S.No.</th>
			<th>Exam Title</th>
			<th>Issue Details</th>
			<th>Image</th>
			<th>Posted On</th>
			<!-- <th style="width:75px;">Action</th> -->
			</tr>
			</thead>
				<tbody>
					
					 <?php
						if(!empty($reportedProblemList)){ //pr($testUserListAr);
						$i = 1;
							foreach($reportedProblemList as $val){
					?>
					<tr>
						<td><?php echo $i;?></td>
						<td><?php echo $val['SchoolReportedProblem']['issue_type'];?></td>
						<td><?php echo $val['SchoolReportedProblem']['description'];?></td>
						<?php $imgSrc = WWW_ROOT . 'img\problems\students\\'.$val['SchoolReportedProblem']['image'];  ?>
						<td><img src="<?php echo $imgSrc;  ?>"><?php echo $val['SchoolReportedProblem']['image'] ;?></td>
						<td><?php echo $val['SchoolReportedProblem']['created'];?></td>
						<!-- <td class="center">
                                
                                <a href="<?php //echo $this->webroot.'students/view_student_reported_problem/'.$val['SchoolReportedProblem']['issue_type'];?>" class="btn-info btn-sm" title="View" data-subject="2" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;">
                                <i class="fa fa-eye-slash" style="font-color:blue;"></i>
                                </a>
                                <a href="<?php //echo $this->webroot.'students/delete_reported_problem/'.$val['SchoolReportedProblem']['id'];?>" class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" onclick="return confirm('Are you sure to delete this record?')">
                                    <span class="glyphicon glyphicon-trash icon-white"></span>
                                </a>
                                
                            </td> -->
						
				   </tr>
			<?php
			   $i++;
				}
			}
			?>
		
					
				</tbody>
			</table>
                   
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
function validation(){ 
	var result=true;
	if($.trim($('#SchoolReportedProblemIssueType').val()) == ""){
		 $('#issueTypeErr').html('Please select issue type');
		 result=false;
	} else {
		 $('#issueTypeErr').html('');
	}
	if($.trim($('#SchoolReportedProblemDescription').val()) == ""){
		 $('#issueDetailsErr').html('Please enter issue details');
		 result=false;
	} else {
		 $('#issueDetailsErr').html('');
	}
	
	
	
	
	return result;
	
 }
</script>
