<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>

   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('school_dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Create Exam</h2>
                  <h3> <?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
				   
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('SchoolTakeExam'); 
				echo $this->Form->input('school_purchase_exam_id', array('type' => 'hidden', 'required' => 'required', 'value' => $school_purchase_exam_id));
				echo $this->Form->input('school_id', array('label' => false,'type' => 'hidden', 'value' => $myPurchasedExam['SchoolPurchaseExam']['school_id']));
				echo $this->Form->input('examination_id', array('label' => false, 'type' => 'hidden', 'value' => $myPurchasedExam['SchoolPurchaseExam']['examination_id']));
                       
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Exam Name :</label>
						<?php echo $this->Form->input('exam_name', array('label' => false, 'readonly' => 'readonly', 'value' => @$myPurchasedExam['Examination']['title'], 'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
                    </div>
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label > Exam Category :</label>
						<?php echo $this->Form->input('category_name', array('label' => false,'readonly' => 'readonly', 'value' => @$myPurchasedExam['Examination']['ExaminationCategory']['name'], 'class' => 'form-control inp_text'))?>

                        <div class="clear_fix"></div>
                      </div>
                     
                    </div>
					<div class="col-md-12">
					 <div class="form-group form-group2">
						<div class="select1">
                        <label style="font-weight:normal;">Set Exam Date :</label>
						 <?php echo $this->Form->input('exam_date_time', array('label' => false, 'div' => true,  'class' => ''))?>
						 </div>
						 
						  <?php // echo $this->Form->input('exam_date_time', array('label' => false, 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text'))?>
					<div class="clear_fix"></div>
                      </div>
					</div>
					
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn" value="Submit">
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  
  <script>
$( "#SchoolTakeExamExamDateTimeHour , #SchoolTakeExamExamDateTimeMin , #SchoolTakeExamExamDateTimeMeridian" ).wrapAll( "<div class="time_zoon" ></div>" );
</script>
 