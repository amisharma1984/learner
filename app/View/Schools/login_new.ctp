<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
				       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				  echo $this->Form->create('SchoolLogin'); 
				 echo $this->Form->input('SchoolLogin.login_registration_flag', array('type' => 'hidden', 'value' => 'school_login'));
				 ?>
						<div class="form_info">
							<h2>Sign in as Head Teacher</h2>
							<div class="form_cont">
							<div class="form-group">
								<i class="fa fa-user" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.login', array('label' => false,  'placeholder' => 'Email', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('SchoolLogin.password', array('label' => false,  'placeholder' => 'Password ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginPassErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="checkbox form-group">
								<label><!--<input type="checkbox"> Remember me--></label>
								<a href="<?php echo $this->Html->url(array('controller'=>'schools','action' => 'forgot_password')) ?>" class="formbtn forget"> Forgot Password?</a><br>
							</div>
							<div class=" form-group">
								<button id="submitbtn" type="submit" class="btn btn-info def_btn btn-sm">Submit</button>
								<button id="approvalbtn" style="display:none;" type="button" class="btn btn-info def_btn btn-sm" data-toggle="modal" data-target="#schoolActivationPop">Submit</button>

								
							</div>
						
							
							<div class="form-group">
								Don’t have an account? <a href="<?php echo $this->Html->url(array('controller'=>'schools','action' => 'registration')) ?>" > Sign Up!</a>
								
							</div>
							</div>
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
				

			</div>
			
		</div>
		
	</div>
</div>
<!--1st modal -->
<div class="modal fade" id="schoolActivationPop" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Account is not activated yet:</h4>
            </div> 
            <div class="modal-body">
                 <p>
					Account is not activated until verification process is completed. For faster activation or further enquiries contact: EMAIL: <a href="mailto:INFO@LEARNERLEADER.COM?Subject=Account Activation">INFO@LEARNERLEADER.COM</a>

				 </p>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
	$("#SchoolLoginLogin").keyup(function(){
        var email = this.value;
		
		 $.ajax({
			 url: "<?php echo $this->webroot.'schools/registration_approve_check';?>",
			 type : 'post',
			 data : {email : email},
			 success: function(result){
				 
			   if(result == 'OK'){
				   //alert(result);
					$('#submitbtn').hide();
					$('#approvalbtn').show();
				} else { 
					$('#submitbtn').show();
					$('#approvalbtn').hide();
				}
			}
		});
		
		
    });
    
});
</script>