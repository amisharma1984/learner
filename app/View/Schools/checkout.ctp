<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('school_dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
				<h2 style="text-align:center; color:#119548;"><?php echo @$schoolData['School']['name'];?></h2>
                <h2>Purchase Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
     <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'Schools', 'action' => 'checkout'), true))); ?>            
<table class="student-list-apr">
  <tr>
   <th>List</th>
    <th>Exam Name</th>
    <th>Category</th>
	<th>Price per subject<br> per student<br>($ AUD)</th>
	
	<th>No. of student</th>
	<th>Total($ AUD)</th>
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  $i=1;
	  foreach($allExamAr as $val){
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
		  <td><?php echo $i?></td>
    <td><?php echo $val['Examination']['title'];?></td>
    <td><?php echo $val['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['Examination']['price'];?></td>
	
	<input id="unit_price_<?php echo $val['Examination']['id'];?>" type="hidden" value="<?php echo $val['Examination']['price'];?>">
 <input type="hidden" value="<?php echo $val['Examination']['id'];?>" name="ids[]">

	 
	 <td align="center">
	  <input id="no_of_stdnt_<?php echo $val['Examination']['id'];?>" autocomplete="off" onkeyup="price_count('<?php echo $val['Examination']['id'];?>')" type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"  required size="4" name="no_of_student[]">
	  </td>
	  <td>
	  <input id="total_price_<?php echo $val['Examination']['id'];?>" class="total_price"  type="text" size="8" name="total_price[]" readonly>
	  </td>
  </tr>
<?php
$i++;
	  }
  }
?>
  
</table>
               <input type="hidden" value="purchase_exam_payment" name="page_name">
				<div class="exam_pay_div">
				<div class="gross_total">
					<label style="line-height:30px">Sub Total ($ AUD) :</label> <input id="gross_total" name="sub_total"  readonly type="text" size="10"></br>
					<label style="margin-left:1px;line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GST (10%) :</label> <input id="gst_10" name="gross_total"  readonly type="text" size="10"></br>
					<label style="line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total ($ AUD) :</label> <input id="total_with_gst" name="gross_total"  readonly type="text" size="10">
				</div>
				
				<!--<div class="exam_chkout"  id="pay_button_default">
					<input type="button" class="btn btn-info def_btn" value="Make Payment">
				</div>
				
				
				<div class="exam_chkout" style="display:none;" id="pay_button_show_hide">
					<input type="submit" class="btn btn-info def_btn"  value="Make Payment">
				</div>-->
				<div class="exam_chkout">
					<input type="submit" class="btn btn-info def_btn"  value="Make Payment">
				</div>
				
				
			</div>
			
			<?php echo $this->Form->end(); ?>

				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
	function price_count(id){
		 var sum = 0;
		var unit_val = Number($('#unit_price_' + id).val());
		var no_of_student = Number($('#no_of_stdnt_' + id).val());
		var tot = unit_val *  no_of_student;
				
				if(tot != ''){
					 $('#total_price_' + id).val(tot);
				} else {
					 $('#total_price_' + id).val('');
				}
				
				$('.total_price').each(function() {
					sum += Number($(this).val());
				});
				
				$('#gross_total').val(sum);
				$('#gst_10').val((sum*10)/100);
			     $('#total_with_gst').val((sum + ((sum*10)/100)));
				 
				if($('#gross_total').val() == ''  || $('#gross_total').val() == 0){
					$('#pay_button_show_hide').hide();
					$('#pay_button_default').show();
				} else {
					$('#pay_button_show_hide').show();
					$('#pay_button_default').hide();
				}
				 
				
		        
		
	}
	</script>
