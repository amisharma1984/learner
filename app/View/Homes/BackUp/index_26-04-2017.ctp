<div class="schools_details_boxes">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="schools_acess">
                    <div class="schools_acess_inner">
                       <div class="row">
                       <div class="col-sm-5 col-md-12 col-lg-5">
                        <div class="schools_acess_inner_img">
                            <img src="<?php echo $this->webroot;?>images/blog1.png" alt=""/>
                        </div>
                        </div>
                        <div class="col-sm-7 col-md-12 col-lg-7">
                        <div class="schools_acess_inner_text">
                            <h3><img src="<?php echo $this->webroot;?>images/sc_i.png" alt=""/> Schools Access</h3>
                            <h4>NAPLAN ONLINE TESTING</h4>
                            <div class="choose_school">
                                <a href="<?php echo $this->webroot.'schools-access/online-testing/primary';?>">Primary</a>
                                <a href="<?php echo $this->webroot.'schools-access/online-testing/high-school/';?>" class="hight_s">High School</a>
                            </div>
                            <p>Use this portal to access the online 
NAPLAN trail paper, view sample questions and learn about the benefits your school.</p>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="schools_acess schools_acess_blue">
                    <div class="schools_acess_inner">
                       <div class="row">
                       <div class="col-sm-5 col-md-12 col-lg-5">
                        <div class="schools_acess_inner_img">
                            <img src="<?php echo $this->webroot;?>images/blog2.png" alt=""/>
                        </div>
                        </div>
                        <div class="col-sm-7 col-md-12 col-lg-7">
                        <div class="schools_acess_inner_text">
                            <h3><img src="<?php echo $this->webroot;?>images/sc_i2.png" alt=""/> Students / Parents Access</h3>
                            <h4>ONLINE PRACTICE</h4>
                            <div class="choose_school">
                                <a href="<?php echo $this->webroot.'students-access/online-practice/primary';?>">Primary</a>
                                <a href="<?php echo $this->webroot.'students-access/online-practice/high-school';?>" class="hight_s">High School</a>
                            </div>
                            <p>Use this portal to access practice questions, view sample questions and learn about how you can learn from home.</p>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="gallery_images">
   <div class="container">
       <div class="row">
           <div class="col-sm-3">
              <div class="gallery_images_box">
           <img src="<?php echo $this->webroot;?>images/pic1.jpg" alt=""/>
           </div>
           </div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic2.jpg" alt=""/></div></div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic3.jpg" alt=""/></div></div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic4.jpg" alt=""/></div></div>
       </div>
   </div>
    
</div>