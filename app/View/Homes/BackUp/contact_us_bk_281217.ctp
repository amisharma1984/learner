 <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
              <div class="contact_info" >
                  <div class="row">
                      <div class="col-sm-5 col-sm-push-7">
					  <?php echo $this->Session->flash();?>
                        <div class="contact_head">
                          <p>Please fill this form if you would like to contact us.</p>
                        </div>

						<?php  echo $this->Form->create('AdminMaster' , array('id'=>'form1', 'class' => 'adminform', 'onsubmit' => "return form_validate('contact_us');", 'enctype' => 'multipart/form-data'));?>
	
                            <div class="forminfo">
                              <div class="form-group">
                                <input class="form-control" maxlength="100" name="name" id="name"  placeholder="Name*" type="text">
								 <span class="validationErrorM" id="name_err" style="display:none;"><?php echo __("Must be completed"); ?> </span>
                              </div>
							 
                              <div class="form-group">
                                <input class="form-control" name="address" maxlength="150" id="address" placeholder="Address*" type="text">
								 <span class="validationErrorM" id="address_err" style="display:none;"><?php echo __("Must be completed"); ?> </span>
                              </div>
                              
                              <div class="form-group">
                                <input class="form-control" maxlength="20"  name="mobile" id="mobile" placeholder="Mobile*" type="text">
								 <span class="validationErrorM" id="mobile_err" style="display:none;"><?php echo __("Must be completed"); ?> </span>
                              </div>
                              <div class="form-group">
                                <input class="form-control" maxlength="100" name="email" id="email" placeholder="Email*" type="text">
								 <span class="validationErrorM" id="email_err" style="display:none;"><?php echo __("Must be completed"); ?> </span>
								 <span class="validationErrorM" id="valid_email_err" style="display:none;"><?php echo __("Must be valid email"); ?> </span>
                              </div>
                             <!-- <div class="form-group">
                                <select class="selectpicker show-tick form-control">
                                  <option selected>Query Type</option>
                                  <option>Query Type 2</option>
                                </select>
                              </div>-->
                              <div class="form-group">
                               <textarea class="form-control" name="other_info" id="other_info" placeholder="Other Information"></textarea>
							    <span class="validationErrorM" id="other_info_err" style="display:none;"><?php echo __("Must be completed"); ?> </span>
                              </div>
                               
                              <div class="form-group">
                                <span class="def_btn2" ><input type="submit" class="btn btn-info" value="Submit"></span>

                              </div>

                            </div><?php echo $this->Form->end(); ?>
                    </div>
                    <div class="col-sm-7 col-sm-pull-5 ">
                        <div class="contact_head">
                          <h2>
                           
                            <strong>Contact Us</strong>
                          </h2>
                          <p>We also offer tutoring services at the following address.</p>
                        </div>
                        <div class="row">
                          <div class="col-sm-6">
                              <h3>
                                <strong>Address</strong>
                              </h3>
							<p>Learner Leader Online<br> 
							Suite 304<br>
							398 Chapel Rd<br>
							Bankstown<br>
							Postcode 2200<br>
							NSW Australia <br>
							(02) 9707 3067<br>
							info@learnerleaderonline.com
							</p>
                          </div>
                          <div class="col-sm-6">
                              <h3>
                                <strong>Email Address</strong>
                              </h3>
                              <p>
                                <a href="mailto:sales@learnerleader.com">sales@learnerleaderonline.com</a>
                                <a href="mailto:webmaster@learnerleader.com">webmaster@learnerleaderonline.com</a>
                                <a href="mailto:info@learnerleader.com">info@learnerleaderonline.com</a>
                              </p>
                          </div>
                          </div>
                           <div class="row">
                          <div class="col-sm-6">
                              <h3>
                                <strong>Phone</strong>
                              </h3>
                              <p>
                                ABN 61 201 907 800<br>
                                Landline : (02) 9707 3067 <br>
                                Fax : (02) 9707 3067 <br>
                              </p>
                          </div>

                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                           <iframe class="iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.9910379961857!2d151.03119341492416!3d-33.91563022892174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12bc10773dd567%3A0x141f75eb10a033c6!2s304%2F398+Chapel+Rd%2C+Bankstown+NSW+2200%2C+Australia!5e0!3m2!1sen!2sin!4v1486976463121" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>

                        

                    </div>
                 
                  </div>
              </div>

              <div class="contact_add_botton">
                <div class="row">
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
                 </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
	<script type="text/javascript">
function form_validate(field_type)
{
	var result=true;
	switch(field_type)
	{
		
	case 'contact_us' :
		 var email=$('#email').val();
         var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if($.trim($('#name').val()) == "")
		{
			$('#name_err').css('display', 'block');
			result=false;
		} else {
			$('#name_err').css('display', 'none');
		}
		
		if($.trim($('#address').val()) == "")
		{
			$('#address_err').css('display', 'block');
			result=false;
		} else {
			$('#address_err').css('display', 'none');
		}
		
		if($.trim($('#mobile').val()) == "")
		{
			$('#mobile_err').css('display', 'block');
			result=false;
		} else {
			$('#mobile_err').css('display', 'none');
		}
		
		if($.trim($('#email').val()) == "")
		{
			$('#email_err').css('display', 'block');
			result=false;
		} else if($.trim($('#email').val()) != "" && !emailReg.test( email ))
		{
			$('#valid_email_err').css('display', 'block');
			$('#email_err').css('display', 'none');
			result=false;
		} else {
			$('#email_err').css('display', 'none');
			$('#valid_email_err').css('display', 'none');
		}
		
		if($.trim($('#other_info').val()) == "")
		{
			$('#other_info_err').css('display', 'block');
			result=false;
		} else {
			$('#other_info_err').css('display', 'none');
		}
		
		break;
	}
	return result;
}

</script>
<style>
.validationErrorM{
	color:red;
	font-size:13px;
	font-family: 'Roboto Slab', serif;
	font-weight:400;

}
</style>