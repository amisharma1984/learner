   <?php 
	  if($this->params['action'] == 'primary_school'){
		  $schoolType = 'primary';
		  $heading = 'primary';
	  } else {
		  $schoolType = 'high-school';
		  $heading = 'high school';
	  }
   ?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online trial exams</strong></h2>
</div>				
 <div class="available_exam_list">				
<table>
  <tr>
    <th>List</th>
	<th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Price($ AUD)</th>
	<th>Action</th>
  </tr>

  <?php
  if(!empty($allExamAr)){ 
	  $i = 1;
	  foreach($allExamAr as $val){
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
    <td><?php echo $i?></td>
    <td><?php echo  ucwords(strtolower($val['Examination']['title']));?></td>
    <td><?php echo $val['ExaminationType']['name'];?></td>
    <td><?php echo $val['ExaminationCategory']['name'];?></td>
	 <td align="center"><?php echo $val['Examination']['price'];?></td>
	
	  <td width="170" align="center"> 
	  <a target="_blank" href="<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId;?>" style="color:green;">Sample Questions</a>
	  
	  </td>
  </tr>
<?php
		$i++;
	  }
	}
?>
</table>
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
                <div class="row">
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
                 </div>
                </div>

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
	<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
}
	</style>