   <?php 
	  if($this->params['action'] == 'primary_school'){
		  $schoolType = 'primary';
		  $heading = 'primary';
	  } else {
		  $schoolType = 'high-school';
		  $heading = 'high school';
	  }
   ?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online trial exams</strong></h2>
</div>				
 <div class="available_exam_list hm-table">	    
<table  class="student-list-apr" style="width:55%; float:left;">
  <tr>
    <th>Year</th>
	<th>Exam Category</th>
	<th>Exam Name</th>
	<th>Price per student<br>($ AUD)</th>
	<th>Click below to see sample questions</th>
  </tr>

  <?php
  if(!empty($allExamAr)){ 
	  $i = 1;
	  foreach($allExamAr as $val){
		    $category = explode(' ',$val['ExaminationCategory']['name']);
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
    <td><?php echo $category[1]; ?></td>
    <td>NAPLAN Style Trial<?php //echo  ucwords(strtolower($val['Examination']['title']));?></td>
    <td><?php echo  ucwords(strtolower($val['Examination']['title']));//echo $val['ExaminationType']['name'];?></td>
	 <td align="center"><?php echo $val['Examination']['price'];?></td>
	
	  <td width="170" align="center"> 
	  <a target="_blank" href="<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId;?>" style="color:green;">Click Here</a>
	  
	  </td>
  </tr>
<?php
		$i++;
	  }
	}
?>
</table>
<?php echo $this->element('site_video');?>
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
                <div class="row">
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
                 </div>
                </div>

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
	<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
}
	</style>