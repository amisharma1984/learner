<?php 

	if($this->params['action'] == 'primary_student'){
		$studentType = 'primary';
		$heading = 'primary';
	} else {
		$studentType = 'high-school';
		$heading = 'high school';
	}
?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
				
				
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online practice exams</strong></h2>
</div>				
				
 <div class="available_exam_list hm-table">	    
<table  class="student-list-apr" style="width:55%; float:left;">
  <tr>
  <!--<th>List</th>-->
	<th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
    <th>Number of Exam Available</th>
    
	 <th>Price of each exam($ AUD)</th>
	 <th>Click Below to attempt sample questions</th>
	
    <!--<th>Click to purchase Exams</th>-->
   
	
  </tr>
  
  <?php
  if(!empty($allExamAr)){ //pr($allExamAr);
	  $j = 1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		  ?>
		  
<?php 
//if(@$allExamAr[1]['Examination'][3]['id'] == 20 && @$allExamAr[1]['Examination'][4]['id'] == 21 && @$allExamAr[1]['Examination'][5]['id'] == 22){

?>
		  
		  <tr>
	<!--<td><?php echo $j;?></td>-->
	 <td><?php echo $category[1]; ?></td> 
    <td>NAPLAN Style</td>
    <td>Numeracy Practice Exam <?php //echo $val['Examination'][0]['title'];?></td>
   
   <td>
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		 if($v['id'] != 20 && $v['id'] != 21 && $v['id'] != 22){
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span style="color:red;"> <?php echo $v['paper']//$i;?> (Already purchased)</span>
	<?php } else {?>
	<a href="<?php echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['paper']//$i;?></a>
	<?php
		 }
		 echo '<br>';
		 $i++;
	 }
		 }
	?>
	
	</td>
	<td>
		<?php 
		 foreach($val['Examination'] as $vp){ 
		 if($vp['id'] != 20 && $vp['id'] != 21 && $vp['id'] != 22){
			echo $vp['price'];
			echo '<br>';
		 }
		 }
	    ?>
	</td>
    <td>
	
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		 
		  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['id']));
		  if($v['exam_flag'] == 2){
			  $exam_flag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['exam_flag']));
		  } else {
			 $exam_flag = ''; 
		  }
		  
		if($v['id'] != 20 && $v['id'] != 21 && $v['id'] != 22){  
	?>
	 <a target="_blank" href="<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_flag;?>">Click Here</a>
		 <?php 
			 echo '<br>';
		 }
		 }
		 
		 ?>
	</td>
	
	  
  </tr>
		  <?php //}?>
  
  
<?php
	$j++;
	  }
  }
?>

<tr>
	
	 <td>9</td> 
    <td>NAPLAN Style</td>
    <td>Literacy Practice Exam<?php //echo $val['Examination'][0]['title'];?></td>
   
   <td>
	<a href="<?php echo $this->webroot.'students/checkout_new/';?>"> Test 1</a><br>
	<a href="<?php echo $this->webroot.'students/checkout_new/';?>"> Test 2</a><br>
	<a href="<?php echo $this->webroot.'students/checkout_new/';?>"> Test 3</a>
	</td>
	<td>
	2.00 <br>
	2.00 <br>
	2.00 
	</td>
    <td>
	 <a target="_blank" href="<?php echo $this->webroot.'students-access/online-naplan-practice/high-school/sample-questions/MjA/';?>">Click Here</a><br>&nbsp;<br>&nbsp;
	</td>
	
	  
  </tr>
</table>

<?php echo $this->element('site_video');?>
			
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
			   <div class="row">
				<?php if($this->params['action'] == 'primary_student'){?>
                 
						  <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic3.jpg" alt="">
						 </div>
				 
				<?php } else if($this->params['action'] == 'high_school_student'){?>
				
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
						 </div>
				
				<?php 
					} else { //echo 'hiiiiiii';
				}
				?>
				 
                </div>
			  
			  
              

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
	}
	

</style>