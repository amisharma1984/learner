 <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
              <div class="contact_info" >
                  <div class="row">
                      <div class="col-sm-5 col-sm-push-7">
                        <div class="contact_head">
                          <h2>
                            <span>Contact</span>
                            <strong>Form</strong>
                          </h2>
                          <p>We also offer tutoring services at the following address.</p>
                        </div>

                        <form accept="" method="post">
                            <div class="forminfo">
                              <div class="form-group">
                                <input class="form-control " placeholder="Name*" type="text">
                              </div>
                              <div class="form-group">
                                <input class="form-control " placeholder="Address*" type="text">
                              </div>
                              <div class="form-group">
                                <input class="form-control " placeholder="Mobile*" type="tel">
                              </div>
                              <div class="form-group">
                                <input class="form-control " placeholder="Mobile*" type="text">
                              </div>
                              <div class="form-group">
                                <input class="form-control " placeholder="Email*" type="email">
                              </div>
                              <div class="form-group">
                                <select class="selectpicker show-tick form-control">
                                  <option selected>Query Type</option>
                                  <option>Query Type 2</option>
                                </select>
                              </div>
                              <div class="form-group">
                               <textarea class="form-control" placeholder="Other Information"></textarea>
                              </div>
                                <p class="req_field">*Denotes Required Field</p>
                              <div class="form-group">
                                <span class="def_btn2" ><input type="submit" class="btn btn-info" value="Submit"></span>

                              </div>

                            </div>

                        </form>

                    </div>
                    <div class="col-sm-7 col-sm-pull-5 ">
                        <div class="contact_head">
                          <h2>
                            <span>Info</span>
                            <strong>Contact Us</strong>
                          </h2>
                          <p>We also offer tutoring services at the following address.</p>
                        </div>
                        <div class="row">
                          <div class="col-sm-6">
                              <h3>
                                <strong>Address</strong>
                              </h3>
                                <p>S & G Publishing and Coaching Academy<br>
                                Suite 304<br>
                                398 Chapel Rd<br>
                                Bankstown</p>
                          </div>
                          <div class="col-sm-6">
                              <h3>
                                <strong>Email Address</strong>
                              </h3>
                              <p>
                                <a href="mailto:sales@sngweb.com">sales@sngweb.com</a>
                                <a href="mailto:webmaster@sngweb.com">webmaster@sngweb.com</a>
                              </p>
                          </div>
                          </div>
                           <div class="row">
                          <div class="col-sm-6">
                              <h3>
                                <strong>Phone</strong>
                              </h3>
                              <p>
                                ABN 61 201 907 800<br>
                                Landline : (02) 9707 3067 <br>
                                Fax : (02) 9707 3067 <br>
                              </p>
                          </div>

                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                           <iframe class="iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3310.9910379961857!2d151.03119341492416!3d-33.91563022892174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12bc10773dd567%3A0x141f75eb10a033c6!2s304%2F398+Chapel+Rd%2C+Bankstown+NSW+2200%2C+Australia!5e0!3m2!1sen!2sin!4v1486976463121" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                           </div>
                        </div>

                        

                    </div>
                 
                  </div>
              </div>

              <div class="contact_add_botton">
                <div class="row">
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
                 </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>