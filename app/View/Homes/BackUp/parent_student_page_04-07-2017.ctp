<?php 
	if($this->params['action'] == 'primary_student'){
		$studentType = 'primary';
		$heading = 'primary';
	} else {
		$studentType = 'high-school';
		$heading = 'high school';
	}
?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
				
				
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online practice exams</strong></h2>
</div>				
				
 <div class="available_exam_list">				
<table  class="student-list-apr">
  <tr>
  <!--<th>List</th>-->
	<th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
    <th>Number of Exam Available</th>
    
	 <th>Price of each exam($ AUD)</th>
	 <th> Sample Questions</th>
	
    <!--<th>Click to purchase Exams</th>-->
   
	
  </tr>
  
  <?php
  if(!empty($allExamAr)){ //pr($allExamAr);
	  $j = 1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		 
		  ?>
		  <tr>
	<!--<td><?php echo $j;?></td>-->
	 <td><?php echo $category[1]; ?></td>
    <td>NAPLAN Style</td>
    <td>Numeracy Practice Exam</td>
   
   <td>
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span style="color:red;"> <?php echo $v['title']//$i;?> (Already purchased)</span>
	<?php } else {?>
	<a href="<?php echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['title']//$i;?></a>
	<?php
		 }
		 echo '<br>';
		 $i++;
	 }
	?>
	
	</td>
	<td>
		<?php 
		 foreach($val['Examination'] as $vp){ 
			echo $vp['price'];
			echo '<br>';
		 }
	    ?>
	</td>
    <td>
	
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['id']));
	?>
	 <a target="_blank" href="<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId;?>">Click Here to attempt sample questions</a>
		 <?php 
			 echo '<br>';
		 }
		 ?>
	</td>
	
	  
  </tr>
<?php
	$j++;
	  }
  }
?>
</table>

<div><img src="<?php echo $this->webroot.'img/video-play.jpg';?>"></div>
			
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
                <div class="row">
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
                 </div>
                 <div class="col-sm-4">
                      <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
                 </div>
                </div>

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
	<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
}
	</style>