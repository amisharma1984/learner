   <?php 
 
	  if($this->params['action'] == 'primary_school'){
		  $schoolType = 'primary';
		  $heading = 'primary';
	  } else {
		  $schoolType = 'high-school';
		  $heading = 'high school';
	  }
   ?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online trial exams</strong></h2>
</div>				
 <div class="available_exam_list hm-table">	    
<table  class="student-list-apr" style="width:55%; float:left;">
  <tr>
    <th>Year</th>
	<th>Exam Category</th>
	<th>Exam Name</th>
	<th>Price per student<br>($ AUD)</th>
	<th>Click below to see sample questions</th>
  </tr>

  <?php
  if(!empty($allExamAr)){ 
	  $i = 1;
	  foreach($allExamAr as $val){
		    $category = explode(' ',$val['ExaminationCategory']['name']);
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
			
			if($val['Examination']['exam_flag'] == 2){
			  $exam_section_id1 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 1)); //Reading booklet questions
			  
			  $exam_section_id2 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 2)); // Language Conventions
			  
			  $exam_section_id3 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 3));  //Writing Leaflet
			  
			  $exam_flag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 2));
		  } 
			
			
		  ?>
		  
		  
		  
		  <tr>
    <td><?php echo $category[1]; ?></td>
    <td>NAPLAN Style Trial<?php //echo  ucwords(strtolower($val['Examination']['title']));?></td>
    <td><?php echo  ucwords(strtolower($val['Examination']['title']));//echo $val['ExaminationType']['name'];?></td>
	 <td align="center"><?php echo $val['Examination']['price'];?></td>
	
	  <td width="170" align="center"> 
	  
	  <?php if($val['Examination']['exam_flag'] == 2){?>
	  
	  
<div class="modal fade" id="SamplaeExamPopUp_<?php echo $val['Examination']['id'];?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Literacy Online Trial Exam:</h4>
            </div> 
            <div class="modal-body"> 
			<a href="#" onclick="openLink('<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id1;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Reading booklet questions</a>
			
			<a href="#" onclick="openLink('<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id2;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Language Conventions</a>
			
			
			
			<a href="#" onclick="openLink('<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id3.'/'.$exam_flag;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Writing Leaflet</a>
           	
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>	
	
	
	
	<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#SamplaeExamPopUp_<?php echo $val['Examination']['id'];?>">Click Here</a>
	  
	  
	  
	  
	   <!--<a target="_blank" href="<?php //echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId;?>" style="color:green;">Click Here</a>-->
	   
	  <?php } else {?>
	  <a target="_blank" href="<?php echo $this->webroot.'schools-access/online-testing/'.$schoolType.'/sample-questions/'.$ExaminationId;?>" style="color:green;">Click Here</a>
	  
	  <?php }?>
	  
	  
	  
	  </td>
  </tr>
<?php
		$i++;
	  }
	}
?>
</table>
<?php echo $this->element('site_video');?>
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
                <div class="row">
				<?php if($this->params['action'] == 'primary_school'){?>   
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/school_add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/school_add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/school_add_pic3.jpg" alt="">
						 </div>
				<?php } else if($this->params['action'] == 'high_school'){?>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/high_school_add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/high_school_add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/high_school_add_pic3.jpg" alt="">
						 </div>
				<?php 
					} else { //echo 'hiiiiiii';
				}
				?>
				 
                </div>

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
	<script type="text/javascript">
    function openLink(url){
            //var win = window.open(url, '_blank');
            var win = window.open(url);
    }
	
 </script>
	<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
}
	</style>