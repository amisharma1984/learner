<?php 
	if($this->params['action'] == 'primary_student'){
		$studentType = 'primary';
		$heading = 'primary';
	} else {
		$studentType = 'high-school';
		$heading = 'high school';
	}
?>
   <div class="container">
      <div class="profile_section inner_field">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact_details">
                <div class="inner_block">
                  <div class="contact_head">
                      <h2>
                        <strong><?php echo !empty($cmsData['ContentManagement']['title']) ? $cmsData['ContentManagement']['title'] : $page_heading;?></strong>
                      </h2>
                    </div>
                    <?php
					       if(!empty($cmsData)){
							   echo $cmsData['ContentManagement']['description'];
						   } else {
							   echo 'Coming soon..........';
						   }
					?>
					
                </div>
				
				
<div class="contact_head">
	<h2><strong>available <?php echo $heading;?> online practice exams</strong></h2>
</div>				
				
 <div class="available_exam_list hm-table">	    
<table  class="student-list-apr" style="width:55%; float:left;">
  <tr>
  <!--<th>List</th>-->
	<th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
    <th>Number of Exam Available</th>
    
	 <th>Price of each exam($ AUD)</th>
	 <th>Click Below to attempt sample questions</th>
	
    <!--<th>Click to purchase Exams</th>-->
   
	
  </tr>
  
  <?php
  if(!empty($allExamAr)){ //pr($allExamAr);
	  $j = 1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		  ?>
		  
<?php 
//if(@$allExamAr[1]['Examination'][3]['id'] == 20 && @$allExamAr[1]['Examination'][4]['id'] == 21 && @$allExamAr[1]['Examination'][5]['id'] == 22){

?>
		  
		  <tr>
	<!--<td><?php echo $j;?></td>-->
	 <td><?php echo $category[1]; ?></td> 
    <td>NAPLAN Style</td>
    <td>Numeracy Practice Exam <?php //echo $val['Examination'][0]['title'];?></td>
   
   <td>
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		if($v['exam_flag'] != 2){
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span style="color:red;"> <?php echo $v['paper']//$i;?> (Already purchased)</span>
	<?php } else {?>
	<a href="<?php echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['paper']//$i;?></a>
	<?php
		 }
		 echo '<br>';
		 $i++;
	 }
		 }
	?>
	
	</td>
	<td>
		<?php 
		 foreach($val['Examination'] as $vp){ 
		 if($vp['exam_flag'] != 2){
			echo $vp['price'];
			echo '<br>';
		 }
		 }
	    ?>
	</td>
    <td>
	
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		 
		  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['id']));
		  if($v['exam_flag'] == 2){
			  $exam_flag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['exam_flag']));
		  } else {
			 $exam_flag = ''; 
		  }
		  
		if($v['exam_flag'] != 2){  
	?>
	 <a target="_blank" href="<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_flag;?>">Click Here</a>
		 <?php 
			 echo '<br>';
		 }
		 }
		 
		 ?>
	</td>
	
	  
  </tr>
		  <?php //}?>
  
  
<?php
	$j++;
	  }
  }
?>

<!--2nd part of same array-->
 <?php
  if(!empty($allExamLiteracyArr)){ //pr($allExamAr);
	  $j = 1;
	  foreach($allExamLiteracyArr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		  ?>
		  
<?php 
//if(@$allExamAr[1]['Examination'][3]['id'] == 20 && @$allExamAr[1]['Examination'][4]['id'] == 21 && @$allExamAr[1]['Examination'][5]['id'] == 22){

?>
		  
		  <tr>
	<!--<td><?php echo $j;?></td>-->
	 <td><?php echo $category[1]; ?></td> 
    <td>NAPLAN Style</td>
    <td>Literacy Practice Exam <?php //echo $val['Examination'][0]['title'];?></td>
   
   <td>
	<?php 
		$i = 1;
		$abc = array();
		 foreach($val['Examination'] as $v){ 
		 
		if($v['exam_flag'] == 2){
			
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span style="color:red;"> <?php echo $v['paper']//$i;?> (Already purchased)</span>
	<?php } else {?>
	<a href="<?php echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['paper']//$i;?></a>
	<?php
		 }
		 echo '<br>';
		 $i++;
	 }
		 }
		
	?>
	
	</td>
	<td>
		<?php 
		 foreach($val['Examination'] as $vp){ 
		 if($vp['exam_flag'] == 2){
			echo $vp['price'];
			echo '<br>';
		 }
		 }
	    ?>
	</td>
    <td>
	
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){ 
		  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['id']));
		
		  if($v['exam_flag'] == 2){
			  $exam_section_id1 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 1)); //Reading booklet questions
			  
			  $exam_section_id2 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 2)); // Language Conventions
			  
			  $exam_section_id3 = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 3));  //Writing Leaflet
			  
			  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $v['id']));
			  $exam_flag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 2));
		  } 
		if($v['exam_flag'] == 2){  
		
	?>
	
	 <!--1st modal -->
 
<div class="modal fade" id="SamplaeExamPopUp_<?php echo $v['id'];?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Literacy Practice Exam:</h4>
            </div> 
            <div class="modal-body">
			<a href="#" onclick="openLink('<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id1;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Reading booklet questions</a>
			
			<a href="#" onclick="openLink('<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id2;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Language Conventions</a>
			
			
			
			<a href="#" onclick="openLink('<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_section_id3.'/'.$exam_flag;?>')" class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Writing Leaflet</a>
           	
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>	
	
	
	
	<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#SamplaeExamPopUp_<?php echo $v['id'];?>">Click Here</a>
	 <!--<a target="_blank" href="<?php echo $this->webroot.'students-access/online-naplan-practice/'.$studentType.'/sample-questions/'.$ExaminationId.'/'.$exam_flag;?>"></a>-->
		 <?php 
			 echo '<br>';
		 }
		 }
		 
		 ?>
	</td>
	
	  
  </tr>
		
 
	
<?php
	$j++;
	  }
  }
?>




</table>

<?php echo $this->element('site_video');?>
			
</div>
				
		 <div class="row">&nbsp;&nbsp;&nbsp;</div>		

              <div class="contact_add_botton">
			   <div class="row">
				<?php if($this->params['action'] == 'primary_student'){?>
                 
						  <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/primary_add_pic3.jpg" alt="">
						 </div>
				 
				<?php } else if($this->params['action'] == 'high_school_student'){?>
				
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic1.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic2.jpg" alt="">
						 </div>
						 <div class="col-sm-4">
							  <img src="<?php echo $this->webroot;?>images/add_pic3.jpg" alt="">
						 </div>
				
				<?php 
					} else { //echo 'hiiiiiii';
				}
				?>
				 
                </div>
			  
			  
              

              </div>
            </div>

          </div>
		  
        </div>
      </div>
    </div>
	
	
	<!--<div class="modal fade" id="alertToLogin" tabindex="-1" role="dialog" aria-labelledby="alertToLogin" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Alert !</h4>
            </div> 
			
			 <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block">You need to login to access this ! </button>
            </div>
			
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>-->

	
	<script type="text/javascript">
    function openLink(url){
            //var win = window.open(url, '_blank');
            var win = window.open(url);
    }
	
 </script>
<style>
	table th {
    background: #022a58;
    padding: 10px;
    color: #fff;
	}
	

</style>