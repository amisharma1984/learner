
 <style>
  .carousel-fade .carousel-inner .item {
  opacity: 0;
  transition-property: opacity;
}
.carousel-fade .carousel-inner .active {
  opacity: 1;
}
.carousel-fade .carousel-inner .active.left,
.carousel-fade .carousel-inner .active.right {
  left: 0;
  opacity: 0;
  z-index: 1;
}
.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {
  opacity: 1;
}
.carousel-fade .carousel-control {
  z-index: 2;
}
 </style>
 <script type="text/javascript">


  $(document).ready(function () {
  $('.carousel').carousel({
    interval: 10000,
    pause: false 
  })
        });

    </script>
	
	<?php 
	$schoolData = $this->requestAction(array('controller' => 'homes', 'action' => 'fetch_content',13));
	$studentParentData = $this->requestAction(array('controller' => 'homes', 'action' => 'fetch_content',14));
	?>

<div class="schools_details_boxes">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="schools_acess">
                    <div class="schools_acess_inner">
                       <div class="row">
                       <div class="col-sm-5 col-md-12 col-lg-5">
                        <div class="schools_acess_inner_img">
     <div  class="carousel slide carousel-fade">
<div class="carousel-inner">
                              <div class="item active">
                               <img src="<?php echo $this->webroot;?>images/blog2.jpg" alt=""/>
                              </div>

                              <div class="item">
                               <img src="<?php echo $this->webroot;?>images/blog1.jpg" alt=""/>
                              </div>

                            </div> 
                            </div> 


                        </div>
                        </div>
                        <div class="col-sm-7 col-md-12 col-lg-7">
                        <div class="schools_acess_inner_text">
                            <h3><img src="<?php echo $this->webroot;?>images/sc_i.png" alt=""/> School Access</h3>
                            <h4>ONLINE NAPLAN TRIAL</h4>
                            <div class="choose_school">
                                <a href="<?php echo $this->webroot.'schools-access/online-testing/primary';?>">Primary</a>
                                <a href="<?php echo $this->webroot.'schools-access/online-testing/high-school/';?>" class="hight_s">High School</a>
                            </div>
							<?php if(!empty($schoolData['ContentManagement']['description'])){
								echo $schoolData['ContentManagement']['description'];
							} else {
								?>
                            <p>Use this portal to access the online 
NAPLAN trial paper, view sample questions and learn benefits to your school.</p>
							<?php }?>

                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="schools_acess schools_acess_blue">
                    <div class="schools_acess_inner">
                       <div class="row">
                       <div class="col-sm-5 col-md-12 col-lg-5">
                        <div class="schools_acess_inner_img">
                             <div  class="carousel slide carousel-fade"> 
<div class="carousel-inner">
                              <div class="item active">
                               <img src="<?php echo $this->webroot;?>images/blog3.jpg" alt=""/>
                              </div>

                              <div class="item">
                               <img src="<?php echo $this->webroot;?>images/blog4.jpg" alt=""/>
                              </div>

                            </div>
                            </div> 
                        </div>
                        </div>
                        <div class="col-sm-7 col-md-12 col-lg-7">
                        <div class="schools_acess_inner_text">
                            <h3><img src="<?php echo $this->webroot;?>images/sc_i2.png" alt=""/> Student/Parent Access</h3>
                            <h4>ONLINE NAPLAN PRACTICE</h4>
                            <div class="choose_school">
                                <a href="<?php echo $this->webroot.'students-access/online-practice/primary';?>">Primary</a>
                                <a href="<?php echo $this->webroot.'students-access/online-practice/high-school';?>" class="hight_s">High School</a>
                            </div>
							<?php if(!empty($studentParentData['ContentManagement']['description'])){
								echo $studentParentData['ContentManagement']['description'];
							} else {
								?>
                            <p>Use this portal to access practice questions, view sample questions and learn about how you can learn from home.</p>
							<?php }?>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="gallery_images">
   <div class="container">
       <div class="row">
           <div class="col-sm-3">
              <div class="gallery_images_box">
           <img src="<?php echo $this->webroot;?>images/pic1.jpg" alt=""/>
           </div>
           </div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic2.jpg" alt=""/></div></div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic3.jpg" alt=""/></div></div>
           <div class="col-sm-3"><div class="gallery_images_box"><img src="<?php echo $this->webroot;?>images/pic4.jpg" alt=""/></div></div>
       </div>
   </div>
    
</div>