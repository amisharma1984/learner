<?php $url = $this->Html->url('/', true);?>

<p style="font-size: 14px; margin-bottom: 7px; margin-top: 10px;margin-left:-200px;"><?php echo __('Dear %s,', ucwords($fullName)); ?></p>
<p style="margin-left:-200px;"><?php echo __('Please find the information details below:'); ?></p>
<p style="margin-left:-200px;"> <?php echo $message; ?></p>

<p style="margin-left:-200px;"><strong>Learner Leader Team</strong></p>
<p></p>
<p style="font-size: 14px; margin-bottom: 5px;"></p>
<p style="font-size: 14px; margin-bottom: 5px;"><img width="202" height="54" title="logo.png" alt="" src="https://learnerleaderonline.com/files/editor/images/59ca3536-5c34-4d47-9dab-66ab176f8145.png"></p>
<p style="margin-left:-200px;">***************************************************************************************</p>
<p style="margin-left:-200px;">This message is intended for the addressee named and may contain privileged information or confidential information or both.If you are not the intended recipient, please delete it and notify the sender.</p>
<p style="margin-left:-200px;">***************************************************************************************</p>