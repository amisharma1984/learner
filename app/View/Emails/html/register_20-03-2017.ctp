<?php
$url = $this->Html->url('/', true);
$schoolUrl = $this->Html->url('/', true).'schools';
?>
<p style="font-size: 14px; margin-bottom: 7px;margin-top:10px;"><?php echo __('Dear %s,', ucwords($fullName)); ?></p>
<p style="font-size: 14px; margin-bottom: 5px; font-weight: normal;"><?php echo __('Thank you for joining with us. %s to start your subscription', $this->Html->link('Click here', $schoolUrl, array('style' => 'text-decoration: none;color: #00869B;'))); ?> </p>
<p style="font-size: 14px; margin-bottom: 5px;"><?php echo __('Here is your login credentials:'); ?></p>
<p style="font-size: 14px; margin-bottom: 5px;">
	<strong><?php echo __('User:'); ?></strong> <?php echo $username; ?><br />
	<strong><?php echo __('Password:'); ?></strong> <?php echo $password; ?>
</p>
<p style="font-size: 14px; margin-bottom: 5px;"><?php echo __('Note: your subscription in %s will start after making a payment and It will valid till '.$validity.'days from the date of your payment', $this->Html->link('Naplan Exam', $url, array('style' => 'text-decoration: none;color: #00869B;'))); ?></p>
<p style="font-size: 14px; margin-bottom: 5px;"><?php echo __('Team %s', $this->Html->link('Naplan Exam', $url, array('style' => 'text-decoration: none;color: #00869B;'))) ?></p>
