<?php
$url = $this->Html->url('/', true);

?>
<p style="font-size: 14px; margin-bottom: 7px;margin-top:10px;"><?php echo __('We heard that you lost your %s password. Sorry about that!', Configure::read('App.appName')); ?> </p>
<p style="font-size: 14px; margin-bottom: 5px;"><?php echo __('But don\'t worry! You can use the following link to reset your password:'); ?></p>
<p><a href="<?php echo $userData['resetLink']; ?>" style="text-decoration: none;color: #00869B; font-size: 14px;"><?php echo $userData['resetLink']; ?></a></p>
<p style="font-size: 14px; margin-bottom: 5px;"><?php echo __('Thanks,<br>Team %s', $this->Html->link(Configure::read('App.appName'), $url, array('style' => 'text-decoration: none;color: #00869B;'))) ?></p>
