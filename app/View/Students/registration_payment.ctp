<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $title_for_layout; ?></title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        
        <style type="text/css">
        .help-block{
            display: block;
    margin-top: 5px;
    margin-bottom: 10px;
    color: #959595;
        }
        </style>
    </head>
    <body>
        <h1>Hello, <?php echo ucwords($schooldata['School']['manager_name']); ?>!</h1>
        <p>Welcome to <strong><?php echo Configure::read('App.appName'); ?></strong>&nbsp; &nbsp;<a href="<?php echo $this->Html->url(array('controller' => 'schools', 'action'=>'logout'), true);?>">Logout</a></p>
<!--        <p>Please make a payment of <?php echo $paypalFormSettings['amount'].$paypalFormSettings['currency_code']; ?>&nbsp;to get the benifits of <strong><?php echo Configure::read('App.fullName'); ?></strong></p>-->
        <?php echo $this->Session->flash(); ?>
        <p class="help-block" style="display:none;" ><span style="color:#c71c22;"> Please don't change the registration fees.</span></p>
        <?php
        if(!empty($paypalFormSettings)){
            $formAction = $paypalFormSettings['payment_url'];
            unset($paypalFormSettings['payment_url']);
            
            echo $this->Form->create(null, array('url' => $formAction, 'id' => 'payPalPaymentForm'));
            
            foreach ($paypalFormSettings as $key => $value){
                echo '<input type="hidden" name="'.$key.'" value="'.$value.'" >';
            }
            echo $this->Form->end('Pay Now');
        }
        ?>
        
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?php echo Router::url('/', true); ?>js/jquery.validate.min.js"></script>
        
        <script type="text/javascript">
        $(function(){
            
            /* PayPal form Validation */            
            $("#payPalPaymentForm").validate({
                submitHandler: function(form) {
                
                    var amount = $("#payPalPaymentForm").find('input[name=amount]').val();
                    var custom = $("#payPalPaymentForm").find('input[name=custom]').val();
                    
                    var jqxhrPayPalForm = $.ajax({
                        type: "POST",
                        url: "<?php echo Router::url('/', true); ?>schools/checkFingerprint",
                        data: {
                            amount: amount,
                            custom: custom
                        },
                        success: function (response) {
                            console.log(response);
                            if(response != ''){
                                $('.help-block').fadeIn();
                            }else{
                                $('.help-block').fadeOut();
                                form.submit();
                            }

                        }
                    });
                }
            });
            
            
        });   
        </script>
    </body>
</html>