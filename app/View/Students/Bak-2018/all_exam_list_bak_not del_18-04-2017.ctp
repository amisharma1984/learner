<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
	<?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Exam Category List</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
				<div class="prof_details prof_form">
					<table id="YourTableId">
						<tr>
						<th>List</th>
						<th>Exam Category</th>
						<th>Created Date</th>
						<th>Modified Date</th>
						<th>Action</th>
						</tr>

							<?php
							if(!empty($practiceExamCategory)){ 
							$i = 1;
							foreach($practiceExamCategory as $val){
							$category_id = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['ExaminationCategory']['id']));
							?>
							<tr>
							<td><?php echo $i?></td>
							<td><?php echo $val['ExaminationCategory']['name'];?></td>
							<td><?php echo $val['ExaminationCategory']['created'];?></td>
							<td><?php echo $val['ExaminationCategory']['modified'];?></td>
							<td> <a href="<?php echo $this->webroot.'students/purchase_exam/'.$category_id?>" style="color:green;">Purchase</a></td>
							</tr>
							<?php
							$i++;
							}
							}
							?>
					</table>
				</div>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>