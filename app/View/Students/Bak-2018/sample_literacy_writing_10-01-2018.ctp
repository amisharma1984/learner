<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>
    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>NEXT &#187;</a>");
			  } else {
				   // $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
				    //$(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SUBMIT</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function save_next(){
			
			
			 $("input:radio[class=att_rd]:checked").each(function () {
				 var attemptOptionVal = $(this).attr("attempt-option");
				  $("#"+ attemptOptionVal).addClass("activeQ");
				});
				
				 $('.input_count[value!=""]').each(function () {
				 var attemptTextArVal = $(this).attr("attempt-option");
				  $("#"+ attemptTextArVal).addClass("activeQ");
				});
			
			
			
			var textareaFieldCount = $('.input_count[value!=""]').length;	
			  var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
				  var totalattemptqs = (Number(textareaFieldCount) + Number(numberOfCheckedRadio));
					$('#attempted_qs').text(totalattemptqs);
					//$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
				 /*var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
										$('#attempted_qs').text(numberOfCheckedRadio);
										if(percentageBar == 100){
											 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
										} else {
											  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
										}
				                       
										
									   
										$('#qs_attempted_prcessing').css({"width": ""});
										$('#qs_attempted_prcessing').css({"width": percentageBar+"%"}); */
				 
			});	
		});
		
    </script>


<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
       <!-- <h2>Questions</h2>-->
       <div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
    	   </ul>
    </div>
	
 
</nav>




<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
      <div class="content_main">
	   <h2>Sample Questions</h2>
	   <p><?php //echo @$ExamDetails['Examination']['description'];?> Year 9 NAPLAN Trial Literacy Writing</p>
	  <div id="page-wrap">
		
		<div id="qusdetails" class="tabs">
 <?php echo $this->Form->create(null, array('onsubmit'=> "return confirm('Are you sure you want to submit and exit the exam?');",'url' => $this->Html->url(array('controller' => 'homes', 'action' => 'writing_leaflet_mail'), true))); ?>
 
	  <?php
	  if(!empty($QuestionAns)){ 
		
		  foreach($QuestionAns as $qval){ 
	?>
	<input type="hidden" name="questionids[]" value="<?php echo $qval['Question']['id']?>">
	   <div id="fragment1" class="ui-tabs-panel tbminwd admar">
	   
	    <div class="qus book_reading">
		<?php echo !empty($qval['Question']['reading_book_content']) ? $qval['Question']['reading_book_content'] : '';?>
		</div>
        <div class="qus qtxt"><span class="qs_num" style="position:relative; top:18px;"> </span> <?php echo $qval['Question']['title'];?></div>  
				   
<textarea name="descriptive_ans" cols="95" rows="20" class="input_count bg1_" placeholder="Type your response in the space provided"></textarea>	  
	
			   
        	</div>
	 
	  <?php 
				}
			}
	  ?>
	   
   <button type="submit" class="qs_save_btn"> SUBMIT</button>
         
	  <?php echo $this->Form->end(); ?>
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<style>
.qtxt p{
	margin-left:10px !important;
}
</style>