<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>
    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>NEXT &#187;</a>");
			  } else {
				    $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function save_next(){
			
			
			var textareaFieldCount = $('.input_count[value!=""]').length;	
			  var numberOfCheckedRadio = $('input:radio:checked').length;
			  
			  $("input:radio[class=att_rd]:checked").each(function () {
				 var attemptOptionVal = $(this).attr("attempt-option");
				  $("#"+ attemptOptionVal).addClass("activeQ");
				});
				
				 $('.input_count[value!=""]').each(function () {
				 var attemptTextArVal = $(this).attr("attempt-option");
				  $("#"+ attemptTextArVal).addClass("activeQ");
				});
			  
			  
			  
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
				  var totalattemptqs = (Number(textareaFieldCount) + Number(numberOfCheckedRadio));
					$('#attempted_qs').text(totalattemptqs);
					//$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
				
				
				
				 /*var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
										$('#attempted_qs').text(numberOfCheckedRadio);
										if(percentageBar == 100){
											 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
										} else {
											  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
										}
				                       
										
									   
										$('#qs_attempted_prcessing').css({"width": ""});
										$('#qs_attempted_prcessing').css({"width": percentageBar+"%"}); */
				 
			});	
		});
		
    </script>


<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
       <div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){ 
			?>
			<li><a id="attempt_<?php echo $i;?>" class="<?php echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php $i++;}}?>
    	   </ul>
    </div>
	
  <!--<div class="attamp_check">
          <label>Attempted</label>
        <label>Not attempted</label>
		
        </div>
        <div class="view_all_qu">
        <a href="javascript:void(0);">View All Questions</a>
        </div>-->
</nav>




<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
      <div class="content_main">
	   <h2><?php echo $ExamDetails['Examination']['title'];?></h2>
	   <p><?php //echo @$ExamDetails['Examination']['description'];?>
	   <?php 
	    if(!empty($exam_section_id) && $exam_section_id == 1){
			 echo $ExamDetails['ExaminationCategory']['name'].' NAPLAN Trial Literacy Reading Booklet and Questions';
		} else if(!empty($exam_section_id) && $exam_section_id == 2){
			 echo $ExamDetails['ExaminationCategory']['name'].' NAPLAN Trial Literacy Language Conventions';
		} else {
			 echo @$ExamDetails['Examination']['description'];
		}
	  
	   ?>
	   
	   </p>
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="qusdetails" class="tabs">
 <?php echo $this->Form->create(null, array('onsubmit'=> "return confirm('Are you sure you want to submit and exit the exam?');",'url' => $this->Html->url(array('controller' => 'students', 'action' => 'take_exam_save'), true))); ?>
 <input type="hidden" name="my_exam_id" value="<?php echo $my_exam_id;?>">
 <input type="hidden" name="page_action" value="<?php echo $this->params['action'];?>">
 <input type="hidden" name="exam_flag" value="<?php echo @$examFlag;?>">
  <input type="hidden" name="totalQuestionSet" value="<?php echo @$totalQuestions;?>">
  <input type="hidden" name="exam_section_id" value="<?php echo @$exam_section_id;?>">
 
<!-- <input type="hidden" name="totalQuestionSet" value="<?php echo @$totalQuestions;?>">
 <input type="hidden" name="student_ans_random_id" value="<?php echo @$student_ans_random_id;?>">-->
	  <?php
	  if(!empty($QuestionAns)){ 
		  $i = 1;
		  $k = 1;
		  foreach($QuestionAns as $qval){ 
	?>
	<input type="hidden" name="questionids[]" value="<?php echo $qval['Question']['id']?>">
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
	   
	   <?php if(!empty($qval['Question']['reading_book_content'])){?>
	    <div class="qus book_reading">
		<?php echo $qval['Question']['reading_book_content'];?>
		</div>
	   <?php }?>
        	       <div class="qus"><span class="qs_num question_pos_"><?php echo 'Q '.$i;?>. </span>
				   <?php 
				    if(empty($qval['Question']['box_for_pargraph_no'])){
				      echo str_replace('../../', '../../../',$qval['Question']['title']);
					}
				   ?>
				   </div>  
				   
		
		
		
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 1){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<!--<div class="text_area">
		<textarea class="first_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_26" placeholder="Enter your answer here"></textarea>
		</div>-->
	</div> 
	<?php }?>
	
	
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 2){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<!--<div class="text_area">
		<textarea class="second_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_26" placeholder="Enter your answer here"></textarea>
		</div>-->
	</div> 
	<?php }?>
	
	
	<?php 
	//echo $qval['Question']['box_for_pargraph_no'];
	if($qval['Question']['box_for_pargraph_no'] == 3){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<!--<div class="text_area">
		<textarea class="third_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_28" placeholder="Enter your answer here"></textarea>
		</div>-->
	</div> 
	<?php }?>
	
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 4){ 
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div> 
	</div> 
	<?php }?>
	
	
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 5){ 
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div> 
	</div> 
	<?php }?>
	
	
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 6){ 
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div> 
	</div> 
	<?php }?>
	
	
	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 7){ 
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div> 
	</div> 
	<?php }?>
	

	<?php 
	if($qval['Question']['box_for_pargraph_no'] == 8){ 
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>&nbsp; </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div> 
	</div> 
	<?php }?>
	
	
		
	<?php /*
	if($qval['Question']['id'] == 920){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>. </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<div class="text_area">
		<textarea class="first_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_26" placeholder="Enter your answer here"></textarea>
		</div>
	</div> 
	<?php }?>


<?php  
	if($qval['Question']['id'] == 921){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>. </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<div class="text_area">
		<textarea class="second_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_27" placeholder="Enter your answer here"></textarea>
		</div>
	</div> 
	<?php }?>	
	
	<?php 
	if($qval['Question']['id'] == 922){ ////this line temperory delete latter after dynamic is done
	?>
	<div class="question_textarea">
		<div class="qus qtxt blog_q"><span class="qs_num question_pos"><?php //echo 'Q '.$i;?>. </span>
		<?php echo str_replace('../../', '../../../',$qval['Question']['title']);?>

		</div>  
		<div class="text_area">
		<textarea class="third_textarea input_count" name="descriptive_ans[]"  attempt-option="attempt_28" placeholder="Enter your answer here"></textarea>
		</div>
	</div> 
	<?php } */?>	


	



	
				   
				  
<ul class="ans_field right_area">
 <?php 
  $j = 'a';

  foreach($qval['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" class="att_rd" attempt-option="attempt_'.$k.'" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" class="att_rd" attempt-option="attempt_'.$k.'" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio" class="att_rd" attempt-option="attempt_'.$k.'"  value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="'.$k.'_ans[]">';
		
	} else if($ansval['answer_type'] == 4){
		//$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'" class="input_count" placeholder="Enter your answer here"></textarea>';
		if(!empty($ansval['answer_unit'])){
			$space_prevention = 'onkeypress="space_prevent(event)"';
		} else {
			$space_prevention = '';
		}
		
		
		 if(empty($qval['Question']['box_for_pargraph_no'])){
			$answer_type = '<textarea name="descriptive_ans[]" class="input_count" '.$space_prevention.' attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>';
		 }
		$hidenTextArea = '<input type="hidden" value="'.$qval['Question']['id'].'" name="HiddenTextQid[]">';
		 $qid_920_21_22 =  $qval['Question']['id'];

		if($qval['Question']['box_for_pargraph_no'] == 1 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="first_textarea txtarea1 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		 
		 if($qval['Question']['box_for_pargraph_no'] == 2 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="second_textarea txtarea2 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		
		if($qval['Question']['box_for_pargraph_no'] == 3 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="third_textarea txtarea3 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		
		
		if($qval['Question']['box_for_pargraph_no'] == 4 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="fourth_textarea txtarea4 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		 
		 if($qval['Question']['box_for_pargraph_no'] == 6 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="fourth_textarea txtarea6 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		 
		 if($qval['Question']['box_for_pargraph_no'] == 8 && $qid_920_21_22 != 920 && $qid_920_21_22 != 921 && $qid_920_21_22 != 922){
			$answer_type = '<div class="text_area">
			<textarea class="fourth_textarea txtarea8 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		 }
		 
		if($qid_920_21_22 == 920){
			$answer_type = '<div class="text_area">
			<textarea class="first_textarea bgtxt_1 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		}
		
		if($qid_920_21_22 == 921){
			$answer_type = '<div class="text_area">
			<textarea class="second_textarea bgtxt_2 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		}
		
		if($qid_920_21_22 == 922){
			$answer_type = '<div class="text_area">
			<textarea class="third_textarea bgtxt_3 input_count" '.$space_prevention.' name="descriptive_ans[]"  attempt-option="attempt_'.$k.'" placeholder="Enter your answer here"></textarea>
			</div>';
		}
		
		
		
	}
	
	//echo strlen($ansval['answer_text']);
	if(strlen(strip_tags($ansval['answer_text'])) >= 20  && strpos($ansval['answer_text'], 'src="') == false) {
		$answerclass = 'big_answer';
	} else if(strpos($ansval['answer_text'], 'src="') !== false){
		$answerclass = 'big_answer';
	} else 	if(strlen(strip_tags($ansval['answer_text'])) <= 20  && strpos($ansval['answer_text'], 'src="') == false && count($qval['Answer']) >=5) {
		$answerclass = 'ans5_options';
	}
	else {
		$answerclass = 'ans_options';
	}
	
	//echo strlen(strip_tags($ansval['answer_text'])) >= 20 ? 'big_answer' : 'ans_options';
  ?>	
<?php if($ansval['answer_type'] == 4 && !empty($ansval['answer_unit']) && trim($ansval['answer_unit']) == '$'){?>
		   <div class="ans_unit_pre"><?php echo $ansval['answer_unit'];?></div>
       <?php }?> 
		 
<li class="<?php echo ($ansval['answer_type'] == 4) ? 'ans_options_txt_area' : $answerclass;?> ">
	<?php 
	
		// if(empty($qval['Question']['box_for_pargraph_no'])){ ////this line temperory delete latter after dynamic is done
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
			 echo $hidenTextArea;
		 } else {
		    echo $answer_type.'&nbsp;&nbsp;&nbsp;'.str_replace('../../', '../../../',$ansval['answer_text']);
		 }
		// }
		 
		 ?>
		 </li>
		<?php if($ansval['answer_type'] == 4 && !empty($ansval['answer_unit']) && trim($ansval['answer_unit']) != '$'){?>
		   <div class="ans_unit_post"><?php echo $ansval['answer_unit'];?></div>
       <?php }?> 
		  
		 
		 <?php
		 
		 $j++;
  } 
	?> 
</ul>	
			   
        	</div>
	
	  <?php 
	  
					if($qval['Question']['answer_type']){
						$k++;
					}
				 $i++;
				}
			}
	  ?>
	   
                    <button type="submit" class="qs_save_btn"> SUBMIT TEST</button>
         
	  <?php echo $this->Form->end(); ?>
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>



<!-- The Modal -->
<div id="myModalQs" class="modal_qs">

  <!-- Modal content -->
  <div class="modal-content_qs">
    <div class="modal-header_qs">
      <span class="close" onclick="close_popup()">&times;</span>
      <h4>Message from learner leader online</h4>
    </div>
    <div class="modal-body_qs">
      <p>Do not use space !</p>
     
    </div>
    <div class="modal-footer_qs">
      <h3><a href="javascript:void(0);" onclick="close_popup()" class="btn closebut btn-default">OK</a></h3>
    </div>
  </div>

</div>
<script>
// Get the modal
var modal = document.getElementById('myModalQs');
function close_popup(){
	 modal.style.display = "none";
}

function space_prevent(event)
{
   if(event.which ==32)
   {
      event.preventDefault();
	    modal.style.display = "block";
      return false;
   }
}

</script>

<style>
.question_pos {
	position:relative; top:18px !important;
}

.txtarea6 {
		margin-top:430px !important;
}

.txtarea8 {
		margin-top:552px !important;
}

</style>