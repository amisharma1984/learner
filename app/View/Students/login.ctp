<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no"/>
        <title><?php echo $title_for_layout; ?></title>	
        <?php echo $this->fetch('meta'); ?>
        <script type="text/javascript">
          //  var BASEURL = '<?php echo Router::url('/', true); ?>';
            //var pageControllerName = '<?php echo $this->request->params['controller']; ?>';
            //var pageActionName  = '<?php echo $this->request->params['action']; ?>';      
        </script>

        <!-- The styles -->
        <link href="<?php echo Router::url('/', true); ?>css/bootstrap-cerulean.min.css" rel="stylesheet">

        <link href="<?php echo Router::url('/', true); ?>css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/fullcalendar/dist/fullcalendar.print.css" rel="stylesheet" media="print">
        <link href="<?php echo Router::url('/', true); ?>bower_components/chosen/chosen.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/jquery.noty.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/noty_theme_default.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/elfinder.min.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/elfinder.theme.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/jquery.iphone.toggle.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/uploadify.css" rel="stylesheet">
        <link href="<?php echo Router::url('/', true); ?>css/animate.min.css" rel="stylesheet">

        <!-- jQuery -->
        <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo Router::url('/', true); ?>img/favicon.ico" type="image/x-icon">

    </head>

    <body>
        <div class="ch-container">
            <div class="row">

                <div class="row">
                    <div class="col-md-12 center login-header">
                        <h2>Welcome to Learner Leader Online</h2>
                    </div>
                    <!--/span-->
                </div><!--/row-->

                <div class="row">
                    <div class="well col-md-5 center login-box">
                        <div class="alert alert-default">
                            Please login with your Username and Password.
                        </div>
                        <?php echo $this->Session->flash(); ?>
                        <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); ?>
                            <fieldset>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                                    <?php echo $this->Form->input('Student.login', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Username', 'required' => true)); ?>
                                </div>
                                <div class="clearfix"></div><br>

                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                                    <?php echo $this->Form->input('Student.password', array('type' => 'password', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Password', 'required' => true)); ?>
                                </div>
                                <div class="clearfix"></div>

                                <div class="input-prepend">
                                    <label class="remember" for="remember"><?php echo $this->Form->input('Student.remember', array('type' => 'checkbox', 'label' => false, 'div' => false, 'id' => 'remember')); ?> Remember me</label>
                                </div>
                                <div class="clearfix"></div>

                                <p class="center col-md-5">
                                    <button type="submit" class="btn btn-primary">Login</button>
                                </p>
                            </fieldset>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <!--/span-->
                </div><!--/row-->
            </div><!--/fluid-row-->

        </div><!--/.fluid-container-->

        <!-- external javascript -->

        <?php echo $this->element('JS/external-scripts'); ?>
        <script type="text/javascript">
            $("#studentLoginForm").validate({
                rules: {
                    "data[Student][login]": {
                        required: true,
                        email: true
                    },
                },
                messages: {
                    "data[Student][login]": {
                        email: "Please enter a valid email address.",
                    }
                },
            });
        </script>

    </body>
</html>
