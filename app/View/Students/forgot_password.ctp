<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); 
							  echo $this->Form->create('Student'); 
							 ?>
                       
							<h2>Forgot Password</h2>
							<div class="form_cont">
							<div class="form-group">
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<?php echo $this->Form->input('Student.email', array('label' => false,'type' => 'text',  'placeholder' => 'Enter registered email address', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$EmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn" >Submit</button>
							</div>
							<div class="form-group">
								Don’t have an account? <a href="<?php echo $this->Html->url(array('controller'=>'students','action' => 'registration')) ?>" > Sign Up!</a>
								
							</div>
							</div>
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
                                  
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>