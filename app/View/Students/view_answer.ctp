<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
<style>
.corrct_ans_qs {
	color:green;
}

.wrong_ans_qs {
	color:red;
}

.notattempt_ans_qs {
	color:orange;
}
</style>	
	
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
       <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont view_qsd">
                <div class="main_headding">
                <h2>View Answer</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span></span>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 
			<div class="content_main view_qsd">
				<h2><?php echo $StudentAnswerAr[0]['Question']['Examination']['title'];?></h2>
				<p><?php echo @$StudentAnswerAr[0]['Question']['Examination']['description'];?></p>
				<div class="attempt_details">
					<div class="attempt_qus">
							<ul>
									<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
									<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>

									<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>

							</ul>
					</div>
				
				</div>
			
			</div>
			
<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>							
												
		 <!-- tab start by Dinesh-->

		
		<div id="view_quistion_ans">
 <?php //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'take_exam_save'), true))); ?>

	  <?php
	  if(!empty($StudentAnswerAr)){ 
		  $i = 1;
		  $k = 1;
		  foreach($StudentAnswerAr as $qval){ //pr($qval);
				if(!empty($qval['StudentAnswer']['answer_id'])){
					if($qval['StudentAnswer']['answer_id'] == $qval['Question']['CorrectAnswer'][0]['id']){
						$qs_color = 'corrct_ans_qs';
					} else {
						$qs_color = 'wrong_ans_qs';
					}
				} else {
					$qs_color = 'notattempt_ans_qs';
				}
		    
		  
	?>
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
        	       <div class="qus <?php echo $qs_color;?>"><span class="qs_num_va"><?php echo 'Q '.$i;?>. </span> <?php echo str_replace('../../', '../../../',$qval['Question']['title']);?><?php //echo $qval['Question']['title'];?></div>  
				  
<ul class="ans_field">
 <?php 
  $j = 'a';

  foreach($qval['Question']['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
				$checked = 'checked';
			} else {
				$checked = ' ';
			}
		$answer_type = '<input type="checkbox" '.$checked.' value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
			if($qval['StudentAnswer']['answer_id'] == $ansval['id']){
				$checked = 'checked';
			} else {
				$checked = ' ';
			}
		$answer_type = '<input type="radio"  '.$checked.'  value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="'.$k.'_ans[]">';
		
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'"></textarea>';
	}
	
	//echo strlen($ansval['answer_text']);
	if(strlen(strip_tags($ansval['answer_text'])) >= 20  && strpos($ansval['answer_text'], 'src="') == false) {
		$answerclass = 'big_answer';
	} else if(strpos($ansval['answer_text'], 'src="') !== false){
		$answerclass = 'big_answer';
	} else {
		$answerclass = 'ans_options';
	}
	
	//echo strlen(strip_tags($ansval['answer_text'])) >= 20 ? 'big_answer' : 'ans_options';
  ?>	

<li class="<?php echo $answerclass;?> ">
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
		 //  echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		   // echo $answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		    echo $answer_type.'&nbsp;&nbsp;&nbsp;'.str_replace('../../', '../../../',$ansval['answer_text']);
		 }
		 ?>
		 </li>
		 <?php
		 
		 $j++;
  } 
 ?>
 </ul>
 <?php
  foreach($qval['Question']['CorrectAnswer'] as $corrans) { //pr($corrans);
	  //echo 'Correct Answer : '.$corrans['answer_text'].'</br>';
	  echo '<div class="qus_details">Correct Answer : '.str_replace('../../', '../../../',$corrans['answer_text']).'</p>';
	  
  }
  
	?> 
	
			   
        	</div>
	
	  <?php 
	  
					if($qval['Question']['answer_type']){
						$k++;
					}
				 $i++;
				}
			}
	  ?>
	   
                 
         
	  <?php //echo $this->Form->end(); ?>
        </div>

	    <!-- tab End by Dinesh-->										
												
												
												
												
												
												
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>