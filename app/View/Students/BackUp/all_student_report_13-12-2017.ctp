<style>
.individual_report_table1 table tr td{width:auto;border:2px solid #ccc;}
.row_inside1{margin-top:15px;margin-bottom:10px;border:2px solid #ccc;padding-bottom:10px;}
.row_inside1 h5{text-align:center;font-size:16px;font-weight:bold;color:#FF0000;text-decoration:underline;}
.row_inside1 table tr td{width:auto;border:none !important; font-weight:bold;color:#FF0000;text-decoration:underline;}
.row_inside1 table, .row_inside1 table tr{border:none !important;}
.final_result p{font-size:16px;font-weight:bold;color:#FF0000;text-decoration:underline;text-align:center;margin-top:65px;}
.div_non_cal, .div_cal, .fina_div{background: url("<?php echo $this->webroot;?>img/front-end/header_g.jpg") repeat left top; padding:5px 10px;margin-bottom:50px;}
.div_non_cal h2, .div_cal h2 {margin: 0;padding: 0;color: #ffffff;font-weight: bold;font-size: 16px;text-transform: uppercase;}
.fina_div table tr td{padding:15px !important;}
.fina_div1 table tr td, .fina_div2 table tr td{padding:15px !important;} 
.fina_div1 table tr td:nth-child(2), .fina_div2 table tr td:nth-child(2){width:60px;}
.table_span{background-color: #0668bd;border-radius: 2px;padding: 8px 10px;width: 100%;display: block;}

</style>
<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>

<script type="text/javascript">
	$(function() {
		var $tabs = $('.tabs').tabs();
	});
</script>

<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
	
      <div class="content"> 
	  <div class="individual_report_table1">
		<h5 style="text-align:center;font-size:20px;font-weight:bold;color:green;text-decoration:underline;margin-bottom:30px;">
		<?php echo @$LoopData[0]['SchoolPurchaseExam']['School']['name'];?>
		</h5>
		</div>
<?php 
if(!empty($LoopData)){ //pr($LoopData);die;
	foreach($LoopData as $StVal){ 
		 
		 $student_id = $StVal['MyExamStudentAnswer']['student_id'];
		 $name = ucwords($StVal['Student']['first_name'].' '.$StVal['Student']['last_name']);
		 $email = $StVal['Student']['email'];
		 $student_class= $StVal['MyExamStudentAnswer']['class_name'];
		 $student_year = $StVal['Student']['ExaminationCategory']['name'];
		 $class_rank = $StVal['MyExamStudentAnswer']['class_rank'];
		 $year_group_rank = $StVal['MyExamStudentAnswer']['year_group_rank'];
		 $score = $StVal['MyExamStudentAnswer']['score'];
		 $unique_flag = $StVal['MyExamStudentAnswer']['unique_flag'];
		 
		 
		 
		 $QuestionAnsNonCalculator = $this->requestAction(array('controller' => 'students', 'action'=> 'get_non_calculator_qans',$student_id));
		 $QuestionAnsCalculator = $this->requestAction(array('controller' => 'students', 'action'=> 'get_calculator_qans',$student_id));
		 $multiData = $this->requestAction(array('controller' => 'students', 'action'=> 'get_multiple_data',$student_id,$unique_flag,$school_purchase_exam_id,$student_class));
		 //echo $multiData;
		 list($totalAttemptQs, $correctAnswer,$totalAttemptQs1,$correctAnswer1,$totalqustionSet,$yearGroupMean,$sameClassMean,$noOfStudentInYearGroup) = explode('@@', $multiData);
			
		$d1 = ($correctAnswer - $yearGroupMean);
		$daviation1= ($d1*$d1);
		$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
		$studentZscoreByYearGrp1 = round((($correctAnswer - $yearGroupMean)/$standardDeviation),2); 
		
		
		$d2 = ($correctAnswer1 - $yearGroupMean);
		$daviation1= ($d2*$d2);
		$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
		$studentZscoreByYearGrp2 = round((($correctAnswer1 - $yearGroupMean)/$standardDeviation),2); 
		
		$correctAnswer3 = ($correctAnswer + $correctAnswer1);
		$d3 = ($correctAnswer3 - $yearGroupMean);
		$daviation1= ($d3*$d3);
		$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
		$studentZscoreByYearGrp3 = round((($correctAnswer3 - $yearGroupMean)/$standardDeviation),2); 	

  ?>
	  <div class="individual_report_table1">
		
		<table >
			<tr>
				<td>Student Name:</td>				
				<td><?php echo $name;?></td>
				<td>Student Year:</td>
				<td><?php echo $student_year;?></td>
				
			</tr>
			<tr>
				<td>Student Email:</td>
				<td><?php echo $email; ?></td>
				<td>Student Class:</td>
				<td><?php echo $student_class;?></td>
				
			</tr>
		</table>
	  </div>
	   <div class="row individual_report_row1">
		  <div class="col-md-8">
			<div class="row_inside1">
			<h5>Result Per Section</h5>
			<table>
				<tr>
					<td>Non-Calculator Section</td>
					<td>Calculator Section</td>
				</tr>
			</table>
			</div>
		  </div>
		   <div class="col-md-4 final_result">
			<p>Final Result</p>
		  </div>
	  </div>
	   <div class="row individual_report_row2">
		  <div class="col-md-4">
			
			<div class='div_non_cal'>
				<h2>Questions</h2>
				<div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAnsNonCalculator)){ //echo '<pre>';print_r($QuestionAnsNonCalculator);die;
			$i = 1;
			foreach($QuestionAnsNonCalculator as $qval){
			
			 if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				} else if(!empty($qval['StudentAnswer']['answer_text']) && $qval['StudentAnswer']['answer_id'] == 0){
					$stans = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['StudentAnswer']['answer_text'])); // remove p tag
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['Question']['CorrectAnswer'][0]['answer_text'])); // remove p tag
					
					if($stans== $corAns){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				}
				
				else {
					$qstab_color = 'unattempt_qa';
				}
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			?>
    	   </ul>
		</div>
		<div class="attampt_flag">
			 <div class="res_point"><span class="correct_answer" ></span>Correct Answer
				<div class="square">
				<div class="sq_text"><?php echo $correctAnswer;?></div>
				</div>
			  </div>
			  
			 <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer
			 <div class="square">
				<div class="sq_text_wrng"><?php echo ($totalAttemptQs - $correctAnswer);?></div>
				</div>
			 </div>
		   <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted
		   <div class="square">
				<div class="sq_text_unattmpt"><?php echo ($totalqustionSet - $totalAttemptQs);?></div>
				</div>
		   </div>
		</div>
		<div class="fina_div1">
		<div class="rp-student-info">		
				<table>
					<tbody>
						<tr>
							<td><span>Mark</span></td>
							<td><?php echo $correctAnswer;?></td>
						</tr>

						<tr>
							<td>Year group</br> mean</td>
							<td><?php echo @$yearGroupMean;?></td>
						</tr>

						<tr>
							<td>Year group</br> rank</td>
							<td><?php echo @$year_group_rank;?></td>
						</tr>
						
						<tr>
							<td>Class mean</td>
							<td><?php echo @$sameClassMean;?></td>
						</tr>
						
						<tr>
							<td>Class rank</td>
							<td><?php echo @$class_rank;?></td>
						</tr>
						
						<tr>
							<td><span>Z-score per year group</span> </td>
							<td><?php echo @$studentZscoreByYearGrp1;?></td>
						</tr>
						

					</tbody>
				</table>
			</div>
		</div>
		</div>
			
		  </div>
		  <?php if(!empty($QuestionAnsCalculator)){?>
		   <div class="col-md-4">
			<div class='div_cal'>
				<h2>QUESTIONS</h2>
				<div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAnsCalculator)){
			$i = 1;
			foreach($QuestionAnsCalculator as $qval){
			
			 if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				} else if(!empty($qval['StudentAnswer']['answer_text']) && $qval['StudentAnswer']['answer_id'] == 0){
					$stans = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['StudentAnswer']['answer_text'])); // remove p tag
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['Question']['CorrectAnswer'][0]['answer_text'])); // remove p tag
					
					if($stans== $corAns){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				}
				
				else {
					$qstab_color = 'unattempt_qa';
				}
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo ($i + 32);?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			?>
    	   </ul>
			</div>
				<div class="attampt_flag">
					 <div class="res_point"><span class="correct_answer" ></span>Correct Answer
						<div class="square">
						<div class="sq_text"><?php echo $correctAnswer1;?></div>
						</div>
					  </div>
					  
					 <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer
					 <div class="square">
						<div class="sq_text_wrng"><?php echo ($totalAttemptQs1 - $correctAnswer1);?></div>
						</div>
					 </div>
				   <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted
				   <div class="square">
						<div class="sq_text_unattmpt"><?php echo ($totalqustionSet - $totalAttemptQs1);?></div>
						</div>
				   </div>
				</div>
				<div class="fina_div2">
				<div class="rp-student-info">		
				<table>
					<tbody>
						<tr>
							<td><span>Mark</span></td>
							<td><?php echo $correctAnswer1;?></td>
						</tr>

						<tr>
							<td>Year group mean</td>
							<td><?php echo @$yearGroupMean;?></td>
						</tr>

						<tr>
							<td>Year group rank</td>
							<td><?php echo @$year_group_rank;?></td>
						</tr>
						
						<tr>
							<td>Class mean</td>
							<td><?php echo @$sameClassMean;?></td>
						</tr>
						
						<tr>
							<td>Class rank</td>
							<td><?php echo @$class_rank;?></td>
						</tr>
						
						<tr>
							<td><span>Z-score per year group</span> </td>
							<td><?php echo @$studentZscoreByYearGrp2;?></td>
						</tr>
						

					</tbody>
				</table>
			</div>
				</div>
			</div>
			
		  </div>
		  
	<?php  }?>
		  
		  
		  
		  
		  <div class="col-md-4">
			<div class="fina_div">
			
		
		
		
<div class="rp-student-info">		
<table>
	<tbody>
		<tr>
			<td><span>Mark</span></td>
			<td><?php echo $score//($correctAnswer + $correctAnswer1);?></td>
		</tr>

		<tr>
			<td>Year group mean</td>
			<td><?php echo @$yearGroupMean;?></td>
		</tr>

		<tr>
			<td>Year group rank</td>
			<td><?php echo @$year_group_rank;?></td>
		</tr>
		
		<tr>
			<td>Class mean</td>
			<td><?php echo @$sameClassMean;?></td>
		</tr>
		
		<tr>
			<td>Class rank</td>
			<td><?php echo @$class_rank;?></td>
		</tr>
		
		<tr>
			<td><span>Z-score per year group</span> </td>
			<td><?php echo $studentZscoreByYearGrp3;?></td>
		</tr>
		

	</tbody>
</table>
</div>

			</div>
		  </div>
	  </div>
	  
	  
	  
	 <?php   }}?> 
      
  </div>
</div>
</div>

<style>
.square {
    background:#fff;
    width: 35px;
    height: 25px;
	margin: -19px 0px 0px 132px
}
.sq_text {
    padding: 4px 0px 0px 9px;
   color:green!important;
}
.sq_text_wrng {
   padding: 4px 0px 0px 9px;
   color:red!important;
}

.sq_text_unattmpt {
   padding: 4px 0px 0px 9px;
   color:orange!important;
}

.rp-student-info table, tr, td{
	width:92%;
	border:2px solid #a4b0db !important;
	margin-left:10px;
}
.rp-student-info table tr td {
    padding: 23px;
	font-size:16px;
	
    color: #fff !important;
}
.right_bar_combined {
	min-height: 980px !important;
}
</style>