<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3> <?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                 <!-- <span>year 9</span>-->
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('Student'); 
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >First Name :</label>
						<?php echo $this->Form->input('first_name', array('label' => false, 'required' => 'required', 'value' => @$studentDetails['Student']['first_name'], 'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Class :</label>
						
						  <?php echo $this->Form->input('student_class_id', array('label' => false, 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $classOptions))?>
			
                  
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Exam Type :</label>
                       <?php echo $this->Form->input('examination_type_id', array('label' => false,'required' => 'required', 'div' => false,  'value' => @$studentDetails['Student']['examination_type_id'],  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $examination_types))?>
					
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Remaining days :</label>
                        <input type="text" class="form-control inp_text " placeholder="10 Days">
                        <div class="clear_fix"></div>
                      </div>
                    </div>
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Last Name :</label>
						<?php echo $this->Form->input('last_name', array('label' => false,'required' => 'required', 'value' => @$studentDetails['Student']['last_name'], 'class' => 'form-control inp_text'))?>

                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >School  :</label>
							<?php echo $this->Form->input('school_name', array('label' => false,'required' => 'required', 'value' => @$studentDetails['Student']['school_name'], 'class' => 'form-control inp_text'))?>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Exam Category :</label>
						<?php echo $this->Form->input('examination_category_id', array('label' => false,'required' => 'required', 'div' => false,  'value' => @$studentDetails['Student']['examination_category_id'], 'class' => 'selectpicker show-tick form-control inp_text', 'options' => $examination_categories))?>
					
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >History :</label>
						<?php echo $this->Form->input('history', array('type' => 'textarea','required' => 'required','label' => false, 'value' => @$studentDetails['Student']['history'], 'class' => 'form-control inp_text', 'placeholder' => 'Type about you...'))?>
					  <div class="clear_fix"></div>
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn adj_btn" value="Submit">
                      </div>
                    </div>
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>