<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3>Anthony Fernando</h3>
                  <span>year 9</span>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
    <th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Price</th>
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  foreach($allExamAr as $val){
		  ?>
		  <tr>
    <td><?php echo $val['Examination']['title'];?></td>
    <td><?php echo $val['ExaminationType']['name'];?></td>
    <td><?php echo $val['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['Examination']['price'];?></td>
	  <td>
	  <?php
	  if(!empty($val['MyExam']['payment_status']) && $val['MyExam']['payment_status'] == 1){ 
	  ?>
	  <a href="<?php echo $this->webroot.'students/take_exam/'.$val['Examination']['id'];?>">Purchased</a>
	  <?php } else {?>
	
	    <?php
        if(!empty($paypalFormSettings)){
			$paypalFormSettings['amount'] = $val['Examination']['price'];
			$paypalFormSettings['custom'] = $studentDetails['Student']['id'].'##'.$val['Examination']['id'];
			
			$paypalFormSettings['item_name'] = $val['Examination']['title'].'('.$val['ExaminationType']['name'].'/'.$val['ExaminationCategory']['name'].')';
            $formAction = $paypalFormSettings['payment_url'];
            unset($paypalFormSettings['payment_url']);
            
            echo $this->Form->create(null, array('url' => $formAction, 'id' => 'payPalPaymentForm'));
            
            foreach ($paypalFormSettings as $key => $value){
                echo '<input type="hidden" name="'.$key.'" value="'.$value.'" >';
            }
            echo $this->Form->end('Pay Now');
        }
        ?>

	  
	  
	  
	  
	  <?php }?>
	  </td>
  </tr>
  <tr>
<?php
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
   <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>