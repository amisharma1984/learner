<style>
.regi_main{
	margin-left:1.5%;
}
.rigis_right {
	width:48.9% !important;
}

.rgerror {
    color: red;
    font-size: 13px;
    margin-left: 44.5%;
}

</style>  

  <div class="container">
      <div class="inner_field regi_main">
        <div class="row">
		
			<div class="contact_head">
					<h2>
						<span>Student Registration</span>
						<strong>Form</strong>
					</h2>
				
			</div>
          <?php echo $this->Session->flash();?>
            <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('Student'); 
				 ?>
                  <div class="row">
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >First Name :</label>
						<?php echo $this->Form->input('first_name', array('label' => false, 'required' => '',  'placeholder' => 'Enter first name', 'class' => 'form-control inp_text'))?>
                          <span  class="rgerror"><?php echo @$fnerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					  
					  
                      <div class="form-group">
                        <label >Class :</label>
                        <select class="selectpicker show-tick form-control inp_text">
                          <option>Xii  A(2016-2017)</option>
                          <option>Xii  A(2015-2016)</option>
                        </select>
						  <span  class="rgerror"><?php //echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					   
					   
                      <div class="form-group">
                        <label >Exam Type :</label>
                       <?php echo $this->Form->input('examination_type_id', array('label' => false, 'div' => false,  'value' => @$studentDetails['Student']['examination_type_id'],  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$examination_types))?>
					  <span  class="rgerror"><?php //echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					   
                     
					 <div class="form-group">
                        <label >Address :</label>
                     	<?php echo $this->Form->input('address', array('type' => 'text','label' => false, 'required' => '', 'class' => 'form-control inp_text', 'placeholder' => 'Enter address'))?>
                       <span  class="rgerror"><?php echo @$adderror;?> </span>
					   <div class="clear_fix"></div>
                      </div>
					  
					   
					  
					  <div class="form-group">
                        <label >Password :</label>
                     	<?php echo $this->Form->input('password', array('type' => 'text','required' => '',  'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Enter 6 to 20 characters'))?>
                        <span  class="rgerror"><?php echo @$pwderror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					  
					   <div class="form-group">
					
                        <label >Type the captcha text here :</label>
                     	    <?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','type'=>'text', 'value' => @$captcha_txt, 'id'=>'captcha_txt'));?>
						<span  class="rgerror"><?php echo @$captchaErr;?> </span>
						<div class="clear_fix"></div>
                      </div>
					   
					  
                    </div>
                    <div class="col-md-6 form_info rigis_right">
                      <div class="form-group">
                        <label >Last Name :</label>
						<?php echo $this->Form->input('last_name', array('label' => false, 'required' => '',  'placeholder' => 'Enter last name',  'class' => 'form-control inp_text'))?>
						<span  class="rgerror"><?php echo @$lnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					   
                      <div class="form-group">
                        <label >School  :</label>
							<?php echo $this->Form->input('school_name', array('label' => false, 'required' => '',  'placeholder' => 'Enter school name', 'class' => 'form-control inp_text'))?>
                        <span  class="rgerror"><?php echo @$scerror;?> </span>
						<div class="clear_fix"></div>
                      </div>
					   
					  
                      <div class="form-group">
                        <label >Exam Category :</label>
						<?php echo $this->Form->input('examination_category_id', array('label' => false, 'div' => false,  'value' => @$studentDetails['Student']['examination_category_id'], 'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$examination_categories))?>
					  <span  class="rgerror"><?php //echo @$fnerror;?> </span>
                        <div class="clear_fix"></div>
                      </div>
					   
					  
                      <div class="form-group">
                        <label >Email :</label>
							<?php echo $this->Form->input('email', array('type' => 'text', 'required' => '', 'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Enter email'))?>
						<span  class="rgerror"><?php echo @$emerror;?> </span>
					 <div class="clear_fix"></div>
                      </div>
					   
					  
					   <div class="form-group">
                        <label >Confirm Password :</label>
								<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'required' => '', 'pattern' => '.{6,20}','label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Enter 6 to 20 characters'))?>
					 <span  class="rgerror"><?php echo @$cpwderror;?> </span>
					 <div class="clear_fix"></div>
                      </div>
					   
					   <div class="form-group">
                        <label >
						<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
						<a href="javascript:void(0);"
						onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
						<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
						</label>
					 <div class="clear_fix"></div>
                      </div>
					  
					  
					    <div class="form-group">
                        <label >&nbsp;</label>
								 <div class="col-md-4">
                                     <input type="submit" class="btn btn-info def_btn" value="Submit">
                                 </div>
					  <div class="clear_fix"></div>
                      </div>
					   
					  
					  
                    </div>
                  </div>
                   
                   <?php echo $this->Form->end(); ?>
                </div>
				
       
		  
		  
        </div>
      </div>
    </div>