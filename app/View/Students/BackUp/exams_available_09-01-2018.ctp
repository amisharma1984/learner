<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Exams Available</h2>
                </div>
				
 <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'checkout'), true))); ?>
<div class="prof_details prof_form">
<table id="YourTableId" class="student-list-apr">
  <tr>
  <th>List</th>
   <th>Year</th>
    <th>Exam Category</th>
    <th>Exam Name</th>
   
	 <th style="text-align:center;">Price($ AUD)</th>
    <th>Click to purchase Exams <!--&nbsp;<input type="button" class="check chk-unchk-btn" value="Check All" />-->
	</th>
   
	
  </tr>
  
  <?php
  if(!empty($allExamAr)){ 
	  $j = 1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		  ?>
		  <tr>
	<td><?php echo $j;?></td>
	 <td><?php echo $category[1]; ?></td>
    <td>NAPLAN Style Practice</td>
    <td><?php echo $val['Examination'][0]['title']; ?></td>
	<td style="text-align:center;">
		<?php 
		 foreach($val['Examination'] as $vp){ 
		  if($vp['id'] != 20 && $vp['id'] != 21 && $vp['id'] != 22){
			echo $vp['price'];
			echo '<br>';
		 }
		 }
	    ?>
	</td>
    
	<td style="text-align:center;">
	<?php 
		$i = 1;
		 foreach($val['Examination'] as $v){
				if($v['id'] != 20 && $v['id'] != 21 && $v['id'] != 22){
		  $isMyexamPurchased = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', $v['id']));
		if($isMyexamPurchased > 0){
	?>
	<span> <input type="checkbox" disabled="disabled">
	  &nbsp;&nbsp;&nbsp;<?php echo $v['paper'];?> (Already purchased)</span>
	<?php } else {?>
	<input type="checkbox" class="case" value="<?php echo $v['id'];?>" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo $v['paper'];?>
	
	<!--<a href="<?php //echo $this->webroot.'students/checkout_new/'.$v['id'];?>"> <?php echo $v['paper'];?></a>-->
	
	<?php
		 }
		 echo '<br>';
		 $i++;
		}
	 }
	?>
	
	</td>
	
	
	
	
	 
	<?php /*?> <td><?php echo @$val['Examination']['price'];?></td>
	  <td>
	  <?php
	  if(!empty($val['MyExam']['payment_status']) && $val['MyExam']['payment_status'] == 1){ 
	  ?>
	  <a href="<?php echo $this->webroot.'students/take_exam/'.@$val['Examination']['id'];?>">Purchased</a>
	  <?php } else {?>
	 <a href="<?php echo $this->webroot.'students/make_payment/'.@$val['Examination']['id'];?>">Make Payment</a>
	  <?php }?>
	  </td><?php */?>
	  
  </tr>
<?php
	$j++;
	  }
  }
?>

<tr>
	<td>5</td>
	 <td>9</td>
    <td>NAPLAN Style Practice</td>
    <td>Literacy Practice Exam</td>
	<td style="text-align:center;">
		2.00<br>
		2.00<br>
		2.00
			
	</td>
    
	<td style="text-align:center;">
	<?php
  $isMyexamPurchased1 = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', 20));
  $isMyexamPurchased2 = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', 21));
  $isMyexamPurchased3 = $this->requestAction(array('controller' => 'students', 'action' => 'is_exist_in_myexam', 22));
		
	?>
	
	<?php if($isMyexamPurchased1 > 0){?>
	<span> <input type="checkbox" disabled="disabled">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 1';?> (Already purchased)</span>
	<?php } else {?>
	<input type="checkbox" class="case" value="20" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 1';?>
	<?php
		 }
	?> 
	</br>
	
	<?php if($isMyexamPurchased2 > 0){?>
	<span> <input type="checkbox" disabled="disabled">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 2';?> (Already purchased)</span>
	<?php } else {?>
	<input type="checkbox" class="case" value="21" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 2';?>
	<?php
		 }
	?> 
	</br>
	
	<?php if($isMyexamPurchased3 > 0){?>
	<span> <input type="checkbox" disabled="disabled">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 3';?> (Already purchased)</span>
	<?php } else {?>
	<input type="checkbox" class="case" value="22" name="data[Examination][ids][]">
	  &nbsp;&nbsp;&nbsp;<?php echo 'Test 3';?>
	<?php
		 }
	?> 
	</td>	  
  </tr>


</table>
				 
				 
                </div>
				
					<input type="hidden" value="checkout" name="page_name">
					<div class="clear_fix"></div>
					<div class="exam_chkout">
					<input type="submit" id="YourbuttonId" class="btn btn-info def_btn" value="Checkout">

					<?php echo $this->Form->end(); ?>
					</div>
				
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<style>
table td a {
    color: #A0760A;
    font-weight: normal;
    text-decoration: none;
}
table td span {
    color: red;
    font-weight: normal;
    text-decoration: none;
}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
	<SCRIPT language="javascript">
	$(document).ready(function(){
		$("#YourbuttonId").click(function(){
				if($('#YourTableId').find('input[type=checkbox]:checked').length == 0)
				{
					alert('Please select atleast one checkbox');
					return false;
				}
		});
		
			  $('.check:button').click(function(){
					  var checked = !$(this).data('checked');
					  $('input:checkbox').prop('checked', checked);
					  $(this).val(checked ? 'Uncheck All' : 'Check All' )
					  $(this).data('checked', checked);
			});
		
		
	
	});
	

</SCRIPT>
