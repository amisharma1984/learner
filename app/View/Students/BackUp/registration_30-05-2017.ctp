<div class="container">
	<div class="inner_field regi_main">
		<div class="row1">
			
			<!-- <div class="contact_head">
				<h2>
				<span>School Registration</span>
				<strong>Form</strong>
				</h2>
				
			</div> -->
			<?php echo $this->Session->flash();?>
			<div class="prof_details prof_form">
			
				<div class="row">
					<?php /* ?><div class="col-md-4">
						<div class="form_info">
							 <?php
							 //echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'login'), true), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'studentLoginForm')); 
							  echo $this->Form->create('StudentLogin'); 
							  echo $this->Form->input('StudentLogin.login_registration_flag', array('type' => 'hidden', 'value' => 'student_login'));
							 ?>
                       
							<h2>Sign in your account</h2>
							<div class="form-group">
								<i class="fa fa-user" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentLogin.login', array('label' => false,  'placeholder' => 'Username', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginEmailErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="form-group">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<?php echo $this->Form->input('StudentLogin.password', array('label' => false,  'placeholder' => 'Password ', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror"><?php echo @$loginPassErr;?> </span>
								<div class="clear_fix"></div>
							</div>
							
							<div class="checkbox form-group">
								<label><?php echo $this->Form->input('StudentLogin.remember', array('type' => 'checkbox', 'label' => false, 'div' => false, 'id' => 'remember')); ?> Remember me</label>
								<a href="" class="formbtn forget"><!-- Forget Password? --></a><br>
							</div>
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Login</button>
							</div>
							<div class="form-group adjpad">
								Don’t have an account? <a href="" > Sign Up!</a>
								
							</div>
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div><?php */ ?>
					<div class="col-md-12">
       <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				 echo $this->Form->create('Student'); 
				 ?>


						<div class="row">
							<div class="col-md-offset-1 col-md-10">
								<div class="form_info">
									<h2>Sign up for student account</h2>
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-graduation-cap" aria-hidden="true"></i>
													<?php echo $this->Form->input('first_name', array('label' => false, 'required' => '',  'placeholder' => 'First name', 'class' => 'form-control inp_text'))?>
                                                    <span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-graduation-cap" aria-hidden="true"></i>
													<?php echo $this->Form->input('last_name', array('label' => false, 'required' => '',  'placeholder' => 'Last name',  'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$lnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											  <?php echo $this->Form->input('student_class_id', array('label' => false, 'div' => false, 'empty' => '- Class -',  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $classOptions))?>
				
												<?php //echo $this->Form->input('class', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Class -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('school_name', array('label' => false, 'required' => '',  'placeholder' => 'School name', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
											<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												 <?php 
												 $exam_type = array('Numeracy' => 'Numeracy', 'Naplan Practice' => 'Naplan Practice');
												// echo $this->Form->input('examination_type_id', array('label' => false, 'div' => false,'empty' => 'Exam type',  'value' => @$studentDetails['Student']['examination_type_id'],  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$examination_types));
												 echo $this->Form->input('exam_type', array('label' => false, 'div' => false, 'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$exam_type));
												 ?>
					 
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<?php 
											$exam_categories = array(
																	'Year 3 Numeracy' => 'Year 3 Numeracy', 
																	'Year 5 Numeracy' => 'Year 5 Numeracy', 
																	'Year 7 Numeracy' => 'Year 7 Numeracy', 
																	'Year 9 Numeracy' => 'Year 9 Numeracy'
																	);
											//echo $this->Form->input('examination_category_id', array('label' => false, 'div' => false, 'empty' => 'Exam category',  'value' => @$studentDetails['Student']['examination_category_id'], 'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$examination_categories));
											echo $this->Form->input('exam_category', array('label' => false, 'div' => false, 'empty' => 'Exam category','class' => 'selectpicker show-tick form-control inp_text', 'options' => $exam_categories));
											?>
											<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									
									
									
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('address', array('type' => 'text','label' => false, 'required' => '', 'class' => 'form-control inp_text', 'placeholder' => 'Address'))?>
                                              <span  class="rgerror"><?php echo @$adderror;?> </span>
												
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-envelope-o" aria-hidden="true"></i>
												<?php echo $this->Form->input('email', array('label' => false, 'type' => 'text','required' => '' ,  'placeholder' => 'Email', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$emerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									
											
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-lock" aria-hidden="true"></i>
											<?php echo $this->Form->input('password', array('type' => 'text','required' => '',  'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Password 6 to 20 characters'))?>
                                               <span  class="rgerror"><?php echo @$pwderror;?> </span>
												
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-lock" aria-hidden="true"></i>
												<?php echo $this->Form->input('confirm_password', array('type' => 'password', 'required' => '', 'label' => false,'class' => 'form-control inp_text', 'placeholder' => 'Confirm password'))?>
					                             <span  class="rgerror"><?php echo @$cpwderror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									
									
								
								<?php /*?>
								<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.level', array('label' => false, 'type' => 'select', 'div' => false,  'empty' => '- Select Level -',     'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$schoolLevelOptions))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('SchoolInformation.type', array('label' => false, 'type' => 'select','emty'=>'Type', 'div' => false,  'class' => 'selectpicker show-tick form-control inp_text', 'options' => @$types))?>
												<span  class="rgerror"><?php //echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<i class="fa fa-street-view" aria-hidden="true"></i>
												<?php echo $this->Form->input('SchoolInformation.street', array('label' => false,'type' => 'text', 'required' => 'required' ,  'placeholder' => 'Street', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-location-arrow" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.town', array('label' => false, 'required' => 'required' , 'type' => 'text',  'placeholder' => 'City / Town ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>


												<?php echo $this->Form->input('SchoolInformation.district', array('label' => false, 'required' => 'required' ,  'placeholder' => 'District', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-map-signs" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.state', array('label' => false, 'required' => 'required' ,  'placeholder' => 'State / Province ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-codepen" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.zip', array('label' => false, 'required' => 'required' ,  'placeholder' => ' Zip Code ', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-phone" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.phone', array('label' => false, 'required' => 'required' ,  'placeholder' => 'Phone', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-fax" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.fax', array('label' => false, 'required' => 'required' ,  'placeholder' => 'Fax', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$fnerror;?> </span>
												<div class="clear_fix"></div>
											</div>
											
										</div>
										<div class="col-md-6">
											<div class="form-group">
											<i class="fa fa-link" aria-hidden="true"></i>

												<?php echo $this->Form->input('SchoolInformation.url', array('label' => false, 'required' => 'required' ,  'placeholder' => 'URL', 'class' => 'form-control inp_text'))?>
												<span  class="rgerror"><?php echo @$scerror;?> </span>
												<div class="clear_fix"></div>
											</div>
										</div>
									</div><?php */?>
									
									
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
											
											<div class="row">
												<div class="col-sm-7">

												<?php echo $this->Form->input('captcha_txt', array('label' => false,'class'=>'form-control inp_text','placeholder' => 'Type the captcha text here','type'=>'text', 'value' => @$captcha_txt, 'id'=>'captcha_txt'));?>
												<span  class="rgerror"><?php echo @$captchaErr;?> </span>
												<div class="clear_fix"></div>
												</div>
												<div class="col-sm-5 led_wid">
													<label >
															<img id="captcha" src="<?php echo $this->webroot.'students/captcha_image';?>" alt="" />
															<a href="javascript:void(0);"
																onclick="javascript:document.images.captcha.src='<?php echo $this->webroot.'students/captcha_image'?>?' + Math.round(Math.random(0)*1000)+1" align="left" >
															<img src="<?PHP echo $this->webroot;?>images/refresh.png"/></a>
														</label>
													</div>
										</div>
											
										
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<?php //echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); ?><!--Is previous year customer? -->
												
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
									<div class="row">
										<div class="col-md-6">
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<input type="submit" class="btn btn-info def_btn" value="Submit">
												<div class="clear_fix"></div>
											</div>
										</div>
									</div>
									
									
								</div>
							</div>
						</div>

						<?php echo $this->Form->end(); ?>
					</div>
					
				</div>
				

			</div>
			
		</div>
		
		
		
		
	</div>
</div>
</div>