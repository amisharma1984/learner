<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
       <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3><?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span>year 9</span>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
    <th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Date</th>
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($myExamAr)){
	  foreach($myExamAr as $val){
		$random_id = $val['MyExamStudentAnswer']['student_ans_random_id'];
		 if(!empty($val['MyExam']['Examination']['title'])){
		 ?>
		  
		  <tr>
    <td><?php echo $val['MyExam']['Examination']['title'];?></td>
    <td><?php echo $val['MyExam']['Examination']['ExaminationType']['name'];?></td>
    <td><?php echo $val['MyExam']['Examination']['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['MyExamStudentAnswer']['created'];?></td>
	  <td>
	  <a href="<?php echo $this->webroot.'students/view_answer/'.$random_id;?>">View Result</a></td>
  </tr>
  
<?php
		 }
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>