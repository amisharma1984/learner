<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
		<?php echo $this->element('dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3><?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span>year 9</span>
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table>
  <tr>
    <th>Exam Name</th>
    <th>Type</th>
    <th>Category</th>
	<th>Price</th>
	<th>Action</th>
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  foreach($allExamAr as $val){
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
    <td><?php echo $val['Examination']['title'];?></td>
    <td><?php echo $val['ExaminationType']['name'];?></td>
    <td><?php echo $val['ExaminationCategory']['name'];?></td>
	 <td><?php echo $val['Examination']['price'];?></td>
	  <td>
	  <?php
	  if(!empty($val['MyExam']['payment_status']) && $val['MyExam']['payment_status'] == 1){ 
	  ?>
	  <a href="javascript:void(0);">Purchased</a>
	  <?php } else {?>
	 <a href="<?php echo $this->webroot.'students/make_payment/'.$ExaminationId;?>">Make Payment</a>
	  <?php }?>
	  </td>
  </tr>
<?php
	  }
  }
?>
</table>
				 
				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
