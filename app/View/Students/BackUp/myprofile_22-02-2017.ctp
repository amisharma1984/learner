<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3> <?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  <span>year 9</span>
                </div>
                <div class="prof_details prof_form">
                  <form action="" method="post">
                  <div class="row">
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Name :</label>
                        <input type="text" class="form-control inp_text " placeholder="Anthony">
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Class :</label>
                        <select class="selectpicker show-tick form-control inp_text">
                          <option>Xii  A(2016-2017)</option>
                          <option>Xii  A(2015-2016)</option>
                        </select>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Exam Type :</label>
                        <select class="selectpicker show-tick form-control inp_text">
                          <option>Napla Exam</option>
                          <option>Napla Exam2</option>
                        </select>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Remaining days :</label>
                        <input type="text" class="form-control inp_text " placeholder="10 Days">
                        <div class="clear_fix"></div>
                      </div>
                    </div>
                    <div class="col-md-6 form_info">
                      <div class="form-group">
                        <label >Last Name :</label>
                        <input type="text" class="form-control inp_text " placeholder="Fernando">
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >school  :</label>
                        <input type="text" class="form-control inp_text " placeholder="St. John’s Diocesan School">
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >Exam Category :</label>
                        <select class="selectpicker show-tick form-control inp_text">
                          <option selected>Outside Experience</option>
                          <option>Outside Experience 2</option>
                        </select>
                        <div class="clear_fix"></div>
                      </div>
                      <div class="form-group">
                        <label >History :</label>
                        <textarea class="form-control inp_text" placeholder="Type about you..."></textarea>
                        <div class="clear_fix"></div>
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="submit" class="btn btn-info def_btn adj_btn" value="Submit Button">
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>