<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>
    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE & NEXT &#187;</a>");
			  } else {
				    $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function save_next(){
			
			
				
			  var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
					$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
				 /*var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
										$('#attempted_qs').text(numberOfCheckedRadio);
										if(percentageBar == 100){
											 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
										} else {
											  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
										}
				                       
										
									   
										$('#qs_attempted_prcessing').css({"width": ""});
										$('#qs_attempted_prcessing').css({"width": percentageBar+"%"}); */
				 
			});	
		});
		
    </script>


<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
       <div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){ 
			?>
			<li><a class="<?php echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php $i++;}}?>
    	   </ul>
    </div>
	
  <!--<div class="attamp_check">
          <label>Attempted</label>
        <label>Not attempted</label>
		
        </div>
        <div class="view_all_qu">
        <a href="javascript:void(0);">View All Questions</a>
        </div>-->
</nav>




<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
      <div class="content_main">
	   <h2><?php echo $ExamDetails['Examination']['title'];?></h2>
	   <p><?php echo @$ExamDetails['Examination']['description'];?></p>
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="qusdetails" class="tabs">
 <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'take_exam_save'), true))); ?>
 <input type="hidden" name="my_exam_id" value="<?php echo $my_exam_id;?>">
	  <?php
	  if(!empty($QuestionAns)){ 
		  $i = 1;
		  $k = 1;
		  foreach($QuestionAns as $qval){ 
	?>
	<input type="hidden" name="questionids[]" value="<?php echo $qval['Question']['id']?>">
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
        	       <div class="qus"><span class="qs_num"><?php echo 'Q '.$i;?>. </span> <?php echo str_replace('../../', '../../../',$qval['Question']['title']);?><?php //echo $qval['Question']['title'];?></div>  
				  
<ul class="ans_field">
 <?php 
  $j = 'a';

  foreach($qval['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio"  value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="'.$k.'_ans[]">';
		
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'"></textarea>';
	}
	
	//echo strlen($ansval['answer_text']);
	if(strlen(strip_tags($ansval['answer_text'])) >= 20  && strpos($ansval['answer_text'], 'src="') == false) {
		$answerclass = 'big_answer';
	} else if(strpos($ansval['answer_text'], 'src="') !== false){
		$answerclass = 'big_answer';
	} else {
		$answerclass = 'ans_options';
	}
	
	//echo strlen(strip_tags($ansval['answer_text'])) >= 20 ? 'big_answer' : 'ans_options';
  ?>	

<li class="<?php echo $answerclass;?> ">
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
		 //  echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		   // echo $answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		    echo $answer_type.'&nbsp;&nbsp;&nbsp;'.str_replace('../../', '../../../',$ansval['answer_text']);
		 }
		 ?>
		 </li>
		 <?php
		 
		 $j++;
  } 
	?> 
</ul>	
			   
        	</div>
	
	  <?php 
	  
					if($qval['Question']['answer_type']){
						$k++;
					}
				 $i++;
				}
			}
	  ?>
	   
                    <button type="submit" class="qs_save_btn"> SUBMIT TEST</button>
         
	  <?php echo $this->Form->end(); ?>
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>