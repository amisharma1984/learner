<div class="container">
	<div class="inner_field regi_main">
			<?php echo $this->Session->flash();?>
			<div class="prof_details online_exm_lg">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="form_info">
							 <?php
							 
							  echo $this->Form->create('Student', array('onsubmit' => 'return validation()','url'=>'result')); 
							 ?>
							
							 
							<h2>Review Exam Results </h2>
							<div class="form_cont">
												
							
						
							
							<div class="form-group">
								
								<?php echo $this->Form->input('school_email', array('label' => false, 'id'=>'school_email', 'autocomplete' => 'off', 'placeholder' => 'Student School Email', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="scerror"></span>
								<div class="clear_fix"></div>
							</div>
							<div class="form-group">
								
								<?php echo $this->Form->input('review_exam_code', array('label' => false, 'id'=>'review_exam_code', 'autocomplete' => 'off', 'placeholder' => 'Review Exam Results Code', 'class' => 'form-control inp_text'))?>
								<span  class="rgerror" id="scerror1"></span>
								<div class="clear_fix"></div>
							</div>							
				
							<div class=" form-group">
								<button type="submit" class="btn btn-info def_btn">Enter</button>
							</div>
							</div>
						
							
							<?php echo $this->Form->end(); ?>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>

<script>
function validation(){ 
	var result=true;
	var email_id 			= $.trim($('#school_email').val());
	var review_exam_code 	= $.trim($('#review_exam_code').val());
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	
	if($.trim($('#school_email').val()) == ""){
		 $('#scerror').html('Please enter your email id');
		 result=false;
	} else if(!pattern.test(email_id)){
		 $('#scerror').html('Please enter valid email format');
		 result=false;
	}else if(email_id != ""){
		$.ajax({
				url : '<?php echo $this->webroot;?>students/check_valid_student',
				type : 'POST',
				async: false,
				data : {email_id : email_id},
				success : function(responce){
					console.log(responce);
					if(responce == 'UNVALLID'){
						$('#scerror').html('Given email address is not registered in our system or you are already review your exam.');
						result=false;
					} else {
						$('#scerror').html('');
					}
				}
		});
		
	}else{
		$('#scerror').html('');
	}
	if($.trim($('#review_exam_code').val()) == ""){
		 $('#scerror1').html('Please enter review exam code');
		 result=false;
	}else if(review_exam_code !=""){
		$.ajax({
			url : '<?php echo $this->webroot;?>students/check_review_exam_code',
			type : 'POST',
			async: false,
			data : {review_exam_code : review_exam_code},
			success : function(responce){
				console.log(responce);
				if(responce == 'UNVALLID'){
					$('#scerror1').html('Invalid review exam code.');
					result=false;
				} else {
					$('#scerror1').html('');
				}
			}
		});
	
	}else{
		$('#scerror1').html('');
	}	
	
	if(email_id != '' && review_exam_code !=""){
		$.ajax({
			url : '<?php echo $this->webroot;?>students/check_exam_given',
			type : 'POST',
			async: false,
			data : {email_id : email_id,review_exam_code : review_exam_code},
			success : function(responce){ //alert(responce);
				if(responce =='UNVALLID'){
					$('#scerror1').html('You have not given the exam.');
					result=false;
				}else {
					$('#scerror1').html('');
				}
			}
			
		});
		
	} else {
		 $('#scerror1').html('');
	}
	
	return result;
	
 }
</script>
