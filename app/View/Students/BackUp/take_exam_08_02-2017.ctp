<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>
    <script type="text/javascript">
		$(function() {
		
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' class='next-tab mover nexts ' rel='" + next + "'>SAVE & NEXT &#187;</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       
//$( ".mover, .qs_save_btn " ).wrapAll( "<div class="dashboard-footer"> </div>" );
		});
    </script>
<style>
	.ui-tabs-panel {
		min-width:700px !important;
	}

</style>

<nav class="navbar navbar-inverse easy-sidebar">
    <div class="navbar-header"> </div>
    <div class="question_left">
       <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
       <div id="tabs__" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){ 
			?>
			<li><a class="<?php echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php $i++;}}?>
    	   </ul>
    </div>
	
  <!--<div class="attamp_check">
          <label>Attempted</label>
        <label>Not attempted</label>
		
        </div>
        <div class="view_all_qu">
        <a href="javascript:void(0);">View All Questions</a>
        </div>-->
</nav>




<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
      <div class="content_main">
	  
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="tabs__" class="tabs">
		
    		<ul style="display:none;">
			<?php
			if(!empty($QuestionAns)){
			$i = 1;
			foreach($QuestionAns as $qval){ 
			?>
			<li><a href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php $i++;}}?>
    	   </ul>
		   
		      <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'take_exam_save'), true))); ?>
	  <?php
  if(!empty($QuestionAns)){
	  $i = 1;
	  foreach($QuestionAns as $qval){ 
?>
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
        	       <p><?php echo 'Q '.$i .' . '.$qval['Question']['title'];?></p>  
				  

 <?php 
  $j = 'a';
  foreach($qval['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio"  value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'"></textarea>';
	}
  ?>	
<p>
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
		   echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		 }
		 $j++;
  } 
	?>  
	</p>			   
        	</div>
	
	  <?php 
				 $i++;
				}
			}
	  ?>
	   
                    <button type="submit" class="qs_save_btn"> SUBMIT TEST</button>
         
	  <?php echo $this->Form->end(); ?>
        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
     
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>