<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>
    <script type="text/javascript">
		$(function() {

			var $tabs = $('#tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' class='next-tab mover save_next' rel='" + next + "'>SAVE & NEXT &#187;</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover prev_tab' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
    </script>
<style>
	.ui-tabs-panel {
		min-width:700px !important;
	}

</style>
<div class="container-fluid">

    <div class="col-lg-9 col-md-7 col-sm-12">
      <div class="content"> 
      <div class="content_main">
	  
	  <!-- tab start by Dinesh-->
	  <div id="page-wrap">
		
		<div id="tabs">
		
    		<ul style="display:none;">
        		<li><a href="#fragment-1">1</a></li>
        		<li><a href="#fragment-2">2</a></li>
        		<li><a href="#fragment-3">3</a></li>
        		<li><a href="#fragment-4">4</a></li>
        		<li><a href="#fragment-5">5</a></li>
        		<li><a href="#fragment-6">6</a></li>
        		<li><a href="#fragment-7">7</a></li>
        		<li><a href="#fragment-8">8</a></li>
        		<li><a href="#fragment-9">9</a></li>
        		<li><a href="#fragment-10">10</a></li>
        		<li><a href="#fragment-11">11</a></li>
        		<li><a href="#fragment-12">12</a></li>
        		<li><a href="#fragment-13">13</a></li>
        		<li><a href="#fragment-14">14</a></li>
        		<li><a href="#fragment-15">15</a></li>
    	   </ul>
		   
		      <?php echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'take_exam_save'), true))); ?>
	  <?php
  if(!empty($QuestionAns)){
	  $i = 1;
	  foreach($QuestionAns as $qval){ 
?>
	   <div id="fragment-<?php echo $i;?>" class="<?php ($i == 1) ? 'ui-tabs-panel tbminwd' : 'ui-tabs-panel ui-tabs-hide tbminwd' ?>">
        	       <p><?php echo 'Q '.$i .' . '.$qval['Question']['title'];?></p>  
				  

 <?php 
  $j = 'a';
  foreach($qval['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio"  value="'.$qval['Question']['id'].'Q#'.$ansval['id'].'" name="ans[]">';
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'"></textarea>';
	}
  ?>	
<p>
	<?php 
		 if($ansval['answer_type'] == 4){
			 echo $answer_type;
		 } else {
		   echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.$ansval['answer_text'];
		 }
		 $j++;
  } 
	?>  
	</p>			   
        	</div>
	
	  <?php 
				 $i++;
				}
			}
	  ?>
	    <p style="width:100%">
                    <button type="submit" class="btn btn-primary" style="float:right;margin-top:20px;"> SUBMIT TEST</button>
                </p>
	  <?php echo $this->Form->end(); ?>
        	<!--<div id="fragment-1" class="ui-tabs-panel">
        	       <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>  
        	</div>
        	
        	<div id="fragment-2" class="ui-tabs-panel ui-tabs-hide">
                    <p>Donec ultricies senectus tristique egestas vitae, et ac morbi habitant quam sit mi quam, malesuada leo. Vestibulum tempor Mauris tortor libero eget, egestas. eu vitae feugiat netus amet Pellentesque ante. amet, ultricies eleifend turpis sit placerat et semper. Aenean est. fames </p>
            </div>
            
        	<div id="fragment-3" class="ui-tabs-panel ui-tabs-hide">
            		<p>ante. Mauris Vestibulum est. fames egestas quam, leo. amet tristique sit libero egestas. ultricies mi turpis senectus Pellentesque habitant eu ac morbi netus eget, Aenean malesuada vitae, semper. eleifend et feugiat vitae amet, placerat Donec et tortor ultricies tempor quam sit </p>
            </div>
        
        	<div id="fragment-4" class="ui-tabs-panel ui-tabs-hide">

        	</div>
        	
        	<div id="fragment-5" class="ui-tabs-panel ui-tabs-hide">
                
        	</div>
        
        	<div id="fragment-6" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-7" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-8" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-9" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-10" class="ui-tabs-panel ui-tabs-hide">

        	</div>
        	
        	<div id="fragment-11" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-12" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-13" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-14" class="ui-tabs-panel ui-tabs-hide">
        
        	</div>
        	
        	<div id="fragment-15" class="ui-tabs-panel ui-tabs-hide">
        		<p>The end.</p>
        	</div>-->

        </div>
		
	</div>
	    <!-- tab End by Dinesh-->
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
     
     
        <div class="clearfix"></div>
       
	   
	   <!-- content Start--> 
<?php /*?>

<table>
  
  <?php
  if(!empty($QuestionAns)){
	  $i = 1;
	  foreach($QuestionAns as $qval){ 
?>
	<tr>
    <th><?php echo $i .' . '.strip_tags($qval['Question']['title']);?></th>
  </tr>

  
  <?php 
  $j = 1;
  foreach($qval['Answer'] as $ansval) { //pr($ansval);
	if($ansval['answer_type'] == 1){
		$answer_type = '<input type="radio" name="ans_'.$qval['Question']['id'].'">';
	}
	else if($ansval['answer_type'] == 2){
		$answer_type = '<input type="checkbox" name="ans_'.$qval['Question']['id'].'">';
	} else if($ansval['answer_type'] == 3){
		$answer_type = '<input type="radio" name="ans_'.$qval['Question']['id'].'">';
	} else if($ansval['answer_type'] == 4){
		$answer_type = '<textarea name="ans_'.$qval['Question']['id'].'"></textarea>';
	}
	
  ?>
  <tr>
    <td><?php 
			 if($ansval['answer_type'] == 4){
				 echo $answer_type;
			 } else {
			   echo $j .' . '.$answer_type.'&nbsp;&nbsp;&nbsp;'.strip_tags($ansval['answer_text']);
			 }
	?></td>
  </tr>
  
  
<?php
		  $j++;
		
		}
		$i++;
	  }
  }
?>
</table>
<?php */?>

      </div>
    </div>
    
  
 
</div>


   <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>