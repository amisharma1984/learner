<style>
.individual_report_table1 table tr td{width:auto;border:2px solid #ccc;}
.row_inside1{margin-top:15px;margin-bottom:10px;border:2px solid #ccc;padding-bottom:10px;}
.row_inside1 h5{text-align:center;font-size:16px;font-weight:bold;color:#FF0000;text-decoration:underline;}
.row_inside1 table tr td{width:auto;border:none !important; font-weight:bold;color:#FF0000;text-decoration:underline;}
.row_inside1 table, .row_inside1 table tr{border:none !important;}
.final_result p{font-size:16px;font-weight:bold;color:#FF0000;text-decoration:underline;text-align:center;margin-top:65px;}
.div_non_cal, .div_cal, .fina_div{background: url("<?php echo $this->webroot;?>img/front-end/header_g.jpg") repeat left top; padding:5px 10px;margin-bottom:50px;}
.div_non_cal h2, .div_cal h2 {margin: 0;padding: 0;color: #ffffff;font-weight: bold;font-size: 16px;text-transform: uppercase;}
.fina_div table tr td{padding:15px !important;}
.fina_div1 table tr td, .fina_div2 table tr td{padding:15px !important;} 
.fina_div1 table tr td:nth-child(2), .fina_div2 table tr td:nth-child(2){width:60px;}
.table_span{background-color: #0668bd;border-radius: 2px;padding: 8px 10px;width: 100%;display: block;}

</style>
<link rel="stylesheet" href="<?php echo $this->webroot;?>TabsNextPrev/tabs.css" type="text/css" media="screen, projection"/>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->webroot;?>TabsNextPrev/js/jquery-ui-1.7.custom.min.js"></script>

    <script type="text/javascript">
		$(function() {
		   
			var $tabs = $('.tabs').tabs();
	
			$(".ui-tabs-panel").each(function(i){
	
			  var totalSize = $(".ui-tabs-panel").size() - 1;
	
			  if (i != totalSize) {
			      next = i + 2;
		   		  $(this).append("<a href='#' onclick='save_next()' class='next-tab mover nexts ' rel='" + next + "'>NEXT &#187;</a>");
			  } else {
				    $(this).append("<a href='#' onclick='page_redirect()' class='next-tab mover nexts ' rel='" + next + "'>SAVE</a>");
			  }
	  
			  if (i != 0) {
			      prev = i;
		   		  $(this).append("<a href='#' class='prev-tab mover  prevs' rel='" + prev + "'>&#171; PREVIOUS</a>");
			  }
   		
			});
	
			$('.next-tab, .prev-tab').click(function() { 
		           $tabs.tabs('select', $(this).attr("rel"));
		           return false;
		       });
       

		});
		
		function page_redirect(){
		  var schoolPurchaseExam_id = '<?php echo !empty($schoolPurchaseExam_id) ? $schoolPurchaseExam_id : ''?>';
			if(schoolPurchaseExam_id != ''){
				var url = '<?php echo $this->webroot."schools/student_list_appeared_in_exam/";?>'+ schoolPurchaseExam_id;
			} else {
				var url = '<?php echo $this->webroot."students/take_exam_list";?>';
			}
			
			
			window.location.href= url;
		}
		
		function save_next(){
			
			
				
			  var numberOfCheckedRadio = $('input:radio:checked').length;
				 var qst = numberOfCheckedRadio;
				 var totalQuestions = <?php echo count($QuestionAns);?>;
				 var percentageBar = (100 * qst) / totalQuestions;
					$('#attempted_qs').text(numberOfCheckedRadio);
					if(percentageBar == 100){
						 $( "#percent_val_txt_qs" ).text( percentageBar+'%' );
					} else {
						  $( "#percent_val_txt_qs" ).text( percentageBar.toFixed(1)+'%' );
					}
					$('#qs_attempted_prcessing').css({"width": ""});
					$('#qs_attempted_prcessing').css({"width": percentageBar+"%"});
		}
		
	$(document).ready(function(){
			$("input[type=radio]").click(function(event) {
			});	
		});
		
		
		function answer_details(ans_id){
			$('#details_showhide_' + ans_id).toggle();
		}
		
    </script>


<?php 
	$d1 = ($correctAnswer - $yearGroupMean);
	$daviation1= ($d1*$d1);
	$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
	$studentZscoreByYearGrp1 = round((($correctAnswer - $yearGroupMean)/$standardDeviation),2); 
	
	
	$d2 = ($correctAnswer1 - $yearGroupMean);
	$daviation1= ($d2*$d2);
	$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
	$studentZscoreByYearGrp2 = round((($correctAnswer1 - $yearGroupMean)/$standardDeviation),2); 
	
	$correctAnswer3 = ($correctAnswer + $correctAnswer1);
	$d3 = ($correctAnswer3 - $yearGroupMean);
	$daviation1= ($d3*$d3);
	$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 3);
	$studentZscoreByYearGrp3 = round((($correctAnswer3 - $yearGroupMean)/$standardDeviation),2); 
			
?>	
<div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-12">
	
      <div class="content"> 
	  <div class="individual_report_table1">
		<h5 style="text-align:center;font-size:16px;font-weight:bold;color:green;text-decoration:underline;"><?php echo $school_name;?></h5>
		<table >
			<tr>
				<td>Student Name:</td>				
				<td><?php echo $name;?></td>
				<td>Student Year:</td>
				<td><?php echo $student_year;?></td>
				
			</tr>
			<tr>
				<td>Student Email:</td>
				<td><?php echo $email; ?></td>
				<td>Student Class:</td>
				<td><?php echo $student_class;?></td>
				
			</tr>
		</table>
	  </div>
	   <div class="row individual_report_row1">
		  <div class="col-md-8">
			<div class="row_inside1">
			<h5>Result Per Section</h5>
			<table>
				<tr>
					<td>Non-Calculator Section</td>
					<td>Calculator Section</td>
				</tr>
			</table>
			</div>
		  </div>
		   <div class="col-md-4 final_result">
			<p>Final Result</p>
		  </div>
	  </div>
	   <div class="row individual_report_row2">
		  <div class="col-md-4">
			
			<div class='div_non_cal'>
				<h2>Questions</h2>
				<div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAnsNonCalculator)){
			$i = 1;
			foreach($QuestionAnsNonCalculator as $qval){
			
			 if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				} else if(!empty($qval['StudentAnswer']['answer_text']) && $qval['StudentAnswer']['answer_id'] == 0){
					$stans = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['StudentAnswer']['answer_text'])); // remove p tag
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['Question']['CorrectAnswer'][0]['answer_text'])); // remove p tag
					
					if($stans== $corAns){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				}
				
				else {
					$qstab_color = 'unattempt_qa';
				}
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo $i;?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			?>
    	   </ul>
		</div>
		<div class="attampt_flag">
			 <div class="res_point"><span class="correct_answer" ></span>Correct Answer
				<div class="square">
				<div class="sq_text"><?php echo $correctAnswer;?></div>
				</div>
			  </div>
			  
			 <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer
			 <div class="square">
				<div class="sq_text_wrng"><?php echo ($totalAttemptQs - $correctAnswer);?></div>
				</div>
			 </div>
		   <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted
		   <div class="square">
				<div class="sq_text_unattmpt"><?php echo ($totalqustionSet - $totalAttemptQs);?></div>
				</div>
		   </div>
		</div>
		<div class="fina_div1">
		<div class="rp-student-info">		
				<table>
					<tbody>
						<tr>
							<td><span class="table_span">Mark</span></td>
							<td><?php echo $correctAnswer;?></td>
						</tr>

						<tr>
							<td>Year group</br> mean</td>
							<td><?php echo @$yearGroupMean;?></td>
						</tr>

						<tr>
							<td>Year group</br> rank</td>
							<td><?php echo @$studentRank['MyExamStudentAnswer']['year_group_rank'];?></td>
						</tr>
						
						<tr>
							<td>Class mean</td>
							<td><?php echo @$sameClassMean;?></td>
						</tr>
						
						<tr>
							<td>Class rank</td>
							<td><?php echo @$studentRank['MyExamStudentAnswer']['class_rank'];?></td>
						</tr>
						
						<tr>
							<td><span class="table_span">Z-score</br> per year</br> group</span> </td>
							<td><?php echo @$studentZscoreByYearGrp1;?></td>
						</tr>
						

					</tbody>
				</table>
			</div>
		</div>
		</div>
			
		  </div>
		   <div class="col-md-4">
			<div class='div_cal'>
				<h2>QUESTIONS</h2>
				<div id="" class="tabs question_tab_">
		
    		<ul style="display:block;">
			<?php
			if(!empty($QuestionAnsCalculator)){
			$i = 1;
			foreach($QuestionAnsCalculator as $qval){
			
			 if(!empty($qval['StudentAnswer']['answer_id'])){
					if(@$qval['StudentAnswer']['answer_id'] == @$qval['Question']['CorrectAnswer'][0]['id']){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				} else if(!empty($qval['StudentAnswer']['answer_text']) && $qval['StudentAnswer']['answer_id'] == 0){
					$stans = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['StudentAnswer']['answer_text'])); // remove p tag
					$corAns = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', strtolower($qval['Question']['CorrectAnswer'][0]['answer_text'])); // remove p tag
					
					if($stans== $corAns){
						$qstab_color = 'right_qa';
					} else {
						$qstab_color = 'wrong_qa';
					}
				}
				
				else {
					$qstab_color = 'unattempt_qa';
				}
			
			?>
			<li><a class="<?php echo $qstab_color;?><?php //echo (@$i == 1) ? 'active' : '';?>" href="#fragment-<?php echo ($i + 32);?>"><?php echo $i;?></a></li>
			<?php 
				$i++;
				}
			}
			?>
    	   </ul>
			</div>
				<div class="attampt_flag">
					 <div class="res_point"><span class="correct_answer" ></span>Correct Answer
						<div class="square">
						<div class="sq_text"><?php echo $correctAnswer1;?></div>
						</div>
					  </div>
					  
					 <div class="res_point"> <span class="wrong_answer" ></span>Wrong Answer
					 <div class="square">
						<div class="sq_text_wrng"><?php echo ($totalAttemptQs1 - $correctAnswer1);?></div>
						</div>
					 </div>
				   <div class="res_point"><span class="not_attemptedqs" ></span> Not attempted
				   <div class="square">
						<div class="sq_text_unattmpt"><?php echo ($totalqustionSet - $totalAttemptQs1);?></div>
						</div>
				   </div>
				</div>
				<div class="fina_div2">
				<div class="rp-student-info">		
				<table>
					<tbody>
						<tr>
							<td><span class="table_span">Mark</span></td>
							<td><?php echo $correctAnswer1;?></td>
						</tr>

						<tr>
							<td>Year group mean</td>
							<td><?php echo @$yearGroupMean;?></td>
						</tr>

						<tr>
							<td>Year group rank</td>
							<td><?php echo @$studentRank['MyExamStudentAnswer']['year_group_rank'];?></td>
						</tr>
						
						<tr>
							<td>Class mean</td>
							<td><?php echo @$sameClassMean;?></td>
						</tr>
						
						<tr>
							<td>Class rank</td>
							<td><?php echo @$studentRank['MyExamStudentAnswer']['class_rank'];?></td>
						</tr>
						
						<tr>
							<td><span class="table_span">Z-score per year group</span> </td>
							<td><?php echo @$studentZscoreByYearGrp2;?></td>
						</tr>
						

					</tbody>
				</table>
			</div>
				</div>
			</div>
			
		  </div>
		  <div class="col-md-4">
			<div class="fina_div">
			
		
		<?php /*?><div class="attempt_qus">
				<ul>
						<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
						<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>

						<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>

				</ul>
		</div><?php */?>
		
			
		
<div class="rp-student-info">		
<table>
	<tbody>
		<tr>
			<td><span class="table_span">Mark</span></td>
			<td><?php echo ($correctAnswer + $correctAnswer1);?></td>
		</tr>

		<tr>
			<td>Year group mean</td>
			<td><?php echo @$yearGroupMean;?></td>
		</tr>

		<tr>
			<td>Year group rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['year_group_rank'];?></td>
		</tr>
		
		<tr>
			<td>Class mean</td>
			<td><?php echo @$sameClassMean;?></td>
		</tr>
		
		<tr>
			<td>Class rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['class_rank'];?></td>
		</tr>
		
		<tr>
			<td><span class="table_span">Z-score per year group</span> </td>
			<td><?php echo $studentZscoreByYearGrp3;?></td>
		</tr>
		

	</tbody>
</table>
</div>

			</div>
		  </div>
	  </div>
	  
	  
	  
	  
      
  </div>
</div>
</div>
<style>
.square {
    background:#fff;
    width: 35px;
    height: 25px;
	margin: -19px 0px 0px 132px
}
.sq_text {
    padding: 4px 0px 0px 9px;
   color:green!important;
}
.sq_text_wrng {
   padding: 4px 0px 0px 9px;
   color:red!important;
}

.sq_text_unattmpt {
   padding: 4px 0px 0px 9px;
   color:orange!important;
}
</style>
<style>
.rp-student-info table, tr, td{
	width:92%;
	border:2px solid #a4b0db !important;
	margin-left:10px;
}
.rp-student-info table tr td {
    padding: 23px;
	font-size:16px;
	
    color: #fff !important;
}
.right_bar_combined {
	min-height: 980px !important;
}
</style>