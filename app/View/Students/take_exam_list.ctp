<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
       <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
				<?php echo $this->Session->flash();?>
                <h2>Take Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                
                </div>
                <div class="prof_details prof_form">
                   <!-- content Start--> 


<table class="student-list-apr" style="border:1px solid pink;">
  <tr>
    <th width="80">List</th>
	 <th width="80">Year</th>
    <th width="200">Exam Category</th>
	
    <th width="200">Exam Name</th>
    <th width="100">Days remaining</br>before expiry</th>
	<!--<th width="150">Price($ AUD)</th>-->
	<th width="310" style="text-align: center;">Start Exam</th>
  </tr>
  
  <?php
  if(!empty($examinations)){ //pr($examinations);die;
  $i = 1;
	  foreach($examinations as $val){
		  
		  $non_cal = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'NON-CALCULATOR'));
		  $cal = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'CALCULATOR'));
		  $nothing = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'nothing'));
		  
		   $category = explode(' ',$val['Examination']['ExaminationCategory']['name']);
		  
		  $myExamId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['MyExam']['id']));
		  $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		 
		 $CalculatorQuestionCount = $this->requestAction(array('controller' => 'students', 'action' => 'get_calculator_exam_count', $val['Examination']['id']));
		  $NonCalculatorQuestionCount = $this->requestAction(array('controller' => 'students', 'action' => 'get_non_calculator_exam_count', $val['Examination']['id']));
		  if($CalculatorQuestionCount > 0 && $NonCalculatorQuestionCount > 0){
				$content = 'Calculator not allowed';
				$examFlag = 'BothExam';
				$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'NON-CALCULATOR'));
			} else if($CalculatorQuestionCount == 0 && $NonCalculatorQuestionCount > 0){
				$content = 'Calculator not allowed';
				$examFlag = 'SingleExam';
				$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'NON-CALCULATOR'));
			} else if($CalculatorQuestionCount > 0 && $NonCalculatorQuestionCount == 0){
				$content = 'Calculator allowed';
				$examFlag = 'SingleExam';
				$calculator_non_calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'CALCULATOR'));
			} else {
				//nothing
				$examFlag = 'nothing';
			}
			
			$examFlag1 = $examFlag;
	  $start_subscription = strtotime($val['MyExam']['start_subscription']);
	  $end_subscription = strtotime($val['MyExam']['end_subscription']);
	  //$datediff = $end_subscription - $start_subscription;
	  //$validityDays = floor($datediff / (60 * 60 * 24));
	  
	  $now = time();
	  $datediff = ($end_subscription - $now);
	  $remainingDays = floor($datediff / (60 * 60 * 24));
	  if($remainingDays > 0){
		  $leftDays = '<font color="#A0760A">  '.$remainingDays.'</font>';
		  if($remainingDays > 1){
			  $timeUnit = '<font color="#A0760A">Days</font>';
		  } else {
			   $timeUnit = '<font color="#A0760A">Day</font>';
		  }
	  } else {
		  $leftDays = '';
		  $timeUnit = '';
	  }
	  
	  
      $examFlag = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $examFlag));
		  
		  
		  ?>
		  <tr>
		<td><?php echo $i?></td>
		<td><?php echo $category[1];?></td>
		<td><?php echo 'NAPLAN Style Practice';?></td>
		
		<td>
		<?php 
		echo ($val['Examination']['exam_flag'] == 2)? 'Literacy '.$val['Examination']['paper'] : 'Numeracy '.$val['Examination']['paper'];
		?>
		
		</td>
		<!--<td><?php //echo $val['Examination']['price'];?></td>-->
		<td>
		<?php 
		 if($remainingDays > 0){
		  echo $leftDays.' '.$timeUnit;
		 } else {
			 echo '0';
		 }
		?>
		</td>
  
	  
	  <?php
	  if($remainingDays > 0){
		  if($examFlag1 != 'SingleExam' && $examFlag1 != 'nothing'){
	  ?>
	 
	  <td>
	  <!--<a href="<?php //echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$calculator_non_calculator.'/'.$examFlag;;?>">Click here</a>-->
	    <!--<a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target=".examModalBox<?php echo $val['Examination']['id'];?>"   class="student_list_ans" >Click here</a>-->
	    
		<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$non_cal.'/'.$examFlag;?>" style="margin-bottom: 10px;"  class="student_list_ans" >Click Here for Non-calculator Section</a>
	    </br></br>
		<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$cal.'/'.$examFlag;?>" style="margin-bottom: 10px;"  class="student_list_ans" >Click Here for Calculator Section</a>
		</td>
		<?php } else {?>
		 <td style="text-align:center;">
		 <?php if($val['Examination']['exam_flag'] != 2){?>
		<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$nothing.'/'.$examFlag;?>" style="margin-bottom: 10px;"  class="student_list_ans" >Click Here</a>
		 <?php } else {?>
		 
		 <a target="_blank" href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#SamplaeExamPopUp_<?php echo $ExaminationId;?>"  class="student_list_ans" >Click Here</a>
		 
		 
<div class="modal fade" id="SamplaeExamPopUp_<?php echo $ExaminationId;?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Literacy Practice Exam:</h4>
            </div> 
            <div class="modal-body">
			<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$nothing.'/'.$examFlag.'/1';?>"  class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Reading booklet questions</a>
			
			<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$nothing.'/'.$examFlag.'/2';?>"  class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Language Conventions</a>
			
			
			<a target="_blank" href="<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$nothing.'/'.$examFlag.'/3';?>"  class="btn btn-secondary btn-lg btn-block" data-toggle="modal">Writing Leaflet</a>
            	
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>	
		 
		 
		 
		
	    <?php
		  }
		}
		?>
	  </td>
	  <?php } else {?>
	  <td style="text-align:center;">
	  <a href="javascript:void(0)" style="color:red;">Expired</a>
	  <?php }?>
	  </td>
	  
	  
  </tr>
  
  
  
  
      <!--1st modal -->
<div class="modal fade examModalBox<?php echo $val['Examination']['id'];?>" id="#examModalBox__" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="width:500px;margin:100px;">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Click the below section that you want to do first:</h4>
            </div>
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$non_cal.'/'.$examFlag;;?>')">Non-calculator</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'students/take_exam/'.$ExaminationId.'/'.$myExamId.'/'.$cal.'/'.$examFlag;;?>')">Calculator</button>
                    
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
  
  
  
  
  
  
  
  
<?php
$i++;
	  }
  }
?>
</table>
					<?php if(empty($examinations)){?>
						<div class="not_found">
								<span>You have not purchased any exam yet.</span>
						</div> 
				<?php }?>
				 
                </div>
				
<div class="pagination_qa">
<!-- Shows the page numbers -->
<?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>

<?php echo $this->Paginator->numbers(); ?>
<!-- Shows the next and previous links -->
<?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
<!-- prints X of Y, where X is current page and Y is number of pages -->
<strong><?php echo $this->Paginator->counter(); ?></strong>
</div>	
				
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	

<script type="text/javascript">
    function openUrl(url){
            var win = window.open(url, '_blank');
    }
    </script>	