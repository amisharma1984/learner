<?php $calculator = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', 'CALCULATOR'));?>

<div class="container">
	<div class="profile_section inner_field">
		<div class="row">
		<?php echo $this->element('dashboard_left');?>
			<div class="col-sm-8 col-md-9">
				<div class="mainsection">
					<div class="inner_cont">
						<div class="main_headding">
						<h2><h2>Second Section Exam (Calculator allowed)</h2></h2>
						<h3> <?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
						</div>
						<div class="prof_details prof_form">
							<div class="row">
									 <div class="exam_instruction">
										<p style="color:#119549; font-weight:600;">Your first section exam has been done. Please click below link to proceed second section exam.</p>
										<div class="clear_fix"></div>
									  </div>
									<div class="col-md-12">
										<div class="form-group start_exam" style="margin-left:23%;">
										<a href="<?php echo $this->webroot.'students/take_exam_second_section/'.$calculator;?>" class="btn btn-info def_btn"><span class="blink">Click here to start 2nd section exam</span></a>
										<div class="clear_fix"></div>
									</div>
								 </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>