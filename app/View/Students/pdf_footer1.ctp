<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pindigo</title>

    <!-- Bootstrap CSS-->
    <link href="<?php echo $this->webroot; ?>css/bootstrap.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo $this->webroot; ?>css/customCheckboxRadio.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--[if IE 7]>
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome-ie7.min.css">
<![endif]-->
    
    <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/style-for-monitor-task-journal-report.css" >

    <script type="text/javascript">
        var pdfInfo = {};
        var x = document.location.search.substring(1).split('&');
        for (var i in x) { 
            var z = x[i].split('=',2); 
            pdfInfo[z[0]] = unescape(z[1]); 
        }
        function getPdfInfo() {
            var page = pdfInfo.page || 1;
            var pageCount = pdfInfo.topage || 1;
            document.getElementById('pdfkit_page_current').textContent = page;
            document.getElementById('pdfkit_page_count').textContent = pageCount;
        }
    </script>

</head>
<body onload="getPdfInfo();">
 <div class="container">
    <!--<div class="row mar_top20">
    <div class="col-md-3"><?php //echo date('d M Y', time()); ?></div>
    <div class="col-md-3 text-right">
      <img src="<?php //echo $this->webroot;?>img/pd-logo.png" alt="footer-image" class="img-thumbnail" style="border: none; float: right; margin-top: -8px;
    width: 140px;" />
    </div>
    <div class="col-md-3">&#169; <?php //echo date('Y', time()); ?> Purpleone Technologies Limited.</div>
    <div class="col-md-3" style="text-align:right;">Page&nbsp;<span id="pdfkit_page_current"></span>&nbsp;of&nbsp;<span id="pdfkit_page_count"></span></div>
    </div>border-top: 1px solid #ddd;-->
     
     <footer class="footer">
        <div class="row">
            <div class="col-md-12">
            <table style="width:100%; margin-top: 10px;" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td style="width:25%;">
                        <?php echo date('d M Y', time()); ?>
                    </td>
                    <!--<td style="width:25%;">
                        <img src="<?php echo $this->webroot;?>img/pd-logo.png" alt="footer-image" class="img-thumbnail" style="border: none; float: right; margin-top: -8px;
    width: 140px;" />
                    </td>-->
                    <td style="width:25%;">
                        <p class="text-center"></p>
                    </td>
                    <td style="width:25%;">
                        <p class="text-right">Page&nbsp;<span id="pdfkit_page_current"></span>&nbsp;of&nbsp;<span id="pdfkit_page_count"></span></p>
                    </td>
                </tr>
            </table>
            </div>
        </div>
     </footer>
     
</div>   
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $this->webroot; ?>js/jquery-1.11.0.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo $this->webroot; ?>js/bootstrap.js"></script>

</body>
</html>