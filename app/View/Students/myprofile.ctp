<!-- below jquery file for hide flash message aftrer update and after login -->
  <script src="<?php echo Router::url('/', true); ?>js/jquery-1.12.4.js"></script>
   
<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
         <?php echo $this->element('dashboard_left');?>
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>My Profile</h2>
                  <h3> <?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                 <!-- <span>year 9</span>-->
				  <?php echo $this->Session->flash();?>
                </div>
                <div class="prof_details prof_form">
                 <?php 
				// echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'myprofile'), true))); 
				// echo $this->Form->create('Student'); 
				 echo $this->Form->create('Student', array('name' => 'StudentUpdate', 'onsubmit' => 'return validation()')); 
				 ?>
				 
				 <div class="row">
					 <div style="float:right;color:#f69d25;margin:0 18px 20px 0">Last Logged In : 
						 <?php 
						 if(!empty($lastLoginStudent)){
							  echo date("D, jS \of F Y, h:i:s A", strtotime($lastLoginStudent));
						 }
						 ?>
					 </div>
				 </div>
				 
				 
						<div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >First Name :</label>
									<?php echo $this->Form->input('first_name', array('label' => false, 'value' => @$studentDetails['Student']['first_name'], 'class' => 'form-control inp_text'))?>			
									<span  class="prof_err" id="fnerror"><?php echo @$fnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Last Name :</label>
									<?php echo $this->Form->input('last_name', array('label' => false,'value' => @$studentDetails['Student']['last_name'], 'class' => 'form-control inp_text'))?>
									<span  class="prof_err" id="lnerror"><?php echo @$lnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
						</div>

						<!-- <div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Year :</label>
									<?php //echo $this->Form->input('student_class_id', array('label' => false, 'div' => false,'value' => @$studentDetails['Student']['student_class_id'],  'class' => 'selectpicker show-tick form-control inp_text', 'options' => $classOptions))?>
									<div class="clear_fix"></div>
								</div>
							</div>
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >School :</label>
									<?php //echo $this->Form->input('school_name', array('label' => false, 'value' => @$studentDetails['Student']['school_name'], 'class' => 'form-control inp_text'))?>
									<span  class="prof_err" id="scerror"><?php //echo @$lnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>
						</div> -->


						<div class="row">
							<div class="col-md-6 form_info">
								<div class="form-group">
									<label >Email :</label>
									 <?php echo $this->Form->input('email', array('label' => false, 'readonly' => 'readonly', 'value' => @$studentDetails['Student']['email'], 'class' => 'form-control inp_text'))?>
									<div class="clear_fix"></div>
								</div>
							</div>
							<!--<div class="col-md-6 form_info">
								<div class="form-group">
									<label >School :</label>
									<?php echo $this->Form->input('school_name', array('label' => false,'required' => 'required', 'value' => @$studentDetails['Student']['school_name'], 'class' => 'form-control inp_text'))?>
									<span  class="prof_err" id="lnerror"><?php echo @$lnerror;?> </span>
									<div class="clear_fix"></div>
								</div>
							</div>-->
						</div>
						
							<div class="row">
								<div class="col-md-6"></div>
									<div class="col-md-6">
										<div class="form-group">
											<input type="submit" class="btn btn-info def_btn" value="Submit">
											<div class="clear_fix"></div>
										</div>
									</div>
							</div>		  
                   
                   <?php echo $this->Form->end(); ?>
                </div>
				
				
				
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
function validation(){ 
	var result=true;
	if($.trim($('#StudentFirstName').val()) == ""){
		 $('#fnerror').html('Please enter first name');
		 result=false;
	} else {
		 $('#fnerror').html('');
	}
	if($.trim($('#StudentLastName').val()) == ""){
		 $('#lnerror').html('Please enter last name');
		 result=false;
	} else {
		 $('#lnerror').html('');
	}
	
	if($.trim($('#StudentSchoolName').val()) == ""){
		 $('#scerror').html('Please enter school name');
		 result=false;
	} else {
		 $('#scerror').html('');
	}
	
	
	
	
	return result;
	
 }
</script>
