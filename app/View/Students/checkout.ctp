<div class="container">
      <div class="profile_section inner_field">
        <div class="row">
			<?php echo $this->element('dashboard_left');?>
		  
          <div class="col-sm-8 col-md-9">
            <div class="mainsection">
              <div class="inner_cont">
                <div class="main_headding">
                <h2>Purchase Exam</h2>
                  <h3><?php //echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  
                </div>
                <div class="prof_details prof_form">
     <?php  echo $this->Form->create(null, array('url' => $this->Html->url(array('controller' => 'students', 'action' => 'checkout'), true))); ?>            
<table class="student-list-apr">
  <tr>
   <th>List</th>
    <th>Year</th>
    <th width="250">Exam Category</th>
     <th>Exam Name</th>
	
    <th>Paper</th>
	<!--<th>Type</th>-->
	<th width="90">Price($ AUD)</th>
	
	<!--<th>No. of student</th>
	<th>Total($ AUD)</th>-->
  </tr>
  
  <?php
  if(!empty($allExamAr)){
	  $i=1;
	  foreach($allExamAr as $val){
		  $category = explode(' ',$val['ExaminationCategory']['name']);
		    $ExaminationId = $this->requestAction(array('controller' => 'App', 'action' => 'data_encrypt', $val['Examination']['id']));
		  ?>
		  <tr>
		  <td><?php echo $i?></td>
		   <td><?php echo $category[1];?></td>
    <td><?php echo 'NAPLAN Style Practice';?></td>
	
	
    <td><?php echo $val['Examination']['title'];?></td>
    <td><?php echo $val['Examination']['paper'];?></td>
	
	<!--<td><?php //echo $val['ExaminationType']['name'];?></td>-->
     <input type="hidden" value="<?php echo $val['Examination']['id'];?>" name="ids[]">

	 
	 <td>
	  <input id="unit_price_<?php echo $val['Examination']['id'];?>" value="<?php echo $val['Examination']['price'];?>" autocomplete="off"  type="text" readonly   required size="10" name="total_price[]">
	  </td>
	 
  </tr>
<?php
$i++;
	  }
  }
?>
  
</table>
               <input type="hidden" value="purchase_exam_payment" name="page_name">
				<div class="exam_pay_div">
				<div class="gross_total">
					<label style="line-height:30px">Sub Total ($ AUD) :</label> <input id="gross_total" name="sub_total" value="<?php echo $subTotal;?>"  readonly type="text" size="10"></br>
					<label style="margin-left:1px;line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GST (10%) :</label> <input id="gst_10" value="<?php echo $gst;?>"  name="gst"  readonly type="text" size="10"></br>
					<label style="line-height:30px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total ($ AUD) :</label> <input id="total_with_gst" value="<?php echo $total;?>"  name="gross_total"  readonly type="text" size="10">
				</div>
				
				<!--<div class="exam_chkout"  id="pay_button_default">
					<input type="button" class="btn btn-info def_btn" value="Make Payment">
				</div>
				
				
				<div class="exam_chkout" style="display:none;" id="pay_button_show_hide">
					<input type="submit" class="btn btn-info def_btn"  value="Make Payment">
				</div>-->
				<div class="exam_chkout">
					<input type="submit" class="btn btn-info def_btn"  value="Make Payment">
				</div>
				
				
			</div>
			
			<?php echo $this->Form->end(); ?>

				 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<script>
	function price_count(id){
		 var sum = 0;
		var unit_val = Number($('#unit_price_' + id).val());
		var no_of_student = Number($('#no_of_stdnt_' + id).val());
		var tot = unit_val *  no_of_student;
				
				if(tot != ''){
					 $('#total_price_' + id).val(tot);
				} else {
					 $('#total_price_' + id).val('');
				}
				
				$('.total_price').each(function() {
					sum += Number($(this).val());
				});
				
				$('#gross_total').val(sum);
				$('#gst_10').val((sum*10)/100);
			     $('#total_with_gst').val((sum + ((sum*10)/100)));
				 
				if($('#gross_total').val() == ''  || $('#gross_total').val() == 0){
					$('#pay_button_show_hide').hide();
					$('#pay_button_default').show();
				} else {
					$('#pay_button_show_hide').show();
					$('#pay_button_default').hide();
				}
				 
				
		        
		
	}
	</script>
