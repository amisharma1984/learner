<!--preview_mail_template-->
<?php if(!empty($previous) || !empty($next)){ ?>
<div class="col-md-12 col-xs-12 col-sm-12 mb-sm">
    <?php if (!empty($previous)) echo $this->Html->link(__('&laquo; Previous'), array('controller' => 'email_systems', 'action' => 'preview_mail_template', $previous['EmailTemplate']['id']), array('class' => 'btn btn-sm btn-primary preNextButton', 'style' => 'float:left;', 'escape' => false, 'data-template' => $previous['EmailTemplate']['id'])); ?>
    <?php if (!empty($next))  echo $this->Html->link(__('Next &raquo;'), array('controller' => 'email_systems', 'action' => 'preview_mail_template', $next['EmailTemplate']['id']), array('class' => 'btn btn-sm btn-primary preNextButton', 'style' => 'float:right;', 'escape' => false, 'data-template' => $next['EmailTemplate']['id'])); ?>
</div>
<?php } ?>

<?php if(!empty($templateDetails['EmailTemplate'])){ ?>
<div class="col-md-12 col-xs-12 col-sm-12">
    
    <div class="row">
        <div class="col-md-10 col-sm-10 col-xs-9" >
            <p><strong>Title:&nbsp;</strong><?php echo $templateDetails['EmailTemplate']['title']; ?></p>
            <div class="">
                <p class="mb-sm"><strong>Content: </strong></p>
<!--                <iframe id="emailTemplateFrame" src="<?php echo Router::url('/', true); ?>pages/get_template_content/<?php echo $templateDetails['EmailTemplate']['id']; ?>" class="" style="min-height: 700px;" frameborder="0" height="100%" width="100%"></iframe>-->
                <?php echo $templateDetails['EmailTemplate']['content']; ?>
            </div>
<!--            <div id="templateHiddenContent" style="display: none;">
                <?php //echo $templateDetails['EmailTemplate']['content']; ?>
            </div>-->
        </div>
        <div class="col-md-2 col-sm-2 col-xs-3 text-right">
            <?php if(!empty($admindata['Admin'])){ ?>
            <a class="btn-info btn-sm editTemplateButton" title="Edit" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-email-template="<?php echo $templateDetails['EmailTemplate']['id']; ?>" data-toggle="modal" data-target="#emailTemplateCreateUpdateModal">
                <span class="glyphicon glyphicon-edit icon-white"></span>
            </a>
            <a class="btn-danger btn-sm deleteTemplateButton" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#deleteEmailTemplateModal" data-email-template="<?php echo $templateDetails['EmailTemplate']['id']; ?>" data-previous="<?php echo (!empty($previous['EmailTemplate']['id']))?$previous['EmailTemplate']['id']:''; ?>" data-next="<?php echo (!empty($next['EmailTemplate']['id']))?$next['EmailTemplate']['id']:''; ?>">
                <span class="glyphicon glyphicon-trash icon-white"></span>
            </a>
            <?php } ?>
        </div>
    </div>
    
</div>
<?php } ?>