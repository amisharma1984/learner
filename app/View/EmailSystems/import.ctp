<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Email Systems'), "link" => array('controller' => 'email_systems', 'action' => 'index')),
    array("text" => __('Import'), "link" => array('controller' => 'email_systems', 'action' => 'import'))
        ), true);
?>

<?php echo $this->Session->flash(); ?>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-upload"></i> Upload Excel file for School</h2>

                <div class="box-icon">
                    <!--<a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>-->
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <form class="form-horizontal" id="uploadExcelForm" onsubmit="return ValidationEvent()" role="form" action="<?php echo $this->Html->url(array('controller' => 'EmailSystems', 'action' => 'import')); ?>" method='post' enctype='multipart/form-data' accept-charset='utf-8'>
					
					
					<div class="form-group mb-xs">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 5px;">
                            <div class="input-group">
                                <label class="input-group-btn1"><input checked type="radio" value="upload_for_new"  name="upload_type" >&nbsp;Upload New School Data   
                                </label>&nbsp;&nbsp;
                                <label class="input-group-btn1"><input type="radio" value="upload_for_prev"  name="upload_type" >&nbsp;Previous Customer Excel       
                                </label>
                            </div>
                            
                        </div>
                         
                    </div>
					
					
                    <div class="form-group mb-xs">
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 5px;">
                            <div class="input-group">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary btn-sm">
                                        Browse&hellip; <input type="file" style="display: none;" name="data[email_systems][][filename]" id="filename" multiple="multiple" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    </span>
                                </label>
                                <input id="fileNameValue" type="text" class="form-control" readonly disabled>
                            </div>
                            <span class="help-block" style="display:none; color:#c71c22;"> Please select a file. </span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12" style="margin-bottom: 5px;">
                            <p style="font-size:14px;color:#000;">
                                <a class="btn btn-primary btn-sm" href="<?php echo $this->Html->url(array('controller' => 'email_systems', 'action' => 'download')); ?>">Use This Excel to Manage Schools</a>
                            </p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="btn btn-primary btn-sm" value="Add" type="submit" name="dataToAdd">
                            <input class="btn btn-primary btn-sm" value="Update" type="submit" name="dataToUpdate">
                            <!--<input class="btn btn-primary btn-sm" value="Delete School Data" type="submit" name="dataToDelete">-->
							
                        </div>
                    </div>

                </form>
                
            </div>
        </div>
    </div>
</div>
<!--/span-->

<?php echo $this->element('JS/external-scripts'); ?>
<script type="text/javascript">
$(function() {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready( function() {
        $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
              
            if( input.length ) {
                input.val(log);
                $('#uploadExcelForm').find('.help-block').fadeOut();
            } else {
                if( log ) alert(log);
            }

        });
    });

});

function ValidationEvent(){
    if($('#fileNameValue').val() != ''){
        return true;
    }else{
        $('#uploadExcelForm').find('.help-block').fadeIn();
        return false;
    }
}

</script>