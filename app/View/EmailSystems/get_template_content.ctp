<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- 		<meta name="viewport" content="width=device-width">
        <meta content="width=600" name="viewport">
        -->	

        <meta content="width=600, initial-scale=1.0;" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>S & G Publishing and Coaching Academy </title>
        <style type="text/css">
            html,
            body {
                margin: 0;
                padding: 0;
                height: 100% !important;
                width: 100% !important;
            }
            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }
            .ExternalClass {
                width: 100%;
            }
            table,
            td {
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }
            table {
                border-spacing:0 !important;
            }
            .ExternalClass,
            .ExternalClass * {
                line-height: 100%;
            }
            table {
                border-collapse: collapse;
                margin: 0 auto;
            }
            img {
                -ms-interpolation-mode:bicubic;
            }
            .yshortcuts a {
                border-bottom: none !important;
            }
            .mobile-link--footer a {
                color: #666666 !important;
            }
            img {
                border:0 !important;
                outline:none !important;
                text-decoration:none !important;
            }
            /* Media Queries */
            @media screen and (max-device-width: 600px), screen and (max-width: 600px) {
                /* What it does: Overrides email-container's desktop width and forces it into a 100% fluid width. */
                .email-container {
                    width: 100% !important;
                }
                /* What it does: Forces images to resize to the width of their container. */
                img[class="fluid"],
                img[class="fluid-centered"] {
                    width: 100% !important;
                    max-width: 100% !important;
                    height: auto !important;
                    margin: auto !important;
                }
                /* And center justify these ones. */
                img[class="fluid-centered"] {
                    margin: auto !important;
                }
                /* What it does: Forces table cells into full-width rows. */
                td[class="stack-column"],
                td[class="stack-column-center"] {
                    display: block !important;
                    width: 100% !important;
                    direction: ltr !important;
                }
                /* And center justify these ones. */
                td[class="stack-column-center"] {
                    text-align: center !important;
                }
                /* Data Table Styles */
                /* What it does: Hides table headers */
                td[class="data-table-th"] {
                    display: none !important;
                }
                /* What it does: Change the look and layout of the remaining td's */
                td[class="data-table-td"],
                td[class="data-table-td-title"] {
                    display: block !important;
                    width: 100% !important;
                    border: 0 !important;
                }
                /* What it does: Changes the appearance of the first td in each row */
                td[class="data-table-td-title"] {
                    font-weight: bold;
                    color: #333333;
                    padding: 10px 0 0 0 !important;
                    border-top: 2px solid #eeeeee !important;
                }
                /* What it does: Changes the appearance of the other td's in each row */
                td[class="data-table-td"] {
                    padding: 5px 0 0 0 !important
                }
                /* What it does: Provides a visual divider between table rows. In this case, a bit of extra space. */
                td[class="data-table-mobile-divider"] {
                    display: block !important;
                    height: 20px;
                }
                /* END Data Table Styles */
            }
            input{
                border: 0px; outline: 0px; border-bottom: 1px solid #000;
                margin-bottom: 12px;
                padding-left: 4px;
                padding-right: 4px;
            }
        </style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#222222" style="margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
        <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#fff" style="border-collapse:collapse;">
            <tr>
                <td>
                    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="width:600px; margin: auto;" class="email-container">
                        <tr>
                            <td>
                                <!-- Logo + Links : BEGIN -->
                                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td height="5" style="font-size: 0; line-height: 0;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="middle" width="150" style="padding:5px 0; text-align:left; line-height: 1;" class="stack-column-center">
                                            <img src="http://localhost/naplan-exam/img/email_template/logo.png" alt="S & G Publishing and Coaching Academy" width="170" border="0" style="display: block; margin: auto;">
                                        </td>
                                        <td valign="middle" style="padding:5px 0; text-align:center; line-height:1.3; font-family: sans-serif; font-size: 14px; color: #000;" class="stack-column-center">
                                            <p>
                                                <strong style="font-size: 16px;">S & G Publishing and Coaching Academy</strong><br>
                                                ABN 61 201 907 800 <br>
                                                <strong style="font-size: 16px;"> P.O.BOX 237, Punchbowl NSW 2196 </strong><br>
                                                <strong style="font-size: 16px;">Tel: (02) 9707 3067 &nbsp Fax: (02) 9707 3067</strong>
                                                <br>
                                                Email: <a href="mailto:sngpublishing@gmail.com">sngpublishing@gmail.com</a><br>
                                                Website: <a href="www.sngweb.com">www.sngweb.com</a>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10" style="font-size: 0; line-height: 0;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style=" text-transform: uppercase; text-align: center; font-size: 18px">
                                            HEAD TEACHER MATHEMATICS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style=" text-transform: uppercase; text-align: center; padding-top: 10px">
                                            <img src="http://localhost/naplan-exam/img/email_template/sep1.png" alt="sep" width="500" border="0" style="display: block; margin: auto;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20" style="font-size: 0; line-height: 0;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="10" style="font-size: 0; line-height: 0;">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <!-- Logo + Links : END -->
                                <!-- Main Email Body : BEGIN -->
                                <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                                    <!-- Single Fluid Image, No Crop : BEGIN -->
                                    <tr>
                                        <td valign="middle" align="center">
                                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding-right: 10px">
                                                        <img src="http://localhost/naplan-exam/img/email_template/prod_add1.png" alt="product image" width="165" border="0" style="display: block; margin: auto;">
                                                    </td>
                                                    <td style="padding-right: 10px">
                                                        <img src="http://localhost/naplan-exam/img/email_template/prod_add2.png" alt="product image" width="165" border="0" style="display: block; margin: auto;">
                                                    </td>
                                                    <td valign="top" style="padding:8px 6px 8px 8px; border:1px solid #732f6c; line-height:1.3; font-family: sans-serif; font-size: 14px; color: #000;">
                                                        <p style="font-weight: 600; margin: 0px; padding:0px 0px 10px 0">
                                                            NUMERACY BOOKS (New Edition)
                                                        </p>
                                                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td valign="top" style="padding:0px 5px 0 0;">
                                                                    -
                                                                </td>
                                                                <td valign="top" style="padding:0px 0px 10px 0;">
                                                                    Seven Papers modelled on real examinations
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding:0px 5px 0 0;">
                                                                    -
                                                                </td>
                                                                <td valign="top" style="padding:0px 0px 10px 0;">
                                                                    Answers and fully worked solutions, problem solving techniques and Australian Curriculum References
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p style=" margin: 0px; padding:0px 0px 10px 0">
                                                            when you buy 10 books or more
                                                        </p>
                                                        <p style="font-weight: 600; font-size: 18px; margin: 0px; padding: 0px">
                                                            Pay $25 per book
                                                        </p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan=3 height="10" style="font-size: 0; line-height: 0;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-right: 10px">
                                                        <img src="http://localhost/naplan-exam/img/email_template/prod_add3.png" alt="product image" width="165" border="0" style="display: block; margin: auto;">
                                                    </td>
                                                    <td style="padding-right: 10px">
                                                        <img src="http://localhost/naplan-exam/img/email_template/prod_add4.png" alt="product image" width="165" border="0" style="display: block; margin: auto;">
                                                    </td>
                                                    <td valign="top" style="line-height:1.3; font-family: sans-serif; font-size: 14px; color: #000;">
                                                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td valign="top" style="padding:8px 6px 5px 8px; margin-bottom: 5px; border:1px solid #732f6c; ">
                                                                    <p style=" margin: 0px; padding:0px 0px 10px 0">
                                                                        New HSC Mathematics 14 examination papers with fully worked solutions.
                                                                    </p>
                                                                    <p style=" margin: 0px; padding:0px 0px 5px 0">
                                                                        When you buy 10 books or more
                                                                    </p>
                                                                    <p style="font-weight: 600; font-size: 18px; margin: 0px; padding: 0px 0px 3px 0">
                                                                        Pay $35 per book
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5" style="font-size: 0; line-height: 0;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" style="padding:8px 6px 5px 8px; border:1px solid #732f6c; ">
                                                                    <p style=" margin: 0px; padding:0px 0px 5px 0">
                                                                        New HSC Mathematics Extension 1 15 examination papers with fully worked solutions.
                                                                    </p>
                                                                    <p style=" margin: 0px; padding:0px 0px 5px 0">
                                                                        When you buy 10 books or more
                                                                    </p>
                                                                    <p style="font-weight: 600; font-size: 18px; margin: 0px; padding: 0px 0px 2px 0">
                                                                        Pay $35 per book
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- Single Fluid Image, No Crop : END -->
                                    <!-- Full Width, Fluid Column : BEGIN -->
                                    <tr>
                                        <td style="padding:30px 0px; text-align: center; font-family: sans-serif; font-size: 18px; line-height: 1.3; color: #000;">
                                            ORDER FORM
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form_section" style=" font-family: sans-serif; font-size: 13px; text-align: center; line-height: 1.3; color: #000;">
                                            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="padding:0 0px 30px;">
                                                        <form accept="" method="post">
                                                            <div class="formcont">
                                                                <label>CONTACT NAME:</label>
                                                                <input type="text" name="userName" class="form-control" id="name" style="width: 29%;">
                                                                <label>EMAIL:</label>
                                                                <input type="text" name="email" class="form-control" id="email" style="width: 40%;">
                                                                <br>
                                                                <label>SCHOOL NAME:</label>
                                                                <input type="text" name="schoolName" class="form-control" id="schoolName" style="width: 31%;">
                                                                <label>SCHOOL ORDER NUMBER:</label>
                                                                <input type="text" name="schoolorderNumber" class="form-control" id="schoolorderNumber" style="width:19%;">
                                                                <br>
                                                                <label>ADDRESS:</label>
                                                                <input type="text" name="address" class="form-control" id="address" style="width:50%;">
                                                                <label>POSTCODE:</label>
                                                                <input type="text" name="postcode" class="form-control" id="postcode " style="width:20%;">
                                                                <br>
                                                                <label>TEL:</label>
                                                                <input type="text" name="tel" class="form-control" id="tel " style="width: 23%;">
                                                                <label>FAX:</label>
                                                                <input type="text" name="fax" class="form-control" id="fax " style="width: 24%;">
                                                                <label>DATE:</label>
                                                                <input type="text" name="date" class="form-control" id="date " style="width: 29%;">
                                                                <br>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 0px 30px; text-align: center;  font-size: 12px;">
                                                        <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                            <tr>
                                                                <td colspan=6 valign="middle" align="center" style="padding: 15px 5px;font-size:15px; font-weight: bold; ">
                                                                    2016 – NAPLAN TRIAL PAPERS – DELIVERED LATE FEBRUARY Year
                                                                </td>
                                                            </tr>
                                                            <tr valign="middle" align="center">
                                                                <td style="padding: 10px;font-size:14px; font-weight: bold; ">
                                                                    Year 7 NUMERACY
                                                                </td>
                                                                <td style="padding: 10px;font-size:14px; font-weight: bold; ">
                                                                    $60
                                                                </td>
                                                                <td style="padding: 10px;font-size:14px; font-weight: bold; ">
                                                                    &nbsp; &nbsp;
                                                                </td>
                                                                <td style="padding: 15px 5px;font-size:14px; font-weight: bold; ">
                                                                    Year 9 NUMERACY $60
                                                                </td>
                                                                <td style="padding: 10px;font-size:14px; font-weight: bold; ">
                                                                    $60
                                                                </td>
                                                                <td style="padding: 10px;font-size:14px; font-weight: bold; ">
                                                                    &nbsp; &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0px 0px 15px; text-align: center; font-family: sans-serif; font-size: 18px; font-weight: bold; line-height: 1.3; color: #000;">
                                                        BOOKS
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold;  font-size: 12px;">
                                                        <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                            <tr>
                                                                <td rowspan=6 style="width: 22%; padding:5px;font-weight: bold;">
                                                                    (New Edition) Learner Leader NUMERACY SUCCESS NAPLAN STYLE BOOKS
                                                                </td>
                                                                <td rowspan=6 style="width: 20%; padding:5px; font-weight: bold;">
                                                                    <strong style="font-size: 15px;">Seven </strong> Papers modelled on real examinations Answers and fully worked solutions
                                                                </td>
                                                                <td style="width: 40%; padding:5px;">
                                                                    NUMERACY BOOKS
                                                                </td>
                                                                <td style="width:15%; padding:5px;">
                                                                    Price per book
                                                                </td>
                                                                <td style="width: 10%; padding:5px;">
                                                                    QTY
                                                                </td>
                                                                <td style="width: 10%; padding:5px;">
                                                                    TOTAL
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="padding:5px;width: 50px; border-right:1px solid #000">
                                                                                Year 7
                                                                            </td>
                                                                            <td style="padding:5px; font-size: 15px">
                                                                                10 books or more
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    $25
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="padding:5px;width: 50px; border-right:1px solid #000">
                                                                                Year 7
                                                                            </td>
                                                                            <td style="padding:5px; ">
                                                                                Less than 10 books
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    $30
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan=4 height="5" style="padding:0px;font-size: 0; line-height: 0;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="padding:5px; width: 50px; border-right:1px solid #000">
                                                                                Year 9
                                                                            </td>
                                                                            <td style="padding:5px; font-size: 15px ">
                                                                                10 books or more
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    $25
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="padding:5px; width: 50px; border-right:1px solid #000">
                                                                                Year 9
                                                                            </td>
                                                                            <td style="padding:5px; ">
                                                                                Less than 10 books
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    $30
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                                <td style="padding:5px; ">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold;   font-size: 12px;">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td rowspan=3 style="width: 20%; padding:5px;font-weight: bold;">
                                                        NEW HSC MATHEMATICS EXTENSION 1
                                                    </td>
                                                    <td rowspan=3 style="width: 32%; padding:5px; font-weight: bold;">
                                                        <strong style="font-size: 15px;">15 trial HSC</strong> exam papers, each includes 10 multiple choice questions and four 15 mark questions with fully worked solutions. Price
                                                    </td>
                                                    <td style="width:40%; padding:5px;">
                                                        Price per book
                                                    </td>
                                                    <td style="width: 12%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 12%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">
                                                                    10 books or more
                                                                </td>
                                                                <td style="padding:5px; font-size: 15px">
                                                                    $35
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px;"></td>
                                                    <td style="padding:5px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">Less than 10 books
                                                                </td>
                                                                <td style="padding:5px; ">$40</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px; "></td>
                                                    <td style="padding:5px; "></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold;  font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td rowspan=3 style="width: 20%; padding:5px;font-weight: bold;">
                                                        NEW HSC MATHEMATICS
                                                    </td>
                                                    <td rowspan=3 style="width: 32%; padding:5px; font-weight: bold;">
                                                        <strong style="font-size: 15px;">14 trial HSC</strong> exam papers, each includes  10 multiple choice questions and six  15 mark questions with fully worked solutions.
                                                    </td>
                                                    <td style="width:40%; padding:5px;">
                                                        Price per book
                                                    </td>
                                                    <td style="width: 12%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 12%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">
                                                                    10 books <br>or more
                                                                </td>
                                                                <td style="padding:5px; font-size: 15px">
                                                                    $35
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px;"></td>
                                                    <td style="padding:5px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">Less than  <br> 10 books
                                                                </td>
                                                                <td style="padding:5px; ">$40</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px; "></td>
                                                    <td style="padding:5px; "></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold;  font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td rowspan=3 style="width: 20%; padding:5px;font-weight: bold;">
                                                        Measurement and Trigonometry For Years 9 and 10
                                                    </td>
                                                    <td style="width:40%; padding:5px;">
                                                        Price
                                                    </td>
                                                    <td style="width: 15%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 15%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">
                                                                    10 books or more
                                                                </td>
                                                                <td style="padding:5px; font-size: 15px">
                                                                    $35
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px;"></td>
                                                    <td style="padding:5px;"></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="padding:5px;width: 100px; border-right:1px solid #000">Less than 10 books
                                                                </td>
                                                                <td style="padding:5px; ">$40</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="padding:5px; "></td>
                                                    <td style="padding:5px; "></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        YEAR 12
                                                    </td>
                                                    <td style="width:50%; padding:5px; font-size: 16px; ">
                                                        YEAR 12 MATHEMATICS TRIALS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        PRICE
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Mathematics General 2
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left;  ">
                                                        Mathematics General 2 HSC Trial Exam with detailed solutions
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Mathematics
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left;  ">
                                                        Mathematics HSC Trial Exam with detailed solutions.
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Maths Ext 1
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left; ">
                                                        Mathematics Ext 1 HSC Trial Exam with detailed solutions.
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        2016 Maths Ext 2
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left; ">
                                                        Mathematics Ext 2 HSC Trial Exam with detailed solutions.
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px;">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        YEAR 11
                                                    </td>
                                                    <td style="width:50%; padding:5px; font-size: 16px; ">
                                                        YEAR 11 MATHEMATICS TRIALS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        PRICE
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Mathematics General 2
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left;  ">
                                                        Mathematics General 2 yearly preliminary with detailed solutions
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Mathematics
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left;  ">
                                                        Mathematics yearly preliminary Exam with detailed solutions.
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px; text-align: left; ">
                                                        2016 Maths Ext 1
                                                    </td>
                                                    <td style="width:50%; padding:5px; text-align: left;  ">
                                                        Mathematics Ext 1 yearly preliminary with detailed solutions.
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>


                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align: left; font-weight: bold; font-size: 12px;"><span style="text-decoration: underline;">PACKAGES OF PAST EXAMINATION PAPERS WITH FULLY WORKED SOLUTIONS. </span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 10%; padding:5px;text-align: left; ">
                                                        YEAR
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        TWO PAST  PAPERS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TICK
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        YEAR
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TWO PAST  PAPERS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TICK
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        YEAR
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TWO PAST  PAPERS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TICK
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        YEAR
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TWO PAST  PAPERS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TICK
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%; padding:5px;text-align: left; ">
                                                        12 Gen
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        11 Gen
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        10 5.3
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        9 5.3
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 10%; padding:5px;text-align: left; ">
                                                        12 Gen Ext 2
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        11 Maths
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        10 5.2
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        8
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan=3 style="width: 10%; padding:5px;text-align: left; ">
                                                    </td>

                                                    <td style="width: 10%; padding:5px;">
                                                        11 Ext 1
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        10 5.1
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        7
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $65
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align:center;font-weight: bold;font-size: 12px; "><span style=" font-size: 20px; display: block;">WORKED SOLUTIONS BY SAMI EL HOSRI </span>
                                            WORKED SOLUTIONS TO NEW SENIOR MATHEMATICS 2 UNIT, 3 UNIT AND 4 UNIT - J. B. FITZPATRICK
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        FITZPATRICK MATHS  (2 UNIT)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        FITZPATRICK MATHS
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Indices and Logarithms
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Quadratic Functions
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Differential Calculus
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Sequences and Series
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Exponential & Logarithmic Functions
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Application of Calculus
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Trigonometric (Circular) Functions
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Integral Calculus
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Geometrical Applications of differentiation
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Test Papers
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $40
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>



                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        FITZPATRICK <br>EXT  1  (3 UNIT)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        FITZPATRICK<br> EXT  1  (3 UNIT)
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Applied and Further Trigonometry
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Inverse Trigonometric Functions
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Trigonometric Equations
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Miscellaneous
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Plane Geometry
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Polynomials
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Permutations and Combinations.
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Calculus
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Binomial Theorem & Binomial Probability
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Applications of Calculus to the Physical World
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">

                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Test Papers
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $45
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>



                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 25px; text-align:center;font-weight: bold;font-size: 12px; "><span style=" font-size: 20px; display: block;">WORKED SOLUTIONS BY SAMI EL HOSRI  </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align: left; font-weight: bold;font-size: 12px; "><span style="text-decoration: underline;">WORKED SOLUTIONS TO THE FITZPATRICK 4 UNIT MATHEMATICS TEXTBOOK  </span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        FITZPATRICK EXT  2  (4 UNIT)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        FITZPATRICK EXT  2  (4 UNIT)
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Graphs
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Volumes
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Complex Nos.
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Mechanics
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Conics
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Polynomials
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Integration
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Adv 3U Topics
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Test Papers
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>





                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align: left; font-weight: bold;font-size: 12px; "><span style="text-decoration: underline;">WORKED SOLUTIONS TO THE CAMBRIDGE 4 UNIT MATHEMATICS TEXTBOOK (D & G ARNOLD)  </span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        EXT 2  (4 UNIT D G ARNOLD)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        EXT 2  (4 UNIT D G ARNOLD)
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Mechanics
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Volumes
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Harder 3U Topics
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Conics
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Specimen Papers
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Polynomials
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Integration
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Complex Nos
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Graphs
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $55
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>





                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align: left; font-weight: bold; font-size: 12px; "><span style="text-decoration: underline;">WORKED SOLUTIONS TO THE CAMBRIDGE YEAR 11  3 UNIT MATHEMATICS TEXTBOOK (BILL PENDER)  </span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" border="1px solid #000" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">

                                                        EXT 1 (YEAR 11   3 UNIT CAMBRIDGE)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>

                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        The Logarithmic Function
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Methods In Algebra
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Integration
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Methods In Algebra
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td colspan="4" style="padding:5px;text-align: left; ">
                                                        <p style="font-size: 18px; padding: 0px; margin: 10px 0px; text-align: center;">The Trigonometric Functions    <span style="color: red"> *** N E W  ***</span></p>
                                                    </td>

                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>




                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 0px 10px; text-align: left; font-weight: bold; font-size: 12px; "><span style="text-decoration: underline;">WORKED SOLUTIONS TO THE CAMBRIDGE YEAR 12  3 UNIT  MATHEMATICS TEXTBOOK (BILL PENDER)  </span></td>
                                    </tr>
                                    <tr>
                                        <td style="padding:0 0px 30px; font-family: sans-serif;font-weight: bold; font-size: 12px; ">
                                            <table border="1px solid #000" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        EXT 1 (YR 12  3 UNIT B PENDER)
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        EXT 1 (YEAR 12  3 UNIT B PENDER)
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        COST
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        QTY
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        TOTAL
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        The Inverse Trig Functions
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Further Calculus
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Further Trigonometry
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Rates and Finance
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Motion
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Circle Geometry
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        Polynomial Functions
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Euclidean Geometry
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 20%; padding:5px;text-align: left; ">
                                                        The Binomial Theorem
                                                    </td>
                                                    <td style="width:10%; padding:5px;  ">
                                                        $60
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 20%; padding:5px;text-align: left;">
                                                        Probability and Counting
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">
                                                        $75
                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>
                                                    <td style="width: 10%; padding:5px;">

                                                    </td>

                                                </tr>

                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign="top"  style="padding: 0px 0px 10px; text-align: right;font-size: 12px; font-weight: 600 ">
                                            <table  border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>

                                                    <td style=" margin:5px;">
                                                        <p style="margin: 5px 0px"> COST OF GOODS &nbsp; = &nbsp; $<input name="userName" class="form-control" id="name" style="width:10%; margin: 0px;text-align: center;" type="text"> </p>
                                                        <p style="margin: 5px 0px"> POSTAGE, PACKAGING AND INSURANCE &nbsp; = &nbsp; $<input name="userName" class="form-control" id="name" style="width:10%; margin: 0px" type="text"> 
                                                            <!-- <span style=" display: inline-block; width:10%; margin: 0px;text-align: center;">16.00</span>  --> </p>
                                                        <p style="margin: 5px 0px"> 10 % GST  &nbsp; = &nbsp; $<input name="userName" class="form-control" id="name" style="width:10%; margin: 0px;text-align: center;" type="text"> </p>
                                                        <p style="margin: 5px 0px"> TOTAL COST OF ORDER  &nbsp; = &nbsp; $<input name="userName" class="form-control" id="name" style="width:10%; margin: 0px;text-align: center;" type="text"> </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td valign="top"  style="padding: 0px 0px 10px; text-align: left;font-style: italic; font-size: 11px; ">
                                            <table  border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style=" padding:0px 3px; vertical-align: top;" >

                                                        <p style="margin: 5px 0px">PLEASE FIND ENCLOSED CHEQUE FOR $<input name="userName" class="form-control" id="name" style="width:8%; margin: 0px;text-align: center;" type="text">  PAYABLE TO:    S & G PUBLISHING AND COACHING ACADEMY  </p>
                                                        <p style="margin: 5px 0px" >ABN: 61 201 907 800 ,       P.O.BOX 237,   PUNCHBOWL  NSW  2196  </p>
                                                        <p style="margin: 5px 0px">TEL: 02 – 9707 3067 ,    FAX: 02 – 9707 3067       ,       EMAIL: <a href="mailto:sngpublishing@gmail.com">sngpublishing@gmail.com</a> </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td valign="top"  style="padding: 0px 0px 10px; text-align: left;font-size: 11px; font-weight: 600 ">
                                            <table  border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style=" width:72px; padding:5px; vertical-align: top;" ><span style="text-decoration: underline;">Please note :</span> </td>
                                                    <td style=" padding:5px;">Together with your order you will receive a “Tax Invoice” as required by the Taxation System. This “Tax Invoice” will also acknowledge payment.  </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"  style="padding: 0px 0px 30px; text-align: left;font-size: 11px; font-weight: 600 ">
                                            <table  border="0px" width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style=" text-align: center;  padding:5px; vertical-align: top;" >
                                                        <a href="#" class="clickbut" style="background: #007bed; border-radius: 5px;box-shadow: 0 2px 5px rgba(0, 0, 0, 0.48);color: #fff; display: inline-block; font-size: 18px; padding: 15px 30px; text-decoration: none;text-shadow: 0 1px 3px rgba(21, 13, 13, 0.6);text-transform: uppercase;" >click here</a>
                                                    </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <!-- Full Width, Fluid Column : END -->
                                </table>
                                <!-- Main Email Body : END -->
                            </td>
                        </tr>
                    </table>
                    <!-- Email wrapper : END -->
                </td>
            </tr>
        </table>
    </body>
</html>