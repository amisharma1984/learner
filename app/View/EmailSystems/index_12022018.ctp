<?php if(!empty($schools_names) && count($schools_names)> 0 ){?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">   
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  
 
  $( function() {
	  
    var availableTags = <?php echo json_encode($schools_names);?>;
     
    $( "#SchoolSearch" ).autocomplete({
      source: availableTags,
	  select: function( event, ui ) {  
		console.log(ui.item.value); 
		$( "#SchoolSearch" ).val(ui.item.value);
		$('#SchoolSearchForm').submit();
		}
    });
  } );
  </script>
  <?php }?>
  <style type="text/css">
    .tab_box{
        background: #37a3e8 none repeat scroll 0 0;
        color: #fff;
        height: 20px;
        padding: 7px;
        width: 20px;
    }
    .tab_box:hover{
        color:#000;
    }
    
    #droparea { width: 100%; background: #54b4eb; color: white; padding: 16px; margin-bottom: 16px; }
    #droparea.hover { background: #fff; }
    .ui-progressbar { height: 12px; }
    .ui-progressbar .ui-progressbar-value{width: 100%; margin: 0;}
    .expandable-header { cursor:pointer; margin:0px; height: 24px; }
    .expandable-header .cancel { float:right; display:inline-block; margin-left: 10px; }
/*    .progress {height: auto; float:right; display:inline-block; margin-left: 10px;color: white;font-size: 9px;margin: 3px 3px 4px 3px; font-weight: bold; background: transparent;}
    .progressarea { width:90%;float:left; display:inline-block; }
    .progressbar { margin-top: 3px; background: transparent;}*/
    .file-name { float:left; margin-right:5px; width: 100%; display:inline-block; }
    .expandable-body { display:none; }
    table td { border-bottom: none !important; }
    div.input label{margin: 5px 0;}
    div.checkbox{margin: 20px 0 0 0;}
    div.checkbox input[type="checkbox"]{ position:relative; float:none; margin-left:0;}
    .moxie-shim.moxie-shim-html5{
        width:auto !important; 
    }
    
    .progress-override{ height: 12px; margin: 6px 0 0 0;}
    .progress-bar-override{font-size: 9px; line-height: 12px;}
    .filters .form-control{margin-left: 0;}
      
</style>
<!--<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/jquery-ui-1.9.2.custom.min.js"></script>-->
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Email System'), "link" => array('controller' => 'email_systems', 'action' => 'index'))
        ), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">

            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-envelope"></i> Email Systems</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 mb-sm">
<!--                        <input type="button" class="btn btn-primary btn-sm" value="Upload Excel" style="margin-bottom: 10px;" onclick="window.location.href = '<?php //echo $this->Html->url(array('controller' => 'email_systems', 'action' => 'import')); ?>'" >-->
                        <input type="button" class="btn btn-primary btn-sm selectAllSchoolButton" value="Select All" style="margin-bottom: 10px;" >
                        <input type="button" class="btn btn-primary btn-sm" value="Attachment" style="margin-bottom: 10px;display:none" data-toggle="modal" data-target="#schoolEmailModal" >
                        <input type="button" class="btn btn-primary btn-sm" value="Add School" style="margin-bottom: 10px;display:none" data-toggle="modal" data-target="#newSchoolModal">
						<input type="button" class="btn btn-primary btn-sm" value="Compose Email" style="margin-bottom: 10px;display:none" data-toggle="modal" data-target="#ComposeEmailModal" >
						
						<input type="button" class="btn btn-primary btn-sm" onclick="openUrl('<?php echo $this->webroot.'admins/saved_customers';?>')" value="Sent Mail" style="margin-bottom: 10px;display:none" data-toggle="modal">
						
                    </div>
                </div>
                <div class="row filters">
                    <?php
                    $selectedLevel = (!empty($level))?explode(',', $level):'';
                    $selectedType = (!empty($type))?explode(',', $type):'';
                    $selectedState = (!empty($state))?explode(',', $state):'';
                    ?>
                    <?php echo $this->Form->create('School', array('url' => array('controller' => 'email_systems', 'action' => 'search'), 'class' => 'form-inline', 'role' => 'form' )); ?>
                    <?php
                        echo $this->Form->input('order', array('type' => 'hidden', 'label' => false, 'div' => false));
                        echo $this->Form->input('orderType', array('type' => 'hidden', 'label' => false, 'div' => false));
                    ?>

                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm">                            
                        <?php echo $this->Form->input('search', array('label' => __('Search: '), 'div' => false, 'value' => urldecode($search), 'class' => 'form-control','id'=>'SchoolSearch', 'style' => 'width:100%;')); ?>
                    </div>
                    <div class="form-group col-md-2 col-sm-2 col-xs-12 pr-none mb-sm">
                        <?php
                            //echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, -1 => 'All'), 'selected' => $limit, 'class' => 'chosen form-control', 'style' => 'width:100%;', 'label' => __('Show: '), 'div' => false));
                            echo $this->Form->input('limit', array('options' => array(10 => 10, 20 => 20, 40 => 40, 100 => 100, 500 => 500), 'selected' => $limit, 'class' => 'chosen form-control', 'style' => 'width:100%;', 'label' => __('Show: '), 'div' => false));
                        ?>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 pr-none mb-sm">
                        <div class="checkbox" style="margin-top:0;"> 
                            <label style="display:block;">&nbsp;</label>
                            <label for="SchoolInformationIsprevious" class="mb-xs pt-sm"> 
                                <?php echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'checked' => $isprevious)); ?>Is previous year customer?
                            </label> 
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm" >
                        <?php echo $this->Form->input('SchoolInformation.state', array('type' => 'select', 'label' => 'State: ', 'div' => false, 'data-placeholder' => '- Select State -', 'selected' => $selectedState, 'class' => 'form-control chosen', 'style' => 'width:100%;', 'options' => $schoolStateOptions, 'multiple' => true)); ?>
                    </div>
                    
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 pr-none mb-sm">
                        <?php echo $this->Form->input('SchoolInformation.level', array('type' => 'select', 'label' => 'Level: ', 'div' => false, 'data-placeholder' => '- Select Level -', 'selected' => $selectedLevel, 'class' => 'form-control chosen', 'style' => 'width:100%;', 'options' => $schoolLevelOptions, 'multiple' => true)); ?>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12 mb-sm" >
                        <?php echo $this->Form->input('SchoolInformation.type', array('type' => 'select', 'label' => 'Type: ', 'div' => false, 'data-placeholder' => '- Select Type -', 'selected' => $selectedType, 'class' => 'form-control chosen', 'style' => 'width:100%;', 'options' => $schoolTypeOptions, 'multiple' => true)); ?>
                    </div>

                    <?php echo $this->Form->end(); ?>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable dataTable">
                        <thead>
                            <tr>
                                <th>School</th>
                                <th>Email</th>
                                <th>Level</th>
                                <th>Phone</th>
    <!--                            <th>Manager</th>
                                <th style="max-width:192px !important;">Email</th>
                                <th>State / Province</th>
                                <th>Level</th>
                                <th>Type</th>
                                <th>Phone</th>
                                <th>Previous customer</th>
                                <th>Created</th>-->

    <!--                            <th>Status</th>-->
                                <th>Actions</th>
                                <!--<th>Delete</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(!empty($school)){
                                foreach ($school As $key => $val) {
                                ?>
                                <tr>
                                    <td> 
                                        <div class="checkbox" style="margin-top:0;"> <label class="pl-xs" for="schoolName<?php echo $val['School']['id']; ?>"> <input class="select-school" type="checkbox" id="schoolName<?php echo $val['School']['id']; ?>" value="<?php echo $val['School']['id']; ?>"> <?php echo $val['School']['name']; ?> </label> </div>
                                    </td>
									<td><a href="javascript:void(0)" onclick="$('#schoolName<?php echo $val['School']['id']; ?>').prop('checked',true);$(this).next('.write_mail_btn_cust').trigger('click');"><?php echo $val['School']['email']; ?></a>
									
									
									 <a class="btn-warning btn-sm write_mail_btn_cust" title="Send Mail" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;display:none" data-action-type="login" data-toggle="modal" data-target="#schoolEmailModal" data-school-name="<?php echo $val['School']['name']; ?>" data-school="<?php echo $val['School']['id']; ?>" >
											Write Mail
                                        </a>
										
										
									</td>
									<td><?php echo $val['SchoolInformation']['level']; ?></td>
									<td><?php echo $val['SchoolInformation']['phone']; ?></td>
    <!--                                <td><?php //echo $val['School']['manager_name']; ?></td>
                                    <td style="max-width:192px !important; word-wrap: break-word; word-break: break-all;"><?php echo $val['School']['email']; ?></td>
                                    <td><?php echo $val['SchoolInformation']['state']; ?></td>
                                    <td><?php echo $val['SchoolInformation']['level']; ?></td>
                                    <td><?php echo $val['SchoolInformation']['type']; ?></td>
                                    <td><?php echo $val['SchoolInformation']['phone']; ?></td>
                                    <td><?php echo (!empty($val['SchoolInformation']['isprevious']))?'Yes':'No'; ?></td>
                                    <td class="center"><?php echo date("jS F, Y", strtotime($val['School']['created'])); ?></td>
									-->
    <!--                                <td class="center">

                                        <?php //if(!empty($val['School']['payment_status'])){ ?>
                                            <span class="label-success label label-default" style=" margin-right: 5px; ">
                                                Active
                                            </span>
                                        <?php //}else{ ?>    
                                            <span class="label-default label label-danger" style=" margin-right: 5px; ">
                                                Inactive
                                            </span>
                                        <?php //} ?>

                                    </td>-->
                                    <td class="center">
                                        <a class="btn-success view-school btn-sm" title="View" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#schoolViewModal" data-school="<?php echo $val['School']['id']; ?>">
                                            <span class="glyphicon glyphicon-zoom-in icon-white"></span>
                                        </a>
                                        <a class="btn-info btn-sm" title="Edit" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#newSchoolModal" data-school="<?php echo $val['School']['id']; ?>" data-schoolInfo="<?php echo $val['SchoolInformation']['id']; ?>">
                                            <span class="glyphicon glyphicon-edit icon-white"></span>
                                        </a>
                                        <a class="btn-warning btn-sm write_mail_btn_cust" title="Send Mail" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-action-type="login" data-toggle="modal" data-target="#schoolEmailModal" data-school-name="<?php echo $val['School']['name']; ?>" data-school="<?php echo $val['School']['id']; ?>">
                                            <!--<span class="glyphicon glyphicon-share-alt icon-white"></span>&nbsp;<span class="glyphicon glyphicon-envelope icon-white"></span>-->
											Write Mail
                                        </a>
										
										<a class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#deleteSchoolModal" data-school-name="<?php echo $val['School']['name']; ?>" data-school="<?php echo $val['School']['id']; ?>">
                                            <span class="glyphicon glyphicon-trash icon-white"></span>
                                        </a>
										
                                    </td>
                                    <!--<td class="center">
                                        <a class="btn-danger btn-sm" title="Delete" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#deleteSchoolModal" data-school-name="<?php echo $val['School']['name']; ?>" data-school="<?php echo $val['School']['id']; ?>">
                                            <span class="glyphicon glyphicon-trash icon-white"></span>
                                        </a>
                                    </td>-->
                                </tr>

                            <?php 
                                }
                            }else{ 
                            ?>
                            <tr class="odd"><td valign="top" colspan="10" class="dataTables_empty">No record found</td></tr>    
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->
            </div>
        </div>
    </div>
</div>
<!--/span-->

<!-- Start Add And edit School Modal-->
<?php echo $this->element('school/modal/add-edit', array('schoolLevelOptions' => $schoolLevelOptions, 'schoolTypeOptions' => $schoolTypeOptions)); ?>
<!-- End Add Edit School Modal-->

<!-- Start School View Modal-->
<div class="modal fade" id="schoolViewModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School View Modal -->

<!-- Start Send Email to School Modal-->
<div class="modal fade" id="schoolEmailModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail to School</h4>
            </div>
            <div class="modal-body" id="emailhtml">
                <div class="row" id="sendEmailSuccess" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> Email has sent to the selected users. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'email_systems', 'action' => 'send_mail'),
                    'id' => 'sendEmailsForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <?php echo $this->Form->input('Email.fromId', array('type' => 'hidden', 'value' => (!empty($admindata['Admin']['id']))?$admindata['Admin']['id']:'')); ?>
                <?php echo $this->Form->input('Email.attachmentMask', array('type' => 'hidden')); ?>
                <?php echo $this->Form->input('Email.toIds', array('type' => 'hidden')); ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
				
				
				 <div class="form-group">
                    <label for="EmailFromemailId" class=" col-md-2 col-sm-2 col-xs-12 control-label">From</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.fromemail_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Email -', 'class' => 'form-control','required' => true, 'style' => 'width:100%;', 'options' => $fromEmailList)); ?>
                    </div>
                </div>
				
				
				
                <div class="form-group">
                    <label for="EmailSubject" class=" col-md-2 col-sm-2 col-xs-12 control-label"><sup style="color:red;">*</sup>Subject</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.subject', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="EmailEmailTemplateId" class=" col-md-2 col-sm-2 col-xs-12 control-label">Mail Template</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.email_template_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Template -', 'class' => 'form-control', 'style' => 'width:100%;', 'options' => $emailTemplates)); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="EmailMessage" class=" col-md-2 col-sm-2 col-xs-12 control-label">Message</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">                        
                        <?php echo $this->Form->input('Email.message', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-sm-2 col-xs-12 control-label">Attach files</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <a id="pickfiles" href="#" class="btn btn-primary" ><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;CHOOSE YOUR FILES</a>
                    </div>
                </div>
                <div class="form-group mb-none">
                    <label class="col-md-2 col-sm-2 col-xs-12 control-label">&nbsp;</label>
                    <div class="col-md-10 col-sm-10 col-xs-12" id="fileAttachments" style="max-height:140px; overflow-y: scroll; overflow-x: hidden;">
                        
                    </div>
                </div>
<!--                <div class="form-group">
                    <div class="col-md-offset-1 col-md-11 col-sm-11 col-xs-12">
                        <div id="upload-container">
                            <div id="droparea">
                                <div style="text-align: center; border: 3px dashed white; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; padding: 16px 0 16px 0;">
                                    <span id="text-to-remove">DRAG &amp; DROP FILES HERE<br /> OR</span>
                                    <div style="height: 8px;"></div>
                                    <a id="pickfiles" href="#" class="button" style="color: #fff;text-decoration:none;">CHOOSE YOUR FILES</a><br />
                                    <div style="height: 8px;"></div>
                                </div>
                            </div>

                            <div id="filelist"></div>
                        </div>
                    </div>
                </div>-->

                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="button" class="btn btn-primary" id="sendEmailsButton" value="Send Mail" style="" >
                <!--        <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Send Email to School Modal -->

<!-- Start School delete Modal-->
<div class="modal fade" id="deleteSchoolModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Delete school</h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary schoolDeleteConfirm" data-isChecked="false">Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School Delete Modal -->

<!-- Start School Login Credentials Modal-->
<div class="modal fade" id="loginCredentialsModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <p></p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary schoolCredentialsConfirm" data-isChecked="false">Yes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School Login Credentials Modal -->





<!-- Start Send Email to any one-->
<div class="modal fade" id="ComposeEmailModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail to Individual</h4>
            </div>
            <div class="modal-body" id="emailhtml">
                <div class="row" id="sendComposeEmailSuccess" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> Email has been sent to users. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'email_systems', 'action' => 'send_mail_to_any_one'),
                    'id' => 'composeEmailForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <?php echo $this->Form->input('Email.attachmentMask1', array('type' => 'hidden')); ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                
				
				 <div class="form-group">
                    <label for="EmailEmailTemplateId" class=" col-md-2 col-sm-2 col-xs-12 control-label">From</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.from_email_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Email -', 'class' => 'form-control','required' => true, 'style' => 'width:100%;', 'options' => $fromEmailList)); ?>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="EmailSubject" class=" col-md-2 col-sm-2 col-xs-12 control-label"><sup style="color:red;">*</sup>To</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.to_email', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="EmailSubject" class=" col-md-2 col-sm-2 col-xs-12 control-label"><sup style="color:red;">*</sup>Subject</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.subject', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="EmailEmailTemplateId" class=" col-md-2 col-sm-2 col-xs-12 control-label">Mail Template</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <?php echo $this->Form->input('Email.email_template_id', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Template -', 'class' => 'form-control', 'style' => 'width:100%;', 'options' => $emailTemplates)); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="EmailContent" class=" col-md-2 col-sm-2 col-xs-12 control-label">Message</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">                        
                        <?php echo $this->Form->input('Email.content', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-sm-2 col-xs-12 control-label">Attach files</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <a id="pickfiles1" href="#" class="btn btn-primary" ><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;CHOOSE YOUR FILES</a>
                    </div>
                </div>
                <div class="form-group mb-none">
                    <label class="col-md-2 col-sm-2 col-xs-12 control-label">&nbsp;</label>
                    <div class="col-md-10 col-sm-10 col-xs-12" id="fileAttachments1" style="max-height:140px; overflow-y: scroll; overflow-x: hidden;">
                        
                    </div>
                </div>
<!--                <div class="form-group">
                    <div class="col-md-offset-1 col-md-11 col-sm-11 col-xs-12">
                        <div id="upload-container">
                            <div id="droparea">
                                <div style="text-align: center; border: 3px dashed white; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; padding: 16px 0 16px 0;">
                                    <span id="text-to-remove">DRAG &amp; DROP FILES HERE<br /> OR</span>
                                    <div style="height: 8px;"></div>
                                    <a id="pickfiles" href="#" class="button" style="color: #fff;text-decoration:none;">CHOOSE YOUR FILES</a><br />
                                    <div style="height: 8px;"></div>
                                </div>
                            </div>

                            <div id="filelist"></div>
                        </div>
                    </div>
                </div>-->

                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="button" class="btn btn-primary" id="composeEmailsButton" value="Send Mail" style="" >
               
            </div>
        </div>
    </div>
</div>
<!-- End compose Email to anyone -->








<!-- Start school View Template -->
<?php echo $this->element('school/template/view'); ?>
<!-- End school View Template -->

<script type="text/template" id="attachedFileTemplate">
    <div class="row attachment-wrapper" id="rowId<%- fileData.id %>">
        <div class="col-md-6 col-sm-6 col-xs-6 pr-none" id="fileDetails<%- fileData.id %>">
            <a class="dO" id=":ud" href="javascript:void(0);" target="_blank">
                <div class="vI" id="fileName<%- fileData.id %>" title="<%- fileData.name %>"><%- fileData.name %></div> 
                <div class="vJ" id="fileSize<%- fileData.id %>">(<%- plupload.formatSize(fileData.size) %>)</div>
            </a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 pr-none" id="progressWrapper<%- fileData.id %>">
            <div class="progress progress-override" id="ProgressBar<%- fileData.id %>">
                <div class="progress-bar progress-bar-override" id="Progress<%- fileData.id %>" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                     aria-valuemax="100" style="width: 0%;">
                    0%
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2 pr-none">
            <div role="button" id="cancleAttachment<%- fileData.id %>" class="vq" tabindex="-1"></div>
        </div>
    </div>
    
</script>

<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/tinymce/tinymce.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/school-management.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/email-systems.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Router::url('/', true); ?>css/jquery-ui/jquery-ui-1.9.2.custom.css" />
<script src="<?php echo Router::url('/', true); ?>js/plupload/plupload.full.min.js"></script>

<script type="text/javascript">
    function openUrl(url){
            var win = window.open(url);
    }
	</script>

<script type="text/javascript">
    $(function () {
        
        if($('.chosen').length){
            $('.chosen').chosen();
        }
        
        $('select#SchoolLimit, select#SchoolInformationState, select#SchoolInformationLevel, select#SchoolInformationType, input#SchoolSearch, input#SchoolOrder, input#SchoolInformationIsprevious', '.filters').bind("change", changePaginator); // orange
        
        $('table.dataTable thead .table-heading').click(function(){
//            var parentScop = this;
//            var orderName = $(this).attr('data-order');
//            var orderType = ($(this).attr('data-order-type') == 'asc')?'desc':'asc';
//            
//            $('#SchoolOrder').val(orderName);
//            $('#SchoolOrderType').val(orderType);
//            
//            $('table.dataTable thead .table-heading').each(function(index, element){
//                $(element).removeClass('sorting_'+$(parentScop).attr('data-order-type')).addClass('sorting');
//            });
//            $(this).removeClass('sorting').addClass('sorting_'+orderType);
//            $(this).attr('data-order-type', orderType);
//            changePaginator();
            //console.log('orderName : '+orderName+'\n'+'orderType : '+orderType);
        });
        
    });
    
    function clearAllFilters() {
        $('input#SchoolSearch', '.filters').val('');
        $('select#SchoolLimit', '.filters').val('');
        changePaginator();
    }
    function changePaginator() {
        var passed = $.parseJSON('<?php echo json_encode($this->passedArgs) ?>');
        var base = '<?php echo $this->Html->url('/', true) ?>';

        var communitySlug = '<?php echo Configure::read('Community.slug') ?>/';
        if (communitySlug == '/') communitySlug = '';

        var newurl = base+'email_systems/index';
        
//        if (passed.page !== undefined)
//            newurl += '/page:'+passed.page;
        
        var searchData = $('input#SchoolSearch', '.filters').val();
//        if (passed.search !== undefined){
//            newurl += '/search:'+passed.search;
//        }else 
        if(searchData){
            newurl += '/search:'+encodeURI(searchData);
        }
        
        var state = $('select#SchoolInformationState', '.filters').val();
        if (state)
            newurl += '/state:'+$('select#SchoolInformationState', '.filters').val();
        
        var level = $('select#SchoolInformationLevel', '.filters').val();
        if (level)
            newurl += '/level:'+$('select#SchoolInformationLevel', '.filters').val();
        
        if ($('select#SchoolInformationType', '.filters').val() != null && $('select#SchoolInformationType', '.filters').val() != '')
            newurl += '/type:'+$('select#SchoolInformationType', '.filters').val();

        if ($('select#SchoolLimit', '.filters').val() != '')
            newurl += '/limit:'+$('select#SchoolLimit', '.filters').val();
        
        if($('input#SchoolInformationIsprevious', '.filters').is(':checked'))
            newurl += '/previous:' + $('input#SchoolInformationIsprevious', '.filters').val();
        
        if ($('input#SchoolOrder', '.filters').val() != '')
            newurl += '/order:'+$('input#SchoolOrder', '.filters').val();
        if ($('input#SchoolOrderType', '.filters').val() != '')
            newurl += '/orderType:'+$('input#SchoolOrderType', '.filters').val();
        //console.log(newurl);
        window.location = newurl;
    }
    
 /* Full Settings of TinyMCE EDITOR */
//    tinymce.init({
//        selector: '.textEditor',
//        autoresize_min_height: 120,
//        autoresize_max_height: 250,
//        theme: 'modern',
//        plugins: [
//            'autoresize advlist autolink lists link image charmap print preview hr anchor pagebreak',
//            'searchreplace wordcount visualblocks visualchars code fullscreen',
//            'insertdatetime media nonbreaking save table contextmenu directionality',
//            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
//        ],
//        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
//        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
//        image_advtab: true,
//        templates: [
//            { title: 'Test template 1', content: 'Test 1' },
//            { title: 'Test template 2', content: 'Test 2' }
//        ],
//        content_css: [
//            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
//            '//www.tinymce.com/css/codepen.min.css',
//            BASEURL + 'css/custom-editor-style.css'
//        ]
//    });

    tinymce.init({
        selector: '.textEditor',
        valid_elements : '*[*]',
        valid_children : '+body[style], +body[script]',
        autoresize_min_height: 250,
        autoresize_max_height: 550,
        theme: 'modern',
        element_format : 'html',
        plugins: [
            'autoresize advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: BASEURL + 'app/uploadEditorImageFiles',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
    
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
                var file = this.files[0];
      
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);
      
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
            };
    
            input.click();
        },
        relative_urls: false,
        convert_urls: false,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css',
            BASEURL + 'css/custom-editor-style.css'
        ]
    });
    
    
    var filecount = 0;
    // Initialize the widget when the DOM is ready
    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles', // you can pass an id...
        //container: 'upload-container', // ... or DOM Element itself
        //drop_element : "droparea",
        max_file_size : '<?php echo ini_get("upload_max_filesize") ?>',
        url : "<?php echo $this->Html->url(array('plugin' => 'uploader', 'controller'=> 'uploader', 'action'=>'upload'), true); ?>",
        flash_swf_url : '<?php echo Router::url('/', true); ?>js/plupload/Moxie.swf',
        silverlight_xap_url : '<?php echo Router::url('/', true); ?>js/plupload/Moxie.xap',
        filters : [
            { title : "<?php echo __("Image files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.image')) ?>"},
            { title : "<?php echo __("Video files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.video')) ?>"},
            { title : "<?php echo __("Document files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.document.all')) ?>"}
        ],
        init: {
            PostInit: function() {

            },
            BeforeUpload: function(up, file) {
                /* Called right before the upload for a given file starts, can be used to cancel it if required */
                uploader.settings.multipart_params = {
                    attachmentMask: ($('#EmailAttachmentMask').val() !== undefined)?$('#EmailAttachmentMask').val():''
                };
            },
            FilesAdded: function(up, files) {

                $.each(files, function(i, file) {

                    file['attachmentMask'] = ($('#EmailAttachmentMask').val() !== undefined)?$('#EmailAttachmentMask').val():'';

                    _.templateSettings.variable = "fileData";                 
                    var template = _.template(
                        $('#attachedFileTemplate').html()
                    );
                    $('#schoolEmailModal').find('.modal-body').find('#fileAttachments').prepend(template(file));

                    $("#cancleAttachment" + file.id ).click(function () {
                        var attachment = $(this).data('attachment');
                        if(attachment !== undefined){
                            var jqxhrDeleteAttachment = $.ajax({
                                type: "POST",
                                url: BASEURL + 'email_systems/delete_attachment',
                                data:{
                                    attachmentId : attachment
                                },
                                success: function (response) {
                                    //console.log(response);
                                    if(response != ''){
                                        $('#rowId' + file.id).fadeOut().remove();
                                        if($('#fileAttachments').find('.attachment-wrapper').length == 0){
                                            $('#EmailAttachmentMask').val('');
                                        }
                                    }
                                }
                            });
                        }else{
                            $('#rowId' + file.id).fadeOut().remove();
                        }
                    });
                });
                up.start();
                up.refresh(); // Reposition Flash/Silverlight
            },
            UploadProgress: function(up, file) {

                $('#fileAttachments').find('#Progress'+file.id).attr('aria-valuenow', file.percent).css('width', file.percent+'%').html(file.percent + "%");
                if($('#fileAttachments').find('#Progress'+file.id).attr('aria-valuenow') == '100'){
                    $('#fileAttachments').find('#progressWrapper'+file.id).remove();
                    $('#fileAttachments').find('#fileDetails'+file.id).attr('class', '').addClass('col-md-10 col-sm-10 col-xs-10 pr-none');
                }
            },
            Error: function(up, err) {
                if (err.code == -600) { // Size
                        alert("You have selected files that exceed the <?php echo ini_get('upload_max_filesize')?> MB size limit.");
                } else if (err.code == -601) { // Extension
                        alert("You have selected file types that are not supported, please select this type of file jpg,png,gif,bmp,jpeg,psd,pdf,ppsx,ppt,pptx,avi,mp4,ogg,webm,3gp,mov,mpg,ogv.");
                } else {
                        console.log(err.code);
                        console.log(err.message);
                }
                //console.log(err);
                up.refresh();
            }, 
            FileUploaded: function(up, file, response) {

                var obj = $.parseJSON(response.response);
                $('#fileAttachments').find('#cancleAttachment'+file.id).attr('data-attachment', obj.result.id);
                $('#EmailAttachmentMask').val(obj.result.maskId);
                var out = '<input type="hidden" name="data[School][fileid'+filecount+']" id="fileid'+filecount+'" value="'+obj.result.id+'" />';
                $("#" + file.id).find(".expandable-body").html(out);
                filecount++;

            },  
            UploadComplete: function(uploader, files){
                $('#SchoolTotalfiles').val(filecount);

            }                   
        }
    });

    uploader.init();
	<!-- 26-02-2018 -->
	var filecount1 = 0;
    // Initialize the widget when the DOM is ready
    var uploader1 = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',
        browse_button : 'pickfiles1', // you can pass an id...
        //container: 'upload-container', // ... or DOM Element itself
        //drop_element : "droparea",
        max_file_size : '<?php echo ini_get("upload_max_filesize") ?>',
        url : "<?php echo $this->Html->url(array('plugin' => 'uploader', 'controller'=> 'uploader', 'action'=>'upload'), true); ?>",
        flash_swf_url : '<?php echo Router::url('/', true); ?>js/plupload/Moxie.swf',
        silverlight_xap_url : '<?php echo Router::url('/', true); ?>js/plupload/Moxie.xap',
        filters : [
            { title : "<?php echo __("Image files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.image')) ?>"},
            { title : "<?php echo __("Video files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.video')) ?>"},
            { title : "<?php echo __("Document files") ?>", extensions : "<?php echo implode(",", Configure::read('ue.document.all')) ?>"}
        ],
        init: {
            PostInit: function() {

            },
            BeforeUpload: function(up, file) {
                /* Called right before the upload for a given file starts, can be used to cancel it if required */
                uploader1.settings.multipart_params = {
                    attachmentMask: ($('#EmailAttachmentMask1').val() !== undefined)?$('#EmailAttachmentMask1').val():''
                };
            },
            FilesAdded: function(up, files) {

                $.each(files, function(i, file) {

                    file['attachmentMask'] = ($('#EmailAttachmentMask1').val() !== undefined)?$('#EmailAttachmentMask1').val():'';

                    _.templateSettings.variable = "fileData";                 
                    var template = _.template(
                        $('#attachedFileTemplate').html()
                    );
                    $('#ComposeEmailModal').find('.modal-body').find('#fileAttachments1').prepend(template(file));

                    $("#cancleAttachment" + file.id ).click(function () {
                        var attachment = $(this).data('attachment');
                        if(attachment !== undefined){
                            var jqxhrDeleteAttachment = $.ajax({
                                type: "POST",
                                url: BASEURL + 'email_systems/delete_attachment',
                                data:{
                                    attachmentId : attachment
                                },
                                success: function (response) {
                                    //console.log(response);
                                    if(response != ''){
                                        $('#rowId' + file.id).fadeOut().remove();
                                        if($('#fileAttachments1').find('.attachment-wrapper').length == 0){
                                            $('#EmailAttachmentMask1').val('');
                                        }
                                    }
                                }
                            });
                        }else{
                            $('#rowId' + file.id).fadeOut().remove();
                        }
                    });
                });
                up.start();
                up.refresh(); // Reposition Flash/Silverlight
            },
            UploadProgress: function(up, file) {

                $('#fileAttachments1').find('#Progress'+file.id).attr('aria-valuenow', file.percent).css('width', file.percent+'%').html(file.percent + "%");
                if($('#fileAttachments1').find('#Progress'+file.id).attr('aria-valuenow') == '100'){
                    $('#fileAttachments1').find('#progressWrapper'+file.id).remove();
                    $('#fileAttachments1').find('#fileDetails'+file.id).attr('class', '').addClass('col-md-10 col-sm-10 col-xs-10 pr-none');
                }
            },
            Error: function(up, err) {
                if (err.code == -600) { // Size
                        alert("You have selected files that exceed the <?php echo ini_get('upload_max_filesize')?> MB size limit.");
                } else if (err.code == -601) { // Extension
                        alert("You have selected file types that are not supported, please select this type of file jpg,png,gif,bmp,jpeg,psd,pdf,ppsx,ppt,pptx,avi,mp4,ogg,webm,3gp,mov,mpg,ogv.");
                } else {
                        console.log(err.code);
                        console.log(err.message);
                }
                //console.log(err);
                up.refresh();
            }, 
            FileUploaded: function(up, file, response) {

                var obj = $.parseJSON(response.response);
                $('#fileAttachments1').find('#cancleAttachment'+file.id).attr('data-attachment', obj.result.id);
                $('#EmailAttachmentMask1').val(obj.result.maskId);
                var out = '<input type="hidden" name="data[Email][fileid'+filecount1+']" id="fileid'+filecount1+'" value="'+obj.result.id+'" />';
                $("#" + file.id).find(".expandable-body").html(out);
                filecount1++;

            },  
            UploadComplete: function(uploader1, files){
                $('#SchoolTotalfiles').val(filecount1);

            }                   
        }
    });

    uploader1.init();
	
	<!-- 26-02-2018 -->
	
	
	
    function truncate(str) {
        var l = 30;
        if (str.length > l) {
            str = str.substring(0, l) + " ...";
        }
        return str;
    }
</script>   