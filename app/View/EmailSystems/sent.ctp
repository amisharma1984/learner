<style type="text/css">
    .tab_box{
        background: #37a3e8 none repeat scroll 0 0;
        color: #fff;
        height: 20px;
        padding: 7px;
        width: 20px;
    }
    .tab_box:hover{
        color:#000;
    }
    
    #droparea { width: 100%; background: #54b4eb; color: white; padding: 16px; margin-bottom: 16px; }
    #droparea.hover { background: #fff; }
    .ui-progressbar { height: 12px; }
    .ui-progressbar .ui-progressbar-value{width: 100%; margin: 0;}
    .expandable-header { cursor:pointer; margin:0px; height: 24px; }
    .expandable-header .cancel { float:right; display:inline-block; margin-left: 10px; }
/*    .progress {height: auto; float:right; display:inline-block; margin-left: 10px;color: white;font-size: 9px;margin: 3px 3px 4px 3px; font-weight: bold; background: transparent;}
    .progressarea { width:90%;float:left; display:inline-block; }
    .progressbar { margin-top: 3px; background: transparent;}*/
    .file-name { float:left; margin-right:5px; width: 100%; display:inline-block; }
    .expandable-body { display:none; }
    table td { border-bottom: none !important; }
    div.input label{margin: 5px 0;}
    div.checkbox{margin: 20px 0 0 0;}
    div.checkbox input[type="checkbox"]{ position:relative; float:none; margin-left:0;}
    .moxie-shim.moxie-shim-html5{
        width:auto !important; 
    }
    
    .progress-override{ height: 12px; margin: 6px 0 0 0;}
    .progress-bar-override{font-size: 9px; line-height: 12px;}
    .filters .form-control{margin-left: 0;}
    .modal-content {
    background-clip: padding-box;
    background-color: #ffffff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 6px;
    box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
    outline: 0 none;
    position: relative;
    width: 685px !important;
} 
</style>
<script type="text/javascript" src="<?php echo Router::url('/', true); ?>js/jquery-ui-1.9.2.custom.min.js"></script>
<?php
echo $this->Breadcrumb->show(array(
    array("text" => __('Email System'), "link" => array( 'controller' => 'email_systems', 'action' => 'sent'))), true);
?>
<?php echo $this->Session->flash(); ?>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Sent Email</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                
                <div class="table-responsive">                
                  
                <table class="table table-striped table-bordered bootstrap-datatable dataTable" id="schoolListData">
                    <thead>
                        <tr>
                            <th class="table-heading ">SL</th>
                            <th class="table-heading <?php //echo ($order == 'price')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="price" data-order-type="<?php //echo ($order == 'price')?strtolower($orderType):'asc'; ?>">To</th>
                            <th class="table-heading <?php //echo ($order == 'exam_type')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="exam_type" data-order-type="<?php //echo ($order == 'exam_type')?strtolower($orderType):'asc'; ?>">From</th>
                            <th class="table-heading <?php //echo ($order == 'category')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="category" data-order-type="<?php //echo ($order == 'category')?strtolower($orderType):'asc'; ?>">Subject</th>
							<th class="table-heading <?php //echo ($order == 'category')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="category" data-order-type="<?php //echo ($order == 'category')?strtolower($orderType):'asc'; ?>">Date</th>
                            <th class="table-heading <?php //echo ($order == 'created')?'sorting_'.strtolower($orderType):'sorting'; ?>" data-order="created" data-order-type="<?php //echo ($order == 'created')?strtolower($orderType):'asc'; ?>">Attachment</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						
                        if(!empty($emailSentDetails)){
							//pr($emailSentDetails);exit;
                            $examCount = 1;
                            $rowClass = 'even';
                            $securityKey = Configure::read('Security.salt');
                            foreach ($emailSentDetails as $key => $email){
								//pr($email);exit;
                                $rowClass = (($examCount % 2) != 0)?'odd':'even';
                                
                                $attachment = ($email['Email']['attachment_mask'] !='')?'Yes' :'No';
                                
                        ?>
                        <tr>
                            <td>
                                <div class="checkbox" style="margin-top:0;"> <label for="">  <?php echo $examCount; ?> </label> </div>
                            </td>
                            <td><?php echo  $email['School']['email']; ?></td>
                            <td><?php echo 'admin@gmail.com'; ?></td>
                            <td><?php echo $email['Email']['subject']; ?></td>
							<td class="center"><?php echo date("jS F, Y h:i:s A", strtotime($email['EmailDelivery']['created'])); ?></td>
                            <td class="center"><?php echo  $attachment; ?></td>
                            <td class="center">
<!--                                <a class="btn-success previewExamination btn-sm" title="Preview Exam" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-exam="<?php echo $encryptedResult; ?>">
                                    <span class="glyphicon glyphicon-zoom-in icon-white"></span> 
                                </a>-->
								<a class="btn-success view-school btn-sm" title="View" style="margin:0 0px 5px 0; text-decoration: none; cursor: pointer; display: inline-block;" data-toggle="modal" data-target="#schoolViewModal" data-school="<?php echo trim($email['Email']['id']) ; ?>">
                                            <span class="glyphicon glyphicon-zoom-in icon-white"></span>
                                        </a>
                               
                               
                            </td>
                        </tr>
						
						
						
						
						
						
                        <?php
                                $examCount++;
                            }
                        }else{
                        ?>
                        <tr class="odd">
                            <td valign="top" colspan="6" class="dataTables_empty">
                                <?php
                                    echo 'No email found';
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                
                <!-- Start Pagination -->
                <?php echo $this->element('Commons/Pagination/Bottom'); ?>    
                <!-- End Pagination -->
                    
                </div>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->



<?php echo $this->element('JS/external-scripts'); ?>
<script src="<?php echo Router::url('/', true); ?>js/underscore-min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.form.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/tinymce/tinymce.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/date.format.js"></script>



<!-- Start School View Modal-->
<div class="modal fade" id="schoolViewModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Email Sent</h4>
      </div>
	  <div class="modal-body">
	  <div>
		 <label >Email Subject</label>
		<div class="modal-subject" >
	  
		</div>
	  </div>
	  <div>
	   <label >Email Content</label>
      <div class="modal-message">
        
      </div>
	  </div>
	  <div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End School View Modal -->
<!-- End School Login Credentials Modal -->
</div>
</div>
<script type="text/javascript">
$(function () {
$('#schoolViewModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('school') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        
        var jqxhrViewSchool = $.ajax({
            type: "POST",
            url: BASEURL + "email_systems/emailcontect",
            data: {
                emailId: recipient
            },
            success: function (response) {
                console.log(response);
                if(response != ''){
                    
                    var result = JSON.parse(response);
					console.log(result);
					// var html = $(result.Email.message).html(); 
                    modal.find('.modal-subject').text(result.Email.subject);
					 modal.find('.modal-message').html(result.Email.message);
                    _.templateSettings.variable = "data";
                                        
                    var template = _.template(
                        $('#schoolViewTemplate').html()
                    );
                    
                    modal.find('.modal-body').html(template(result));                    
                }

            }
        });
        
    });
 });
</script>