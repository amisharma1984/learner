<div id="<?php echo $key; ?>Message" class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><span class="glyphicon glyphicon-ok-sign"></span></strong> <?php echo $message; ?>
</div>
<script type="text/javascript">
$(function () {	
    /* For Notification */
    if($('#<?php echo $key; ?>Message').length){
        
        setTimeout(function(){ 
            $('#<?php echo $key; ?>Message').slideToggle();
        }, <?php echo strlen($message) * Configure::read('App.Static.notification_messages_timeout_by_letter') ?>);
    }
});
</script>