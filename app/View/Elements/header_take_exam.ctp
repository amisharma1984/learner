<style>
.school-name-hd {
	float:left;
	width:48%;
	text-align:center;
	padding:10px;
	color:#eaeaea;
}
.school-name-hd h3 {
	color:#eaeaea;
}
</style>
<?php
 $isLoggedIn = $this->Session->check('App.studentData');
        if (!$isLoggedIn) {
			//$page_url = 'javascript:void(0)';
			$page_url = $this->webroot;
		} else {
			$page_url = $this->webroot.'students/myprofile/';
		}
		
	if(!empty($schoolData)){
		$ctrler = 'schools';
	} else {
		$ctrler = $this->params['controller'];
	}	
		
?>
<div class="dashboard-header">
  <div class="dashboard_title">
    <h1><a href="<?php echo $page_url;?>"><img src="<?php echo $this->webroot;?>images/logo.jpg" alt=""/></a></h1>
  </div>
   <div class="school-name-hd">
         <h3><?php 
         if($this->params['controller'] != 'students'){
            echo ucwords(@$studentDetails['Student']['school_name']);
         }
         
         ?>
         </h3>
        
		 <!--Welcome --><?php //echo !empty($studentDetails['Student']['first_name'])? ucwords($studentDetails['Student']['first_name']) : 'User'; ?>

		 <h4>
		 <?php
		 		if($this->params['controller'] == 'students' && $this->params['action'] == 'view_answer')
		 		{
		 			$ansHeading2 = '';

		 			if($cal_non_cal=='non-calculator')
		 			{
		 				$ansHeading2 =	'Non-Calculator';
		 			}
		 			else if($cal_non_cal=='calculator')
		 			{
		 				$ansHeading2 =	'Calculator';
		 			}
		 			else if($cal_non_cal==1)
		 			{
		 				$ansHeading2 =	'Reading Booklet Questions Results';
		 			}
		 			else if($cal_non_cal==2)
		 			{
		 				$ansHeading2 =	'Language Conventions Results';
		 			}
		 			else if($cal_non_cal== 'combined')
		 			{
		 				$ansHeading2 =	'Combined';
		 			}

		 			if($ansHeading2!='')
		 				echo 'Individual Student Results - '.$ansHeading2;
		 			else
		 				echo 'Individual Student Results';
		 		}
		  ?>
		 </h4>
	
        
		 
  </div>
  
  <div class="dashboard-header-right">
  
     <div class="user_account">
	 
	<?php if(!empty($schoolData['School']['manager_name']) && empty($studentDetails)){?>
	<h3 class="user_name">Welcome <?php echo ucwords($schoolData['School']['manager_name']);?></h3>
	<?php } else {?>
	<h3 class="user_name">Welcome <?php echo !empty($studentDetails['Student']['first_name'])? ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']) : 'User'; ?></h3>
	<?php }?>
	
	<?php /*?> <h3 class="user_name">Welcome <?php echo !empty($studentDetails['Student']['first_name'])? ucwords($studentDetails['Student']['first_name']) : 'User'; ?></h3><?php */?>

	<?php  if($this->params['controller'] != 'online_exams' && $this->params['action'] != 'sample_questions' && $this->params['action'] != 'sample_question_view_answer'){?>
	<h3 class="user_name">
	<?php if($this->params['action'] == 'result'){?>
	<a href="<?php echo $this->Html->url(array('controller' => 'students', 'action'=>'close_page'), true);?>">Close</a>
	<?php } else {?>
	<a href="<?php echo $this->Html->url(array('controller' => $ctrler, 'action'=>'logout'), true);?>">Logout</a>
	<?php }?>
	</h3>
	<?php }?>

         <!--<div class="user_pro"><img src="<?php echo $this->webroot;?>images/pro_pic.png" alt=""/></div>-->
     </div>
  </div>
</div>