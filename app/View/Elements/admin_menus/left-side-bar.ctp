<div class="sidebar-nav">
    <div class="nav-canvas">
        <div class="nav-sm nav nav-stacked">

        </div>
        <ul class="nav nav-pills nav-stacked main-menu">
            <li class="nav-header">Main</li>
            <li><a class="ajax-link" href="<?php echo $this->Html->url(array( "controller" => "admins", "action" => "home" ), true); ?>"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
            </li>
			
			
			 <!--<li class="accordion">
                <a href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i><span>Test User Management</span></a>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "admins", "action" => "test_user_list" ), true); ?>">Test Students/Parent</a></li>
					
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "admins", "action" => "test_school_list" ), true); ?>">Test Schools</a></li>
                   
                </ul>
            </li>-->
			
			 <li><a class="ajax-link" href="<?php echo $this->Html->url('/admins/management_access', true); ?>"><i class="glyphicon glyphicon-cog"></i><span> Management Access</span></a>
            </li>
			
			
			        <li><a class="ajax-link" href="<?php echo $this->Html->url('/school_managements/index', true); ?>"><i class="glyphicon glyphicon-cog"></i><span> School Management</span></a>
                    </li>
            
			
			
            <li class="accordion">
                <a href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i><span> Examination Management</span></a>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "examination_managements", "action" => "subjects" ), true); ?>">Subjects</a></li>
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "examination_managements", "action" => "types" ), true); ?>">Exam Types</a></li>
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "examination_managements", "action" => "categories" ), true); ?>">Exam Categories</a></li>
                    <li><a href="<?php echo $this->Html->url('/examination_managements/index', true); ?>">Examinations</a></li>
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "question_managements", "action" => "questions" ), true); ?>">Questions & Answers</a></li>
                </ul>
            </li>
<!--            <li><a class="ajax-link" href="<?php echo $this->Html->url('/email_systems/index', true); ?>"><i class="glyphicon glyphicon-envelope"></i><span> Email Systems</span></a>
            </li>-->           
            
            <li class="accordion">
                <a href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i><span> Email Systems</span></a>
                <ul class="nav nav-pills nav-stacked">
                    
                    <li><a href="<?php echo $this->Html->url('/email_systems/index', true); ?>">Compose Mail</a></li>
					
                    <li><a href="<?php echo $this->Html->url('/admins/saved_customers', true); ?>">Sent Mail</a></li>
					<li><a href="<?php echo $this->Html->url(array( "controller" => "email_systems", "action" => "mail_templates" ), true); ?>">E-mail Templates</a></li>
                    <li><a href="<?php echo $this->Html->url(array( "controller" => "email_systems", "action" => "import" ), true); ?>">Upload School Data</a></li>
                </ul>
            </li>
            
			 <li><a class="ajax-link" href="<?php echo $this->Html->url('/content_managements/index', true); ?>"><i class="glyphicon glyphicon-cog"></i><span> Content Management</span></a>
            </li>
			
			
            <li><a class="ajax-link" href="<?php echo $this->Html->url('/settings/index', true); ?>"><i class="glyphicon glyphicon-cog"></i><span> Settings</span></a>
            </li>
            <!-- <li><a class="ajax-link" href="form.html"><i class="glyphicon glyphicon-edit"></i><span> Forms</span></a></li>
            <li><a class="ajax-link" href="chart.html"><i class="glyphicon glyphicon-list-alt"></i><span> Charts</span></a>
            </li>
            <li><a class="ajax-link" href="typography.html"><i class="glyphicon glyphicon-font"></i><span> Typography</span></a>
            </li>
            <li><a class="ajax-link" href="gallery.html"><i class="glyphicon glyphicon-picture"></i><span> Gallery</span></a>
            </li>
            <li class="nav-header hidden-md">Sample Section</li>
           <li><a class="ajax-link" href="table.html"><i
                        class="glyphicon glyphicon-align-justify"></i><span> Tables</span></a></li>
            <li class="accordion">
                <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Accordion Menu</span></a>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="#">Child Menu 1</a></li>
                    <li><a href="#">Child Menu 2</a></li>
                </ul>
            </li>
            <li><a class="ajax-link" href="calendar.html"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
            </li>
            <li><a class="ajax-link" href="grid.html"><i
                        class="glyphicon glyphicon-th"></i><span> Grid</span></a></li>
            <li><a href="tour.html"><i class="glyphicon glyphicon-globe"></i><span> Tour</span></a></li>
            <li><a class="ajax-link" href="icon.html"><i
                        class="glyphicon glyphicon-star"></i><span> Icons</span></a></li>
            <li><a href="error.html"><i class="glyphicon glyphicon-ban-circle"></i><span> Error Page</span></a>
            </li>
                        
            <li><a href="login.html"><i class="glyphicon glyphicon-lock"></i><span> Login Page</span></a>
            </li>-->
        </ul>
<!--        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
    </div>
</div>