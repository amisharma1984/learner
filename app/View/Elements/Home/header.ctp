 <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            
            <a class="navbar-brand" href="index.html"> <img alt="Charisma Logo" src="<?php echo $this->webroot; ?>img/logo20.png" class="hidden-xs"/>
                <span><?php echo Configure::read('App.appName'); ?></span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo $admindata['Admin']['first_name']." ".$admindata['Admin']['last_name'];?></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'Admins', 'action'=>'profile',$admindata['Admin']['id']));?>">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->Html->url(array('controller' => 'Admins', 'action'=>'logout'));?>">Logout</a></li>
                </ul>
            </div>
          
        </div>
    </div>
    <!-- topbar ends -->