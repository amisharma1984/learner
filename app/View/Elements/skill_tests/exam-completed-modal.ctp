<div id="yourModalId" class="yourModalClass" role="dialog" aria-labelledby="yourModalHeading" style="display:none;">

    <div class="modal_content">
        <h2>Submit the test</h2>
        <p>Once closed, you can no longer view or modify this test. Are you sure you are done, and want to close 
            the test?</p>
        <div class="modal_button">
            <a href="#">Yes, submit the test</a>
            <a href="#" class="donot">No, do not submit</a>
        </div>
    </div>

    <!-- This closes your modal -->
    <div class="modal_close_b"><a href="#yourModalId" aria-label="close"><img src="<?php echo Router::url('/', true); ?>img/front-end/cross_modal.png" alt=""/></a></div>

</div>