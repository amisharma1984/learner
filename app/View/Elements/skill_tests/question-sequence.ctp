<nav class="navbar navbar-inverse easy-sidebar">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header"> </div>
    <div class="question_left ll-hide-all">
        <div class="left_panel"><a href="#" class="easy-sidebar-toggle" title="Click to open menu">Toggle Sidebar</a></div>
        <h2>Questions</h2>
        <?php if(!empty($questions)){ ?>
        <ul class="question_tab questionSeqList">
            <?php foreach ($questions as $key => $value){ ?>
            <li><a id="questionSeqId<?php echo $value['Question']['id']; ?>"><?php echo $value['Question']['question_order']; ?></a></li>
            <?php } ?>
        </ul>
        <?php } ?>
    </div>
    <div class="attamp_check ll-hide-all">
        <label>Attempted</label>
        <label>Not attempted</label>
    </div>
    <div class="view_all_qu ll-hide-all">
        <a href="javascript:void(0);">View All Questions</a>
    </div>
</nav>