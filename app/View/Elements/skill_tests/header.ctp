<div class="dashboard-header">
    <div class="dashboard_title">
        <h1><a href="#"><img src="<?php echo Router::url('/', true); ?>img/front-end/logo.jpg" alt=""/></a></h1>
    </div>

    <div class="dashboard-header-right">
        <div class="user_account">
            <h3 class="user_name">James William</h3>
            <div class="user_pro"><img src="<?php echo Router::url('/', true); ?>img/front-end/pro_pic.png" alt=""/></div>
        </div>
    </div>
</div>