<div class="right_bar">
	 <div class="attempt_details">
	
<?php if(!empty($view_by_student)){?>	
<div class="attempt_qus">
	<ul>
		<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
		<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>
		<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>
	</ul>
</div>
<?php }?>
	 
	 <?php if($this->params['action'] != 'view_exam' && empty(@$view_by_student)){?>
		
		<?php /*?><div class="attempt_qus">
				<ul>
						<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
						<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>

						<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>

				</ul>
		</div><?php */?>
		
		<?php 
			$d1 = ($correctAnswer - $yearGroupMean);
			$daviation1= ($d1*$d1);
			$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 2);
			$studentZscoreByYearGrp = round((($correctAnswer - $yearGroupMean)/$standardDeviation),2); 
			
		?>		
		
<div class="rp-student-info">		
<table>
	<tbody>
		<tr>
			<td>Student's mark</td>
			<td><?php echo $correctAnswer;?></td>
		</tr>

		<tr>
			<td>Year group mean</td>
			<td><?php echo @$yearGroupMean;?></td>
		</tr>

		<tr>
			<td>Year group rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['year_group_rank'];?></td>
		</tr>
		
		<tr>
			<td>Class mean</td>
			<td><?php echo @$sameClassMean;?></td>
		</tr>
		
		<tr>
			<td>Class rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['class_rank'];?></td>
		</tr>
		
		<tr>
			<td>Student's Z score by year group </td>
			<td><?php echo $studentZscoreByYearGrp;?></td>
		</tr>
		

	</tbody>
</table>
</div>
<?php }?>
		
	</div>
	
	
	
</div>
<style>
.rp-student-info table, tr, td{
	width:92%;
	border:2px solid #a4b0db !important;
	margin-left:10px;
}
.rp-student-info table tr td {
    padding: 23px;
	font-size:16px;
	
    color: #fff !important;
}
</style>