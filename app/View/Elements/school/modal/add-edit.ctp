<div class="modal fade" id="newSchoolModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New School</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="addSchoolSuccess" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> School information saved successfully. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'school_managements', 'action' => 'add'),
                    'id' => 'addSchoolForm',
                    'role' => 'form',
                    'class' => 'form-horizontal'
                ));
                ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="posterFormRequired" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>               
                
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolName" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pr-none"><sup style="color:red;">*</sup>Name</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('School.name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolEmail" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pl-none"><sup style="color:red;">*</sup>E-mail</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('School.email', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
				
				  <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolPassword" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pr-none"><sup style="color:red;">*</sup>Password</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('School.password', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolConfirmPassword" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pl-none"><sup style="color:red;">*</sup>Confirm Password</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('School.confirm_password', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
				
                
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolManagerName" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pr-none">Manager name</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('School.manager_name', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolManagerEmail" class="col-md-12 col-sm-12 col-xs-12 control-label text-left pl-none">Manager email</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('School.manager_email', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                </div>
				
				
				
                
                <!-- school_informations  -->
                <div class="form-group mb-xs">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationLevel" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left"><sup style="color:red;">*</sup>Level</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('SchoolInformation.level', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Level -', 'class' => 'form-control', 'required' => true, 'options' => $schoolLevelOptions)); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationType" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left"><sup style="color:red;">*</sup>Type</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('SchoolInformation.type', array('type' => 'select', 'label' => false, 'div' => false, 'empty' => '- Select Type -', 'class' => 'form-control', 'required' => true, 'options' => $schoolTypeOptions)); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label for="SchoolInformationStreet" class="col-md-12 col-sm-12 col-xs-12 control-label text-left">Street</label>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('SchoolInformation.street', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationTown" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">City / Town</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('SchoolInformation.town', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationDistrict" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left">District</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('SchoolInformation.district', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationState" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">State / Province</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('SchoolInformation.state', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationZip" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left">Zip Code</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('SchoolInformation.zip', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationPhone" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">Phone</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('SchoolInformation.phone', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationFax" class="col-md-12 col-sm-12 col-xs-12 pl-none control-label text-left">Fax</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pl-none">
                                <?php echo $this->Form->input('SchoolInformation.fax', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <label for="SchoolInformationUrl" class="col-md-12 col-sm-12 col-xs-12 pr-none control-label text-left">URL</label>
                            <div class="col-md-12 col-sm-12 col-xs-12 pr-none">
                                <?php echo $this->Form->input('SchoolInformation.url', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="checkbox" style="margin-top:0;"> 
                                <label style="display:block;">&nbsp;</label>
                                <label for="SchoolInformationIsPrevious"> 
                                    <?php echo $this->Form->checkbox('SchoolInformation.isprevious', array('hiddenField' => false, 'class' => 'mr-xs', 'id' => 'SchoolInformationIsPrevious')); ?>Is previous year customer?
                                </label> 
                            </div>
                        </div>
                                                
                    </div>
                    
                </div>
                
                <?php echo $this->Form->end(); ?>
                
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <input type="button" class="btn btn-primary" id="saveSchoolData" value="Save changes" style="" >
            </div>
        </div>
    </div>
</div>