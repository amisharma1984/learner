<script type="text/template" id="schoolViewTemplate">
    <div class="table-responsive">
    <table cellspacing="0" class="school-view-table" cellpadding="0" style="width:100%;">
        <tr>
            <td style="width:30%;">
                <strong>E-mail</strong>
            </td>
            <td style="width:70%;">
                <%- data.School.email %>
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                <strong>Manager</strong>
            </td>
            <td style="width:70%;">
                <% if(data.School.manager_name){ %> <%- data.School.manager_name %> <% }else{ %> N/A <% } %>
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                <strong>Manager E-mail</strong>
            </td>
            <td style="width:70%;">
                <% if(data.School.manager_email){ %> <%- data.School.manager_email %> <% }else{ %> N/A <% } %>
            </td>
        </tr>
        <% if(data.SchoolInformation.level){ %>
        <tr>
            <td style="width:30%;">
                <strong>Level</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.level %>
            </td>
        </tr>
        <% } %>
        <% if(data.SchoolInformation.type){ %>
        <tr>
            <td style="width:30%;">
                <strong>Type</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.type %>
            </td>
        </tr>
        <% } %>
        <% if(data.SchoolInformation.street){ %>
        <tr>
            <td style="width:30%;">
                <strong>Address</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.street %>
            </td>
        </tr>
        <% } %>
        <% if(data.SchoolInformation.phone){ %>
        <tr>
            <td style="width:30%;">
                <strong>Phone</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.phone %>
            </td>
        </tr>
        <% } %>
        <% if(data.SchoolInformation.fax){ %>
        <tr>
            <td style="width:30%;">
                <strong>Fax</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.fax %>
            </td>
        </tr>
        <% } %>
        <% if(data.SchoolInformation.url){ %>
        <tr>
            <td style="width:30%;">
                <strong>URL</strong>
            </td>
            <td style="width:70%;">
                <%- data.SchoolInformation.url %>
            </td>
        </tr>
        <% } %>
        <tr>
            <td style="width:30%;">
                <strong>Previous year customer?</strong>
            </td>
            <td style="width:70%;">
                <% if(data.SchoolInformation.isprevious){ %> Yes <% }else{ %> No <% } %>
            </td>
        </tr>
        <tr>
            <td style="width:30%;">
                <strong>Registered On</strong>
            </td>
            <td style="width:70%;">
                <% var t = data.School.created.split(/[- :]/); var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]); %>
                <%- d.format('dS F, Y h:ia') %>
            </td>
        </tr>   
           
    </table>
    </div>
</script>