<div id="<?php echo $key; ?>Message" class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong><span class="glyphicon glyphicon-info-sign"></span></strong> <?php echo $message; ?>
</div>
<script type="text/javascript">
$(function () {	
    /* For Notification */
    if($('#<?php echo $key; ?>Message').length){
        
        setTimeout(function(){ 
            $('#<?php echo $key; ?>Message').slideToggle();
        }, <?php echo strlen($message) * Configure::read('App.Static.notification_messages_timeout_by_letter') ?>);
    }
});
</script>