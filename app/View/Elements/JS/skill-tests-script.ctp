<script src="<?php echo Router::url('/', true); ?>js/front-end/bootstrap.js"></script> 
<script src="<?php echo Router::url('/', true); ?>js/front-end/jquery.detect_swipe.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/front-end/modality.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('.shtm').click(function () {
            $('.topbarmenu').slideToggle();
        });

        $('.easy-sidebar-toggle').click(function (e) {
            e.preventDefault();
            $('body').toggleClass('toggled');
            $('.navbar.easy-sidebar').removeClass('toggled');
        });
        $('html').on('swiperight', function () {
            $('body').addClass('toggled');
        });
        $('html').on('swipeleft', function () {
            $('body').removeClass('toggled');
        });

    });
    
    /* Disable ways to open Inspect Element in JavaScript and HTML */
    document.onkeydown = function(e) {
//        if(event.keyCode == 123) {
//            return false;
//        }
//        if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
//            return false;
//        }
//        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
//            return false;
//        }
//        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
//            return false;
//        }
    }
    
    /* To prevent right click */
//    $(document).bind("contextmenu",function(e) {
//        e.preventDefault();
//    });
    
    window.addEventListener("resize", function(event){
        return false;
        if ($(window).width() < 768) {
            $(".topbarmenu").hide();
        } else {
            $(".topbarmenu").show();
        }
    }, false);
    
    Modality.init('#yourModalId', {effect: 'slide-up'});
</script>