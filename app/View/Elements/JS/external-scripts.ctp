<!-- external javascript -->

<script src="<?php echo Router::url('/', true); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="<?php echo Router::url('/', true); ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.dataTables.min.js"></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo Router::url('/', true); ?>bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo Router::url('/', true); ?>bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo Router::url('/', true); ?>bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo Router::url('/', true); ?>bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo Router::url('/', true); ?>js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo Router::url('/', true); ?>js/charisma.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.validate.min.js"></script>
<script src="<?php echo Router::url('/', true); ?>js/jquery.validation/jquery.clearValidation.js"></script>

<script type="text/javascript">
    
/* Checking admin login status */
function checkAdminStatus() {
    //console.log()

    var jqxhrLoginStatus = $.ajax({
        type: "POST",
        url: BASEURL + 'admins/checkAdminStatus',
        success: function (response) {
            //console.log(response);
            if (response != '') {
            
                var result = JSON.parse(response);
                var className = (result.status == 'Error')?'alert-danger':'alert-info';

                var statusString = (result.status != 'Error')?'<span class="glyphicon glyphicon-info-sign"></span>':'<span class="glyphicon glyphicon-remove-sign"></span>';

                var htmlString = '<div class="row"><div class="col-md-12 col-xs-12 col-sm-12 text-center">';
                htmlString += '<div class="alert '+className+'" role="alert">';
                htmlString += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                htmlString += '<strong>'+statusString+'</strong> '+result.message; 
                htmlString += '</div>';
                htmlString += '</div></div>';

                $('#adminStatusModal').find('.modal-body').html(htmlString);
                if(result.status != 'Error'){
                    $('#adminStatusModal').modal({
                        show:true,
                        keyboard: false,
                        backdrop:'static'
                    });

                    var timeOutValue = parseInt(result.messageLetterCount) * parseInt(result.messageTimeOut);
                    setTimeout(function () {
                        window.location = BASEURL + 'admins/login';
                    }, timeOutValue);
                }
            }
        }
    });
    
}

function redirectToAdminLogin(){
    window.location = BASEURL + 'admins/login';
}

/* This function works like "setInterval" */
function Timer(timeout) {
    var self = this;
    this.interval = timeout ? timeout : 1000;   // Default

    this.run = function (runnable) {
        setInterval(function () { runnable(self); }, this.interval);
    };
}

var timer = new Timer(2000);
var timerCounter = 1;
timer.run(function (timer) {
    //console.log(' Time '+timerCounter+' : ' + timerCounter * timer.interval + 'ms');
    
    checkAdminStatus();
    
    timerCounter++;
});

/* Capitalise first leter of a string */
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

/* Convert digits into words with JavaScript */
function numberInWords (num) {
    
    var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
    var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
    
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
    return str;
    
}

// prevent Bootstrap from hijacking TinyMCE modal focus    
$(document).on('focusin', function(e) {
    if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});
</script>