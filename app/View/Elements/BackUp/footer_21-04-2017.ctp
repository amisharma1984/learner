<div class="container">
    <div class="about_footer_section">
        <div class="about_section">
            <img src="<?php echo $this->webroot;?>images/about_logo.png" alt=""/>
            <h2>ABOUT US</h2>
            <p>Our aim is to help students achieve successful results in Mathematics and English and higher grades in NAPLAN <br>examinations. <br>
For these purposes we offer our educational services which present the opportunity to access assistance in schools and at home.</p>
       <a href="<?php echo $this->webroot.'about-us';?>">Read More</a>
        </div>
        <div class="footer_section">
		<?php echo $this->Session->flash();?>
           <?php echo $this->Form->create(null, array('id' => 'subscribe_news_letter', 'url' => $this->Html->url(array('controller' => 'homes', 'action' => 'subscribe_news_letter'), true))); ?>
		   <div class="search_section">
		  
				<?php echo $this->Form->input('User.email', array('label' => false, 'div' => false, 'type' => 'email','required' => 'required',  'placeholder' => 'Enter your email...'))?>
                <input type="submit"  value="Subscribe">
				<span  class="rgerror"><?php //if($this->Session->read('emailErr')) { echo $this->Session->read('emailErr');}?> </span>
            </div>
		<?php echo $this->Form->end(); ?>
			
            <div class="footer_nav_section">
                <div class="footer_nav_box">
                    <h2>What we offer</h2>
                    <ul>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-school';?>">For schools</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-families';?>">For families</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Maths';?>">Maths</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/English';?>">English</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Awards';?>">Awards</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Analytics';?>">Analytics</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Standards';?>">Standards</a></li>
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>About</h2>
                    <ul>
                        <li><a href="<?php echo $this->webroot.'company-information';?>">Company information</a></li>
                        <li><a href="<?php echo $this->webroot.'terms-of-service';?>">Terms of service</a></li>
                        <li><a href="<?php echo $this->webroot.'privacy-policy';?>">Privacy policy</a></li>
                         <li><a href="<?php echo $this->webroot.'admins';?>">Admin</a></li>
                       
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>Contact us</h2>
                    <p>PO Box CT16122 Collins Street West,<br> 
Victoria 8007,<br>
Australia.<br>
+81 (2) 345 6789</p>
                </div>
            </div>
            <div class="copy_right">©<?php echo date('Y');?> S&G Publishing and Coaching Academy, All rights reserved.</div>
        </div>
    </div>
</div>
 