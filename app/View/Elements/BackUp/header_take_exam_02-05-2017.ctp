<style>
.school-name-hd {
	float:left;
	width:48%;
	text-align:center;
	padding:10px;
	color:#eaeaea;
}
.school-name-hd h3 {
	color:#eaeaea;
}
</style>
<?php
 $isLoggedIn = $this->Session->check('App.studentData');
        if (!$isLoggedIn) {
			$page_url = 'javascript:void(0)';
		} else {
			$page_url = $this->webroot.'students/myprofile/';
		}
?>
<div class="dashboard-header">
  <div class="dashboard_title">
    <h1><a href="<?php echo $page_url;?>"><img src="<?php echo $this->webroot;?>images/logo.jpg" alt=""/></a></h1>
  </div>
   <div class="school-name-hd">
         <h3><?php 
         if($this->params['controller'] != 'students'){
            echo ucwords(@$studentDetails['Student']['school_name']);
         }
         
         ?></h3>
  </div>
  
  <div class="dashboard-header-right">
  
     <div class="user_account">
         <h3 class="user_name">Welcome <?php echo ucwords($studentDetails['Student']['first_name']); ?></h3>
         
         <?php  if($this->params['controller'] != 'online_exams'){?>
		   <h3 class="user_name"><a href="<?php echo $this->Html->url(array('controller' => $this->params['controller'], 'action'=>'logout'), true);?>">Logout</a></h3>
		   <?php }?>
		 
         <!--<div class="user_pro"><img src="<?php echo $this->webroot;?>images/pro_pic.png" alt=""/></div>-->
     </div>
  </div>
</div>