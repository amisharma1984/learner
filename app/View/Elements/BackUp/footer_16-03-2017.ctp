
      <div class="container">
    <div class="about_footer_section">
        <div class="about_section">
            <img src="<?php echo $this->webroot;?>images/about_logo.png" alt=""/>
            <h2>ABOUT US</h2>
            <p>Our aim is to help students achieve successful results in Mathematics and English and higher grades in NAPLAN <br>examinations. <br>
For these purposes we offer our educational services which present the opportunity to access assistance in schools and at home.</p>
       <a href="#">Read More</a>
        </div>
        <div class="footer_section">
            <div class="search_section">
                <input type="text" placeholder="Enter your email...">
                <input type="button" value="Subscribe">
            </div>
            <div class="footer_nav_section">
                <div class="footer_nav_box">
                    <h2>What we offer</h2>
                    <ul>
                        <li><a href="#">For schools</a></li>
                        <li><a href="#">For families</a></li>
                        <li><a href="#">Maths</a></li>
                        <li><a href="#">English</a></li>
                        <li><a href="#">Awards</a></li>
                        <li><a href="#">Analytics</a></li>
                        <li><a href="#">Standards</a></li>
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>About</h2>
                    <ul>
                        <li><a href="#">Company information</a></li>
                        <li><a href="#">Terms of service</a></li>
                        <li><a href="#">Privacy policy</a></li>
                        <li><a href="#">Jobs</a></li>
                      
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>Contact us</h2>
                    <p>PO Box CT16122 Collins Street West,<br> 
Victoria 8007,<br>
Australia.<br>
+81 (2) 345 6789</p>
                </div>
            </div>
            <div class="copy_right">©2017 S&G Publishing and Coaching Academy, All rights reserved.</div>
        </div>
    </div>
</div>
 