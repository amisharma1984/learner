  <?php $action = $this->params['action'];?>
	<div class="col-sm-4 col-md-3">
            <div class="profile_tab">
              <div class="profile_head">
                <h2>Dashboard</h2>
                <div class="prof_add">
                  <div class="profileimg">
                    <img src="<?php echo $this->webroot;?>images/profile_pic.png" alt="Profile Pic">
                  </div>
                  <div class="prof_welcome_info" >
                    <h3><span>Welcome,</span>
                    <?php echo ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']); ?></h3>
                  </div>
                </div>
              </div>
              <div class="prof_tab_link">
                <ul>
                  
                  
				  <!--<li class="<?php //echo ($action == 'purchase_exam') ? 'active' : ' ';?>"><a href="<?php //echo $this->webroot.'students/purchase_exam';?>"><i class="fa fa-credit-card" aria-hidden="true"></i>Purchase exam online</a></li>-->
				 <li class="<?php echo ($action == 'take_exam_list') ? 'active' : ' ';?>"><a href="<?php echo $this->webroot.'students/take_exam_list'?>"><i class="fa fa-file-text-o" aria-hidden="true"></i>Take exam</a></li>
				 <li class="<?php echo ($action == 'purchase_exam' || $action == 'all_exam_list') ? 'active' : ' ';?>"><a href="<?php echo $this->webroot.'students/all_exam_list';?>"><i class="fa fa-credit-card" aria-hidden="true"></i>Purchase Exams</a></li>
                
				
                  <!--<li class="<?php echo ($action == 'my_exams' || $action == 'view_answer' ) ? 'active' : ' ';?>"><a href="<?php echo $this->webroot.'students/my_exams'?>"><i class="fa fa-file-excel-o" aria-hidden="true"></i> My Exams</a></li>-->
                   <li class="<?php echo ($action == 'my_exams' || $action == 'view_answer' ) ? 'active' : ' ';?>"><a href="<?php echo $this->webroot.'students/my_exams'?>"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Exams Taken</a></li>
                  <li class="<?php echo ($action == 'myprofile') ? 'active' : ' ';?>"><a href="<?php echo $this->webroot.'students/myprofile/';?>"><i class="fa fa-user" aria-hidden="true"> </i> My Profile</a></li>
				  <li><a href="<?php echo $this->webroot.'students/logout';?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a></li>
                </ul>
              </div>
            </div>
          </div>