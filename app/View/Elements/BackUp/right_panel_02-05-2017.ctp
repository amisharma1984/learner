<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/jquery.countdownTimer.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/jquery.countdownTimer.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/w3.css" />-->
<script>
				var totalExamTimeMinutes = <?php echo $examDuration;?>; //minutes
				 var examTimeSec = totalExamTimeMinutes * 60; //seconds
				 var timeIntervalSec = examTimeSec / 100; //100 means % age 
				 var timeIntervalMs = timeIntervalSec * 1000; //ms
				setInterval(function() {
					  // Do something every 5 seconds
							
							 $(document).ready(function(){
								 
								var time_input = $( "#time_input" ).val( );
								time_input = time_input -1;
								var t = time_input;
								
								if(time_input >=1)
									  $( "#time_input" ).val( time_input);
									   $( "#percent_val_txt" ).text( time_input+'%' );
									   
										$('#time_prcessing_speed').css({"width": ""});
										$('#time_prcessing_speed').css({"width": t+"%"});
										
										
										});
							
						
							//$('#time_prcessing_speed').width(100%);

				}, timeIntervalMs);

</script>


<div class="right_bar">

<?php if($this->params['action'] == 'take_exam' || $this->params['action'] == 'take_exam_second_section'){?>
    <div class="timing_bar">
        <div class="time_progress_bar">
            <div class="white_bar">
                <div class="main_bar"><div class="prcessing_speed" id="time_prcessing_speed" style="width:100%">&nbsp;</div></div>
				<input type="hidden" value="100" id="time_input">
                <div class="bar_value" id="percent_val_txt">100%</div>
            </div>
        </div>
        <div class="text_result">
<span id="hms_timer"></span>

<!--&nbsp;&nbsp;<button id="pauseBtnhms">Pause</button>&nbsp;&nbsp;
<button id="stopBtnhms">Stop</button>->

            <!--<h2>00:29:50</h2>-->
            <p>Time Remaining</p>
        </div>
    </div>

	
    
    <div class="date_bar">
	
        <div class="time_progress_bar">
            <div class="white_bar">
                <div class="main_bar"><div class="prcessing_speed2" id="qs_attempted_prcessing" style="width:0%">&nbsp;</div></div>
                <div class="bar_value" id="percent_val_txt_qs">0%</div>
            </div>
        </div>
		
        <div class="text_result">
            <h2><span id="attempted_qs">0</span>/<?php echo @count($QuestionAns);?></h2>
            <p>Questions Attempted</p>
        </div>
    </div>
    <?php }?>
</div>
	<script>
	$(function(){
			$('#hms_timer').countdowntimer({
				hours : 00,
				minutes :  <?php echo $examDuration;?>,
				seconds : 00,
				size : "lg",
				
			pauseButton : "pauseBtnhms",
			stopButton : "stopBtnhms"
			});
	});
	</script>