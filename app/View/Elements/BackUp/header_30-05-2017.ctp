<?php
$controller = $this->params['controller'];
$action = $this->params['action'];
$isLoggedIn = $this->Session->check('App.studentData');
?>
<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
          <div class="logo">
              <a href="<?php echo $this->webroot;?>"><img src="<?php echo $this->webroot;?>images/logo.png" alt=""/></a>
          </div>
      </div>
       <div class="col-sm-9">
           <div class="navigation_section">
		   
		   <?php   if (!$isLoggedIn) {?>
		   
             <div class="registration_button"   style="margin:0 0 0 10px">
                <!--  <a    href="javascript:void(0);">Access</a>-->
                      <a href="javascript:void(0);">log in</a>
					<div class="drop_down">
						<ul>
						<!--<li><a href="<?php //echo $this->webroot.'schools/registration';?>">School</a><l/i>
							<li><a href="<?php //echo $this->webroot.'students/registration';?>">Student</a><l/i>-->
                            <!-- <li><a href="<?php echo $this->webroot.'schools/login_new';?>">schools online testing</a></li>
							<li><a href="<?php echo $this->webroot.'students/login_new';?>">student online practice</a></li>
							<li><a href="<?php echo $this->webroot.'online_exams/login';?>">Online Exam Login</a></li>
							<li><a href="<?php //echo $this->webroot.'admins';?>">Admin</a><l/i>-->
							 <li><a href="<?php echo $this->webroot.'students/login_new';?>">Student/ Parent Log in</a></li>
							  <li><a href="#" style="margin-bottom: 10px;" data-toggle="modal" data-target="#schoolLoginPop">School Log in</a></li>
						
						</ul>
					</div>
				 
             </div>
			  <div class="registration_button">

                             <a href="javascript:void(0);">Create Account</a>
					<div class="drop_down">
						<ul>
							<li><a href="<?php echo $this->webroot.'students/registration';?>">Student/ Parent Account</a></li>
							 <li><a href="<?php echo $this->webroot.'schools/registration';?>">School Account</a></li>

						</ul>
					</div>
				 
             </div>
			 
			 
		   <?php }?>
		   
              <div class="navigation_bar">
               <nav class="navbar navbar-default navbar-static-top">
   
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
       </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php echo ($controller == 'homes' && $action == 'index') ? 'active' : ' ' ;?>"><a href="<?php echo $this->webroot;?>">Home</a></li>
        <li class="<?php echo ($controller == 'homes' && $action == 'about_us') ? 'active' : ' ' ;?>"><a href="<?php echo $this->webroot.'about-us';?>">About Us</a></li>
        <li class="<?php echo ($controller == 'homes' && $action == 'contact_us') ? 'active' : ' ' ;?>"><a href="<?php echo $this->webroot.'contact-us';?>">Contact Us</a></li>
		
		<?php if($isLoggedIn){?>
         <li class="<?php echo ($controller == 'students' && $action == 'myprofile') ? 'active' : ' ' ;?>"><a href="<?php echo $this->webroot.'students/myprofile/';?>">My Profile</a></li>
		 <?php }?>
		
		 
		 
      </ul>
      
      
    </div>
    <!-- /.navbar-collapse --> 
 
  <!-- /.container-fluid --> 
</nav>
              </div>
           </div>
       </div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>


<!--1st modal -->
<div class="modal fade" id="schoolLoginPop" tabindex="-1" role="dialog" aria-labelledby="examAddEditModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:500px;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#119548;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:#fff;"> Log in as:</h4>
            </div> 
            <div class="modal-body">
                 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'online_exams/login';?>')">Student </button>
                  <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/login_new';?>')">Head Teacher</button>
                   
            </div>
            <div class="modal-footer" style="background-color:rgba(28, 140, 75, 0.49);">
                <a href="#" class="btn closebut btn-default" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function openUrl(url){
            //var win = window.open(url, '_blank');
            var win = window.open(url);
    }
    </script>