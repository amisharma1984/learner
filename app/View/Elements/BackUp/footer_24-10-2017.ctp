<?php 
$aboutContent = $this->requestAction(array('controller' => 'homes', 'action' => 'get_about_us_content')); 
$content = $aboutContent['ContentManagement']['description'];
 if (strlen($content) > 200){
      $conten_255 = substr($content, 0, strrpos(substr($content, 0, 170), ' ')) . '...';
 } else {
	 $conten_255 = $content;
 }
?>
<div class="container">
    <div class="about_footer_section">
        <div class="about_section">
            <img src="<?php echo $this->webroot;?>images/about_logo.png" alt=""/>
            <h2>ABOUT US</h2>
          
		  <!--<p>Our aim is to help students achieve successful results in Mathematics and English and higher grades in NAPLAN <br>examinations. <br>
For these purposes we offer our educational services which present the opportunity to access assistance in schools and at home.</p>-->
		<?php echo $conten_255;?>
       <a href="<?php echo $this->webroot.'about-us';?>">Read More</a>
        </div>
        <div class="footer_section">
		<?php echo $this->Session->flash();?>
           <?php echo $this->Form->create(null, array('id' => 'subscribe_news_letter', 'url' => $this->Html->url(array('controller' => 'homes', 'action' => 'subscribe_news_letter'), true))); ?>
		   <!--<div class="search_section">
				<?php //echo $this->Form->input('User.email', array('label' => false, 'div' => false, 'type' => 'email','required' => 'required',  'placeholder' => 'Enter your email...'))?>
                <input type="submit"  value="Subscribe">
            </div>-->
		<?php echo $this->Form->end(); ?>
			
            <div class="footer_nav_section">
                <div class="footer_nav_box">
                    <h2>What we offer</h2>
                    <ul>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-school';?>">For schools</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-families';?>">For families</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Maths';?>">Maths</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/English';?>">English</a></li>
                        <li><a href="<?php echo $this->webroot;?>">NAPLAN</a></li>
						
                        <!--<li><a href="<?php //echo $this->webroot.'we-offer/Awards';?>">Awards</a></li>
                        <li><a href="<?php //echo $this->webroot.'we-offer/Analytics';?>">Analytics</a></li>
                        <li><a href="<?php //echo $this->webroot.'we-offer/Standards';?>">Standards</a></li>-->
						
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>About</h2>
                    <ul>
                        <li><a href="<?php echo $this->webroot.'about-us';?>">Company information</a></li>
                        <li><a href="<?php echo $this->webroot.'terms-of-service';?>">Terms and Conditions</a></li>
                        <li><a href="<?php echo $this->webroot.'privacy-policy';?>">Privacy policy</a></li>
                         <li><a href="<?php echo $this->webroot.'admins';?>">Admin</a></li>
                       
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>Contact us</h2>
                    <p>S & G Publishing and Coaching Academy<br> 
Suite 304<br>
398 Chapel Rd<br>
Bankstown<br>
NSW Australia <br>
(02) 9707 3067<br>
info@learnerleader.com
</p>
                </div>
            </div>
            <div class="copy_right">©<?php echo date('Y');?> Learner Leader Online, All rights reserved.</div>
        </div>
    </div>
</div>
 