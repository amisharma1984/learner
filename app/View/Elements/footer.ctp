<!-- Start Send Email to School Modal-->
<div class="modal fade" id="schoolEmailModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Mail</h4>
            </div>
           <div class="modal-body emailhtml" id="emailhtml">
                <div class="row" id="sendEmailSuccess" style="display:none;">
                    <div class="col-md-6 col-xs-12 col-sm-12 text-center">
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Thank you!</strong> Email has sent to the selected users. 
                        </div>
                    </div>
                </div>
                <?php
                echo $this->Form->create(false, array(
                    'url' => array('controller' => 'homes', 'action' => 'send_mail'),
                    'id' => 'sendEmailsForm',
                    'role' => 'form',
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ));
                ?>
                <?php echo $this->Form->input('Email.fromId', array('type' => 'hidden', 'value' => (!empty($admindata['Admin']['id']))?$admindata['Admin']['id']:'')); ?>
                <?php echo $this->Form->input('Email.attachmentMask', array('type' => 'hidden')); ?>
                <div class="form-group" style="margin-bottom: 4px;">
                    <label for="" class="col-md-12 col-sm-12 col-xs-12 control-label text-right" style="font-size: 13px; font-weight: normal;"><sup style="color:red;">*</sup> indicates a required field.</label>
                </div>
                <div class="form-group">
                    <label for="SendmailId" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Mail To</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Email.sendmailid', array('type' => 'text', 'id' => 'sendmailid', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true, 'readonly' => false)); ?>
                    <label id="SendmailId-error" class="error" for="SendmailId"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="EmailSubject" class=" col-md-3 col-sm-3 col-xs-12 control-label"><sup style="color:red;">*</sup>Subject</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('Email.subject', array('type' => 'text', 'id' => 'subject', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'required' => true)); ?>
                    <label id="EmailSubject-error" class="error" for="EmailSubject"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="EmailMessage" class=" col-md-3 col-sm-3 col-xs-12 control-label">Message</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">                        
                        <?php echo $this->Form->input('Email.message', array('type' => 'textarea', 'label' => false, 'div' => false, 'class' => 'form-control textEditor', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Attach files</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <!--  <a id="pickfiles" href="#" class="btn btn-primary" ><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;CHOOSE YOUR FILES</a> -->
                       <?php echo $this->Form->input('Email.attachment', array('type' => 'file', 'id' => 'attachment', 'label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off')); ?>
                    <label id="Attachment-error" class="error" for="Attachment"></label>
                    </div>
                </div>
               <!-- <div class="form-group mb-none">
                    <label class="col-md-2 col-sm-2 col-xs-12 control-label">&nbsp;</label>
                    <div class="col-md-10 col-sm-10 col-xs-12" id="fileAttachments" style="max-height:140px; overflow-y: scroll; overflow-x: hidden;">
                        
                    </div>
                </div> -->
<!--                <div class="form-group">
                    <div class="col-md-offset-1 col-md-11 col-sm-11 col-xs-12">
                        <div id="upload-container">
                            <div id="droparea">
                                <div style="text-align: center; border: 3px dashed white; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; padding: 16px 0 16px 0;">
                                    <span id="text-to-remove">DRAG &amp; DROP FILES HERE<br /> OR</span>
                                    <div style="height: 8px;"></div>
                                    <a id="pickfiles" href="#" class="button" style="color: #fff;text-decoration:none;">CHOOSE YOUR FILES</a><br />
                                    <div style="height: 8px;"></div>
                                </div>
                            </div>

                            <div id="filelist"></div>
                        </div>
                    </div>
                </div>-->

                <?php echo $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="button" class="btn btn-primary" id="sendEmailsButton1" value="Send Mail" style="" >
                <!--        <button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Send Email to School Modal -->
<?php 
$aboutContent = $this->requestAction(array('controller' => 'homes', 'action' => 'get_about_us_content')); 
$content = $aboutContent['ContentManagement']['description'];
 if (strlen($content) > 200){
      $conten_255 = substr($content, 0, strrpos(substr($content, 0, 170), ' ')) . '...';
 } else {
	 $conten_255 = $content;
 }
?>
<div class="container">
    <div class="about_footer_section">
        <div class="about_section">
            <img src="<?php echo $this->webroot;?>images/about_logo.png" alt=""/>
            <h2>ABOUT US</h2>
          
		  <!--<p>Our aim is to help students achieve successful results in Mathematics and English and higher grades in NAPLAN <br>examinations. <br>
For these purposes we offer our educational services which present the opportunity to access assistance in schools and at home.</p>-->
		<?php echo $conten_255;?>
       <a href="<?php echo $this->webroot.'about-us';?>">Read More</a>
        </div>
        <div class="footer_section">
		<?php echo $this->Session->flash();?>
           <?php echo $this->Form->create(null, array('id' => 'subscribe_news_letter', 'url' => $this->Html->url(array('controller' => 'homes', 'action' => 'subscribe_news_letter'), true))); ?>
		   <!--<div class="search_section">
				<?php //echo $this->Form->input('User.email', array('label' => false, 'div' => false, 'type' => 'email','required' => 'required',  'placeholder' => 'Enter your email...'))?>
                <input type="submit"  value="Subscribe">
            </div>-->
		<?php echo $this->Form->end(); ?>
			
            <div class="footer_nav_section">
                <div class="footer_nav_box">
                    <h2>What we offer</h2>
                    <ul>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-school';?>">For schools</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/for-families';?>">For families</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/Maths';?>">Maths</a></li>
                        <li><a href="<?php echo $this->webroot.'we-offer/English';?>">English</a></li>
                        <li><a href="<?php echo $this->webroot;?>">NAPLAN</a></li>
						
                        <!--<li><a href="<?php //echo $this->webroot.'we-offer/Awards';?>">Awards</a></li>
                        <li><a href="<?php //echo $this->webroot.'we-offer/Analytics';?>">Analytics</a></li>
                        <li><a href="<?php //echo $this->webroot.'we-offer/Standards';?>">Standards</a></li>-->
						
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>About</h2>
                    <ul>
                        <!--<li><a href="<?php //echo $this->webroot.'about-us';?>">Company information</a></li>-->
                        <li><a href="<?php echo $this->webroot.'terms-of-service';?>">Terms and Conditions</a></li>
                        <li><a href="<?php echo $this->webroot.'privacy-policy';?>">Privacy policy</a></li>
                         <li><a href="<?php echo $this->webroot.'admins';?>">Admin</a></li>
                       
                        
                    </ul>
                </div>
                <div class="footer_nav_box">
                    <h2>Contact us</h2>
					<p>Learner Leader Online<br> 
					Suite 304<br>
					398 Chapel Rd<br>
					Bankstown<br>
					Postcode 2200<br>
					NSW Australia <br>
					(02) 9707 3067<br>
					<a title="Send Mail" style="color:#b3d2f5" href="javascript:void(0);" onclick="openMailModal('info@learnerleaderonline.com');">info@learnerleaderonline.com</a>
					</p>
                </div>
            </div>
            <div class="copy_right">©<?php echo date('Y');?> Learner Leader Online, All rights reserved.</div>
        </div>
    </div>
</div>

<script  type="text/javascript" >
  
jQuery(document).ready(function($) {
  var alterClass = function() {
    var ww = document.body.clientWidth;
    if (ww >= 768) {
      $('body').removeClass('mobile');
    } else if (ww < 767) {
      $('body').addClass('mobile');

    };
  };
  $(window).resize(function(){
    alterClass();
  }); 
  //Fire it when the page first loads:
  alterClass();

       $(".mobile .login").click(function(){
            $('.mobile .drop1').slideToggle();
        });
           $(".mobile .creatac").click(function(){
            $('.mobile .drop2').slideToggle();
        });
        
         $( "#sendEmailsButton1" ).click(function() {
        	   var flag = 1;
        	   $("#Attachment-error").html("");
   			$("#EmailSubject-error").html("");
        	   if($("#attachment").val() != "")
        		{
        			var allowedFiles = [".doc", ".docx", ".pdf"];
        			var fileUpload = document.getElementById("attachment");
        			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        			if (!regex.test(fileUpload.value.toLowerCase())) {
            		
            		flag = 0;
        			}
        			
        		}
        		if($("#subject").val() == "")
        		{
        			$("#EmailSubject-error").html("This field is required.");
        			return false;
        		}
        		else if(flag == "0")
        		{
        			$("#Attachment-error").html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
        			return false;
        		}
        		else{
        			$( "#sendEmailsForm" ).submit();
        		}
 		  	
		  })
		  $( "#attachment" ).change(function() {
		  	   $("#Attachment-error").html("");
 		  		var allowedFiles = [".doc", ".docx", ".pdf"];
        		var fileUpload = document.getElementById("attachment");
        		var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        		if (!regex.test(fileUpload.value.toLowerCase())) {
            	$("#Attachment-error").html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            
        		}
		  })

});
function openMailModal(email) {
	$('#schoolEmailModal').modal('show');
	$("#Attachment-error").html("");
   $("#EmailSubject-error").html("");
	$('#sendEmailsForm')[0].reset();
	$('#sendmailid').val(email);
}
</script>
 