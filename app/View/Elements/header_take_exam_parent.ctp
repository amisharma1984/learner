<style>
.school-name-hd {
	float:left;
	width:48%;
	text-align:center;
	padding:10px;
	color:#eaeaea;
}
.school-name-hd h3 {
	color:#eaeaea;
}
</style>
<?php
 $isLoggedIn = $this->Session->check('App.parentData');
        if (!$isLoggedIn) {
			//$page_url = 'javascript:void(0)';
			$page_url = $this->webroot;
		} else {
			$page_url = $this->webroot.'parent/take_exam_list/';
		}
?>
<div class="dashboard-header">
  <div class="dashboard_title">
    <h1><a href="<?php echo $page_url;?>"><img src="<?php echo $this->webroot;?>images/logo.jpg" alt=""/></a></h1>
  </div>
   <!--<div class="school-name-hd">
         <h3><?php 
         if($this->params['controller'] != 'students'){
            echo ucwords(@$studentDetails['Student']['school_name']);
         }
         
         ?>
		 Welcome <?php echo !empty($studentDetails['Student']['first_name'])? ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']) : 'User'; ?>
		 </h3>
	
        
		 
  </div>-->
  
  <div class="dashboard-header-right">
  
     <div class="user_account">
         <h3 class="user_name">Welcome <?php echo !empty($studentDetails['Student']['first_name'])? ucwords($studentDetails['Student']['first_name']).' '.ucwords($studentDetails['Student']['last_name']) : 'User'; ?></h3>
         
         <?php  if($this->params['controller'] != 'online_exams' && $this->params['action'] != 'sample_questions'){?>
		   <h3 class="user_name"><a href="<?php echo $this->Html->url(array('controller' => $this->params['controller'], 'action'=>'logout'), true);?>">Logout</a></h3>
		   <?php }?>
		 
         <!--<div class="user_pro"><img src="<?php echo $this->webroot;?>images/pro_pic.png" alt=""/></div>-->
     </div>
  </div>
</div>