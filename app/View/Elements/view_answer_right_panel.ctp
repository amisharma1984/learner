<?php if($this->params['action'] != 'view_individual_report'){?>
<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/jquery.countdownTimer.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/jquery.countdownTimer.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/w3.css" />-->
<?php $exam_view_time = 15; //minutes?>
<script>
				var totalExamTimeMinutes = <?php echo $exam_view_time;?>; //minutes
				 var examTimeSec = totalExamTimeMinutes * 60; //seconds
				 var timeIntervalSec = examTimeSec / 100; //100 means % age 
				 var timeIntervalMs = timeIntervalSec * 1000; //ms
				setInterval(function() {
					  // Do something every 5 seconds
							
							 $(document).ready(function(){
								 
								var time_input = $( "#time_input" ).val( );
								time_input = time_input -1;
								var t = time_input;
								
								if(time_input >=1)
									  $( "#time_input" ).val( time_input);
									   $( "#percent_val_txt" ).text( time_input+'%' );
									   
										$('#time_prcessing_speed').css({"width": ""});
										$('#time_prcessing_speed').css({"width": t+"%"});
										
										
										});
							
						
							//$('#time_prcessing_speed').width(100%);

				}, timeIntervalMs);

</script>



<?php 
	if(isset($cal_non_cal) && $cal_non_cal == 'combined'){
	 $right_bar_class = 'right_bar right_bar_combined';
	} else {
		$right_bar_class = 'right_bar';
	}
	?>

<div class="<?php echo $right_bar_class;?>">
	 <div class="attempt_details">
	
<?php if(!empty($view_by_student)){?>	
<div class="attempt_qus">
	<ul>
		<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
		<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>
		<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>
	</ul>
</div>
<?php }?>
	 
	 <?php if($this->params['action'] != 'view_exam' && empty(@$view_by_student)){?>
		
		<?php /*?><div class="attempt_qus">
				<ul>
						<li><span>Attempt Questions : </span> <strong><?php echo $totalAttemptQs;?> </strong></li>
						<li><span>Correct Answer : </span> <strong> <?php echo $correctAnswer;?> </strong></li>

						<li><span>Result : </span> <strong> <?php echo round(($correctAnswer/$totalqustionSet)*100 ,2); ?>% </strong></li>

				</ul>
		</div><?php */?>
		
		<?php 
			$d1 = ($correctAnswer - $yearGroupMean);
			$daviation1= ($d1*$d1);
			$standardDeviation = round(($daviation1) / sqrt($noOfStudentInYearGroup), 2);
			$studentZscoreByYearGrp = round((($correctAnswer - $yearGroupMean)/$standardDeviation),2); 
			
		?>		
		
<div class="rp-student-info">		
<table>
	<tbody>
		<tr>
			<td>Student's mark</td>
			<td><?php echo $correctAnswer;?></td>
		</tr>

		<tr>
			<td>Year group mean</td>
			<td><?php echo @$yearGroupMean;?></td>
		</tr>

		<tr>
			<td>Year group rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['year_group_rank'];?></td>
		</tr>
		
		<tr>
			<td>Class mean</td>
			<td><?php echo @$sameClassMean;?></td>
		</tr>
		
		<tr>
			<td>Class rank</td>
			<td><?php echo @$studentRank['MyExamStudentAnswer']['class_rank'];?></td>
		</tr>
		
		<tr>
			<td>Student's Z score by year group </td>
			<td><?php echo $studentZscoreByYearGrp;?></td>
		</tr>
		

	</tbody>
</table>
</div>

<?php
$category = explode(' ',$myPurchasedExam['Examination']['ExaminationCategory']['name']);
 
if($category[1] != 3 && $category[1] != 5){
 ?>
<div class="left_view_a_label">
<a title="Results by Question" href="javascript:void(0);" onclick="openModal('viewReportByType<?php echo $schoolPurchaseExam_id;?>');">Results by Question</a>
</div>

<div class="left_view_a_label">
<a title="Results by Question" href="javascript:void(0);" onclick="openModal('viewReportBySyllabusRef<?php echo $schoolPurchaseExam_id;?>');">Full analysis of Student Results and Results by Syllabus Reference</a>
</div>
<?php 
}
else {
	 if($myPurchasedExam['Examination']['exam_flag'] != 2){?>

	 <div class="left_view_a_label">
		<a title="Results by Question" href="javascript:void(0);" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$schoolPurchaseExam_id;?>')">Results by Question</a>
	 </div>


	 <div class="left_view_a_label">
		<a title="Results by Question" href="javascript:void(0);" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id;?>')">Full analysis of Student Results and Results by Syllabus Reference</a>
	  </div>
                   
<?php }
 }?>



<?php }?>
		
		
		
	</div>
	
	<?php if($this->params['action'] == 'result'){?>
    <div class="timing_bar">
        <div class="time_progress_bar">
            <div class="white_bar">
                <div class="main_bar"><div class="prcessing_speed" id="time_prcessing_speed" style="width:100%">&nbsp;</div></div>
				<input type="hidden" value="100" id="time_input">
                <div class="bar_value" id="percent_val_txt">100%</div>
            </div>
        </div>
        <div class="text_result">
<span id="hms_timer"></span>

<!--&nbsp;&nbsp;<button id="pauseBtnhms">Pause</button>&nbsp;&nbsp;
<button id="stopBtnhms">Stop</button>->

            <!--<h2>00:29:50</h2>-->
            <p>Time Remaining</p>
        </div>
    </div>
    <?php }?>
	
</div>
<?php }?>


<!-- Modal content for Results by Question-->
 <div id="viewReportByType<?php echo $schoolPurchaseExam_id;?>" class="modal_qs">
  
  	<div class="modal-content_qs">
	    <div class="modal-header_qs">
	      <span class="close" onclick="close_popup('viewReportByType<?php echo $schoolPurchaseExam_id;?>')">&times;</span>
	      <h4>Results by Question</h4>
  	</div>

    <div class="modal-body_qs">
      <?php  if($myPurchasedExam['Examination']['exam_flag'] == 2){?>
				    
	<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$schoolPurchaseExam_id.'/1';?>')">Reading Booklet Questions Results</button>
    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$schoolPurchaseExam_id.'/2';?>')">Language Conventions Results</button>
   
   <?php } else {?>
 <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$schoolPurchaseExam_id.'/non-calculator';?>')">Non-Calculator Results</button>
    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/result_chart/'.$schoolPurchaseExam_id.'/calculator';?>')">Calculator Results</button>
	   <?php }?>
     
    </div>
    

    <div class="modal-footer_qs">
      <h3><a href="javascript:void(0);" onclick="close_popup('viewReportByType<?php echo $schoolPurchaseExam_id;?>')" class="btn closebut btn-default">OK</a></h3>
    </div>
  </div>

</div>
<!-- end -->

<!-- Modal content for Full analysis of Student Results and Results by Syllabus Reference-->
<div id="viewReportBySyllabusRef<?php echo $schoolPurchaseExam_id;?>" class="modal_qs">
  
  	<div class="modal-content_qs">
	    <div class="modal-header_qs">
	      <span class="close" onclick="close_popup('viewReportBySyllabusRef<?php echo $schoolPurchaseExam_id;?>')">&times;</span>
	      <h4>Full analysis of Student Results and Results by Syllabus Reference</h4>
  	</div>

    <div class="modal-body_qs">
			
			 <?php  if($myPurchasedExam['Examination']['exam_flag'] == 2){?>
				    
					<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/1';?>')">Reading Booklet Questions Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/2';?>')">Language Conventions Results</button>
					<button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/all';?>')">Combined</button>
				   
				   <?php } else {?>
			
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/non-calculator';?>')">Non-Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/calculator';?>')">Calculator Results</button>
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="openUrl('<?php echo $this->webroot.'schools/substrands_result_chart/'.$schoolPurchaseExam_id.'/combined';?>')">Combined Results</button>
				   <?php }?>
                   
    </div>

            <div class="modal-footer_qs">
      <h3><a href="javascript:void(0);" onclick="close_popup('viewReportBySyllabusRef<?php echo $schoolPurchaseExam_id;?>')" class="btn closebut btn-default">OK</a></h3>
    </div>
  </div>

</div>
<!-- end -->


<script>
	$(function(){
			$('#hms_timer').countdowntimer({
				hours : 00,
				minutes :  <?php echo $exam_view_time;?>,
				seconds : 00,
				size : "lg",
				
			pauseButton : "pauseBtnhms",
			stopButton : "stopBtnhms"
			});
	});

	function openModal(modal_id) {
	var modal = document.getElementById(modal_id);
	 modal.style.display = "block";
      return false;
	}


	function close_popup(modal_id){
		var modal = document.getElementById(modal_id);
		 modal.style.display = "none";
	}

	function openUrl(url){
            var win = window.open(url, '_blank');
    }
	</script>
<style>
.rp-student-info table, tr, td{
	width:92%;
	border:2px solid #a4b0db !important;
	margin-left:10px;
}
.rp-student-info table tr td {
    padding: 23px;
	font-size:16px;
	
    color: #fff !important;
}
.right_bar_combined {
	min-height: 980px !important;
}
.modal_qs {
  width: 60% !important;
  left: 252px !important;
}
.left_view_a_label{
   margin-left: 10px !important;
   width: 92% !important;	
}
</style>