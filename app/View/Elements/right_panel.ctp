<script type="text/javascript" src="<?php echo $this->webroot;?>countdownTimer/jquery.countdownTimer.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/jquery.countdownTimer.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>countdownTimer/CSS/w3.css" />-->
<?php 

if(!isset($examSection))
{
	$examSection = 0;
}

if(!isset($step))
{
	$step = 0;
}
?>
<div class="right_bar">

<?php if($this->params['action'] == 'take_literacy_exam' || $this->params['action'] == 'take_exam' || $this->params['action'] == 'take_exam_second_section' || $this->params['action'] == 'sample_questions'){?>
    <div class="timing_bar">
        <div class="time_progress_bar">
            <div class="white_bar">
                <div class="main_bar"><div class="prcessing_speed" id="time_prcessing_speed" style="width:0%">&nbsp;</div></div>
				<input type="hidden" value="0" id="time_input">
                <div class="bar_value" id="percent_val_txt">0%</div>
            </div>
        </div>
        <div class="text_result">
<span id="hms_timer"></span>

<!--&nbsp;&nbsp;<button id="pauseBtnhms">Pause</button>&nbsp;&nbsp;
<button id="stopBtnhms">Stop</button>->

            <!--<h2>00:29:50</h2>-->
            <p>Time Remaining</p>
        </div>
    </div>

	
    
    <div class="date_bar">
	    <!-- do not delete commented below part
        <div class="time_progress_bar">
            <div class="white_bar">
                <div class="main_bar"><div class="prcessing_speed2" id="qs_attempted_prcessing" style="width:0%">&nbsp;</div></div>
                <div class="bar_value" id="percent_val_txt_qs">0%</div>
            </div>
        </div>
		
        <div class="text_result">
            <h2><span id="attempted_qs">0</span>/<?php echo @count($QuestionAns);?></h2>
            <p>Questions Attempted</p>
        </div>
		-->
		
    </div>
    <?php }?>
</div>
<script>			
			    var cookieValue = document.cookie;
			    
				
				var totalExamTime = <?php echo $examDuration;?>;
				var examination_id = <?php echo $examination_id;?>;
				var examSectionId = <?php echo $examSection;?>;
				var examStep = <?php echo $step;?>;
				var stratTime;
				var startTimeMinutes;
				var startTimeSecond;
				var cookieExamId;
				var cookieExamSectionId; 
				var cookieStep;
				
				
				stratTime = get_cookie_value(cookieValue,'remaining_time=');
				cookieExamId = get_cookie_value(cookieValue,'examid=');
				cookieExamSectionId = get_cookie_value(cookieValue,'exam_section_id=');
				cookieStep = get_cookie_value(cookieValue,'step=');
	
				//first time save time in cookie
				if(cookieExamId!=examination_id || cookieExamSectionId!=examSectionId || cookieStep!=examStep || stratTime == '' || stratTime == null)
				{ 
					stratTime = (totalExamTime*60); //in seconds									
					document.cookie = "remaining_time="+stratTime +"; path=/";
					document.cookie = "examid="+examination_id+"; path=/";
					document.cookie = "exam_section_id="+examSectionId+"; path=/";
					document.cookie = "step="+examStep+"; path=/";

					var remainTimeHMS = secondsTimeSpanToMS(stratTime);
					var remainTimeHMSJar = remainTimeHMS.split(':');
					
					startTimeMinutes = remainTimeHMSJar[0];
					startTimeSecond = remainTimeHMSJar[1];
					
				}
				else
				{		
					 var remainTimeHMS = secondsTimeSpanToMS(stratTime);
					 var remainTimeHMSJar = remainTimeHMS.split(':');
					 
					 startTimeMinutes = remainTimeHMSJar[0];
					 startTimeSecond = remainTimeHMSJar[1];
				}

				
				//update cookie value on interval of second			
						var cookie_set_time = 1*1000;
						
						setInterval(function() {
						
							if(stratTime > 0)
							{					   	
								 stratTime--;
								 document.cookie = "remaining_time="+stratTime +"; path=/";
								 document.cookie = "examid="+examination_id+"; path=/";
								 document.cookie = "exam_section_id="+examSectionId+"; path=/";
								 document.cookie = "step="+examStep+"; path=/";
								 var remainTimeHMS = secondsTimeSpanToMS(stratTime);
								 var remainTimeHMSJar = remainTimeHMS.split(':');
								 startTimeMinutes = remainTimeHMSJar[0];
								 startTimeSecond = remainTimeHMSJar[1];

							}
							
						}, cookie_set_time);

				
				 
				 
				 var examTimeSec = stratTime; //seconds
				 var remaining_percent =  (startTimeMinutes/totalExamTime)*100;
				 remaining_percent = Math.round(remaining_percent);

				 if(remaining_percent > 0)
				 {
				 	 $( "#time_input" ).val( remaining_percent);
					 $( "#percent_val_txt" ).text( remaining_percent+'%' );
					 $('#time_prcessing_speed').css({"width": remaining_percent+"%"});
					 var timeIntervalSec = examTimeSec / remaining_percent; //100 means % age
					 var timeIntervalMs = timeIntervalSec * 1000; //ms 


					 	setInterval(function() {
					 	 // Do something every 5 seconds
							
							 $(document).ready(function(){
								 
								var time_input = $( "#time_input" ).val( );
								time_input = time_input -1;
								var t = time_input;
								
								if(time_input >=1)
									  $( "#time_input" ).val( time_input);
									   $( "#percent_val_txt" ).text( time_input+'%' );
									   
										$('#time_prcessing_speed').css({"width": ""});
										$('#time_prcessing_speed').css({"width": t+"%"});
										
										
										});
							
						
							//$('#time_prcessing_speed').width(100%);

						}, timeIntervalMs);
				 }
			


	$(function(){
			$('#hms_timer').countdowntimer({
				hours : 00,
				minutes : startTimeMinutes,
				seconds : startTimeSecond,
				size : "lg",
				
			pauseButton : "pauseBtnhms",
			stopButton : "stopBtnhms"
			});
	});


	function secondsTimeSpanToMS(s) 
	{
				    var h = Math.floor(s/3600); //Get whole hours
				    s -= h*3600;
				    var m = Math.floor(s/60); //Get remaining minutes
				    s -= m*60;
				    m += h*60;
				    return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
	}

	function get_cookie_value(cookieValue,cookie_name)
	{
		var cookieJar = cookieValue.split(';');
		var cookieJarLength = cookieJar.length;
		var cookie_val;

		for (var i = 0; i < cookieJarLength; i++) {
						var chips = cookieJar[i];
						
						while (chips.charAt(0) === ' ') {	
							chips = chips.substring(1, chips.length);
						}
						if (chips.indexOf(cookie_name) === 0) {
							 cookie_val = chips.substring(cookie_name.length, chips.length);
							 //console.log(stratTime);					
						}
						
					}
		return cookie_val;			
	}
	</script>