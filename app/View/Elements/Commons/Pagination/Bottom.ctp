<style type="text/css">
    .current{
        font-weight: 900;
        font-size: medium;
    }
    li.disabled a{color: #fff !important; background-color: #337ab7 !important; border-color: #2e6da4 !important;}
</style>
<?php $paginator = $this->Paginator->params(); //pr($paginator); ?>
<div class="row" style="margin:5px 0;">

    <div class="col-md-8 col-sm-8 col-xs-12 mt-xs mb-sm" style="padding-left:0;">
        <?php if ($paginator['pageCount'] > 1) : ?>
            <!-- Pages -->
                <ul class="pagination pagination-sm" style="margin:0;">
                <?php
                echo $this->Paginator->numbers(array(
//                    'before' => ' | ',
//                    'after' => ' | ',
                    'tag' => 'li',
                    'currentTag' => 'a',
                    'separator' => '',
                    'modulus' => 2,
                    'currentClass' => 'disabled',
                    'first' => '« '.__('First page'),
                    'last' => __('Last page').' »'
                ));
                ?>
                </ul>
        <?php endif; ?>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 mt-xs mb-sm" style="padding-right:0;">
        <!-- Pages/Total -->
        <div style="float:right;" class="paginator floatNone">
            <?php echo $this->Paginator->counter(__('({:start} to {:end} of {:count})')); ?>
        </div>
    </div>
</div>

<div style="clear:both;"></div>