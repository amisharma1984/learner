<?php

App::uses('CakeEmail', 'Network/Email');

class SchoolimportShell extends AppShell {

    const STATUS_NEW = 0;
    const STATUS_COMPLETE = 1;
    const STATUS_PROCESSING = 2;
    const DEFAULTPAYMENTAMOUNT = 2;
    const DEFAULTVALIDITY = 100;
  
    public $from_default = array('arif.brainium@gmail.com' => 'Naplan Exam');
    public $from_domain_default = 'info@';
    public $uses = array('SchoolImport','School', 'Setting');

    public function main() {
        $this->out('Schools importer for cronjobs');
    }

    public function process() {
        $imports = $this->getPending();
        if (count($imports) > 0) {
            require(WWW_ROOT . 'lib/simplexlsx.class.php');
            CakeLog::write('userimport', 'There are ' . count($imports) . ' import(s) waiting for process.');
            foreach ($imports as $import) {
                CakeLog::write('document', 'Processing ' . $import['SchoolImport']['id']);
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_PROCESSING);
                $this->import($import);
                $this->SchoolImport->id = $import['SchoolImport']['id'];
                $this->SchoolImport->saveField('status', self::STATUS_COMPLETE);
            }
        }
    }

    public function getPending() {
        $imports = $this->SchoolImport->find('all', array('conditions' => array('status' => self::STATUS_NEW)));
        return $imports;
    }

    public function import($import) {

        $xlsFile = WWW_ROOT . 'files/school/imports/' . $import['SchoolImport']['path'];
        $xlsx = new SimpleXLSX($xlsFile);
        if (!$xlsx || empty($xlsx)) {
            CakeLog::write('schoolimport', 'There was an error opening xls file.');
            return;
        }

        $error = false;
        $already = 0;
        $total = 0;
       for($j=1;$j <= $xlsx->sheetsCount();$j++){

       foreach( $xlsx->rows($j) as $k => $r) {
          $worksheets =  $xlsx->rows($j);
       }
   } 
   unset($worksheets[0]);
        // Avoid first line for headers
        foreach ($worksheets as $index => $row) {
          
            $check = $this->School->findByEmail($row[2]);
            if (empty($check)) {
              if(!empty($row[4])){
                  $address=$row[4];
                }else{
                    $address='';
                }
                if(!empty($row[5])){
                  $phone=$row[5];
                }else{
                    $phone='';
                }
               
                if(!empty($row[6])){
                  $fax=$row[6];
                }else{
                    $fax='';
                }
            $data = array(
                'name' => $row[0],
                'manager_name' =>$row[1],
                'email' => $row[2],
                'password' => md5($row[3]),
                'address' => $address,
                'phone' => $phone,
                'fax' => $fax,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $dataemail = $data;
            $this->School->create();
            $this->School->save($data);
            $school_id = $this->School->id;
            $this->sendEmail($dataemail, $school_id,$row[3]);
           
            $total++;

            $this->SchoolImport->id = $import['SchoolImport']['id'];
            $this->SchoolImport->saveField('imported', $total);
        }

        $msg = '';
        if ($total > 0)
            $msg .= __('%d users was imported!', $total);
        if ($already > 0)
            $msg .= ($total > 0 ? " " : "") . __('%d already exists.', $already);
        CakeLog::write('schoolimport', $msg);
        }
    }

    private function sendEmail($school, $school_id = null,$password) {
        $domainURL = Configure::read('App.fullUrl');
        $app_name = Configure::read('App.appName');
        CakeLog::write('schoolimport', "Sending email to: ".$school['email']);
        
        $settingData = $this->Setting->getSettingsData();
        
        $email = new CakeEmail();
        $email->template('register', 'welcome');
        $email->to($school['email']);
        $email->subject(__('You registration at ', $app_name));
        $email->viewVars(array(
            'username' => $school['email'],
            'password' =>$password,
            'fullName' => $school['name'],
            'schoolId' => $school_id,
            'full_url' =>$domainURL,
            'validity' => (!empty($settingData['Setting']['school_registration_validity'])) ? $settingData['Setting']['school_registration_validity'] : self::DEFAULTVALIDITY,
            'registrationFee' => (!empty($settingData['Setting']['school_registration_fee'])) ? $settingData['Setting']['school_registration_fee'] : self::DEFAULTPAYMENTAMOUNT
            
        ));
        $email->replyTo($this->from_default);
        $email->from($this->from_default);
        $email->domain($domainURL);
        $email->emailFormat('html');

        if($email->send()){
            CakeLog::write('debug', "Email sent to: ".$school['email']);
        }else{
            CakeLog::write('error', "Can't send email to: ".$school['email']);
        }
    }

}